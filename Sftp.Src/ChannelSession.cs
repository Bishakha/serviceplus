using System;
using System.Net;
using System.Net.Sockets;

namespace SFTP.jsch
{
	
	public class ChannelSession : Channel
	{
		private static byte[] _session=Util.getBytes("session");
		internal ChannelSession():base()
		{
			type=_session;
			io=new IO();
		}

		/*
		public void init(){
		  io.setInputStream(session.in);
		  io.setOutputStream(session.out);
		}
		*/

		//  public void finalize() throws Throwable{
		//    super.finalize();
		//  }

		public override void run()
		{
			thread=System.Threading.Thread.CurrentThread;
			Buffer buf=new Buffer();
			Packet packet=new Packet(buf);
			int i=0;
			try
			{
				while(isConnected() &&
					thread!=null && 
					io!=null && 
					io.ins!=null)
				{
					i=io.ins.Read(buf.buffer, 14, buf.buffer.Length-14);
					if(i==0)continue;
					if(i==-1)
					{
						eof();
						break;
					}
					packet.reset();
					buf.putByte((byte) Session.SSH_MSG_CHANNEL_DATA);
					buf.putInt(recipient);
					buf.putInt(i);
					buf.skip(i);
					session.write(packet, this, i);
				}
			}
			catch
			{
				//System.out.println("ChannelSession.run: "+e);
			}
			thread=null;
		}

		//  public String toString(){
		//      return "Channel: type="+new String(type)+",id="+id+",recipient="+recipient+",window_size="+window_size+",packet_size="+packet_size;
		//  }
	}

}
