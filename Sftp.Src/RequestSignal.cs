using System;

namespace SFTP.jsch
{
	

	class RequestSignal : Request
	{
		String signal="KILL";
		public void setSignal(String foo){ signal=foo; }
		public void request(Session session, Channel channel)
		{
			Buffer buf=new Buffer();
			Packet packet=new Packet(buf);

			packet.reset();
			buf.putByte((byte) Session.SSH_MSG_CHANNEL_REQUEST);
			buf.putInt(channel.getRecipient());
			buf.putString( Util.getBytes("signal"));
			buf.putByte((byte)(waitForReply() ? 1 : 0));
			buf.putString(Util.getBytes(signal));
			session.write(packet);
		}
		public bool waitForReply(){ return false; }
	}

}
