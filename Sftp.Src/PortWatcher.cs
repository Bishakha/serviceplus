using System;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Threading;

namespace SFTP.jsch
{


	class PortWatcher
	{
		private static System.Collections.ArrayList pool=new System.Collections.ArrayList();

		internal Session session;
		internal int lport;
		internal int rport;
		internal String host;
		internal String boundaddress;
		internal System.Net.Sockets.TcpListener ss;
		internal object thread;

		internal static String[] getPortForwarding(Session session)
		{
			System.Collections.ArrayList foo=new System.Collections.ArrayList();
			lock(pool)
			{
				for(int i=0; i<pool.Count; i++)
				{
					PortWatcher p=(PortWatcher)(pool[i]);
					if(p.session==session)
					{
						foo.Add(p.lport+":"+p.host+":"+p.rport);
					}
				}
			}
			String[] bar=new String[foo.Count];
			for(int i=0; i<foo.Count; i++)
			{
				bar[i]=(String)(foo[i]);
			}
			return bar;
		}
		internal static PortWatcher getPort(Session session, int lport)
		{
			lock(pool)
			{
				for(int i=0; i<pool.Count; i++)
				{
					PortWatcher p=(PortWatcher)(pool[i]);
					if(p.session==session && p.lport==lport) return p;
				}
				return null;
			}
		}
		internal static PortWatcher addPort(Session session, String address, int lport, String host, int rport)
		{
			if(getPort(session, lport)!=null)
			{
				throw new JSchException("PortForwardingL: local port "+lport+" is already registered.");
			}
			PortWatcher pw=new PortWatcher(session, address, lport, host, rport);
			pool.Add(pw);
			return pw;
		}
		internal static void delPort(Session session, int lport)
		{
			PortWatcher pw=getPort(session, lport);
			if(pw==null)
			{
				throw new JSchException("PortForwardingL: local port "+lport+" is not registered.");
			}
			pw.delete();
			pool.Remove(pw);
		}
		internal static void delPort(Session session)
		{
			PortWatcher[] foo=new PortWatcher[pool.Count];
			int count=0;
			lock(pool)
			{
				for(int i=0; i<pool.Count; i++)
				{
					PortWatcher p=(PortWatcher)(pool[i]);
					if(p.session==session) 
					{
						p.delete();
						foo[count++]=p;
					}
				}
				for(int i=0; i<count; i++)
				{
					PortWatcher p=foo[i];
					pool.Remove(p);
				}
			}
		}
		internal PortWatcher(Session session, 
			String boundaddress, int lport, 
			String host, int rport)
		{
			this.session=session;
			this.boundaddress=boundaddress;
			this.lport=lport;
			this.host=host;
			this.rport=rport;
			try
			{
				//    ss=new ServerSocket(port);
				ss=new TcpListener(Dns.GetHostByName(this.boundaddress).AddressList[0], lport);	
				
			}
			catch(Exception e)
			{ 
				Console.WriteLine(e);
				throw new JSchException("PortForwardingL: local port "+lport+" cannot be bound.");
			}
		}
	
	

		public void run()
		{
			Buffer buf=new Buffer(300); // ??
			Packet packet=new Packet(buf);
			thread=this;
			try
			{
				ss.Start();
				while(thread!=null)
				{
					Socket socket=ss.AcceptSocket();
					socket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.NoDelay, 1);
					NetworkStream ns = new NetworkStream(socket);
					Stream ins= ns;
					Stream outs=ns;
					ChannelDirectTCPIP channel=new ChannelDirectTCPIP();
					channel.init();
					channel.setInputStream(ins);
					channel.setOutputStream(outs);
						session.addChannel(channel);
					((ChannelDirectTCPIP)channel).setHost(host);
					((ChannelDirectTCPIP)channel).setPort(rport);
					((ChannelDirectTCPIP)channel).setOrgIPAddress( ((IPEndPoint)socket.RemoteEndPoint).Address.ToString() );
					((ChannelDirectTCPIP)channel).setOrgPort(((IPEndPoint)socket.RemoteEndPoint).Port);
					channel.connect();
					if(channel.exitstatus!=-1)
					{
					}
				}
			}
			catch
			{
				//System.out.println("! "+e);
			}
		}

		void delete()
		{
			thread=null;
			try
			{ 
				if(ss!=null)ss.Stop();
				ss=null;
			}
			catch
			{
			}
		}
	}

}
