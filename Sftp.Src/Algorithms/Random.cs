using System;

namespace SFTP.jsch.jce
{
	public class Random : SFTP.jsch.Random
	{
		private byte[] tmp=new byte[16];
		//private SecureRandom random;
		private System.Security.Cryptography.RNGCryptoServiceProvider rand;
		public Random()
		{
			//    random=null;
			//	  random = new SecureRandom();
			//	  
			//    try{ random=SecureRandom.getInstance("SHA1PRNG"); }
			//    catch(java.security.NoSuchAlgorithmException e){ 
			//      // System.out.println(e); 
			//
			//      // The following code is for IBM's JCE
			//      try{ random=SecureRandom.getInstance("IBMSecureRandom"); }
			//      catch(java.security.NoSuchAlgorithmException ee){ 
			//	System.out.println(ee); 
			//      }
			//    }
			rand = new System.Security.Cryptography.RNGCryptoServiceProvider();
		}
		public void fill(byte[] foo, int start, int len)
		{
			if(len>tmp.Length){ tmp=new byte[len]; }
			//random.nextBytes(tmp);
			rand.GetBytes(tmp);
			Array.Copy(tmp, 0, foo, start, len);
		}
	}

}
