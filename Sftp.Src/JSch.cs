using System;
using System.IO;

namespace SFTP.jsch
{
	

	public class JSch{

		static System.Collections.Hashtable config;
	
		public static void Init()
		{
			config=new System.Collections.Hashtable();

			//  config.Add("kex", "diffie-hellman-group-exchange-sha1");
			config.Add("kex", "diffie-hellman-group1-sha1,diffie-hellman-group-exchange-sha1");
			config.Add("server_host_key", "ssh-rsa,ssh-dss");
			//config.Add("server_host_key", "ssh-dss,ssh-rsa");

//			config.Add("cipher.s2c", "3des-cbc,blowfish-cbc");
//			config.Add("cipher.c2s", "3des-cbc,blowfish-cbc");

			config.Add("cipher.s2c", "3des-cbc");
			config.Add("cipher.c2s", "3des-cbc");

//			config.Add("mac.s2c", "hmac-md5,hmac-sha1,hmac-sha1-96,hmac-md5-96");
//			config.Add("mac.c2s", "hmac-md5,hmac-sha1,hmac-sha1-96,hmac-md5-96");
			config.Add("mac.s2c", "hmac-md5");
			config.Add("mac.c2s", "hmac-md5");
			config.Add("compression.s2c", "none");
			config.Add("compression.c2s", "none");
			config.Add("lang.s2c", "");
			config.Add("lang.c2s", "");

			config.Add("diffie-hellman-group-exchange-sha1", 
				"SFTP.jsch.DHGEX");
			config.Add("diffie-hellman-group1-sha1", 
				"SFTP.jsch.DHG1");

			config.Add("dh",            "SFTP.jsch.jce.DH");
			config.Add("3des-cbc",      "SFTP.jsch.jce.TripleDESCBC");
			//config.Add("blowfish-cbc",  "SFTP.jsch.jce.BlowfishCBC");
			config.Add("hmac-sha1",     "SFTP.jsch.jce.HMACSHA1");
			config.Add("hmac-sha1-96",  "SFTP.jsch.jce.HMACSHA196");
			config.Add("hmac-md5",      "SFTP.jsch.jce.HMACMD5");
			config.Add("hmac-md5-96",   "SFTP.jsch.jce.HMACMD596");
			config.Add("sha-1",         "SFTP.jsch.jce.SHA1");
			config.Add("md5",           "SFTP.jsch.jce.MD5");
			config.Add("signature.dss", "SFTP.jsch.jce.SignatureDSA");
			config.Add("signature.rsa", "SFTP.jsch.jce.SignatureRSA");
			config.Add("keypairgen.dsa",   "SFTP.jsch.jce.KeyPairGenDSA");
			config.Add("keypairgen.rsa",   "SFTP.jsch.jce.KeyPairGenRSA");
			config.Add("random",        "SFTP.jsch.jce.Random");

			//config.Add("aes128-cbc",    "SFTP.jsch.jce.AES128CBC");

			//config.Add("zlib",          "com.jcraft.jsch.jcraft.Compression");

			config.Add("StrictHostKeyChecking",  "ask");
		}
	
	internal System.Collections.ArrayList pool=new System.Collections.ArrayList();
	internal System.Collections.ArrayList identities=new System.Collections.ArrayList();
	//private KnownHosts known_hosts=null;
	private HostKeyRepository known_hosts=null;

	public JSch(){
		//known_hosts=new KnownHosts(this);
		if (config==null)
			Init();
	}

	public Session getSession(String username, String host)  { return getSession(username, host, 22); }
	public Session getSession(String username, String host, int port)  {
		Session s=new Session(this); 
		s.setUserName(username);
		s.setHost(host);
		s.setPort(port);
		pool.Add(s);
		return s;
	}
	public void setHostKeyRepository(HostKeyRepository foo){
		known_hosts=foo;
	}
	public void setKnownHosts(String foo) {
		if(known_hosts==null) known_hosts=new KnownHosts(this);
		if(known_hosts is KnownHosts){
		lock(known_hosts){
		((KnownHosts)known_hosts).setKnownHosts(foo); 
		}
		}
	}
	public void setKnownHosts(StreamReader foo) { 
		if(known_hosts==null) known_hosts=new KnownHosts(this);
		if(known_hosts is KnownHosts){
		lock(known_hosts){
		((KnownHosts)known_hosts).setKnownHosts(foo); 
		}
		}
	}
	/*
	HostKeyRepository getKnownHosts(){ 
		if(known_hosts==null) known_hosts=new KnownHosts(this);
		return known_hosts; 
	}
	*/
	public HostKeyRepository getHostKeyRepository(){ 
		if(known_hosts==null) known_hosts=new KnownHosts(this);
		return known_hosts; 
	}
	/*
	public HostKey[] getHostKey(){
		if(known_hosts==null) return null;
		return known_hosts.getHostKey(); 
	}
	public void removeHostKey(String foo, String type){
		removeHostKey(foo, type, null);
	}
	public void removeHostKey(String foo, String type, byte[] key){
		if(known_hosts==null) return;
		known_hosts.remove(foo, type, key); 
	}
	*/
	public void addIdentity(String foo) {
		addIdentity(foo, (String)null);
	}
	public void addIdentity(String foo, String bar) {
		Identity identity=new IdentityFile(foo, this);
		if(bar!=null) identity.setPassphrase(bar);
		identities.Add(identity);
	}
	internal String getConfig(String foo){ return (String)(config[foo]); }

	private System.Collections.ArrayList proxies;
	void setProxy(String hosts, Proxy proxy){
		String[] patterns=Util.split(hosts, ",");
		if(proxies==null){proxies=new System.Collections.ArrayList();}
		lock(proxies){
		for(int i=0; i<patterns.Length; i++){
		if(proxy==null){
		proxies[0] = null;
		proxies[0] = System.Text.Encoding.Default.GetBytes( patterns[i] );
		}
		else{
		proxies.Add( System.Text.Encoding.Default.GetBytes( patterns[i] ) );
		proxies.Add(proxy);
		}
		}
		}
	}
	internal Proxy getProxy(String host){
		if(proxies==null)return null;
		byte[] _host= System.Text.Encoding.Default.GetBytes( host );
		lock(proxies){
		for(int i=0; i<proxies.Count; i+=2){
		if(Util.glob(((byte[])proxies[i]), _host)){
		return (Proxy)(proxies[i+1]);
		}
		}
		}
		return null;
	}
	internal void removeProxy(){
		proxies=null;
	}

	public static void setConfig(System.Collections.Hashtable foo){
		lock(config){
			System.Collections.IEnumerator e=foo.Keys.GetEnumerator();
		while(e.MoveNext()){
		String key=(String)(e.Current);
		config.Add(key, (String)(foo[key]));
		}
		}
	}
	}

}
