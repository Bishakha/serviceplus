using System;

namespace SFTP.jsch
{
	

	public interface UserInfo
	{
		String getPassphrase();
		String getPassword();
		bool promptPassword(String message);
		bool promptPassphrase(String message);
		bool promptYesNo(String message);
		void showMessage(String message);
	}
}
