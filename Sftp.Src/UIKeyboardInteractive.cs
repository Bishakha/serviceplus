using System;

namespace SFTP.jsch
{
	

	public interface UIKeyboardInteractive
	{
		String[] promptKeyboardInteractive(String destination,
			String name,
			String instruction,
			String[] prompt,
			bool[] echo);
	}
}
