using System;

namespace SFTP.jsch
{
	

	public interface DH
	{
		void init();
		void setP(byte[] p);
		void setG(byte[] g);
		byte[] getE();
		void setF(byte[] f);
		byte[] getK();
	}
}
