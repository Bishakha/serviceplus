using System;

namespace SFTP.jsch
{
	

	public abstract class SftpProgressMonitor
	{
		public static int PUT=0;
		public static int GET=1;
		public abstract void init(int op, String src, String dest, long max);
		public abstract bool count(long count);
		public abstract void end();
	}
}
