using System;

namespace SFTP.jsch
{


	internal interface Request
	{
		bool waitForReply();
		void request(Session session, Channel channel);
	}
}
