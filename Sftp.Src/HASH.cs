using System;

namespace SFTP.jsch
{


	public abstract class HASH
	{
		public abstract void init();
		public abstract int getBlockSize();
		public abstract void update(byte[] foo, int start, int len);
		public abstract byte[] digest();
	}
}
