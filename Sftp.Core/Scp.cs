using System;
using System.Net;
using SFTP.jsch;
using System.IO;
using System.Text;
using System.Collections;

namespace Sftp.SharpSsh {
    /// <summary>
    /// Class for handling SCP file transfers over SSH connection.
    /// </summary>
    public class Scp
	{
		/// <summary>
		/// Triggered when this SCP object starts connecting to the remote server.
		/// </summary>
		public event FileTansferEvent OnConnecting;
		/// <summary>
		/// Triggered when this SCP object starts the file transfer process.
		/// </summary>
		public event FileTansferEvent OnStart;
		/// <summary>
		/// Triggered when this SCP object ends the file transfer process.
		/// </summary>
		public event FileTansferEvent OnEnd;
		/// <summary>
		/// Triggered on every interval with the transfer progress iformation.
		/// </summary>
		public event FileTansferEvent OnProgress;

		/// <summary>
		/// The default value of the progress update interval.
		/// </summary>
		private int m_interval = 250;
        
		/// <summary>
		/// Copies a file from local machine to a remote SSH machine.
		/// </summary>
		/// <param name="localFile">The local file path.</param>
		/// <param name="remoteHost">The remote machine's hostname or IP address</param>
		/// <param name="remoteFile">The path of the remote file.</param>
		/// <param name="user">The username for the connection.</param>
		/// <param name="pass">The password for the connection.</param>
		public void To(string localFile, string remoteHost,string remoteFile, string user, string pass)
		{
            
			double progress=0;
			SendConnectingMessage("Connecting to "+remoteHost+"...");

			JSch jsch=new JSch();
			Session session=jsch.getSession(user, remoteHost, 22);
			session.setPassword( pass );

			Hashtable config=new Hashtable();
			config.Add("StrictHostKeyChecking", "no");
			session.setConfig(config);

			session.connect();

			// exec 'scp -t rfile' remotely
			String command="scp -p -t \""+remoteFile+"\"";
			Channel channel=session.openChannel("exec");
			((ChannelExec)channel).setCommand(command);

			// get I/O streams for remote scp
			Stream outs=channel.getOutputStream();
			Stream ins=channel.getInputStream();

			channel.connect();

			SendStartMessage("Connected, starting transfer.");

			byte[] tmp=new byte[1];

			if(checkAck(ins)!=0)
			{
				SendEndMessage(-1,0, "Error openning communication channel.");
				try{channel.close();}
				catch{}
				throw new Exception("Unknow error during file transfer.");
			}

			// send "C0644 filesize filename", where filename should not include '/'
							
			int filesize=(int)(new FileInfo(localFile)).Length;
			command="C0644 "+filesize+" ";
			if(localFile.LastIndexOf('/')>0)
			{
				command+=localFile.Substring(localFile.LastIndexOf('/')+1);
			}
			else
			{
				command+=localFile;
			}
			command+="\n";
			byte[] buff = Util.getBytes(command);
			outs.Write(buff, 0, buff.Length); outs.Flush();

			if(checkAck(ins)!=0)
			{
				SendEndMessage(-1,0, "Error openning communication channel.");
				try{channel.close();}
				catch{}
				throw new Exception("Unknow error during file transfer.");				
			}

			// send a content of lfile
			SendProgressMessage(0, filesize, "Transferring...");
			FileStream fis=File.OpenRead(localFile);
			byte[] buf=new byte[1024];
			int copied = 0;
			while(true)
			{
				int len=fis.Read(buf, 0, buf.Length);
				if(len<=0) break;
				outs.Write(buf, 0, len); outs.Flush();
				copied += len;
				progress = (copied*100.0/filesize);
				SendProgressMessage(copied, filesize, "Transferring...");
			}
			fis.Close();

			// send '\0'
			buf[0]=0; outs.Write(buf, 0, 1); outs.Flush();
			
			SendProgressMessage(copied, filesize, "Verifying transfer...");
			if(checkAck(ins)!=0)
			{
				SendEndMessage(-1,filesize, "Unknow error during file transfer.");
				try{channel.close();}
				catch{}
				throw new Exception("Unknow error during file transfer.");				
			}
			SendEndMessage(copied, filesize, "Transfer completed successfuly ("+copied+" bytes).");
			try{channel.close();}
			catch{}		
		}

		/// <summary>
		/// Copies a file from a remote SSH machine to the local machine.
		/// </summary>
		/// <param name="remoteHost">The remote machine's hosname or IP address.</param>
		/// <param name="remoteFile">The remote file path.</param>
		/// <param name="user">The username or the connection.</param>
		/// <param name="pass">The password for the connection.</param>
		/// <param name="localFile">The local file path.</param>
		public void From(string remoteHost,string remoteFile, string user, string pass, string localFile)
		{
			String prefix=null;
			if(Directory.Exists(localFile))
			{
				prefix=localFile+Path.DirectorySeparatorChar;
			}

			double progress=0;
			SendConnectingMessage("Connecting to "+remoteHost+"...");

			JSch jsch=new JSch();
			Session session=jsch.getSession(user, remoteHost, 22);
			session.setPassword( pass );

			Hashtable config=new Hashtable();
			config.Add("StrictHostKeyChecking", "no");
			session.setConfig(config);

			session.connect();

			// exec 'scp -f rfile' remotely
			String command="scp -f \""+remoteFile + "\"";
			Channel channel=session.openChannel("exec");
			((ChannelExec)channel).setCommand(command);

			// get I/O streams for remote scp
			Stream outs=channel.getOutputStream();
			Stream ins=channel.getInputStream();

			channel.connect();

			SendStartMessage("Connected, starting transfer.");

			byte[] buf=new byte[1024];

			// send '\0'
			buf[0]=0; outs.Write(buf, 0, 1); outs.Flush();
			int c=checkAck(ins);
			if(c!='C')
			{
				SendEndMessage(-1,0, "Error openning communication channel.");
				throw new Exception("Unknow error during file transfer.");
			}

			// read '0644 '
			ins.Read(buf, 0, 5);

			int filesize=0;
			while(true)
			{
				ins.Read(buf, 0, 1);
				if(buf[0]==' ')break;
				filesize=filesize*10+(buf[0]-'0');
			}

			String file=null;
			for(int i=0;;i++)
			{
				ins.Read(buf, i, 1);
				if(buf[i]==(byte)0x0a)
				{
					file=Util.getString(buf, 0, i);
					break;
				}
			}

			// send '\0'
			buf[0]=0; outs.Write(buf, 0, 1); outs.Flush();

			// read a content of lfile
			FileStream fos=File.OpenWrite(prefix==null ? 
			localFile :
				prefix+file);
			int foo;
			int size=filesize;
			int copied=0;
			while(true)
			{
				if(buf.Length<filesize) foo=buf.Length;
				else foo=filesize;
				int len=ins.Read(buf, 0, foo);
				copied += len;
				fos.Write(buf, 0, foo);
				progress += (len*100.0/size);
				SendProgressMessage(copied, size, "Transferring...");
				filesize-=foo;
				if(filesize==0) break;
			}
			fos.Close();

			byte[] tmp=new byte[1];

			SendProgressMessage(copied, size, "Verifying transfer...");
			if(checkAck(ins)!=0)
			{
				SendEndMessage(copied,size, "Unknow error during file transfer.");
				throw new Exception("Unknow error during file transfer.");
			}

			// send '\0'
			buf[0]=0; outs.Write(buf, 0, 1); outs.Flush();
			SendEndMessage(copied, size, "Transfer completed successfuly ("+copied+" bytes).");
		}

		private int checkAck(Stream ins) 
		{
			int b=ins.ReadByte();
			// b may be 0 for success,
			//          1 for error,
			//          2 for fatal error,
			//          -1
			if(b==0) return b;
			if(b==-1) return b;

			if(b==1 || b==2)
			{
				StringBuilder sb=new StringBuilder();
				int c;
				do 
				{
					c=ins.ReadByte();
					sb.Append((char)c);
				}
				while(c!='\n');
				if(b==1)
				{ // error
					Console.Write(sb.ToString());
				}
				if(b==2)
				{ // fatal error
					Console.Write(sb.ToString());
				}
			}
			return b;
		}

		private void SendConnectingMessage(string msg)
		{
			if(OnConnecting != null)
				OnConnecting(-1, 0, msg);
		}

		private void SendStartMessage(string msg)
		{
			if(OnStart != null)
				OnStart(-1, 0, msg);
		}

		private void SendEndMessage(int transferredBytes, int totalBytes, string msg)
		{
			if(OnEnd != null)
				OnEnd(transferredBytes, totalBytes, msg);
		}

		DateTime lastUpdate = DateTime.Now;
		private void SendProgressMessage(int transferredBytes, int totalBytes, string msg)
		{
			if(OnProgress != null)
			{
				TimeSpan diff = DateTime.Now-lastUpdate;

				if(diff.Milliseconds>ProgressUpdateInterval)
				{
					OnProgress(transferredBytes,totalBytes, msg);
					lastUpdate=DateTime.Now;
				}				
			}
		}

		/// <summary>
		/// Gets or sets the progress update interval in milliseconds
		/// </summary>
		public int ProgressUpdateInterval
		{
			get{return m_interval;}
			set{m_interval=value;}
		}
        public string[] GetFileList(string location)
        {
            string[] downloadFiles;
            StringBuilder result = new StringBuilder();
            WebResponse response = null;
            StreamReader reader = null;

   
            try
            {
                FtpWebRequest reqFTP;
                //reqFTP = (FtpWebRequest)FtpWebRequest.Create(new Uri("ftp://" + "43.145.65.155" + "/" + "apps/ServicesPlus/SoftwarePLUSDownloadFiles"));
                reqFTP = (FtpWebRequest)FtpWebRequest.Create(new Uri("ftp://" + "43.145.65.155" + "/" + location));
                reqFTP.UseBinary = true;
                reqFTP.Credentials = new NetworkCredential("swplus", "S3E5T9");
                reqFTP.Method = WebRequestMethods.Ftp.ListDirectory;
                reqFTP.Proxy = null;
                reqFTP.KeepAlive = false;
                reqFTP.UsePassive = false;
                response = reqFTP.GetResponse();
                reader = new StreamReader(response.GetResponseStream());
                string line = reader.ReadLine();
                while (line != null)
                {
                    result.Append(line);
                    result.Append("\n");
                    line = reader.ReadLine();
                }
                // to remove the trailing '\n'
                result.Remove(result.ToString().LastIndexOf('\n'), 1);
                return result.ToString().Split('\n');
            }
            catch (Exception ex)
            {
                if (reader != null)
                {
                    reader.Close();
                }
                if (response != null)
                {
                    response.Close();
                }
                downloadFiles = null;
                return downloadFiles;
            }
        }

        private void Download(string file)
        {
            try
            {
                string uri = "ftp://" + "43.145.65.155" + "/" + "/apps/ServicesPlus/SoftwarePLUSUserGuides" + "/" + file;
                Uri serverUri = new Uri(uri);
                if (serverUri.Scheme != Uri.UriSchemeFtp)
                {
                    return;
                }
                FtpWebRequest reqFTP;
                reqFTP = (FtpWebRequest)FtpWebRequest.Create(new Uri("ftp://" + "43.145.65.155" + "/" + "/apps/ServicesPlus/SoftwarePLUSUserGuides" + "/" + file));
                reqFTP.Credentials = new NetworkCredential("swplus", "S3E5T9");
                reqFTP.KeepAlive = false;
                reqFTP.Method = WebRequestMethods.Ftp.DownloadFile;
                reqFTP.UseBinary = true;
                reqFTP.Proxy = null;
                reqFTP.UsePassive = false;
                FtpWebResponse response = (FtpWebResponse)reqFTP.GetResponse();
                Stream responseStream = response.GetResponseStream();

                FileStream writeStream = new FileStream(@"C:\log\" + file, FileMode.Create);
                int Length = 2048;
                Byte[] buffer = new Byte[Length];
                int bytesRead = responseStream.Read(buffer, 0, Length);
                while (bytesRead > 0)
                {
                    writeStream.Write(buffer, 0, bytesRead);
                    bytesRead = responseStream.Read(buffer, 0, Length);
                }
                writeStream.Close();
                response.Close();
            }
            catch (WebException wEx)
            {
                // MessageBox.Show(wEx.Message, "Download Error");
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message, "Download Error");
            }
        }
          public bool TestConnection(string ipAddress,string folderLocation,string userid,string password)
        {
            try
            {
                WebResponse response = null;
                FtpWebRequest reqFTP;
                //reqFTP = (FtpWebRequest)FtpWebRequest.Create(new Uri("ftp://" + "43.145.65.155" + "/" + "apps/ServicesPlus/SoftwarePLUSDownloadFiles"));
                reqFTP = (FtpWebRequest)FtpWebRequest.Create(new Uri("ftp://" + ipAddress + "/" + folderLocation));
                reqFTP.UseBinary = true;
                reqFTP.Credentials = new NetworkCredential(userid, password);
                reqFTP.Method = WebRequestMethods.Ftp.ListDirectory;
                reqFTP.Proxy = null;
                reqFTP.KeepAlive = false;
                reqFTP.UsePassive = false;
                response = reqFTP.GetResponse();
                if (response == null)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch (WebException wEx)
            {
                // MessageBox.Show(wEx.Message, "Download Error");
                return false;
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message, "Download Error");
                return false;
            }
        }

	}
	public delegate void FileTansferEvent(int transferredBytes, int totalBytes, string message);
}
