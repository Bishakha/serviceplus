using System;
using Sony.US.siam;
using Sony.US.SIAMUtilities;
using Sony.US.siam.Interfaces;
using Sony.US.siam.Administration;
using Sony.US.siam.Authentication;
using Sony.US.siam.Authorization;

namespace Sony.US.siam
{
	/// <summary>
	/// A serializable object used to define an application.
	/// </summary>
	[Serializable]
	public class Application:IComparable
	{
		private int _id;
		private string _name;

		/// <summary>
		/// IComparable: CompareTo implementation
		/// </summary>
		/// <param name="App"></param>
		/// <returns></returns>
		public int CompareTo(object App)
		{
			if (App is Application)
			{
				Application theApp = (Application)App;
				return this.Name.CompareTo(theApp.Name);
			}
			else
			{
				throw new ArgumentException("IComparable:Object is not of type sony.us.siam.User");
			}
		}

		/// <summary>
		/// The Id of the application.
		/// </summary>
		public int Id
		{
			get
			{
				return _id;
			}
			set
			{
				_id = value;
			}
		}

		/// <summary>
		/// The name of the application.
		/// </summary>
		public string Name
		{
			get
			{
				return _name;
			}
			set
			{
				_name = value;
			}
		}
	}
}
