using System;
using System.Diagnostics;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Web;
using Sony.US.AuditLog;
using Sony.US.siam.Interfaces;
using Sony.US.SIAMUtilities;

namespace Sony.US.siam.Administration {
    /// <summary>
    /// The LDAPAdministrator class is the administration delegate for LDAP in the Sony specific implementation
    /// of SIAM. This class is defined in configuration specific to each user type. Not all user types defined
    /// in SIAM will require LDAP administration. This class implements the ISecurityAdministrator interface.
    /// Those methods that do not have contextual meaning for LDAP administration will simply return without
    /// any actual implemtation. 
    /// </summary>
    internal class LDAPAdministrator : ISecurityAdministrator {
        #region Class Variables

        private WinLDAP.ldap _rldap = null; //for reading operation
        private WinLDAP.ldap _wldap = null; //for writing operation
        private const string ClassName = "LDAPAdministrator";
        private string _bindobject = "";
        private string _password = "";
        private string _userDN = "";
        private OracleAdministrator _dal;
        //private SonyLogger Logger = new SonyLogger();
        #endregion

        #region Constructor

        /// <summary>
        /// LDAPAdministrator constructor requires the following constructor arguments. These arguments are
        /// defined in configuration as "constructorArgs". The order in which they are defined in configuration
        /// is significant. They will be passed into this class according to configuration's index order.
        /// </summary>
        /// <param name="RHost"></param>
        /// <param name="RPort"></param>
        /// <param name="WHost"></param>
        /// <param name="WPort"></param>
        /// <param name="BindObject"></param>
        /// <param name="Password"></param>
        /// <param name="UserDN"></param>
        internal LDAPAdministrator(string RHost, string RPort, string WHost, string WPort, string BindObject, string Password, string UserDN) {
            _rldap = new WinLDAP.ldap(RHost, System.Int32.Parse(RPort));
            _wldap = new WinLDAP.ldap(WHost, System.Int32.Parse(WPort));
            _bindobject = BindObject;
            _password = Password;
            _userDN = UserDN;
            _dal = new DelegateHandler().GetCurrentDal() as OracleAdministrator;

        }

        #endregion

        #region Add Methods



        /// <summary>
        /// Add Action has no contextal meaning in terms of LDAP. Present only to satisfy
        /// the implementation of ISecurityAdministrator. Method has no implementation.
        /// </summary>
        /// <param name="ActionName">No meaning</param>
        /// <param name="ErrorString">None</param>
        /// <returns>Success code in all cases</returns>
        /// //6732 starts
        //public double AddAction(string ActionName,out string ErrorString)
        //{
        //    ErrorString = "";
        //    return (double)Messages.ADD_ACTION_COMPLETED;

        //}
        //6732 ends

        /// <summary>
        /// Add Application has no contextual meaning in terms of LDAP. Present only to satisfy
        /// the implementation of ISecurityAdministrator. Method has no implementation.
        /// </summary>
        /// <param name="ApplicationName">No meaning</param>
        /// <param name="ErrorString">None</param>
        /// <returns>Success code in all cases</returns>
        public double AddApplication(string ApplicationName, out string ErrorString) {
            ErrorString = "";
            return (double)Messages.ADD_APPLICATION_COMPLETED;

        }


        /// <summary>
        /// Add Application Resource has no contextual meaning in terms of LDAP. Present only to satisfy
        /// the implementation of ISecurityAdministrator. Method has no implementation.
        /// </summary>
        /// <param name="ApplicationId">No meaning</param>
        /// <param name="ResourceId">No meaning</param>
        /// <param name="ErrorString">None</param>
        /// <returns>Success code in all cases</returns>
        public double AddApplicationResource(double ApplicationId, double ResourceId, out string ErrorString) {
            ErrorString = "";
            return (double)Messages.ADD_RESOURCE_COMPLETED;

        }

        //public object GetCCModifierUser(string UserId, out string ErrorString)
        //{
        //    ErrorString = "";
        //    return null;
        //}

        /// <summary>
        /// Add Resource has no contextual meaning in terms of LDAP. Present only to satisfy the implementation
        /// of ISecurityAdministrator. Method has no implementation.
        /// </summary>
        /// <param name="ResourceName">No meaning</param>
        /// <param name="ErrorString">None</param>
        /// <returns>Success code in all cases</returns>
        public double AddResource(string ResourceName, out string ErrorString) {
            ErrorString = "";
            return (double)Messages.ADD_RESOURCE_COMPLETED;

        }


        /// <summary>
        /// Add Role has no contextual meaning in terms of LDAP. Present only to satisfy the implementation
        /// of ISecurityAdministrator. Method has no implementation.
        /// </summary>
        /// <param name="RoleName">No meaning</param>
        /// <param name="ParentRoleId">No meaning</param>
        /// <param name="ErrorString">None</param>
        /// <returns>Success code in all cases</returns>
        public double AddRole(string RoleName, int ParentRoleId, out string ErrorString) {
            ErrorString = "";
            return (double)Messages.ADD_ROLE_COMPLETED;

        }


        /// <summary>
        /// Add Role (overload). Has no contextual meaning in terms of LDAP. Method has no implementation.
        /// Present only to satisfy the implementation of ISecurityAdministrator.
        /// </summary>
        /// <param name="RoleName">No meaning</param>
        /// <param name="ErrorString">None</param>
        /// <returns>Success code in all cases</returns>
        public double AddRole(string RoleName, out string ErrorString) {
            ErrorString = "";
            return (double)Messages.ADD_ROLE_COMPLETED;

        }


        /// <summary>
        /// Add User does have meaning in terms of LDAP. Under certain conditions when a user is added
        /// to SIAM, the user must also be added to LDAP. This method will implement that process. 
        /// NOTE: At the moment this will be a call into a Sony Web Service. SIAM does not have the
        /// authority to add a user to Sony's LDAP directory. We will pass user information to a Web
        /// Service written by Sony and await the response in an Asynchronous fashion.
        /// </summary>
        /// <param name="user">An instance of sony.us.siam.User containing the details for the user
        /// being added to the LDAP directory.</param>
        /// <param name="ErrorString">An LDAP value representing any error conditions that occur.</param>
        /// <returns>A string value. Not of any significance in this implementation of siam. The string
        /// value returned by AddUser is normally defined as the User's SIAM Identity. This value will
        /// have already been obtained in a prior call to SIAM's backend administration delegate. The value
        /// returned by this call will be an empty string (""). The implementation of this method will be
        /// an asynchronous call to a Web Service. The WebService will call back using SIAM's ApproveB2BUser
        /// with the User's LDAP ID. This value is stored in SIAM as the alternate user id. In future
        /// implementations of SIAM where update authority is granted this call could operate in a 
        /// synchronous fashion, add the user and then immediately return the LDAP ID.</returns>
        //        public string AddUser(User user,out string ErrorString)
        //        {
        //            // Log Data
        //            TimeSpan start = TimeKeeper.GetCurrentTime();
        //            //Logger.RaiseTrace(ClassName+":AddUser","Entry:"+start.ToString(),TraceLevel.Verbose);

        //            // Implementation goes here. If any!
        //            ErrorString = "";

        //            //CreateUserMapperService mapper = new CreateUserMapperService();
        //            //RequestedUserInfo requestedInfo = new RequestedUserInfo();
        //            //requestedInfo.ADDRESS_LINE1 = user.AddressLine1;
        //            //requestedInfo.ADDRESS_LINE2 = user.AddressLine2;
        //            //requestedInfo.ADDRESS_LINE3 = "";
        //            //requestedInfo.CITY = user.City;
        //            //requestedInfo.COMPANY_NAME = user.CompanyName;
        //            //requestedInfo.CONTACT_NUMBER = user.Phone;
        //            //requestedInfo.COUNTRY = "USA";
        //            //requestedInfo.DEPARTMENT = "Technology";
        //            //requestedInfo.EMAIL = user.EmailAddress;
        //            //requestedInfo.FAX = user.Fax;
        //            //requestedInfo.FIRST_NAME = user.FirstName;
        //            //requestedInfo.isUserValid = true;
        //            //requestedInfo.LAST_NAME = user.LastName;
        //            //requestedInfo.PASSWORD = user.Password;
        //            //requestedInfo.PHONE = user.Phone;
        //            //requestedInfo.POSTAL_CODE = user.Zip;
        //            //requestedInfo.SECRETPIN_ANSWER = user.ThirdID.UserSecurityPINs[0];
        //            //requestedInfo.SECRETPIN_ANSWER2 = user.ThirdID.UserSecurityPINs[1];
        //            //requestedInfo.SECRETPIN_ANSWER3 = user.ThirdID.UserSecurityPINs[2];
        //            //requestedInfo.STATE = user.State;
        //            //requestedInfo.USERID = user.UserId;
        //            //requestedInfo.userValid = true;


        //            String admin_id = "uid=iam-admin,ou=systems,l=america,dc=sony,dc=com";


        //            CreateSonyB2BUserSearch search = new CreateSonyB2BUserSearch();
        //            search.CreateNew = true;
        //            search.CreateNewSpecified = true;
        //            CreateSonyB2BUserSonyB2BUserProfileTab pt = new CreateSonyB2BUserSonyB2BUserProfileTab();

        //            pt._BAR_LAOrgID_BAR_ = "test";
        //            pt._BAR_LASonyLoginID_BAR_ = user.EmailAddress;
        //            pt._PCT_FIRST_NAME_PCT_ = user.FirstName;
        //            pt._PCT_LAST_NAME_PCT_ = user.LastName;
        //            pt._PCT_FULL_NAME_PCT_ = user.FirstName + " " + user.LastName; 
        //            pt._PCT_EMAIL_PCT_ = user.EmailAddress;
        //            pt._PCT_PASSWORD_PCT_ = "Sony$123";//user.Password;
        //            pt._BAR_passwordConfirm_BAR_ = "Sony$123"; //user.Password;
        //            pt.st = user.State;
        //            pt.l = user.City;
        //            pt.postaladdress = user.Zip;
        //            pt.telephonenumber = user.Phone;
        //            pt.postaladdress = user.AddressLine1 + " " + user.AddressLine2;



        //            //AccountInfoVO acctInfo = new AccountInfoVO();
        //            //acctInfo.accountType = 10001;
        //            //acctInfo.duns = "";
        //            //acctInfo.account = "1234567";
        //            //acctInfo.sonyContact = "R. Upton";

        //            //get requesting application requested, requesting userid and password from config file - bs 03/26/2004
        //            //SIAMConfig config = CachingServices.Cache["SiamConfiguration"] as SIAMConfig;
        //            //if (config == null)
        //            //{
        //            //    config = new SIAMConfig();
        //            //}

        //            try
        //            {
        ////				WSResults r = mapper.userRequest(requestedInfo,acctInfo,"10049","100B000198","password");
        //                try
        //                {
        //                    //WSResults r = mapper.userRequest(requestedInfo, acctInfo, ConfigurationData.Environment.ISSAWebService.RequestingApplication, ConfigurationData.Environment.ISSAWebService.RequestingUserID, ConfigurationData.Environment.ISSAWebService.RequestingPassword);
        //                    //ErrorString = r.message;
        //                    CreateSonyB2BUser sr = new CreateSonyB2BUser();
        //                    sr.CreateSonyB2BUserSearch = search;
        //                    sr.CreateSonyB2BUserSonyB2BUserProfileTab = pt;
        //                    Tews6SoapBinding sb = new Tews6SoapBinding();
        //                    TaskContext tc = new TaskContext();
        //                    //Configuration value
        //                    tc.admin_id = "uid=iam-admin,ou=systems,l=america,dc=sony,dc=com";
        //                    ImsStatus ret = sb.CreateSonyB2BUser(tc, sr);
        //                    if (!string.IsNullOrEmpty(ret.transactionId))
        //                    {
        //                        SEL_ViewUserSearchFilter sf = new SEL_ViewUserSearchFilter();

        //                        sf.index = "0";
        //                        sf.Field = "%EMAIL%";
        //                        //sf.Field = "%USER_ID%";
        //                        sf.Op = OperatorType.CONTAINS;
        //                        sf.Value = user.EmailAddress;
        //                        //sf.Value = "praveen22@a.mn";

        //                        SEL_ViewUserSearchFilter[] sfArr = new SEL_ViewUserSearchFilter[1];
        //                        sfArr[0] = sf;
        //                        SEL_ViewUserSearch us = new SEL_ViewUserSearch();
        //                        us.Items = sfArr;

        //                        SEL_ViewUserQuery vuq = new SEL_ViewUserQuery();
        //                        vuq.SEL_ViewUserSearch = us;

        //                        SEL_ViewUserQueryResult res = sb.SEL_ViewUserQuery(tc, vuq);

        //                        SEL_ViewUserProfileTab NewProfile = res.SEL_ViewUserProfileTab;
        //                        if (pt != null)
        //                        {
        //                            string LdapID = NewProfile._PCT_USER_ID_PCT_;
        //                        }
        //                    }
        //                }
        //                catch (System.Web.Services.Protocols.SoapException spEX)
        //                {
        //                    ErrorString = spEX.Message; // reached the ldap server

        //                }	

        //                }
        //                catch(System.Exception ex)
        //                {
        //                    ErrorString = ex.Message;
        //                }

        //            //Add to ISSAWEBCALL must happen before posting data to LDAP and at the time of B2B registration.
        //            //It is in AddUser Method of SecurityAdministrator
        //            //Here only Message will be updated
        //            //_dal.AddISSAWebCall(user,ErrorString,out ErrorString);

        //            string outError = string.Empty;
        //            _dal.UpdateISSAWebCallMessage(user.FirstName, user.LastName, user.EmailAddress, ErrorString, out outError);

        //            // Log Data
        //            TimeSpan end = TimeKeeper.GetCurrentTime();
        //            TimeSpan exectime = start.Subtract(end);
        //            //Logger.RaiseTrace(ClassName+":Add User","Exit:ExecutionTime="+exectime.ToString(),TraceLevel.Verbose);
        //            return ErrorString;
        //        }
        public string AddUser(User user, out string ErrorString) {
            //Need to load from Configuration
            String admin_id = "uid=iam-admin,ou=systems,l=america,dc=sony,dc=com";
            string LdapID = string.Empty;
            ErrorString = string.Empty;
            CreateSonyB2BUserSearch search = new CreateSonyB2BUserSearch();
            search.CreateNew = true;
            search.CreateNewSpecified = true;
            CreateSonyB2BUserSonyB2BUserProfileTab pt = new CreateSonyB2BUserSonyB2BUserProfileTab();
            PCILogger objPCILoggerForCustomer = new PCILogger(); //6524

            pt._BAR_LAOrgID_BAR_ = user.CompanyName;
            pt._BAR_LASonyLoginID_BAR_ = user.EmailAddress;
            pt._PCT_FIRST_NAME_PCT_ = user.FirstName;
            pt._PCT_LAST_NAME_PCT_ = user.LastName;
            pt._PCT_FULL_NAME_PCT_ = user.FirstName + " " + user.LastName;
            pt._PCT_EMAIL_PCT_ = user.EmailAddress;
            pt._PCT_PASSWORD_PCT_ = user.Password;
            pt._BAR_passwordConfirm_BAR_ = user.Password;
            pt.st = user.State;
            pt.l = user.City;
            pt.postaladdress = user.Zip;
            pt.telephonenumber = user.Phone;
            pt.postaladdress = user.AddressLine1 + " " + user.AddressLine2;

            try {

                try {
                    //6524 start
                    objPCILoggerForCustomer.EventOriginApplication = "ServicesPLUS";
                    objPCILoggerForCustomer.EventOriginApplicationLocation = this.GetType().ToString();
                    objPCILoggerForCustomer.EventOriginMethod = "AddUser(User user, out string ErrorString)";
                    objPCILoggerForCustomer.EventOriginServerMachineName = HttpContext.Current.Server.MachineName.ToString();
                    objPCILoggerForCustomer.OperationalUser = Environment.UserName.ToString();
                    objPCILoggerForCustomer.CustomerID = user.CustomerID.ToString();
                    objPCILoggerForCustomer.CustomerID_SequenceNumber = user.SequenceNumber;
                    //objPCILoggerForCustomer.UserName = user.EmailAddress.ToString();
                    objPCILoggerForCustomer.EmailAddress = user.EmailAddress.ToString(); //6524
                    objPCILoggerForCustomer.EventType = EventType.Add;
                    objPCILoggerForCustomer.EventDateTime = DateTime.Now.ToLongTimeString();
                    objPCILoggerForCustomer.HTTPRequestObjectValues = (HTTPRequestObject)objPCILoggerForCustomer.GetRequestContextValue();
                    objPCILoggerForCustomer.IndicationSuccessFailure = "Success";
                    objPCILoggerForCustomer.Message = "User Added";
                    //6524 end

                    //WSResults r = mapper.userRequest(requestedInfo, acctInfo, ConfigurationData.Environment.ISSAWebService.RequestingApplication, ConfigurationData.Environment.ISSAWebService.RequestingUserID, ConfigurationData.Environment.ISSAWebService.RequestingPassword);
                    //ErrorString = r.message;
                    CreateSonyB2BUser sr = new CreateSonyB2BUser();
                    sr.CreateSonyB2BUserSearch = search;
                    sr.CreateSonyB2BUserSonyB2BUserProfileTab = pt;
                    Tews6PortTypeClient sb = new Tews6PortTypeClient();
                    //sb.ClientCredentials.UserName.UserName = "ServicesPlus-WebSrvc";
                    //sb.ClientCredentials.UserName.Password = "76LRq2hLYH3";

                    TaskContext tc = new TaskContext();
                    //Configuration value
                    tc.admin_id = admin_id;

                    if (ConfigurationData.GeneralSettings.Environment.Equals("Production")) {

                        MessageHeader header = MessageHeader.CreateHeader("My-CustomHeader", "http://myurl", "Custom Header.");

                        using (OperationContextScope scope = new OperationContextScope(sb.InnerChannel)) {
                            OperationContext.Current.OutgoingMessageHeaders.Add(header);

                            HttpRequestMessageProperty httpRequestProperty = new HttpRequestMessageProperty();
                            httpRequestProperty.Headers.Add("Authorization", "Basic U2VydmljZXNQbHVzLVdlYlNydmM6NzZMUnEyaExZSDM=");
                            httpRequestProperty.Headers.Add("COOKIE", "SMCHALLENGE=YES");
                            OperationContext.Current.OutgoingMessageProperties[HttpRequestMessageProperty.Name] = httpRequestProperty;

                        }
                    }
                    ImsStatus ret = sb.CreateSonyB2BUser(tc, sr);
                    if (!string.IsNullOrEmpty(ret.transactionId)) {
                        SEL_ViewUserSearchFilter sf = new SEL_ViewUserSearchFilter();

                        sf.index = "0";
                        sf.Field = "%EMAIL%";
                        //sf.Field = "%USER_ID%";
                        sf.Op = OperatorType.CONTAINS;
                        sf.Value = user.EmailAddress;
                        //sf.Value = "praveen22@a.mn";

                        SEL_ViewUserSearchFilter[] sfArr = new SEL_ViewUserSearchFilter[1];
                        sfArr[0] = sf;
                        SEL_ViewUserSearch us = new SEL_ViewUserSearch();
                        us.Items = sfArr;

                        SEL_ViewUserQuery vuq = new SEL_ViewUserQuery();
                        vuq.SEL_ViewUserSearch = us;

                        SEL_ViewUserQueryResult res = sb.SEL_ViewUserQuery(tc, vuq);

                        SEL_ViewUserProfileTab NewProfile = res.SEL_ViewUserProfileTab;
                        if (pt != null) {
                            LdapID = NewProfile._PCT_USER_ID_PCT_;
                            objPCILoggerForCustomer.CustomerID = LdapID;//6524
                        }
                    }
                } catch (System.Web.Services.Protocols.SoapException spEX) {
                    ErrorString = spEX.Message; // reached the ldap server
                    objPCILoggerForCustomer.IndicationSuccessFailure = "Failure";
                    objPCILoggerForCustomer.Message = "Unable to Add User. " + spEX.Message.ToString();
                }

            } catch (Exception ex) {
                ErrorString = ex.Message;
                objPCILoggerForCustomer.IndicationSuccessFailure = "Failure";//6524
                objPCILoggerForCustomer.Message = "Unable to Add User. " + ex.Message.ToString();//6524
            } finally {
                objPCILoggerForCustomer.PushLogToMSMQ();//6524
            }


            return LdapID;
        }

        public string AddTestUser(User user, AccountInfoVO acctInfo, string RequestingApplication, string RequestingUserID, string RequestingPassword, out string ErrorString) {
            ErrorString = "";

            CreateUserMapperService mapper = new CreateUserMapperService();
            RequestedUserInfo requestedInfo = new RequestedUserInfo();
            requestedInfo.ADDRESS_LINE1 = user.AddressLine1;
            requestedInfo.ADDRESS_LINE2 = user.AddressLine2;
            requestedInfo.ADDRESS_LINE3 = "";
            requestedInfo.CITY = user.City;
            requestedInfo.COMPANY_NAME = user.CompanyName;
            requestedInfo.CONTACT_NUMBER = user.Phone;
            requestedInfo.COUNTRY = "USA";
            requestedInfo.DEPARTMENT = "Technology";
            requestedInfo.EMAIL = user.EmailAddress;
            requestedInfo.FAX = user.Fax;
            requestedInfo.FIRST_NAME = user.FirstName;
            requestedInfo.isUserValid = true;
            requestedInfo.LAST_NAME = user.LastName;
            requestedInfo.PASSWORD = user.Password;
            requestedInfo.PHONE = user.Phone;
            requestedInfo.POSTAL_CODE = user.Zip;
            requestedInfo.SECRETPIN_ANSWER = user.ThirdID.UserSecurityPINs[0];
            requestedInfo.SECRETPIN_ANSWER2 = user.ThirdID.UserSecurityPINs[1];
            requestedInfo.SECRETPIN_ANSWER3 = user.ThirdID.UserSecurityPINs[2];
            requestedInfo.STATE = user.State;
            requestedInfo.USERID = user.UserId;
            requestedInfo.userValid = true;

            //AccountInfoVO acctInfo = new AccountInfoVO();
            //acctInfo.accountType = 10001;
            //acctInfo.duns = "";
            //acctInfo.account = "1234567";
            //acctInfo.sonyContact = "R. Upton";
            try {
                try {
                    WSResults r = mapper.userRequest(requestedInfo, acctInfo, RequestingApplication, RequestingUserID, RequestingPassword);
                    ErrorString = r.message;
                } catch (System.Web.Services.Protocols.SoapException spEX) {
                    ErrorString = spEX.Message; // reached the ldap server

                }

            } catch (Exception ex) {
                ErrorString = ex.Message;
            }
            return ErrorString;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="user"></param>
        /// <param name="ErrorString"></param>
        /// <param name="ErrorString"></param>
        /// <returns></returns>
        public bool AddISSAWebCall(User user, string Message, out string ErrorString) {
            ErrorString = "";
            return false;
        }


        /// <summary>
        /// AddUserRole has no meaning in terms of LDAP. Method has no implementation.
        /// </summary>
        /// <param name="UserId">No meaning</param>
        /// <param name="RoleId">No meaning</param>
        /// <param name="ErrorString">None</param>
        /// <returns>Success code in all cases</returns>
        public double AddUserRole(string UserId, int RoleId, out string ErrorString) {
            ErrorString = "";
            return (double)Messages.ADD_USERROLE_COMPLETED;

        }


        /// <summary>
        /// Add Access Right has no contextual meaning in terms of LDAP. Method has no implementation.
        /// </summary>
        /// <param name="RoleId">No meaning</param>
        /// <param name="ApplicationId">No meaning</param>
        /// <param name="ResourceId">No meaning</param>
        /// <param name="ActionId">No meaning</param>
        /// <param name="ErrorString">None</param>
        /// <returns>Success code in all cases</returns>
        public double AddAccessRight(int RoleId, int ApplicationId, int ResourceId, int ActionId, out string ErrorString) {
            ErrorString = "";
            return (double)Messages.ADD_ACCESSRIGHT_COMPLETED;
        }

        #endregion

        #region Delete Methods


        /// <summary>
        /// No contextual meaning in terms of LDAP. Method has no implementation.
        /// </summary>
        /// <param name="ActionId">No meaning</param>
        /// <param name="ErrorString">None</param>
        /// <returns>Success value in all cases</returns>
        //6732 starts
        /*public bool DeleteAction(int ActionId,out string ErrorString)
		{
			ErrorString = "";
			return true;
		}*/
        //6732 ends
        /// <summary>
        /// No contextual meaning in terms of LDAP. Method has no implementation.
        /// </summary>
        /// <param name="ApplicationId">No meaning</param>
        /// <param name="ErrorString">None</param>
        /// <returns>Success value in all cases</returns>
        public bool DeleteApplication(int ApplicationId, out string ErrorString) {
            ErrorString = "";
            return true;

        }

        /// <summary>
        /// No contextual meaning in terms of LDAP. Method has no implementation.
        /// </summary>
        /// <param name="ApplicationId">No meaning</param>
        /// <param name="ResourceId">No meaning</param>
        /// <param name="ErrorString">None</param>
        /// <returns>Success value in all cases</returns>
        public bool DeleteApplicationResource(int ApplicationId, int ResourceId, out string ErrorString) {
            ErrorString = "";
            return true;

        }

        /// <summary>
        /// No contextual meaning in terms of LDAP. Method has no implementation.
        /// </summary>
        /// <param name="ResourceId">No meaning</param>
        /// <param name="ErrorString">None</param>
        /// <returns>Success value in all cases</returns>
        public bool DeleteResource(int ResourceId, out string ErrorString) {
            ErrorString = "";
            return true;

        }

        /// <summary>
        /// No contextual meaning in terms of LDAP. Method has no implementation.
        /// </summary>
        /// <param name="ResourceId">No meaning</param>
        /// <param name="ActionId">No meaning</param>
        /// <param name="ErrorString">None</param>
        /// <returns>Success value in all cases</returns>
        public bool DeleteResourceAction(int ResourceId, int ActionId, out string ErrorString) {
            ErrorString = "";
            return true;

        }

        /// <summary>
        /// No contextual meaning in terms of LDAP. Method has no implementation.
        /// </summary>
        /// <param name="RoleId">No meaning</param>
        /// <param name="ErrorString">None</param>
        /// <returns>Success value in all cases</returns>
        public bool DeleteRole(int RoleId, out string ErrorString) {
            ErrorString = "";
            return true;
        }

        /// <summary>
        /// No contextual meaning in terms of LDAP. Method has no implementation.
        /// </summary>
        /// <param name="UserId">No meaning</param>
        /// <param name="ErrorString">None</param>
        /// <returns>Success value in all cases</returns>
        public bool DeleteUser(string UserId, out string ErrorString, out User oUser) {
            ErrorString = "";
            oUser = null;
            return true;
        }

        /// <summary>
        /// No contextual meaning in terms of LDAP. Method has no implementation.
        /// </summary>
        /// <param name="UserId">No meaning</param>
        /// <param name="RoleId">No meaning</param>
        /// <param name="ErrorString">None</param>
        /// <returns>Success value in all cases</returns>
        public bool DeleteUserRole(string UserId, int RoleId, out string ErrorString) {
            ErrorString = "";
            return true;
        }

        #endregion

        #region Get Methods

        /// <summary>
        /// No contextual meaning in terms of LDAP. Method has no implementation
        /// </summary>
        /// <param name="UserName"></param>
        /// <param name="EmailAddress"></param>
        /// <param name="ErrorString"></param>
        /// <returns></returns>
        //6691 starts
        public User GetUserWithEmail(string UserName, out string ErrorString) {
            ErrorString = "";
            return null;
        }
        //6691 ends
        /// <summary>
        /// No contextual meaning in terms of LDAP. Method has no implementation.
        /// </summary>
        /// <param name="roles">No meaning</param>
        /// <param name="ErrorString">None</param>
        /// <returns>The value Null (Nothing in Visual Basic) in all cases</returns>
        public AccessRight[] GetEffectiveAccessRights(Role[] roles, out string ErrorString) {
            ErrorString = "";
            return null;
        }


        /// <summary>
        /// No contextual meaning in terms of LDAP. Method has no implementation.
        /// </summary>
        /// <param name="ErrorString">None</param>
        /// <returns>The value Null (Nothing in Visual Basic) in all cases</returns>
        public string[] GetUserTypes(out string ErrorString) {
            ErrorString = "";
            return null;
        }


        /// <summary>
        /// No contextual meaning in terms of LDAP. Method has no implementation.
        /// </summary>
        /// <param name="ErrorString">None</param>
        /// <returns>The value Null (Nothing in Visual Basic) in all cases</returns>
        //6668 starts
        //public UserStatus[] GetStatusCodes(out string ErrorString)
        //{
        //    ErrorString = "";
        //    return null;
        //}
        //6668 ends

        /// <summary>
        /// No contextual meaning in terms of LDAP. Method has no implementation.
        /// </summary>
        /// <param name="ActionId">No meaning</param>
        /// <param name="ErrorString">None</param>
        /// <returns>The value Null (Nothing in Visual Basic) in all cases</returns>
        /// //6732 starts
        /*public Action GetAction(int ActionId,out string ErrorString)
        {
            ErrorString = "";
            return null;

        }*/
        //6732 ends

        /// <summary>
        /// No contextual meaning in terms of LDAP. Method has no implementation.
        /// </summary>
        /// <param name="ErrorString">None</param>
        /// <returns>The value Null (Nothing in Visual Basic) in all cases</returns>
        //6732 starts
        /*public Action[] GetActions(out string ErrorString)
		{
			ErrorString = "";
			return null;

		}*/
        //6732 ends

        /// <summary>
        /// No contextual meaning in terms of LDAP. Method has no implementation.
        /// </summary>
        /// <param name="ApplicationId">No meaning</param>
        /// <param name="ErrorString">None</param>
        /// <returns>The value Null (Nothing in Visual Basic) in all cases</returns>
        public Application GetApplication(int ApplicationId, out string ErrorString) {
            ErrorString = "";
            return null;

        }


        /// <summary>
        /// No contextual meaning in terms of LDAP. Method has no implementation.
        /// </summary>
        /// <param name="ErrorString">None</param>
        /// <returns>The value Null (Nothing in Visual Basic) in all cases</returns>
        public Application[] GetApplications(out string ErrorString) {
            ErrorString = "";
            return null;

        }


        /// <summary>
        /// No contextual meaning in terms of LDAP. Method has no implementation.
        /// </summary>
        /// <param name="ApplicationId">No meaning</param>
        /// <param name="ErrorString">None</param>
        /// <returns>The value Null (Nothing in Visual Basic) in all cases</returns>
        public Resource[] GetApplicationResources(int ApplicationId, out string ErrorString) {
            ErrorString = "";
            return null;
        }


        /// <summary>
        /// No contextual meaning in terms of LDAP. Method has no implementation.
        /// </summary>
        /// <param name="ResourceId">No meaning</param>
        /// <param name="ErrorString">None</param>
        /// <returns>The value Null (Nothing in Visual Basic) in all cases</returns>
        public Resource GetResource(int ResourceId, out string ErrorString) {
            ErrorString = "";
            return null;
        }


        /// <summary>
        /// No contextual meaning in terms of LDAP. Method has no implementation.
        /// </summary>
        /// <param name="ErrorString">None</param>
        /// <returns>The value Null (Nothing in Visual Basic) in all cases</returns>
        public Functionality[] GetFunctionalities(out string ErrorString) {
            ErrorString = "";
            return null;
        }

        /// <summary>
        /// No contextual meaning in terms of LDAP. Method has no implementation.
        /// </summary>
        /// <param name="RoleID"></param>
        /// <param name="ErrorString">None</param>
        /// <returns>The value Null (Nothing in Visual Basic) in all cases</returns>
        public Functionality[] GetFunctionalities(Int32 RoleID, out string ErrorString) {
            ErrorString = "";
            return null;
        }

        /// <summary>
        /// No contextual meaning in terms of LDAP. Method has no implementation.
        /// </summary>
        /// <param name="ErrorString">None</param>
        /// <returns>The value Null (Nothing in Visual Basic) in all cases</returns>
        public Role[] GetRoles(out string ErrorString) {
            ErrorString = "";
            return null;
        }


        /// <summary>
        /// No contextual meaning in terms of LDAP. Method has no implementation.
        /// </summary>
        /// <param name="RoleId">No meaning</param>
        /// <param name="ErrorString">None</param>
        /// <returns>The value Null (Nothing in Visual Basic) in all cases</returns>
        public Role GetRoles(int RoleId, out string ErrorString) {
            ErrorString = "";
            return null;
        }


        /// <summary>
        /// No contextual meaning in terms of LDAP. Method has no implementation.
        /// </summary>
        /// <param name="UserId">No meaning</param>
        /// <param name="ErrorString">None</param>
        /// <returns>The value Null (Nothing in Visual Basic) in all cases</returns>
        public Role[] GetRoles(string UserId, out string ErrorString) {
            ErrorString = "";
            return null;
        }


        /// <summary>
        /// No contextual meaning in terms of LDAP. Method has no implementation.
        /// </summary>
        /// <param name="ParentRoleId">No meaning</param>
        /// <param name="ErrorString">None</param>
        /// <returns>The value Null (Nothing in Visual Basic) in all cases</returns>
        public Role[] GetChildRoles(int ParentRoleId, out string ErrorString) {
            ErrorString = "";
            return null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Identity"></param>
        /// <param name="ErrorString"></param>
        /// <returns></returns>
        public System.Data.DataSet GetSiamAndCustomer(string Identity, out string ErrorString) {
            ErrorString = "";
            return null;
        }

        /// <summary>
        /// No contextual meaning in terms of LDAP. Method has no implementation.
        /// </summary>
        /// <param name="Identity">No meaning</param>
        /// <param name="ErrorString">None</param>
        /// <returns>The value Null (Nothing in Visual Basic) in all cases</returns>
        public User GetUser(string Identity, out string ErrorString) {
            ErrorString = "";
            return null;
        }

        /// <summary>
        /// Invalid user list
        /// </summary>
        /// <param name="ErrorString"></param>
        /// <returns></returns>
        public object GetInvalidUsers(out string ErrorString) {
            ErrorString = "";
            return null;
        }

        public string[] GetUserPinValues(string LDAPID) {
            TimeSpan start = TimeKeeper.GetCurrentTime();
            //Logger.RaiseTrace(ClassName,"Entry:"+start.ToString(),TraceLevel.Verbose);
            Sony.US.siam.User._thirdIDstructure idStruct = new Sony.US.siam.User._thirdIDstructure();
            idStruct.UserSecurityPINs = new string[3];
            if (LDAPID != null) {
                try {
                    WinLDAP.WinLDAPResult r = _rldap.Bind(_bindobject, _password, 0);
                    if (r.ReturnCode != 0) {
                        throw new Exception(Messages.LDAP_ADMIN_BIND_FAILURE.ToString() + " " + r.ReturnMessage);
                    }
                    WinLDAP.WinLDAPSearchResults results = _rldap.Search(_userDN, WinLDAP.SearchScope.LDAP_SCOPE_SUBTREE, "(uid=" + LDAPID + ")");
                    if (results.ReturnCode == 0) {
                        if (results.LDAPEntries.Length > 0) {
                            idStruct.UserSecurityPINs[0] = ((System.Collections.ArrayList)results.LDAPEntries[0].Attributes["AMERICASONYB2BPIN1"])[0].ToString();
                            idStruct.UserSecurityPINs[1] = ((System.Collections.ArrayList)results.LDAPEntries[0].Attributes["AMERICASONYB2BPIN2"])[0].ToString();
                            idStruct.UserSecurityPINs[2] = ((System.Collections.ArrayList)results.LDAPEntries[0].Attributes["AMERICASONYB2BPIN3"])[0].ToString();
                        }
                    } else {
                        throw new Exception(Messages.LDAP_BIND_SEARCH_FAILURE.ToString() + " " + results.Message);
                    }
                } catch (Exception ex) {
                    string ErrorString = "LDAP Error";
                    HandleError(ex, out ErrorString);
                }
                _rldap.UnBind();
            }
            TimeSpan end = TimeKeeper.GetCurrentTime();
            TimeSpan exectime = start.Subtract(end);
            //Logger.RaiseTrace(ClassName,"Exit:ExecutionTime="+exectime.ToString(),TraceLevel.Verbose);
            return idStruct.UserSecurityPINs;
        }

        /// <summary>
        /// Sony specific implementation of GetUser using credentials. It obtains User data that is 
        /// specifically stored in the LDAP backend systems. 
        /// </summary>
        /// <param name="result">The instance of <see cref="AuthenticationResult"/></param>
        /// <param name="ErrorString">A string value indicating any LDAP error conditions.</param>
        /// <returns>An instance of sony.us.siam.User containing the LDAP specific data. All other
        /// instance members of the User object will be null.</returns>
        public void GetUser(AuthenticationResult result, out string ErrorString) {
            TimeSpan start = TimeKeeper.GetCurrentTime();
            //Logger.RaiseTrace(ClassName,"Entry:"+start.ToString(),TraceLevel.Verbose);
            ErrorString = "";
            if (result.user.ThirdID.UserSecurityPINs == null) {
                result.user.ThirdID.UserSecurityPINs = new string[3];
            }
            try {
                WinLDAP.WinLDAPResult r = _rldap.Bind(_bindobject, _password, 0);
                if (r.ReturnCode != 0) {
                    throw new Exception(Messages.LDAP_ADMIN_BIND_FAILURE.ToString() + " " + r.ReturnMessage);
                }
                WinLDAP.WinLDAPSearchResults results = _rldap.Search(_userDN, WinLDAP.SearchScope.LDAP_SCOPE_ONELEVEL, "(uid=" + result.user.AlternateUserId + ")");
                if (results.ReturnCode == 0) {
                    if (results.LDAPEntries.Length > 0) {
                        result.user.ThirdID.UserSecurityPINs[0] = ((System.Collections.ArrayList)results.LDAPEntries[0].Attributes["AMERICASONYB2BPIN1"])[0].ToString();
                        result.user.ThirdID.UserSecurityPINs[1] = ((System.Collections.ArrayList)results.LDAPEntries[0].Attributes["AMERICASONYB2BPIN2"])[0].ToString();
                        result.user.ThirdID.UserSecurityPINs[2] = ((System.Collections.ArrayList)results.LDAPEntries[0].Attributes["AMERICASONYB2BPIN3"])[0].ToString();
                    }
                } else {
                    throw new Exception(Messages.LDAP_BIND_SEARCH_FAILURE.ToString() + " " + results.Message);
                }
            } catch (Exception ex) {
                HandleError(ex, out ErrorString);
                result.Result = false;
                result.SiamResponseCode = (int)Messages.LDAP_AUTHENTICATE_FAILURE;
                result.SiamResponseMessage = Messages.LDAP_AUTHENTICATE_FAILURE.ToString() + ex.Message;
            }
            _rldap.UnBind();
            TimeSpan end = TimeKeeper.GetCurrentTime();
            TimeSpan exectime = start.Subtract(end);
            //Logger.RaiseTrace(ClassName,"Exit:ExecutionTime="+exectime.ToString(),TraceLevel.Verbose);
            return;
        }


        /// <summary>
        /// No contextual meaning in terms of LDAP. Method has no implementation.
        /// </summary>
        /// <param name="deep">No meaning</param>
        /// <param name="ErrorString">None</param>
        /// <returns>The value Null (Nothing in Visual Basic) in all cases</returns>
        public object[] GetUsers(bool deep, out string ErrorString) {
            ErrorString = "";
            return null;
        }

        /// <summary>
        /// No contextual meaning in terms of LDAP. Method has no implementation.
        /// </summary>
        /// <param name="SearchPattern">No meaning</param>
        /// <param name="deep">No meaning</param>
        /// <param name="SPSSearch">A bool value saying SPS Search</param>
        /// <param name="ErrorString">None</param>
        /// <returns>The value Null (Nothing in Visual Basic) in all cases</returns>
        public object[] GetUsers(string SearchPattern, bool deep, bool SPSSearch, out string ErrorString) {
            ErrorString = "";
            return null;
        }

        /// <summary>
        /// No contextual meaning in terms of LDAP. Method has no implementation.
        /// </summary>
        /// <param name="SearchPattern">No meaning</param>
        /// <param name="deep">No meaning</param>
        /// <param name="ErrorString">None</param>
        /// <returns>The value Null (Nothing in Visual Basic) in all cases</returns>
        public object[] GetUsers(string SearchPattern, bool deep, out string ErrorString) {
            ErrorString = "";
            return null;
        }


        /// <summary>
        /// No contextual meaning in terms of LDAP. Method has no implementation.
        /// </summary>
        /// <param name="RoleId">No meaning</param>
        /// <param name="ErrorString">None</param>
        /// <returns>The value Null (Nothing in Visual Basic) in all cases</returns>
        /// Commented By Rajkumar A on 07-01-2010. Stored Procedure is not used in the application
        //public object[] GetUsers(int RoleId,out string ErrorString)
        //{
        //    ErrorString = "";
        //    return null;
        //}


        /// <summary>
        /// No contextual meaning in terms of LDAP. Method has no implementation.
        /// </summary>
        /// <param name="RoleId">No meaning</param>
        /// <param name="ErrorString">None</param>
        /// <returns>The value Null (Nothing in Visual Basic) in all cases</returns>
        public AccessRight[] GetAccessRights(int RoleId, out string ErrorString) {
            ErrorString = "";
            return null;
        }

        #endregion

        #region Update Methods

        /// <summary>
        /// No contextual meaning in terms of LDAP. Method has no implementation.
        /// </summary>
        /// <param name="ActionName">No meaning</param>
        /// <param name="ActionId">No meaning</param>
        /// <param name="ErrorString">None</param>
        /// <returns>The boolean value True in all cases</returns>
        /// //6732 starts
        /*public bool UpdateAction(string ActionName, int ActionId,out string ErrorString)
        {
            ErrorString = "";
            return true;
        }*/
        //6732 ends

        /// <summary>
        /// No contextual meaning in terms of LDAP. Method has no implementation.
        /// </summary>
        /// <param name="ApplicationName">No meaning</param>
        /// <param name="ApplicationId">No meaning</param>
        /// <param name="ErrorString">None</param>
        /// <returns>The boolean value True in all cases</returns>
        public bool UpdateApplication(string ApplicationName, int ApplicationId, out string ErrorString) {
            ErrorString = "";
            return true;
        }


        /// <summary>
        /// No contextual meaning in terms of LDAP. Method has no implementation.
        /// </summary>
        /// <param name="ResourceName">No meaning</param>
        /// <param name="ResourceId">No meaning</param>
        /// <param name="ErrorString">None</param>
        /// <returns>The boolean value True in all cases</returns>
        public bool UpdateResource(string ResourceName, int ResourceId, out string ErrorString) {
            ErrorString = "";
            return true;
        }


        /// <summary>
        /// No contextual meaning in terms of LDAP. Method has no implementation.
        /// </summary>
        /// <param name="RoleName">No meaning</param>
        /// <param name="RoleId">No meaning</param>
        /// <param name="ErrorString">None</param>
        /// <returns>The boolean value True in all cases</returns>
        public bool UpdateRole(string RoleName, int RoleId, out string ErrorString) {
            ErrorString = "";
            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="EMail"></param>
        /// <param name="ErrorString"></param>
        /// <returns></returns>
        //commented by Sunil as UpdateBULKEMail is not used on 07-01-10
        //public bool UpdateBULKEMAIL(string EMail, out string ErrorString)
        //{
        //    ErrorString = "";
        //    return true;
        //}
        /// <summary>
        /// No contextual meaning in terms of LDAP. Method has no implementation.
        /// </summary>
        /// <param name="RoleId"></param>
        /// <param name="AddFuncId"></param>
        /// <param name="DelFuncId"></param>
        /// <param name="ErrorString"></param>
        /// <returns></returns>
        public bool UpdateRoleFunction(string RoleId, string AddFuncId, string DelFuncId, out string ErrorString) {
            ErrorString = "";
            return true;
        }

        /// <summary>
        /// No contextual meaning in terms of LDAP. Method has no implementation.
        /// </summary>
        /// <param name="user">No meaning</param>
        /// <param name="ErrorString">None</param>
        /// <returns>The boolean value True in all cases</returns>
        public bool UpdateUser(User user, out string ErrorString) {
            ErrorString = "";
            return true;
        }

        #endregion

        #region Maintenence Methods

        /// <summary>
        /// 
        /// </summary>
        public void ReTryEmailFailure() {

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="MessageException"></param>
        public void LogEmailFailure(System.Net.Mail.MailMessage msg, string MessageException) {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="FirstName"></param>
        /// <param name="LastName"></param>
        /// <param name="EmailAddress"></param>
        /// <param name="newStatusName"></param>
        /// <param name="LDAPUID"></param>
        /// <param name="ErrorString"></param>
        /// <returns></returns>
        /*6668 starts
                public bool UpdateApprovalStatus(string FirstName,string LastName,string EmailAddress,string newStatusName,string LDAPUID,out string ErrorString)
                {
                    ErrorString = "";
                    return true;
                }
                //6668 ends*/
        /// <summary>
        /// 
        /// </summary>
        /// <param name="user"></param>
        /// <param name="NewCredentials"></param>
        /// <param name="ExpiresInDays"></param>
        /// <param name="ErrorString"></param>
        /// <returns></returns>
        public bool UpdateCredentials(User user, Credential NewCredentials, int ExpiresInDays, out string ErrorString) {
            TimeSpan start = TimeKeeper.GetCurrentTime();
            //Logger.RaiseTrace(ClassName,"Entry:"+start.ToString(),TraceLevel.Verbose);
            ErrorString = "";
            bool retVal = false;
            try {
                if (NewCredentials.Password != null && NewCredentials.Password != "") {
                    _wldap.Bind(_bindobject, _password, 0);
                    WinLDAP.WinLDAPSearchResults sresults = _wldap.Search(_userDN, WinLDAP.SearchScope.LDAP_SCOPE_SUBTREE, "(uid=" + user.AlternateUserId + ")");
                    if (sresults.LDAPEntries == null || sresults.LDAPEntries.Length != 1) {
                        retVal = false;
                        ErrorString = sresults.Message;
                    } else {
                        string userDN = sresults.LDAPEntries[0].DistinguishedName;
                        WinLDAP.WinLDAPResult r = _wldap.Modify(userDN, "USERPASSWORD", NewCredentials.Password);
                        if (r.ReturnCode == 0) {
                            retVal = true;
                        } else {
                            ErrorString = r.ReturnMessage;
                            retVal = false;
                        }
                        //System.DateTime today = System.DateTime.Now;
                        //today = today.AddDays((double)ExpiresInDays);
                        //r = _ldap.Modify(userDN,"AMERICASONYB2BPASSWORDNOTIFICATIONDATE", today.ToString("MM/dd/yyyy"));
                    }
                    _wldap.UnBind();
                }
            } catch (Exception ex) {
                HandleError(ex, out ErrorString);
            }
            TimeSpan end = TimeKeeper.GetCurrentTime();
            TimeSpan exectime = start.Subtract(end);
            //Logger.RaiseTrace(ClassName,"Exit:ExecutionTime="+exectime.ToString(),TraceLevel.Verbose);
            return retVal;
        }
        /// <summary>
        /// The LockAccount method will obtain a users LDAP attributes based on inforamtion contained in the
        /// instance of sony.us.siam.User object passed in as an argument. The attribute "AMERICASONYB2BPORTALSTATUS"
        /// is set to Locked.
        /// </summary>
        /// <param name="user">An instance of sony.us.siam.User</param>
        /// <param name="ErrorString">A string value containing any LDAP error conditions.</param>
        /// <returns>A boolean value indicating success or failure. If failure check the value of
        /// ErrorString to obtain the LDAP Error code. For a list of valid LDAP error codes see
        /// http://msdn.microsoft.com/library/default.asp?url=/library/en-us/netdir/ldap/return_values.asp</returns>
        public bool LockAccount(User user, out string ErrorString) {
            TimeSpan start = TimeKeeper.GetCurrentTime();
            //Logger.RaiseTrace(ClassName,"Entry:"+start.ToString(),TraceLevel.Verbose);
            ErrorString = "";
            bool retVal = false;
            try {
                _wldap.Bind(_bindobject, _password, 0);

                //gotta find the guy full DN first - bs 03/24/2004
                WinLDAP.WinLDAPSearchResults sresults = _wldap.Search(_userDN, WinLDAP.SearchScope.LDAP_SCOPE_SUBTREE, "(uid=" + user.AlternateUserId + ")");

                if (sresults.LDAPEntries == null || sresults.LDAPEntries.Length != 1) {
                    retVal = false;
                    ErrorString = sresults.Message;
                } else {
                    string userDN = sresults.LDAPEntries[0].DistinguishedName;

                    //					WinLDAP.WinLDAPResult r = _ldap.Modify("uid="+user.AlternateUserId+","+ _userDN,"AMERICASONYB2BPORTALSTATUS","Lock");
                    WinLDAP.WinLDAPResult r = _wldap.Modify(userDN, "AMERICASONYB2BPORTALSTATUS", "Lock");

                    if (r.ReturnCode == 0) {
                        retVal = true;
                    } else {
                        ErrorString = r.ReturnMessage;
                        retVal = false;
                    }
                }

                _wldap.UnBind();
            } catch (Exception ex) {
                HandleError(ex, out ErrorString);
            }
            TimeSpan end = TimeKeeper.GetCurrentTime();
            TimeSpan exectime = start.Subtract(end);
            //Logger.RaiseTrace(ClassName,"Exit:ExecutionTime="+exectime.ToString(),TraceLevel.Verbose);
            return retVal;
        }


        /// <summary>
        /// UnLockAccount obtains the Users attributes from LDAP using the instance of sony.us.siam.User
        /// passed in as an argument. The attribute "AMERICASONYB2BPORTALSTATUS" is set to UnLocked.
        /// </summary>
        /// <param name="user">An instance of sony.us.siam.User</param>
        /// <param name="ErrorString">A string value containing any LDAP error conditions.</param>
        /// <returns>A boolean value indicating success or failure. If failure check ErrorString
        /// to obtain the LDAP Error String returned. For a list of valid LDAP Error Conditions and
        /// codes see http://msdn.microsoft.com/library/default.asp?url=/library/en-us/netdir/ldap/return_values.asp</returns>
        public bool UnLockAccount(User user, out string ErrorString) {
            TimeSpan start = TimeKeeper.GetCurrentTime();
            //Logger.RaiseTrace(ClassName,"Entry:"+start.ToString(),TraceLevel.Verbose);
            ErrorString = "";
            bool retVal = false;
            try {
                _wldap.Bind(_bindobject, _password, 0);

                //gotta find the guy full DN first - bs 03/24/2004
                WinLDAP.WinLDAPSearchResults sresults = _wldap.Search(_userDN, WinLDAP.SearchScope.LDAP_SCOPE_SUBTREE, "(uid=" + user.AlternateUserId + ")");

                if (sresults.LDAPEntries == null || sresults.LDAPEntries.Length != 1) {
                    retVal = false;
                    ErrorString = sresults.Message;
                } else {
                    string userDN = sresults.LDAPEntries[0].DistinguishedName;

                    //					WinLDAP.WinLDAPResult r = _ldap.Modify("uid="+user.AlternateUserId+","+ _userDN,"AMERICASONYB2BPORTALSTATUS","Active");
                    WinLDAP.WinLDAPResult r = _wldap.Modify(userDN, "AMERICASONYB2BPORTALSTATUS", "Active");

                    if (r.ReturnCode == 0) {
                        retVal = true;
                    } else {
                        ErrorString = r.ReturnMessage;
                        retVal = false;
                    }
                }

                _wldap.UnBind();
            } catch (Exception ex) {
                HandleError(ex, out ErrorString);
            }

            TimeSpan end = TimeKeeper.GetCurrentTime();
            TimeSpan exectime = start.Subtract(end);
            //Logger.RaiseTrace(ClassName,"Exit:ExecutionTime="+exectime.ToString(),TraceLevel.Verbose);
            return retVal;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="newVal"></param>
        /// <param name="ErrorString"></param>
        /// <returns></returns>
        public bool UpdateNextQuestionIndex(string id, int newVal, out string ErrorString) {
            ErrorString = "";
            return true;

        }

        #endregion

        #region Private Methods

        private void HandleError(Exception ex, out string ErrorString) {
            System.Diagnostics.StackTrace st = new StackTrace(ex);
            System.Diagnostics.StackFrame sf = st.GetFrame(st.FrameCount - 1);
            System.Reflection.MethodBase mb = sf.GetMethod();
            string MemberName = ClassName + ":" + mb.Name;
            ErrorString = "ERROR:" + MemberName + " has thrown an exception. Exception Message=" + ex.Message;
            //Logger.RaiseTrace(ClassName,ErrorString,TraceLevel.Error);
        }


        #endregion
    }
}
