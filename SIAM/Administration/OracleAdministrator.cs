using System;
using System.Collections;
using System.Data;
using System.Diagnostics;
using Oracle.ManagedDataAccess.Client;
using Oracle.ManagedDataAccess.Types;
using Sony.US.siam.Interfaces;
using Sony.US.SIAMUtilities;

namespace Sony.US.siam.Administration {


    /// <summary>
    /// OracleAdministrator is the data access layer for SIAM that sit on top of an Oracle
    /// Backend.
    /// NOTE: This class makes use of the .Net Framework 1.1 Oracle client class
    /// library. If your not using 1.1 of the framework then the Add On oracle
    /// client Oracle.ManagedDataAccess.Client can be installed from Microsoft's web site.
    /// This download is only required for versions of the framework older than 1.1
    /// In Addition the Oracle 9i client must be installed on the computer running this
    /// assembly (most likely a server).
    /// NOTE: One might be tempted to Open the connection in the constructor and leave it open.
    /// This class was specifically designed NOT to do that. The connection to the database is 
    /// opened just prior to it being used and is closed immediately thereafter. The connection
    /// string is not defined by this class but rather passed in as a constructor aurgument from
    /// configuration. This design will facilitate connection pooling which is turned on by the
    /// Oracle Data Provider by default. Other connection pooling configuration options can be
    /// controlled from the connection string passed in from configuration.
    /// NOTE: This class must inherit from MarshalByRefObject. DO NOT CHANGE THIS. It will break the
    /// reflection code that instantiates this class at runtime. The SERIALIZABLE attribute and the
    /// inherit marshalbyrefobject are probably redundant.
    /// NOTE: This class is designed to consume results from the database in XML. The Oracle data base in
    /// use by this class (Defined by the connection string in configuration) must have the Oracle XML
    /// extensions installed.
    /// </summary>
    [Serializable]
    internal class OracleAdministrator : System.MarshalByRefObject, ISecurityAdministrator {

        #region Class Variables

        /// <summary>
        /// 
        /// </summary>
        //private SonyLogger Logger = new SonyLogger();

        /// <summary>
        /// The oracle connection object
        /// </summary>
        private OracleConnection cn;

        /// <summary>
        /// A constant value used by Logging to capture the name of this class.
        /// </summary>
        private const string ClassName = "OracleAdministrator";

        //Added By Chandra on July 09 07
        private bool mSPSSearch = false;
        private string mConnectionStringSPS = string.Empty;

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor. Connection String is required to instantiate. The constructor gets an instance
        /// of the Oracle Connection object and sets it's connection string value, but does NOT open the
        /// connection.
        /// </summary>
        /// <param name="ConnectionString">A string value containing the connection string. This value
        /// is defined in configuration.</param>
        internal OracleAdministrator(string ConnectionString, string ConnectionStringSPS) {
            mConnectionStringSPS = ConnectionStringSPS;
            // Log Data
            TimeSpan start = TimeKeeper.GetCurrentTime();
            //Logger.RaiseTrace(ClassName,"Entry:"+start.ToString(),TraceLevel.Verbose);
            cn = new OracleConnection();
            cn.ConnectionString = ConnectionString;
            // Log Data
            TimeSpan end = TimeKeeper.GetCurrentTime();
            TimeSpan exectime = start.Subtract(end);
            //Logger.RaiseTrace(ClassName,"Exit:ExecutionTime="+exectime.ToString(),TraceLevel.Verbose);

        }

        #endregion

        #region Public Implementation of ISecurityAdministrator


        #region Add methods


        /// <summary>
        /// AddUserRole adds a defined SIAM Role to the users Role collection. This has the effect of
        /// assigning that User the role in question and giving that user all the rights associated with
        /// that role.
        /// </summary>
        /// <param name="UserId">A string value containing the users SIAM Identity.</param>
        /// <param name="RoleId">An integer value containing the role id to add.</param>
        /// <param name="ErrorString">A string value containing any error condition.</param>
        /// <returns>a double value containing the data base record id.</returns>
        public double AddUserRole(string UserId, int RoleId, out string ErrorString) {
            TimeSpan start = TimeKeeper.GetCurrentTime();
            //Logger.RaiseTrace(ClassName,"Entry:"+start.ToString(),TraceLevel.Verbose);
            ErrorString = "";
            double retval;
            try {
                OracleCommand cmd = new OracleCommand();
                cmd.CommandText = "addUserRole";
                cmd.Parameters.Add("xUserId", OracleDbType.Varchar2, 50);
                cmd.Parameters["xUserId"].Value = UserId;
                cmd.Parameters.Add("xRoleId", OracleDbType.Decimal, 22);
                cmd.Parameters["xRoleId"].Value = RoleId;
                retval = ExecuteCMD(cmd, out ErrorString);
            } catch (Exception ex) {
                HandleError(ex, out ErrorString);
                retval = -1;
            }
            TimeSpan end = TimeKeeper.GetCurrentTime();
            TimeSpan exectime = start.Subtract(end);
            //Logger.RaiseTrace(ClassName,"Exit:ExecutionTime="+exectime.ToString(),TraceLevel.Verbose);
            return retval;
        }

        /// <summary>
        /// Add Resource. Adds a resource to SIAM. Adding a resource to SIAM's back end is actually
        /// a two step process. The resource must be added using this method first to obtain it's data base
        /// record id. It is then associated with an Application using AddApplicationResource. On the public
        /// interface for SIAM administration this two step process is handled automatically.
        /// </summary>
        /// <param name="ResourceName">A string value containing the name of the resource to add.</param>
        /// <param name="ErrorString">A string value containing any error condition.</param>
        /// <returns>A double value containing the record id.</returns>
        public double AddResource(string ResourceName, out string ErrorString) {
            TimeSpan start = TimeKeeper.GetCurrentTime();
            //Logger.RaiseTrace(ClassName,"Entry:"+start.ToString(),TraceLevel.Verbose);
            ErrorString = "";
            double retval;
            try {
                OracleCommand cmd = new OracleCommand();
                cmd.CommandText = "addResource";
                cmd.Parameters.Add("xResourceName", OracleDbType.Varchar2, 50);
                cmd.Parameters["xResourceName"].Value = ResourceName;
                retval = ExecuteCMD(cmd, out ErrorString);
            } catch (Exception ex) {
                HandleError(ex, out ErrorString);
                retval = -1;
            }
            TimeSpan end = TimeKeeper.GetCurrentTime();
            TimeSpan exectime = start.Subtract(end);
            //Logger.RaiseTrace(ClassName,"Exit:ExecutionTime="+exectime.ToString(),TraceLevel.Verbose);
            return retval;
        }



        /// <summary>
        /// Add Role add a role to SIAM. 
        /// </summary>
        /// <param name="RoleName">The name of the role being added</param>
        /// <param name="ParentRoleId">The ID of the parent role. -1 if no parent role is defined.</param>
        /// <param name="ErrorString">A string value containing any error condition</param>
        /// <returns>A double value containing the data base record id.</returns>
        public double AddRole(string RoleName, int ParentRoleId, out string ErrorString) {
            TimeSpan start = TimeKeeper.GetCurrentTime();
            //Logger.RaiseTrace(ClassName,"Entry:"+start.ToString(),TraceLevel.Verbose);
            ErrorString = "";
            double retval;
            try {
                OracleCommand cmd = new OracleCommand();
                cmd.CommandText = "addRole";
                cmd.Parameters.Add("xRoleName", OracleDbType.Varchar2, 50);
                cmd.Parameters["xRoleName"].Value = RoleName;
                if (ParentRoleId == -1) {
                    cmd.Parameters.Add("xParentId", OracleDbType.Decimal, 22);
                    cmd.Parameters["xParentId"].Value = System.DBNull.Value;
                } else {
                    cmd.Parameters.Add("xParentId", OracleDbType.Decimal, 22);
                    cmd.Parameters["xParentId"].Value = ParentRoleId;
                }
                retval = ExecuteCMD(cmd, out ErrorString);
            } catch (Exception ex) {
                HandleError(ex, out ErrorString);
                retval = -1;
            }
            TimeSpan end = TimeKeeper.GetCurrentTime();
            TimeSpan exectime = start.Subtract(end);
            //Logger.RaiseTrace(ClassName,"Exit:ExecutionTime="+exectime.ToString(),TraceLevel.Verbose);
            return retval;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="user"></param>
        /// <param name="acctInfo"></param>
        /// <param name="RequestingApplication"></param>
        /// <param name="RequestingUserID"></param>
        /// <param name="RequestingPassword"></param>
        /// <param name="ErrorString"></param>
        /// <returns></returns>
        public string AddTestUser(User user, AccountInfoVO acctInfo, string RequestingApplication, string RequestingUserID, string RequestingPassword, out string ErrorString) {
            ErrorString = "";
            return ErrorString;
        }


        /// <summary>
        /// AddUser. Adds a user to SIAM.
        /// </summary>
        /// <param name="User">An instance of sony.us.siam.User containing the details</param>
        /// <param name="ErrorString">A string value containing any error information.</param>
        /// <returns>A string value containing the users SIAM Identity.</returns>

        public string AddUser(siam.User User, out string ErrorString) {
            //AddUser Method is not getting used
            //TimeSpan start = TimeKeeper.GetCurrentTime();
            ////Logger.RaiseTrace(ClassName, "Entry:" + start.ToString(), TraceLevel.Verbose);
            ErrorString = "";
            //try
            //{
            //    // Gen a new user Id.
            //    OracleCommand cmd = new OracleCommand();
            //    cmd.CommandText = "GetNextUserId";
            //    double seedValue = ExecuteCMD(cmd, out ErrorString);
            //    User.Identity = GUID.CreateGUID("10", seedValue.ToString());
            //    cmd.Parameters.Clear();
            //    cmd.CommandText = "addUser";
            //    cmd.Parameters.Add("xUserId", OracleDbType.Varchar2, 16);
            //    cmd.Parameters["xUserId"].Value = User.Identity;
            //    cmd.Parameters.Add("xStatusId", OracleDbType.Int32, 22);
            //    cmd.Parameters["xStatusId"].Value = User.UserStatus;
            //    cmd.Parameters.Add("xFirstName", OracleDbType.Varchar2, 50);
            //    cmd.Parameters["xFirstName"].Value = User.FirstName;
            //    cmd.Parameters.Add("xLastName", OracleDbType.Varchar2, 50);
            //    cmd.Parameters["xLastName"].Value = User.LastName;
            //    cmd.Parameters.Add("xCompanyName", OracleDbType.Varchar2, 50);
            //    cmd.Parameters["xCompanyName"].Value = User.CompanyName;
            //    cmd.Parameters.Add("xAddress1", OracleDbType.Varchar2, 50);
            //    cmd.Parameters["xAddress1"].Value = User.AddressLine1;
            //    cmd.Parameters.Add("xAddress2", OracleDbType.Varchar2, 50);
            //    cmd.Parameters["xAddress2"].Value = User.AddressLine2;
            //    cmd.Parameters.Add("xCity", OracleDbType.Varchar2, 50);
            //    cmd.Parameters["xCity"].Value = User.City;
            //    cmd.Parameters.Add("xState", OracleDbType.Varchar2, 50);
            //    cmd.Parameters["xState"].Value = User.State;
            //    cmd.Parameters.Add("xZip", OracleDbType.Varchar2, 50);
            //    cmd.Parameters["xZip"].Value = User.Zip;
            //    cmd.Parameters.Add("xFax", OracleDbType.Varchar2, 16);
            //    cmd.Parameters["xFax"].Value = User.Fax;
            //    cmd.Parameters.Add("xPhone", OracleDbType.Varchar2, 16);
            //    cmd.Parameters["xPhone"].Value = User.Phone;
            //    cmd.Parameters.Add("xEmailAddress", OracleDbType.Varchar2, 50);
            //    cmd.Parameters["xEmailAddress"].Value = User.EmailAddress;
            //    cmd.Parameters.Add("xUserName", OracleDbType.Varchar2, 50);
            //    cmd.Parameters["xUserName"].Value = User.UserId;
            //    cmd.Parameters.Add("xPassword", OracleDbType.Varchar2, 50);
            //    //cmd.Parameters["xPassword"].Value = Encryption.Encrypt(User.Password);
            //    cmd.Parameters["xPassword"].Value = Encryption.Encrypt(User.Password, "test key");
            //    cmd.Parameters.Add("xUserType", OracleDbType.Varchar2, 15);
            //    cmd.Parameters["xUserType"].Value = User.UserType;
            //    cmd.Parameters.Add("xPasswordExpiration", OracleDbType.Varchar2, 50);
            //    cmd.Parameters["xPasswordExpiration"].Value = User.PasswordExpirationDate;
            //    ExecuteCMD(cmd, out ErrorString);
            //    //{
            //    //    throw new ApplicationException("User name already exist, please select a different user name");
            //    //}
            //}
            //catch (System.Exception ex)
            //{
            //    HandleError(ex, out ErrorString);
            //    return "";
            //}
            //TimeSpan end = TimeKeeper.GetCurrentTime();
            //TimeSpan exectime = start.Subtract(end);
            ////Logger.RaiseTrace(ClassName, "Exit:ExecutionTime=" + exectime.ToString(), TraceLevel.Verbose);
            //return User.Identity;
            return "";
        }


        /// <summary>
        /// AddAction add an action to SIAM. 
        /// </summary>
        /// <param name="ActionName">The name of the action being added.</param>
        /// <param name="ErrorString">A string value containing any error information</param>
        /// <returns>A double value containing the data base record id</returns>
        //6732 starts
        //public double AddAction(string ActionName,out string ErrorString)
        //{
        //    // Log Data
        //    TimeSpan start = TimeKeeper.GetCurrentTime();
        //    //Logger.RaiseTrace(ClassName,"Entry:"+start.ToString(),TraceLevel.Verbose);
        //    ErrorString = "";
        //    double retval;
        //    try
        //    {
        //        OracleCommand cmd = new OracleCommand();
        //        cmd.CommandText = "addAction";
        //        cmd.Parameters.Add("xActionName",OracleDbType.Varchar2,50);
        //        cmd.Parameters["xActionName"].Value = ActionName;
        //        retval = ExecuteCMD(cmd,out ErrorString);
        //    }
        //    catch(System.Exception ex)
        //    {
        //        HandleError(ex,out ErrorString);
        //        retval = -1;
        //    }
        //    TimeSpan end = TimeKeeper.GetCurrentTime();
        //    TimeSpan exectime = start.Subtract(end);
        //    //Logger.RaiseTrace(ClassName,"Exit:ExecutionTime="+exectime.ToString(),TraceLevel.Verbose);
        //    return retval;
        //}
        //6732 ends
        /// <summary>
        /// Add Application. Adds an application to SIAM
        /// </summary>
        /// <param name="ApplicationName">The name of the application being added.</param>
        /// <param name="ErrorString">A string value representing any error condition.</param>
        /// <returns>a double value containing the data base id.</returns>
        public double AddApplication(string ApplicationName, out string ErrorString) {
            // Log Data
            TimeSpan start = TimeKeeper.GetCurrentTime();
            //Logger.RaiseTrace(ClassName,"Entry:"+start.ToString(),TraceLevel.Verbose);
            ErrorString = "";
            double retval;
            try {
                OracleCommand cmd = new OracleCommand();
                cmd.CommandText = "addApplication";
                cmd.Parameters.Add("xApplicationName", OracleDbType.Varchar2, 50);
                cmd.Parameters["xApplicationName"].Value = ApplicationName;
                retval = ExecuteCMD(cmd, out ErrorString);
            } catch (Exception ex) {
                HandleError(ex, out ErrorString);
                retval = -1;
            }
            TimeSpan end = TimeKeeper.GetCurrentTime();
            TimeSpan exectime = start.Subtract(end);
            //Logger.RaiseTrace(ClassName,"Exit:ExecutionTime="+exectime.ToString(),TraceLevel.Verbose);
            return retval;
        }


        /// <summary>
        /// Add Application Resource. Adds an association in the data base between an application and a
        /// resource. This is the second of two steps required to add a resource to SIAM.
        /// </summary>
        /// <param name="ApplicationId">The application id being associated.</param>
        /// <param name="ResourceId">The resource id being associated</param>
        /// <param name="ErrorString">A string value containing any error information</param>
        /// <returns>a double value containing the data base record id.</returns>
        public double AddApplicationResource(double ApplicationId, double ResourceId, out string ErrorString) {
            // Log Data
            TimeSpan start = TimeKeeper.GetCurrentTime();
            //Logger.RaiseTrace(ClassName,"Entry:"+start.ToString(),TraceLevel.Verbose);
            ErrorString = "";
            double retval;
            try {
                OracleCommand cmd = new OracleCommand();
                cmd.CommandText = "addApplicationResource";
                cmd.Parameters.Add("xApplicationId", OracleDbType.Decimal, 22);
                cmd.Parameters["xApplicationId"].Value = ApplicationId;
                cmd.Parameters.Add("xResourceId", OracleDbType.Decimal, 22);
                cmd.Parameters["xResourceId"].Value = ResourceId;
                retval = ExecuteCMD(cmd, out ErrorString);
            } catch (Exception ex) {
                HandleError(ex, out ErrorString);
                retval = -1;
            }
            TimeSpan end = TimeKeeper.GetCurrentTime();
            TimeSpan exectime = start.Subtract(end);
            //Logger.RaiseTrace(ClassName,"Exit:ExecutionTime="+exectime.ToString(),TraceLevel.Verbose);
            return retval;
        }


        /// <summary>
        /// Add Access Right. Adds an access right to SIAM.
        /// </summary>
        /// <param name="RoleId">The role id this access right is associated with</param>
        /// <param name="ApplicationId">The application id this right is associated with</param>
        /// <param name="ResourceId">The resource id this right is associated with</param>
        /// <param name="ActionId">The action id this right is associated with</param>
        /// <param name="ErrorString">A string value containing any error information</param>
        /// <returns>a double value containing the data base id</returns>
        public double AddAccessRight(int RoleId, int ApplicationId, int ResourceId, int ActionId, out string ErrorString) {
            // Log Data
            TimeSpan start = TimeKeeper.GetCurrentTime();
            //Logger.RaiseTrace(ClassName,"Entry:"+start.ToString(),TraceLevel.Verbose);
            ErrorString = "";
            double retval;
            try {
                OracleCommand cmd = new OracleCommand();
                cmd.CommandText = "addAccessRight";
                cmd.Parameters.Add("xRoleId", OracleDbType.Decimal, 22);
                cmd.Parameters["xRoleId"].Value = RoleId;
                cmd.Parameters.Add("xApplicationId", OracleDbType.Decimal, 22);
                cmd.Parameters["xApplicationId"].Value = ApplicationId;
                cmd.Parameters.Add("xResourceId", OracleDbType.Decimal, 22);
                cmd.Parameters["xResourceId"].Value = ResourceId;
                cmd.Parameters.Add("xActionId", OracleDbType.Decimal, 22);
                cmd.Parameters["xActionId"].Value = ActionId;
                retval = ExecuteCMD(cmd, out ErrorString);
            } catch (Exception ex) {
                HandleError(ex, out ErrorString);
                retval = -1;
            }
            TimeSpan end = TimeKeeper.GetCurrentTime();
            TimeSpan exectime = start.Subtract(end);
            //Logger.RaiseTrace(ClassName,"Exit:ExecutionTime="+exectime.ToString(),TraceLevel.Verbose);
            return retval;
        }


        #endregion

        #region Update Methods


        /// <summary>
        /// UpdateAction: Updates an Action Entity in the data base.
        /// </summary>
        /// <param name="ActionName">The new Action Name</param>
        /// <param name="ActionId">The Action Id</param>
        /// <param name="ErrorString">A string value containing any error information</param>
        /// <returns>A boolean value indicating success or failure</returns>
        //6732 starts
        /*public bool UpdateAction(string ActionName, int ActionId,out string ErrorString)
		{
			TimeSpan start = TimeKeeper.GetCurrentTime();
			//Logger.RaiseTrace(ClassName,"Entry:"+start.ToString(),TraceLevel.Verbose);
			ErrorString = "";
			bool retval = false;
			try
			{
				OracleCommand cmd = new OracleCommand();
				cmd.CommandText = "UpdateAction";
				cmd.CommandType = System.Data.CommandType.StoredProcedure;
				cmd.Parameters.Add("xActionId",OracleDbType.Int32,22);
				cmd.Parameters["xActionId"].Value = ActionId;
				cmd.Parameters.Add("xActionName",OracleDbType.Varchar2,50);
				cmd.Parameters["xActionName"].Value = ActionName;
				ExecuteCMD(cmd,out ErrorString);
				retval = true;
			}
			catch(System.Exception ex)
			{
				HandleError(ex,out ErrorString);
				retval = false;
			}
			TimeSpan end = TimeKeeper.GetCurrentTime();
			TimeSpan exectime = start.Subtract(end);
			//Logger.RaiseTrace(ClassName,"Exit:ExecutionTime="+exectime.ToString(),TraceLevel.Verbose);
			return retval;
		}*/
        //6732 ends

        /// <summary>
        /// UpdateApplication. Updates an applicatoin record in the data base.
        /// </summary>
        /// <param name="ApplicationName">The new application Name</param>
        /// <param name="ApplicationId">The Id for the Application</param>
        /// <param name="ErrorString">A string value containing any error information</param>
        /// <returns>A boolean value indicating sucess or failure</returns>
        public bool UpdateApplication(string ApplicationName, int ApplicationId, out string ErrorString) {
            TimeSpan start = TimeKeeper.GetCurrentTime();
            //Logger.RaiseTrace(ClassName,"Entry:"+start.ToString(),TraceLevel.Verbose);
            ErrorString = "";
            bool retval = false;
            try {
                OracleCommand cmd = new OracleCommand();
                cmd.CommandText = "UpdateApplication";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add("xApplicationId", OracleDbType.Decimal, 22);
                cmd.Parameters["xApplicationId"].Value = ApplicationId;
                cmd.Parameters.Add("xApplicationName", OracleDbType.Varchar2, 50);
                cmd.Parameters["xApplicationName"].Value = ApplicationName;
                ExecuteCMD(cmd, out ErrorString);
                retval = true;
            } catch (Exception ex) {
                HandleError(ex, out ErrorString);
                retval = false;
            }
            TimeSpan end = TimeKeeper.GetCurrentTime();
            TimeSpan exectime = start.Subtract(end);
            //Logger.RaiseTrace(ClassName,"Exit:ExecutionTime="+exectime.ToString(),TraceLevel.Verbose);
            return retval;
        }


        /// <summary>
        /// UpdateResource: Updates a resource entity in the data base
        /// </summary>
        /// <param name="ResourceName">The new resource name</param>
        /// <param name="ResourceId">The resource Id</param>
        /// <param name="ErrorString">A string value containing any error information</param>
        /// <returns>A boolean value indicating sucess or failure</returns>
        public bool UpdateResource(string ResourceName, int ResourceId, out string ErrorString) {
            TimeSpan start = TimeKeeper.GetCurrentTime();
            //Logger.RaiseTrace(ClassName,"Entry:"+start.ToString(),TraceLevel.Verbose);
            ErrorString = "";
            bool retval = false;
            try {
                OracleCommand cmd = new OracleCommand();
                cmd.CommandText = "UpdateResource";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add("xResourceId", OracleDbType.Decimal, 22);
                cmd.Parameters["xResourceId"].Value = ResourceId;
                cmd.Parameters.Add("xResourceName", OracleDbType.Varchar2, 50);
                cmd.Parameters["xResourceName"].Value = ResourceName;
                ExecuteCMD(cmd, out ErrorString);
                retval = true;
            } catch (Exception ex) {
                HandleError(ex, out ErrorString);
                retval = false; ;
            }
            TimeSpan end = TimeKeeper.GetCurrentTime();
            TimeSpan exectime = start.Subtract(end);
            //Logger.RaiseTrace(ClassName,"Exit:ExecutionTime="+exectime.ToString(),TraceLevel.Verbose);
            return retval;
        }


        /// <summary>
        /// UpdateRole: Updates a Role entity in the data base.
        /// </summary>
        /// <param name="RoleName">The new role name</param>
        /// <param name="RoleId">The role id</param>
        /// <param name="ErrorString">A string value containing any error information</param>
        /// <returns>A boolean value indicating success or failure</returns>
        public bool UpdateRole(string RoleName, int RoleId, out string ErrorString) {
            TimeSpan start = TimeKeeper.GetCurrentTime();
            //Logger.RaiseTrace(ClassName,"Entry:"+start.ToString(),TraceLevel.Verbose);
            ErrorString = "";
            bool retval = false;
            try {
                OracleCommand cmd = new OracleCommand();
                cmd.CommandText = "UpdateRole";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add("xRoleId", OracleDbType.Decimal, 22);
                cmd.Parameters["xRoleId"].Value = RoleId;
                cmd.Parameters.Add("xRoleName", OracleDbType.Varchar2, 50);
                cmd.Parameters["xRoleName"].Value = RoleName;
                ExecuteCMD(cmd, out ErrorString);
                retval = true;
            } catch (Exception ex) {
                HandleError(ex, out ErrorString);
                retval = false;
            }
            TimeSpan end = TimeKeeper.GetCurrentTime();
            TimeSpan exectime = start.Subtract(end);
            //Logger.RaiseTrace(ClassName,"Exit:ExecutionTime="+exectime.ToString(),TraceLevel.Verbose);
            return retval;
        }


        /// <summary>
        /// Update Bulk Email after imaging data from Production
        /// </summary>
        /// <param name="EMail"></param>
        /// <param name="ErrorString"></param>
        /// <returns></returns>
        //commented by Sunil as UpdateBULKEMail is not used on 07-01-10
        //public bool UpdateBULKEMAIL(string EMail, out string ErrorString)
        //{
        //    TimeSpan start = TimeKeeper.GetCurrentTime();
        //    //Logger.RaiseTrace(ClassName, "Entry:" + start.ToString(), TraceLevel.Verbose);
        //    ErrorString = "";
        //    bool retval = false;
        //    try
        //    {
        //        OracleCommand cmd = new OracleCommand();
        //        cmd.CommandText = "UPDATEBULKEMAIL_SIAM"; //exported form SIAM database
        //        cmd.CommandType = System.Data.CommandType.StoredProcedure;
        //        cmd.Parameters.Add("xEmail", OracleDbType.Varchar2, 50);
        //        cmd.Parameters["xEmail"].Value = EMail;
        //        ExecuteCMD(cmd, out ErrorString);
        //        retval = true;
        //    }
        //    catch (System.Exception ex)
        //    {
        //        HandleError(ex, out ErrorString);
        //        retval = false;
        //    }
        //    TimeSpan end = TimeKeeper.GetCurrentTime();
        //    TimeSpan exectime = start.Subtract(end);
        //    //Logger.RaiseTrace(ClassName, "Exit:ExecutionTime=" + exectime.ToString(), TraceLevel.Verbose);
        //    return retval;
        //}


        /// <summary>
        /// UpdateRoleFunction: Updates a Role and Functionality Mapping in the data base.
        /// </summary>
        /// <param name="RoleId"></param>
        /// <param name="AddFuncId"></param>
        /// <param name="DelFuncId"></param>
        /// <param name="ErrorString"></param>
        /// <returns></returns>
        public bool UpdateRoleFunction(string RoleId, string AddFuncId, string DelFuncId, out string ErrorString) {
            TimeSpan start = TimeKeeper.GetCurrentTime();
            string[] strIDS;
            //Logger.RaiseTrace(ClassName,"Entry:"+start.ToString(),TraceLevel.Verbose);
            ErrorString = "";
            bool retval = false;
            try {
                strIDS = AddFuncId.Split(",".ToCharArray());
                foreach (string strId in strIDS) {
                    OracleCommand cmd = new OracleCommand();
                    cmd.CommandText = "UpdateRoleFunctionMapping";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.Add("xRoleId", OracleDbType.Decimal, 22);
                    cmd.Parameters["xRoleId"].Value = Convert.ToInt64(RoleId);
                    cmd.Parameters.Add("xAddFuncId", OracleDbType.Varchar2, 50);
                    cmd.Parameters["xAddFuncId"].Value = strId;
                    cmd.Parameters.Add("xDelFuncId", OracleDbType.Varchar2, 50);
                    cmd.Parameters["xDelFuncId"].Value = "0";
                    ExecuteCMD(cmd, out ErrorString);

                }
                strIDS = DelFuncId.Split(",".ToCharArray());
                foreach (string strId in strIDS) {
                    OracleCommand cmd = new OracleCommand();
                    cmd.CommandText = "UpdateRoleFunctionMapping";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.Add("xRoleId", OracleDbType.Decimal, 22);
                    cmd.Parameters["xRoleId"].Value = Convert.ToInt64(RoleId);
                    cmd.Parameters.Add("xAddFuncId", OracleDbType.Varchar2, 50);
                    cmd.Parameters["xAddFuncId"].Value = "0";
                    cmd.Parameters.Add("xDelFuncId", OracleDbType.Varchar2, 50);
                    cmd.Parameters["xDelFuncId"].Value = strId;
                    ExecuteCMD(cmd, out ErrorString);

                }
                retval = true;
            } catch (Exception ex) {
                HandleError(ex, out ErrorString);
                retval = false;
            }
            TimeSpan end = TimeKeeper.GetCurrentTime();
            TimeSpan exectime = start.Subtract(end);
            //Logger.RaiseTrace(ClassName,"Exit:ExecutionTime="+exectime.ToString(),TraceLevel.Verbose);
            return retval;
        }

        /// <summary>
        /// Updates a user record in the database. siam.User object must contain an
        /// identitiy
        /// </summary>
        /// <param name="user">The user to update</param>
        /// <param name="ErrorString">A string value containing any error information</param>
        /// <returns>a boolean value indicating success or failure.</returns>
        public bool UpdateUser(User user, out string ErrorString) {
            TimeSpan start = TimeKeeper.GetCurrentTime();
            //Logger.RaiseTrace(ClassName,"Entry:"+start.ToString(),TraceLevel.Verbose);
            ErrorString = "";
            bool retval = false;
            try {
                OracleCommand cmd = new OracleCommand();
                //cmd.CommandText = "UpdateUser";
                cmd.CommandText = "UPDATECUSTOMER";
                if (user.CustomerID != null) {
                    cmd.Parameters.Add("xCustomerID", OracleDbType.Varchar2, 20);
                    cmd.Parameters["xCustomerID"].Value = user.CustomerID;
                } else {
                    cmd.Parameters.Add("xCustomerID", OracleDbType.Varchar2, 20);
                    cmd.Parameters["xCustomerID"].Value = string.Empty;
                }
                if (user.Identity != null) {
                    cmd.Parameters.Add("xUserId", OracleDbType.Varchar2, 20);
                    cmd.Parameters["xUserId"].Value = user.Identity;
                } else {
                    cmd.Parameters.Add("xUserId", OracleDbType.Varchar2, 20);
                    cmd.Parameters["xUserId"].Value = string.Empty;
                }
                //6668 starts
                /*if (user.UserStatus != null)
                {
                    cmd.Parameters.Add("xStatusId", OracleDbType.Int32, 22);
                    cmd.Parameters["xStatusId"].Value = user.UserStatus;
                }
                else
                {
                    cmd.Parameters.Add("xStatusId", OracleDbType.Int32, 22);
                    cmd.Parameters["xStatusId"].Value = string.Empty;

                }*/
                //6668 ends
                if (user.FirstName != null) {
                    cmd.Parameters.Add("xFirstName", OracleDbType.Varchar2, 50);
                    cmd.Parameters["xFirstName"].Value = user.FirstName;
                } else {
                    cmd.Parameters.Add("xFirstName", OracleDbType.Varchar2, 50);
                    cmd.Parameters["xFirstName"].Value = string.Empty;
                }
                if (user.LastName != null) {
                    cmd.Parameters.Add("xLastName", OracleDbType.Varchar2, 50);
                    cmd.Parameters["xLastName"].Value = user.LastName;
                } else {
                    cmd.Parameters.Add("xLastName", OracleDbType.Varchar2, 50);
                    cmd.Parameters["xLastName"].Value = string.Empty;
                }
                if (user.CompanyName != null) {
                    cmd.Parameters.Add("xCompanyName", OracleDbType.Varchar2, 50);
                    cmd.Parameters["xCompanyName"].Value = user.CompanyName;
                } else {

                    cmd.Parameters.Add("xCompanyName", OracleDbType.Varchar2, 50);
                    cmd.Parameters["xCompanyName"].Value = string.Empty;
                }


                if (user.AddressLine1 != null) {
                    cmd.Parameters.Add("xAddress1", OracleDbType.Varchar2, 50);
                    cmd.Parameters["xAddress1"].Value = user.AddressLine1;
                } else {
                    cmd.Parameters.Add("xAddress1", OracleDbType.Varchar2, 50);
                    cmd.Parameters["xAddress1"].Value = string.Empty;
                }

                if (user.AddressLine2 != null) {
                    cmd.Parameters.Add("xAddress2", OracleDbType.Varchar2, 50);
                    cmd.Parameters["xAddress2"].Value = user.AddressLine2;
                } else {
                    cmd.Parameters.Add("xAddress2", OracleDbType.Varchar2, 50);
                    cmd.Parameters["xAddress2"].Value = string.Empty;
                }
                if (user.AddressLine3 != null) {
                    cmd.Parameters.Add("xAddress3", OracleDbType.Varchar2, 50);
                    cmd.Parameters["xAddress3"].Value = user.AddressLine3;
                } else {
                    cmd.Parameters.Add("xAddress3", OracleDbType.Varchar2, 50);
                    cmd.Parameters["xAddress3"].Value = string.Empty;
                }

                if (user.City != null) {
                    cmd.Parameters.Add("xCity", OracleDbType.Varchar2, 50);
                    cmd.Parameters["xCity"].Value = user.City;
                } else {

                    cmd.Parameters.Add("xCity", OracleDbType.Varchar2, 50);
                    cmd.Parameters["xCity"].Value = string.Empty;
                }

                if (user.State != null) {
                    cmd.Parameters.Add("xState", OracleDbType.Varchar2, 50);
                    cmd.Parameters["xState"].Value = user.State;
                } else {
                    cmd.Parameters.Add("xState", OracleDbType.Varchar2, 50);
                    cmd.Parameters["xState"].Value = string.Empty;
                }

                if (user.Country != null) {
                    cmd.Parameters.Add("xCountryCode", OracleDbType.Varchar2, 50);
                    cmd.Parameters["xCountryCode"].Value = user.Country;
                } else {
                    cmd.Parameters.Add("xCountryCode", OracleDbType.Varchar2, 50);
                    cmd.Parameters["xCountryCode"].Value = string.Empty;
                }

                if (user.Zip != null) {
                    cmd.Parameters.Add("xZip", OracleDbType.Varchar2, 10);
                    cmd.Parameters["xZip"].Value = user.Zip;
                } else {

                    cmd.Parameters.Add("xZip", OracleDbType.Varchar2, 10);
                    cmd.Parameters["xZip"].Value = string.Empty;
                }

                if (user.Fax != null) {
                    cmd.Parameters.Add("xFax", OracleDbType.Varchar2, 20);
                    cmd.Parameters["xFax"].Value = user.Fax;

                } else {
                    cmd.Parameters.Add("xFax", OracleDbType.Varchar2, 20);
                    cmd.Parameters["xFax"].Value = string.Empty;
                }

                if (user.Phone != null) {
                    cmd.Parameters.Add("xPhone", OracleDbType.Varchar2, 20);
                    cmd.Parameters["xPhone"].Value = user.Phone;
                } else {
                    cmd.Parameters.Add("xPhone", OracleDbType.Varchar2, 20);
                    cmd.Parameters["xPhone"].Value = string.Empty;
                }

                if (user.EmailAddress != null) {
                    cmd.Parameters.Add("xEmailAddress", OracleDbType.Varchar2, 50);
                    cmd.Parameters["xEmailAddress"].Value = user.EmailAddress;
                } else {
                    cmd.Parameters.Add("xEmailAddress", OracleDbType.Varchar2, 50);
                    cmd.Parameters["xEmailAddress"].Value = string.Empty;

                }

                if (user.UserId != null) {
                    cmd.Parameters.Add("xUserName", OracleDbType.Varchar2, 50);
                    cmd.Parameters["xUserName"].Value = user.UserId;
                } else {
                    cmd.Parameters.Add("xUserName", OracleDbType.Varchar2, 50);
                    cmd.Parameters["xUserName"].Value = string.Empty;
                }
                // cmd.Parameters.Add("xPassword", OracleDbType.Varchar2, 50);
                //cmd.Parameters["xPassword"].Value = Encryption.Encrypt(user.Password);
                // cmd.Parameters["xPassword"].Value = Encryption.Encrypt(user.Password, "test key");

                if (user.UserType != null) {
                    //cmd.Parameters.Add("xUserType", OracleDbType.Varchar2, 15);'6668
                    cmd.Parameters.Add("xUserType", OracleDbType.Char);//6668
                    cmd.Parameters["xUserType"].Value = user.UserType;
                } else {
                    //cmd.Parameters.Add("xUserType", OracleDbType.Varchar2, 15);'6668
                    cmd.Parameters.Add("xUserType", OracleDbType.Char);//6668
                    cmd.Parameters["xUserType"].Value = string.Empty;
                }
                // cmd.Parameters.Add("xPasswordExpiration", OracleDbType.Varchar2, 50);
                //cmd.Parameters["xPasswordExpiration"].Value = user.PasswordExpirationDate;
                //Commeted by prasad for 2632, Uncommented  by Naveen for 2817
                // cmd.Parameters.Add("xLockStatus", OracleDbType.Int32, 22);
                //cmd.Parameters["xLockStatus"].Value = user.LockStatus;

                cmd.Parameters.Add("xSequenceNumber", OracleDbType.Int32, 22);
                cmd.Parameters["xSequenceNumber"].Value = user.SequenceNumber;
                cmd.Parameters.Add("xEmailOK", OracleDbType.Int32, 22);
                cmd.Parameters["xEmailOK"].Value = user.EmailOK;

                if (user.Enabledocumentidnumber != null) {
                    cmd.Parameters.Add("xENABLEDOCUMENTIDNUMBER", OracleDbType.Varchar2, 50);
                    cmd.Parameters["xENABLEDOCUMENTIDNUMBER"].Value = user.Enabledocumentidnumber;
                } else {
                    cmd.Parameters.Add("xENABLEDOCUMENTIDNUMBER", OracleDbType.Varchar2, 50);
                    cmd.Parameters["xENABLEDOCUMENTIDNUMBER"].Value = string.Empty;
                }

                if (user.PhoneExtension != null) {
                    cmd.Parameters.Add("xPhoneExtension", OracleDbType.Varchar2, 10);
                    cmd.Parameters["xPhoneExtension"].Value = user.PhoneExtension;
                } else {
                    cmd.Parameters.Add("xPhoneExtension", OracleDbType.Varchar2, 10);
                    cmd.Parameters["xPhoneExtension"].Value = string.Empty;
                }

                if (user.SubscriptionID != null) {
                    cmd.Parameters.Add("xSubscriptionID", OracleDbType.Varchar2, 50);
                    cmd.Parameters["xSubscriptionID"].Value = user.SubscriptionID;
                } else {
                    cmd.Parameters.Add("xSubscriptionID", OracleDbType.Varchar2, 50);
                    cmd.Parameters["xSubscriptionID"].Value = string.Empty;
                }
                cmd.Parameters.Add("xStatusCode", OracleDbType.Int32);
                cmd.Parameters["xStatusCode"].Value = user.UserStatusCode;
                //cmd.Parameters["xSequenceNumber"].Direction = ParameterDirection.InputOutput;

                double iReturnValue = ExecuteCMD(cmd, out ErrorString);
                if (iReturnValue == 2) {
                    ErrorString = "DuplicateUser";
                    retval = false;
                } else {
                    //if (cmd.Parameters["xSequenceNumber"].Value != System.DBNull.Value)
                    //{
                    //    user.SequenceNumber = System.Convert.ToInt32(cmd.Parameters["xSequenceNumber"].Value);
                    //}
                    // 2018-09-10 - ASleight - Below code fixes an issue with Contact-Information page. "Cannot convert System.DBNull to INullable"
                    //if (!((INullable)cmd.Parameters["retVal"].Value).IsNull) {
                    if (cmd.Parameters["retVal"]?.Value != DBNull.Value) {
                        if (Convert.ToInt16(cmd.Parameters["retVal"].Value) == 1) {
                            ErrorString = "PasswordMatch";
                            retval = false;
                        } else {
                            ErrorString = string.Empty;
                            retval = true;
                        }
                    } else {
                        ErrorString = string.Empty;
                        retval = true;
                    }
                }
            } catch (Exception ex) {
                HandleError(ex, out ErrorString);
                retval = false;
            } finally {
                cn.Close();
            }
            TimeSpan end = TimeKeeper.GetCurrentTime();
            TimeSpan exectime = start.Subtract(end);
            //Logger.RaiseTrace(ClassName,"Exit:ExecutionTime="+exectime.ToString(),TraceLevel.Verbose);
            return retval;
        }


        #endregion

        #region Get Methods


        /// <summary>
        /// Get User Types obtains the current user types definiton and returns the values as a string array.
        /// The information obtained by this method is actually obtained from configuration not the data base.
        /// The SIAM data base schema does not contain a definiton of User Types.
        /// </summary>
        /// <param name="ErrorString">A string value containing any error information</param>
        /// <returns>An array of string values</returns>
        public string[] GetUserTypes(out string ErrorString) {
            //    string dummyString = ErrorString;
            //    string[] Names = { ErrorString }; 
            //    TimeSpan start = TimeKeeper.GetCurrentTime();
            //    //Logger.RaiseTrace(ClassName,"Entry:"+start.ToString(),TraceLevel.Verbose);
            //    ErrorString = "";
            //    string[] Names = null;
            //    try
            //    {
            //        //SIAMConfig config = SIAMUtilities.CachingServices.Cache["SiamConfiguration"] as SIAMConfig;
            //        //if (config == null)
            //        //{
            //        //    config = new SIAMConfig();
            //        //    SIAMUtilities.CachingServices.Cache["SiamConfiguration"] = config;
            //        //}
            //        Names = new string[ConfigurationData.Environment.UserTypes.Length];
            //        for(int x = 0; x <= Names.GetUpperBound(0);x++)
            //        {
            //            Names[x] = ConfigurationData.Environment.UserTypes[x].Name;
            //        }
            //    }
            //    catch(System.Exception ex)
            //    {
            //        HandleError(ex,out ErrorString);
            //    }
            //    TimeSpan end = TimeKeeper.GetCurrentTime();
            //    TimeSpan exectime = start.Subtract(end);
            //    //Logger.RaiseTrace(ClassName,"Exit:ExecutionTime="+exectime.ToString(),TraceLevel.Verbose);

            // to avoid the build errors
            ErrorString = "";
            return null;

        }


        /// <summary>
        /// GetStatusCodes returns a listing of Status Codes defined by the data base.
        /// </summary>
        /// <param name="ErrorString">A string value containing any error information</param>
        /// <returns>An array of UserStatus objects.</returns>
        //6668 starts
        /*public UserStatus[] GetStatusCodes(out string ErrorString)
		{
			TimeSpan start = TimeKeeper.GetCurrentTime();
			//Logger.RaiseTrace(ClassName,"Entry:"+start.ToString(),TraceLevel.Verbose);
			System.Data.DataSet ds;
			ErrorString = "";
			UserStatus[] statusArray = null;
			try
			{
				OracleCommand cmd = new OracleCommand();
				cmd.CommandText = "GetStatusCodes";
				ds = GetResults(cmd,out ErrorString);
				if (ErrorString == "" && ds != null)
				{
					statusArray = new UserStatus[ds.Tables[0].Rows.Count];
					int count = 0;
					foreach (System.Data.DataRow row in ds.Tables[0].Rows)
					{
						statusArray[count] = new UserStatus();
						statusArray[count].StatusId = System.Int32.Parse(row["StatusId"].ToString());
						statusArray[count].StatusName = row["StatusName"].ToString();
						count++;
					}
				}
				else
				{
					throw new System.Exception(ErrorString);
				}
			}
			catch(System.Exception ex)
			{
				HandleError(ex,out ErrorString);
			}
			TimeSpan end = TimeKeeper.GetCurrentTime();
			TimeSpan exectime = start.Subtract(end);
			//Logger.RaiseTrace(ClassName,"Exit:ExecutionTime="+exectime.ToString(),TraceLevel.Verbose);
			return statusArray;
		}*/
        //6668 ends


        /// <summary>
        /// GetActions obtains a listing of Actions defined by the data base.
        /// </summary>
        /// <param name="ErrorString">A string value containing any error information</param>
        /// <returns>An array of Action objects</returns>
        //6732 starts
        /*public Action[] GetActions(out string ErrorString)
		{
			TimeSpan start = TimeKeeper.GetCurrentTime();
			//Logger.RaiseTrace(ClassName,"Entry:"+start.ToString(),TraceLevel.Verbose);
			Action[] actions = null;
			ErrorString = "";
			try
			{
				OracleCommand cmd = new OracleCommand();
				cmd.CommandText = "GetActions";
				System.Data.DataSet ds = GetResults(cmd,out ErrorString);
				if (ErrorString == "" && ds != null)
				{
					if (ds.Tables.Count > 0)
					{
						if (ds.Tables[0].Rows.Count > 0)
						{
							actions = new Action[ds.Tables[0].Rows.Count];
							for (int x = 0; x <= actions.GetUpperBound(0);x++)
							{
								actions[x] = new Action();
								actions[x].Id = ds.Tables[0].Rows[x]["ActionId"].ToString();
								actions[x].Name = ds.Tables[0].Rows[x]["Name"].ToString();
							}
						}
					}
				}
				else
				{
					throw new System.Exception(ErrorString);
				}
			}
			catch(System.Exception ex)
			{
				HandleError(ex,out ErrorString);
			}
			TimeSpan end = TimeKeeper.GetCurrentTime();
			TimeSpan exectime = start.Subtract(end);
			//Logger.RaiseTrace(ClassName,"Exit:ExecutionTime="+exectime.ToString(),TraceLevel.Verbose);
			return actions;
		}*/
        //6732 ends
        /// <summary>
        /// GetFunctionalities obtains a listing of Functionality defined by the data base.
        /// </summary>
        /// <param name="ErrorString">A string value containing any error information</param>
        /// <returns>An array of sony.us.siam.Role objects</returns>
        public Functionality[] GetFunctionalities(out string ErrorString) {
            TimeSpan start = TimeKeeper.GetCurrentTime();
            //Logger.RaiseTrace(ClassName, "Entry:" + start.ToString(), TraceLevel.Verbose);
            ErrorString = "";
            Functionality[] Functionalities = null;
            try {
                OracleCommand cmd = new OracleCommand();
                cmd.CommandText = "GetFunctionalities";
                System.Data.DataSet ds = GetResults(cmd, out ErrorString);
                if (ErrorString == "" && ds != null) {
                    if (ds.Tables.Count > 0) {
                        if (ds.Tables[0].Rows.Count > 0) {
                            Functionalities = new Functionality[ds.Tables[0].Rows.Count];
                            int count = 0;
                            foreach (System.Data.DataRow row in ds.Tables[0].Rows) {
                                Functionalities[count] = new Functionality();
                                Functionalities[count].Name = row["FUNCTIONALITYNAME"].ToString();
                                Functionalities[count].FunctionalityId = System.Convert.ToInt32(row["FUNCTIONALITYID"].ToString());
                                Functionalities[count].AvailableStatus = System.Convert.ToInt32(row["STATUS"].ToString());
                                count++;
                            }
                        }
                    }
                } else {
                    throw new Exception(ErrorString);
                }
            } catch (Exception ex) {
                HandleError(ex, out ErrorString);
            }
            TimeSpan end = TimeKeeper.GetCurrentTime();
            TimeSpan exectime = start.Subtract(end);
            //Logger.RaiseTrace(ClassName, "Exit:ExecutionTime=" + exectime.ToString(), TraceLevel.Verbose);
            return Functionalities;
        }


        /// <summary>
        /// GetFunctionalities obtains a listing of Functionality defined by the data base.
        /// </summary>
        /// <param name="RoleID"></param>
        /// <param name="ErrorString">A string value containing any error information</param>
        /// <returns>An array of sony.us.siam.Role objects</returns>
        public Functionality[] GetFunctionalities(Int32 RoleID, out string ErrorString) {
            TimeSpan start = TimeKeeper.GetCurrentTime();
            //Logger.RaiseTrace(ClassName, "Entry:" + start.ToString(), TraceLevel.Verbose);
            ErrorString = "";
            Functionality[] Functionalities = null;
            try {
                OracleCommand cmd = new OracleCommand();
                cmd.CommandText = "GETFUNCTIONALITIESFORROLE";

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add("xRoleId", OracleDbType.Int32, 22);
                cmd.Parameters["xRoleId"].Value = RoleID;

                System.Data.DataSet ds = GetResults(cmd, out ErrorString);
                if (ErrorString == "" && ds != null) {
                    if (ds.Tables.Count > 0) {
                        if (ds.Tables[0].Rows.Count > 0) {
                            Functionalities = new Functionality[ds.Tables[0].Rows.Count];
                            int count = 0;
                            foreach (System.Data.DataRow row in ds.Tables[0].Rows) {
                                Functionalities[count] = new Functionality();
                                Functionalities[count].Name = row["FUNCTIONALITYNAME"].ToString();
                                Functionalities[count].FunctionalityId = System.Convert.ToInt32(row["FUNCTIONALITYID"].ToString());
                                Functionalities[count].AvailableStatus = System.Convert.ToInt32(row["STATUS"].ToString());
                                count++;
                            }
                        }
                    }
                } else {
                    throw new Exception(ErrorString);
                }
            } catch (Exception ex) {
                HandleError(ex, out ErrorString);
            }
            TimeSpan end = TimeKeeper.GetCurrentTime();
            TimeSpan exectime = start.Subtract(end);
            //Logger.RaiseTrace(ClassName, "Exit:ExecutionTime=" + exectime.ToString(), TraceLevel.Verbose);
            return Functionalities;
        }

        /// <summary>
        /// GetRoles obtains a listing of roles defined by the data base.
        /// </summary>
        /// <param name="ErrorString">A string value containing any error information</param>
        /// <returns>An array of sony.us.siam.Role objects</returns>
        public Role[] GetRoles(out string ErrorString) {
            TimeSpan start = TimeKeeper.GetCurrentTime();
            //Logger.RaiseTrace(ClassName,"Entry:"+start.ToString(),TraceLevel.Verbose);
            ErrorString = "";
            Role[] roles = null;
            try {
                OracleCommand cmd = new OracleCommand();
                cmd.CommandText = "GetRoles";
                System.Data.DataSet ds = GetResults(cmd, out ErrorString);
                if (ErrorString == "" && ds != null) {
                    if (ds.Tables.Count > 0) {
                        if (ds.Tables[0].Rows.Count > 0) {
                            roles = new Role[ds.Tables[0].Rows.Count];
                            int count = 0;
                            foreach (System.Data.DataRow row in ds.Tables[0].Rows) {
                                roles[count] = new Role();
                                roles[count].Name = row["Name"].ToString();
                                roles[count].RoleId = System.Convert.ToInt32(row["RoleId"].ToString());
                                //roles[count].Children = GetChildRoles(System.Convert.ToInt32(row["RoleId"].ToString()),out ErrorString);
                                //roles[count].AccessRights = GetAccessRights(roles[count].RoleId,out ErrorString);
                                count++;
                            }
                        }
                    }
                } else {
                    throw new Exception(ErrorString);
                }
            } catch (Exception ex) {
                HandleError(ex, out ErrorString);
            }
            TimeSpan end = TimeKeeper.GetCurrentTime();
            TimeSpan exectime = start.Subtract(end);
            //Logger.RaiseTrace(ClassName,"Exit:ExecutionTime="+exectime.ToString(),TraceLevel.Verbose);
            return roles;
        }


        /// <summary>
        /// GetChildRoles. Obtains a listing of child roles for a given parent role in the data base.
        /// </summary>
        /// <param name="ParentRoleId">The ID of the parent role</param>
        /// <param name="ErrorString">A string value containing any error information</param>
        /// <returns>An array of sony.us.siam.Role objects</returns>
        public Role[] GetChildRoles(int ParentRoleId, out string ErrorString) {
            TimeSpan start = TimeKeeper.GetCurrentTime();
            //Logger.RaiseTrace(ClassName,"Entry:"+start.ToString(),TraceLevel.Verbose);
            Role[] roles = null;
            ErrorString = "";
            try {
                OracleCommand cmd = new OracleCommand();
                cmd.CommandText = "GetChildRoles";
                cmd.Parameters.Add("xParentId", OracleDbType.Int32, 20);
                cmd.Parameters["xParentId"].Value = ParentRoleId;
                System.Data.DataSet ds = GetResults(cmd, out ErrorString);
                if (ErrorString == "" && ds != null) {
                    if (ds.Tables.Count > 0) {
                        if (ds.Tables[0].Rows.Count > 0) {
                            roles = new Role[ds.Tables[0].Rows.Count];
                            int count = 0;
                            foreach (System.Data.DataRow row in ds.Tables[0].Rows) {
                                roles[count] = new Role();
                                roles[count].RoleId = System.Int32.Parse(row["RoleId"].ToString());
                                roles[count].Name = row["Name"].ToString();
                                //Right now we do not support Child role in ServicesPLUS Admin
                                //roles[count].Children = GetChildRoles(System.Convert.ToInt32(row["RoleId"].ToString()),out ErrorString);
                                roles[count].AccessRights = GetAccessRights(roles[count].RoleId, out ErrorString);
                                count++;
                            }
                        }
                    }
                } else {
                    throw new Exception(ErrorString);
                }

            } catch (Exception ex) {
                HandleError(ex, out ErrorString);
            }
            TimeSpan end = TimeKeeper.GetCurrentTime();
            TimeSpan exectime = start.Subtract(end);
            //Logger.RaiseTrace(ClassName,"Exit:ExecutionTime="+exectime.ToString(),TraceLevel.Verbose);
            return roles;
        }


        /// <summary>
        /// GetRole. Obtains a single role object from the data base.
        /// </summary>
        /// <param name="RoleId">The id of the Role to obtain</param>
        /// <param name="ErrorString">A string value containing any error information</param>
        /// <returns>An instance of sony.us.siam.Role</returns>
        public Role GetRoles(int RoleId, out string ErrorString) {
            TimeSpan start = TimeKeeper.GetCurrentTime();
            //Logger.RaiseTrace(ClassName,"Entry:"+start.ToString(),TraceLevel.Verbose);
            ErrorString = "";
            Role r = null;
            try {
                OracleCommand cmd = new OracleCommand();
                cmd.CommandText = "GetRoleById";
                cmd.Parameters.Add("xRoleId", OracleDbType.Int32, 22);
                cmd.Parameters["xRoleId"].Value = RoleId;
                System.Data.DataSet ds = GetResults(cmd, out ErrorString);
                if (ErrorString == "" && ds != null) {
                    if (ds.Tables.Count > 0 && ds.Tables[0] != null) {
                        r = new Role();
                        r.Name = ds.Tables[0].Rows[0]["Name"].ToString();
                        r.RoleId = System.Convert.ToInt32(ds.Tables[0].Rows[0]["RoleId"].ToString());
                        r.Children = GetChildRoles(RoleId, out ErrorString);
                        r.AccessRights = GetAccessRights(r.RoleId, out ErrorString);
                    }
                } else {
                    throw new Exception(ErrorString);
                }

            } catch (Exception ex) {
                HandleError(ex, out ErrorString);
            }
            TimeSpan end = TimeKeeper.GetCurrentTime();
            TimeSpan exectime = start.Subtract(end);
            //Logger.RaiseTrace(ClassName,"Exit:ExecutionTime="+exectime.ToString(),TraceLevel.Verbose);
            return r;
        }

        /// <summary>
        /// GetRole. Obtains a single role object from the data base.
        /// </summary>
        /// <param name="RoleId">The id of the Role to obtain</param>
        /// <param name="ErrorString">A string value containing any error information</param>
        /// <returns>An instance of sony.us.siam.Role</returns>
        /// Commented as its not used in Presentation Layer
        //public object GetCCModifierUser(string UserId, out string ErrorString)
        //{
        //    TimeSpan start = TimeKeeper.GetCurrentTime();
        //    //Logger.RaiseTrace(ClassName, "Entry:" + start.ToString(), TraceLevel.Verbose);
        //    ErrorString = "";
        //    System.Data.DataSet ds = new DataSet();
        //    try
        //    {
        //        OracleCommand cmd = new OracleCommand();
        //        cmd.CommandText = "CCModifierLogin";
        //        cmd.Parameters.Add("xUserId", OracleDbType.Varchar2, 50);
        //        cmd.Parameters["xUserId"].Value = UserId;
        //        ds = GetResults(cmd, out ErrorString);
        //    }
        //    catch (System.Exception ex)
        //    {
        //        HandleError(ex, out ErrorString);
        //    }
        //    TimeSpan end = TimeKeeper.GetCurrentTime();
        //    TimeSpan exectime = start.Subtract(end);
        //    //Logger.RaiseTrace(ClassName, "Exit:ExecutionTime=" + exectime.ToString(), TraceLevel.Verbose);
        //    return ds;
        //}


        /// <summary>
        /// Get Roles overload. Obtains a listing of Roles defined in the data base for a given 
        /// user.
        /// </summary>
        /// <param name="UserId">The UserId whose roles are being obtained.</param>
        /// <param name="ErrorString">A string value containing any error information</param>
        /// <returns>An array of sony.us.siam.Role objects.</returns>
        public Role[] GetRoles(string UserId, out string ErrorString) {
            TimeSpan start = TimeKeeper.GetCurrentTime();
            //Logger.RaiseTrace(ClassName,"Entry:"+start.ToString(),TraceLevel.Verbose);
            Role[] roles = null;
            ErrorString = "";
            try {
                OracleCommand cmd = new OracleCommand();
                cmd.CommandText = "GetUserRoles";
                cmd.Parameters.Add("xUserId", OracleDbType.Char, 20);
                cmd.Parameters["xUserId"].Value = UserId;
                System.Data.DataSet ds = GetResults(cmd, out ErrorString);
                if (ErrorString == "" && ds != null) {
                    if (ds.Tables.Count > 0) {
                        if (ds.Tables[0].Rows.Count > 0) {
                            roles = new Role[ds.Tables[0].Rows.Count];
                            int count = 0;
                            foreach (System.Data.DataRow row in ds.Tables[0].Rows) {
                                roles[count] = new Role();
                                roles[count].RoleId = System.Int32.Parse(row["RoleId"].ToString());
                                roles[count].Name = row["RoleName"].ToString();
                                roles[count].Children = GetChildRoles(System.Convert.ToInt32(row["RoleId"].ToString()), out ErrorString);
                                roles[count].AccessRights = GetAccessRights(roles[count].RoleId, out ErrorString);
                                count++;
                            }
                        }

                    }
                } else {
                    throw new Exception(ErrorString);
                }
            } catch (Exception ex) {
                HandleError(ex, out ErrorString);
            }
            TimeSpan end = TimeKeeper.GetCurrentTime();
            TimeSpan exectime = start.Subtract(end);
            //Logger.RaiseTrace(ClassName,"Exit:ExecutionTime="+exectime.ToString(),TraceLevel.Verbose);
            return roles;
        }



        /// <summary>
        /// GetAccessRights. Obtains a listing of Access Rights defined by a Role.
        /// </summary>
        /// <param name="RoleId">The role id.</param>
        /// <param name="ErrorString">A string value containing any error information.</param>
        /// <returns>An array of sony.us.siam.AccessRight objects</returns>
        public AccessRight[] GetAccessRights(int RoleId, out string ErrorString) {
            TimeSpan start = TimeKeeper.GetCurrentTime();
            //Logger.RaiseTrace(ClassName,"Entry:"+start.ToString(),TraceLevel.Verbose);
            AccessRight[] ar = null;
            ErrorString = "";
            try {
                OracleCommand cmd = new OracleCommand();
                cmd.CommandText = "GetAccessRights";
                cmd.Parameters.Add("xRoleId", OracleDbType.Int32, 22);
                cmd.Parameters["xRoleId"].Value = RoleId;
                System.Data.DataSet ds = GetResults(cmd, out ErrorString);
                if (ErrorString == "" && ds != null) {
                    if (ds.Tables.Count > 0) {
                        if (ds.Tables[0].Rows.Count > 0) {
                            ar = new AccessRight[ds.Tables[0].Rows.Count];
                            int count = 0;
                            foreach (System.Data.DataRow row in ds.Tables[0].Rows) {
                                ar[count] = new AccessRight();
                                Action action = new Action();
                                action.Name = row["actionname"].ToString();
                                action.Id = row["actionId"].ToString();
                                ar[count].Action = action;
                                Resource resource = new Resource();
                                resource.Name = row["resourcename"].ToString();
                                resource.Id = System.Convert.ToInt32(row["resourceId"].ToString());
                                ar[count].Resource = resource;
                                Application application = new Application();
                                application.Name = row["ApplicationName"].ToString();
                                application.Id = System.Convert.ToInt32(row["ApplicationId"].ToString());
                                ar[count].Application = application;
                                count++;
                            }
                        }
                    }
                } else {
                    throw new Exception(ErrorString);
                }
            } catch (Exception ex) {
                HandleError(ex, out ErrorString);
            }
            TimeSpan end = TimeKeeper.GetCurrentTime();
            TimeSpan exectime = start.Subtract(end);
            //Logger.RaiseTrace(ClassName,"Exit:ExecutionTime="+exectime.ToString(),TraceLevel.Verbose);
            return ar;
        }


        /// <summary>
        /// GetAction obtains an action from the database.
        /// </summary>
        /// <param name="ActionId">The id of the action</param>
        /// <param name="ErrorString">A string value containing any error information</param>
        /// <returns>An instance of sony.us.siam.Action</returns>
        //6732 starts
        /*public Action GetAction(int ActionId,out string ErrorString)
		{
			// Log Data
			TimeSpan start = TimeKeeper.GetCurrentTime();
			//Logger.RaiseTrace(ClassName,"Entry:"+start.ToString(),TraceLevel.Verbose);
			ErrorString = "";
			Action action = null;
			try
			{
				OracleCommand cmd = new OracleCommand();
				cmd.CommandText = "GetActionById";
				cmd.Parameters.Add("xActionId",OracleDbType.Int32,22);
				cmd.Parameters["xActionId"].Value = ActionId;
				System.Data.DataSet ds = GetResults(cmd,out ErrorString);
				if (ErrorString == "" && ds != null)
				{
					if (ds.Tables.Count > 0)
					{
						if (ds.Tables[0].Rows.Count > 0)
						{
							action = new Action();
							action.Id = ds.Tables[0].Rows[0]["Id"].ToString();
							action.Name = ds.Tables[0].Rows[0]["Name"].ToString();
						}
					}
				}
				else
				{
					throw new System.Exception(ErrorString);
				}
			}
			catch(System.Exception ex)
			{
				HandleError(ex,out ErrorString);
			}
			TimeSpan end = TimeKeeper.GetCurrentTime();
			TimeSpan exectime = start.Subtract(end);
			//Logger.RaiseTrace(ClassName,"Exit:ExecutionTime="+exectime.ToString(),TraceLevel.Verbose);
			return action;
		}*/
        //6732 ends


        /// <summary>
        /// GetApplication. Obtains an application from the database.
        /// </summary>
        /// <param name="ApplicationId">The id of the application</param>
        /// <param name="ErrorString">A string value containing any error information</param>
        /// <returns>An instance of sony.us.siam.Application</returns>
        public Application GetApplication(int ApplicationId, out string ErrorString) {
            // Log Data
            TimeSpan start = TimeKeeper.GetCurrentTime();
            //Logger.RaiseTrace(ClassName,"Entry:"+start.ToString(),TraceLevel.Verbose);
            ErrorString = "";
            Application app = null;

            try {
                OracleCommand cmd = new OracleCommand();
                cmd.CommandText = "GetApplicationByID";
                cmd.Parameters.Add("xApplicationId", OracleDbType.Int32, 22);
                cmd.Parameters["xApplicationId"].Value = ApplicationId;
                System.Data.DataSet ds = GetResults(cmd, out ErrorString);
                if (ErrorString == "" && ds != null) {
                    if (ds.Tables.Count > 0) {
                        if (ds.Tables[0].Rows.Count > 0) {
                            app = new Application();
                            app.Id = System.Int32.Parse(ds.Tables[0].Rows[0]["ApplicationId"].ToString());
                            app.Name = ds.Tables[0].Rows[0]["Name"].ToString();
                        }
                    }
                } else {
                    throw new Exception(ErrorString);
                }
            } catch (Exception ex) {
                HandleError(ex, out ErrorString);
            }
            TimeSpan end = TimeKeeper.GetCurrentTime();
            TimeSpan exectime = start.Subtract(end);
            //Logger.RaiseTrace(ClassName,"Exit:ExecutionTime="+exectime.ToString(),TraceLevel.Verbose);
            return app;
        }


        /// <summary>
        /// Get Applications. Obtains a listing of Applications defined by SIAM.
        /// </summary>
        /// <param name="ErrorString">A string value containing any error information.</param>
        /// <returns>An array of sony.us.siam.Application objects.</returns>
        public Application[] GetApplications(out string ErrorString) {
            // Log Data
            TimeSpan start = TimeKeeper.GetCurrentTime();
            //Logger.RaiseTrace(ClassName,"Entry:"+start.ToString(),TraceLevel.Verbose);
            Application[] Applications = null;
            ErrorString = "";
            try {
                OracleCommand cmd = new OracleCommand();
                cmd.CommandText = "GetApplications";
                System.Data.DataSet ds = GetResults(cmd, out ErrorString);
                if (ErrorString == "" && ds != null) {
                    if (ds.Tables.Count > 0) {
                        if (ds.Tables[0].Rows.Count > 0) {
                            Applications = new Application[ds.Tables[0].Rows.Count];
                            for (int x = 0; x <= Applications.GetUpperBound(0); x++) {
                                Applications[x] = new Application();
                                Applications[x].Id = System.Int32.Parse(ds.Tables[0].Rows[x]["ApplicationId"].ToString());
                                Applications[x].Name = ds.Tables[0].Rows[x]["Name"].ToString();
                            }
                        }
                    }
                } else {
                    throw new Exception(ErrorString);
                }
            } catch (Exception ex) {
                HandleError(ex, out ErrorString);
            }
            TimeSpan end = TimeKeeper.GetCurrentTime();
            TimeSpan exectime = start.Subtract(end);
            //Logger.RaiseTrace(ClassName,"Exit:ExecutionTime="+exectime.ToString(),TraceLevel.Verbose);
            return Applications;
        }


        /// <summary>
        /// GetApplicationResources. Obtains a listing of resources defined for an application.
        /// </summary>
        /// <param name="ApplicationId">The application id.</param>
        /// <param name="ErrorString">A string value containing any error information</param>
        /// <returns>An array of sony.us.siam.Resource objects.</returns>
        public Resource[] GetApplicationResources(int ApplicationId, out string ErrorString) {
            // Log Data
            TimeSpan start = TimeKeeper.GetCurrentTime();
            //Logger.RaiseTrace(ClassName,"Entry:"+start.ToString(),TraceLevel.Verbose);
            Resource[] resources = null;
            ErrorString = "";
            try {
                OracleCommand cmd = new OracleCommand();
                cmd.CommandText = "GetApplicationResources";
                cmd.Parameters.Add("xApplicationId", OracleDbType.Int32, 22);
                cmd.Parameters["xApplicationId"].Value = ApplicationId;
                System.Data.DataSet ds = GetResults(cmd, out ErrorString);
                if (ErrorString == "" && ds != null) {
                    if (ds.Tables.Count > 0) {
                        if (ds.Tables[0].Rows.Count > 0) {
                            resources = new Resource[ds.Tables[0].Rows.Count];
                            for (int x = 0; x <= resources.GetUpperBound(0); x++) {
                                resources[x] = new Resource();
                                resources[x].Name = ds.Tables[0].Rows[x]["ResourceName"].ToString();
                                resources[x].Id = System.Int32.Parse(ds.Tables[0].Rows[x]["ResourceId"].ToString());
                            }
                        }
                    }
                } else {
                    throw new Exception(ErrorString);
                }
            } catch (Exception ex) {
                HandleError(ex, out ErrorString);
            }
            TimeSpan end = TimeKeeper.GetCurrentTime();
            TimeSpan exectime = start.Subtract(end);
            //Logger.RaiseTrace(ClassName,"Exit:ExecutionTime="+exectime.ToString(),TraceLevel.Verbose);
            return resources;
        }


        /// <summary>
        /// GetResource. Obtains a resource from SIAM.
        /// </summary>
        /// <param name="ResourceId">The ID of the resource</param>
        /// <param name="ErrorString">A string value containing any error information</param>
        /// <returns>An instance of sony.us.siam.Resource</returns>
        public Resource GetResource(int ResourceId, out string ErrorString) {
            // Log Data
            TimeSpan start = TimeKeeper.GetCurrentTime();
            //Logger.RaiseTrace(ClassName,"Entry:"+start.ToString(),TraceLevel.Verbose);
            ErrorString = "";
            Resource resource = null;
            try {
                OracleCommand cmd = new OracleCommand();
                cmd.CommandText = "GetResourceByID";
                cmd.Parameters.Add("xResourceId", OracleDbType.Int32, 22);
                cmd.Parameters["xResourceId"].Value = ResourceId;
                System.Data.DataSet ds = GetResults(cmd, out ErrorString);
                if (ErrorString == "" && ds != null) {
                    if (ds.Tables.Count > 0) {
                        if (ds.Tables[0].Rows.Count > 0) {
                            resource = new Resource();
                            resource.Id = System.Int32.Parse(ds.Tables[0].Rows[0]["ResourceId"].ToString());
                            resource.Name = ds.Tables[0].Rows[0]["ResourceName"].ToString();
                        }
                    }
                } else {
                    throw new Exception(ErrorString);
                }
            } catch (Exception ex) {
                HandleError(ex, out ErrorString);
            }
            TimeSpan end = TimeKeeper.GetCurrentTime();
            TimeSpan exectime = start.Subtract(end);
            //Logger.RaiseTrace(ClassName,"Exit:ExecutionTime="+exectime.ToString(),TraceLevel.Verbose);
            return resource;
        }

        /// <summary>
        /// GetUser. Obtains a user from SIAM.
        /// </summary>
        /// <param name="Identity">The Users SIAM Identity.</param>
        /// <param name="ErrorString">A string value containing any error information</param>
        /// <returns>An instance sony.us.siam.User</returns>
        public DataSet GetSiamAndCustomer(string Identity, out string ErrorString) {
            // Log Data
            TimeSpan start = TimeKeeper.GetCurrentTime();
            //Logger.RaiseTrace(ClassName, "Entry:" + start.ToString(), TraceLevel.Verbose);
            ErrorString = "";
            System.Data.DataSet ds = new DataSet();
            try {
                OracleCommand cmd = new OracleCommand();
                cmd.CommandText = "CustomerDetails.GETSIAMUSERANDCUSTOMERDATA";
                cmd.Parameters.Add("xSIAMID", OracleDbType.Varchar2, 50);
                cmd.Parameters["xSIAMID"].Value = Identity;
                cmd.Parameters.Add("xCustomer", OracleDbType.RefCursor);
                cmd.Parameters["xCustomer"].Direction = ParameterDirection.Output;
                cmd.Parameters.Add("xSIAMUSER", OracleDbType.RefCursor);
                cmd.Parameters["xSIAMUSER"].Direction = ParameterDirection.Output;

                ds = ExecuteDataSet(cmd, out ErrorString);
                if (ErrorString == "" && ds != null) {
                    return ds;
                } else {
                    throw new Exception(ErrorString);
                }
            } catch (Exception ex) {
                HandleError(ex, out ErrorString);
            }
            TimeSpan end = TimeKeeper.GetCurrentTime();
            TimeSpan exectime = start.Subtract(end);
            //Logger.RaiseTrace(ClassName, "Exit:ExecutionTime=" + exectime.ToString(), TraceLevel.Verbose);
            return ds;
        }

        /// <summary>
        /// GetUser. Obtains a user from SIAM.
        /// </summary>
        /// <param name="Identity">The Users SIAM Identity.</param>
        /// <param name="ErrorString">A string value containing any error information</param>
        /// <returns>An instance sony.us.siam.User</returns>
        public User GetUser(string Identity, out string ErrorString) {
            // Log Data
            TimeSpan start = TimeKeeper.GetCurrentTime();
            //Logger.RaiseTrace(ClassName,"Entry:"+start.ToString(),TraceLevel.Verbose);
            User u = null;
            ErrorString = "";
            try {
                OracleCommand cmd = new OracleCommand();
                cmd.CommandText = "GetUser";
                cmd.Parameters.Add("xUserId", OracleDbType.Char, 50);
                cmd.Parameters["xUserId"].Value = Identity;
                System.Data.DataSet ds = GetResults(cmd, out ErrorString);
                if (ErrorString == "" && ds != null) {
                    if (ds.Tables.Count > 0) {
                        u = new User();
                        HydrateUser(ds.Tables[0], u);
                        u.Roles = this.GetRoles(u.Identity, out ErrorString);
                        u.EffectiveRights = GetEffectiveAccessRights(u.Roles, out ErrorString);
                    }
                } else {
                    throw new Exception(ErrorString);
                }
            } catch (Exception ex) {
                HandleError(ex, out ErrorString);
            }
            TimeSpan end = TimeKeeper.GetCurrentTime();
            TimeSpan exectime = start.Subtract(end);
            //Logger.RaiseTrace(ClassName,"Exit:ExecutionTime="+exectime.ToString(),TraceLevel.Verbose);
            return u;
        }


        /// <summary>
        /// GetUser. Obtains a user from SIAM.
        /// </summary>
        /// <param name="ErrorString">A string value containing any error information</param>
        /// <returns>An instance sony.us.siam.User</returns>
        /// Commented by Prasad because this method is not used any where in the application
        //public object GetInvalidUsers(out string ErrorString)
        //{
        //    // Log Data
        //    TimeSpan start = TimeKeeper.GetCurrentTime();
        //    //Logger.RaiseTrace(ClassName,"Entry:"+start.ToString(),TraceLevel.Verbose);
        //    System.Data.DataSet ds = new DataSet();
        //    ErrorString = "";
        //    try
        //    {
        //        OracleCommand cmd = new OracleCommand();
        //        cmd.CommandText = "GetInvalidSIAMUser";
        //         ds = GetResults(cmd,out ErrorString);
        //    }
        //    catch(System.Exception ex)
        //    {
        //        HandleError(ex,out ErrorString);
        //    }
        //    TimeSpan end = TimeKeeper.GetCurrentTime();
        //    TimeSpan exectime = start.Subtract(end);
        //    //Logger.RaiseTrace(ClassName,"Exit:ExecutionTime="+exectime.ToString(),TraceLevel.Verbose);
        //    return (object)ds;
        //}	

        /// <summary>
        /// Returns a SIAM user based on username and email address. Used for forgot password
        /// functionality.
        /// </summary>
        /// <param name="UserName"></param>
        /// <param name="EmailAddress"></param>
        /// <param name="ErrorString"></param>
        /// <returns></returns>
        ///
        //6691 starts
        /*public User GetUserWithEmail(string UserName,string EmailAddress,out string ErrorString)
		{
			// Log Data
			TimeSpan start = TimeKeeper.GetCurrentTime();
			//Logger.RaiseTrace(ClassName,"Entry:"+start.ToString(),TraceLevel.Verbose);
			User u = null;
			ErrorString = "";
			try
			{
				OracleCommand cmd = new OracleCommand();
                cmd.CommandText = "CUSTOMERDETAILS.GETUSERWITHEMAILANDIDENTITY";
				cmd.Parameters.Add("xUserId",OracleDbType.Char,50);
				cmd.Parameters["xUserId"].Value = UserName.ToLower();
				cmd.Parameters.Add("xEmail",OracleDbType.Char,50);
				cmd.Parameters["xEmail"].Value = EmailAddress.ToLower();

                cmd.Parameters.Add("xSITESTATUS", OracleDbType.Varchar2, 1);
                cmd.Parameters["xSITESTATUS"].Value = ConfigurationData.GeneralSettings.UnderMaintenance;
                
                OracleParameter outparam1;
                outparam1 = new OracleParameter("xSIAMUSER", OracleDbType.RefCursor);
                outparam1.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(outparam1);

                OracleParameter outparam2;
                outparam2 = new OracleParameter("xUSERROLE", OracleDbType.RefCursor);
                outparam2.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(outparam2);

                 OracleParameter outparam3;
                 outparam3 = new OracleParameter("xUSERFunction", OracleDbType.RefCursor);
                outparam3.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(outparam3);

                
                System.Data.DataSet ds = GetResultsWithAdapter(cmd, out ErrorString);
				if (ErrorString == "" && ds != null)
				{
					if (ds.Tables.Count > 1)
					{
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            u = new User();
						    HydrateUser(ds.Tables[0],u);
                        }
						
						//u.Roles = this.GetRoles(u.Identity,out ErrorString);
                        if (ds.Tables[1].Rows.Count > 0)
                        {
                            Role[] roles = new Role[ds.Tables[1].Rows.Count];
                            int count = 0;
                            foreach (System.Data.DataRow row in ds.Tables[1].Rows)
                            {
                                roles[count] = new Role();
                                roles[count].RoleId = System.Int32.Parse(row["RoleId"].ToString());
                                roles[count].Name = row["RoleName"].ToString();
                                count++;
                            }
                            u.Roles = roles;
                        }
						//u.EffectiveRights = GetEffectiveAccessRights(u.Roles,out ErrorString);
                        if (ds.Tables[2].Rows.Count > 0)
                        {
                            Functionality userFunctionality ;
                            ArrayList userFuncitonalityList = new ArrayList();
                            foreach (System.Data.DataRow row in ds.Tables[2].Rows)
                            {
                                userFunctionality =new Functionality();
                                userFunctionality.Name = row["FUNCTIONALITYNAME"].ToString();
                                userFunctionality.GroupName = row["GROUPNAME"].ToString();
                                userFunctionality.URLMapped = row["MAPPEDURL"].ToString();
                                userFuncitonalityList.Add(userFunctionality);
                            }
                            Functionality[] lstFunctionality = new Functionality[userFuncitonalityList.Count];
                            userFuncitonalityList.CopyTo(lstFunctionality);
                            u.Functionalities = lstFunctionality;
                        }
					}
				}
				else
				{
					throw new Exception(ErrorString);
				}
			}
			catch(System.Exception ex)
			{
				//HandleError(ex,out ErrorString);
                throw;
			}
			TimeSpan end = TimeKeeper.GetCurrentTime();
			TimeSpan exectime = start.Subtract(end);
			//Logger.RaiseTrace(ClassName,"Exit:ExecutionTime="+exectime.ToString(),TraceLevel.Verbose);
			return u;
		}*/

        public User GetUserWithEmail(string UserName, out string ErrorString) {
            // Log Data
            TimeSpan start = TimeKeeper.GetCurrentTime();
            //Logger.RaiseTrace(ClassName,"Entry:"+start.ToString(),TraceLevel.Verbose);
            User u = null;
            ErrorString = "";
            try {
                OracleCommand cmd = new OracleCommand();
                cmd.CommandText = "CUSTOMERDETAILS.GETUSERWITHEMAILANDIDENTITY";
                cmd.Parameters.Add("xUserId", OracleDbType.Char, 50);
                cmd.Parameters["xUserId"].Value = UserName.ToLower();

                cmd.Parameters.Add("xSITESTATUS", OracleDbType.Varchar2, 1);
                cmd.Parameters["xSITESTATUS"].Value = ConfigurationData.GeneralSettings.UnderMaintenance;

                OracleParameter outparam1;
                outparam1 = new OracleParameter("xSIAMUSER", OracleDbType.RefCursor);
                outparam1.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(outparam1);

                OracleParameter outparam2;
                outparam2 = new OracleParameter("xUSERROLE", OracleDbType.RefCursor);
                outparam2.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(outparam2);

                OracleParameter outparam3;
                outparam3 = new OracleParameter("xUSERFunction", OracleDbType.RefCursor);
                outparam3.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(outparam3);


                System.Data.DataSet ds = GetResultsWithAdapter(cmd, out ErrorString);
                if (ErrorString == "" && ds != null) {
                    if (ds.Tables.Count > 1) {
                        if (ds.Tables[0].Rows.Count > 0) {
                            u = new User();
                            HydrateUser(ds.Tables[0], u);
                        }

                        //u.Roles = this.GetRoles(u.Identity,out ErrorString);
                        if (ds.Tables[1].Rows.Count > 0) {
                            Role[] roles = new Role[ds.Tables[1].Rows.Count];
                            int count = 0;
                            foreach (System.Data.DataRow row in ds.Tables[1].Rows) {
                                roles[count] = new Role();
                                roles[count].RoleId = System.Int32.Parse(row["RoleId"].ToString());
                                roles[count].Name = row["RoleName"].ToString();
                                count++;
                            }
                            u.Roles = roles;
                        }
                        //u.EffectiveRights = GetEffectiveAccessRights(u.Roles,out ErrorString);
                        if (ds.Tables[2].Rows.Count > 0) {
                            Functionality userFunctionality;
                            ArrayList userFuncitonalityList = new ArrayList();
                            foreach (System.Data.DataRow row in ds.Tables[2].Rows) {
                                //Added by Sneha on 27/11/2013
                                switch (row["MAPPEDURL"].ToString()) {
                                    case "Update-Order-CertificateNumber.aspx":
                                        break;
                                    case "CertificationActivationCode.aspx":
                                        break;
                                    default:
                                        userFunctionality = new Functionality();
                                        userFunctionality.Name = row["FUNCTIONALITYNAME"].ToString();
                                        userFunctionality.GroupName = row["GROUPNAME"].ToString();
                                        userFunctionality.URLMapped = row["MAPPEDURL"].ToString();
                                        userFuncitonalityList.Add(userFunctionality);
                                        break;
                                }
                            }
                            Functionality[] lstFunctionality = new Functionality[userFuncitonalityList.Count];
                            userFuncitonalityList.CopyTo(lstFunctionality);
                            u.Functionalities = lstFunctionality;
                        }
                    }
                } else {
                    throw new Exception(ErrorString);
                }
            } catch (Exception) {
                //HandleError(ex,out ErrorString);
                throw;
            }
            TimeSpan end = TimeKeeper.GetCurrentTime();
            TimeSpan exectime = start.Subtract(end);
            //Logger.RaiseTrace(ClassName,"Exit:ExecutionTime="+exectime.ToString(),TraceLevel.Verbose);
            return u;
        }
        //6691 ends

        //public bool IsDuplicateUser(string UserName,string EmailAddress,string FirstName,string LastName)
        public bool IsDuplicateUser(string EmailAddress, string FirstName, string LastName, string userId) {
            // Log Data
            TimeSpan start = TimeKeeper.GetCurrentTime();
            //Logger.RaiseTrace(ClassName,"Entry:"+start.ToString(),TraceLevel.Verbose);
            string ErrorString = "";
            bool retVal = false;
            try {
                //---- Commented the following block logic for Bug # 6579, and added new code logic below this code block ----
                /*
                OracleCommand cmd = new OracleCommand();
                cmd.CommandText = "CheckDupUserId";
                cmd.Parameters.Add("xUserId",OracleDbType.Char,50);
                cmd.Parameters["xUserId"].Value = UserName.ToLower();
                System.Data.DataSet ds = GetResults(cmd,out ErrorString);
                if (ErrorString == "" && ds != null)
                {
                    if (ds.Tables.Count > 0)
                    {
                        retVal = true;
                    }
                }

                cmd.CommandText = "CheckDupNameEmail";
                cmd.Parameters.Clear();
                cmd.Parameters.Add("xFirstName",OracleDbType.Char,50);
                cmd.Parameters["xFirstName"].Value = FirstName;
                cmd.Parameters.Add("xLastName",OracleDbType.Char,50);
                cmd.Parameters["xLastName"].Value = LastName;
                cmd.Parameters.Add("xEmail",OracleDbType.Char,50);
                cmd.Parameters["xEmail"].Value = EmailAddress;
                ds = GetResults(cmd,out ErrorString);
                if (ErrorString == "" && ds != null)
                {
                    if (ds.Tables.Count > 0)
                    {
                        retVal = true;
                    }
                }
                */
                if (string.IsNullOrEmpty(userId)) {
                    OracleCommand cmd = new OracleCommand();
                    cmd.CommandText = "CHECKDUPEMAILID";
                    cmd.Parameters.Add("xEmailId", OracleDbType.Char, 50);
                    cmd.Parameters["xEmailId"].Value = EmailAddress;
                    System.Data.DataSet ds = GetResults(cmd, out ErrorString);
                    if (ErrorString == "" && ds != null) {
                        if (ds.Tables.Count > 0) {
                            retVal = true;
                        }
                    }

                    cmd.CommandText = "CHECKDUPNAME";
                    cmd.Parameters.Clear();
                    cmd.Parameters.Add("xFirstName", OracleDbType.Char, 50);
                    cmd.Parameters["xFirstName"].Value = FirstName;
                    cmd.Parameters.Add("xLastName", OracleDbType.Char, 50);
                    cmd.Parameters["xLastName"].Value = LastName;
                    ds = GetResults(cmd, out ErrorString);
                    if (ErrorString == "" && ds != null) {
                        if (ds.Tables.Count > 0) {
                            retVal = true;
                        }
                    }
                } else {
                    OracleCommand cmd = new OracleCommand();
                    cmd.CommandText = "CHECKDUPEMAILIDUSERID";
                    cmd.Parameters.Add("xEmailId", OracleDbType.Char, 50);
                    cmd.Parameters.Add("xUID", OracleDbType.Char, 50);
                    cmd.Parameters["xEmailId"].Value = EmailAddress;
                    cmd.Parameters["xUID"].Value = userId;
                    System.Data.DataSet ds = GetResults(cmd, out ErrorString);
                    if (ErrorString == "" && ds != null) {
                        if (ds.Tables.Count > 0) {
                            retVal = true;
                        }
                    }
                }
            } catch (Exception ex) {
                HandleError(ex, out ErrorString);
            }
            TimeSpan end = TimeKeeper.GetCurrentTime();
            TimeSpan exectime = start.Subtract(end);
            //Logger.RaiseTrace(ClassName,"Exit:ExecutionTime="+exectime.ToString(),TraceLevel.Verbose);
            return retVal;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="FirstName"></param>
        /// <param name="LastName"></param>
        /// <param name="EmailAddress"></param>
        /// <returns></returns>
        public string[] GetUserPinValues(string FirstName, string LastName, string EmailAddress) {
            TimeSpan start = TimeKeeper.GetCurrentTime();
            //Logger.RaiseTrace(ClassName,"Entry:"+start.ToString(),TraceLevel.Verbose);
            Sony.US.siam.User._thirdIDstructure idStruct = new Sony.US.siam.User._thirdIDstructure();
            idStruct.UserSecurityPINs = new string[3];
            string testString = string.Empty;
            if (FirstName != null && LastName != null && EmailAddress != null) {
                try {
                    OracleCommand cmd = new OracleCommand();
                    cmd.CommandText = "GETUSERSSECURITYIDS";
                    cmd.Parameters.Add("xFirstName", OracleDbType.Varchar2, 50);
                    //We should not trim here as the customer may type in name with trailing space accidently
                    //Here use Like in SQL to accomadate this situation
                    cmd.Parameters["xFirstName"].Value = FirstName.ToLower().ToString().Trim() + "%";
                    cmd.Parameters.Add("xLastName", OracleDbType.Varchar2, 50);
                    cmd.Parameters["xLastName"].Value = LastName.ToLower().ToString().Trim() + "%";
                    cmd.Parameters.Add("xEmailAddress", OracleDbType.Varchar2, 50);
                    cmd.Parameters["xEmailAddress"].Value = EmailAddress.ToLower().ToString().Trim();
                    string ErrorString = string.Empty;

                    System.Data.DataSet ds = GetResults(cmd, out ErrorString);

                    if (ErrorString == "" && ds != null) {
                        if (ds.Tables.Count > 0) {
                            DateTime oDate;
                            if (DateTime.TryParse(ds.Tables[0].Rows[0]["PIN2"].ToString(), out oDate) || ds.Tables[0].Rows[0]["PIN2"].ToString() == "n/a") {
                                idStruct.UserSecurityPINs[0] = ds.Tables[0].Rows[0]["PIN1"].ToString();
                                idStruct.UserSecurityPINs[1] = ds.Tables[0].Rows[0]["PIN2"].ToString();
                                idStruct.UserSecurityPINs[2] = ds.Tables[0].Rows[0]["PIN3"].ToString();
                            } else {
                                idStruct.UserSecurityPINs[0] = Encryption.DeCrypt(ds.Tables[0].Rows[0]["PIN1"].ToString(), Encryption.PWDKey);
                                idStruct.UserSecurityPINs[1] = Encryption.DeCrypt(ds.Tables[0].Rows[0]["PIN2"].ToString(), Encryption.PWDKey);
                                idStruct.UserSecurityPINs[2] = Encryption.DeCrypt(ds.Tables[0].Rows[0]["PIN3"].ToString(), Encryption.PWDKey);
                            }
                        } else {
                            idStruct.UserSecurityPINs[0] = Encryption.DeCrypt("", Encryption.PWDKey);
                            idStruct.UserSecurityPINs[1] = Encryption.DeCrypt("Table count = 0", Encryption.PWDKey); ;
                            idStruct.UserSecurityPINs[2] = Encryption.DeCrypt(cmd.Parameters["xFirstName"].Value.ToString() + " " + cmd.Parameters["xLastName"].Value.ToString() + " " + cmd.Parameters["xEmailAddress"].Value, Encryption.PWDKey);
                        }
                    } else {
                        idStruct.UserSecurityPINs[0] = Encryption.DeCrypt("", Encryption.PWDKey); ;
                        idStruct.UserSecurityPINs[1] = Encryption.DeCrypt(ErrorString, Encryption.PWDKey); ;
                        idStruct.UserSecurityPINs[2] = Encryption.DeCrypt("No records", Encryption.PWDKey); ;
                    }
                } catch (Exception ex) {
                    string ErrorString = "Error getting PINs from table. " + ex.Message.ToString();
                    HandleError(ex, out ErrorString);
                    throw new ApplicationException(ErrorString);
                    //idStruct.UserSecurityPINs[0] = Encryption.DeCrypt("", Encryption.PWDKey); ;
                    //idStruct.UserSecurityPINs[1] = Encryption.DeCrypt(ErrorString, Encryption.PWDKey); ;
                    //idStruct.UserSecurityPINs[2] = Encryption.DeCrypt("No records", Encryption.PWDKey); ;
                }
            }
            TimeSpan end = TimeKeeper.GetCurrentTime();
            TimeSpan exectime = start.Subtract(end);
            //Logger.RaiseTrace(ClassName,"Exit:ExecutionTime="+exectime.ToString(),TraceLevel.Verbose);
            return idStruct.UserSecurityPINs;
        }



        /// <summary>
        /// GetUser overload. Obtains a User object based on an instance of sony.us.siam.Credential
        /// </summary>
        /// <param name="result">An instance of <see cref="AuthenticationResult"/></param>
        /// <param name="ErrorString">A string value containing any error information</param>
        /// <returns>An instance of sony.us.siam.User</returns>
        public void GetUser(AuthenticationResult result, out string ErrorString) {
            TimeSpan start = TimeKeeper.GetCurrentTime();
            //Logger.RaiseTrace(ClassName,"Entry:"+start.ToString(),TraceLevel.Verbose);
            User u = new User();
            ErrorString = "";
            try {
                OracleCommand cmd = new OracleCommand();
                cmd.CommandText = "GetUserWithCredential";
                cmd.Parameters.Add("xUserId", OracleDbType.Char, 50);
                cmd.Parameters["xUserId"].Value = result.credential.UserName.ToLower().ToString();
                cmd.Parameters.Add("xPassword", OracleDbType.Char, 30);
                //cmd.Parameters["xPassword"].Value = result.credential.Password;
                //In GetUserWithCredential records are getting matched with user id only not with password,
                //so have added password in encrypted formate to check the user id and password both for getting user record.
                cmd.Parameters["xPassword"].Value = Encryption.Encrypt(result.credential.Password, Encryption.PWDKey);
                System.Data.DataSet ds = GetResults(cmd, out ErrorString);

                if (ErrorString == "" && ds != null) {
                    if (ds.Tables.Count > 0) {

                        switch (ds.Tables[0].Rows[0]["USERID"].ToString()) {
                            case "-1":
                                u.UserId = "-1";
                                result.user = u;
                                result.Result = true;
                                result.SiamResponseCode = (int)Messages.AUTHENTICATE_FAILURE_BADUSERID;
                                result.SiamResponseMessage = Messages.AUTHENTICATE_FAILURE_BADUSERID.ToString();
                                break;
                            case "-2":
                                u.UserId = "-2";
                                result.user = u;
                                result.Result = true;
                                result.SiamResponseCode = (int)Messages.AUTHENTICATE_FAILURE_BADPASSWORD;
                                result.SiamResponseMessage = Messages.AUTHENTICATE_FAILURE_BADPASSWORD.ToString();
                                break;
                            case "-3":
                                u.UserId = "-3";
                                result.user = u;
                                result.Result = true;
                                result.SiamResponseCode = (int)Messages.AUTHENTICATE_FAILURE_ACCOUNTLOCKED;
                                result.SiamResponseMessage = Messages.AUTHENTICATE_FAILURE_ACCOUNTLOCKED.ToString();
                                break;
                            default:
                                HydrateUser(ds.Tables[0], u);
                                u.Roles = this.GetRoles(u.Identity, out ErrorString);
                                u.EffectiveRights = GetEffectiveAccessRights(u.Roles, out ErrorString);
                                result.user = u;
                                result.Result = true;
                                result.SiamResponseCode = (int)Messages.AUTHENTICATE_SUCCESS;
                                result.SiamResponseMessage = Messages.AUTHENTICATE_SUCCESS.ToString();
                                break;
                        }


                    } else {
                        u = null;
                        result.user = null;
                        result.Result = false;
                        result.SiamResponseCode = (int)Messages.AUTHENTICATE_FAILURE;
                        result.SiamResponseMessage = Messages.AUTHENTICATE_FAILURE.ToString();
                    }
                } else {
                    throw new Exception(ErrorString);
                }
            } catch (Exception ex) {
                result.user = null;
                result.Result = false;
                result.SiamResponseCode = (int)Messages.AUTHENTICATE_FAILURE;
                result.SiamResponseMessage = Messages.AUTHENTICATE_FAILURE.ToString();
                HandleError(ex, out ErrorString);
            }
            TimeSpan end = TimeKeeper.GetCurrentTime();
            TimeSpan exectime = start.Subtract(end);
            //Logger.RaiseTrace(ClassName,"Exit:ExecutionTime="+exectime.ToString(),TraceLevel.Verbose);
            return;
        }


        /// <summary>
        /// GetEffectiveAccessRights. Obtains a listing of effective rights for a given array of roles.
        /// SIAM roles contain AccessRight collections. SIAM roles can also contain child roles each with
        /// it's own collection of AccessRights. A user can be associated with any number of roles. 
        /// A sony.us.siam.User object also defines a collection of Effective Rights. The effective rights
        /// are a aggregation of all the rights defined by all the roles to which this user belongs.
        /// </summary>
        /// <param name="roles">The array of roles to evaluate.</param>
        /// <param name="ErrorString">A string value containing any error information.</param>
        /// <returns>An array of sony.us.siam.AccessRight objects.</returns>
        public AccessRight[] GetEffectiveAccessRights(Role[] roles, out string ErrorString) {
            TimeSpan start = TimeKeeper.GetCurrentTime();
            //Logger.RaiseTrace(ClassName,"Entry:"+start.ToString(),TraceLevel.Verbose);
            ErrorString = "";
            AccessRight[] arights = null;
            try {
                if (roles != null) {
                    System.Collections.ArrayList arList = new System.Collections.ArrayList();
                    for (int x = 0; x <= roles.GetUpperBound(0); x++) {
                        if (roles[x].AccessRights != null) {
                            foreach (AccessRight right in roles[x].AccessRights) {
                                arList.Add(right);
                                if (roles[x].Children != null) {
                                    GetChildEffectiveRights(arList, roles[x].Children);
                                }
                            }
                        }
                    }
                    if (arList.Count > 0) {
                        arights = (AccessRight[])arList.ToArray(typeof(AccessRight));
                    }
                }
            } catch (Exception ex) {
                HandleError(ex, out ErrorString);
            }
            TimeSpan end = TimeKeeper.GetCurrentTime();
            TimeSpan exectime = start.Subtract(end);
            //Logger.RaiseTrace(ClassName,"Exit:ExecutionTime="+exectime.ToString(),TraceLevel.Verbose);
            return arights;
        }


        /// <summary>
        /// GetUsers (overloaded) returns a listing of hydrated user objects to the caller. This method
        /// calls the OracleAdministrator:GetUser overload with an empty string search parameter. This
        /// method could be very long running. If the caller passes a deep parameter of true then the
        /// return value will be an array of fully hydrated user objects. Depending on the number of
        /// users contained in SIAM this could be a very expensive call in terms of performance. If the
        /// user passes a deep parameter of false then an array of ShallowUser objects will be returned.
        /// Shallow hydration is the preferred method.
        /// </summary>
        /// <param name="deep">True if the user wants a deep hydration of the object model. False
        /// if the user wants a shallow hydration.</param>
        /// <param name="ErrorString">A string value containing any error information</param>
        /// <returns>An array of objects.</returns>
        public object[] GetUsers(bool deep, out string ErrorString) {
            TimeSpan start = TimeKeeper.GetCurrentTime();
            //Logger.RaiseTrace(ClassName,"Entry:"+start.ToString(),TraceLevel.Verbose);
            ErrorString = "";
            object[] r = null;
            try {
                r = GetUsers("", deep, out ErrorString);
                if (ErrorString != "") {
                    throw new Exception(ErrorString);
                }
            } catch (Exception ex) {
                HandleError(ex, out ErrorString);
            }
            TimeSpan end = TimeKeeper.GetCurrentTime();
            TimeSpan exectime = start.Subtract(end);
            //Logger.RaiseTrace(ClassName,"Exit:ExecutionTime="+exectime.ToString(),TraceLevel.Verbose);
            return r;
        }


        /// <summary>
        /// GetUsers (overloaded). Obtains a listing of Users for a given roleId.
        /// </summary>
        /// <param name="RoleId">The role id to be used.</param>
        /// <param name="ErrorString">A string value containing any error information.</param>
        /// <returns>An array of objects. Consumers of this method can expect an array of User objects.
        /// Shallow hydration is not available on this call.</returns>
        /// Commented By Rajkumar A on 07-01-2010. Stored Procedure is not used in the application
        //public object[] GetUsers(int RoleId,out string ErrorString)
        //{
        //    TimeSpan start = TimeKeeper.GetCurrentTime();
        //    //Logger.RaiseTrace(ClassName,"Entry:"+start.ToString(),TraceLevel.Verbose);
        //    siam.User[] users = null;
        //    ErrorString = "";
        //    try
        //    {
        //        OracleCommand cmd = new OracleCommand();
        //        cmd.CommandText = "GetUsersInRole";
        //        cmd.Parameters.Add("xRoleId",OracleDbType.Int32,22);
        //        cmd.Parameters["xRoleId"].Value = RoleId;
        //        DataSet ds = GetResults(cmd,out ErrorString);
        //        if (ErrorString == "" && ds != null)
        //        {
        //            if (ds.Tables.Count > 0 )
        //            {
        //                if (ds.Tables[0].Rows.Count > 0)
        //                {
        //                    users = ConvertUserDataTableToArray(ds.Tables[0]);
        //                }
        //            }
        //        }
        //        else
        //        {
        //            throw new Exception(ErrorString);
        //        }
        //    }
        //    catch(System.Exception ex)
        //    {
        //        HandleError(ex,out ErrorString);
        //    }
        //    TimeSpan end = TimeKeeper.GetCurrentTime();
        //    TimeSpan exectime = start.Subtract(end);
        //    //Logger.RaiseTrace(ClassName,"Exit:ExecutionTime="+exectime.ToString(),TraceLevel.Verbose);
        //    return users;
        //}

        /// <summary>
        /// GetUsers (overloaded). Obtains a listing of users based on a search pattern passed in.
        /// </summary>
        /// <param name="SearchPattern">The search pattern to use.</param>
        /// <param name="deep">A boolean value indicating the hydration method to use. True if the calle
        /// desires hydrated User objects. False if the user desires ShallowUser objects.</param>
        /// <param name="SPSSearch">A bool value saying if searching will be done on ServicesPLUS database.</param>
        /// <param name="ErrorString">A string value containing error information.</param>
        /// <returns>An array of objects.</returns>
        public object[] GetUsers(string SearchPattern, bool deep, bool SPSSearch, out string ErrorString) {
            mSPSSearch = SPSSearch;
            return GetUsers(SearchPattern, deep, out ErrorString);
        }


        /// <summary>
        /// GetUsers (overloaded). Obtains a listing of users based on a search pattern passed in.
        /// </summary>
        /// <param name="SearchPattern">The search pattern to use.</param>
        /// <param name="deep">A boolean value indicating the hydration method to use. True if the calle
        /// desires hydrated User objects. False if the user desires ShallowUser objects.</param>
        /// <param name="ErrorString">A string value containing error information.</param>
        /// <returns>An array of objects.</returns>
        //public object[] GetUsers(string SearchPattern, bool deep, out string ErrorString)
        //{
        //    TimeSpan start = TimeKeeper.GetCurrentTime();
        //    //Logger.RaiseTrace(ClassName,"Entry:"+start.ToString(),TraceLevel.Verbose);
        //    ErrorString = "";
        //    object[] objArray = null;
        //    try
        //    {
        //        OracleCommand cmd = new OracleCommand();
        //        cmd.CommandText = "GetUsers";

        //        cmd.Parameters.Add("whereClause", OracleDbType.Varchar2, 1000);
        //        cmd.Parameters["whereClause"].Value = SearchPattern;

        //        DataSet ds = GetResults(cmd,out ErrorString);
        //        if (ErrorString == "" && ds != null)
        //        {
        //            if (ds.Tables.Count > 0)
        //            {
        //                if (ds.Tables[0].Rows.Count > 0)
        //                {
        //                    if (deep)
        //                    {
        //                        objArray = ConvertUserDataTableToArray(ds.Tables[0]);
        //                    }
        //                    else
        //                    {
        //                        objArray = ConvertToShallowArray(ds.Tables[0]);
        //                    }
        //                }
        //            }
        //        }
        //        else
        //        {
        //            throw new Exception(ErrorString);
        //        }
        //    }
        //    catch(System.Exception ex)
        //    {
        //        HandleError(ex,out ErrorString);
        //    }
        //    TimeSpan end = TimeKeeper.GetCurrentTime();
        //    TimeSpan exectime = start.Subtract(end);
        //    //Logger.RaiseTrace(ClassName,"Exit:ExecutionTime="+exectime.ToString(),TraceLevel.Verbose);
        //    return objArray;
        //}

        public object[] GetUsers(string SearchPattern, bool deep, out string ErrorString) {
            string[] splitSearchPattern;
            OracleCommand cmd = new OracleCommand();
            cmd.CommandText = "USERS.GetUsers";
            cmd.CommandType = CommandType.StoredProcedure;



            TimeSpan start = TimeKeeper.GetCurrentTime();
            //Logger.RaiseTrace(ClassName,"Entry:"+start.ToString(),TraceLevel.Verbose);
            ErrorString = "";
            object[] objArray = null;
            try {

                if (SearchPattern != string.Empty || SearchPattern != "") {

                    splitSearchPattern = SearchPattern.Split('*');

                    cmd.Parameters.Add("xFirsName", OracleDbType.Varchar2, 50);
                    cmd.Parameters["xFirsName"].Value = splitSearchPattern[0];
                    cmd.Parameters.Add("xLastName", OracleDbType.Varchar2, 50);
                    cmd.Parameters["xLastName"].Value = splitSearchPattern[1];
                    cmd.Parameters.Add("xEmail", OracleDbType.Varchar2, 50);
                    cmd.Parameters["xEmail"].Value = splitSearchPattern[2];
                    cmd.Parameters.Add("xCompany", OracleDbType.Varchar2, 50);
                    cmd.Parameters["xCompany"].Value = splitSearchPattern[3];
                    cmd.Parameters.Add("xLdapId", OracleDbType.Varchar2, 50);
                    cmd.Parameters["xLdapId"].Value = splitSearchPattern[4];

                    cmd.Parameters.Add("xSiamIdentity", OracleDbType.Varchar2, 50);
                    cmd.Parameters["xSiamIdentity"].Value = splitSearchPattern[5];
                    cmd.Parameters.Add("xSubscriptionId", OracleDbType.Varchar2, 50);
                    cmd.Parameters["xSubscriptionId"].Value = splitSearchPattern[6];
                    cmd.Parameters.Add("xChecktheSearch", OracleDbType.Int32, 1);
                    cmd.Parameters["xChecktheSearch"].Value = 1;
                    //6868 start
                    cmd.Parameters.Add("xUserID", OracleDbType.Varchar2, 50);
                    cmd.Parameters["xUserID"].Value = splitSearchPattern[7];
                    //6868 end
                    cmd.Parameters.Add("xStatusCode", OracleDbType.Int32, 1);
                    cmd.Parameters["xStatusCode"].Value = Convert.ToInt32(splitSearchPattern[8]);
                } else {
                    cmd.Parameters.Add("xFirsName", OracleDbType.Varchar2, 50);
                    cmd.Parameters["xFirsName"].Value = "";
                    cmd.Parameters.Add("xLastName", OracleDbType.Varchar2, 50);
                    cmd.Parameters["xLastName"].Value = "";
                    cmd.Parameters.Add("xEmail", OracleDbType.Varchar2, 50);
                    cmd.Parameters["xEmail"].Value = "";
                    cmd.Parameters.Add("xCompany", OracleDbType.Varchar2, 50);
                    cmd.Parameters["xCompany"].Value = "";
                    cmd.Parameters.Add("xLdapId", OracleDbType.Varchar2, 50);
                    cmd.Parameters["xLdapId"].Value = "";

                    cmd.Parameters.Add("xSiamIdentity", OracleDbType.Varchar2, 50);
                    cmd.Parameters["xSiamIdentity"].Value = "";
                    cmd.Parameters.Add("xSubscriptionId", OracleDbType.Varchar2, 50);
                    cmd.Parameters["xSubscriptionId"].Value = "";
                    cmd.Parameters.Add("xChecktheSearch", OracleDbType.Int32, 1);
                    cmd.Parameters["xChecktheSearch"].Value = 0;
                    cmd.Parameters.Add("xUserID", OracleDbType.Varchar2, 50);
                    cmd.Parameters["xUserID"].Value = "";
                    cmd.Parameters.Add("xStatusCode", OracleDbType.Int32, 1);
                    cmd.Parameters["xStatusCode"].Value = 4;

                }

                //      Dim xFunctionalityInfo As New OracleParameter("xPromoInfo", OracleDbType.RefCursor)
                //xFunctionalityInfo.Direction = ParameterDirection.Output
                //cmd.Parameters.Add(xFunctionalityInfo)

                OracleParameter xUserfo = new OracleParameter("xUserfo", OracleDbType.RefCursor);
                xUserfo.Direction = ParameterDirection.Output;

                cmd.Parameters.Add(xUserfo);
                if (cn.State != System.Data.ConnectionState.Open) {
                    cn.Open();
                }

                cmd.Connection = cn;
                DataSet ds = new DataSet();
                OracleDataAdapter oDadapter = new OracleDataAdapter(cmd);
                oDadapter.Fill(ds);

                //  DataSet ds = GetResults(cmd,out ErrorString);
                if (ds != null) {
                    if (ds.Tables.Count > 0) {
                        if (ds.Tables[0].Rows.Count > 0) {
                            if (deep) {
                                objArray = ConvertUserDataTableToArray(ds.Tables[0]);
                            } else {
                                objArray = ConvertToShallowArray(ds.Tables[0]);
                            }
                        }
                    }
                } else {
                    throw new Exception(ErrorString);
                }
            } catch (Exception ex) {
                HandleError(ex, out ErrorString);
            }
            TimeSpan end = TimeKeeper.GetCurrentTime();
            TimeSpan exectime = start.Subtract(end);
            //Logger.RaiseTrace(ClassName,"Exit:ExecutionTime="+exectime.ToString(),TraceLevel.Verbose);
            return objArray;
        }


        #endregion

        #region Delete Methods


        /// <summary>
        /// Deletes a UserRole from the system.
        /// </summary>
        /// <param name="UserId">The associated userid</param>
        /// <param name="RoleId">The roleid</param>
        /// <param name="ErrorString">A string value containing error inforamtion</param>
        /// <returns>siam.response</returns>
        public bool DeleteUserRole(string UserId, int RoleId, out string ErrorString) {
            TimeSpan start = TimeKeeper.GetCurrentTime();
            //Logger.RaiseTrace(ClassName,"Entry:"+start.ToString(),TraceLevel.Verbose);
            ErrorString = "";
            bool retval = false;
            try {
                OracleCommand cmd = new OracleCommand();
                cmd.CommandText = "DeleteUserRole";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add("xUserId", OracleDbType.Int32, 22);
                cmd.Parameters["xUserId"].Value = UserId;
                cmd.Parameters.Add("xRoleId", OracleDbType.Int32, 22);
                cmd.Parameters["xRoleId"].Value = RoleId;
                ExecuteCMD(cmd, out ErrorString);
                retval = true;
            } catch (Exception ex) {
                HandleError(ex, out ErrorString);
            }
            TimeSpan end = TimeKeeper.GetCurrentTime();
            TimeSpan exectime = start.Subtract(end);
            //Logger.RaiseTrace(ClassName,"Exit:ExecutionTime="+exectime.ToString(),TraceLevel.Verbose);
            return retval;
        }

        /// <summary>
        /// DeleteAction: Deletes an Action Entity from the database.
        /// </summary>
        /// <param name="ActionId">The Action Id</param>
        /// <param name="ErrorString">A string value containing error inforamtion</param>
        /// <returns>A boolean value indicating success or failure</returns>
        /// //6732 starts
        /*public bool DeleteAction(int ActionId,out string ErrorString)
		{
			TimeSpan start = TimeKeeper.GetCurrentTime();
			//Logger.RaiseTrace(ClassName,"Entry:"+start.ToString(),TraceLevel.Verbose);
			ErrorString = "";
			bool retval = false;
			try
			{
				OracleCommand cmd = new OracleCommand();
				cmd.CommandText = "DeleteAction";
				cmd.CommandType = System.Data.CommandType.StoredProcedure;
				cmd.Parameters.Add("xActionId",OracleDbType.Int32,22);
				cmd.Parameters["xActionId"].Value = ActionId;
				ExecuteCMD(cmd,out ErrorString);
				retval = true;
			}
			catch(System.Exception ex)
			{
				HandleError(ex,out ErrorString);
			}
			TimeSpan end = TimeKeeper.GetCurrentTime();
			TimeSpan exectime = start.Subtract(end);
			//Logger.RaiseTrace(ClassName,"Exit:ExecutionTime="+exectime.ToString(),TraceLevel.Verbose);
			return retval;
		}*/
        //6732 ends

        /// <summary>
        /// DeleteApplication: Deletes an Application Entity from the database.
        /// </summary>
        /// <param name="ApplicationId">The Application Id</param>
        /// <param name="ErrorString">A string value containing error inforamtion</param>
        /// <returns>A boolean value indicating success or failure</returns>
        public bool DeleteApplication(int ApplicationId, out string ErrorString) {
            TimeSpan start = TimeKeeper.GetCurrentTime();
            //Logger.RaiseTrace(ClassName,"Entry:"+start.ToString(),TraceLevel.Verbose);
            ErrorString = "";
            bool retval = false;
            try {
                OracleCommand cmd = new OracleCommand();
                cmd.CommandText = "DeleteApplication";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add("xApplicationId", OracleDbType.Int32, 22);
                cmd.Parameters["xApplicationId"].Value = ApplicationId;
                ExecuteCMD(cmd, out ErrorString);
                retval = true;
            } catch (Exception ex) {
                HandleError(ex, out ErrorString);
            }
            TimeSpan end = TimeKeeper.GetCurrentTime();
            TimeSpan exectime = start.Subtract(end);
            //Logger.RaiseTrace(ClassName,"Exit:ExecutionTime="+exectime.ToString(),TraceLevel.Verbose);
            return retval;
        }


        /// <summary>
        /// Deletes an application resource entity from the database.
        /// </summary>
        /// <param name="ApplicationId">The Application Id</param>
        /// <param name="ResourceId">The resource Id</param>
        /// <param name="ErrorString">A string value containing error inforamtion</param>
        /// <returns>A boolean value indicating success or failure</returns>
        public bool DeleteApplicationResource(int ApplicationId, int ResourceId, out string ErrorString) {
            TimeSpan start = TimeKeeper.GetCurrentTime();
            //Logger.RaiseTrace(ClassName,"Entry:"+start.ToString(),TraceLevel.Verbose);
            ErrorString = "";
            bool retval = false;
            try {
                OracleCommand cmd = new OracleCommand();
                cmd.CommandText = "DeleteApplicationResource";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add("xApplicationId", OracleDbType.Int32, 22);
                cmd.Parameters["xApplicationId"].Value = ApplicationId;
                cmd.Parameters.Add("xResourceId", OracleDbType.Int32, 22);
                cmd.Parameters["xResourceId"].Value = ResourceId;
                ExecuteCMD(cmd, out ErrorString);
                retval = true;
            } catch (Exception ex) {
                HandleError(ex, out ErrorString);
            }
            TimeSpan end = TimeKeeper.GetCurrentTime();
            TimeSpan exectime = start.Subtract(end);
            //Logger.RaiseTrace(ClassName,"Exit:ExecutionTime="+exectime.ToString(),TraceLevel.Verbose);
            return retval;
        }



        /// <summary>
        /// Deletes a resource entity from the Database.
        /// </summary>
        /// <param name="ResourceId">The resource Id</param>
        /// <param name="ErrorString">A string value containing error inforamtion</param>
        /// <returns>A boolean value indicating success or failure</returns>
        public bool DeleteResource(int ResourceId, out string ErrorString) {
            TimeSpan start = TimeKeeper.GetCurrentTime();
            //Logger.RaiseTrace(ClassName,"Entry:"+start.ToString(),TraceLevel.Verbose);
            ErrorString = "";
            bool retval = false;
            try {
                OracleCommand cmd = new OracleCommand();
                cmd.CommandText = "DeleteResource";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add("xResourceId", OracleDbType.Int32, 22);
                cmd.Parameters["xResourceId"].Value = ResourceId;
                ExecuteCMD(cmd, out ErrorString);
                retval = true;
            } catch (Exception ex) {
                HandleError(ex, out ErrorString);
            }
            TimeSpan end = TimeKeeper.GetCurrentTime();
            TimeSpan exectime = start.Subtract(end);
            //Logger.RaiseTrace(ClassName,"Exit:ExecutionTime="+exectime.ToString(),TraceLevel.Verbose);
            return retval;
        }



        /// <summary>
        /// Deletes a resource action entity from the database
        /// </summary>
        /// <param name="ResourceId">The resource Id</param>
        /// <param name="ActionId">The Action Id</param>
        /// <param name="ErrorString">A string value containing error inforamtion</param>
        /// <returns>A boolean value indicating success or failure</returns>
        public bool DeleteResourceAction(int ResourceId, int ActionId, out string ErrorString) {
            TimeSpan start = TimeKeeper.GetCurrentTime();
            //Logger.RaiseTrace(ClassName,"Entry:"+start.ToString(),TraceLevel.Verbose);
            ErrorString = "";
            bool retval = false;
            try {
                OracleCommand cmd = new OracleCommand();
                cmd.CommandText = "DeleteResourceAction";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add("xResourceId", OracleDbType.Int32, 22);
                cmd.Parameters["xResourceId"].Value = ResourceId;
                cmd.Parameters.Add("xActionId", OracleDbType.Int32, 22);
                cmd.Parameters["xActionId"].Value = ActionId;
                ExecuteCMD(cmd, out ErrorString);
                retval = true;
            } catch (Exception ex) {
                HandleError(ex, out ErrorString);
            }
            TimeSpan end = TimeKeeper.GetCurrentTime();
            TimeSpan exectime = start.Subtract(end);
            //Logger.RaiseTrace(ClassName,"Exit:ExecutionTime="+exectime.ToString(),TraceLevel.Verbose);
            return retval;
        }



        /// <summary>
        /// Deletes a role entity from the data base.
        /// </summary>
        /// <param name="RoleId">The role Id</param>
        /// <param name="ErrorString">A string value containing error inforamtion</param>
        /// <returns>A boolean value indicating success or failure</returns>
        public bool DeleteRole(int RoleId, out string ErrorString) {
            TimeSpan start = TimeKeeper.GetCurrentTime();
            //Logger.RaiseTrace(ClassName,"Entry:"+start.ToString(),TraceLevel.Verbose);
            ErrorString = "";
            bool retval = false;
            try {
                OracleCommand cmd = new OracleCommand();
                cmd.CommandText = "DeleteRole";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add("xRoleId", OracleDbType.Int32, 22);
                cmd.Parameters["xRoleId"].Value = RoleId;
                ExecuteCMD(cmd, out ErrorString);
                retval = true;
            } catch (Exception ex) {
                HandleError(ex, out ErrorString);
            }
            TimeSpan end = TimeKeeper.GetCurrentTime();
            TimeSpan exectime = start.Subtract(end);
            //Logger.RaiseTrace(ClassName,"Exit:ExecutionTime="+exectime.ToString(),TraceLevel.Verbose);
            return retval;
        }



        /// <summary>
        /// Deletes a user entity from the database
        /// </summary>
        /// <param name="UserId">The user id</param>
        /// <param name="ErrorString">A string value containing error inforamtion</param>
        /// <returns>A boolean value indicating success or failure</returns>
        public bool DeleteUser(string UserId, out string ErrorString, out User oUser) {
            TimeSpan start = TimeKeeper.GetCurrentTime();
            //Logger.RaiseTrace(ClassName,"Entry:"+start.ToString(),TraceLevel.Verbose);
            ErrorString = "";
            bool retval = false;
            oUser = null;
            try {
                oUser = GetUser(UserId, out ErrorString);
                OracleCommand cmd = new OracleCommand();
                cmd.CommandText = "DeleteUser";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add("xUserId", OracleDbType.Varchar2, 20);
                cmd.Parameters["xUserId"].Value = UserId;
                ExecuteCMD(cmd, out ErrorString);
                retval = true;
            } catch (Exception ex) {
                HandleError(ex, out ErrorString);
            }
            TimeSpan end = TimeKeeper.GetCurrentTime();
            TimeSpan exectime = start.Subtract(end);
            //Logger.RaiseTrace(ClassName,"Exit:ExecutionTime="+exectime.ToString(),TraceLevel.Verbose);
            return retval;
        }

        #endregion

        #region Maintenence Methods

        private void purgeFailedEmail(string EmailAddress) {
            TimeSpan start = TimeKeeper.GetCurrentTime();
            //Logger.RaiseTrace(ClassName,"Entry:"+start.ToString(),TraceLevel.Verbose);
            string ErrorString = "";
            try {
                OracleCommand cmd = new OracleCommand();
                cmd.CommandText = "PurgeEmailFailure";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;

                cmd.Parameters.Add("xEmailAddress", OracleDbType.Varchar2, 50);
                cmd.Parameters["xEmailAddress"].Value = EmailAddress;

                ExecuteCMD(cmd, out ErrorString);
            } catch (Exception ex) {
                HandleError(ex, out ErrorString);
            }
            TimeSpan end = TimeKeeper.GetCurrentTime();
            TimeSpan exectime = start.Subtract(end);
            //Logger.RaiseTrace(ClassName,"Exit:ExecutionTime="+exectime.ToString(),TraceLevel.Verbose);
        }


        /// <summary>
        /// 
        /// </summary>
        public void ReTryEmailFailure() {
            TimeSpan start = TimeKeeper.GetCurrentTime();
            //Logger.RaiseTrace(ClassName,"Entry:"+start.ToString(),TraceLevel.Verbose);
            System.Data.DataSet ds;
            string ErrorString = "";
            try {
                OracleCommand cmd = new OracleCommand();
                cmd.CommandText = "GetEmailFailures";
                ds = GetResults(cmd, out ErrorString);
                if (ErrorString == "" && ds != null && ds.Tables != null && ds.Tables.Count > 0) {
                    Mailer mailer = new Mailer();
                    foreach (System.Data.DataRow row in ds.Tables[0].Rows) {
                        purgeFailedEmail(row["msgto"].ToString());
                        mailer.SendMail(row["msg"].ToString(), row["MsgTo"].ToString(), row["MsgFrom"].ToString(), row["subject"].ToString(), true);
                    }
                }
            } catch (Exception ex) {
                HandleError(ex, out ErrorString);
            }
            TimeSpan end = TimeKeeper.GetCurrentTime();
            TimeSpan exectime = start.Subtract(end);
            //Logger.RaiseTrace(ClassName,"Exit:ExecutionTime="+exectime.ToString(),TraceLevel.Verbose);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="FirstName"></param>
        /// <param name="LastName"></param>
        /// <param name="EmailAddress"></param>
        /// <param name="newStatusName"></param>
        /// <param name="LDAPUID"></param>
        /// <param name="ErrorString"></param>
        /// <returns></returns>
        //6668 starts
        /*public bool UpdateApprovalStatus(string FirstName,string LastName,string EmailAddress,string newStatusName,string LDAPUID,out string ErrorString)
		{
			TimeSpan start = TimeKeeper.GetCurrentTime();
			//Logger.RaiseTrace(ClassName,"Entry:"+start.ToString(),TraceLevel.Verbose);
            
            ErrorString = "";
			bool retval = false;
			string outComeMessage = FirstName.ToString() + ";" + LastName.ToString() + ";" + EmailAddress.ToString() + ";" + newStatusName.ToString() + ";" + LDAPUID.ToString();
			string UserName = string.Empty;
			
			try
			{
				OracleCommand cmd = new OracleCommand();
				cmd.CommandText = "UpdateApprovalStatus";
				cmd.CommandType = System.Data.CommandType.StoredProcedure;
				cmd.Parameters.Add("xCallBackDate",OracleDbType.Char,20);
				cmd.Parameters["xCallBackDate"].Value = System.DateTime.Now.ToShortDateString();
				cmd.Parameters.Add("xOutCome",OracleDbType.Char,100);
				cmd.Parameters["xOutCome"].Value = outComeMessage;
				cmd.Parameters.Add("xFirstName",OracleDbType.Char,50);
				cmd.Parameters["xFirstName"].Value = FirstName;
				cmd.Parameters.Add("xLastName",OracleDbType.Char,50);
				cmd.Parameters["xLastName"].Value = LastName;
				cmd.Parameters.Add("xEmailAddress",OracleDbType.Char,50);
				cmd.Parameters["xEmailAddress"].Value = EmailAddress;
				cmd.Parameters.Add("xNewStatusName",OracleDbType.Char,20);
				cmd.Parameters["xNewStatusName"].Value = newStatusName;
				cmd.Parameters.Add("xLDAPUID",OracleDbType.Char,20);
				cmd.Parameters["xLDAPUID"].Value = LDAPUID;				
				cmd.Parameters.Add("xUserName", OracleDbType.Char,50);				
				cmd.Parameters["xUserName"].Direction = ParameterDirection.Output;
				
				ExecuteCMD(cmd,out ErrorString);
				
				UserName = cmd.Parameters["xUserName"].Value.ToString().Trim();
                if (UserName == "NOT FOUND")
                {
                    newStatusName = "Pending";
                    throw new ApplicationException("No record found for First Name:" + FirstName + " Last Name:" + LastName + " and Email Address:" + EmailAddress);
                }
                else
                {

                    if (LDAPUID == null || LDAPUID == "")
                    {
                        outComeMessage = "Rejected";
                        newStatusName = "Pending";
                        SendRejectedEmail(FirstName, LastName, EmailAddress);
                    }
                    else
                    {
                        outComeMessage = "Approved";
                        SendApprovedEmail(FirstName, LastName, EmailAddress, UserName);
                    }
                }
				
				retval = true;
			}
			catch(System.Exception ex)
			{
                retval = false;
				HandleError(ex,out ErrorString);
				//send email to me for testing
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                if (UserName == "NOT FOUND")
                    sb.Append("UserName: " + "No User found for following record" + "<br>");
                else
                    sb.Append("UserName: " + UserName.ToString() + "<br>");

				sb.Append("FirstName: " + FirstName.ToString() + "<br>");
				sb.Append("LastName: " + LastName.ToString() + "<br>");
				sb.Append("EmailAddress: " + EmailAddress.ToString() + "<br>");
				sb.Append("LDAPUID: " + LDAPUID.ToString() + "<br>");
				sb.Append("Account Status: " + newStatusName.ToString() + "<br>");
				if (ErrorString != null) 
				{									 
					sb.Append("ErrorString: " + ErrorString.ToString() + "<br><br>");
				}
				sb.Append(ex.Message.ToString() + "<br>");
				
				new Mailer().SendMail(sb.ToString(),"Richard.Upton@am.sony.com","ProParts@am.sony.com","Error in UpdateApprovalStatus!",System.Web.Mail.MailFormat.Html);
			}						
			
			TimeSpan end = TimeKeeper.GetCurrentTime();
			TimeSpan exectime = start.Subtract(end);
			//Logger.RaiseTrace(ClassName,"Exit:ExecutionTime="+exectime.ToString(),TraceLevel.Verbose);
			return retval;
		}*/
        //6668 ends

        /// <summary>
        /// After LDAP call return message will be updated into ISSAWEBSRVCALLS table in SIAM
        /// </summary>
        /// <param name="FirstName">B2B first Name</param>
        /// <param name="LastName">B2B Last Name</param>
        /// <param name="EmailAddress">B2B Email Address</param>
        /// <param name="outComeMessage">Message to be updated after LDAP Call</param>
        /// <param name="ErrorString">Out Error string</param>
        /// <returns>Boolean Success Message</returns>
        //public bool UpdateISSAWebCallMessage(string FirstName, string LastName, string EmailAddress, string outComeMessage, out string ErrorString)
        //{
        //    TimeSpan start = TimeKeeper.GetCurrentTime();
        //    //Logger.RaiseTrace(ClassName, "Entry:" + start.ToString(), TraceLevel.Verbose);
        //    ErrorString = "";
        //    bool retval = false;
        //    string UserName = string.Empty;

        //    try
        //    {
        //        OracleCommand cmd = new OracleCommand();
        //        cmd.CommandText = "UpdateISSAWebCallMessage";
        //        cmd.CommandType = System.Data.CommandType.StoredProcedure;
        //        cmd.Parameters.Add("xOutCome", OracleDbType.Varchar2, 20);
        //        cmd.Parameters["xOutCome"].Value = outComeMessage;
        //        cmd.Parameters.Add("xFirstName", OracleDbType.Char, 20);
        //        cmd.Parameters["xFirstName"].Value = FirstName;
        //        cmd.Parameters.Add("xLastName", OracleDbType.Char, 20);
        //        cmd.Parameters["xLastName"].Value = LastName;
        //        cmd.Parameters.Add("xEmailAddress", OracleDbType.Char, 50);
        //        cmd.Parameters["xEmailAddress"].Value = EmailAddress;
        //        cmd.Parameters.Add("xUserID", OracleDbType.Varchar2, 25);
        //        cmd.Parameters["xUserID"].Direction = ParameterDirection.Output;

        //        ExecuteCMD(cmd, out ErrorString);

        //        if (cmd.Parameters["xUserID"].Value != System.DBNull.Value)
        //        {
        //            UserName = cmd.Parameters["xUserID"].Value.ToString().Trim();
        //            if (UserName == "F")
        //            {
        //                retval = false;
        //            }
        //            else
        //            { retval = true; }
        //        }
        //        else
        //        { retval = false; }
        //    }
        //    catch (System.Exception ex)
        //    {
        //        HandleError(ex, out ErrorString);
        //        //send email to me for testing
        //        System.Text.StringBuilder sb = new System.Text.StringBuilder();
        //        sb.Append("UserName: " + UserName.ToString() + "<br>");
        //        sb.Append("FirstName: " + FirstName.ToString() + "<br>");
        //        sb.Append("LastName: " + LastName.ToString() + "<br>");
        //        sb.Append("EmailAddress: " + EmailAddress.ToString() + "<br>");
        //        if (ErrorString != null)
        //        {
        //            sb.Append("ErrorString: " + ErrorString.ToString() + "<br><br>");
        //        }
        //        sb.Append(ex.Message.ToString() + "<br>");

        //        new Mailer().SendMail(sb.ToString(), "chandra.maurya@am.sony.com", "UpdateISSAWebCallMessage@am.sony.com", "Error in UpdateISSAWebCallMessage!", System.Web.Mail.MailFormat.Html);
        //        ErrorString = ex.Message;
        //    }

        //    TimeSpan end = TimeKeeper.GetCurrentTime();
        //    TimeSpan exectime = start.Subtract(end);
        //    //Logger.RaiseTrace(ClassName, "Exit:ExecutionTime=" + exectime.ToString(), TraceLevel.Verbose);
        //    return retval;
        //}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="user"></param>
        /// <param name="NewCredentials"></param>
        /// <param name="ExpiresInDays"></param>
        /// <param name="ErrorString"></param>
        /// <returns></returns>
        public bool UpdateCredentials(User user, Credential NewCredentials, int ExpiresInDays, out string ErrorString) {
            TimeSpan start = TimeKeeper.GetCurrentTime();
            //Logger.RaiseTrace(ClassName,"Entry:"+start.ToString(),TraceLevel.Verbose);
            ErrorString = "";
            bool retval = false;
            //try
            //{
            //    // Has the new password been used before.
            //    if (CheckPasswordHistory(NewCredentials.Password,user.Identity,out ErrorString))
            //    {
            //        OracleCommand cmd = new OracleCommand();
            //        cmd.CommandText = "UpdateCredentials";
            //        cmd.CommandType = System.Data.CommandType.StoredProcedure;

            //        cmd.Parameters.Add("xIdentity",OracleDbType.Varchar2,20);
            //        cmd.Parameters["xIdentity"].Value = user.Identity;

            //        cmd.Parameters.Add("xOldUserId",OracleDbType.Varchar2,50);
            //        cmd.Parameters["xOldUserId"].Value = user.UserId;

            //        cmd.Parameters.Add("xOldPassword",OracleDbType.Varchar2,50);
            //        //cmd.Parameters["xOldPassword"].Value = Encryption.Encrypt(user.Password);
            //        cmd.Parameters["xOldPassword"].Value = Encryption.Encrypt(user.Password,"test key");

            //        if (NewCredentials.UserName == null || NewCredentials.UserName == "")
            //        {
            //            cmd.Parameters.Add("xNewUserId",OracleDbType.Varchar2,50);
            //            cmd.Parameters["xNewUserId"].Value = user.UserId;
            //        }
            //        else
            //        {
            //            cmd.Parameters.Add("xNewUserId",OracleDbType.Varchar2,50);
            //            cmd.Parameters["xNewUserId"].Value = NewCredentials.UserName;
            //        }

            //        if (NewCredentials.Password == null || NewCredentials.Password == "")
            //        {
            //            cmd.Parameters.Add("xNewPassword",OracleDbType.Varchar2,50);
            //            //cmd.Parameters["xNewPassword"].Value = Encryption.Encrypt(user.Password);
            //            cmd.Parameters["xNewPassword"].Value = Encryption.Encrypt(user.Password,"test key");
            //        }
            //        else
            //        {
            //            cmd.Parameters.Add("xNewPassword",OracleDbType.Varchar2,50);
            //            //cmd.Parameters["xNewPassword"].Value = Encryption.Encrypt(NewCredentials.Password);
            //            cmd.Parameters["xNewPassword"].Value = Encryption.Encrypt(NewCredentials.Password,"test key");
            //        }
            //        //-- the application no longer looking at the expiration date but I didn't want to remove this code because
            //        //-- i didn't want to change the stored proc. - dwd
            //        string ExpiresIn = System.DateTime.Now.Add(new System.TimeSpan(ExpiresInDays,0,0,0,0)).ToShortDateString();
            //        cmd.Parameters.Add("xNewExpirationDate",OracleDbType.Varchar2,20);
            //        cmd.Parameters["xNewExpirationDAte"].Value = ExpiresIn;
            //        ExecuteCMD(cmd,out ErrorString);
            //        retval = true;
            //    }
            //    else
            //    {
            //        // this password has been used before
            //        throw new System.Exception("Password reuse exception: The new password specified has been used as one of the last three user passwords.");
            //    }
            //}
            //catch(System.Exception ex)
            //{
            //    HandleError(ex,out ErrorString);
            //}
            //TimeSpan end = TimeKeeper.GetCurrentTime();
            //TimeSpan exectime = start.Subtract(end);
            ////Logger.RaiseTrace(ClassName,"Exit:ExecutionTime="+exectime.ToString(),TraceLevel.Verbose);
            return retval;
        }

        /// <summary>
        /// Lock Account set the lock status to Locked for the user passed in.
        /// </summary>
        /// <param name="user">An instance of sony.us.siam.User</param>
        /// <param name="ErrorString">A string value containing error information</param>
        /// <returns>A boolean value indicating success or failure</returns>
        public bool LockAccount(User user, out string ErrorString) {
            TimeSpan start = TimeKeeper.GetCurrentTime();
            //Logger.RaiseTrace(ClassName,"Entry:"+start.ToString(),TraceLevel.Verbose);
            ErrorString = "";
            bool retval = false;
            try {
                OracleCommand cmd = new OracleCommand();
                cmd.CommandText = "LockAccount";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add("xUserId", OracleDbType.Varchar2, 20);
                cmd.Parameters["xUserId"].Value = user.UserId;

                cmd.Parameters.Add("xRetAlternateID", OracleDbType.Varchar2, 20);
                cmd.Parameters["xRetAlternateID"].Direction = ParameterDirection.Output;    // Was .InputOutput

                cmd.Parameters.Add("xRetUserType", OracleDbType.Varchar2, 20);
                cmd.Parameters["xRetUserType"].Direction = ParameterDirection.Output;    // Was .InputOutput

                ExecuteCMD(cmd, out ErrorString);
                if (!((INullable)cmd.Parameters["retVal"].Value).IsNull) {
                    user.UserId = cmd.Parameters["retVal"].Value.ToString();
                }

                if (!((INullable)cmd.Parameters["xRetAlternateID"].Value).IsNull) {
                    user.AlternateUserId = cmd.Parameters["xRetAlternateID"].Value.ToString();
                }
                if (!((INullable)cmd.Parameters["xRetUserType"].Value).IsNull) {
                    user.UserType = cmd.Parameters["xRetUserType"].Value.ToString();
                }
                retval = true;
            } catch (Exception ex) {
                HandleError(ex, out ErrorString);
            }
            TimeSpan end = TimeKeeper.GetCurrentTime();
            TimeSpan exectime = start.Subtract(end);
            //Logger.RaiseTrace(ClassName,"Exit:ExecutionTime="+exectime.ToString(),TraceLevel.Verbose);
            return retval;
        }


        /// <summary>
        /// UnLockAccount. Sets the LockStatus to Unlocked for the user passed in.
        /// </summary>
        /// <param name="user">An instance of sony.us.siam.User</param>
        /// <param name="ErrorString">A string value containing any error information.</param>
        /// <returns>A boolean value indicating success or failure.</returns>
        public bool UnLockAccount(User user, out string ErrorString) {
            TimeSpan start = TimeKeeper.GetCurrentTime();
            //Logger.RaiseTrace(ClassName,"Entry:"+start.ToString(),TraceLevel.Verbose);
            ErrorString = "";
            bool retval = false;
            try {
                OracleCommand cmd = new OracleCommand();
                cmd.CommandText = "UnLockAccount";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add("xUserId", OracleDbType.Varchar2, 20);
                cmd.Parameters["xUserId"].Value = user.Identity;
                ExecuteCMD(cmd, out ErrorString);
                retval = true;
            } catch (Exception ex) {
                HandleError(ex, out ErrorString);
            }
            TimeSpan end = TimeKeeper.GetCurrentTime();
            TimeSpan exectime = start.Subtract(end);
            //Logger.RaiseTrace(ClassName,"Exit:ExecutionTime="+exectime.ToString(),TraceLevel.Verbose);
            return retval;
        }

        #endregion

        #region Private and Helper Methods

        /// <summary>
        /// 
        /// </summary>
        /// <param name="FirstName"></param>
        /// <param name="LastName"></param>
        /// <param name="EmailAddress"></param>
        private void SendRejectedEmail(string FirstName, string LastName, string EmailAddress) {
            //Sony.US.siam.SIAMConfig config = CachingServices.Cache["SiamConfiguration"] as SIAMConfig;
            //if (config == null)
            //{
            //    config = new SIAMConfig();
            //    CachingServices.Cache["SiamConfiguration"] = config;
            //}
            System.IO.FileStream fStream = new System.IO.FileStream(SystemVariables.RunningApplicationPath + @"\EmailTemplates\" + ConfigurationData.GeneralSettings.Template.B2BRejectedTemplate, System.IO.FileMode.Open, System.IO.FileAccess.Read);
            byte[] buffer = new byte[fStream.Length];
            fStream.Read(buffer, 0, buffer.Length);
            fStream.Close();
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            for (int x = 0; x <= buffer.GetUpperBound(0); x++) {
                sb.Append((char)buffer[x]);
            }
            sb.Replace("~FirstName~", FirstName);
            sb.Replace("~LastName~", LastName);
            new Mailer().SendMail(sb.ToString(), EmailAddress, "ProParts@am.sony.com", "ServicesPLUS(sm) -Thank you for registering", true, ConfigurationData.Environment.SMTP.EmailServer_CS);

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="FirstName"></param>
        /// <param name="LastName"></param>
        /// <param name="EmailAddress"></param>
        /// <param name="UserName"></param>
        private void SendApprovedEmail(string FirstName, string LastName, string EmailAddress, string UserName) {
            //Sony.US.siam.SIAMConfig config = CachingServices.Cache["SiamConfiguration"] as SIAMConfig;
            //if (config == null)
            //{
            //    config = new SIAMConfig();
            //    CachingServices.Cache["SiamConfiguration"] = config;
            //}
            System.IO.FileStream fStream = new System.IO.FileStream(SystemVariables.RunningApplicationPath + @"\EmailTemplates\" + ConfigurationData.GeneralSettings.Template.B2BApprovalTemplate, System.IO.FileMode.Open, System.IO.FileAccess.Read);
            byte[] buffer = new byte[fStream.Length];
            fStream.Read(buffer, 0, buffer.Length);
            fStream.Close();
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            for (int x = 0; x <= buffer.GetUpperBound(0); x++) {
                sb.Append((char)buffer[x]);
            }

            sb.Replace("~FirstName~", FirstName);
            sb.Replace("~LastName~", LastName);
            sb.Replace("~UserName~", UserName);
            new Mailer().SendMail(sb.ToString(), EmailAddress, "ProParts@am.sony.com", "ServicesPLUS(sm) -Thank you for registering", true, ConfigurationData.Environment.SMTP.EmailServer_CS);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="ExceptionMessage"></param>
        public void LogEmailFailure(System.Net.Mail.MailMessage msg, string ExceptionMessage) {
            TimeSpan start = TimeKeeper.GetCurrentTime();
            //Logger.RaiseTrace(ClassName,"Entry:"+start.ToString(),TraceLevel.Verbose);
            string ErrorString = "";
            double retval;
            try {
                var cmd = new OracleCommand("addemailmsgfailure");

                cmd.Parameters.Add("xMsgFrom", OracleDbType.Varchar2, 50);
                cmd.Parameters["xMsgFrom"].Value = msg.From;

                cmd.Parameters.Add("xMsgTo", OracleDbType.Varchar2, 50);
                cmd.Parameters["xMsgTo"].Value = msg.To;

                cmd.Parameters.Add("xSubject", OracleDbType.Varchar2, 50);
                cmd.Parameters["xSubject"].Value = msg.Subject;

                cmd.Parameters.Add("xMsgText", OracleDbType.Varchar2, 4000);
                cmd.Parameters["xMsgText"].Value = msg.Body;

                cmd.Parameters.Add("xFailureMsg", OracleDbType.Varchar2, 200);
                cmd.Parameters["xFailureMsg"].Value = ExceptionMessage;

                //cmd.Parameters.Add("xLastAttempt",OracleDbType.Varchar2,200);//6969
                cmd.Parameters.Add("xLastAttempt", OracleDbType.Date);//6969
                cmd.Parameters["xLastAttempt"].Value = DateTime.Now;     // ASleight - This extra isn't necessary.     .ToShortDateString() + " " + System.DateTime.Now.ToLongTimeString();

                retval = ExecuteCMD(cmd, out ErrorString);
            } catch (Exception ex) {
                HandleError(ex, out ErrorString);
                retval = -1;
            }
            TimeSpan end = TimeKeeper.GetCurrentTime();
            TimeSpan exectime = start.Subtract(end);
            //Logger.RaiseTrace(ClassName,"Exit:ExecutionTime="+exectime.ToString(),TraceLevel.Verbose);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="newPassword"></param>
        /// <param name="UserIdentity"></param>
        /// <param name="ErrorString"></param>
        /// <returns></returns>
        private bool CheckPasswordHistory(string newPassword, string UserIdentity, out string ErrorString) {
            TimeSpan start = TimeKeeper.GetCurrentTime();
            //Logger.RaiseTrace(ClassName,"Entry:"+start.ToString(),TraceLevel.Verbose);
            System.Data.DataSet ds;
            ErrorString = "";
            string[] passwords = null;
            bool result = true;
            try {
                OracleCommand cmd = new OracleCommand();
                cmd.CommandText = "Getpasswordhistory";
                cmd.Parameters.Add("xUserId", OracleDbType.Varchar2, 20);
                cmd.Parameters["xUserId"].Value = UserIdentity;
                ds = GetResults(cmd, out ErrorString);
                if (ErrorString == "") {
                    if (ds != null && ds.Tables.Count > 0) {
                        if (ds.Tables[0].Rows.Count > 0) {
                            passwords = new string[4];
                            if (ds.Tables[0].Columns.Contains("PreviousPassword")) {
                                //passwords[0] = Encryption.DeCrypt(ds.Tables[0].Rows[0]["PreviousPassword"].ToString(),null,null);
                                passwords[0] = Encryption.DeCrypt(ds.Tables[0].Rows[0]["PreviousPassword"].ToString(), "test key");
                            } else {
                                passwords[0] = "";
                            }
                            if (ds.Tables[0].Columns.Contains("PreviousPassword1")) {
                                //passwords[1] = Encryption.DeCrypt(ds.Tables[0].Rows[0]["PreviousPassword1"].ToString(),null,null);
                                passwords[1] = Encryption.DeCrypt(ds.Tables[0].Rows[0]["PreviousPassword1"].ToString(), "test key");
                            } else {
                                passwords[1] = "";
                            }
                            if (ds.Tables[0].Columns.Contains("PreviousPassword2")) {
                                //passwords[2] = Encryption.DeCrypt(ds.Tables[0].Rows[0]["PreviousPassword2"].ToString(),null,null);
                                passwords[2] = Encryption.DeCrypt(ds.Tables[0].Rows[0]["PreviousPassword2"].ToString(), "test key");
                            } else {
                                passwords[2] = "";
                            }
                            if (ds.Tables[0].Columns.Contains("PreviousPassword3")) {
                                //passwords[3] = Encryption.DeCrypt(ds.Tables[0].Rows[0]["PreviousPassword3"].ToString(),null,null);
                                passwords[3] = Encryption.DeCrypt(ds.Tables[0].Rows[0]["PreviousPassword3"].ToString(), "test key");
                            } else {
                                passwords[3] = "";
                            }
                            if (newPassword == passwords[0] || newPassword == passwords[1] || newPassword == passwords[2] || newPassword == passwords[3]) {
                                result = false;
                            } else {
                                result = true;
                            }
                        } else {
                            result = true;
                        }
                    } else {
                        result = true;
                    }
                } else {
                    throw new Exception(ErrorString);
                }
            } catch (Exception ex) {
                HandleError(ex, out ErrorString);
            }
            TimeSpan end = TimeKeeper.GetCurrentTime();
            TimeSpan exectime = start.Subtract(end);
            //Logger.RaiseTrace(ClassName,"Exit:ExecutionTime="+exectime.ToString(),TraceLevel.Verbose);
            return result;
        }
        public bool AddISSAWebCall(User user, string Message, out string ErrorString) {
            TimeSpan start = TimeKeeper.GetCurrentTime();
            //Logger.RaiseTrace(ClassName,"Entry:"+start.ToString(),TraceLevel.Verbose);
            ErrorString = "";
            double retval;
            try {
                OracleCommand cmd = new OracleCommand();
                cmd.CommandText = "addISSAWebSrvCall";

                cmd.Parameters.Add("xCallDate", OracleDbType.Varchar2, 20);
                cmd.Parameters["xCallDate"].Value = System.DateTime.Now.ToShortDateString();

                cmd.Parameters.Add("xUserIdentity", OracleDbType.Varchar2, 20);
                cmd.Parameters["xUserIdentity"].Value = user.UserId;

                cmd.Parameters.Add("xFirstName", OracleDbType.Varchar2, 25);
                cmd.Parameters["xFirstName"].Value = user.FirstName;

                cmd.Parameters.Add("xLastName", OracleDbType.Varchar2, 25);
                cmd.Parameters["xLastName"].Value = user.LastName;

                cmd.Parameters.Add("xEmailAddress", OracleDbType.Varchar2, 50);
                cmd.Parameters["xEmailAddress"].Value = user.EmailAddress;

                //cmd.Parameters.Add("xPin1",OracleDbType.Varchar2,50);
                //if (user.ThirdID.UserSecurityPINs[0] == null) user.ThirdID.UserSecurityPINs[0] = "n/a";
                //cmd.Parameters["xPin1"].Value =Encryption.Encrypt(user.ThirdID.UserSecurityPINs[0],Encryption.PWDKey);

                //cmd.Parameters.Add("xPin2",OracleDbType.Varchar2,50);
                //if (user.ThirdID.UserSecurityPINs[1] == null) user.ThirdID.UserSecurityPINs[1] = "n/a";
                //cmd.Parameters["xPin2"].Value = Encryption.Encrypt(user.ThirdID.UserSecurityPINs[1], Encryption.PWDKey);

                //cmd.Parameters.Add("xPin3",OracleDbType.Varchar2,50);
                //if (user.ThirdID.UserSecurityPINs[2] == null) user.ThirdID.UserSecurityPINs[2] = "n/a";
                //cmd.Parameters["xPin3"].Value = Encryption.Encrypt(user.ThirdID.UserSecurityPINs[2], Encryption.PWDKey);

                cmd.Parameters.Add("xMessage", OracleDbType.Varchar2, 200);
                cmd.Parameters["xMessage"].Value = Message;

                retval = ExecuteCMD(cmd, out ErrorString);
            } catch (Exception ex) {
                HandleError(ex, out ErrorString);
                retval = -1;
            }
            TimeSpan end = TimeKeeper.GetCurrentTime();
            TimeSpan exectime = start.Subtract(end);
            //Logger.RaiseTrace(ClassName,"Exit:ExecutionTime="+exectime.ToString(),TraceLevel.Verbose);
            if (retval != -1) return true; else return false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ex"></param>
        /// <param name="ErrorString"></param>
        private void HandleError(Exception ex, out string ErrorString) {
            System.Diagnostics.StackTrace st = new StackTrace(ex);
            System.Diagnostics.StackFrame sf;
            if (st.FrameCount >= 2) {
                sf = st.GetFrame(st.FrameCount - 2);
            } else {
                sf = st.GetFrame(st.FrameCount - 1);
            }

            System.Reflection.MethodBase mb = sf.GetMethod();
            string MemberName = ClassName + ":" + mb.Name;
            ErrorString = "ERROR:" + MemberName + " has thrown an exception. Exception Message=" + ex.Message;
            //Logger.RaiseTrace(ClassName,ErrorString,TraceLevel.Error);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Identity"></param>
        /// <param name="NewVal"></param>
        /// <param name="ErrorString"></param>
        /// <returns></returns>
        public bool UpdateNextQuestionIndex(string Identity, int NewVal, out string ErrorString) {
            TimeSpan start = TimeKeeper.GetCurrentTime();
            //Logger.RaiseTrace(ClassName,"Entry:"+start.ToString(),TraceLevel.Verbose);
            ErrorString = "";
            bool retval = false;
            //if (NewVal != 0)
            {
                try {
                    OracleCommand cmd = new OracleCommand();
                    cmd.CommandText = "UpdateQuestionIndex";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.Add("xIdentity", OracleDbType.Varchar2, 20);
                    cmd.Parameters["xIdentity"].Value = Identity;
                    cmd.Parameters.Add("xNewVal", OracleDbType.Int32, 22);
                    cmd.Parameters["xNewVal"].Value = NewVal;
                    ExecuteCMD(cmd, out ErrorString);
                    retval = true;
                } catch (Exception ex) {
                    HandleError(ex, out ErrorString);
                }
            }
            TimeSpan end = TimeKeeper.GetCurrentTime();
            TimeSpan exectime = start.Subtract(end);
            //Logger.RaiseTrace(ClassName,"Exit:ExecutionTime="+exectime.ToString(),TraceLevel.Verbose);
            return retval;
        }

        /// <summary>
        /// Returns a listing of Child Access Rights defined by SIAM.
        /// </summary>
        /// <param name="arList">An array list of Access Rights.</param>
        /// <param name="roles">An array of roles.</param>
        private void GetChildEffectiveRights(System.Collections.ArrayList arList, siam.Role[] roles) {
            TimeSpan start = TimeKeeper.GetCurrentTime();
            //Logger.RaiseTrace(ClassName,"Entry:"+start.ToString(),TraceLevel.Verbose);
            string ErrorString = "";
            try {
                for (int x = 0; x <= roles.GetUpperBound(0); x++) {
                    if (roles[x].AccessRights != null) {
                        foreach (AccessRight right in roles[x].AccessRights) {
                            arList.Add(right);
                            if (roles[x].Children != null) {
                                GetChildEffectiveRights(arList, roles[x].Children);
                            }
                        }
                    }
                }
            } catch (Exception ex) {
                HandleError(ex, out ErrorString);
            }
            TimeSpan end = TimeKeeper.GetCurrentTime();
            TimeSpan exectime = start.Subtract(end);
            //Logger.RaiseTrace(ClassName,"Exit:ExecutionTime="+exectime.ToString(),TraceLevel.Verbose);
        }


        /// <summary>
        /// Hydrates a user objects from data obtained in the data base. Data obtained from
        /// oracle is in the form of XML. The XML is then read into a data set. Fields that are
        /// null from the data base will not be included in the XML. The resulting data set tables will
        /// therefore not contain a definition for what are otherwise valid members of the User object.
        /// As a result we must test to see if the table contains a defition for the member being set.
        /// We do this to avoid null reference exceptions.
        /// </summary>
        /// <param name="table">The table containing the data.</param>
        /// <param name="u">The instance of sony.us.siam.User being hydrated.</param>
        private void HydrateUser(System.Data.DataTable table, User u) {
            TimeSpan start = TimeKeeper.GetCurrentTime();
            //Logger.RaiseTrace(ClassName,"Entry:"+start.ToString(),TraceLevel.Verbose);
            string ErrorString = "";
            try {
                if (table.Columns.Contains("UserId")) {
                    u.Identity = table.Rows[0]["UserId"].ToString();
                }
                if (table.Columns.Contains("AddressLine1")) {
                    u.AddressLine1 = table.Rows[0]["AddressLine1"].ToString();
                }
                if (table.Columns.Contains("AddressLine2")) {
                    u.AddressLine2 = table.Rows[0]["AddressLine2"].ToString();
                }
                //if (table.Columns.Contains("AlternateUserName"))
                //{
                //    u.AlternateUserId = table.Rows[0]["AlternateUserName"].ToString();
                //}
                if (table.Columns.Contains("City")) {
                    u.City = table.Rows[0]["City"].ToString();
                }
                if (table.Columns.Contains("CompanyName")) {
                    u.CompanyName = table.Rows[0]["CompanyName"].ToString();
                }
                if (table.Columns.Contains("EmailAddress")) {
                    u.EmailAddress = table.Rows[0]["EmailAddress"].ToString();
                }
                if (table.Columns.Contains("Fax")) {
                    u.Fax = table.Rows[0]["Fax"].ToString();
                }
                if (table.Columns.Contains("FirstName")) {
                    u.FirstName = table.Rows[0]["FirstName"].ToString();
                }
                if (table.Columns.Contains("LastName")) {
                    u.LastName = table.Rows[0]["LastName"].ToString();
                }
                if (table.Columns.Contains("Password")) {
                    //u.Password = Encryption.DeCrypt(table.Rows[0]["Password"].ToString(),null,null);
                    u.Password = Encryption.DeCrypt(table.Rows[0]["Password"].ToString(), "test key");
                    //u.Password = Encryption.DeCrypt(table.Rows[0]["Password"].ToString(),null,null);
                }
                if (table.Columns.Contains("PasswordExpirationDate")) {
                    u.PasswordExpirationDate = table.Rows[0]["PasswordExpirationDate"].ToString();
                }
                if (table.Columns.Contains("Phone")) {
                    u.Phone = table.Rows[0]["Phone"].ToString();
                }
                if (table.Columns.Contains("State")) {
                    u.State = table.Rows[0]["State"].ToString();
                }
                if (table.Columns.Contains("UserName")) {
                    u.UserId = table.Rows[0]["UserName"].ToString();
                }
                //6668 starts
                /*if (table.Columns.Contains("StatusId"))
                {
                    u.UserStatus = System.Int32.Parse(table.Rows[0]["StatusId"].ToString());
                }*/
                if (table.Columns.Contains("StatusCode")) {
                    u.UserStatusCode = System.Int32.Parse(table.Rows[0]["StatusCode"].ToString());
                }
                //6668 ends
                if (table.Columns.Contains("UserType")) {
                    u.UserType = table.Rows[0]["UserType"].ToString();
                }
                if (table.Columns.Contains("zip")) {
                    u.Zip = table.Rows[0]["zip"].ToString();
                }
                if (table.Columns.Contains("NextQuestionIndex")) {
                    u.ThirdID.NextQuestionIndex = System.Int32.Parse(table.Rows[0]["NextQuestionIndex"].ToString());
                }
                //Commeted by prasad for 2632, Uncommented by Naveen for 2808
                if (table.Columns.Contains("LockStatus")) {
                    u.LockStatus = System.Int32.Parse(table.Rows[0]["LockStatus"].ToString());
                }
                if (table.Columns.Contains("CustomerId")) {
                    u.CustomerID = table.Rows[0]["CustomerId"].ToString();
                }
                if (table.Columns.Contains("SequenceNumber")) {
                    u.SequenceNumber = System.Int32.Parse(table.Rows[0]["SequenceNumber"].ToString());
                }

                //SIAMConfig config = CachingServices.Cache["SiamConfiguration"] as SIAMConfig;
                //if (config == null)
                //{
                //    config = new SIAMConfig();
                //    CachingServices.Cache["SiamConfiguration"] = config;
                //}


                //for (int x = 0; x <= ConfigurationData.Environment.UserTypes.GetUpperBound(0); x++)
                //{
                //    if (u.UserType == ConfigurationData.Environment.UserTypes[x].Name)
                //    {
                //        if (ConfigurationData.Environment.UserTypes[x].LoginPolicy.SecurityQuestions != null)
                //        {
                //            u.ThirdID.NextQuestion = ConfigurationData.Environment.UserTypes[x].LoginPolicy.SecurityQuestions[u.ThirdID.NextQuestionIndex].Question;
                //            break;
                //        }
                //    }
                //}

                //// see if we need to go to LDAP
                //object[] Delegates = new DelegateHandler().GetAdministrationDelegates(u.UserType);
                //if (Delegates != null)
                //{
                //    for (int x = 0; x <= Delegates.GetUpperBound(0);x++)
                //    {
                //        if ( Delegates[x].GetType() == typeof(Sony.US.siam.Administration.LDAPAdministrator))
                //        {
                //            u.ThirdID.UserSecurityPINs = ((Sony.US.siam.Interfaces.ISecurityAdministrator)Delegates[x]).GetUserPinValues(u.AlternateUserId);
                //        }
                //    }
                //}
            } catch (Exception ex) {
                HandleError(ex, out ErrorString);
            }
            TimeSpan end = TimeKeeper.GetCurrentTime();
            TimeSpan exectime = start.Subtract(end);
            //Logger.RaiseTrace(ClassName,"Exit:ExecutionTime="+exectime.ToString(),TraceLevel.Verbose);
        }

        public string[] GetUserPinValues(string LDAPID) {

            return null;
        }


        /// <summary>
        /// Converts a data table of user information to an array of ShallowUser objects. Consumed by
        /// the GetUser overloads where a deep parameter is available.
        /// </summary>
        /// <param name="table">The System.Data.DataTable containing the data.</param>
        /// <returns>An array of ShallowUser objects.</returns>
        private ShallowUser[] ConvertToShallowArray(System.Data.DataTable table) {
            TimeSpan start = TimeKeeper.GetCurrentTime();
            //Logger.RaiseTrace(ClassName,"Entry:"+start.ToString(),TraceLevel.Verbose);
            string ErrorString = "";
            ShallowUser[] users = null;
            try {
                users = new ShallowUser[table.Rows.Count];
                for (int x = 0; x <= table.Rows.Count - 1; x++) {
                    users[x] = new ShallowUser();
                    users[x].Identity = table.Rows[x]["SiamIdentity"].ToString();
                    users[x].Name = table.Rows[x]["LastName"].ToString() + ", " + table.Rows[x]["FirstName"].ToString();
                    users[x].Company = table.Rows[x]["CompanyName"].ToString();
                    users[x].EmailAddress = table.Rows[x]["EmailAddress"].ToString();
                    users[x].LdapId = table.Rows[x]["LdapId"].ToString();//6650
                    //Added by chandra on 10 July 07
                    users[x].UserName = table.Rows[x]["UserName"].ToString();
                    //users[x].AlternateUserName = table.Rows[x]["ALTERNATEUSERNAME"].ToString();
                }
            } catch (Exception ex) {
                HandleError(ex, out ErrorString);
            }
            TimeSpan end = TimeKeeper.GetCurrentTime();
            TimeSpan exectime = start.Subtract(end);
            //Logger.RaiseTrace(ClassName,"Exit:ExecutionTime="+exectime.ToString(),TraceLevel.Verbose);
            return users;
        }


        /// <summary>
        /// Converts data contained in a data table to an array of User objects. This method will attempt
        /// to fully hydrate each User object.
        /// </summary>
        /// <param name="table">An instance of System.Data.DataTable containing the data.</param>
        /// <returns>An array of sony.us.siam.User objects.</returns>
        private User[] ConvertUserDataTableToArray(System.Data.DataTable table) {
            TimeSpan start = TimeKeeper.GetCurrentTime();
            //Logger.RaiseTrace(ClassName,"Entry:"+start.ToString(),TraceLevel.Verbose);
            string ErrorString = "";
            User[] users = null;
            try {
                users = new User[table.Rows.Count];
                for (int x = 0; x <= users.GetUpperBound(0); x++) {
                    users[x] = new User();

                    if (table.Columns.Contains("AddressLine1")) {
                        users[x].AddressLine1 = table.Rows[x]["AddressLine1"].ToString();
                    } else {
                        users[x].AddressLine1 = "";
                    }
                    if (table.Columns.Contains("AddressLine2")) {
                        users[x].AddressLine2 = table.Rows[x]["AddressLine2"].ToString();
                    } else {
                        users[x].AddressLine2 = "";
                    }
                    //if (table.Columns.Contains("AlternateUserName"))
                    //{
                    //    users[x].AlternateUserId = table.Rows[x]["AlternateUserName"].ToString();
                    //}
                    //else
                    //{
                    //    users[x].AlternateUserId = "";
                    //}
                    if (table.Columns.Contains("City")) {
                        users[x].City = table.Rows[x]["City"].ToString();
                    } else {
                        users[x].City = "";
                    }
                    if (table.Columns.Contains("CompanyName")) {
                        users[x].CompanyName = table.Rows[x]["CompanyName"].ToString();
                    } else {
                        users[x].CompanyName = "";
                    }
                    if (table.Columns.Contains("EmailAddress")) {
                        users[x].EmailAddress = table.Rows[x]["EmailAddress"].ToString();
                    } else {
                        users[x].EmailAddress = "";
                    }
                    if (table.Columns.Contains("fax")) {
                        users[x].Fax = table.Rows[x]["fax"].ToString();
                    } else {
                        users[x].Fax = "";
                    }
                    if (table.Columns.Contains("FirstName")) {
                        users[x].FirstName = table.Rows[x]["FirstName"].ToString();
                    } else {
                        users[x].FirstName = "";
                    }
                    if (table.Columns.Contains("UserId")) {
                        users[x].Identity = table.Rows[x]["UserId"].ToString();
                    } else {
                        users[x].Identity = "";
                    }
                    if (table.Columns.Contains("LastName")) {
                        users[x].LastName = table.Rows[x]["LastName"].ToString();
                    } else {
                        users[x].LastName = "";
                    }
                    if (table.Columns.Contains("Password")) {
                        users[x].Password = table.Rows[x]["Password"].ToString();
                    } else {
                        users[x].Password = "";
                    }
                    if (table.Columns.Contains("PasswordExpirationDate")) {
                        users[x].PasswordExpirationDate = table.Rows[x]["PasswordExpirationDate"].ToString();
                    } else {
                        users[x].PasswordExpirationDate = "";
                    }
                    if (table.Columns.Contains("Phone")) {
                        users[x].Phone = table.Rows[x]["Phone"].ToString();
                    } else {
                        users[x].Phone = "";
                    }
                    users[x].Roles = GetRoles(users[x].Identity, out ErrorString);
                    users[x].EffectiveRights = GetEffectiveAccessRights(users[x].Roles, out ErrorString);
                }
            } catch (Exception ex) {
                HandleError(ex, out ErrorString);
            }
            TimeSpan end = TimeKeeper.GetCurrentTime();
            TimeSpan exectime = start.Subtract(end);
            //Logger.RaiseTrace(ClassName,"Exit:ExecutionTime="+exectime.ToString(),TraceLevel.Verbose);
            return users;
        }



        /// <summary>
        /// Executes an oracle command object against the data source. By Convention all access to the
        /// database is via stored procedure. The Method is used to execute the command object and obtain
        /// the results from the database. All Oracle stored procedures that return results will return the
        /// results via a CLOB object containing XML data.
        /// 
        /// All result sets are exposed by the DAL as standard .Net framework data sets. This method
        /// will convert the xml results into a dataset to be passed back to the caller.
        /// 
        /// The command object passed in should be ready to execute, It should already have it's command
        /// Text, Connection, CommandType and any required input parameters already set. 
        /// 
        /// This method will add the requried output parameter to the end of the existing parameters collection
        /// </summary>
        /// <param name="cmd">The Command object to execute</param>
        /// <param name="ErrorString"></param>
        /// <returns>An object of type System.Data.DataSet</returns>
        private System.Data.DataSet GetResults(OracleCommand cmd, out string ErrorString) {
            TimeSpan start = TimeKeeper.GetCurrentTime();
            //Logger.RaiseTrace(ClassName,"Entry:"+start.ToString(),TraceLevel.Verbose);
            ErrorString = "";
            System.Data.DataSet ds = null;
            cmd.CommandType = CommandType.StoredProcedure;

            //Added by Chandra on July 09 07
            if (mSPSSearch == true) {
                this.cn.ConnectionString = mConnectionStringSPS;
            }


            cmd.Connection = this.cn;
            try {
                if (cn.State != System.Data.ConnectionState.Open) {
                    cn.Open();
                }
                OracleParameter outparam = new OracleParameter("RetVal", OracleDbType.Clob);
                outparam.Direction = System.Data.ParameterDirection.Output;
                cmd.Parameters.Add(outparam);
                cmd.ExecuteNonQuery();
                OracleClob ReturnValue = cmd.Parameters["RetVal"].Value as OracleClob;
                if (ReturnValue != null) {
                    ReturnValue.Seek(0, System.IO.SeekOrigin.Begin);
                    byte[] lobBuffer = new byte[ReturnValue.Length];
                    ReturnValue.Read(lobBuffer, 0, (int)ReturnValue.Length);
                    byte[] newBuffer;
                    newBuffer = System.Text.UTF8Encoding.Convert(System.Text.Encoding.Unicode, System.Text.Encoding.UTF8, lobBuffer, 0, lobBuffer.Length);
                    System.Text.StringBuilder sb = new System.Text.StringBuilder();
                    for (int x = 0; x <= newBuffer.GetUpperBound(0); x++) {
                        sb.Append((char)newBuffer[x]);
                    }
                    if (CheckXMLerror(sb.ToString(), out ErrorString)) {
                        throw new Exception(ErrorString);
                    } else {
                        ds = new System.Data.DataSet("ResultSet");
                        ds.ReadXml(new System.Xml.XmlTextReader(sb.ToString(), System.Xml.XmlNodeType.Document, null));
                    }
                }
                cn.Close();
            } catch (Exception ex) {
                //Added by chandra
                cn.Close();
                HandleError(ex, out ErrorString);
            }
            TimeSpan end = TimeKeeper.GetCurrentTime();
            TimeSpan exectime = start.Subtract(end);
            //Logger.RaiseTrace(ClassName,"Exit:ExecutionTime="+exectime.ToString(),TraceLevel.Verbose);
            return ds;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="ErrorString"></param>
        /// <returns></returns>
        private System.Data.DataSet GetResultsWithAdapter(OracleCommand cmd, out string ErrorString) {
            TimeSpan start = TimeKeeper.GetCurrentTime();
            //Logger.RaiseTrace(ClassName, "Entry:" + start.ToString(), TraceLevel.Verbose);
            ErrorString = "";
            System.Data.DataSet ds = new DataSet();
            cmd.CommandType = CommandType.StoredProcedure;

            if (mSPSSearch == true) {
                this.cn.ConnectionString = mConnectionStringSPS;
            }


            cmd.Connection = this.cn;
            try {
                if (cn.State != System.Data.ConnectionState.Open) {
                    cn.Open();
                }
                OracleDataAdapter oDadapter = new OracleDataAdapter(cmd);
                oDadapter.Fill(ds);
                cn.Close();
            } catch (Exception ex) {
                //Added by chandra
                cn.Close();
                HandleError(ex, out ErrorString);
            }
            TimeSpan end = TimeKeeper.GetCurrentTime();
            TimeSpan exectime = start.Subtract(end);
            //Logger.RaiseTrace(ClassName, "Exit:ExecutionTime=" + exectime.ToString(), TraceLevel.Verbose);
            return ds;
        }

        /// <summary>
        /// Checks an XML string from the Data base for an error. In certain circumstances errors from
        /// oracle get returned as an xml string of the following format
        /// <ERROR>oracle.xml.sql.OracleXMLSQLException: ORA-00942: table or view does not exist
        /// </ERROR>
        /// </summary>
        /// <param name="XMLString"></param>
        /// <param name="ErrorString"></param>
        /// <returns></returns>
        private bool CheckXMLerror(string XMLString, out string ErrorString) {

            ErrorString = "";
            System.Xml.XmlDocument theDoc = new System.Xml.XmlDocument();
            try {
                theDoc.LoadXml(XMLString);
            } catch (Exception ex) {
                ErrorString = ex.Message;
                return true;
            }
            System.Xml.XmlNode n = theDoc.SelectSingleNode(@"//ERROR");
            if (n == null) {
                return false;
            } else {
                ErrorString = n.InnerText;
                return true;
            }


        }

        /// <summary>
        /// Executes an oracle command object against the data source. This method is used with Command
        /// objects that do not expect result sets returned from the data base. This method will Add
        /// an output parameter to the end of any already configured parameters to hold any return values
        /// sent back as output parameters. The value will be returned. If no value is sent back as an out
        /// parameter the method will return 0;
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="ErrorString"></param>
        /// <returns></returns>
        private double ExecuteCMD(OracleCommand cmd, out string ErrorString) {
            OracleTransaction oTran = null;
            TimeSpan start = TimeKeeper.GetCurrentTime();
            //Logger.RaiseTrace(ClassName,"Entry:"+start.ToString(),TraceLevel.Verbose);
            double x = -1;
            ErrorString = "";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = this.cn;
            try {
                if (cn.State != System.Data.ConnectionState.Open) {
                    cn.Open();
                }
                OracleParameter outparam = new OracleParameter("retVal", OracleDbType.Int32);
                outparam.Direction = System.Data.ParameterDirection.Output;
                cmd.Parameters.Add(outparam);
                try {
                    oTran = cn.BeginTransaction();
                    cmd.Transaction = oTran;
                    cmd.ExecuteNonQuery();
                    if (!((INullable)cmd.Parameters["retVal"].Value).IsNull) {
                        x = System.Convert.ToDouble(cmd.Parameters["retVal"].Value);
                    } else {
                        x = 0;
                    }
                    if (x == 3) {
                        oTran.Rollback();
                        throw new ApplicationException("Failed to update customer information.");
                    } else oTran.Commit();
                } catch (Exception exConn) {
                    oTran.Rollback();
                    cn.Close();
                    throw exConn;
                }

                cn.Close();
            } catch (Exception ex) {
                ErrorString = ex.Message;
                HandleError(ex, out ErrorString);
            }
            TimeSpan end = TimeKeeper.GetCurrentTime();
            TimeSpan exectime = start.Subtract(end);
            //Logger.RaiseTrace(ClassName,"Exit:ExecutionTime="+exectime.ToString(),TraceLevel.Verbose);
            return x;
        }

        private DataSet ExecuteDataSet(OracleCommand cmd, out string ErrorString) {
            TimeSpan start = TimeKeeper.GetCurrentTime();
            //Logger.RaiseTrace(ClassName, "Entry:" + start.ToString(), TraceLevel.Verbose);
            ErrorString = "";
            DataSet oDS = new DataSet();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = this.cn;
            try {
                if (cn.State != System.Data.ConnectionState.Open) {
                    cn.Open();
                }
                OracleDataAdapter oDa = new OracleDataAdapter(cmd);
                oDa.Fill(oDS);
            } catch (Exception ex) {
                ErrorString = ex.Message;
                HandleError(ex, out ErrorString);
            } finally {
                cn.Close();
            }

            TimeSpan end = TimeKeeper.GetCurrentTime();
            TimeSpan exectime = start.Subtract(end);
            //Logger.RaiseTrace(ClassName, "Exit:ExecutionTime=" + exectime.ToString(), TraceLevel.Verbose);
            return oDS;
        }

        #endregion

        #endregion
    }
}
