using System;
using System.Diagnostics;
using Sony.US.siam;
using Sony.US.SIAMUtilities;
using Sony.US.siam.Interfaces;
//using ServicesPlusException;
using System.Reflection;
using System.Runtime.Remoting;
namespace Sony.US.siam
{
	/// <summary>
	/// The delegate handler class implements tha main class factory code in SIAM. This is an internal class
	/// and cannot be instantiated directly. 
	/// </summary>
	internal class DelegateHandler
	{

		#region Private Class Variables

		/// <summary>
		/// 
		/// </summary>
		//private SonyLogger Logger = new SonyLogger();

		/// <summary>
		/// A constant value used by Loggin.
		/// </summary>
		private const string ClassName = "DelegateHandler";
		/// <summary>
		/// An instance of <see cref="SIAMConfig"/> that wraps the configuration provider.
		/// </summary>
        /// Commented this line due to merge of both Configuration files. Used ConfigurationData.Environment instead
		//private SIAMConfig config = null;
		
		#endregion

		#region Constructor

		/// <summary>
		/// Internal constructor cannot be instantiated externally.
		/// </summary>
		internal DelegateHandler()
		{
            //config = CachingServices.Cache["SiamConfiguration"] as SIAMConfig;
            //if (config == null)
            //{
            //    config = new SIAMConfig();
            //    CachingServices.Cache["SiamConfiguration"] = config;
            //}
		}

		#endregion

		#region Public Methods


		/// <summary>
		/// The class factory for the current data access layer defined in SIAM's configuration. 
		/// This method will parse configuration for the definiton of the Current DAL. It will load
		/// any constructor arguments required by the class definition and uses <see cref="System.Reflection"/> 
		/// Activator.CreateInstance to locate and create an instance of the defined class.
		/// </summary>
		/// <returns></returns>
		internal Interfaces.ISecurityAdministrator GetCurrentDal()
		{
			TimeSpan start = TimeKeeper.GetCurrentTime();
			//Logger.RaiseTrace(ClassName,"Entry:"+start.ToString(),TraceLevel.Verbose);
			//Logger.RaiseTrace(ClassName,"Getting Configuration Data",TraceLevel.Info);
			object _obj = null;
            //Changed by Chandra on July 09 07 for sending SPS connection string
			object[] constructorArgs = new object[2];
            //constructorArgs[0] = config.CurrentDAL.ConnectionString;
            //constructorArgs[1] = config.CurrentDAL.ConnectionStringSPS;
            //string typeDef = config.CurrentDAL.AdministrationDelegate;

            constructorArgs[0] = ConfigurationData.Environment.DataBase.ConnectionString;
            constructorArgs[1] = ConfigurationData.Environment.DataBase.ConnectionString;
            string typeDef = ConfigurationData.GeneralSettings.General.AdministrationDelegate;


			//Logger.RaiseTrace(ClassName,"Creating ISecurityAdministrator Instance",TraceLevel.Info);
			try
			{
				//ObjectHandle newobj = Activator.CreateInstance(null,typeDef,true,BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.CreateInstance,null,constructorArgs,System.Globalization.CultureInfo.CurrentCulture,null,null);
                ObjectHandle newobj = Activator.CreateInstance(null, typeDef, true, BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.CreateInstance, null, constructorArgs, System.Globalization.CultureInfo.CurrentCulture, null);
                _obj = newobj.Unwrap();
			}
            catch (Exception ex)
			{
                //Utilities.LogMessages("Exception during GetCurrentDAL - " + ex.Message);
				throw new Exception($"DelegateHandler.GetCurrentDAL - Error creating administration delegate. Message: " + ex.Message + ", InnerException: " + ex.InnerException?.Message);
			}
			TimeSpan end = TimeKeeper.GetCurrentTime();
			TimeSpan exectime = start.Subtract(end);
			//Logger.RaiseTrace(ClassName,"Exit:ExecutionTime="+exectime.ToString(),TraceLevel.Verbose);
			return _obj as ISecurityAdministrator;
		}

		/// <summary>
		/// GetAuthenticationAuthority will parse configuration for the Authentication Delegate defined
		/// by configuration for the given user type. An instance of the class will be created using
		/// Activator.CreateInstance and returned to the caller. Any class defined by configuration as
		/// an AuthenticationAuthority must implement <see cref="IAuthentication"/>
		/// </summary>
		/// <param name="UserType">A string value representing the User type.</param>
		/// <returns>An instance of <see cref="IAuthentication"/></returns>
        //internal Interfaces.IAuthentication GetAuthenticationAuthority(string UserType)
        //{
        //    string className = "";
        //    object[] Args = null;
        //    object _obj = null;
        //    //for (int x = 0; x <= config.UserTypes.GetUpperBound(0); x++)
        //    for (int x = 0; x <=ConfigurationData.Environment.UserTypes.GetUpperBound(0); x++)
        //    {
        //        //if (config.UserTypes[x].Name == UserType)
        //        if (ConfigurationData.Environment.UserTypes[x].Name == UserType)
        //        {
        //            //className = config.UserTypes[x].AuthenticationDelegate.TypeDefinition;
        //            className = ConfigurationData.Environment.UserTypes[x].AuthenticationDelegate.TypeDefinition;
        //            //if (config.UserTypes[x].AuthenticationDelegate.ConstructorArgs != null)
        //            if (ConfigurationData.Environment.UserTypes[x].AuthenticationDelegate.ConstructorArgs != null)
        //            {
        //                //Args = new object[config.UserTypes[x].AuthenticationDelegate.ConstructorArgs.Length];
        //                Args = new object[ConfigurationData.Environment.UserTypes[x].AuthenticationDelegate.ConstructorArgs.Length];
        //                //for (int z = 0; z <= config.UserTypes[x].AuthenticationDelegate.ConstructorArgs.GetUpperBound(0); z++)
        //                for (int z = 0; z <= ConfigurationData.Environment.UserTypes[x].AuthenticationDelegate.ConstructorArgs.GetUpperBound(0); z++)
        //                {
        //                    //Args[z] = config.UserTypes[x].AuthenticationDelegate.ConstructorArgs[z].Value;
        //                    Args[z] = ConfigurationData.Environment.UserTypes[x].AuthenticationDelegate.ConstructorArgs[z].Value;
        //                }
        //            }
        //            else
        //            {
        //                Args = null;
        //            }
        //            ObjectHandle newobj = Activator.CreateInstance(null,className,true,BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.CreateInstance,null,Args,System.Globalization.CultureInfo.CurrentCulture,null,null);
        //            _obj = newobj.Unwrap();
        //        }
        //    }
        //    return _obj as IAuthentication;
        //}

		/// <summary>
		/// GetAuthorizationAuthority will obtain an instance of the class defined in configuration as the
		/// Authorization Authority for this user type. Uses the same logic as GetAuthenticationAuthority.
		/// </summary>
		/// <param name="UserType">A string value containing the User type.</param>
		/// <returns>An instance of <see cref="IAuthorization"/></returns>
        //internal Interfaces.IAuthorization GetAuthorizationAuthority(string UserType)
        //{
        //    string className = "";
        //    object[] Args = null;
        //    object _obj = null;
        //    ObjectHandle newobj;
        //    //for (int x = 0; x <= config.UserTypes.GetUpperBound(0); x++)
        //    for (int x = 0; x <= ConfigurationData.Environment.UserTypes.GetUpperBound(0); x++)
        //    {
        //       //if (config.UserTypes[x].Name == UserType)
        //        if (ConfigurationData.Environment.UserTypes[x].Name == UserType)
        //        {
        //            //className = config.UserTypes[x].AuthorizationDelegate.TypeDefinition;
        //            className = ConfigurationData.Environment.UserTypes[x].AuthorizationDelegate.TypeDefinition;

        //            //if (config.UserTypes[x].AuthorizationDelegate.ConstructorArgs != null)
        //            if (ConfigurationData.Environment.UserTypes[x].AuthorizationDelegate.ConstructorArgs != null)
        //            {
        //                //Args = new object[config.UserTypes[x].AuthorizationDelegate.ConstructorArgs.Length];
        //                Args = new object[ConfigurationData.Environment.UserTypes[x].AuthorizationDelegate.ConstructorArgs.Length];
        //                //for (int z = 0; z <= config.UserTypes[x].AuthorizationDelegate.ConstructorArgs.GetUpperBound(0); z++)
        //                for (int z = 0; z <= ConfigurationData.Environment.UserTypes[x].AuthorizationDelegate.ConstructorArgs.GetUpperBound(0); z++)
        //                {
        //                    //Args[z] = config.UserTypes[x].AuthorizationDelegate.ConstructorArgs[z].Value;
        //                    Args[z] = ConfigurationData.Environment.UserTypes[x].AuthorizationDelegate.ConstructorArgs[z].Value;
        //                }
        //            }
        //            else
        //            {
        //                Args = null;
        //            }
        //            newobj = Activator.CreateInstance(null,className,true,BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.CreateInstance,null,Args,System.Globalization.CultureInfo.CurrentCulture,null,null);
        //            _obj = newobj.Unwrap();
        //        }
        //    }
        //    return _obj as IAuthorization;
        //}

		/// <summary>
		/// Obtains an array of objects defined in configuration as Administration Delegates for a given 
		/// user type. Administration delegates are used throughout SIAM to perform administrative functions
		/// agains mulitple backend systems.
		/// </summary>
		/// <param name="UserType">A string value containing the User Type.</param>
		/// <returns>An array of objects of type <see cref="ISecurityAdministrator"/></returns>
        //internal object[] GetAdministrationDelegates(string UserType)
        //{
        //    string className = "";
        //    object[] Args = null;
        //    object[] DelegateArray = null;
        //    //for (int x = 0; x <= config.UserTypes.GetUpperBound(0); x++)
        //    for (int x = 0; x <= ConfigurationData.Environment.UserTypes.GetUpperBound(0); x++)
        //    {
        //        //if (config.UserTypes[x].Name == UserType)
        //        if (ConfigurationData.Environment.UserTypes[x].Name == UserType)
        //        {
        //            //if (config.UserTypes[x].AdministrationDelegates != null)
        //            if (ConfigurationData.Environment.UserTypes[x].AdministrationDelegates != null)
        //            {
        //                //DelegateArray = new object[config.UserTypes[x].AdministrationDelegates.Length];
        //                DelegateArray = new object[ConfigurationData.Environment.UserTypes[x].AdministrationDelegates.Length];
        //                //for (int y = 0; y <= config.UserTypes[x].AdministrationDelegates.GetUpperBound(0); y++)
        //                for (int y = 0; y <= ConfigurationData.Environment.UserTypes[x].AdministrationDelegates.GetUpperBound(0); y++)
        //                {
        //                    //className = config.UserTypes[x].AdministrationDelegates[y].TypeDefinition;
        //                    className = ConfigurationData.Environment.UserTypes[x].AdministrationDelegates[y].TypeDefinition;
        //                    //Args = new object[config.UserTypes[x].AdministrationDelegates[y].ConstructorArgs.Length];
        //                    Args = new object[ConfigurationData.Environment.UserTypes[x].AdministrationDelegates[y].ConstructorArgs.Length];
        //                    //for (int z = 0; z <= config.UserTypes[x].AdministrationDelegates[y].ConstructorArgs.GetUpperBound(0); z++)
        //                    for (int z = 0; z <= ConfigurationData.Environment.UserTypes[x].AdministrationDelegates[y].ConstructorArgs.GetUpperBound(0); z++)
        //                    {
        //                        //Args[z] = config.UserTypes[x].AdministrationDelegates[y].ConstructorArgs[z].Value;
        //                        Args[z] = ConfigurationData.Environment.UserTypes[x].AdministrationDelegates[y].ConstructorArgs[z].Value;
        //                    }
        //                    ObjectHandle newobj = Activator.CreateInstance(null,className,true,BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.CreateInstance,null,Args,System.Globalization.CultureInfo.CurrentCulture,null,null);
        //                    DelegateArray[x] = (ISecurityAdministrator)newobj.Unwrap();
        //                }
        //            }
        //        }
        //    }
        //    return DelegateArray;
        //}

		#endregion

	}
}
