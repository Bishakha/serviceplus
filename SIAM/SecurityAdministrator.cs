using System;
using System.Reflection;
using System.Runtime.Remoting;
using System.Diagnostics;
using Sony.US.siam;
using Sony.US.SIAMUtilities;
using Sony.US.siam.Interfaces;
using Sony.US.siam.Administration;
using Sony.US.siam.Authentication;
using Sony.US.siam.Authorization;
using System.Text.RegularExpressions;
using System.Collections;
using Microsoft.Win32;
using System.Net;
using Sony.US.ServicesPLUS.Core;
using System.Security.Cryptography.X509Certificates;
using Sony.US.AuditLog;
using System.Web;
using System.ServiceModel;
using System.ServiceModel.Channels;
using SoftwarePlus.BusinessAccess;
using System.Web.Services.Protocols;

namespace Sony.US.siam {
    /// <summary>
    /// 
    /// </summary>
    public class SecurityAdministrator {
        #region Private Class Variables

        /// <summary>
        /// 
        /// </summary>
        //private SonyLogger Logger = new SonyLogger();

        /// <summary>
        /// An instance of ISecurityAdministrator that represents the current SIAM backend
        /// </summary>
        private Interfaces.ISecurityAdministrator _dal;

        //private sony.us.siam.SIAMConfig config;

        /// <summary>
        /// A constant value used by logging.
        /// </summary>
        private const string ClassName = "SecurityAdministrator";

        #endregion


        #region Constructor

        /// <summary>
        /// Security Administrators constructor requires no arguments. 
        /// </summary>
        public SecurityAdministrator() {

            //added by satheesh-arshad for testing registration -Start 
            //System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls;
            //added by satheesh-arshad for testing registration -end ..
            _dal = new DelegateHandler().GetCurrentDal();
        }

        #endregion


        #region Add Methods

        /// <summary>
        /// Adds a user to the SIAM backend. 
        /// </summary>
        /// <param name="user">An instance of <see cref="User"/>that contains the data for
        /// the user being added to SIAM</param>
        /// <returns>An instance of <see cref="AddUserResponse"/> that will contain the results
        /// of the call.</returns>

        public String TestBindB2B(User user, AccountInfoVO acctInfo, string RequestingApplication, string RequestingUserID, string RequestingPassword) {
            //    TimeSpan start = TimeKeeper.GetCurrentTime();
            //    AddUserResponse response = new AddUserResponse();
            //    string outMessage = "";

            //    try
            //    {
            //        user.Identity = "9999999999999999";
            //        object[] adminDelegates = new DelegateHandler().GetAdministrationDelegates(user.UserType);
            //        if (adminDelegates != null)
            //        {
            //            foreach (object obj in adminDelegates)
            //            {
            //                Interfaces.ISecurityAdministrator adminObj = obj as ISecurityAdministrator;
            //                if (obj != null)
            //                {
            //                    adminObj.AddTestUser(user, acctInfo,RequestingApplication,RequestingUserID,RequestingPassword, out outMessage);
            //                    if (outMessage != "")
            //                    {
            //                        throw new Exception(outMessage);
            //                    }
            //                }
            //            }
            //        }
            //        if (outMessage != "")
            //        {
            //            throw new System.Exception(outMessage);
            //        }
            //        else
            //        {
            //            return outMessage;
            //        }
            //    }
            //    catch
            //    {
            //        return outMessage;
            //    }
            return string.Empty;
        }


        /// <summary>
        /// Adds a user to the SIAM backend. 
        /// </summary>
        /// <param name="user">An instance of <see cref="User"/>that contains the data for
        /// the user being added to SIAM</param>
        /// <returns>An instance of <see cref="AddUserResponse"/> that will contain the results
        /// of the call.</returns>

        public AddUserResponse AddUser(User user) {
            TimeSpan start = TimeKeeper.GetCurrentTime();
            //Logger.RaiseTrace(ClassName, "Entry:" + start.ToString(), TraceLevel.Verbose);
            AddUserResponse response = new AddUserResponse();
            string outMessage = "";

            try {
                if (!IsValidEmail(user.EmailAddress)) {
                    throw new Exception("Invalid Email Address");
                }

                if (!IsValidPassword(user.Password, user.UserType)) {
                    throw new Exception("Invalid Password");
                }

                if (!IsValidUserId(user.UserId)) {
                    throw new Exception("Invalid UserId");
                }

                user.Identity = _dal.AddUser(user, out outMessage);
                if (outMessage != "") {
                    throw new Exception(outMessage);
                } else {
                    //if (user.UserType == "AccountHolder")//6668
                    if (user.UserType == "A" || user.UserType == "P")//6668
                    {
                        string ErrorString = "New Registration";
                        _dal.AddISSAWebCall(user, ErrorString, out ErrorString);
                    }
                    response.User = user;
                    response.SiamResponseCode = (int)Messages.ADD_USER_COMPLETE;
                    response.SiamResponseMessage = Messages.ADD_USER_COMPLETE.ToString();

                }
            } catch (Exception ex) {
                response.User = null;
                response.SiamResponseCode = (int)Messages.ADD_USER_FAILED;
                response.SiamResponseMessage = Messages.ADD_USER_FAILED.ToString() + " " + ex.Message;
                //Logger.RaiseTrace(ClassName, "Error=" + response.SiamResponseMessage, TraceLevel.Error);
            }
            TimeSpan end = TimeKeeper.GetCurrentTime();
            TimeSpan exectime = start.Subtract(end);
            //Logger.RaiseTrace(ClassName, "Exit:ExecutionTime=" + exectime.ToString(), TraceLevel.Verbose);
            return response;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public AddUserResponse RetryAddUser(User user) {
            //    TimeSpan start = TimeKeeper.GetCurrentTime();
            //    //Logger.RaiseTrace(ClassName, "Entry:" + start.ToString(), TraceLevel.Verbose);
            //    AddUserResponse response = new AddUserResponse();
            //    string outMessage = "";

            //    //-- get the PINs from the issawebsrvcalls table --						
            //    OracleAdministrator myDAL = (OracleAdministrator)_dal;
            //    user.ThirdID.UserSecurityPINs = myDAL.GetUserPinValues(user.FirstName.ToString(), user.LastName.ToString(), user.EmailAddress.ToString());
            //    if (user.ThirdID.UserSecurityPINs[0] != null && user.ThirdID.UserSecurityPINs[0] != String.Empty)
            //    {
            //        try
            //        {
            //            if (!IsValidEmail(user.EmailAddress))
            //            {
            //                throw new System.Exception("Invalid Email Address");
            //            }

            //            if (!IsValidPassword(user.Password,user.UserType))
            //            {
            //                throw new System.Exception("Invalid Password");
            //            }

            //            if (!IsValidUserId(user.UserId))
            //            {
            //                throw new System.Exception("Invalid UserId");
            //            }
            //            object[] adminDelegates = new DelegateHandler().GetAdministrationDelegates(user.UserType);
            //            if (adminDelegates != null)
            //            {
            //                foreach (object obj in adminDelegates)
            //                {
            //                    Interfaces.ISecurityAdministrator adminObj = obj as ISecurityAdministrator;
            //                    if (obj != null)
            //                    {
            //                        adminObj.AddUser(user, out outMessage);
            //                        if (outMessage != "")
            //                        {
            //                            throw new Exception(outMessage);
            //                        }
            //                    }
            //                }
            //            }
            //            if (outMessage != "")
            //            {
            //                throw new System.Exception(outMessage);
            //            }
            //            else
            //            {
            //                response.User = user;
            //                response.SiamResponseCode = (int)Messages.ADD_USER_COMPLETE;
            //                response.SiamResponseMessage = Messages.ADD_USER_COMPLETE.ToString();
            //            }
            //        }
            //        catch (System.Exception ex)
            //        {
            //            response.User = null;
            //            response.SiamResponseCode = (int)Messages.ADD_USER_FAILED;
            //            if (outMessage != "")
            //            {
            //                response.SiamResponseMessage = Messages.ADD_USER_FAILED.ToString() + " " + outMessage.ToString() + " " + ex.Message;
            //            }
            //            else
            //            {
            //                response.SiamResponseMessage = Messages.ADD_USER_FAILED.ToString() + " " + ex.Message;
            //            }
            //            //Logger.RaiseTrace(ClassName, "Error=" + response.SiamResponseMessage, TraceLevel.Error);
            //        }
            //    }
            //    else
            //    {
            //        response.User = null;
            //        response.SiamResponseCode = (int)Messages.ADD_USER_FAILED;
            //        response.SiamResponseMessage = Messages.ADD_USER_FAILED.ToString() + " Unable to find security pins. " + user.FirstName.ToString() + " " + user.LastName.ToString() + " " + user.EmailAddress.ToString();
            //        // this will be removed once we find the problem.
            //        if (user.ThirdID.UserSecurityPINs[1] != null && user.ThirdID.UserSecurityPINs[1] != null)
            //        {
            //            response.SiamResponseMessage += " : " + Encryption.Encrypt(user.ThirdID.UserSecurityPINs[1].ToString(), Encryption.PWDKey) + " " + Encryption.Encrypt(user.ThirdID.UserSecurityPINs[2].ToString(), Encryption.PWDKey);
            //        }

            //        //Logger.RaiseTrace(ClassName, "Error=" + response.SiamResponseMessage, TraceLevel.Error);
            //    }
            //    TimeSpan end = TimeKeeper.GetCurrentTime();
            //    TimeSpan exectime = start.Subtract(end);
            //    //Logger.RaiseTrace(ClassName, "Exit:ExecutionTime=" + exectime.ToString(), TraceLevel.Verbose);
            return null;
        }

        /// <summary>
        /// Adds an Action to SIAM. 
        /// </summary>
        /// <param name="ActionName">A string value containing the name of the action to add.</param>
        /// <returns>An instance of <see cref="SiamResponse"/> that will contain the results
        /// of the call.</returns>
        //6732 starts
        /*public SiamResponse AddAction(string ActionName)
        {
            TimeSpan start = TimeKeeper.GetCurrentTime();
            //Logger.RaiseTrace(ClassName, "Entry:" + start.ToString(), TraceLevel.Verbose);
            SiamResponse r = new SiamResponse();
            string outMessage = "";
            try
            {
                r.SiamPayLoad = _dal.AddAction(ActionName, out outMessage);
                if (outMessage != "")
                {
                    throw new System.Exception(outMessage);
                }
                else
                {
                    r.SiamResponseCode = (int)Messages.ADD_ACTION_COMPLETED;
                    r.SiamResponseMessage = Messages.ADD_ACTION_COMPLETED.ToString();
                }
            }
            catch (System.Exception ex)
            {
                r.SiamResponseCode = (int)Messages.ADD_ACTION_FAILED;
                r.SiamResponseMessage = Messages.ADD_ACTION_FAILED.ToString() + " " + ex.Message;
                //Logger.RaiseTrace(ClassName, "Error=" + r.SiamResponseMessage, TraceLevel.Error);
            }
            TimeSpan end = TimeKeeper.GetCurrentTime();
            TimeSpan exectime = start.Subtract(end);
            //Logger.RaiseTrace(ClassName, "Exit:ExecutionTime=" + exectime.ToString(), TraceLevel.Verbose);
            return r;
        }*/
        //6732 ends

        /// <summary>
        /// Adds an application to the SIAM backend.
        /// </summary>
        /// <param name="ApplicationName">A string value representing the application name to add.</param>
        /// <returns>An instance of <see cref="SiamResponse"/> that will contain the results
        /// of the call.</returns>
        public SiamResponse AddApplication(string ApplicationName) {
            TimeSpan start = TimeKeeper.GetCurrentTime();
            //Logger.RaiseTrace(ClassName, "Entry:" + start.ToString(), TraceLevel.Verbose);
            SiamResponse r = new SiamResponse();
            string outMessage;
            try {
                r.SiamPayLoad = _dal.AddApplication(ApplicationName, out outMessage);
                if (outMessage != "") {
                    throw new Exception(outMessage);
                } else {
                    r.SiamResponseCode = (int)Messages.ADD_APPLICATION_COMPLETED;
                    r.SiamResponseMessage = Messages.ADD_APPLICATION_COMPLETED.ToString();
                }
            } catch (Exception ex) {
                r.SiamResponseCode = (int)Messages.ADD_APPLICATION_FAILED;
                r.SiamResponseMessage = Messages.ADD_APPLICATION_FAILED.ToString() + " " + ex.Message;
                //Logger.RaiseTrace(ClassName, "Error=" + r.SiamResponseMessage, TraceLevel.Error);
            }
            TimeSpan end = TimeKeeper.GetCurrentTime();
            TimeSpan exectime = start.Subtract(end);
            //Logger.RaiseTrace(ClassName, "Exit:ExecutionTime=" + exectime.ToString(), TraceLevel.Verbose);
            return r;
        }


        /// <summary>
        /// Adds an application resource to SIAM.
        /// </summary>
        /// <param name="ApplicationId">a double value containing the application id of the application that the 
        /// resource is being added for.</param>
        /// <param name="ResourceName">The name of the new resource</param>
        /// <returns>An instance of <see cref="SiamResponse"/> that will contain the results
        /// of the call.</returns>
        public SiamResponse AddApplicationResource(double ApplicationId, string ResourceName) {
            TimeSpan start = TimeKeeper.GetCurrentTime();
            //Logger.RaiseTrace(ClassName, "Entry:" + start.ToString(), TraceLevel.Verbose);
            string outMessage = "";
            SiamResponse r = new SiamResponse();
            try {
                r.SiamPayLoad = _dal.AddApplicationResource(ApplicationId, AddResource(ResourceName), out outMessage);
                if (outMessage != "") {
                    throw new Exception(outMessage);
                } else {
                    r.SiamResponseCode = (int)Messages.ADD_RESOURCE_COMPLETED;
                    r.SiamResponseMessage = Messages.ADD_RESOURCE_COMPLETED.ToString();
                }
            } catch (Exception ex) {
                r.SiamPayLoad = null;
                r.SiamResponseCode = (int)Messages.ADD_RESOURCE_FAILED;
                r.SiamResponseMessage = Messages.ADD_RESOURCE_FAILED.ToString() + " " + ex.Message;
                //Logger.RaiseTrace(ClassName, "Error=" + r.SiamResponseMessage, TraceLevel.Error);
            }
            TimeSpan end = TimeKeeper.GetCurrentTime();
            TimeSpan exectime = start.Subtract(end);
            //Logger.RaiseTrace(ClassName, "Exit:ExecutionTime=" + exectime.ToString(), TraceLevel.Verbose);
            return r;
        }


        /// <summary>
        /// Adds a Role to SIAM.
        /// </summary>
        /// <param name="RoleName">The name of the new role</param>
        /// <param name="ParentId">An integer value representing the ParentId of the parent role</param>
        /// <returns>An instance of <see cref="SiamResponse"/> that will contain the results
        /// of the call.</returns>
        public SiamResponse AddRole(string RoleName, int ParentId) {
            TimeSpan start = TimeKeeper.GetCurrentTime();
            //Logger.RaiseTrace(ClassName, "Entry:" + start.ToString(), TraceLevel.Verbose);
            string outMessage = "";
            SiamResponse r = new SiamResponse();
            try {
                r.SiamPayLoad = _dal.AddRole(RoleName, ParentId, out outMessage);
                if (outMessage != "") {
                    throw new Exception(outMessage);
                } else {
                    r.SiamResponseCode = (int)Messages.ADD_ROLE_COMPLETED;
                    r.SiamResponseMessage = Messages.ADD_ROLE_COMPLETED.ToString();
                }
            } catch (Exception ex) {
                r.SiamPayLoad = null;
                r.SiamResponseCode = (int)Messages.ADD_ROLE_FAILED;
                r.SiamResponseMessage = Messages.ADD_ROLE_FAILED.ToString() + " " + ex.Message;
                //Logger.RaiseTrace(ClassName, "Error=" + r.SiamResponseMessage, TraceLevel.Error);
            }
            TimeSpan end = TimeKeeper.GetCurrentTime();
            TimeSpan exectime = start.Subtract(end);
            //Logger.RaiseTrace(ClassName, "Exit:ExecutionTime=" + exectime.ToString(), TraceLevel.Verbose);
            return r;
        }


        /// <summary>
        /// Adds a role to SIAM that has no parent.
        /// </summary>
        /// <param name="RoleName">The name of the role being added.</param>
        /// <returns>An instance of <see cref="SiamResponse"/> that will contain the results
        /// of the call.</returns>
        public SiamResponse AddRole(string RoleName) {
            TimeSpan start = TimeKeeper.GetCurrentTime();
            //Logger.RaiseTrace(ClassName, "Entry:" + start.ToString(), TraceLevel.Verbose);

            SiamResponse r = AddRole(RoleName, -1);

            TimeSpan end = TimeKeeper.GetCurrentTime();
            TimeSpan exectime = start.Subtract(end);
            //Logger.RaiseTrace(ClassName, "Exit:ExecutionTime=" + exectime.ToString(), TraceLevel.Verbose);
            return r;
        }


        /// <summary>
        /// Adds an Access right to SIAM
        /// </summary>
        /// <param name="RoleId">The role Id associated with this Access Right</param>
        /// <param name="ApplicationId">The application id associated with this access right</param>
        /// <param name="ResourceId">Resource Id associated with this access right</param>
        /// <param name="ActionId">Action id associated with this access right</param>
        /// <returns>An instance of <see cref="SiamResponse"/> that will contain the results
        /// of the call.</returns>
        public SiamResponse AddAccessRight(int RoleId, int ApplicationId, int ResourceId, int ActionId) {
            TimeSpan start = TimeKeeper.GetCurrentTime();
            //Logger.RaiseTrace(ClassName, "Entry:" + start.ToString(), TraceLevel.Verbose);
            string outMessage = "";
            SiamResponse r = new SiamResponse();
            try {
                r.SiamPayLoad = _dal.AddAccessRight(RoleId, ApplicationId, ResourceId, ActionId, out outMessage);
                if (outMessage != "") {
                    throw new Exception(outMessage);
                } else {
                    r.SiamResponseCode = (int)Messages.ADD_ACCESSRIGHT_COMPLETED;
                    r.SiamResponseMessage = Messages.ADD_ACCESSRIGHT_COMPLETED.ToString();
                }
            } catch (Exception ex) {
                r.SiamPayLoad = null;
                r.SiamResponseCode = (int)Messages.ADD_ACCESSRIGHT_FAILED;
                r.SiamResponseMessage = Messages.ADD_ACCESSRIGHT_FAILED.ToString() + " " + ex.Message;
                //Logger.RaiseTrace(ClassName, "Error=" + r.SiamResponseMessage, TraceLevel.Error);
            }
            TimeSpan end = TimeKeeper.GetCurrentTime();
            TimeSpan exectime = start.Subtract(end);
            //Logger.RaiseTrace(ClassName, "Exit:ExecutionTime=" + exectime.ToString(), TraceLevel.Verbose);
            return r;
        }


        /// <summary>
        /// Adds a user role to SIAM. Adding a user role is the process of assigning a role to a user.
        /// This has the effect of making that user a member of that role and will give that user all
        /// access rights defined by that role and any/all child roles. This is done using the concept
        /// of an aggregate collection of access rights. 
        /// </summary>
        /// <param name="UserId">The user id for whom the role is being assigned</param>
        /// <param name="RoleId">The role id being assigned</param>
        /// <returns>An instance of <see cref="SiamResponse"/> that will contain the results
        /// of the call.</returns>
        public SiamResponse AddUserRole(string UserId, int RoleId) {
            TimeSpan start = TimeKeeper.GetCurrentTime();
            //Logger.RaiseTrace(ClassName, "Entry:" + start.ToString(), TraceLevel.Verbose);
            string outMessage = "";
            SiamResponse r = new SiamResponse();
            try {
                r.SiamPayLoad = _dal.AddUserRole(UserId, RoleId, out outMessage);
                if (outMessage != "") {
                    throw new Exception(outMessage);
                } else {
                    r.SiamResponseCode = (int)Messages.ADD_USERROLE_COMPLETED;
                    r.SiamResponseMessage = Messages.ADD_USERROLE_COMPLETED.ToString();
                }
            } catch (Exception ex) {
                r.SiamPayLoad = null;
                r.SiamResponseCode = (int)Messages.ADD_USERROLE_FAILED;
                r.SiamResponseMessage = Messages.ADD_USERROLE_FAILED.ToString() + " " + ex.Message;
                //Logger.RaiseTrace(ClassName, "Error=" + r.SiamResponseMessage, TraceLevel.Error);
            }
            TimeSpan end = TimeKeeper.GetCurrentTime();
            TimeSpan exectime = start.Subtract(end);
            //Logger.RaiseTrace(ClassName, "Exit:ExecutionTime=" + exectime.ToString(), TraceLevel.Verbose);
            return r;
        }



        #endregion

        #region Update Methods


        /// <summary>
        /// Updates a SIAM user.
        /// </summary>
        /// <param name="user">An instance of <see cref="User"/> that contains the updated
        /// information.</param>
        /// <returns>An instance of <see cref="SiamResponse"/> that will contain the results
        /// of the call.</returns>
        public UpdateUserResponse UpdateUser(User user) {
            TimeSpan start = TimeKeeper.GetCurrentTime();
            //Logger.RaiseTrace(ClassName, "Entry:" + start.ToString(), TraceLevel.Verbose);
            string outMessage = string.Empty;
            UpdateUserResponse response = new UpdateUserResponse();

            try {
                if (!IsValidEmail(user.EmailAddress)) {
                    throw new Exception("Invalid Email Address");
                }

                //Check Added by chandra on 09-06-2007
                if (user.Password != null) {
                    if (user.Password != "-1-1-1-1") {
                        if (!IsValidPassword(user.Password, user.UserType)) {
                            throw new Exception("Invalid Password");
                        }
                    }
                }
                if (!IsValidUserId(user.UserId)) {
                    throw new Exception("Invalid UserId");
                }
                if (_dal.UpdateUser(user, out outMessage)) {
                    response.SiamResponseCode = (int)Messages.UPDATE_USER_COMPLETED;
                    response.SiamResponseMessage = Messages.UPDATE_USER_COMPLETED.ToString();
                } else {
                    throw new Exception(outMessage);
                }

            } catch (Exception ex) {
                response.SiamResponseCode = (int)Messages.UPDATE_USER_FAILED;
                response.SiamResponseMessage = Messages.UPDATE_USER_FAILED.ToString() + " " + ex.Message;
                //Logger.RaiseTrace(ClassName, "Error=" + response.SiamResponseMessage, TraceLevel.Error);
                if (outMessage == "PasswordMatch")
                    throw new Exception("PasswordMatch");
                if (outMessage == "DuplicateUser")
                    throw new Exception("DuplicateUser");

            }
            TimeSpan end = TimeKeeper.GetCurrentTime();
            TimeSpan exectime = start.Subtract(end);
            //Logger.RaiseTrace(ClassName, "Exit:ExecutionTime=" + exectime.ToString(), TraceLevel.Verbose);
            return response;
        }

        /// <summary>
        /// Updates an Action record in SIAM.
        /// </summary>
        /// <param name="ActionName">A string value containing the new action name</param>
        /// <param name="ActionId">The Id of the action to update</param>
        /// <returns>An instance of <see cref="SiamResponse"/> that will contain the results
        /// of the call.</returns>
        //6732 starts
        /*public SiamResponse UpdateAction(string ActionName, int ActionId)
        {
            TimeSpan start = TimeKeeper.GetCurrentTime();
            //Logger.RaiseTrace(ClassName, "Entry:" + start.ToString(), TraceLevel.Verbose);
            string outMessage;
            SiamResponse r = new SiamResponse();
            try
            {
                if (_dal.UpdateAction(ActionName, ActionId, out outMessage))
                {
                    r.SiamResponseMessage = Messages.UPDATE_ACTION_COMPLETED.ToString();
                    r.SiamResponseCode = (int)Messages.UPDATE_ACTION_COMPLETED;
                }
                else
                {
                    throw new System.Exception(outMessage);
                }
            }
            catch (System.Exception ex)
            {
                r.SiamResponseCode = (int)Messages.UPDATE_ACTION_FAILED;
                r.SiamResponseMessage = Messages.UPDATE_ACTION_FAILED.ToString() + " " + ex.Message;
                //Logger.RaiseTrace(ClassName, "Error=" + r.SiamResponseMessage, TraceLevel.Error);
            }
            TimeSpan end = TimeKeeper.GetCurrentTime();
            TimeSpan exectime = start.Subtract(end);
            //Logger.RaiseTrace(ClassName, "Exit:ExecutionTime=" + exectime.ToString(), TraceLevel.Verbose);
            return r;

        }*/
        //6732 ends


        /// <summary>
        /// Updates an application in SIAM.
        /// </summary>
        /// <param name="ApplicationName">A string value containing the new application name.</param>
        /// <param name="ApplicationId">An integer value contianing the application id.</param>
        /// <returns>An instance of <see cref="SiamResponse"/> that will contain the results
        /// of the call.</returns>
        public SiamResponse UpdateApplication(string ApplicationName, int ApplicationId) {
            TimeSpan start = TimeKeeper.GetCurrentTime();
            //Logger.RaiseTrace(ClassName, "Entry:" + start.ToString(), TraceLevel.Verbose);
            string outMessage;
            SiamResponse r = new SiamResponse();
            try {
                if (_dal.UpdateApplication(ApplicationName, ApplicationId, out outMessage)) {
                    r.SiamResponseMessage = Messages.UPDATE_APPLICATION_COMPLETED.ToString();
                    r.SiamResponseCode = (int)Messages.UPDATE_ACTION_COMPLETED;
                } else {
                    throw new Exception(outMessage);
                }
            } catch (Exception ex) {
                r.SiamResponseCode = (int)Messages.UPDATE_ACTION_FAILED;
                r.SiamResponseMessage = Messages.UPDATE_ACTION_FAILED.ToString() + " " + ex.Message;
                //Logger.RaiseTrace(ClassName, "Error=" + r.SiamResponseMessage, TraceLevel.Error);
            }
            TimeSpan end = TimeKeeper.GetCurrentTime();
            TimeSpan exectime = start.Subtract(end);
            //Logger.RaiseTrace(ClassName, "Exit:ExecutionTime=" + exectime.ToString(), TraceLevel.Verbose);
            return r;
        }

        /// <summary>
        /// Updates a resource record in SIAM.
        /// </summary>
        /// <param name="ResourceName">A string value containing the new resource name.</param>
        /// <param name="ResourceId">An integer value containing the resource id</param>
        /// <returns>An instance of <see cref="SiamResponse"/> that will contain the results
        /// of the call.</returns>
        public SiamResponse UpdateResource(string ResourceName, int ResourceId) {
            TimeSpan start = TimeKeeper.GetCurrentTime();
            //Logger.RaiseTrace(ClassName, "Entry:" + start.ToString(), TraceLevel.Verbose);
            string outMessage;
            SiamResponse r = new SiamResponse();
            try {
                if (_dal.UpdateResource(ResourceName, ResourceId, out outMessage)) {
                    r.SiamResponseMessage = Messages.UPDATE_RESOURCE_COMPLETED.ToString();
                    r.SiamResponseCode = (int)Messages.UPDATE_RESOURCE_COMPLETED;
                } else {
                    throw new Exception(outMessage);
                }
            } catch (Exception ex) {
                r.SiamResponseCode = (int)Messages.UPDATE_RESOURCE_FAILED;
                r.SiamResponseMessage = Messages.UPDATE_RESOURCE_FAILED.ToString() + " " + ex.Message;
                //Logger.RaiseTrace(ClassName, "Error=" + r.SiamResponseMessage, TraceLevel.Error);
            }
            TimeSpan end = TimeKeeper.GetCurrentTime();
            TimeSpan exectime = start.Subtract(end);
            //Logger.RaiseTrace(ClassName, "Exit:ExecutionTime=" + exectime.ToString(), TraceLevel.Verbose);
            return r;
        }

        /// <summary>
        /// Updates a role in SIAM.
        /// </summary>
        /// <param name="RoleName">A string value containing the new Role name</param>
        /// <param name="RoleId">An integer value containing the role id.</param>
        /// <returns>An instance of <see cref="SiamResponse"/> that will contain the results
        /// of the call.</returns>
        public SiamResponse UpdateRole(string RoleName, int RoleId) {
            TimeSpan start = TimeKeeper.GetCurrentTime();
            //Logger.RaiseTrace(ClassName, "Entry:" + start.ToString(), TraceLevel.Verbose);
            string outMessage;
            SiamResponse r = new SiamResponse();
            try {
                if (_dal.UpdateRole(RoleName, RoleId, out outMessage)) {
                    r.SiamResponseMessage = Messages.UPDATE_ROLE_COMPLETED.ToString();
                    r.SiamResponseCode = (int)Messages.UPDATE_ROLE_COMPLETED;
                } else {
                    throw new Exception(outMessage);
                }
            } catch (Exception ex) {
                r.SiamResponseCode = (int)Messages.UPDATE_ROLE_FAILED;
                r.SiamResponseMessage = Messages.UPDATE_ROLE_FAILED.ToString() + " " + ex.Message;
                //Logger.RaiseTrace(ClassName, "Error=" + r.SiamResponseMessage, TraceLevel.Error);
            }
            TimeSpan end = TimeKeeper.GetCurrentTime();
            TimeSpan exectime = start.Subtract(end);
            //Logger.RaiseTrace(ClassName, "Exit:ExecutionTime=" + exectime.ToString(), TraceLevel.Verbose);
            return r;
        }

        /// <summary>
        /// Updates a Role Functionality Mapping in SIAM.
        /// </summary>
        /// <param name="RoleName">A string value containing the new Role name</param>
        /// <param name="RoleId">An integer value containing the role id.</param>
        /// <returns>An instance of <see cref="SiamResponse"/> that will contain the results
        /// of the call.</returns>
        public SiamResponse UpdateRoleFunctionalityMapping(string RoleId, string AddFuncId, string DelFuncId) {
            TimeSpan start = TimeKeeper.GetCurrentTime();
            //Logger.RaiseTrace(ClassName, "Entry:" + start.ToString(), TraceLevel.Verbose);
            string outMessage;
            SiamResponse r = new SiamResponse();
            try {
                if (_dal.UpdateRoleFunction(RoleId, AddFuncId, DelFuncId, out outMessage)) {
                    r.SiamResponseMessage = Messages.UPDATE_ROLEFUNMAP_COMPLETED.ToString();
                    r.SiamResponseCode = (int)Messages.UPDATE_ROLEFUNMAP_COMPLETED;
                } else {
                    throw new Exception(outMessage);
                }
            } catch (Exception ex) {
                r.SiamResponseCode = (int)Messages.UPDATE_ROLEFUNMAP_FAILED;
                r.SiamResponseMessage = Messages.UPDATE_ROLEFUNMAP_FAILED.ToString() + " " + ex.Message;
                //Logger.RaiseTrace(ClassName, "Error=" + r.SiamResponseMessage, TraceLevel.Error);
            }
            TimeSpan end = TimeKeeper.GetCurrentTime();
            TimeSpan exectime = start.Subtract(end);
            //Logger.RaiseTrace(ClassName, "Exit:ExecutionTime=" + exectime.ToString(), TraceLevel.Verbose);
            return r;
        }


        /// <summary>
        /// Update Bulk Email after imaging database from Production
        /// </summary>
        /// <param name="Email"></param>
        /// <returns></returns>
        //commented by Sunil as UpdateBULKEMail is not used on 07-01-10
        //public SiamResponse UpdateBulkEMAIL(string Email)
        //{
        //    TimeSpan start = TimeKeeper.GetCurrentTime();
        //    //Logger.RaiseTrace(ClassName, "Entry:" + start.ToString(), TraceLevel.Verbose);
        //    string outMessage;
        //    SiamResponse r = new SiamResponse();
        //    try
        //    {
        //        if (_dal.UpdateBULKEMAIL(Email, out outMessage))
        //        {
        //            r.SiamResponseMessage = Messages.UPDATE_BULK_EMAIL_COMPLETED.ToString();
        //            r.SiamResponseCode = (int)Messages.UPDATE_BULK_EMAIL_COMPLETED;
        //        }
        //        else
        //        {
        //            throw new System.Exception(outMessage);
        //        }
        //    }
        //    catch (System.Exception ex)
        //    {
        //        r.SiamResponseCode = (int)Messages.UPDATE_BULK_EMAIL_FAILED;
        //        r.SiamResponseMessage = Messages.UPDATE_BULK_EMAIL_FAILED.ToString() + " " + ex.Message;
        //        //Logger.RaiseTrace(ClassName, "Error=" + r.SiamResponseMessage, TraceLevel.Error);
        //    }
        //    TimeSpan end = TimeKeeper.GetCurrentTime();
        //    TimeSpan exectime = start.Subtract(end);
        //    //Logger.RaiseTrace(ClassName, "Exit:ExecutionTime=" + exectime.ToString(), TraceLevel.Verbose);
        //    return r;
        //}


        #endregion

        #region Delete Methods

        /// <summary>
        /// Sets the isDeleted flag on this User Record in SIAM.
        /// </summary>
        /// <param name="UserId">A string value containing the User's SIAM Identity.</param>
        /// <param name="DeletedBy">Deleted By User Information</param>
        /// <returns>An instance of <see cref="SiamResponse"/> that will contain the results
        /// of the call.</returns>
        public SiamResponse DeleteUser(string UserId, User DeletedBy) {
            TimeSpan start = TimeKeeper.GetCurrentTime();
            //Logger.RaiseTrace(ClassName, "Entry:" + start.ToString(), TraceLevel.Verbose);
            SiamResponse r = new SiamResponse();
            try {
                User oUser = new User();
                if (_dal.DeleteUser(UserId, out r.SiamResponseMessage, out oUser)) {
                    //read config file for from and to id
                    //SIAMConfig config = CachingServices.Cache["SiamConfiguration"] as SIAMConfig;
                    //if (oUser.UserType.ToUpper() == "ACCOUNTHOLDER" && (oUser.AlternateUserId != null || oUser.AlternateUserId != string.Empty))//6668
                    if ((oUser.UserType.ToUpper() == "A" || oUser.UserType.ToUpper() == "P") && (oUser.AlternateUserId != null || oUser.AlternateUserId != string.Empty))//6668
                    {
                        string strBody = string.Empty;
                        string strBody1 = string.Empty;
                        strBody = strBody + (@"<html><body>");
                        strBody = strBody + (Environment.NewLine + @"<table cellpadding=3 cellspacing=1 border=0 style=""width: 80%;font-family: Arial, Helvetica, sans-serif; font-size: 11px; border-bottom-width:1px; border-right-style:solid; border-right-width:1px; border-left-style:solid; border-left-width:1px; border-top-style:solid; border-top-width:1px; border-bottom-style:solid; border-bottom-width:1px;"">");
                        strBody = strBody + (@"<tr valign=top><td style=""border-bottom-style:solid; border-bottom-width:thin;""><img src=""https://www.servicesplus.sel.sony.com/images/sp_int_sonylogo.gif"" /></td><td  style=""border-bottom-style:solid; border-bottom-width:thin;"" align=right valign=baseline><strong style=""font-size:small"">ServicesPLUS Admin : " + ConfigurationData.GeneralSettings.Environment + "</strong></td></tr>");
                        strBody = strBody + (Environment.NewLine + @"<tr valign=top><td colspan=""2"" style=""border-bottom-style:solid; border-bottom-width:thin;""><table border=""0"" cellpadding=""1"" cellspacing=""1"" style=""width: 100%""><tr><td colspan=""2"" width=100% style=""height: 16px"" align=left><strong>FYI: Following B2B user has been deleted from the ServicesPLUS system</strong></td></tr></table></td></tr>");
                        //strBody = strBody + (@"<tr valign=top bgColor=""#d5dee9""><td style=""width: 173px;"">ALTERNATEUSERNAME:</td><td style=""width: 376px;"">" + oUser.AlternateUserId + "</td></tr>");
                        strBody = strBody + (@"<tr valign=top ><td style=""width: 173px;"">EMAIL ADDRESS:</td><td style=""width: 376px;"">" + oUser.EmailAddress + "</td></tr>");
                        strBody = strBody + (Environment.NewLine + @"<tr valign=top bgColor=""#d5dee9""><td style=""width: 173px;"">FIRSTNAME:</td><td style=""width: 376px;"">" + oUser.FirstName + "</td></tr>");
                        strBody = strBody + (@"<tr valign=top ><td style=""width: 173px;"">LASTNAME:</td><td style=""width: 376px;"">" + oUser.LastName + "</td></tr>");
                        strBody = strBody + (@"<tr valign=top bgColor=""#d5dee9""><td style=""width: 173px;"">COMPANYNAME:</td><td style=""width: 376px;"">" + oUser.CompanyName + "</td></tr>");
                        strBody = strBody + (Environment.NewLine + @"<tr valign=top ><td style=""width: 173px;"">ADDRESS 1:</td><td style=""width: 376px;"">" + oUser.AddressLine1 + "</td></tr>");
                        strBody = strBody + (@"<tr valign=top bgColor=""#d5dee9""><td style=""width: 173px;"">ADDRESS 2:</td><td style=""width: 376px;"">" + oUser.AddressLine2 + @"</td></tr>");
                        strBody1 = strBody1 + (@"<tr valign=top ><td style=""width: 173px;"">CITY:</td><td style=""width: 376px;"">" + oUser.City + "</td></tr>");
                        strBody1 = strBody1 + (Environment.NewLine + @"<tr valign=top bgColor=""#d5dee9""><td style=""width: 173px;"">STATE:</td><td style=""width: 376px;"">" + oUser.State + "</td></tr>");
                        strBody1 = strBody1 + (@"<tr valign=top ><td style=""width: 173px;"">ZIP:</td><td style=""width: 376px;"">" + oUser.Zip + "</td></tr>");
                        strBody1 = strBody1 + (@"<tr valign=top bgColor=""#d5dee9""><td style=""width: 173px;"">PHONE:</td><td style=""width: 376px;"">" + oUser.Phone + "</td></tr>");
                        strBody1 = strBody1 + (Environment.NewLine + @"<tr valign=top ><td style=""width: 173px;"">SIAM ID:</td><td style=""width: 376px;"">" + oUser.Identity + "</td></tr>");
                        strBody1 = strBody1 + (@"<tr valign=top bgColor=""#d5dee9""><td style=""width: 173px;"" colspan=2>&nbsp;</td></tr>");

                        strBody1 = strBody1 + (@"<tr valign=top><td style=""width: 173px;"" colspan=2><strong style=""font-size:small"">Deleted By:</strong></td></tr>");
                        strBody1 = strBody1 + (Environment.NewLine + @"<tr valign=top bgColor=""#d5dee9""><td style=""width: 173px;"">First Name:</td><td style=""width: 376px;"">" + DeletedBy.FirstName + "</td></tr>");
                        strBody1 = strBody1 + (Environment.NewLine + @"<tr valign=top ><td style=""width: 173px;"">Last Name:</td><td style=""width: 376px;"">" + DeletedBy.LastName + "</td></tr>");
                        strBody1 = strBody1 + (Environment.NewLine + @"<tr valign=top bgColor=""#d5dee9""><td style=""width: 173px;"">Global ID:</td><td style=""width: 376px;"">" + DeletedBy.UserId + "</td></tr>");
                        strBody1 = strBody1 + (@"<tr valign=top><td style=""width: 173px;"" colspan=2>&nbsp;</td></tr>");
                        strBody1 = strBody1 + (Environment.NewLine + @"</table></body></html>");


                        string sToMailId = "";
                        string sSubject = "Deleted B2B User Information : " + ConfigurationData.GeneralSettings.Environment;
                        string sFromMailID = ConfigurationData.GeneralSettings.Email.Admin.SIAMAdminMailId;
                        sToMailId = ConfigurationData.GeneralSettings.Email.Admin.SonysecMailId + ";" + ConfigurationData.GeneralSettings.Email.Admin.LDAPMailId;
                        new Mailer().SendMail(strBody + strBody1, sToMailId, sFromMailID, sSubject, true, ConfigurationData.Environment.SMTP.EmailServer_CS);
                    }
                } else {
                    throw new Exception(r.SiamResponseMessage);
                }
            } catch (Exception ex) {
                r.SiamResponseCode = (int)Messages.DELETE_USER_FAILED;
                r.SiamResponseMessage = Messages.DELETE_USER_FAILED.ToString() + " " + ex.Message;
                //Logger.RaiseTrace(ClassName, "Error=" + r.SiamResponseMessage, TraceLevel.Error);
            }
            TimeSpan end = TimeKeeper.GetCurrentTime();
            TimeSpan exectime = start.Subtract(end);
            //Logger.RaiseTrace(ClassName, "Exit:ExecutionTime=" + exectime.ToString(), TraceLevel.Verbose);
            return r;
        }


        /// <summary>
        /// Sets the isDeleted flag for the Action in SIAM.
        /// </summary>
        /// <param name="ActionId">An integer value containing the id of the record</param>
        /// <returns>An instance of <see cref="SiamResponse"/> that will contain the results
        /// of the call.</returns>
        //6732 starts
        /*public SiamResponse DeleteAction(int ActionId)
        {
            TimeSpan start = TimeKeeper.GetCurrentTime();
            //Logger.RaiseTrace(ClassName, "Entry:" + start.ToString(), TraceLevel.Verbose);
            SiamResponse r = new SiamResponse();
            try
            {
                if (_dal.DeleteAction(ActionId, out r.SiamResponseMessage))
                {
                    r.SiamResponseCode = (int)Messages.DELETE_ACTION_COMPLETED;
                    r.SiamResponseMessage = Messages.DELETE_ACTION_COMPLETED.ToString();
                }
                else
                {
                    throw new System.Exception(r.SiamResponseMessage);
                }
            }
            catch (System.Exception ex)
            {
                r.SiamResponseCode = (int)Messages.DELETE_ACTION_FAILED;
                r.SiamResponseMessage = Messages.DELETE_ACTION_FAILED.ToString() + " " + ex.Message;
                //Logger.RaiseTrace(ClassName, "Error=" + r.SiamResponseMessage, TraceLevel.Error);
            }
            TimeSpan end = TimeKeeper.GetCurrentTime();
            TimeSpan exectime = start.Subtract(end);
            //Logger.RaiseTrace(ClassName, "Exit:ExecutionTime=" + exectime.ToString(), TraceLevel.Verbose);
            return r;
        }*/
        //6732 ends

        /// <summary>
        /// Sets the isDeleted flag for an application in SIAM.
        /// </summary>
        /// <param name="ApplicationId">An integer value containing the application Id.</param>
        /// <returns>An instance of <see cref="SiamResponse"/> that will contain the results
        /// of the call.</returns>
        public SiamResponse DeleteApplication(int ApplicationId) {
            TimeSpan start = TimeKeeper.GetCurrentTime();
            //Logger.RaiseTrace(ClassName, "Entry:" + start.ToString(), TraceLevel.Verbose);
            SiamResponse r = new SiamResponse();
            try {
                if (_dal.DeleteApplication(ApplicationId, out r.SiamResponseMessage)) {
                    r.SiamResponseCode = (int)Messages.DELETE_APPLICATION_COMPLETED;
                    r.SiamResponseMessage = Messages.DELETE_APPLICATION_COMPLETED.ToString();
                } else {
                    throw new Exception(r.SiamResponseMessage);
                }
            } catch (Exception ex) {
                r.SiamResponseCode = (int)Messages.DELETE_APPLICATION_FAILED;
                r.SiamResponseMessage = Messages.DELETE_APPLICATION_FAILED.ToString() + " " + ex.Message;
                //Logger.RaiseTrace(ClassName, "Error=" + r.SiamResponseMessage, TraceLevel.Error);
            }
            TimeSpan end = TimeKeeper.GetCurrentTime();
            TimeSpan exectime = start.Subtract(end);
            //Logger.RaiseTrace(ClassName, "Exit:ExecutionTime=" + exectime.ToString(), TraceLevel.Verbose);
            return r;
        }


        /// <summary>
        /// Sets the isDeleted flag on an Application Resource in SIAM.
        /// </summary>
        /// <param name="ApplicationId">An integer value containing the Application Id.</param>
        /// <param name="ResourceId">An integer value containing the Resource Id.</param>
        /// <returns>An instance of <see cref="SiamResponse"/> that will contain the results
        /// of the call.</returns>
        public SiamResponse DeleteApplicationResource(int ApplicationId, int ResourceId) {
            TimeSpan start = TimeKeeper.GetCurrentTime();
            //Logger.RaiseTrace(ClassName, "Entry:" + start.ToString(), TraceLevel.Verbose);
            SiamResponse r = new SiamResponse();
            try {
                if (_dal.DeleteApplicationResource(ApplicationId, ResourceId, out r.SiamResponseMessage)) {
                    r.SiamResponseCode = (int)Messages.DELETE_APPLICATION_RESOURCE_COMPLETED;
                    r.SiamResponseMessage = Messages.DELETE_APPLICATION_RESOURCE_COMPLETED.ToString();
                } else {
                    throw new Exception(r.SiamResponseMessage);
                }
            } catch (Exception ex) {
                r.SiamResponseCode = (int)Messages.DELETE_APPLICATION_RESOURCE_FAILED;
                r.SiamResponseMessage = Messages.DELETE_APPLICATION_RESOURCE_FAILED.ToString() + " " + ex.Message;
                //Logger.RaiseTrace(ClassName, "Error=" + r.SiamResponseMessage, TraceLevel.Error);
            }
            TimeSpan end = TimeKeeper.GetCurrentTime();
            TimeSpan exectime = start.Subtract(end);
            //Logger.RaiseTrace(ClassName, "Exit:ExecutionTime=" + exectime.ToString(), TraceLevel.Verbose);
            return r;
        }


        /// <summary>
        /// Sets the isDeleted flag on a Resource Action in SIAM.
        /// Note: This method is devalued, and may not be included in the final API.
        /// </summary>
        /// <param name="ResourceId">An integer value containing the Resource Id.</param>
        /// <param name="ActionId">An integer value containing the Action Id.</param>
        /// <returns>An instance of <see cref="SiamResponse"/> that will contain the results
        /// of the call.</returns>
        public SiamResponse DeleteResourceAction(int ResourceId, int ActionId) {
            TimeSpan start = TimeKeeper.GetCurrentTime();
            //Logger.RaiseTrace(ClassName, "Entry:" + start.ToString(), TraceLevel.Verbose);
            SiamResponse r = new SiamResponse();
            try {
                if (_dal.DeleteResourceAction(ResourceId, ActionId, out r.SiamResponseMessage)) {
                    r.SiamResponseCode = (int)Messages.DELETE_RESOURCE_ACTION_COMPLETED;
                    r.SiamResponseMessage = Messages.DELETE_RESOURCE_ACTION_COMPLETED.ToString();
                } else {
                    throw new Exception(r.SiamResponseMessage);
                }
            } catch (Exception ex) {
                r.SiamResponseCode = (int)Messages.DELETE_RESOURCE_ACTION_FAILED;
                r.SiamResponseMessage = Messages.DELETE_RESOURCE_ACTION_FAILED.ToString() + " " + ex.Message;
                //Logger.RaiseTrace(ClassName, "Error=" + ex.Message, TraceLevel.Error);
            }
            TimeSpan end = TimeKeeper.GetCurrentTime();
            TimeSpan exectime = start.Subtract(end);
            //Logger.RaiseTrace(ClassName, "Exit:ExecutionTime=" + exectime.ToString(), TraceLevel.Verbose);
            return r;
        }


        /// <summary>
        /// Sets the isDeleted flag on a Role in SIAM.
        /// </summary>
        /// <param name="RoleId">An integer value containing the RoleId</param>
        /// <returns>An instance of <see cref="SiamResponse"/> that will contain the results
        /// of the call.</returns>
        public SiamResponse DeleteRole(int RoleId) {
            TimeSpan start = TimeKeeper.GetCurrentTime();
            //Logger.RaiseTrace(ClassName, "Entry:" + start.ToString(), TraceLevel.Verbose);
            SiamResponse r = new SiamResponse();
            try {
                if (_dal.DeleteRole(RoleId, out r.SiamResponseMessage)) {
                    r.SiamResponseCode = (int)Messages.DELETE_ROLE_COMPLETED;
                    r.SiamResponseMessage = Messages.DELETE_ROLE_COMPLETED.ToString();
                } else {
                    throw new Exception(r.SiamResponseMessage);
                }
            } catch (Exception ex) {
                r.SiamResponseCode = (int)Messages.DELETE_ROLE_FAILED;
                r.SiamResponseMessage = Messages.DELETE_ROLE_FAILED.ToString() + " " + ex.Message;
                //Logger.RaiseTrace(ClassName, "Error=" + ex.Message, TraceLevel.Error);
            }
            TimeSpan end = TimeKeeper.GetCurrentTime();
            TimeSpan exectime = start.Subtract(end);
            //Logger.RaiseTrace(ClassName, "Exit:ExecutionTime=" + exectime.ToString(), TraceLevel.Verbose);
            return r;
        }


        /// <summary>
        /// Sets the isDeleted flag on a UserRole in SIAM.
        /// </summary>
        /// <param name="UserId">An integer value containing the SIAM user's identity.</param>
        /// <param name="RoleId">An integer value containing the Role Id.</param>
        /// <returns>An instance of <see cref="SiamResponse"/> that will contain the results
        /// of the call.</returns>
        public SiamResponse DeleteUserRole(string UserId, int RoleId) {
            TimeSpan start = TimeKeeper.GetCurrentTime();
            //Logger.RaiseTrace(ClassName, "Entry:" + start.ToString(), TraceLevel.Verbose);
            SiamResponse r = new SiamResponse();
            try {
                if (_dal.DeleteUserRole(UserId, RoleId, out r.SiamResponseMessage)) {
                    r.SiamResponseCode = (int)Messages.DELETE_USERROLE_COMPLETED;
                    r.SiamResponseMessage = Messages.DELETE_USERROLE_COMPLETED.ToString();
                } else {
                    throw new Exception(r.SiamResponseMessage);
                }
            } catch (Exception ex) {
                r.SiamResponseCode = (int)Messages.DELETE_USERROLE_FAILED;
                r.SiamResponseMessage = Messages.DELETE_USERROLE_FAILED.ToString() + " " + ex.Message;
                //Logger.RaiseTrace(ClassName, "Error=" + r.SiamResponseMessage, TraceLevel.Error);
            }
            TimeSpan end = TimeKeeper.GetCurrentTime();
            TimeSpan exectime = start.Subtract(end);
            //Logger.RaiseTrace(ClassName, "Exit:ExecutionTime=" + exectime.ToString(), TraceLevel.Verbose);
            return r;
        }

        #endregion

        #region Get Methods


        /// <summary>
        /// This method obtains an array of <see cref="AccessRight"/> objects that make up the
        /// aggregate rights for all the roles in the <see cref="Role"/> array. A SIAM user can
        /// be associated with any number of roles. A SIAM role can have any number of child roles. Each 
        /// <see cref="Role"/> contains a collection of <see cref="AccessRight"/> objects.
        /// This method will recurse through a users Role Collection and return a complete set of aggregate rights
        /// for the combined set of Role for which that user is associated.
        /// </summary>
        /// <param name="roles">An array of <see cref="Role"/> objects</param>
        /// <returns>An instance of <see cref="SiamResponse"/> that will contain the results
        /// of the call.</returns>
        public SiamResponse GetEffectiveAccessRights(Role[] roles) {
            TimeSpan start = TimeKeeper.GetCurrentTime();
            //Logger.RaiseTrace(ClassName, "Entry:" + start.ToString(), TraceLevel.Verbose);
            string outMessage;
            siam.SiamResponse r = new SiamResponse();
            try {
                r.SiamPayLoad = _dal.GetEffectiveAccessRights(roles, out outMessage);
                if (outMessage != "") {
                    throw new Exception(outMessage);
                } else {
                    r.SiamResponseCode = (int)Messages.GET_ACCESSRIGHTS_COMPLETED;
                    r.SiamResponseMessage = Messages.GET_ACCESSRIGHTS_COMPLETED.ToString();
                }
            } catch (Exception ex) {
                r.SiamPayLoad = null;
                r.SiamResponseCode = (int)Messages.GET_ACCESSRIGHTS_FAILED;
                r.SiamResponseMessage = Messages.GET_ACCESSRIGHTS_FAILED.ToString() + " " + ex.Message;
                //Logger.RaiseTrace(ClassName, "Error=" + r.SiamResponseMessage, TraceLevel.Error);
            }
            TimeSpan end = TimeKeeper.GetCurrentTime();
            TimeSpan exectime = start.Subtract(end);
            //Logger.RaiseTrace(ClassName, "Exit:ExecutionTime=" + exectime.ToString(), TraceLevel.Verbose);
            return r;
        }

        /// <summary>
        /// Gets a user from SIAM based on the user identity.
        /// </summary>
        /// <param name="identity">A string value containing the identity of the user.</param>
        /// <returns>An instance of <see cref="SiamResponse"/> that will contain the results
        /// of the call.</returns>
        public System.Data.DataSet GetSiamAndCustomer(string identity) {
            TimeSpan start = TimeKeeper.GetCurrentTime();
            String ErrorString = "";
            System.Data.DataSet oDS = new System.Data.DataSet();
            //Logger.RaiseTrace(ClassName, "Entry:" + start.ToString(), TraceLevel.Verbose);
            try {
                oDS = _dal.GetSiamAndCustomer(identity, out ErrorString);
                if (ErrorString != "") {
                    throw new Exception(ErrorString);
                }
            } catch (Exception ex) {
                throw new Exception(ex.Message);
                //Logger.RaiseTrace(ClassName, "Error=" + ex.Message, TraceLevel.Error);
            }
            TimeSpan end = TimeKeeper.GetCurrentTime();
            TimeSpan exectime = start.Subtract(end);
            //Logger.RaiseTrace(ClassName, "Exit:ExecutionTime=" + exectime.ToString(), TraceLevel.Verbose);
            return oDS;
        }


        /// <summary>
        /// Gets a user from SIAM based on the user identity.
        /// </summary>
        /// <param name="identity">A string value containing the identity of the user.</param>
        /// <returns>An instance of <see cref="SiamResponse"/> that will contain the results
        /// of the call.</returns>
        public FetchUserResponse GetUser(string identity) {
            TimeSpan start = TimeKeeper.GetCurrentTime();
            //Logger.RaiseTrace(ClassName, "Entry:" + start.ToString(), TraceLevel.Verbose);
            FetchUserResponse response = new FetchUserResponse();
            try {
                response.TheUser = _dal.GetUser(identity, out response.SiamResponseMessage);
                if (response.SiamResponseMessage != "") {
                    throw new Exception(response.SiamResponseMessage);
                } else {
                    response.SiamResponseCode = (int)Messages.GET_USER_COMPLETED;
                    response.SiamResponseMessage = Messages.GET_USER_COMPLETED.ToString();
                }
            } catch (Exception ex) {
                response.TheUser = null;
                response.SiamResponseCode = (int)Messages.GET_USER_FAILED;
                response.SiamResponseMessage = Messages.GET_USER_FAILED.ToString() + " " + ex.Message;
                //Logger.RaiseTrace(ClassName, "Error=" + response.SiamResponseMessage, TraceLevel.Error);
            }
            TimeSpan end = TimeKeeper.GetCurrentTime();
            TimeSpan exectime = start.Subtract(end);
            //Logger.RaiseTrace(ClassName, "Exit:ExecutionTime=" + exectime.ToString(), TraceLevel.Verbose);
            return response;
        }

        /// <summary>
        /// Returns a user based on User Identity and Email Address
        /// </summary>
        /// <param name="UserName"></param>
        /// <param name="Email"></param>
        /// <returns></returns>
        //public FetchUserResponse GetUserWithEmail(string UserName, string Email)//6691
        public FetchUserResponse GetUserWithEmail(string UserName) {
            TimeSpan start = TimeKeeper.GetCurrentTime();
            //Logger.RaiseTrace(ClassName, "Entry:" + start.ToString(), TraceLevel.Verbose);
            FetchUserResponse response = new FetchUserResponse();
            try {
                response.TheUser = _dal.GetUserWithEmail(UserName, out response.SiamResponseMessage);//6691
                if (response.SiamResponseMessage != "") {
                    throw new Exception(response.SiamResponseMessage);
                } else {
                    response.SiamResponseCode = (int)Messages.GET_USER_COMPLETED;
                    response.SiamResponseMessage = Messages.GET_USER_COMPLETED.ToString();
                }
            } catch (Exception ex) {
                throw ex;
            }
            TimeSpan end = TimeKeeper.GetCurrentTime();
            TimeSpan exectime = start.Subtract(end);
            //Logger.RaiseTrace(ClassName, "Exit:ExecutionTime=" + exectime.ToString(), TraceLevel.Verbose);
            return response;
        }


        /// <summary>
        /// Gets a listing of valid user types known by SIAM. User types in SIAM are defined in configuration.
        /// The values will be returned a string array.
        /// </summary>
        /// <returns>An instance of <see cref="SiamResponse"/> that will contain the results
        /// of the call.</returns>
        public SiamResponse GetUserTypes() {
            TimeSpan start = TimeKeeper.GetCurrentTime();
            //Logger.RaiseTrace(ClassName, "Entry:" + start.ToString(), TraceLevel.Verbose);
            string outMessage;
            SiamResponse r = new SiamResponse();
            try {
                string[] Names = _dal.GetUserTypes(out outMessage);
                if (outMessage != "") {
                    throw new Exception(outMessage);
                } else {
                    r.SiamPayLoad = Names;
                    r.SiamResponseCode = (int)Messages.GET_USERTYPES_COMPLETED;
                    r.SiamResponseMessage = Messages.GET_USERTYPES_COMPLETED.ToString();
                }
            } catch (Exception ex) {
                r.SiamPayLoad = null;
                r.SiamResponseCode = (int)Messages.GET_USERTYPES_FAILED;
                r.SiamResponseMessage = Messages.GET_USERTYPES_FAILED.ToString() + " " + ex.Message;
                //Logger.RaiseTrace(ClassName, "Error=" + r.SiamResponseMessage, TraceLevel.Error);
            }
            TimeSpan end = TimeKeeper.GetCurrentTime();
            TimeSpan exectime = start.Subtract(end);
            //Logger.RaiseTrace(ClassName, "Exit:ExecutionTime=" + exectime.ToString(), TraceLevel.Verbose);
            return r;
        }


        /// <summary>
        /// Returns a listing of valid status codes and their associted Id's known by SIAM. This method
        /// will return an array of <see cref="UserStatus"/> objects.
        /// </summary>
        /// <returns>An instance of <see cref="SiamResponse"/> that will contain the results
        /// of the call.</returns>
        /// //6668 starts
        //public SiamResponse GetStatusCodes()
        //{
        //    TimeSpan start = TimeKeeper.GetCurrentTime();
        //    //Logger.RaiseTrace(ClassName, "Entry:" + start.ToString(), TraceLevel.Verbose);
        //    string outMessage;
        //    SiamResponse r = new SiamResponse();
        //    try
        //    {
        //        UserStatus[] results = _dal.GetStatusCodes(out outMessage);
        //        if (outMessage != "")
        //        {
        //            throw new System.Exception(outMessage);
        //        }
        //        else
        //        {
        //            r.SiamPayLoad = results;
        //            r.SiamResponseCode = (int)Messages.GET_STATUSCODES_COMPLETED;
        //            r.SiamResponseMessage = Messages.GET_STATUSCODES_COMPLETED.ToString();
        //        }
        //    }
        //    catch (System.Exception ex)
        //    {
        //        r.SiamPayLoad = null;
        //        r.SiamResponseCode = (int)Messages.GET_STATUSCODES_FAILED;
        //        r.SiamResponseMessage = Messages.GET_STATUSCODES_FAILED + " " + ex.Message;
        //        //Logger.RaiseTrace(ClassName, "Error=" + r.SiamResponseMessage, TraceLevel.Error);
        //    }
        //    TimeSpan end = TimeKeeper.GetCurrentTime();
        //    TimeSpan exectime = start.Subtract(end);
        //    //Logger.RaiseTrace(ClassName, "Exit:ExecutionTime=" + exectime.ToString(), TraceLevel.Verbose);
        //    return r;
        //}
        //6668 ends

        /// <summary>
        /// Returns a listing of users known by SIAM. There are two basic containers used to represent
        /// a user in SIAM. The normal <see cref="User"/> and the <see cref="ShallowUser"/> objects.
        /// This method will return an array of <see cref="ShallowUser"/> objects and is ideal
        /// for UI screen population of data where a fully hydrated user object is not required. 
        /// </summary>
        /// <returns>An instance of <see cref="SiamResponse"/> that will contain the results
        /// of the call.</returns>
        public SiamResponse GetUsersShallow() {
            TimeSpan start = TimeKeeper.GetCurrentTime();
            //Logger.RaiseTrace(ClassName, "Entry:" + start.ToString(), TraceLevel.Verbose);
            string outMessage;
            SiamResponse response = new SiamResponse();
            try {
                response.SiamPayLoad = _dal.GetUsers(false, out outMessage);
                if (outMessage != "") {
                    throw new Exception(outMessage);
                } else {
                    response.SiamResponseCode = (int)Messages.GET_USERS_COMPLETED;
                    response.SiamResponseMessage = Messages.GET_USERS_COMPLETED.ToString();
                }
            } catch (Exception ex) {
                response.SiamPayLoad = null;
                response.SiamResponseCode = (int)Messages.GET_USERS_FAILED;
                response.SiamResponseMessage = Messages.GET_USERS_FAILED.ToString() + " " + ex.Message;
                //Logger.RaiseTrace(ClassName, "Error=" + response.SiamResponseMessage, TraceLevel.Error);
            }
            TimeSpan end = TimeKeeper.GetCurrentTime();
            TimeSpan exectime = start.Subtract(end);
            //Logger.RaiseTrace(ClassName, "Exit:ExecutionTime=" + exectime.ToString(), TraceLevel.Verbose);
            return response;
        }


        /// <summary>
        /// Returns a listing of users known by SIAM. There are two basic containers used to represent
        /// a user in SIAM. The normal <see cref="User"/> and the <see cref="ShallowUser"/> objects.
        /// This method will return an array of <see cref="ShallowUser"/> objects and is ideal
        /// for UI screen population of data where a fully hydrated user object is not required. 
        /// </summary>
        /// <returns>An instance of <see cref="SiamResponse"/> that will contain the results
        /// of the call.</returns>
        public SiamResponse GetUsersShallow(string SearchPattern) {
            TimeSpan start = TimeKeeper.GetCurrentTime();
            //Logger.RaiseTrace(ClassName, "Entry:" + start.ToString(), TraceLevel.Verbose);
            string outMessage;
            SiamResponse response = new SiamResponse();
            try {
                response.SiamPayLoad = _dal.GetUsers(SearchPattern, false, out outMessage);
                if (outMessage != "") {
                    throw new Exception(outMessage);
                } else {
                    response.SiamResponseCode = (int)Messages.GET_USERS_COMPLETED;
                    response.SiamResponseMessage = Messages.GET_USERS_COMPLETED.ToString();
                }
            } catch (Exception ex) {
                response.SiamPayLoad = null;
                response.SiamResponseCode = (int)Messages.GET_USERS_FAILED;
                response.SiamResponseMessage = Messages.GET_USERS_FAILED.ToString() + " " + ex.Message;
                //Logger.RaiseTrace(ClassName, "Error=" + response.SiamResponseMessage, TraceLevel.Error);
            }
            TimeSpan end = TimeKeeper.GetCurrentTime();
            TimeSpan exectime = start.Subtract(end);
            //Logger.RaiseTrace(ClassName, "Exit:ExecutionTime=" + exectime.ToString(), TraceLevel.Verbose);
            return response;
        }

        /// <summary>
        /// Returns a listing of users known by SIAM. There are two basic containers used to represent
        /// a user in SIAM. The normal <see cref="User"/> and the <see cref="ShallowUser"/> objects.
        /// This method will return an array of <see cref="ShallowUser"/> objects and is ideal
        /// for UI screen population of data where a fully hydrated user object is not required. 
        /// </summary>
        /// <returns>An instance of <see cref="SiamResponse"/> that will contain the results
        /// of the call.</returns>
        public SiamResponse GetUsersShallow(string SearchPattern, bool SPSSearch) {
            TimeSpan start = TimeKeeper.GetCurrentTime();
            //Logger.RaiseTrace(ClassName, "Entry:" + start.ToString(), TraceLevel.Verbose);
            string outMessage;
            SiamResponse response = new SiamResponse();
            try {
                response.SiamPayLoad = _dal.GetUsers(SearchPattern, false, SPSSearch, out outMessage);
                if (outMessage != "") {
                    throw new Exception(outMessage);
                } else {
                    response.SiamResponseCode = (int)Messages.GET_USERS_COMPLETED;
                    response.SiamResponseMessage = Messages.GET_USERS_COMPLETED.ToString();
                }
            } catch (Exception ex) {
                response.SiamPayLoad = null;
                response.SiamResponseCode = (int)Messages.GET_USERS_FAILED;
                response.SiamResponseMessage = Messages.GET_USERS_FAILED.ToString() + " " + ex.Message;
                //Logger.RaiseTrace(ClassName, "Error=" + response.SiamResponseMessage, TraceLevel.Error);
            }
            TimeSpan end = TimeKeeper.GetCurrentTime();
            TimeSpan exectime = start.Subtract(end);
            //Logger.RaiseTrace(ClassName, "Exit:ExecutionTime=" + exectime.ToString(), TraceLevel.Verbose);
            return response;
        }


        /// <summary>
        /// Obtains an array of <see cref="User"/> objects that are associated with the given
        /// Role.
        /// </summary>
        /// <param name="RoleId">The id of the Role</param>
        /// <returns>An instance of <see cref="SiamResponse"/> that will contain the results
        /// of the call.</returns>
        /// Commented By Rajkumar A on 07-01-2010. Stored Procedure is not used in the application
        //public SiamResponse GetUsers(int RoleId)
        //{
        //    TimeSpan start = TimeKeeper.GetCurrentTime();
        //    //Logger.RaiseTrace(ClassName, "Entry:" + start.ToString(), TraceLevel.Verbose);
        //    string outMessage;
        //    SiamResponse response = new SiamResponse();
        //    try
        //    {
        //        response.SiamPayLoad = _dal.GetUsers(RoleId, out outMessage) as User[];
        //        if (outMessage != "")
        //        {
        //            throw new System.Exception(outMessage);
        //        }
        //        else
        //        {
        //            response.SiamResponseCode = (int)Messages.GET_USERS_COMPLETED;
        //            response.SiamResponseMessage = Messages.GET_USERS_COMPLETED.ToString();
        //        }
        //    }
        //    catch (System.Exception ex)
        //    {
        //        response.SiamPayLoad = null;
        //        response.SiamResponseCode = (int)Messages.GET_USERS_FAILED;
        //        response.SiamResponseMessage = Messages.GET_USERS_FAILED.ToString() + " " + ex.Message;
        //        //Logger.RaiseTrace(ClassName, "Error=" + response.SiamResponseMessage, TraceLevel.Error);
        //    }
        //    TimeSpan end = TimeKeeper.GetCurrentTime();
        //    TimeSpan exectime = start.Subtract(end);
        //    //Logger.RaiseTrace(ClassName, "Exit:ExecutionTime=" + exectime.ToString(), TraceLevel.Verbose);
        //    return response;
        //}


        /// <summary>
        /// Obtains an array of <see cref="User"/> objects that are invalid.
        /// </summary>
        /// <returns>An instance of <see cref="SiamResponse"/> that will contain the results
        /// of the call.</returns>
        /// Commented by Prasad because this method is not used any where in the application
        //public SiamResponse GetInvalidUsers()
        //{
        //    TimeSpan start = TimeKeeper.GetCurrentTime();
        //    //Logger.RaiseTrace(ClassName, "Entry:" + start.ToString(), TraceLevel.Verbose);
        //    string outMessage;
        //    SiamResponse response = new SiamResponse();
        //    try
        //    {
        //        response.SiamPayLoad = _dal.GetInvalidUsers(out outMessage) as object;
        //        if (outMessage != "")
        //        {
        //            throw new System.Exception(outMessage);
        //        }
        //        else
        //        {
        //            response.SiamResponseCode = (int)Messages.GET_USERS_COMPLETED;
        //            response.SiamResponseMessage = Messages.GET_USERS_COMPLETED.ToString();
        //        }
        //    }
        //    catch (System.Exception ex)
        //    {
        //        response.SiamPayLoad = null;
        //        response.SiamResponseCode = (int)Messages.GET_USERS_FAILED;
        //        response.SiamResponseMessage = Messages.GET_USERS_FAILED.ToString() + " " + ex.Message;
        //        //Logger.RaiseTrace(ClassName, "Error=" + response.SiamResponseMessage, TraceLevel.Error);
        //    }
        //    TimeSpan end = TimeKeeper.GetCurrentTime();
        //    TimeSpan exectime = start.Subtract(end);
        //    //Logger.RaiseTrace(ClassName, "Exit:ExecutionTime=" + exectime.ToString(), TraceLevel.Verbose);
        //    return response;
        //}


        /// <summary>
        /// Obtains an instance of <see cref="Application"/> for the given application id.
        /// </summary>
        /// <param name="ApplicationId">An integer value containing the id of the appliction.</param>
        /// <returns>An instance of <see cref="SiamResponse"/> that will contain the results
        /// of the call.</returns>
        public SiamResponse GetApplication(int ApplicationId) {
            TimeSpan start = TimeKeeper.GetCurrentTime();
            //Logger.RaiseTrace(ClassName, "Entry:" + start.ToString(), TraceLevel.Verbose);

            SiamResponse r = new SiamResponse();
            try {
                r.SiamPayLoad = _dal.GetApplication(ApplicationId, out r.SiamResponseMessage);
                if (r.SiamResponseMessage != "") {
                    throw new Exception(r.SiamResponseMessage);
                } else {
                    r.SiamResponseCode = (int)Messages.GET_APPLICATION_COMPLETED;
                    r.SiamResponseMessage = Messages.GET_APPLICATION_COMPLETED.ToString();
                }
            } catch (Exception ex) {
                r.SiamPayLoad = null;
                r.SiamResponseCode = (int)Messages.GET_APPLICATION_FAILED;
                r.SiamResponseMessage = Messages.GET_APPLICATION_FAILED.ToString() + " " + ex.Message;
                //Logger.RaiseTrace(ClassName, "Error=" + r.SiamResponseMessage, TraceLevel.Error);
            }
            TimeSpan end = TimeKeeper.GetCurrentTime();
            TimeSpan exectime = start.Subtract(end);
            //Logger.RaiseTrace(ClassName, "Exit:ExecutionTime=" + exectime.ToString(), TraceLevel.Verbose);
            return r;
        }


        /// <summary>
        /// Obtains an array of <see cref="Application"/> objects containing all applications
        /// known to SIAM.
        /// </summary>
        /// <returns>An instance of <see cref="SiamResponse"/> that will contain the results
        /// of the call.</returns>
        public SiamResponse GetApplications() {
            TimeSpan start = TimeKeeper.GetCurrentTime();
            //Logger.RaiseTrace(ClassName, "Entry:" + start.ToString(), TraceLevel.Verbose);
            SiamResponse r = new SiamResponse();
            try {
                r.SiamPayLoad = _dal.GetApplications(out r.SiamResponseMessage);
                if (r.SiamResponseMessage != "") {
                    throw new Exception(r.SiamResponseMessage);
                } else {
                    r.SiamResponseCode = (int)Messages.GET_APPLICATIONS_COMPLETED;
                    r.SiamResponseMessage = Messages.GET_APPLICATIONS_COMPLETED.ToString();
                }
            } catch (Exception ex) {
                r.SiamPayLoad = null;
                r.SiamResponseCode = (int)Messages.GET_APPLICATIONS_FAILED;
                r.SiamResponseMessage = Messages.GET_APPLICATIONS_FAILED.ToString() + " " + ex.Message;
                //Logger.RaiseTrace(ClassName, "Error=" + r.SiamResponseMessage, TraceLevel.Error);
            }
            TimeSpan end = TimeKeeper.GetCurrentTime();
            TimeSpan exectime = start.Subtract(end);
            //Logger.RaiseTrace(ClassName, "Exit:ExecutionTime=" + exectime.ToString(), TraceLevel.Verbose);
            return r;
        }


        /// <summary>
        /// Obtains an array of <see cref="Resource"/> objects in SIAM for a given application.
        /// </summary>
        /// <param name="ApplicationId">An integer value containing the id of the application</param>
        /// <returns>An instance of <see cref="SiamResponse"/> that will contain the results
        /// of the call.</returns>
        public SiamResponse GetApplicationResources(int ApplicationId) {
            TimeSpan start = TimeKeeper.GetCurrentTime();
            //Logger.RaiseTrace(ClassName, "Entry:" + start.ToString(), TraceLevel.Verbose);
            SiamResponse r = new SiamResponse();
            try {
                r.SiamPayLoad = _dal.GetApplicationResources(ApplicationId, out r.SiamResponseMessage);
                if (r.SiamResponseMessage != "") {
                    throw new Exception(r.SiamResponseMessage);
                } else {
                    r.SiamResponseCode = (int)Messages.GET_APPLICATIONRESOURCES_COMPLETED;
                    r.SiamResponseMessage = Messages.GET_APPLICATIONRESOURCES_COMPLETED.ToString();
                }
            } catch (Exception ex) {
                r.SiamPayLoad = null;
                r.SiamResponseCode = (int)Messages.GET_APPLICATIONRESOURCES_FAILED;
                r.SiamResponseMessage = Messages.GET_APPLICATIONRESOURCES_FAILED.ToString() + " " + ex.Message;
                //Logger.RaiseTrace(ClassName, "Error=" + r.SiamResponseMessage, TraceLevel.Error);
            }
            TimeSpan end = TimeKeeper.GetCurrentTime();
            TimeSpan exectime = start.Subtract(end);
            //Logger.RaiseTrace(ClassName, "Exit:ExecutionTime=" + exectime.ToString(), TraceLevel.Verbose);
            return r;
        }


        /// <summary>
        /// Obtains an array of <see cref="Action"/> objects.
        /// </summary>
        /// <returns>An instance of <see cref="SiamResponse"/> that will contain the results
        /// of the call.</returns>
        /// //6732 starts
        /*public SiamResponse GetActions()
        {
            TimeSpan start = TimeKeeper.GetCurrentTime();
            //Logger.RaiseTrace(ClassName, "Entry:" + start.ToString(), TraceLevel.Verbose);
            siam.SiamResponse r = new SiamResponse();
            try
            {
                r.SiamPayLoad = _dal.GetActions(out r.SiamResponseMessage);
                if (r.SiamResponseMessage != "")
                {
                    throw new System.Exception(r.SiamResponseMessage);
                }
                else
                {
                    r.SiamResponseCode = (int)Messages.GET_ACTIONS_COMPLETED;
                    r.SiamResponseMessage = Messages.GET_ACTIONS_COMPLETED.ToString();
                }
            }
            catch (System.Exception ex)
            {
                r.SiamPayLoad = null;
                r.SiamResponseCode = (int)Messages.GET_ACTIONS_FAILED;
                r.SiamResponseMessage = Messages.GET_ACTIONS_FAILED.ToString() + " " + ex.Message;
                //Logger.RaiseTrace(ClassName, "Error=" + r.SiamResponseMessage, TraceLevel.Error);
            }
            TimeSpan end = TimeKeeper.GetCurrentTime();
            TimeSpan exectime = start.Subtract(end);
            //Logger.RaiseTrace(ClassName, "Exit:ExecutionTime=" + exectime.ToString(), TraceLevel.Verbose);
            return r;
        }*/
        //6732 ends

        /// <summary>
        /// Obtains an array of <see cref="Functionality"/> objects known to SIAM.
        /// </summary>
        /// <returns>An instance of <see cref="SiamResponse"/> that will contain the results
        /// of the call.</returns>
        public SiamResponse GetFunctionalities() {
            TimeSpan start = TimeKeeper.GetCurrentTime();
            //Logger.RaiseTrace(ClassName, "Entry:" + start.ToString(), TraceLevel.Verbose);
            siam.SiamResponse r = new SiamResponse();
            try {
                r.SiamPayLoad = _dal.GetFunctionalities(out r.SiamResponseMessage);
                if (r.SiamResponseMessage != "") {
                    throw new Exception(r.SiamResponseMessage);
                } else {
                    r.SiamResponseCode = (int)Messages.GET_FUNCTIONALITIES_COMPLETED;
                    r.SiamResponseMessage = Messages.GET_FUNCTIONALITIES_COMPLETED.ToString();
                }
            } catch (Exception ex) {
                r.SiamPayLoad = null;
                r.SiamResponseCode = (int)Messages.GET_FUNCTIONALITIES_FAILED;
                r.SiamResponseMessage = Messages.GET_FUNCTIONALITIES_FAILED.ToString() + " " + ex.Message;
                //Logger.RaiseTrace(ClassName, "Error=" + r.SiamResponseMessage, TraceLevel.Error);
            }
            TimeSpan end = TimeKeeper.GetCurrentTime();
            TimeSpan exectime = start.Subtract(end);
            //Logger.RaiseTrace(ClassName, "Exit:ExecutionTime=" + exectime.ToString(), TraceLevel.Verbose);
            return r;
        }
        /// <summary>
        /// Obtains an array of <see cref="Functionality"/> objects known to SIAM.
        /// </summary>
        /// <param name="RoleID"></param>
        /// <returns>An instance of <see cref="SiamResponse"/> that will contain the results
        /// of the call.</returns>
        public SiamResponse GetFunctionalities(Int32 RoleID) {
            TimeSpan start = TimeKeeper.GetCurrentTime();
            //Logger.RaiseTrace(ClassName, "Entry:" + start.ToString(), TraceLevel.Verbose);
            siam.SiamResponse r = new SiamResponse();
            try {
                r.SiamPayLoad = _dal.GetFunctionalities(RoleID, out r.SiamResponseMessage);
                if (r.SiamResponseMessage != "") {
                    throw new Exception(r.SiamResponseMessage);
                } else {
                    r.SiamResponseCode = (int)Messages.GET_FUNCTIONALITIES_COMPLETED;
                    r.SiamResponseMessage = Messages.GET_FUNCTIONALITIES_COMPLETED.ToString();
                }
            } catch (Exception ex) {
                r.SiamPayLoad = null;
                r.SiamResponseCode = (int)Messages.GET_FUNCTIONALITIES_FAILED;
                r.SiamResponseMessage = Messages.GET_FUNCTIONALITIES_FAILED.ToString() + " " + ex.Message;
                //Logger.RaiseTrace(ClassName, "Error=" + r.SiamResponseMessage, TraceLevel.Error);
            }
            TimeSpan end = TimeKeeper.GetCurrentTime();
            TimeSpan exectime = start.Subtract(end);
            //Logger.RaiseTrace(ClassName, "Exit:ExecutionTime=" + exectime.ToString(), TraceLevel.Verbose);
            return r;
        }

        /// <summary>
        /// Obtains an array of <see cref="Role"/> objects known to SIAM.
        /// </summary>
        /// <returns>An instance of <see cref="SiamResponse"/> that will contain the results
        /// of the call.</returns>
        public SiamResponse GetRoles() {
            TimeSpan start = TimeKeeper.GetCurrentTime();
            //Logger.RaiseTrace(ClassName, "Entry:" + start.ToString(), TraceLevel.Verbose);
            siam.SiamResponse r = new SiamResponse();
            try {
                r.SiamPayLoad = _dal.GetRoles(out r.SiamResponseMessage);
                if (r.SiamResponseMessage != "") {
                    throw new Exception(r.SiamResponseMessage);
                } else {
                    r.SiamResponseCode = (int)Messages.GET_ROLES_COMPLETED;
                    r.SiamResponseMessage = Messages.GET_ROLES_COMPLETED.ToString();
                }
            } catch (Exception ex) {
                r.SiamPayLoad = null;
                r.SiamResponseCode = (int)Messages.GET_ROLES_FAILED;
                r.SiamResponseMessage = Messages.GET_ROLES_FAILED.ToString() + " " + ex.Message;
                //Logger.RaiseTrace(ClassName, "Error=" + r.SiamResponseMessage, TraceLevel.Error);
            }
            TimeSpan end = TimeKeeper.GetCurrentTime();
            TimeSpan exectime = start.Subtract(end);
            //Logger.RaiseTrace(ClassName, "Exit:ExecutionTime=" + exectime.ToString(), TraceLevel.Verbose);
            return r;
        }


        /// <summary>
        /// Obtains an array of <see cref="Role"/> objects for a given user in SIAM.
        /// </summary>
        /// <param name="UserId">A string value containing the user's SIAM identity.</param>
        /// <returns>An instance of <see cref="SiamResponse"/> that will contain the results
        /// of the call.</returns>
        public SiamResponse GetRoles(string UserId) {
            TimeSpan start = TimeKeeper.GetCurrentTime();
            //Logger.RaiseTrace(ClassName, "Entry:" + start.ToString(), TraceLevel.Verbose);
            SiamResponse r = new SiamResponse();
            try {
                r.SiamPayLoad = _dal.GetRoles(UserId, out r.SiamResponseMessage);
                if (r.SiamResponseMessage != "") {
                    throw new Exception(r.SiamResponseMessage);
                } else {
                    r.SiamResponseCode = (int)Messages.GET_ROLES_COMPLETED;
                    r.SiamResponseMessage = Messages.GET_ROLES_COMPLETED.ToString();
                }
            } catch (Exception ex) {
                r.SiamPayLoad = null;
                r.SiamResponseCode = (int)Messages.GET_ROLES_FAILED;
                r.SiamResponseMessage = Messages.GET_ROLES_FAILED.ToString() + " " + ex.Message;
                //Logger.RaiseTrace(ClassName, "Error=" + r.SiamResponseMessage, TraceLevel.Error);
            }
            TimeSpan end = TimeKeeper.GetCurrentTime();
            TimeSpan exectime = start.Subtract(end);
            //Logger.RaiseTrace(ClassName, "Exit:ExecutionTime=" + exectime.ToString(), TraceLevel.Verbose);
            return r;
        }

        public string AddUser(User user, out string ErrorString) {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            string admin_id = ConfigurationData.Environment.LdapWebService.Adminid;  // "uid=iam-admin,ou=systems,l=america,dc=sony,dc=com";
            string LdapID = string.Empty;
            var pciLog = new PCILogger();

            var profileTab = new CreateSonyB2BUserSonyB2BUserProfileTab {
                _BAR_LAOrgID_BAR_ = user.CompanyName.Replace(",", ""),
                _BAR_LASonyLoginID_BAR_ = user.EmailAddress,
                _PCT_FIRST_NAME_PCT_ = user.FirstName,
                _PCT_LAST_NAME_PCT_ = user.LastName,
                _PCT_FULL_NAME_PCT_ = $"{user.FirstName} {user.LastName}",
                _PCT_EMAIL_PCT_ = user.EmailAddress,
                _PCT_PASSWORD_PCT_ = user.Password,
                _BAR_passwordConfirm_BAR_ = user.Password,
                _BAR_forcePasswordReset_BAR_ = "false",
                countryName = user.Country,
                st = user.State,
                l = user.City,
                postalcode = user.Zip,
                telephonenumber = user.Phone,
                postaladdress = $"{user.AddressLine1} {user.AddressLine2}"
            };

            var userSearch = new CreateSonyB2BUserSearch {
                CreateNew = true,
                CreateNewSpecified = true
            };

            ErrorString = string.Empty;
            try {
                try {
                    var binding = new BasicHttpBinding("Tews6SoapBinding");
                    var address = new EndpointAddress(ConfigurationData.Environment.LdapWebService.Url);
                    string userName = ConfigurationData.Environment.LdapWebService.UserName;
                    string password = SIAMUtilities.Encryption.DeCrypt(ConfigurationData.Environment.LdapWebService.Password, SIAMUtilities.Encryption.PWDKey);

                    pciLog.EventOriginApplication = "ServicesPLUS";
                    pciLog.EventOriginApplicationLocation = this.GetType().ToString();
                    pciLog.EventOriginMethod = "AddUser(User user, out string ErrorString)";
                    pciLog.EventOriginServerMachineName = HttpContext.Current.Server.MachineName.ToString();
                    pciLog.OperationalUser = Environment.UserName.ToString();
                    pciLog.CustomerID = user.CustomerID.ToString();
                    pciLog.CustomerID_SequenceNumber = user.SequenceNumber;
                    pciLog.EmailAddress = user.EmailAddress.ToString();
                    pciLog.EventType = EventType.Add;
                    pciLog.EventDateTime = DateTime.Now.ToLongTimeString();
                    pciLog.HTTPRequestObjectValues = pciLog.GetRequestContextValue();
                    pciLog.IndicationSuccessFailure = "Success";
                    pciLog.Message = "User Added";

                    var createUser = new CreateSonyB2BUser() {
                        CreateSonyB2BUserSearch = userSearch,
                        CreateSonyB2BUserSonyB2BUserProfileTab = profileTab
                    };

                    TaskContext tc = new TaskContext {
                        admin_id = admin_id
                    };

                    var soapClient = new Tews6PortTypeClient(binding, address);
                    soapClient.ClientCredentials.UserName.UserName = userName;
                    soapClient.ClientCredentials.UserName.Password = password;

                    // http://tews6/wsdl
                    MessageHeader header = MessageHeader.CreateHeader("My-CustomHeader", "http://myurl", "Custom Header.");

                    using (OperationContextScope scope = new OperationContextScope(soapClient.InnerChannel)) {
                        OperationContext.Current.OutgoingMessageHeaders.Add(header);
                        var httpRequestProperty = new HttpRequestMessageProperty();
                        httpRequestProperty.Headers.Add("Authorization", "Basic U2VydmljZXNQbHVzLVdlYlNydmM6NzZMUnEyaExZSDM=");
                        //httpRequestProperty.Headers.Add("COOKIE", "SMCHALLENGE=YES");
                        //httpRequestProperty.Headers.Add(HttpRequestHeader.UserAgent, "my user agent");
                        OperationContext.Current.OutgoingMessageProperties[HttpRequestMessageProperty.Name] = httpRequestProperty;

                        ImsStatus taskStatus = soapClient.CreateSonyB2BUser(tc, createUser);
                        if (!string.IsNullOrEmpty(taskStatus.transactionId)) {
                            var searchFilter = new SEL_ViewUserSearchFilter {
                                index = "0",
                                Field = "%EMAIL%",
                                //sf.Field = "%USER_ID%";
                                Op = OperatorType.CONTAINS,
                                Value = user.EmailAddress
                            };

                            SEL_ViewUserSearchFilter[] searchFilterArray = new SEL_ViewUserSearchFilter[1];
                            searchFilterArray[0] = searchFilter;
                            var viewUserSearch = new SEL_ViewUserSearch {
                                Items = searchFilterArray
                            };

                            SEL_ViewUserQuery viewUserQuery = new SEL_ViewUserQuery {
                                SEL_ViewUserSearch = viewUserSearch
                            };

                            SEL_ViewUserQueryResult userQueryResult = soapClient.SEL_ViewUserQuery(tc, viewUserQuery);

                            SEL_ViewUserProfileTab NewProfile = userQueryResult.SEL_ViewUserProfileTab;
                            if (profileTab != null) {
                                LdapID = NewProfile._PCT_USER_ID_PCT_;
                                pciLog.CustomerID = LdapID;
                            }
                        }
                    }
                } catch (FaultException spEX) {
                    ErrorString = spEX.Message; // reached the ldap server
                    throw;
                }
            } catch (Exception ex) {
                ErrorString = ex.Message;
                pciLog.IndicationSuccessFailure = "Failure";//6524
                pciLog.Message = "Unable to Add User. " + ex.Message.ToString();//6524
                throw;
            } finally {
                pciLog.PushLogToMSMQ();//6524
            }
            return LdapID;
        }

        public string AddSoftwarePlusUser(Users user, out string ErrorString) {
            System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            //Need to load from Configuration
            String admin_id = ConfigurationData.Environment.LdapWebService.Adminid;  // "uid=iam-admin,ou=systems,l=america,dc=sony,dc=com";
            string LdapID = string.Empty;
            ErrorString = string.Empty;
            CreateSonyB2BUserSearch search = new CreateSonyB2BUserSearch {
                CreateNew = true,
                CreateNewSpecified = true
            };
            var pciLog = new PCILogger();

            var pt = new CreateSonyB2BUserSonyB2BUserProfileTab {
                _BAR_LASonyLoginID_BAR_ = user.Email,
                _PCT_FIRST_NAME_PCT_ = user.FirstName,
                _PCT_LAST_NAME_PCT_ = user.LastName,
                _PCT_FULL_NAME_PCT_ = user.FirstName + " " + user.LastName,
                _PCT_EMAIL_PCT_ = user.Email,
                _PCT_PASSWORD_PCT_ = user.Password,
                _BAR_passwordConfirm_BAR_ = user.Password,
                _BAR_forcePasswordReset_BAR_ = "false"
            };
            // pt._BAR_LAOrgID_BAR_ = user.CompanyName;
            //pt.st = user.State;
            //pt.l = user.City;
            //pt.postaladdress = user.Zip;
            //pt.telephonenumber = user.Phone;
            //pt.postaladdress = user.AddressLine1 + " " + user.AddressLine2;

            try {
                try {
                    BasicHttpBinding binding = new BasicHttpBinding("Tews6SoapBinding");
                    EndpointAddress address = new EndpointAddress(ConfigurationData.Environment.LdapWebService.Url);

                    CreateSonyB2BUser sr = new CreateSonyB2BUser {
                        CreateSonyB2BUserSearch = search,
                        CreateSonyB2BUserSonyB2BUserProfileTab = pt
                    };

                    TaskContext tc = new TaskContext();
                    String strUname = ConfigurationData.Environment.LdapWebService.UserName;
                    String strPwd = SIAMUtilities.Encryption.DeCrypt(ConfigurationData.Environment.LdapWebService.Password, SIAMUtilities.Encryption.PWDKey);
                    tc.admin_id = admin_id;


                    //Tews6PortTypeClient sb = new Tews6PortTypeClient();
                    Tews6PortTypeClient sb = new Tews6PortTypeClient(binding, address);
                    sb.ClientCredentials.UserName.UserName = strUname;
                    sb.ClientCredentials.UserName.Password = strPwd;

                    MessageHeader header = MessageHeader.CreateHeader("My-CustomHeader", "http://myurl", "Custom Header.");


                    using (OperationContextScope scope = new OperationContextScope(sb.InnerChannel)) {
                        OperationContext.Current.OutgoingMessageHeaders.Add(header);

                        HttpRequestMessageProperty httpRequestProperty = new HttpRequestMessageProperty();
                        httpRequestProperty.Headers.Add("Authorization", "Basic U2VydmljZXNQbHVzLVdlYlNydmM6NzZMUnEyaExZSDM=");
                        httpRequestProperty.Headers.Add("COOKIE", "SMCHALLENGE=YES");
                        //httpRequestProperty.Headers.Add(HttpRequestHeader.UserAgent, "my user agent");
                        OperationContext.Current.OutgoingMessageProperties[HttpRequestMessageProperty.Name] = httpRequestProperty;

                        ImsStatus ret = sb.CreateSonyB2BUser(tc, sr);
                        if (!string.IsNullOrEmpty(ret.transactionId)) {
                            SEL_ViewUserSearchFilter sf = new SEL_ViewUserSearchFilter {
                                index = "0",
                                Field = "%EMAIL%",
                                //sf.Field = "%USER_ID%";
                                Op = OperatorType.CONTAINS,
                                Value = user.Email
                            };


                            SEL_ViewUserSearchFilter[] sfArr = new SEL_ViewUserSearchFilter[1];
                            sfArr[0] = sf;
                            SEL_ViewUserSearch us = new SEL_ViewUserSearch {
                                Items = sfArr
                            };

                            SEL_ViewUserQuery vuq = new SEL_ViewUserQuery {
                                SEL_ViewUserSearch = us
                            };

                            SEL_ViewUserQueryResult res = sb.SEL_ViewUserQuery(tc, vuq);

                            SEL_ViewUserProfileTab NewProfile = res.SEL_ViewUserProfileTab;
                            if (pt != null) {
                                LdapID = NewProfile._PCT_USER_ID_PCT_;
                                pciLog.CustomerID = LdapID;
                            }
                        }
                    }
                } catch (SoapException spEX) {
                    ErrorString = spEX.Message; // reached the ldap server
                    throw;
                }

            } catch (Exception ex) {
                ErrorString = ex.Message;
                pciLog.IndicationSuccessFailure = "Failure";//6524
                pciLog.Message = "Unable to Add User. " + ex.Message.ToString();//6524
                throw;
            } finally {
                pciLog.PushLogToMSMQ();//6524
            }
            return LdapID;
        }

        /// <summary>
        /// Search the email Address
        /// </summary>
        public string SearchUser(string emailAddress) {
            try {
                try {
                    //added by satheesh-arshad for testing registration -Start 
                    System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                    //added by satheesh-arshad for testing registration -end ..
                    BasicHttpBinding binding = new BasicHttpBinding("Tews6SoapBinding");
                    EndpointAddress address = new EndpointAddress(ConfigurationData.Environment.LdapWebService.Url);
                    String admin_id = ConfigurationData.Environment.LdapWebService.Adminid;
                    String strUname = ConfigurationData.Environment.LdapWebService.UserName;
                    String strPwd = SIAMUtilities.Encryption.DeCrypt(ConfigurationData.Environment.LdapWebService.Password, SIAMUtilities.Encryption.PWDKey);
                    TaskContext obj = new TaskContext {
                        admin_id = admin_id
                    };
                    //Tews6PortTypeClient obj1 = new Tews6PortTypeClient();
                    Tews6PortTypeClient obj1 = new Tews6PortTypeClient(binding, address);
                    obj1.ClientCredentials.UserName.UserName = strUname;
                    obj1.ClientCredentials.UserName.Password = strPwd;
                    MessageHeader header = MessageHeader.CreateHeader("My-CustomHeader", "http://myurl", "Custom Header.");
                    using (OperationContextScope scope = new OperationContextScope(obj1.InnerChannel)) {
                        OperationContext.Current.OutgoingMessageHeaders.Add(header);
                        HttpRequestMessageProperty httpRequestProperty = new HttpRequestMessageProperty();
                        httpRequestProperty.Headers.Add("Authorization", "Basic U2VydmljZXNQbHVzLVdlYlNydmM6NzZMUnEyaExZSDM=");
                        httpRequestProperty.Headers.Add("COOKIE", "SMCHALLENGE=YES");
                        //httpRequestProperty.Headers.Add(HttpRequestHeader.UserAgent, "my user agent");
                        OperationContext.Current.OutgoingMessageProperties[HttpRequestMessageProperty.Name] = httpRequestProperty;

                        var sf = new SEL_ViewUserSearchFilter {
                            index = "0",
                            Field = "%EMAIL%",
                            Op = OperatorType.EQUALS, // Was: OperatorType.CONTAINS, but this made the email "regtest@test.co" pull up info for "caregtest@test.com"
                            Value = emailAddress.ToLower()  // 2018-09-10 Contains may have been used because it's not case sensitive. This should fix that, since we store emails all lowercase.
                        };
                        //sf.Value = "test@gmail.com";

                        SEL_ViewUserSearchFilter[] sfArr = new SEL_ViewUserSearchFilter[1];
                        SEL_ViewUserSearch us = new SEL_ViewUserSearch();
                        SEL_ViewUserQuery vuq = new SEL_ViewUserQuery();
                        sfArr[0] = sf;
                        us.Items = sfArr;
                        vuq.SEL_ViewUserSearch = us;
                        SEL_ViewUserQueryResult res = obj1.SEL_ViewUserQuery(obj, vuq);

                        if (res.SEL_ViewUserProfileTab == null)
                            return string.Empty;
                        else {
                            return $"{res.SEL_ViewUserProfileTab._PCT_FIRST_NAME_PCT_}*{res.SEL_ViewUserProfileTab._PCT_LAST_NAME_PCT_}*{res.SEL_ViewUserProfileTab._PCT_ORG_MEMBERSHIP_NAME_PCT_}*{res.SEL_ViewUserProfileTab._PCT_USER_ID_PCT_}";
                        }
                    }
                } catch (FaultException spEX) {
                    throw spEX;
                }
            } catch (Exception ex) {
                throw ex;
            }
        }

        // ASleight 2017-05-09 - Updated for the new IDP, which has lost the SEL_ModifyUser actions. Replaced with ModifyUser.
        public bool ModifyUserInfo(string ldapid, User user, out string errorstring) {
            try {
                System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                errorstring = "";
                TaskContext tc = new TaskContext();

                SEL_ModifyUser selModifyUserObj;
                SEL_ModifyUserSearch selModifyUserSearchObj;
                SEL_ModifyUserQuery selModifyUserQueryObj;
                SEL_ModifyUserProfileTab selModifyUserProfileTabObj;
                SEL_ModifyUserSearchFilter[] selModifyUserSearchFilterArr = new SEL_ModifyUserSearchFilter[5];
                BasicHttpBinding binding = new BasicHttpBinding("Tews6SoapBinding");
                EndpointAddress address = new EndpointAddress(ConfigurationData.Environment.LdapWebService.Url);


                String admin_id = ConfigurationData.Environment.LdapWebService.Adminid;
                String strUname = ConfigurationData.Environment.LdapWebService.UserName;
                String strPwd = SIAMUtilities.Encryption.DeCrypt(ConfigurationData.Environment.LdapWebService.Password, SIAMUtilities.Encryption.PWDKey);

                tc.admin_id = admin_id;

                var sb = new Tews6PortTypeClient(binding, address);
                sb.ClientCredentials.UserName.UserName = strUname;
                sb.ClientCredentials.UserName.Password = strPwd;

                selModifyUserSearchFilterArr[0] = new SEL_ModifyUserSearchFilter {
                    index = "0",
                    Field = "%USER_ID%",
                    Op = OperatorType.EQUALS,
                    Value = ldapid
                };

                selModifyUserSearchObj = new SEL_ModifyUserSearch {
                    Items = selModifyUserSearchFilterArr
                };
                selModifyUserQueryObj = new SEL_ModifyUserQuery {
                    SEL_ModifyUserSearch = selModifyUserSearchObj
                };

                MessageHeader header = MessageHeader.CreateHeader("My-CustomHeader", "http://myurl", "Custom Header.");

                using (var scope = new OperationContextScope(sb.InnerChannel)) {
                    OperationContext.Current.OutgoingMessageHeaders.Add(header);

                    var httpRequestProperty = new HttpRequestMessageProperty();
                    httpRequestProperty.Headers.Add("Authorization", "Basic U2VydmljZXNQbHVzLVdlYlNydmM6NzZMUnEyaExZSDM=");
                    httpRequestProperty.Headers.Add("COOKIE", "SMCHALLENGE=YES");
                    //httpRequestProperty.Headers.Add(HttpRequestHeader.UserAgent, "my user agent");
                    OperationContext.Current.OutgoingMessageProperties[HttpRequestMessageProperty.Name] = httpRequestProperty;

                    selModifyUserProfileTabObj = new SEL_ModifyUserProfileTab {
                        _BAR_LAEmail_BAR_ = user.EmailAddress,
                        _PCT_FIRST_NAME_PCT_ = user.FirstName,
                        _PCT_LAST_NAME_PCT_ = user.LastName,
                        _PCT_FULL_NAME_PCT_ = user.FirstName + " " + user.LastName,
                        postalcode = user.Zip,
                        postaladdress = user.AddressLine1 + " " + user.AddressLine2,
                        l = user.City,
                        st = user.State,
                        countryName = user.Country,
                        telephonenumber = user.Phone
                    };
                    //selModifyUserProfileTabObj._PCT_ORG_MEMBERSHIP_NAME_PCT_ = company;
                    //selModifyUserProfileTabObj.sonyloginid = mail;

                    selModifyUserObj = new SEL_ModifyUser {
                        SEL_ModifyUserSearch = selModifyUserSearchObj,
                        SEL_ModifyUserProfileTab = selModifyUserProfileTabObj
                    };
                    //  SEL_ModifyUserQuery modifyUserQuery = new SEL_ModifyUserQuery();

                    ImsStatus ret = sb.SEL_ModifyUser(tc, selModifyUserObj);
                }
            } catch (FaultException spException) {
                errorstring = spException.Message;
                throw;
            } catch (Exception ex) {
                errorstring = ex.Message;
                throw;
            }
            return true;
        }

        public bool ModifyCustomerInfo(Customer customer, out string errorstring) {
            try {
                System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                errorstring = string.Empty;

                TaskContext tc = new TaskContext();
                SEL_ModifyUser selModifyUserObj = null;
                SEL_ModifyUserSearch selModifyUserSearchObj = null;
                // SEL_ModifyUserSearchResult selModifyUserSearchResultsObj = null;
                SEL_ModifyUserQuery selModifyUserQueryObj = null;
                // SEL_ModifyUserQueryResult selModifyUserQueryResultObj = null;
                SEL_ModifyUserSearchFilter[] selModifyUserSearchFilterArr = new SEL_ModifyUserSearchFilter[5];
                SEL_ModifyUserProfileTab selModifyUserProfileTabObj;

                selModifyUserSearchObj = new SEL_ModifyUserSearch();
                selModifyUserQueryObj = new SEL_ModifyUserQuery();

                BasicHttpBinding binding = new BasicHttpBinding("Tews6SoapBinding");
                EndpointAddress address = new EndpointAddress(ConfigurationData.Environment.LdapWebService.Url);

                String admin_id = ConfigurationData.Environment.LdapWebService.Adminid;
                String strUname = ConfigurationData.Environment.LdapWebService.UserName;
                String strPwd = SIAMUtilities.Encryption.DeCrypt(ConfigurationData.Environment.LdapWebService.Password, SIAMUtilities.Encryption.PWDKey);
                selModifyUserSearchFilterArr[0] = new SEL_ModifyUserSearchFilter {
                    index = "0",
                    Field = "%USER_ID%",
                    Op = OperatorType.EQUALS,
                    Value = customer.LdapID
                };

                selModifyUserSearchObj.Items = selModifyUserSearchFilterArr;
                selModifyUserQueryObj.SEL_ModifyUserSearch = selModifyUserSearchObj;

                Tews6PortTypeClient sb = new Tews6PortTypeClient(binding, address);
                sb.ClientCredentials.UserName.UserName = strUname;
                sb.ClientCredentials.UserName.Password = strPwd;

                //Configuration value
                tc.admin_id = admin_id;
                //Need to Configure form configuration file.

                MessageHeader header = MessageHeader.CreateHeader("My-CustomHeader", "http://myurl", "Custom Header.");

                using (OperationContextScope scope = new OperationContextScope(sb.InnerChannel)) {
                    OperationContext.Current.OutgoingMessageHeaders.Add(header);

                    HttpRequestMessageProperty httpRequestProperty = new HttpRequestMessageProperty();
                    httpRequestProperty.Headers.Add("Authorization", "Basic U2VydmljZXNQbHVzLVdlYlNydmM6NzZMUnEyaExZSDM=");
                    httpRequestProperty.Headers.Add("COOKIE", "SMCHALLENGE=YES");
                    //httpRequestProperty.Headers.Add(HttpRequestHeader.UserAgent, "my user agent");
                    OperationContext.Current.OutgoingMessageProperties[HttpRequestMessageProperty.Name] = httpRequestProperty;

                    selModifyUserProfileTabObj = new SEL_ModifyUserProfileTab {
                        _BAR_LAEmail_BAR_ = customer.EmailAddress,
                        _PCT_FIRST_NAME_PCT_ = customer.FirstName,
                        _PCT_LAST_NAME_PCT_ = customer.LastName,
                        _PCT_FULL_NAME_PCT_ = customer.FirstName + customer.LastName,
                        postalcode = customer.Address.Zip,
                        postaladdress = customer.Address.Address1 + " " + customer.Address.Address1 + " " + customer.Address.Address3,
                        st = customer.Address.State,
                        countryName = customer.Address.Country,
                        telephonenumber = customer.PhoneNumber
                    };
                    //selModifyUserProfileTabObj.sonyloginid = mail;
                    //selModifyUserProfileTabObj._PCT_ORG_MEMBERSHIP_NAME_PCT_ = company;

                    selModifyUserObj = new SEL_ModifyUser {
                        SEL_ModifyUserSearch = selModifyUserSearchObj,
                        SEL_ModifyUserProfileTab = selModifyUserProfileTabObj
                    };
                    //  SEL_ModifyUserQuery modifyUserQuery = new SEL_ModifyUserQuery();
                    ImsStatus ret = sb.SEL_ModifyUser(tc, selModifyUserObj);
                }
            } catch (FaultException spException) {
                errorstring = spException.Message;
                throw;
            } catch (Exception ex) {
                errorstring = ex.Message;
                throw;
            }
            return true;
        }

        public string IDMinderNewUserTest(User user, string IDMurl, string IDMadminid, string IDMusername, string IDMpassword) {
            var pciLog = new PCILogger();
            System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            string admin_id = IDMadminid;
            string LdapID = string.Empty;
            string ErrorString = string.Empty;
            var search = new CreateSonyB2BUserSearch {
                CreateNew = true,
                CreateNewSpecified = true
            };

            var pt = new CreateSonyB2BUserSonyB2BUserProfileTab {
                _BAR_LAOrgID_BAR_ = user.CompanyName.Replace(",", ""),
                _BAR_LASonyLoginID_BAR_ = user.EmailAddress,
                _PCT_FIRST_NAME_PCT_ = user.FirstName,
                _PCT_LAST_NAME_PCT_ = user.LastName,
                _PCT_FULL_NAME_PCT_ = user.FirstName + " " + user.LastName,
                _PCT_EMAIL_PCT_ = user.EmailAddress,
                _PCT_PASSWORD_PCT_ = user.Password,
                _BAR_passwordConfirm_BAR_ = user.Password
            };

            try {
                try {
                    var binding = new BasicHttpBinding("Tews6SoapBinding");
                    var address = new EndpointAddress(ConfigurationData.Environment.LdapWebService.Url);

                    pciLog.EventOriginApplication = "ServicesPLUS";
                    pciLog.EventOriginApplicationLocation = this.GetType().ToString();
                    pciLog.EventOriginMethod = "AddUser(User user, out string ErrorString)";
                    pciLog.EventOriginServerMachineName = HttpContext.Current.Server.MachineName.ToString();
                    pciLog.OperationalUser = Environment.UserName.ToString();
                    pciLog.CustomerID = user.CustomerID.ToString();
                    pciLog.CustomerID_SequenceNumber = user.SequenceNumber;
                    pciLog.EmailAddress = user.EmailAddress.ToString();
                    pciLog.EventType = EventType.Add;
                    pciLog.EventDateTime = DateTime.Now.ToLongTimeString();
                    pciLog.HTTPRequestObjectValues = (HTTPRequestObject)pciLog.GetRequestContextValue();
                    pciLog.IndicationSuccessFailure = "Test Success";
                    pciLog.Message = "IDMinderNewUserTest";

                    var sr = new CreateSonyB2BUser {
                        CreateSonyB2BUserSearch = search,
                        CreateSonyB2BUserSonyB2BUserProfileTab = pt
                    };

                    string strUname = IDMusername;
                    string strPwd = IDMpassword;

                    var sb = new Tews6PortTypeClient(binding, address);
                    sb.ClientCredentials.UserName.UserName = strUname;
                    sb.ClientCredentials.UserName.Password = strPwd;

                    var tc = new TaskContext {
                        admin_id = admin_id
                    };

                    ImsStatus ret = sb.CreateSonyB2BUser(tc, sr);
                    if (!string.IsNullOrEmpty(ret.transactionId)) {
                        var sf = new SEL_ViewUserSearchFilter {
                            index = "0",
                            Field = "%EMAIL%",
                            //sf.Field = "%USER_ID%";
                            Op = OperatorType.CONTAINS,
                            Value = user.EmailAddress
                        };

                        SEL_ViewUserSearchFilter[] sfArr = new SEL_ViewUserSearchFilter[1];
                        sfArr[0] = sf;
                        SEL_ViewUserSearch us = new SEL_ViewUserSearch {
                            Items = sfArr
                        };

                        SEL_ViewUserQuery vuq = new SEL_ViewUserQuery {
                            SEL_ViewUserSearch = us
                        };

                        SEL_ViewUserQueryResult res = sb.SEL_ViewUserQuery(tc, vuq);

                        SEL_ViewUserProfileTab NewProfile = res.SEL_ViewUserProfileTab;
                        if (pt != null) {
                            LdapID = NewProfile._PCT_USER_ID_PCT_;
                            pciLog.CustomerID = LdapID;//6524
                        }
                    }
                } catch (FaultException spEX) {
                    ErrorString = spEX.Message; // reached the ldap server
                    LdapID = "Test Sucess";
                }
            } catch (Exception ex) {
                ErrorString = ex.Message;
                LdapID = "Test Failure";
                pciLog.Message = "Test Failure. " + ex.Message.ToString();//6524
            } finally {
                pciLog.PushLogToMSMQ();//6524
            }
            return LdapID;
        }

        /// <summary>
        /// Obtains an array of <see cref="Role"/> objects for a given user in SIAM.
        /// </summary>
        /// <param name="UserId">A string value containing the user's SIAM identity.</param>
        /// <returns>An instance of <see cref="SiamResponse"/> that will contain the results
        /// of the call.</returns>
        /// Commented as its not used in Presentation Layer
        //public SiamResponse GetCCModifierUser(string UserId)
        //{
        //    TimeSpan start = TimeKeeper.GetCurrentTime();
        //    //Logger.RaiseTrace(ClassName, "Entry:" + start.ToString(), TraceLevel.Verbose);
        //    SiamResponse r = new SiamResponse();
        //    try
        //    {
        //        r.SiamPayLoad = _dal.GetCCModifierUser(UserId, out r.SiamResponseMessage);
        //        if (r.SiamResponseMessage != "")
        //        {
        //            throw new System.Exception(r.SiamResponseMessage);
        //        }
        //        else
        //        {
        //            r.SiamResponseCode = (int)Messages.CCMODIFY_LOGIN_COMPLETED;
        //            r.SiamResponseMessage = Messages.CCMODIFY_LOGIN_COMPLETED.ToString();
        //        }
        //    }
        //    catch (System.Exception ex)
        //    {
        //        r.SiamPayLoad = null;
        //        r.SiamResponseCode = (int)Messages.CCMODIFY_LOGIN_FAILED;
        //        r.SiamResponseMessage = Messages.CCMODIFY_LOGIN_FAILED.ToString() + " " + ex.Message;
        //        //Logger.RaiseTrace(ClassName, "Error=" + r.SiamResponseMessage, TraceLevel.Error);
        //    }
        //    TimeSpan end = TimeKeeper.GetCurrentTime();
        //    TimeSpan exectime = start.Subtract(end);
        //    //Logger.RaiseTrace(ClassName, "Exit:ExecutionTime=" + exectime.ToString(), TraceLevel.Verbose);
        //    return r;f
        //}

        /// <summary>
        /// Obtains an array of <see cref="Role"/> objects that are child roles of the Role
        /// specified by RoleId.
        /// </summary>
        /// <param name="RoleId">An integer value containing the Id of the role for which the child roles
        /// are desired.</param>
        /// <returns>An instance of <see cref="SiamResponse"/> that will contain the results
        /// of the call.</returns>
        public SiamResponse GetChildRoles(int RoleId) {
            TimeSpan start = TimeKeeper.GetCurrentTime();
            //Logger.RaiseTrace(ClassName, "Entry:" + start.ToString(), TraceLevel.Verbose);
            SiamResponse r = new SiamResponse();
            try {
                r.SiamPayLoad = _dal.GetChildRoles(RoleId, out r.SiamResponseMessage);
                if (r.SiamResponseMessage != "") {
                    throw new Exception(r.SiamResponseMessage);
                } else {
                    r.SiamResponseCode = (int)Messages.GET_ROLES_COMPLETED;
                    r.SiamResponseMessage = Messages.GET_ROLES_COMPLETED.ToString();
                }
            } catch (Exception ex) {
                r.SiamPayLoad = null;
                r.SiamResponseCode = (int)Messages.GET_ROLES_FAILED;
                r.SiamResponseMessage = Messages.GET_ROLES_FAILED.ToString() + " " + ex.Message;
                //Logger.RaiseTrace(ClassName, "Error=" + r.SiamResponseMessage, TraceLevel.Error);
            }
            TimeSpan end = TimeKeeper.GetCurrentTime();
            TimeSpan exectime = start.Subtract(end);
            //Logger.RaiseTrace(ClassName, "Exit:ExecutionTime=" + exectime.ToString(), TraceLevel.Verbose);
            return r;
        }

        /// <summary>
        /// Returns an array of <see cref="AccessRight"/> objects that are associated with 
        /// the role id specified.
        /// </summary>
        /// <param name="RoleId">An integer value containing the Role Id.</param>
        /// <returns>An instance of <see cref="SiamResponse"/> that will contain the results
        /// of the call.</returns>
        public SiamResponse GetAccessRights(int RoleId) {
            TimeSpan start = TimeKeeper.GetCurrentTime();
            //Logger.RaiseTrace(ClassName, "Entry:" + start.ToString(), TraceLevel.Verbose);
            string outMessage;
            SiamResponse r = new SiamResponse();
            try {
                AccessRight[] rights = _dal.GetAccessRights(RoleId, out outMessage);
                if (outMessage != "") {
                    throw new Exception(outMessage);
                } else {
                    r.SiamPayLoad = rights;
                    r.SiamResponseCode = (int)Messages.GET_ACCESSRIGHTS_COMPLETED;
                    r.SiamResponseMessage = Messages.GET_ACCESSRIGHTS_COMPLETED.ToString();
                }
            } catch (Exception ex) {
                r.SiamPayLoad = null;
                r.SiamResponseCode = (int)Messages.GET_ACCESSRIGHTS_FAILED;
                r.SiamResponseMessage = Messages.GET_ACCESSRIGHTS_FAILED.ToString() + " " + ex.Message;
                //Logger.RaiseTrace(ClassName, "Error=" + r.SiamResponseMessage, TraceLevel.Error);
            }
            TimeSpan end = TimeKeeper.GetCurrentTime();
            TimeSpan exectime = start.Subtract(end);
            //Logger.RaiseTrace(ClassName, "Exit:ExecutionTime=" + exectime.ToString(), TraceLevel.Verbose);
            return r;
        }
        #endregion

        #region Maintenence Methods
        /// <summary>
        /// Validates that a user can be added to SIAM without violating unique constraints. This
        /// type of method would not normally be present but because the way the registration was
        /// designed in Services plus. The method does not actually add the user but only checks the
        /// database to see if the user can be added based on current information.
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public bool IsDuplicateUser(Sony.US.siam.User user) {
            Sony.US.siam.Administration.OracleAdministrator dal = new DelegateHandler().GetCurrentDal() as Sony.US.siam.Administration.OracleAdministrator;
            //return dal.IsDuplicateUser(user.UserId, user.EmailAddress, user.FirstName, user.LastName);
            return dal.IsDuplicateUser(user.EmailAddress, user.FirstName, user.LastName, user.UserId);
        }

        /// <summary>
        /// Validates that a user can be added to SIAM without violating unique constraints. This
        /// type of method would not normally be present but because the way the registration was
        /// designed in Services plus. The method does not actually add the user but only checks the
        /// database to see if the user can be added based on current information.
        /// </summary>
        public bool IsDuplicateUser(string EmailAddress, string FirstName, string LastName) {
            Sony.US.siam.Administration.OracleAdministrator dal = new DelegateHandler().GetCurrentDal() as Sony.US.siam.Administration.OracleAdministrator;
            //return dal.IsDuplicateUser(UserName, EmailAddress, FirstName, LastName);
            return dal.IsDuplicateUser(EmailAddress, FirstName, LastName, null);
        }

        [Obsolete("Orphaned method. No code within.", false)]
        public void SendForgottenPassword(Sony.US.siam.User theUser) {

        }

        private string GetOneTimeUsePassword() {
            return "test";
        }

        ///// <summary>
        ///// Updates a SIAM user's credentials. The method takes as input an instance of <see cref="Credential"/> where
        ///// the caller may set new values for User Name and Password. An integer value for the number of days
        ///// until this password expires must also be supplied.
        ///// </summary>
        ///// <param name="user">An instance of <see cref="User"/></param>
        ///// <param name="NewCredentials">An instance of <see cref="Credential"/></param>
        ///// <param name="ExpiresInDays">An integer value containing the number of days until the password expires.</param>
        ///// <returns>An instance of <see cref="SiamResponse"/> that will contain the results
        ///// of the call.</returns>
        [Obsolete("Method contains no code.", false)]
        public SiamResponse UpdateCredentials(User user, Credential NewCredentials, int ExpiresInDays) {
            //    TimeSpan start = TimeKeeper.GetCurrentTime();
            //    //Logger.RaiseTrace(ClassName, "Entry:" + start.ToString(), TraceLevel.Verbose);
            //    SiamResponse r = new SiamResponse();
            //    string ErrorString;



            //    try
            //    {
            //        if (!IsValidPassword(NewCredentials.Password,user.UserType))
            //        {
            //            throw new System.Exception("Invalid Password");
            //        }

            //        if (!IsValidUserId(NewCredentials.UserName))
            //        {
            //            throw new System.Exception("Invalid UserId");
            //        }
            //        //<Satyam> Reset the password expiration days to a configured value
            //        //ConfigHandler handler = new ConfigHandler(@"SOFTWARE\SERVICESPLUS");
            //        //Hashtable config_settings;
            //        int PwdExpiryDays;
            //        //config_settings = handler.GetConfigSettings(@"//item[@name='SecurityManagement']");
            //        //PwdExpiryDays = int.Parse("0" + config_settings["PwdChangePeriodDays"].ToString());
            //        PwdExpiryDays = int.Parse("0" + ConfigurationData.GeneralSettings.SecurityManagement.PwdChangePeriodDays);
            //        ExpiresInDays = PwdExpiryDays;
            //        //</Satyam>

            //        // Execute the Delegate's First
            //        //Added Alternate ID check to prevent forcing LDAP to change password for a Pendign User

            //        //Commneted temporerly by chandra for avoiding SIAM USER table update when LDAP Modify is not successful
            //        //if (_dal.UpdateCredentials(user, NewCredentials, ExpiresInDays, out ErrorString) )
            //        bool result = false;
            //        if (user.UserType == "AccountHolder" && user.AlternateUserId != string.Empty && user.AlternateUserId != null)
            //        {
            //            object[] _delegates = new DelegateHandler().GetAdministrationDelegates(user.UserType);
            //            if (_delegates != null)
            //            {
            //                for (int x = 0; x <= _delegates.GetUpperBound(0); x++)
            //                {
            //                    result = ((ISecurityAdministrator)_delegates[x]).UpdateCredentials(user, NewCredentials, ExpiresInDays, out ErrorString);
            //                    if (result == false)
            //                    {
            //                        throw new System.Exception(Messages.LDAP_RESET_PASSWORD_FAILURE.ToString() + " " + ErrorString);
            //                    }
            //                }
            //            }
            //        }
            //        else result = true;

            //        if (result == true)
            //        {
            //            if (_dal.UpdateCredentials(user, NewCredentials, ExpiresInDays, out ErrorString))
            //            {
            //                r.SiamResponseCode = (int)Messages.UPDATE_CREDENTIALS_COMPLETED;
            //                r.SiamResponseMessage = Messages.UPDATE_CREDENTIALS_COMPLETED.ToString();
            //            }
            //            else
            //            {
            //                throw new System.Exception(ErrorString);
            //            }

            //        }
            //        //if (true== true)
            //        //{
            //        //    if (user.AlternateUserId != string.Empty && user.AlternateUserId != null)
            //        //    {
            //        //        object[] _delegates = new DelegateHandler().GetAdministrationDelegates(user.UserType);
            //        //        if (_delegates != null)
            //        //        {
            //        //            for (int x = 0; x <= _delegates.GetUpperBound(0); x++)
            //        //            {
            //        //                result = ((ISecurityAdministrator)_delegates[x]).UpdateCredentials(user, NewCredentials, ExpiresInDays, out ErrorString);
            //        //                if (result == false)
            //        //                {
            //        //                    throw new System.Exception(Messages.LDAP_RESET_PASSWORD_FAILURE.ToString() + " " + ErrorString);
            //        //                }
            //        //                //Added by chandra for avoiding SIAM USER table update when LDAP Modify is not successful
            //        //                else
            //        //                {
            //        //                    if (_dal.UpdateCredentials(user, NewCredentials, ExpiresInDays, out ErrorString))
            //        //                    {
            //        //                        r.SiamResponseCode = (int)Messages.UPDATE_CREDENTIALS_COMPLETED;
            //        //                        r.SiamResponseMessage = Messages.UPDATE_CREDENTIALS_COMPLETED.ToString();
            //        //                    }
            //        //                    else 
            //        //                    {
            //        //                        throw new System.Exception(ErrorString);
            //        //                    }

            //        //                }
            //        //            }
            //        //        }
            //        //    }
            //        //    //r.SiamResponseCode = (int)Messages.UPDATE_CREDENTIALS_COMPLETED;
            //        //    //r.SiamResponseMessage = Messages.UPDATE_CREDENTIALS_COMPLETED.ToString();
            //        //}
            //        //else
            //        //{
            //        //    throw new System.Exception(ErrorString);
            //        //}
            //    }
            //    catch (System.Exception ex)
            //    {
            //        r.SiamResponseCode = (int)Messages.UPDATE_CREDENTIALS_FAILED;
            //        r.SiamResponseMessage = Messages.UPDATE_CREDENTIALS_FAILED.ToString() + " " + ex.Message;
            //        //Logger.RaiseTrace(ClassName, "Error=" + r.SiamResponseMessage, TraceLevel.Error);
            //    }
            //    TimeSpan end = TimeKeeper.GetCurrentTime();
            //    TimeSpan exectime = start.Subtract(end);
            //    //Logger.RaiseTrace(ClassName, "Exit:ExecutionTime=" + exectime.ToString(), TraceLevel.Verbose);
            //    return r;
            return null;
        }
        #endregion

        #region Helper Methods
        /// <summary>
        /// Does a RegEx match for the provided string, returning true if it is a valid email.
        /// </summary>
        public bool IsValidEmail(string EmailAddress) {
            string strRegEx = @"^(?!\.)(""([^""\r\\]|\\[""\r\\])*""|"
                    + @"([-a-z0-9!#$%&'*+/=?^_`{|}~]|(?<!\.)\.)*)(?<!\.)"
                    + @"@[a-z0-9][\w\.-]*[a-z0-9]\.[a-z][a-z\.]*[a-z]$";

            Regex objRegEx = new Regex(strRegEx, RegexOptions.IgnoreCase);

            return objRegEx.IsMatch(EmailAddress);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Password"></param>
        /// <returns></returns>
        public bool IsValidPassword(string Password, string userType) {
            int minLen, maxLen;
            minLen = int.Parse("0" + ConfigurationData.GeneralSettings.SecurityManagement.MinPwdLength);

            if (userType == "A" || userType == "P") {
                maxLen = int.Parse("0" + ConfigurationData.GeneralSettings.SecurityManagement.MaxPwdLengthAccount);
            } else {
                maxLen = int.Parse("0" + ConfigurationData.GeneralSettings.SecurityManagement.MaxPwdLengthNonAccount);
            }

            if (Password.Length < minLen || Password.Length > maxLen) {
                return false;
            }

            return Regex.IsMatch(Password, "^[a-zA-Z0-9]+$");
        }

        public bool ValidateB2Bpassword(string password, string email, ref string message) {
            bool isValid = true;
            int minLen, maxLen;

            minLen = int.Parse("0" + ConfigurationData.GeneralSettings.SecurityManagement.MinPwdLength);
            maxLen = int.Parse("0" + ConfigurationData.GeneralSettings.SecurityManagement.MaxPwdLengthAccount);

            if (password == email) {
                message = "Your email address and password cannot be the same. ";
                isValid = false;
            }
            if (password.Length < minLen || password.Length > maxLen) {
                message += $"Your password must be {minLen} to {maxLen} characters long. ";
                isValid = false;
            }
            if (!Regex.IsMatch(password, "[A-Z]")) {
                message += "Password should contain at least 1 uppercase letter. ";
                isValid = false;
            }
            if (!Regex.IsMatch(password, "[a-z]")) {
                message += "Password should contain at least 1 lower case letter. ";
                isValid = false;
            }
            if (!Regex.IsMatch(password, "[0-9]")) {
                message += "Password should contain at least 1 digit. ";
                isValid = false;
            }
            //if (!Regex.IsMatch(password, "[!#-$ %*=+:;,?~]"))
            //{
            //    message = " At least 1 special characters.";
            //    return false;
            //}
            return isValid;
        }

        /// <summary>
        /// Returns false if provided string is less than 5 characters.
        /// </summary>
        public bool IsValidUserId(string UserId) {
            return UserId.Length > 4;
        }

        [Obsolete("This is an orphaned method with no code.", false)]
        public int EvaluateThirdId(User user, string Answer) {
            //    string theValue = user.ThirdID.UserSecurityPINs[user.ThirdID.NextQuestionIndex].ToString();
            //    //string plainTextValue = Sony.US.SIAMUtilities.Encryption.DeCrypt(theValue, null, null);
            //    string plainTextValue = Sony.US.SIAMUtilities.Encryption.DeCrypt(theValue, "test key");

            //    // third user id should also have validation against multiple attempts of bad password
            //    Credential c = new Credential();
            //    c.UserName = user.UserId;
            //    c.Password = Answer;

            //    if (plainTextValue.ToString().ToUpper() == Answer.ToString().ToUpper())
            //    {
            //        return (int)Messages.AUTHENTICATE_SUCCESS;
            //    }
            //    else
            //    {
            //        if (new UtilityMethods().CheckPasswordAttempts(user, c))
            //        {
            //            LockAccount(user);
            //            return (int)Messages.AUTHENTICATE_FAILURE_ACCOUNTLOCKED;
            //        }
            //        else
            //        {
            //            return (int)Messages.AUTHENTICATE_FAILURE_BADPASSWORD;
            //        }
            //    }
            //    // end of modifications - Deepa V, March 5, 2007
            return 0;
        }

        public void ReTryEmailFailure() {
            new DelegateHandler().GetCurrentDal().ReTryEmailFailure();
        }

        /// <summary>
        /// Updates the next question index pointer in the SIAM User table. This value along with the
        /// SecurityQuestions configuration section is used to determine the next security question
        /// the user should be given. This is an internal method not available to consuming code.
        /// </summary>
        /// <param name="Identity">A string value containing the User's SIAM Identity.</param>
        /// <param name="NewValue">The new value of the index.</param>
        /// <returns>An instance of <see cref="SiamResponse"/> that will contain the results
        /// of the call.</returns>
        internal SiamResponse UpdateNextQuestionIndex(string Identity, int NewValue) {
            TimeSpan start = TimeKeeper.GetCurrentTime();
            //Logger.RaiseTrace(ClassName, "Entry:" + start.ToString(), TraceLevel.Verbose);
            SiamResponse r = new SiamResponse();
            string ErrorString;
            try {
                if (_dal.UpdateNextQuestionIndex(Identity, NewValue, out ErrorString)) {
                    r.SiamResponseCode = (int)Messages.UPDATE_NEXTQUESTIONINDEX_COMPLETE;
                    r.SiamResponseMessage = Messages.UPDATE_NEXTQUESTIONINDEX_COMPLETE.ToString();
                } else {
                    throw new Exception(ErrorString);
                }
            } catch (Exception ex) {
                r.SiamResponseCode = (int)Messages.LOCKACCOUNT_FAILED;
                r.SiamResponseMessage = Messages.LOCKACCOUNT_FAILED.ToString() + " " + ex.Message;
                //Logger.RaiseTrace(ClassName, "Error=" + r.SiamResponseMessage, TraceLevel.Error);
            }
            TimeSpan end = TimeKeeper.GetCurrentTime();
            TimeSpan exectime = start.Subtract(end);
            //Logger.RaiseTrace(ClassName, "Exit:ExecutionTime=" + exectime.ToString(), TraceLevel.Verbose);
            return r;
        }

        /// <summary>
        /// Adds a resource primitive to the SIAM Data Store. First in a two step process to add
        /// a resource to SIAM.
        /// </summary>
        /// <param name="ResourceName">A string value containing the resource name.</param>
        /// <returns>An instance of <see cref="SiamResponse"/> that will contain the results
        /// of the call.</returns>
        private double AddResource(string ResourceName) {
            TimeSpan start = TimeKeeper.GetCurrentTime();
            //Logger.RaiseTrace(ClassName, "Entry:" + start.ToString(), TraceLevel.Verbose);

            string outMessage = "";
            double x = _dal.AddResource(ResourceName, out outMessage);
            if (x == -1) {
                throw new Exception(outMessage);
            }
            TimeSpan end = TimeKeeper.GetCurrentTime();
            TimeSpan exectime = start.Subtract(end);
            //Logger.RaiseTrace(ClassName, "Exit:ExecutionTime=" + exectime.ToString(), TraceLevel.Verbose);
            return x;
        }
        #endregion
    }

    public class TrustAllCertificatePolicy : System.Net.ICertificatePolicy {
        #region ICertificatePolicy Members

        public bool CheckValidationResult(ServicePoint srvPoint, X509Certificate certificate, WebRequest request, int certificateProblem) {
            return true;
        }

        #endregion
    }
}
