using System;
using Sony.US.siam;
using Sony.US.SIAMUtilities;
using Sony.US.siam.Interfaces;
using Sony.US.siam.Administration;
using Sony.US.siam.Authentication;
using Sony.US.siam.Authorization;

namespace Sony.US.siam
{
	/// <summary>
	/// A serializable object used to define an AccessRight in SIAM.
	/// </summary>
	[Serializable]
	public class AccessRight
	{

		/// <summary>
		/// 
		/// </summary>
		public string Name
		{
			get
			{
				return Application.Name + "-" + Resource.Name + "-" + Action.Name;
			}
		}

		/// <summary>
		/// An instance of <see cref="Application"/> to which this access right is associated.
		/// </summary>
		public Application Application;
		/// <summary>
		/// An instance of <see cref="Resource"/> to which this access right is associated.
		/// </summary>
		public Resource Resource;
		/// <summary>
		/// An instance of <see cref="Action"/> to which this access right is associated.
		/// </summary>
		public Action Action;
	}
}
