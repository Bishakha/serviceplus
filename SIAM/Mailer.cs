using System;
using System.Net.Mail;
using Sony.US.SIAMUtilities;

namespace Sony.US.siam {

    public class Mailer {

        public string _emailServer = "";

        public Mailer() {
            //SIAMConfig config = CachingServices.Cache["SiamConfiguration"] as SIAMConfig;
            //if (config == null)
            //{
            //    config = new SIAMConfig();
            //    CachingServices.Cache["SiamConfiguration"] = config;
            //}
            _emailServer = ConfigurationData.Environment.SMTP.EmailServer;
        }

        public bool SendMail(string MessageBody, string To, string From, string Subject, bool isBodyHtml) {
            var msg = new MailMessage(From, To) {
                Body = MessageBody,
                IsBodyHtml = isBodyHtml,
                Subject = Subject
            };

            try {
                using (var mailClient = new SmtpClient()) {
                    mailClient.Host = _emailServer;
                    mailClient.Send(msg);
                }
            } catch (Exception ex) {
                // log the mail message for later sending
                new DelegateHandler().GetCurrentDal().LogEmailFailure(msg, ex.Message);
                return false;
            }
            return true;
        }

        public bool SendMail(string MessageBody, string To, string From, string Subject, bool isBodyHtml, string _emailServer_cs) {
            var msg = new MailMessage(From, To) {
                Body = MessageBody,
                IsBodyHtml = isBodyHtml,
                Subject = Subject
            };

            try {
                using (var mailClient = new SmtpClient()) {
                    mailClient.Host = _emailServer_cs;
                    mailClient.Send(msg);
                }
            } catch (Exception ex) {
                // log the mail message for later sending
                new DelegateHandler().GetCurrentDal().LogEmailFailure(msg, ex.Message);
                return false;
            }
            return true;
        }
    }
}
