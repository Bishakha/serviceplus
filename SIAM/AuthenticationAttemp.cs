using System;
using Sony.US.siam;
using Sony.US.SIAMUtilities;
using Sony.US.siam.Interfaces;
using Sony.US.siam.Administration;
using Sony.US.siam.Authentication;
using Sony.US.siam.Authorization;

namespace Sony.US.siam
{
	/// <summary>
	/// An internal structure used by SIAM during the Account Locking process. This structure gets cached
	/// for each bad password login attempt.
    /// 2018-05-16 - ASleight - This class is no longer used, and can be removed.
	/// </summary>
	internal class AuthentAttempt
	{
		/// <summary>
		/// The <see cref="System.DateTime"/> of this attempt.
		/// </summary>
		internal System.DateTime dateTime;
		/// <summary>
		/// A string value containing the user name
		/// </summary>
		internal string User;
		/// <summary>
		/// An integer value containing the current login attempt count.
		/// </summary>
		internal int count;
	}
}
