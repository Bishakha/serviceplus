using System;
using Sony.US.siam;
using Sony.US.SIAMUtilities;
using Sony.US.siam.Interfaces;
using Sony.US.siam.Administration;
using Sony.US.siam.Authentication;
using Sony.US.siam.Authorization;

namespace Sony.US.siam
{
	/// <summary>
	/// A serializable object used to define an Action in SIAM.
	/// </summary>
	[Serializable]
	public class Action:IComparable
	{
		private string _name;
		private string _id;


		/// <summary>
		/// IComparable: CompareTo implementation
		/// </summary>
		/// <param name="obj"></param>
		/// <returns></returns>
		public int CompareTo(object obj)
		{
			if (obj is Action)
			{
				Action theObj = (Action)obj;
				return this.Name.CompareTo(theObj.Name);
			}
			else
			{
				throw new ArgumentException("IComparable:Object is not of type sony.us.siam.User");
			}
		}

		/// <summary>
		/// A string value containing the name.
		/// </summary>
		public string Name
		{
			get
			{
				return _name;
			}
			set
			{
				_name = value;
			}
		}

		/// <summary>
		/// A string value containing the ID of the Action.
		/// </summary>
		public string Id
		{
			get
			{
				return _id;
			}
			set
			{
				_id = value;
			}
		}
	}
}
