using System;
using Sony.US.siam;
using Sony.US.SIAMUtilities;
using Sony.US.siam.Interfaces;
using Sony.US.siam.Administration;
using Sony.US.siam.Authentication;
using Sony.US.siam.Authorization;

namespace Sony.US.siam
{
	/// <summary>
	/// An enumeration structure containing all valid SIAM response codes and messages.
	/// </summary>
	public enum Messages
	{
		/// <summary>
		/// 
		/// </summary>
		ADD_USER_COMPLETE					= 100,

		/// <summary>
		/// 
		/// </summary>
		ADD_USER_FAILED						= 101,

		/// <summary>
		/// 
		/// </summary>
		ADD_ACTION_FAILED					= 102,

		/// <summary>
		/// 
		/// </summary>
		ADD_ACTION_COMPLETED				= 103,

		/// <summary>
		/// 
		/// </summary>
		ADD_APPLICATION_FAILED				= 104,

		/// <summary>
		/// 
		/// </summary>
		ADD_APPLICATION_COMPLETED			= 105,

		/// <summary>
		/// 
		/// </summary>
		ADD_RESOURCE_FAILED					= 106,

		/// <summary>
		/// 
		/// </summary>
		ADD_RESOURCE_COMPLETED				= 107,

		/// <summary>
		/// 
		/// </summary>
		ADD_ROLE_FAILED						= 108,

		/// <summary>
		/// 
		/// </summary>
		ADD_ROLE_COMPLETED					= 109,

		/// <summary>
		/// 
		/// </summary>
		ADD_ACCESSRIGHT_FAILED				= 110,

		/// <summary>
		/// 
		/// </summary>
		ADD_ACCESSRIGHT_COMPLETED			= 111,

		/// <summary>
		/// 
		/// </summary>
		ADD_USERROLE_FAILED					= 112,
		
		/// <summary>
		/// 
		/// </summary>
		ADD_USERROLE_COMPLETED				= 113,

		/// <summary>
		/// 
		/// </summary>
		UPDATE_USER_FAILED					= 114,

		/// <summary>
		/// 
		/// </summary>
		UPDATE_USER_COMPLETED				= 115,

		/// <summary>
		/// 
		/// </summary>
		UPDATE_ACTION_FAILED				= 116,

		/// <summary>
		/// 
		/// </summary>
		UPDATE_ACTION_COMPLETED				= 117,

		/// <summary>
		/// 
		/// </summary>
		UPDATE_APPLICATION_FAILED			= 118,


		/// <summary>
		/// 
		/// </summary>
		UPDATE_APPLICATION_COMPLETED		= 119,

		/// <summary>
		/// 
		/// </summary>
		UPDATE_RESOURCE_FAILED				= 120,

		/// <summary>
		/// 
		/// </summary>
		UPDATE_RESOURCE_COMPLETED			= 121,

		/// <summary>
		/// 
		/// </summary>
		UPDATE_ROLE_FAILED					= 122,

		/// <summary>
		/// 
		/// </summary>
		UPDATE_ROLE_COMPLETED				= 123,

		/// <summary>
		/// 
		/// </summary>
		DELETE_USER_FAILED					= 124,

		/// <summary>
		/// 
		/// </summary>
		DELETE_USER_COMPLETED				= 125,


		/// <summary>
		/// 
		/// </summary>
		DELETE_ACTION_FAILED				= 126,


		/// <summary>
		/// 
		/// </summary>
		DELETE_ACTION_COMPLETED				= 127,


		/// <summary>
		/// 
		/// </summary>
		DELETE_APPLICATION_FAILED			= 128,


		/// <summary>
		/// 
		/// </summary>
		DELETE_APPLICATION_COMPLETED		= 129,

		/// <summary>
		/// 
		/// </summary>
		DELETE_APPLICATION_RESOURCE_FAILED	= 130,

		/// <summary>
		/// 
		/// </summary>
		DELETE_APPLICATION_RESOURCE_COMPLETED = 131,

		/// <summary>
		/// 
		/// </summary>
		DELETE_RESOURCE_ACTION_FAILED		= 132,

		/// <summary>
		/// 
		/// </summary>
		DELETE_RESOURCE_ACTION_COMPLETED	= 133,


		/// <summary>
		/// 
		/// </summary>
		DELETE_ROLE_FAILED					= 134,

		/// <summary>
		/// 
		/// </summary>
		DELETE_ROLE_COMPLETED				= 135,


		/// <summary>
		/// 
		/// </summary>
		DELETE_USERROLE_FAILED				= 136,

		/// <summary>
		/// 
		/// </summary>
		DELETE_USERROLE_COMPLETED			= 137,


		/// <summary>
		/// 
		/// </summary>
		GET_ACCESSRIGHTS_FAILED				= 138,

		/// <summary>
		/// 
		/// </summary>
		GET_ACCESSRIGHTS_COMPLETED			= 139,


		/// <summary>
		/// 
		/// </summary>
		GET_USER_FAILED						= 140,

		/// <summary>
		/// 
		/// </summary>
		GET_USER_COMPLETED					= 141,

		/// <summary>
		/// 
		/// </summary>
		GET_USERTYPES_FAILED				= 142,


		/// <summary>
		/// 
		/// </summary>
		GET_USERTYPES_COMPLETED				= 143,

		/// <summary>
		/// 
		/// </summary>
		GET_STATUSCODES_FAILED				= 144,


		/// <summary>
		/// 
		/// </summary>
		GET_STATUSCODES_COMPLETED			= 145,

		/// <summary>
		/// 
		/// </summary>
		GET_USERS_FAILED					= 146,

		/// <summary>
		/// 
		/// </summary>
		GET_USERS_COMPLETED					= 147,


		/// <summary>
		/// 
		/// </summary>
		GET_APPLICATION_FAILED				= 148,


		/// <summary>
		/// 
		/// </summary>
		GET_APPLICATION_COMPLETED			= 149,


		/// <summary>
		/// 
		/// </summary>
		GET_APPLICATIONS_COMPLETED			= 150,

		/// <summary>
		/// 
		/// </summary>
		GET_APPLICATIONS_FAILED				= 151,


		/// <summary>
		/// 
		/// </summary>
		GET_APPLICATIONRESOURCES_FAILED		= 152,


		/// <summary>
		/// 
		/// </summary>
		GET_APPLICATIONRESOURCES_COMPLETED	= 153,

		/// <summary>
		/// 
		/// </summary>
		GET_ACTIONS_FAILED					= 154,


		/// <summary>
		/// 
		/// </summary>
		GET_ACTIONS_COMPLETED				= 155,

		/// <summary>
		/// 
		/// </summary>
		GET_ROLES_FAILED					= 156,

		/// <summary>
		/// 
		/// </summary>
		GET_ROLES_COMPLETED					= 157,

		/// <summary>
		/// 
		/// </summary>
		AUTHENTICATE_FAILURE_BADUSERID		= 158,

		/// <summary>
		/// 
		/// </summary>
		AUTHENTICATE_FAILURE_BADPASSWORD	= 159,

		/// <summary>
		/// 
		/// </summary>
		AUTHENTICATE_SUCCESS				= 160,


		/// <summary>
		/// 
		/// </summary>
		AUTHENTICATE_FAILURE_ACCOUNTLOCKED	= 161,

		/// <summary>
		/// 
		/// </summary>
		LOCKACCOUNT_COMPLETED				= 162,

		/// <summary>
		/// 
		/// </summary>
		LOCKACCOUNT_FAILED					= 163,

		/// <summary>
		/// 
		/// </summary>
		UNLOCKACCOUNT_COMPLETED				= 164,

		/// <summary>
		/// 
		/// </summary>
		UNLOCKACCOUNT_FAILED				= 165,

		/// <summary>
		/// 
		/// </summary>
		LDAP_ENDUSER_BIND_FAILURE			= 166,

		/// <summary>
		/// 
		/// </summary>
		LDAP_ADMIN_BIND_FAILURE				= 167,

		/// <summary>
		/// 
		/// </summary>
		LDAP_BIND_SEARCH_FAILURE			= 168,

		/// <summary>
		/// 
		/// </summary>
		LDAP_AUTHENTICATE_SUCCESS			= 169,

		/// <summary>
		/// 
		/// </summary>
		AUTHENTICATE_FAILURE_EXPIREDPASSWORD= 170,

		/// <summary>
		/// 
		/// </summary>
		LDAP_BIND_FAILURE_MISSING_ALTUID	= 171,

		/// <summary>
		/// 
		/// </summary>
		LDAP_AUTHENTICATE_FAILURE			= 172,

		/// <summary>
		/// 
		/// </summary>
		AUTHENTICATE_FAILURE				= 173,

		/// <summary>
		/// 
		/// </summary>
		UPDATE_CREDENTIALS_COMPLETED		= 174,

		/// <summary>
		/// 
		/// </summary>
		UPDATE_CREDENTIALS_FAILED			= 175,

		/// <summary>
		/// 
		/// </summary>
		UPDATE_NEXTQUESTIONINDEX_FAILED		= 176,

		/// <summary>
		/// 
		/// </summary>
		UPDATE_NEXTQUESTIONINDEX_COMPLETE	= 177,

		/// <summary>
		/// 
		/// </summary>
		LDAP_RESET_PASSWORD_FAILURE			= 178,

		/// <summary>
		/// 
		/// </summary>
		AUTHENTICATE_FAILURE_NODELEGATE		= 179,

		/// <summary>
		/// 
		/// </summary>
		UPDATE_APPROVALSTATUS_COMPLETE		= 180,

		/// <summary>
		/// 
		/// </summary>
		UPDATE_APPROVALSTATUS_FAILED		= 181,

		/// <summary>
		/// 
		/// </summary>
		ACCESSRIGHT_GRANTED					= 182,

		/// <summary>
		/// 
		/// </summary>
		ACCESSRIGHT_NOT_GRANTED				= 183,

        /// <summary>
        /// 
        /// </summary>
        GET_FUNCTIONALITIES_COMPLETED = 184,
        
        /// <summary>
        /// 
        /// </summary>
        GET_FUNCTIONALITIES_FAILED = 185,
		
        /// <summary>
        /// 
        /// </summary>
        UPDATE_ROLEFUNMAP_COMPLETED = 186,

        /// <summary>
        /// 
        /// </summary>
        UPDATE_ROLEFUNMAP_FAILED = 187,

        /// <summary>
        /// 
        /// </summary>
        CCMODIFY_LOGIN_COMPLETED = 188,
        
        /// <summary>
        /// 
        /// </summary>
        CCMODIFY_LOGIN_FAILED = 189,

        /// <summary>
        /// 
        /// </summary>
        UPDATE_BULK_EMAIL_COMPLETED = 190,

        /// <summary>
        /// 
        /// </summary>
        UPDATE_BULK_EMAIL_FAILED = 191,
        /// <summary>
		/// 
		/// </summary>
		UNKNOWN_ERROR = 999


	}
}
