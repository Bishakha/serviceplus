namespace Sony.US.siam.Authentication {
    /// <summary>
    /// The SonyB2BLDAPAuthenticator class is the implementation of LDAP Authentication that specifically
    /// applies to the Sony implementation of SIAM for a user of any type that defines this delegate.
    /// </summary>
    internal class SonyB2BLDAPAuthenticator // : Sony.US.siam.Interfaces.IAuthentication
	{

//        #region Private Class Variables

//        /// <summary>
//        /// 
//        /// </summary>
//        //private SonyLogger Logger = new SonyLogger();

//        /// <summary>
//        /// A constant value used by Logging.
//        /// </summary>
//        private const string ClassName = "SonyB2BLDAPAuthenticator";
		
//        /// <summary>
//        /// The Base User distinguished name. Searches in LDAP begin at this point in the tree.
//        /// </summary>
//        private string _baseUserDN = "";
		
//        /// <summary>
//        /// An instance of the WinLDAP wrapper. 
//        /// </summary>
//        private WinLDAP.ldap _ldap = null;

//        #endregion

//        #region Constructor

//        /// <summary>
//        /// 
//        /// </summary>
//        /// <param name="Host"></param>
//        /// <param name="Port"></param>
//        /// <param name="HaltOnFailure"></param>
//        /// <param name="BaseUserDN"></param>
//        internal SonyB2BLDAPAuthenticator(string Host,string Port,string HaltOnFailure,string BaseUserDN)
//        {
//            _baseUserDN = BaseUserDN;
//            _ldap = new WinLDAP.ldap(Host,System.Int32.Parse(Port));
//        }

//        #endregion

//        #region Public Implementatoin of IAuthentication


//        /// <summary>
//        /// Authenticate will attempt to contact an LDAP server to authenticate the user based
//        /// on credentials passed in.
//        /// </summary>
//        /// <param name="authResult">A reference to the Authentication Result. Upon return the
//        /// reference will contain additional details about The Authentication Process.</param>
//        /// <returns>An instance of Authentication result.</returns>
//        public void Authenticate(AuthenticationResult authResult)
//        {
//            TimeSpan start = TimeKeeper.GetCurrentTime();
//            //Logger.RaiseTrace(ClassName,"Entry:"+start.ToString(),TraceLevel.Verbose);
//            if (authResult.user.AlternateUserId == null)
//            {
//                UserStatus[] statusValues = new SecurityAdministrator().GetStatusCodes().SiamPayLoad as UserStatus[];
//                int statusId = -1;
//                for (int x = 0;x <= statusValues.GetUpperBound(0);x++)
//                {
//                    if (statusValues[x].StatusName == "Pending")
//                    {
//                        statusId = statusValues[x].StatusId;
//                    }
//                }
//                if (authResult.user.Password == authResult.credential.Password && authResult.user.UserStatus == statusId)
//                {
//                    // sony b2b unapproved pending authentication
//                    authResult.Result = true;
//                    authResult.SiamResponseCode = (int)Messages.LDAP_AUTHENTICATE_SUCCESS;
//                    authResult.SiamResponseMessage = Messages.LDAP_AUTHENTICATE_SUCCESS.ToString();
//                }
//                else //either b2b pending approval or b2c user
//                {
//                    if (new UtilityMethods().CheckPasswordAttempts(authResult.user, authResult.credential))
//                    {
//                        new SecurityAdministrator().LockAccount(authResult.user);
//                        authResult.Result = false;
//                        authResult.SiamResponseCode = (int)Messages.AUTHENTICATE_FAILURE_ACCOUNTLOCKED;
//                        authResult.SiamResponseMessage = Messages.AUTHENTICATE_FAILURE_ACCOUNTLOCKED.ToString();
//                        //Logger.RaiseTrace(ClassName, authResult.SiamResponseMessage, TraceLevel.Error);
//                    }
//                    else
//                    {
//                        authResult.Result = false;
//                        authResult.SiamResponseCode = (int)Messages.LDAP_BIND_FAILURE_MISSING_ALTUID;
//                        authResult.SiamResponseMessage = Messages.LDAP_BIND_FAILURE_MISSING_ALTUID.ToString();
//                    }
//                }
//            }
//            else
//            {
//                // First Bind as Admin
//                //SIAMConfig config = CachingServices.Cache["SiamConfiguration"] as SIAMConfig;
//                //if (config == null)
//                //{
//                //    config = new SIAMConfig();
//                //}
//                Credential adminCredential = new Credential();
//                adminCredential.UserName = ConfigurationData.Environment.SiamLDAPAuthentication.DN;
//                adminCredential.Password = ConfigurationData.Environment.SiamLDAPAuthentication.Password;
//                //Logger.RaiseTrace(ClassName,"LDAP Bind as: " + adminCredential.UserName + " " + adminCredential.Password,System.Diagnostics.TraceLevel.Verbose);
//                WinLDAP.WinLDAPResult r = _ldap.Bind(adminCredential.UserName,adminCredential.Password,3);
//                if (r.ReturnCode == 0)
//                {
//                    WinLDAP.WinLDAPSearchResults sresults = _ldap.Search(_baseUserDN,WinLDAP.SearchScope.LDAP_SCOPE_SUBTREE,"(uid="+authResult.user.AlternateUserId+")");
//                    if (sresults.LDAPEntries == null || sresults.LDAPEntries.Length != 1)
//                    {
//                        authResult.Result = false;
//                        authResult.SiamResponseCode = (int)Messages.LDAP_BIND_SEARCH_FAILURE;
//                        authResult.SiamResponseMessage = Messages.LDAP_BIND_SEARCH_FAILURE.ToString();
//                    }
//                    else
//                    {
//                        // before we attempt a bind lets see if the users account is OK
//                        // for authentication purposes.
//                        //System.DateTime expDate = System.DateTime.Parse(((System.Collections.ArrayList)sresults.LDAPEntries[0].Attributes["AMERICASONYB2BPASSWORDNOTIFICATIONDATE"])[0].ToString());
//                        //if (expDate <= System.DateTime.Now)
//                        //{
//                            // we have an expired password.
//                        //	authResult.Result = false;
//                        //	authResult.SiamResponseCode = (int)Messages.AUTHENTICATE_FAILURE_EXPIREDPASSWORD;
//                        //	authResult.SiamResponseMessage = Messages.AUTHENTICATE_FAILURE_EXPIREDPASSWORD.ToString();
//                        //}
//                        //else
//                        //{
//                            if (((System.Collections.ArrayList)sresults.LDAPEntries[0].Attributes["AMERICASONYB2BPORTALSTATUS"])[0].ToString() == "Lock" || ((System.Collections.ArrayList)sresults.LDAPEntries[0].Attributes["AMERICASONYB2BPORTALSTATUS"])[0].ToString() == "Disabled")
//                            {
//                                authResult.Result = false;
//                                authResult.SiamResponseCode = (int)Messages.AUTHENTICATE_FAILURE_ACCOUNTLOCKED;
//                                authResult.SiamResponseMessage = Messages.AUTHENTICATE_FAILURE_ACCOUNTLOCKED.ToString();
//                            }
//                            else
//                            {
//                                // we have bound as admin and successfully obtained the users record.
//                                // now rebind as the end user to the correct distinguished name
//                                string userDN = sresults.LDAPEntries[0].DistinguishedName;
//                                r = _ldap.Bind(userDN,authResult.credential.Password,0);
//                                if (r.ReturnCode == 0)
//                                {
//                                    authResult.Result = true;
//                                    authResult.SiamResponseCode = (int)Messages.LDAP_AUTHENTICATE_SUCCESS;
//                                    authResult.SiamResponseMessage = Messages.LDAP_AUTHENTICATE_SUCCESS.ToString();
//                                    authResult.user.ThirdID.UserSecurityPINs = new string[3];
//                                    //authResult.user.ThirdID.UserSecurityPINs[0] = Encryption.Encrypt(((System.Collections.ArrayList)sresults.LDAPEntries[0].Attributes["AMERICASONYB2BPIN1"])[0].ToString());
//                                    //authResult.user.ThirdID.UserSecurityPINs[1] = Encryption.Encrypt(((System.Collections.ArrayList)sresults.LDAPEntries[0].Attributes["AMERICASONYB2BPIN2"])[0].ToString());
//                                    //authResult.user.ThirdID.UserSecurityPINs[2] = Encryption.Encrypt(((System.Collections.ArrayList)sresults.LDAPEntries[0].Attributes["AMERICASONYB2BPIN3"])[0].ToString());
//                                    authResult.user.ThirdID.UserSecurityPINs[0] = Encryption.Encrypt(((System.Collections.ArrayList)sresults.LDAPEntries[0].Attributes["AMERICASONYB2BPIN1"])[0].ToString(),Encryption.PWDKey);
//                                    authResult.user.ThirdID.UserSecurityPINs[1] = Encryption.Encrypt(((System.Collections.ArrayList)sresults.LDAPEntries[0].Attributes["AMERICASONYB2BPIN2"])[0].ToString(), Encryption.PWDKey);
//                                    authResult.user.ThirdID.UserSecurityPINs[2] = Encryption.Encrypt(((System.Collections.ArrayList)sresults.LDAPEntries[0].Attributes["AMERICASONYB2BPIN3"])[0].ToString(), Encryption.PWDKey);
//                                }
//                                else
//                                {
//                                    if (r.ReturnCode == 49)
//                                    {
//                                        if (new UtilityMethods().CheckPasswordAttempts(authResult.user,authResult.credential))
//                                        {
//                                            new SecurityAdministrator().LockAccount(authResult.user);
//                                            authResult.Result = false;
//                                            authResult.SiamResponseCode = (int)Messages.AUTHENTICATE_FAILURE_ACCOUNTLOCKED;
//                                            authResult.SiamResponseMessage = Messages.AUTHENTICATE_FAILURE_ACCOUNTLOCKED.ToString();
//                                            //Logger.RaiseTrace(ClassName,authResult.SiamResponseMessage,TraceLevel.Error);
//                                        }
//                                        else
//                                        {
//                                            authResult.Result = false;
//                                            authResult.SiamInnerResponseMessage = r.ReturnMessage;
//                                            authResult.SiamResponseCode = (int)Messages.AUTHENTICATE_FAILURE_BADPASSWORD;
//                                            authResult.SiamResponseMessage = Messages.AUTHENTICATE_FAILURE_BADPASSWORD.ToString();
//                                        }
//                                    }
//                                    else
//                                    {
//                                        authResult.Result = false;
//                                        authResult.SiamInnerResponseMessage = r.ReturnMessage;
//                                        authResult.SiamResponseCode = (int)Messages.LDAP_ENDUSER_BIND_FAILURE;
//                                        authResult.SiamResponseMessage = Messages.LDAP_ENDUSER_BIND_FAILURE.ToString();
//                                    }
//                                }
//                            }
//                        //}
//                    }
//                }
//                else
//                {
//                    // Admin bind Failure
//                    authResult.Result = false;
//                    authResult.SiamInnerResponseMessage = r.ReturnMessage;
//                    authResult.SiamResponseCode = (int)Messages.LDAP_ADMIN_BIND_FAILURE;
//                    authResult.SiamResponseMessage = Messages.LDAP_ADMIN_BIND_FAILURE.ToString();
//                }
//                _ldap.UnBind(); //always unbind.
//            }

//            TimeSpan end = TimeKeeper.GetCurrentTime();
//            TimeSpan exectime = start.Subtract(end);
//            //Logger.RaiseTrace(ClassName,authResult.SiamResponseMessage,TraceLevel.Info);
//            //Logger.RaiseTrace(ClassName,"Exit:ExecutionTime="+exectime.ToString(),TraceLevel.Verbose);
//            return;
//        }


//        public void GetSecurityPIN(AuthenticationResult authResult)
//        {
//            TimeSpan start = TimeKeeper.GetCurrentTime();
//            //Logger.RaiseTrace(ClassName, "Entry:" + start.ToString(), TraceLevel.Verbose);
//            {
//                Credential adminCredential = new Credential();
//                adminCredential.UserName = ConfigurationData.Environment.SiamLDAPAuthentication.DN;
//                adminCredential.Password = ConfigurationData.Environment.SiamLDAPAuthentication.Password;
//                //Logger.RaiseTrace(ClassName, "LDAP Bind as: " + adminCredential.UserName + " " + adminCredential.Password, System.Diagnostics.TraceLevel.Verbose);
//                WinLDAP.WinLDAPResult r = _ldap.Bind(adminCredential.UserName, adminCredential.Password, 3);
//                if (r.ReturnCode == 0)
//                {
//                    WinLDAP.WinLDAPSearchResults sresults = _ldap.Search(_baseUserDN, WinLDAP.SearchScope.LDAP_SCOPE_SUBTREE, "(uid=" + authResult.user.AlternateUserId + ")");
//                    if (sresults.LDAPEntries == null || sresults.LDAPEntries.Length != 1)
//                    {
//                        authResult.Result = false;
//                        authResult.SiamResponseCode = (int)Messages.LDAP_BIND_SEARCH_FAILURE;
//                        authResult.SiamResponseMessage = Messages.LDAP_BIND_SEARCH_FAILURE.ToString();
//                    }
//                    else
//                    {
//                        if (((System.Collections.ArrayList)sresults.LDAPEntries[0].Attributes["AMERICASONYB2BPORTALSTATUS"])[0].ToString() == "Lock" || ((System.Collections.ArrayList)sresults.LDAPEntries[0].Attributes["AMERICASONYB2BPORTALSTATUS"])[0].ToString() == "Disabled")
//                        {
//                            authResult.Result = false;
//                            authResult.SiamResponseCode = (int)Messages.AUTHENTICATE_FAILURE_ACCOUNTLOCKED;
//                            authResult.SiamResponseMessage = Messages.AUTHENTICATE_FAILURE_ACCOUNTLOCKED.ToString();
//                        }
//                        else
//                        {
//                            // we have bound as admin and successfully obtained the users record.
//                            // now rebind as the end user to the correct distinguished name
//                            string userDN = sresults.LDAPEntries[0].DistinguishedName;
//                            r = _ldap.Bind(userDN, authResult.credential.Password, 0);
//                            if (r.ReturnCode == 0)
//                            {
//                                authResult.Result = true;
//                                authResult.SiamResponseCode = (int)Messages.LDAP_AUTHENTICATE_SUCCESS;
//                                authResult.SiamResponseMessage = Messages.LDAP_AUTHENTICATE_SUCCESS.ToString();
//                                authResult.user.ThirdID.UserSecurityPINs = new string[3];
//                                authResult.user.ThirdID.UserSecurityPINs[0] = ((System.Collections.ArrayList)sresults.LDAPEntries[0].Attributes["AMERICASONYB2BPIN1"])[0].ToString();
//                                authResult.user.ThirdID.UserSecurityPINs[1] = ((System.Collections.ArrayList)sresults.LDAPEntries[0].Attributes["AMERICASONYB2BPIN2"])[0].ToString();
//                                authResult.user.ThirdID.UserSecurityPINs[2] = ((System.Collections.ArrayList)sresults.LDAPEntries[0].Attributes["AMERICASONYB2BPIN3"])[0].ToString();
//                            }
//                            else
//                            {
//                                if (r.ReturnCode == 49)
//                                {
//                                    authResult.Result = false;
//                                    authResult.SiamInnerResponseMessage = r.ReturnMessage;
//                                    authResult.SiamResponseCode = (int)Messages.AUTHENTICATE_FAILURE_BADPASSWORD;
//                                    authResult.SiamResponseMessage = Messages.AUTHENTICATE_FAILURE_BADPASSWORD.ToString();
//                                }
//                                else
//                                {
//                                    authResult.Result = false;
//                                    authResult.SiamInnerResponseMessage = r.ReturnMessage;
//                                    authResult.SiamResponseCode = (int)Messages.LDAP_ENDUSER_BIND_FAILURE;
//                                    authResult.SiamResponseMessage = Messages.LDAP_ENDUSER_BIND_FAILURE.ToString();
//                                }
//                            }
//                        }
//                    }
//                }
//                else
//                {
//                    // Admin bind Failure
//                    authResult.Result = false;
//                    authResult.SiamInnerResponseMessage = r.ReturnMessage;
//                    authResult.SiamResponseCode = (int)Messages.LDAP_ADMIN_BIND_FAILURE;
//                    authResult.SiamResponseMessage = Messages.LDAP_ADMIN_BIND_FAILURE.ToString();
//                }
//                _ldap.UnBind(); //always unbind.
//            }

//            TimeSpan end = TimeKeeper.GetCurrentTime();
//            TimeSpan exectime = start.Subtract(end);
//            //Logger.RaiseTrace(ClassName, authResult.SiamResponseMessage, TraceLevel.Info);
//            //Logger.RaiseTrace(ClassName, "Exit:ExecutionTime=" + exectime.ToString(), TraceLevel.Verbose);
//            return;
//        }


//#endregion
	}
}
