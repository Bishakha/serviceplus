using System;
using Sony.US.siam;
using Sony.US.SIAMUtilities;
using Sony.US.siam.Interfaces;
using Sony.US.siam.Administration;
using Sony.US.siam.Authentication;
using Sony.US.siam.Authorization;

namespace Sony.US.siam
{
	/// <summary>
	/// The Utility Methods class contains simple utility calls that are of value throughout SIAM.
	/// </summary>
	internal class UtilityMethods
	{

		/// <summary>
		/// An isntance of <see cref="SecurityAdministrator"/>
		/// </summary>
		private SecurityAdministrator sa = new SecurityAdministrator();
		/// <summary>
		/// An instance of <see cref="SIAMConfig"/>
        /// Commented this line due to merge of both Configuration files. Used ConfigurationData.Environment instead
        //private SIAMConfig config = null;
		

		/// <summary>
		/// Internal constructor. Not for outside instantiation.
		/// </summary>
		internal UtilityMethods()
		{
            //config = CachingServices.Cache["SiamConfiguration"] as SIAMConfig;
            //if (config == null)
            //{
            //    config = new SIAMConfig();
            //    CachingServices.Cache["SiamConfiguration"] = config;
            //}
		}

        ///// <summary>
        ///// Checks Password attempts during authentication. Detects bad password attempts and handles
        ///// the process of incrementing a special counter cached based on the User.
        ///// </summary>
        ///// <param name="TheUser">An instance of <see cref="User"/></param>
        ///// <param name="credential">An instance of <see cref="Credential"/></param>
        ///// <returns></returns>
        //internal bool CheckPasswordAttempts(User TheUser,Credential credential)
        //{
        //    int AllowedAttempts = 3;
        //    int TimeThreshold = 5;
        //    //for (int x = 0; x <= config.UserTypes.GetUpperBound(0); x++)
        //    for (int x = 0; x <= ConfigurationData.Environment.UserTypes.GetUpperBound(0); x++)
        //    {
        //        //if (config.UserTypes[x].Name == TheUser.UserType)
        //        if (ConfigurationData.Environment.UserTypes[x].Name == TheUser.UserType)
        //        {
        //            //AllowedAttempts = config.UserTypes[x].LoginPolicy.FailedLoginThreshold;
        //            //TimeThreshold = config.UserTypes[x].LoginPolicy.TimeThreshold;
        //            AllowedAttempts = ConfigurationData.Environment.UserTypes[x].LoginPolicy.FailedLoginThreshold;
        //            TimeThreshold = ConfigurationData.Environment.UserTypes[x].LoginPolicy.TimeThreshold;
        //            break;
        //        }
        //    }
						
        //    // This is a bad password attemp.
        //    AuthentAttempt attempt = new AuthentAttempt();
        //    attempt.dateTime = System.DateTime.Now;
        //    attempt.User = credential.UserName;

        //    AuthentAttempt lastAttempt = (AuthentAttempt)CachingServices.Cache[credential.UserName+"authentattempt"];
        //    if (lastAttempt != null)
        //    {
        //        CachingServices.Cache.Remove(credential.UserName+"authentattempt");
        //        if (lastAttempt.count >= AllowedAttempts)
        //        {
        //            return true;
        //        }
        //        TimeSpan span = attempt.dateTime.Subtract(lastAttempt.dateTime);
        //        if (span <= new TimeSpan(0,0,TimeThreshold,0,0))
        //        {
        //            attempt.count = lastAttempt.count+1;
        //        }
        //        CachingServices.Cache[credential.UserName+"authentattempt"] = attempt;
        //    }
        //    else
        //    {
        //        attempt.count = 1;
        //        CachingServices.Cache[credential.UserName+"authentattempt"] = attempt;
        //    }
        //    return false;
        //}
	}
}
