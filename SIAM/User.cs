using System;
using Sony.US.siam;
using Sony.US.SIAMUtilities;
using Sony.US.siam.Interfaces;
using Sony.US.siam.Administration;
using Sony.US.siam.Authentication;
using Sony.US.siam.Authorization;

namespace Sony.US.siam
{
	/// <summary>
	/// A serialiable container class for a SIAM User.
	/// </summary>
	[Serializable]
	public class User
	{
		private string _companyname;
		private string _firstname;
		private string _lastname;
		private string _emailaddress;
		private string _usertype;

        public Sony.US.ServicesPLUS.Core.Customer ConvertToCustomer() {
            var customer = new Sony.US.ServicesPLUS.Core.Customer();
            
            customer.SequenceNumber = this.SequenceNumber.ToString();
            customer.CustomerID = this.CustomerID;
            customer.LdapID = this.AlternateUserId;
            customer.UserName = this.UserId;
            customer.SIAMIdentity = this.Identity;
            customer.UserType = this.UserType;
            customer.StatusCode = (this.UserStatusCode == 1) ? (short)1 : (short)3;   // Default to 3, unless LDAP has it set already(?)

            customer.FirstName = this.FirstName;
            customer.LastName = this.LastName;
            customer.CompanyName = this.CompanyName;
            customer.Address.Line1 = this.AddressLine1;
            customer.Address.Line2 = this.AddressLine2;
            customer.Address.Line3 = this.AddressLine3;
            customer.Address.City = this.City;
            customer.Address.State = this.State;
            customer.Address.PostalCode = this.Zip;
            customer.CountryCode = this.Country;

            customer.PhoneNumber = this.Phone;
            customer.PhoneExtension = this.PhoneExtension;
            customer.FaxNumber = this.Fax;
            customer.EmailAddress = this.EmailAddress;
            //customer.EmailOk = bool.Parse(this.EmailOK.ToString());
            customer.Enabledocumentidnumber = "";
            customer.SubscriptionID = this.SubscriptionID;

            return customer;
        }

		/// <summary>
		/// Effective rights array
		/// </summary>
		public AccessRight[] EffectiveRights;

		/// <summary>
		/// Company Name Property
		/// </summary>
		public string CompanyName
		{
			get
			{
				return _companyname;
			}
			set
			{
				_companyname = value;
			}
		}


		/// <summary>
		/// Address Line 1 property
		/// </summary>
		public string AddressLine1;

		/// <summary>
		/// Address Line 2 property
		/// </summary>
		public string AddressLine2;

        /// <summary>
        /// Address Line 3 property
        /// </summary>
        public string AddressLine3;

		/// <summary>
		/// City property
		/// </summary>
		public string City;

		/// <summary>
		/// State property
		/// </summary>
		public string State;

        /// <summary>
        /// Country property
        /// </summary>
        public string Country;

		/// <summary>
		/// zip property
		/// </summary>
		public string Zip;

		/// <summary>
		/// Fax number property
		/// </summary>
		public string Fax;

		/// <summary>
		/// Phone property
		/// </summary>
		public string Phone;

		/// <summary>
		/// The users Login Id
		/// </summary>
		public string UserId;
			
		/// <summary>
		/// The User's directory service LDAP UID attribute value.
		/// </summary>
		public string AlternateUserId;
		
		/// <summary>
		/// The users first name
		/// </summary>
		public string FirstName
		{
			get
			{
				return _firstname;
			}
			set
			{
				_firstname = value;
			}
		}

		/// <summary>
		/// The users Last name
		/// </summary>
		public string LastName
		{
			get
			{
				return _lastname;
			}
			set
			{
				_lastname = value;
			}
		}
		
		/// <summary>
		/// Eamil Address
		/// </summary>
		public string EmailAddress
		{
			get
			{
				return _emailaddress;
			}
			set
			{
				_emailaddress = value;
			}
		}

		/// <summary>
		/// Password
		/// </summary>
		public string Password;
	
		/// <summary>
		/// Password Expiration Date
		/// </summary>
		public string PasswordExpirationDate;
	
		/// <summary>
		/// UserType value. Defined by configuration
		/// </summary>
		public string UserType
		{
			get
			{
				return _usertype;
			}
			set
			{
				_usertype = value;
			}
		}
		
		/// <summary>
		/// UserStatus value. Defined by the database
		/// </summary>
       // public int UserStatus;//6668
        public int UserStatusCode;//6668
		
		/// <summary>
		/// The User's Identity is a unique value generated at runtime by SIAM.
		/// </summary>
		public string Identity;

		/// <summary>
		/// The Account Lock status for this user.
		/// </summary>
		public int LockStatus;

        public string CustomerID = string.Empty;
        public Int32 SequenceNumber = 0;
        public Int16 EmailOK = 1;
        //public Int16 CALIFORNIAPARTIALTAX;
        public string Enabledocumentidnumber = string.Empty;
        public string PhoneExtension = string.Empty;
        public string SubscriptionID = string.Empty;
		/// <summary>
		/// An array containing the Users role hiearchy.
		/// </summary>
		public Role[] Roles;

        public Functionality[] Functionalities;
        
        
        /// <summary>
		/// A user in SIAM may or may not be required to answer a third security question in order
		/// to complete the Authentication process. If defined by configuration when the user is
		/// authenticated this structure will be populated with data if the user is to be prompted
		/// with a third security question. If needed the UI should prompt the user for the answer to 
		/// the question contained in the NextQuestion member of the ThirdId structure. The answer as
		/// well as the user's ThirdId structure is then passed back to SIAM for evaluation. SIAM will
		/// decrypt the users PIN values, obtain the Index and compare the user's response to the value
		/// stored in the LDAP. If a match is obtained then the authentication process is complete. Use
		/// SecurityAdminstrator.CheckThirdId() to perform this operation.
		/// </summary>
		public _thirdIDstructure ThirdID = new _thirdIDstructure();

		/// <summary>
		/// Third Id structure definition
		/// </summary>
		public class _thirdIDstructure
		{
			/// <summary>
			/// 
			/// </summary>
			public _thirdIDstructure()
			{
				UserSecurityPINs = new string[3];
			}
			/// <summary>
			/// An array of string values contianing the Security PIN values for this user from LDAP.
			/// These values will be encrypted.
			/// </summary>
			public string[] UserSecurityPINs;
			/// <summary>
			/// An index pointer to the next question in configuration.
			/// </summary>
			public int NextQuestionIndex;
			/// <summary>
			/// A string value containing the next question.
			/// </summary>
			public string NextQuestion;
           		}
	}
}
