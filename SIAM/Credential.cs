using System;
using Sony.US.siam;
using Sony.US.SIAMUtilities;
using Sony.US.siam.Interfaces;
using Sony.US.siam.Administration;
using Sony.US.siam.Authentication;
using Sony.US.siam.Authorization;

namespace Sony.US.siam
{
	/// <summary>
	/// A serializable structure used to define a credential.
	/// </summary>
	[Serializable]
	public struct Credential
	{

		/// <summary>
		/// The user name
		/// </summary>
		public string UserName;
		/// <summary>
		/// The password
		/// </summary>
		public string Password;

		/// <summary>
		/// 
		/// </summary>
		public bool IsInternalUser;

	}
}
