using System;
using Sony.US.siam;
using Sony.US.SIAMUtilities;
using Sony.US.siam.Interfaces;
using Sony.US.siam.Administration;
using Sony.US.siam.Authentication;
using Sony.US.siam.Authorization;
namespace Sony.US.siam.Interfaces
{
	/// <summary>
	/// Interface definition for authorization components.
	/// </summary>
	public interface IAuthorization
	{
		/// <summary>
		/// 
		/// </summary>
		/// <param name="user"></param>
		/// <param name="Application"></param>
		/// <param name="Resource"></param>
		/// <param name="Action"></param>
		/// <returns></returns>
		AuthorizationResult Authorize(User user,string Application,string Resource,string Action);

		/// <summary>
		/// 
		/// </summary>
		/// <param name="user"></param>
		/// <param name="Application"></param>
		/// <returns></returns>
		AccessRight[] GetAccessRights(User user,string Application);
	}
}
