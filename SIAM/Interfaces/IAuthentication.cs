using System;
using Sony.US.siam;
using Sony.US.SIAMUtilities;
using Sony.US.siam.Interfaces;
using Sony.US.siam.Administration;
using Sony.US.siam.Authentication;
using Sony.US.siam.Authorization;
namespace Sony.US.siam.Interfaces
{
	/// <summary>
	/// Interface definition for Authentication components.
	/// </summary>
	public interface IAuthentication
	{
		/// <summary>
		/// The Authenticate method signature implemented by any authentication class. 
		/// </summary>
		/// <param name="result"></param>
		/// <returns></returns>
        void Authenticate(AuthenticationResult result);
        
        /// <summary>
        /// This method is to return security answer from LDAP
        /// </summary>
        /// <param name="result"></param>
        void GetSecurityPIN(AuthenticationResult result);
	}
}
