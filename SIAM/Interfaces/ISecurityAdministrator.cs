using System;
using System.Data;
using Sony.US.siam;
using Sony.US.SIAMUtilities;
using Sony.US.siam.Interfaces;
using Sony.US.siam.Administration;
using Sony.US.siam.Authentication;
using Sony.US.siam.Authorization;

namespace Sony.US.siam.Interfaces
{
	/// <summary>
	/// Represents the base interface for any component that wishes to serve as an Administration 
	/// Delegate in SIAM.
	/// </summary>
	public interface ISecurityAdministrator
	{
		#region Add Methods

		/// <summary>
		/// Interface Definition for Adding an Action to SIAM
		/// </summary>
		/// <param name="ActionName">The Action Name</param>
		/// <param name="ErrorString">An output parameter containing any error information</param>
		/// <returns>a double value containing the record Id.</returns>
        //double AddAction(string ActionName,out string ErrorString);//6732
		
		/// <summary>
		/// Interface definition for adding an application to SIAM.
		/// </summary>
		/// <param name="ApplicationName">The appliction name.</param>
		/// <param name="ErrorString">A string value containing any error information</param>
		/// <returns>A double value containing the record id.</returns>
		double AddApplication(string ApplicationName,out string ErrorString);
		
		/// <summary>
		/// Interface Definition for adding an application resource to siam
		/// </summary>
		/// <param name="ApplicationId">Athe Application Id.</param>
		/// <param name="ResourceId">The resource Id</param>
		/// <param name="ErrorString">An output parameter containing any error information</param>
		/// <returns>A double value containing the record id.</returns>
		double AddApplicationResource(double ApplicationId, double ResourceId,out string ErrorString);
		
		/// <summary>
		/// Interface Definition for adding a resource to SIAM 
		/// </summary>
		/// <param name="ResourceName">The name of the resource.</param>
		/// <param name="ErrorString">An output parameter containing any error information</param>
		/// <returns>A double value containing the record id.</returns>
		double AddResource(string ResourceName,out string ErrorString);
		
		/// <summary>
		/// Interface Definition for adding a role to siam.
		/// </summary>
		/// <param name="RoleName">The name of the role</param>
		/// <param name="ParentRoleId">Parent Role Id</param>
		/// <param name="ErrorString">An output parameter containing any error information</param>
		/// <returns>A double value containing the record id.</returns>
		double AddRole(string RoleName,int ParentRoleId,out string ErrorString);
		
		/// <summary>
		/// Interface Definition for adding a user to siam
		/// </summary>
		/// <param name="user">An instance of <see cref="User"/></param>
		/// <param name="ErrorString">An output parameter containing any error information</param>
		/// <returns>A string value containing the return result.</returns>        
       string AddUser(User user,out string ErrorString);
		
        /// <summary>
        /// Interface Definition for adding a test user to siam
        /// </summary>
		/// <param name="user">An instance of <see cref="User"/></param>
        /// <param name="acctInfo"></param>
        /// <param name="RequestingApplication"></param>
        /// <param name="RequestingUserID"></param>
        /// <param name="RequestingPassword"></param>
		/// <param name="ErrorString">An output parameter containing any error information</param>
		/// <returns>A string value containing the return result.</returns>        
       string AddTestUser(User user, AccountInfoVO acctInfo, string RequestingApplication, string RequestingUserID, string RequestingPassword, out string ErrorString);

		/// <summary>
		/// Interface Definition for adding a userRole to SIAM
		/// </summary>
		/// <param name="UserId"></param>
		/// <param name="RoleId"></param>
		/// <param name="ErrorString">An output parameter containing any error information</param>
		/// <returns>A double value containing the record id.</returns>
		double AddUserRole(string UserId,int RoleId,out string ErrorString);
		
        /// <summary>
        /// 
        /// </summary>
        /// <param name="user"></param>
        /// <param name="ErrorString"></param>
        /// <param name="ErrorString"></param>
        /// <returns></returns>
        bool AddISSAWebCall(User user, string Message, out string ErrorString);

		/// <summary>
		/// Interface Definition for adding An Access Right to SIAM.
		/// </summary>
		/// <param name="RoleId">The role Id.</param>
		/// <param name="ApplicationId">The application id.</param>
		/// <param name="ResourceId">The resource Id.</param>
		/// <param name="ActionId">The action id</param>
		/// <param name="ErrorString">An output parameter containing any error information</param>
		/// <returns>A double value containing the record id.</returns>
		double AddAccessRight(int RoleId,int ApplicationId,int ResourceId,int ActionId,out string ErrorString);

		#endregion

		#region Delete Methods

		/// <summary>
		/// Interface definition for DeleteAction
		/// </summary>
		/// <param name="ActionId">The Action Id</param>
		/// <param name="ErrorString">A string value containing error information</param>
		/// <returns>A boolean value indicating success or failure</returns>
        //bool DeleteAction(int ActionId,out string ErrorString);//6732

		/// <summary>
		/// Interface definition for DeleteApplication
		/// </summary>
		/// <param name="ApplicationId">The application Id</param>
		/// <param name="ErrorString">A string value containing error information</param>
		/// <returns>A boolean value indicating success or failure</returns>
		bool DeleteApplication(int ApplicationId,out string ErrorString);

		/// <summary>
		/// Interface definition for DeleteApplicationResource
		/// </summary>
		/// <param name="ApplicationId">The Application Id</param>
		/// <param name="ResourceId">The resource Id</param>
		/// <param name="ErrorString">A string value containing error information</param>
		/// <returns>A boolean value indicating success or failure</returns>
		bool DeleteApplicationResource(int ApplicationId, int ResourceId,out string ErrorString);

		/// <summary>
		/// Interface definition for DeleteResource
		/// </summary>
		/// <param name="ResourceId">The resource Id</param>
		/// <param name="ErrorString">A string value containing error information</param>
		/// <returns>A boolean value indicating success or failure</returns>
		bool DeleteResource(int ResourceId,out string ErrorString);

		/// <summary>
		/// Interface definition for DeleteResourceAction
		/// </summary>
		/// <param name="ResourceId">The resource Id</param>
		/// <param name="ActionId">The Action Id</param>
		/// <param name="ErrorString">A string value containing error information</param>
		/// <returns>A boolean value indicating success or failure</returns>
		bool DeleteResourceAction(int ResourceId, int ActionId,out string ErrorString);

		/// <summary>
		/// Interface definition for DeleteRole
		/// </summary>
		/// <param name="RoleId">The role Id</param>
		/// <param name="ErrorString">A string value containing error information</param>
		/// <returns>A boolean value indicating success or failure</returns>
		bool DeleteRole(int RoleId,out string ErrorString);

		/// <summary>
		/// Interface definition for DeleteUser
		/// </summary>
		/// <param name="UserId">The users siam identity</param>
		/// <param name="ErrorString">A string value containing error information</param>
		/// <returns>A boolean value indicating success or failure</returns>
        bool DeleteUser(string UserId, out string ErrorString, out User oUser);

		/// <summary>
		/// Interface definition for DeleteUserRole
		/// </summary>
		/// <param name="UserId">The Users siam identity</param>
		/// <param name="RoleId">The role Id</param>
		/// <param name="ErrorString">A string value containing error information</param>
		/// <returns>A boolean value indicating success or failure</returns>
		bool DeleteUserRole(string UserId,int RoleId,out string ErrorString);

		#endregion

		#region Get Methods

		/// <summary>
		/// 		
		/// </summary>
		/// <param name="LDAPID"></param>
		/// <returns></returns>
		string[] GetUserPinValues(string LDAPID);


		/// <summary>
		/// Interface definition for GetEffectiveAccessRights
		/// </summary>
		/// <param name="roles">An array of <see cref="Role"/> objects</param>
		/// <param name="ErrorString">A string containing any error information</param>
		/// <returns>An array of <see cref="AccessRight"/> objects</returns>
		AccessRight[] GetEffectiveAccessRights(Role[] roles,out string ErrorString);
		

		/// <summary>
		/// Interface definition for GetUserWithEmail
		/// </summary>
		/// <param name="UserName"></param>
		/// <param name="EmailAddress"></param>
		/// <param name="ErrorString"></param>
		/// <returns></returns>
        //User GetUserWithEmail(string UserName,string EmailAddress,out string ErrorString);//6691
        User GetUserWithEmail(string UserName,out string ErrorString);//6691
		
		/// <summary>
		/// Interface definition for GetUserTypes
		/// </summary>
		/// <param name="ErrorString">A string containing any error information</param>
		/// <returns>A string array</returns>
		string[] GetUserTypes(out string ErrorString);
		
		/// <summary>
		/// Interface definition for GetStatusCodes
		/// </summary>
		/// <param name="ErrorString">A string containing any error information</param>
		/// <returns>An array of <see cref="UserStatus"/> objects</returns>
        //UserStatus[] GetStatusCodes(out string ErrorString);//6668
		
		/// <summary>
		/// Interface definition for GetAction
		/// </summary>
		/// <param name="ActionId">An integer value containing the Action Id</param>
		/// <param name="ErrorString">A string containing any error information</param>
		/// <returns>An instance of <see cref="Action"/></returns>
        //Action GetAction(int ActionId,out string ErrorString);//6732
		
		/// <summary>
		/// Interface definition for GetActions
		/// </summary>
		/// <param name="ErrorString">A string containing any error information</param>
		/// <returns>An array of <see cref="Action"/> objects</returns>
        //Action[] GetActions(out string ErrorString);//6732
		
		/// <summary>
		/// Interface definition for GetApplication
		/// </summary>
		/// <param name="ApplicationId">An integer value containing the Application Id</param>
		/// <param name="ErrorString">A string containing any error information</param>
		/// <returns>An instance of <see cref="Application"/></returns>
		Application GetApplication(int ApplicationId,out string ErrorString);
		
		/// <summary>
		/// Interface definition for GetApplications
		/// </summary>
		/// <param name="ErrorString">A string containing any error information</param>
		/// <returns>An array of <see cref="Application"/> objects</returns>
		Application[] GetApplications(out string ErrorString);
		
		/// <summary>
		/// Interface definition for GetApplicationResources
		/// </summary>
		/// <param name="ApplicationId">An integer value containing the application id</param>
		/// <param name="ErrorString">A string containing any error information</param>
		/// <returns>An array of <see cref="Resource"/>objects</returns>
		Resource[] GetApplicationResources(int ApplicationId,out string ErrorString);
		
		/// <summary>
		/// Interface definition for GetResource
		/// </summary>
		/// <param name="ResourceId">An integer value containing the resource id</param>
		/// <param name="ErrorString">A string containing any error information</param>
		/// <returns>An instanct of <see cref="Resource"/></returns>
		Resource GetResource(int ResourceId,out string ErrorString);

        /// <summary>
        /// Interface definition for GetFunctionalities
        /// </summary>
        /// <param name="ErrorString">A string containing any error information</param>
        /// <returns>An array of <see cref="Functionality"/> objects</returns>
        Functionality[] GetFunctionalities (out string ErrorString);

        /// <summary>
        /// Interface definition for GetFunctionalities
        /// </summary>
        /// <param name="RoleID"></param>
        /// <param name="ErrorString">A string containing any error information</param>
        /// <returns>An array of <see cref="Functionality"/> objects</returns>
        Functionality[] GetFunctionalities(Int32 RoleID, out string ErrorString);

        

		/// <summary>
		/// Interface definition for GetRoles
		/// </summary>
		/// <param name="ErrorString">A string containing any error information</param>
		/// <returns>An array of <see cref="Role"/> objects</returns>
		Role[] GetRoles(out string ErrorString);
		
		/// <summary>
		/// Interface definition for GetRoles
		/// </summary>
		/// <param name="RoleId">An integer value containing the Role Id</param>
		/// <param name="ErrorString">A string containing any error information</param>
		/// <returns>An instance of <see cref="Role"/></returns>
		Role GetRoles(int RoleId,out string ErrorString);
		
		/// <summary>
		/// Interface definition for GetRoles
		/// </summary>
		/// <param name="UserId">A string value containing the users siam identity</param>
		/// <param name="ErrorString">A string containing any error information</param>
		/// <returns>An array of <see cref="Role"/> objects</returns>
		Role[] GetRoles(string UserId,out string ErrorString);
		
		/// <summary>
		/// Interface definition for GetChildRoles
		/// </summary>
		/// <param name="ParentRoleId">An integer value containing the parent role id</param>
		/// <param name="ErrorString">A string containing any error information</param>
		/// <returns>An array of <see cref="Role"/> objects</returns>
		Role[] GetChildRoles(int ParentRoleId,out string ErrorString);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Identity"></param>
        /// <param name="ErrorString"></param>
        /// <returns></returns>
        DataSet GetSiamAndCustomer(string Identity, out string ErrorString);

		/// <summary>
		/// Interface definition for GetUser
		/// </summary>
		/// <param name="Identity">A string value containing the User's siam identity</param>
		/// <param name="ErrorString">A string containing any error information</param>
		/// <returns>An instance of <see cref="User"/></returns>
		User GetUser(string Identity,out string ErrorString);				
		
        /// <summary>
        /// Get all invalid user object
        /// </summary>
        /// <param name="ErrorString"></param>
        /// <returns></returns>
        /// Commented by Prasad because this method is not used any where in the application
        //object GetInvalidUsers(out string ErrorString);
		
		/// <summary>
		/// Interface definition for GetUser
		/// </summary>
		/// <param name="result">An instance of <see cref="AuthenticationResult"/></param>
		/// <param name="ErrorString">A string containing any error information</param>
		void GetUser(AuthenticationResult result,out string ErrorString);
		
		/// <summary>
		/// Interface definition for GetUsers
		/// </summary>
		/// <param name="deep">A boolean value indicating whether or not to use deep hydration</param>
		/// <param name="ErrorString">A string containing any error information</param>
		/// <returns>An array of <see cref="User"/> objects if deep is true otherwise
		/// an array of <see cref="ShallowUser"/> objects</returns>
		object[] GetUsers(bool deep,out string ErrorString);


        /// <summary>
        /// Interface definition for GetUsers
        /// </summary>
        /// <param name="SearchPattern">A string value containing the search parameter</param>
        /// <param name="deep">A boolean value indicating whether or not to use deep hydration</param>
        /// <param name="SPSSearch">A bool value saying SPS Search</param>
        /// <param name="ErrorString">A string containing any error information</param>
        /// <returns>An array of <see cref="User"/> objects if deep is true otherwise
        /// an array of <see cref="ShallowUser"/> objects</returns>
        object[] GetUsers(string SearchPattern, bool deep,bool SPSSearch, out string ErrorString);

		
		/// <summary>
		/// Interface definition for GetUsers
		/// </summary>
		/// <param name="SearchPattern">A string value containing the search parameter</param>
		/// <param name="deep">A boolean value indicating whether or not to use deep hydration</param>
		/// <param name="ErrorString">A string containing any error information</param>
		/// <returns>An array of <see cref="User"/> objects if deep is true otherwise
		/// an array of <see cref="ShallowUser"/> objects</returns>
		object[] GetUsers(string SearchPattern,bool deep,out string ErrorString);
		
		/// <summary>
		/// Interface definition for GetUsers
		/// </summary>
		/// <param name="RoleId">An integer value containing Role Id</param>
		/// <param name="ErrorString">A string containing any error information</param>
		/// <returns>An array of <see cref="User"/> objects</returns>
        /// Commented By Rajkumar A on 07-01-2010. Stored Procedure is not used in the application
		//object[] GetUsers(int RoleId,out string ErrorString);
		
		/// <summary>
		/// Interface definition for GetAccessRights
		/// </summary>
		/// <param name="RoleId">An integer value contining the Role id</param>
		/// <param name="ErrorString">A string containing any error information</param>
		/// <returns>An array of <see cref="AccessRight"/> objects</returns>
		AccessRight[] GetAccessRights(int RoleId,out string ErrorString);

		#endregion

		#region Update Methods
		
		/// <summary>
		/// 
		/// </summary>
		/// <param name="ActionName"></param>
		/// <param name="ActionId"></param>
		/// <param name="ErrorString"></param>
		/// <returns></returns>
        //bool UpdateAction(string ActionName, int ActionId,out string ErrorString);//6732
		
		/// <summary>
		/// 
		/// </summary>
		/// <param name="ApplicationName"></param>
		/// <param name="ApplicationId"></param>
		/// <param name="ErrorString"></param>
		/// <returns></returns>
		bool UpdateApplication(string ApplicationName, int ApplicationId,out string ErrorString);
		
		/// <summary>
		/// 
		/// </summary>
		/// <param name="ResourceName"></param>
		/// <param name="ResourceId"></param>
		/// <param name="ErrorString"></param>
		/// <returns></returns>
		bool UpdateResource(string ResourceName, int ResourceId,out string ErrorString);
		
		/// <summary>
		/// 
		/// </summary>
		/// <param name="RoleName"></param>
		/// <param name="RoleId"></param>
		/// <param name="ErrorString"></param>
		/// <returns></returns>
		bool UpdateRole(string RoleName, int RoleId,out string ErrorString);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="EMail"></param>
        /// <param name="ErrorString"></param>
        /// <returns></returns>

        // commented by Sunil as UpdateBULKEMail is not used on 07-01-10
        //bool UpdateBULKEMAIL(string EMail, out string ErrorString);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="RoleId"></param>
        /// <param name="AddFuncId"></param>
        /// <param name="DelFuncId"></param>
        /// <param name="ErrorString"></param>
        /// <returns></returns>
        bool UpdateRoleFunction(string RoleId, string AddFuncId, string DelFuncId, out string ErrorString);
		
        /// <summary>
		/// 
		/// </summary>
		/// <param name="user"></param>
		/// <param name="ErrorString"></param>
		/// <returns></returns>
		bool UpdateUser(User user,out string ErrorString);
		
		#endregion
	
		/// <summary>
		/// 
		/// </summary>
		void ReTryEmailFailure();

		/// <summary>
		/// 
		/// </summary>
		/// <param name="user"></param>
		/// <param name="ErrorString"></param>
		/// <returns></returns>
		bool LockAccount(User user,out string ErrorString);
		
		/// <summary>
		/// 
		/// </summary>
		/// <param name="user"></param>
		/// <param name="ErrorString"></param>
		/// <returns></returns>
		bool UnLockAccount(User user,out string ErrorString);
		
		/// <summary>
		/// 
		/// </summary>
		/// <param name="user"></param>
		/// <param name="NewCredentials"></param>
		/// <param name="ExpiresInDays"></param>
		/// <param name="ErrorString"></param>
		/// <returns></returns>
		bool UpdateCredentials(User user,Credential NewCredentials,int ExpiresInDays,out string ErrorString);
		
		/// <summary>
		/// 
		/// </summary>
		/// <param name="Identity"></param>
		/// <param name="NewValue"></param>
		/// <param name="ErrorString"></param>
		/// <returns></returns>
		bool UpdateNextQuestionIndex(string Identity,int NewValue,out string ErrorString);
		
		/// <summary>
		/// 
		/// </summary>
		/// <param name="FirstName"></param>
		/// <param name="LastName"></param>
		/// <param name="EmailAddress"></param>
		/// <param name="NewStatusName"></param>
		/// <param name="LDAPUID"></param>
		/// <param name="ErrorString"></param>
		/// <returns></returns>
		//bool UpdateApprovalStatus(string FirstName,string LastName,string EmailAddress,string NewStatusName,string LDAPUID,out string ErrorString);//6668

		/// <summary>
		/// 
		/// </summary>
		/// <param name="msg"></param>
		/// <param name="ExceptionMessage"></param>
		void LogEmailFailure(System.Net.Mail.MailMessage msg, string ExceptionMessage);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="UserId"></param>
        /// <param name="ErrorString"></param>
        /// <returns></returns>
        /// Commented as its not used in Presentation Layer
        ///object GetCCModifierUser(string UserId, out string ErrorString);

	}
}
