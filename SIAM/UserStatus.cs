using System;

namespace Sony.US.siam {
    /// <summary>
    /// Serializable container class for a User Status value in SIAM.
    /// </summary>
    [Serializable]
	public class UserStatus
	{
		/// <summary>
		/// The status Id for this instance.
		/// </summary>
		public int StatusId;
		/// <summary>
		/// The name of the status value.
		/// </summary>
		public string StatusName;
	}
}
