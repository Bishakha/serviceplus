/*=====================================================================
  File:      SendRequest.cs

  Summary:   Demonstrates the use of the LdapConnection class to do 
                    various directory operations.

---------------------------------------------------------------------
  This file is part of the Microsoft .NET SDK Code Samples.

  Copyright (C) Microsoft Corporation.  All rights reserved.

This source code is intended only as a supplement to Microsoft
Development Tools and/or on-line documentation.  See these other
materials for detailed information regarding Microsoft code samples.

THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
PARTICULAR PURPOSE.
=====================================================================*/

// To compile: csc.exe /out:App.exe /r:System.DirectoryServices.Protocols.dll 
//                                                              SendRequest.cs
//
// To run: App.exe <ldapServer> <user> <pwd> <domain> <targetOU>
//     Eg: App.exe myDC1.testDom.fabrikam.com  user1  secret@~1 testDom 
//                                  OU=samples,DC=testDom,DC=fabrikam,DC=com            

using System;
using System.Text;
using System.Net;
using System.Security.Permissions;
using SDS = System.DirectoryServices;
using System.Security.Cryptography.X509Certificates;
using System.Configuration;
using System.DirectoryServices.Protocols;


[assembly: SecurityPermission(SecurityAction.RequestMinimum, Execution = true)]
[assembly: SDS.DirectoryServicesPermission(SecurityAction.RequestMinimum)]
namespace WinLDAP {

    public class NetLDAP
{
    // static variables used throughout the sample code
    static LdapConnection ldapConnection;
    static string  ldapServer;
    static NetworkCredential credential;
    //static string targetOU; // dn of an OU. eg: "OU=sample,DC=fabrikam,DC=com"

    // object DNs that are created in the sample
    //static string ou1, ou2, ou3;

    //public static void Main(string[] args)
    //{
    //    try
    //    {
    //        CreateConnection();
    //        Search();
    //        Modify();
    //        Console.WriteLine("\r\nApplication Finished Successfully!!!");
    //        Console.ReadLine();
    //    }
    //    catch (Exception e)
    //    {
    //        Console.WriteLine("\r\nUnexpected exception occured:\r\n\t" +  e.GetType().Name + ":" + e.Message);
    //        Console.ReadLine();
    //    }
    //    finally
    //    {
    //        ldapConnection.Dispose();
    //    }
    //}


    public static void CreateConnection(string UID, string Password, string Host, int Port)
    {
        
        //"uid=b2bsiam-admin,ou=systems,o=b2b,l=america,dc=sony,dc=com", "DW6K3BiG"
        NetworkCredential objCredential = new NetworkCredential(UID,Password);
        
        //LdapDirectoryIdentifier objDirectoryIdentifier = new LdapDirectoryIdentifier("ldap-test.sel.sony.com", 26589);
        //ConfigurationManager.AppSettings["Host"].ToString(), Convert.ToInt16(ConfigurationManager.AppSettings["Port"].ToString())
        LdapDirectoryIdentifier objDirectoryIdentifier = new LdapDirectoryIdentifier(Host,Port);
        
        ldapConnection = new LdapConnection(objDirectoryIdentifier, objCredential);
        
        //X509Certificate objCertificate = new X509Certificate(@"C:\Cert\Entrust.net Secure Server CA.cer");
        //X509Certificate objCertificate = new X509Certificate(@"C:\Cert\CA certs\Test LDAP\IAMTestCA.crt");
        //ldapConnection.ClientCertificates.Add(objCertificate);

        ldapConnection.SessionOptions.SecureSocketLayer = true;
        ldapConnection.SessionOptions.VerifyServerCertificate = new VerifyServerCertificateCallback(ServerCallback);
        ldapConnection.SessionOptions.QueryClientCertificate = new QueryClientCertificateCallback(ClientCallback);
        ldapConnection.AuthType = AuthType.Basic;
        try
        {
            ldapConnection.Bind();
        }
        catch (LdapException eLDAP)
        {
            throw eLDAP;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public static void CloseConnection()
    {
        try
        {
            ldapConnection.Dispose();
        }
        catch (LdapException eLDAP)
        {
            throw eLDAP;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public static void Modify(string AttributeName, string NewValue, string DistinguishedName)
    {
        //ou1 = "uid=100B123682," + targetOU;
        // create a request to modify the object
        DirectoryAttributeModification objDirectoryAttributeModification = new DirectoryAttributeModification();
        objDirectoryAttributeModification.Name = AttributeName;
        objDirectoryAttributeModification.Operation = DirectoryAttributeOperation.Replace;
        
        //"chandra.maurya@am.sony.com"
        objDirectoryAttributeModification.Add(NewValue);
        
        //@"uid=100B123682, ou=users, ou=kbtx-tv, o=B2B, l=america, dc=sony,dc=com"
        ModifyRequest modifyRequest = new ModifyRequest(DistinguishedName ,  objDirectoryAttributeModification);
        
        // send the request through the connection
        try
        {
            ldapConnection.SendRequest(modifyRequest);
        }
        catch (LdapException eLDAP)
        {
            throw eLDAP;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public static Entry[] Search(string SearchFilter, string TargetOU, System.DirectoryServices.Protocols.SearchScope Scope, string[] AttributesToReturn)
    {
        Entry[] collEntry = null;
        //string ldapSearchFilter = "(&(uid=100B123682))";// "(&(americasonyB2BAuthorizedApp=SER_PLUS))"; //"(&(uid=100B123681))";
        //ldapSearchFilter = "uid=100B005450"; 
        // return ALL attributes that have some value
        //string[] attributesToReturn = null;
        //targetOU = "l=america,dc=sony,dc=com";
        // create a search request: specify baseDn, ldap search filter,
        // attributes to return and scope of the search
        SearchRequest searchRequest = new SearchRequest(
                                                        TargetOU,
                                                        SearchFilter,
                                                        Scope, 
                                                        AttributesToReturn
                                                       );


        // send the request through the connection
        try
        {
            SearchResponse searchResponse =
                          (SearchResponse)ldapConnection.SendRequest(searchRequest);

            if (searchResponse != null)
            {
                if (searchResponse.Entries.Count > 0)
                {
                    collEntry = PrintSearchResponse(searchResponse);
                }
            }
            return collEntry;
        }
        catch (LdapException eLDAP)
        {
            throw eLDAP;
        }
        catch (Exception eLDAP)
        {
            throw eLDAP;
        }
    }

    private static Entry[] PrintSearchResponse(SearchResponse searchResponse)
    {
        System.Collections.ArrayList lstEntries = new System.Collections.ArrayList();
        Entry collEntry = new Entry();
        UTF8Encoding utf8 = new UTF8Encoding(false, true);

        string stringvalue= string.Empty;
        // print the returned entries & their attributes
        //Console.WriteLine("\r\nSearch matched " +
        //                            searchResponse.Entries.Count + " entries:");

        int i = 0;
        foreach (SearchResultEntry entry in searchResponse.Entries)
        {
            Console.WriteLine("\r\n" + (++i) + "- " + entry.DistinguishedName);


            // retrieve all attributes
            collEntry = new Entry();
            collEntry.DistinguishedName = entry.DistinguishedName;
            foreach (DirectoryAttribute attr in entry.Attributes.Values)
            {
                //Console.Write(attr.Name + "=");
                //if (attr.Name == "userPassword")
                //{
                //    Console.Write("password");
                //}
                // retrieve all values for the attribute
                // LdapConnection returns everything as 
                // byte[] just as in LDAP C API
                stringvalue = string.Empty;
                foreach (byte[] value in attr)
                {
                    // first check to see if it is a valid UTF8 string
                    try
                    {
                        stringvalue = stringvalue + utf8.GetString(value).ToString();
                    }
                    // it is not a valid UTF8 string
                    // just print raw bytes as hex
                    catch (ArgumentException)
                    {
                        //Console.Write(ByteArrayToHexString(value) + "  ");
                        stringvalue = stringvalue + ByteArrayToHexString(value).ToString();
                    }
                }
                collEntry.SetAttributeValue(attr.Name, stringvalue);

                //Console.WriteLine();
            }
            lstEntries.Add(collEntry);
        }
        Entry[] arrEntries = new Entry[lstEntries.Count];
        lstEntries.CopyTo(arrEntries);
        return arrEntries;
    }
    
    public static bool ServerCallback(LdapConnection connection, X509Certificate certificate)
    {
        X509Certificate2 newCert = new X509Certificate2(certificate);

        if (ConfigurationManager.AppSettings["Verify"].ToString() == "Y")
        {
            return newCert.Verify();
        }
        else
        {
            return true;
        }

    }
    public static X509Certificate ClientCallback(LdapConnection connection, byte[][] trustedCAs)
    {
        //WL("client cert callback called...");
        
        return null;
    }



    #region "Methods"
    //static bool GetParameters(string[] args)
    //{
    //    if (args.Length != 5)
    //    {
    //        Console.WriteLine("Usage: App.exe <ldapServer> <user> <pwd> " +
    //                                                    "<domain> <targetOU>");

    //        // return error 
    //        return false;
    //    }

    //    // initialize variables
    //    ldapServer = args[0];
    //    credential = new NetworkCredential(args[1], args[2], args[3]);
    //    targetOU = args[4];

    //    // return success
    //    return true;
    //}
    //static void Add()
    //{
    //    // create new OUs under the specified OU
    //    ou1 = "OU=sampleOU1," +targetOU;
    //    ou2 = "OU=sampleOU2," +targetOU;
    //    ou3 = "OU=sampleOU3," +targetOU;
    //    string objectClass = "organizationalUnit";
        
        
    //    // create a request to add the new object
    //    AddRequest addRequest = new AddRequest(ou1, objectClass);

    //    // send the request through the connection
    //    ldapConnection.SendRequest(addRequest);

    //    // create a request to add the new object
    //    addRequest = new AddRequest(ou2, objectClass);

    //    // send the request through the connection
    //    ldapConnection.SendRequest(addRequest);

    //    // create a request to add the new object
    //    addRequest = new AddRequest(ou3, objectClass);

    //    // send the request through the connection
    //    ldapConnection.SendRequest(addRequest);
        
    //    Console.WriteLine("Objects are created successfully.");
    //}
    //static void Rename()
    //{
    //    // keep the parent container same to do just rename without moving
    //    string newParent = targetOU;
    //    string newName = "OU=sampleOUNew";

    //    // create a request to rename the object
    //    ModifyDNRequest modDnRequest = new ModifyDNRequest(
    //                                                        ou1,
    //                                                        newParent, 
    //                                                        newName
    //                                                      );

    //    // send the request through the connection
    //    ldapConnection.SendRequest(modDnRequest);

    //    // this is the new path of the object after rename operation
    //    ou1 = "OU=sampleOUNew," + targetOU;
    //    Console.WriteLine("Object has been renamed successfully." );
    //}
    //static void Move()
    //{
    //    // move sampleOU3 under sampleOU2
    //    string newParent = ou2;

    //    // keep the name same to just move without renaming
    //    string newName = "OU=sampleOU3";

    //    // create a request to move the object
    //    ModifyDNRequest modDnRequest = new ModifyDNRequest(
    //                                                        ou3,
    //                                                        newParent, 
    //                                                        newName
    //                                                      );

    //    // send the request through the connection
    //    ldapConnection.SendRequest(modDnRequest);

    //    Console.WriteLine("Object is moved successfully." );                
    //}
    //static void Compare()
    //{
    //    string attributeName = "description";
    //    string valueToCompareAgainst = "This is a sample OU";

    //    // create a request to do compare on the object
    //    CompareRequest compareRequest = new CompareRequest(
    //                                                       ou1, 
    //                                                       attributeName, 
    //                                                       valueToCompareAgainst
    //                                                      );

    //    // send the request through the connection
    //    CompareResponse compareResponse = 
    //                (CompareResponse)ldapConnection.SendRequest(compareRequest);

    //    Console.WriteLine("The result of the comparison is:" + 
    //                                                compareResponse.ResultCode);
    //}
    public static string ByteArrayToHexString(byte[] bytes)
    {
        if(bytes == null) return "";
        StringBuilder s = new StringBuilder(bytes.Length/2);
        s.Append("0x");
        foreach(byte b in bytes)
        {        
            s.Append(String.Format("{0:X2}",b));
        }        
        return s.ToString();
    }
    //static void DeleteLeafObject()
    //{
    //    // create a request to delete the object (ou1 is a leaf object)
    //    DeleteRequest deleteRequest = new DeleteRequest(ou1);

    //    // send the request through the connection
    //    ldapConnection.SendRequest(deleteRequest);

    //    Console.WriteLine("Leaf object is deleted successfully.");
    //}
    //static void DeleteTree()
    //{
    //    // create a request to delete the object 
    //    // (ou2 is not a leaf object since it has child objects)
    //    DeleteRequest deleteRequest = new DeleteRequest(ou2);
        
    //    // add a tree-delete control to the request
    //    deleteRequest.Controls.Add(new TreeDeleteControl());

    //    // send the request through the connection
    //    ldapConnection.SendRequest(deleteRequest);

    //    Console.WriteLine("Object tree is deleted successfully.");
    //}

    #endregion
}// end class App

}
