using System;

namespace WinLDAP
{
	public struct WinLDAPSearchResults
	{
		public int ReturnCode;
		public string Message;
		public WinLDAP.Entry[] LDAPEntries;
	}
}
