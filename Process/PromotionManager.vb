Imports System.Xml
Imports System.Xml.Schema
Imports System.Xml.XPath
Imports Sony.US.ServicesPLUS.Data
Imports Sony.US.ServicesPLUS.Core

Public Class PromotionManager

    Public Function GetCurrentPromotions(ByVal type As Product.ProductType) As Promotion()
        Dim data_store As New PromotionDataManager
        Dim obj() As Promotion

        Try
            obj = data_store.GetCurrentPromotions(type)
        Catch ex As Exception
            Throw ex
        End Try
        Return obj
    End Function

    Public Function GetInternetPromotionParts(ByRef objGlobalData As GlobalData) As PromotionPart()
        Dim data_store As New PromotionDataManager
        Dim obj() As PromotionPart

        Try 'Sasikumar WP SPLUS_WP007
            obj = data_store.GetInternetPromotionParts(objGlobalData)
        Catch ex As Exception
            Throw ex
        End Try
        Return obj
    End Function

    Public Function GetInternetPromotionKits() As PromotionKit()
        Dim data_store As New PromotionDataManager
        Dim obj() As PromotionKit

        Try
            obj = data_store.GetPromotionKits()
        Catch ex As Exception
            Throw ex
        End Try
        Return obj
    End Function

    Public Function GetPartInPromotion(ByVal partName As String) As Part
        Dim data_store As New PromotionDataManager
        Dim obj As Part
        Try
            obj = data_store.GetPartInPromotion(partName)
        Catch ex As Exception
            Throw ex
        End Try
        Return obj
    End Function

    Public Function GetPartsInPromotion() As Part()
        Dim data_store As New PromotionDataManager
        Dim obj() As Part
        Try
            obj = data_store.GetPartsInPromotion()
        Catch ex As Exception
            Throw ex
        End Try
        Return obj
    End Function

    Public Function GetKitsInPromotion() As Kit()
        Dim data_store As New PromotionDataManager
        Dim obj() As Kit

        Try
            obj = data_store.GetKitsInPromotion()
        Catch ex As Exception
            Throw ex
        End Try
        Return obj
    End Function
    Public Function GetKitInPromotion(ByVal kitName As String) As Kit
        Dim data_store As New PromotionDataManager
        Dim obj As Kit

        Try
            obj = data_store.GetKitInPromotion(kitName)
        Catch ex As Exception
            Throw ex
        End Try
        Return obj
    End Function

    Public Function IsPromotionPart(ByVal partNumber As String) As Boolean
        Dim data_store As New PromotionDataManager
        Dim IsValidID As Boolean
        Dim txn_context As TransactionContext = Nothing

        Try
            txn_context = data_store.BeginTransaction
            IsValidID = data_store.IsPromotionPart(txn_context, partNumber)
            data_store.CommitTransaction(txn_context)
        Catch ex As Exception
            data_store.RollbackTransaction(txn_context)
            Throw ex
        End Try
        Return IsValidID
    End Function

    Public Function GetAdvertisement(ByRef promotionConfigFile As String, ByRef displayPage As String, ByRef effectiveDate As Date) As Advertisement
        'Dim xtr As XmlTextReader
        Dim promotionConfigXPD As XPathDocument
        Try
            'xtr = New XmlTextReader(promotionConfigFile)
            'Dim promotionConfigReader As XmlValidatingReader = New XmlValidatingReader(xtr)
            'promotionConfigReader.ValidationType = ValidationType.Schema
            'AddHandler promotionConfigReader.ValidationEventHandler, AddressOf Me.PromotionConfigValidationHandler
            promotionConfigXPD = New XPathDocument(promotionConfigFile)
            Dim xpn As XPathNavigator = promotionConfigXPD.CreateNavigator()
            Dim selectStr As String = "//Promotion[@DisplayControl=""" + displayPage + """]"
            Dim xpni As XPathNodeIterator = xpn.Select(selectStr)
            Dim strEffectiveDate As String
            Dim strURL As String
            Dim strPopup As String
            Dim bPopup As Boolean = False
            Dim intType As Short
            Dim strAltText As String
            Dim advertisement As Advertisement = Nothing

            While xpni.MoveNext
                strEffectiveDate = xpni.Current.GetAttribute("EffectiveDate", xpn.NamespaceURI)
                If strEffectiveDate <> "" Then
                    If Date.Parse(strEffectiveDate) >= effectiveDate Then
                        strURL = xpni.Current.GetAttribute("URL", xpn.NamespaceURI)
                        strAltText = xpni.Current.GetAttribute("AltText", xpn.NamespaceURI)
                        strPopup = xpni.Current.GetAttribute("Popup", xpn.NamespaceURI)
                        intType = Convert.ToInt16(xpni.Current.GetAttribute("Type", xpn.NamespaceURI))
                        If Not strPopup Is Nothing And strPopup.ToLower() = "true" Then
                            bPopup = True
                        End If
                        advertisement = New Advertisement(xpni.Current.Value, strURL, strAltText, bPopup, intType)
                        Exit While 'only the first one
                    End If
                Else 'no effective date attribute means effective all the time
                    strURL = xpni.Current.GetAttribute("URL", xpn.NamespaceURI)
                    strAltText = xpni.Current.GetAttribute("AltText", xpn.NamespaceURI)
                    intType = Convert.ToInt16(xpni.Current.GetAttribute("Type", xpn.NamespaceURI))
                    strPopup = xpni.Current.GetAttribute("Popup", xpn.NamespaceURI)
                    If Not strPopup Is Nothing And strPopup.ToLower() = "true" Then
                        bPopup = True
                    End If
                    advertisement = New Advertisement(xpni.Current.Value, strURL, strAltText, bPopup, intType)
                    Exit While 'only the first one
                End If
            End While
            Return advertisement
        Catch ex As Exception
            Throw ex
        Finally
            'If Not xtr Is Nothing Then
            'xtr.Close()
            'End If
        End Try
    End Function
    Public Function GetAdvertisement(ByRef promotionConfigFile As String, ByRef displayPage As String) As Advertisement
        Dim promotionConfigXPD As XPathDocument
        Try
            promotionConfigXPD = New XPathDocument(promotionConfigFile)
            Dim xpn As XPathNavigator = promotionConfigXPD.CreateNavigator()
            Dim selectStr As String = "//Promotion[@DisplayControl=""" + displayPage + """]"
            Dim xpni As XPathNodeIterator = xpn.Select(selectStr)
            Dim strURL As String
            Dim strAltText As String
            Dim strPopup As String
            Dim intType As Short = 0
            Dim bPopup As Boolean = False
            Dim advertisement As Advertisement = Nothing

            While xpni.MoveNext
                strURL = xpni.Current.GetAttribute("URL", xpn.NamespaceURI)
                strAltText = xpni.Current.GetAttribute("AltText", xpn.NamespaceURI)
                strPopup = xpni.Current.GetAttribute("Popup", xpn.NamespaceURI)
                If Not strPopup Is Nothing And strPopup.ToLower() = "true" Then
                    bPopup = True
                End If
                advertisement = New Advertisement(xpni.Current.Value, strURL, strAltText, bPopup, intType)
                Exit While 'only the first one
            End While
            Return advertisement
        Catch ex As Exception
            Throw ex
        Finally
            'If Not xtr Is Nothing Then
            'xtr.Close()
            'End If
        End Try
    End Function

    Public Function GetAdvertisement(ByRef promotionConfigFile As String, ByRef displayPage As String, ByVal Type As Short) As Advertisement
        Dim promotionConfigXPD As XPathDocument
        Try
            promotionConfigXPD = New XPathDocument(promotionConfigFile)
            Dim xpn As XPathNavigator = promotionConfigXPD.CreateNavigator()
            Dim selectStr As String = "//Promotion[@DisplayControl=""" + displayPage + """][@Type=""" + Type.ToString() + """]"
            Dim xpni As XPathNodeIterator = xpn.Select(selectStr)
            Dim strURL As String
            Dim strAltText As String
            Dim strPopup As String
            Dim intType As Short
            Dim bPopup As Boolean = False
            Dim advertisement As Advertisement = Nothing

            While xpni.MoveNext
                strURL = xpni.Current.GetAttribute("URL", xpn.NamespaceURI)
                strAltText = xpni.Current.GetAttribute("AltText", xpn.NamespaceURI)
                strPopup = xpni.Current.GetAttribute("Popup", xpn.NamespaceURI)
                intType = Convert.ToInt16(xpni.Current.GetAttribute("Type", xpn.NamespaceURI))
                If Not strPopup Is Nothing And strPopup.ToLower() = "true" Then
                    bPopup = True
                End If
                advertisement = New Advertisement(xpni.Current.Value, strURL, strAltText, bPopup, intType)
                Exit While 'only the first one
            End While
            Return advertisement
        Catch ex As Exception
            Throw ex
        Finally
            'If Not xtr Is Nothing Then
            'xtr.Close()
            'End If
        End Try
    End Function

    Public Function GetAdvertisementInserts(ByRef promotionConfigFile As String, ByRef effectiveDate As Date) As Advertisement()
        'Dim xtr As XmlTextReader
        Dim promotionConfigXPD As XPathDocument
        Dim promotionInsertList As ArrayList = New ArrayList
        Try
            'xtr = New XmlTextReader(promotionConfigFile)
            'Dim promotionConfigReader As XmlValidatingReader = New XmlValidatingReader(xtr)
            'promotionConfigReader.ValidationType = ValidationType.Schema
            'AddHandler promotionConfigReader.ValidationEventHandler, AddressOf Me.PromotionConfigValidationHandler
            promotionConfigXPD = New XPathDocument(promotionConfigFile)
            Dim xpn As XPathNavigator = promotionConfigXPD.CreateNavigator()
            Dim selectStr As String = "//PromotionInsert"
            Dim xpni As XPathNodeIterator = xpn.Select(selectStr)
            Dim strEffectiveDate As String
            Dim advertisement As Advertisement

            While xpni.MoveNext
                strEffectiveDate = xpni.Current.GetAttribute("EffectiveDate", xpn.NamespaceURI)
                If strEffectiveDate <> "" Then
                    If Date.Parse(strEffectiveDate) >= effectiveDate Then
                        advertisement = New Advertisement("", xpni.Current.Value, "", False, 0)
                        promotionInsertList.AddRange(advertisement)
                    End If
                Else 'no effective date attribute means effective all the time
                    advertisement = New Advertisement("", xpni.Current.Value, "", False, 0)
                    promotionInsertList.Add(advertisement)
                End If
            End While
            Dim advertisementInserts(promotionInsertList.Count - 1) As Advertisement
            promotionInsertList.CopyTo(advertisementInserts)
            Return advertisementInserts
        Catch ex As Exception
            Throw ex
        Finally
            'If Not xtr Is Nothing Then
            'xtr.Close()
            'End If
        End Try
    End Function
    Public Function GetAdvertisementInserts(ByRef promotionConfigFile As String) As Advertisement()
        'Dim xtr As XmlTextReader
        Dim promotionConfigXPD As XPathDocument
        Dim promotionInsertList As ArrayList = New ArrayList
        Try
            'xtr = New XmlTextReader(promotionConfigFile)
            'Dim promotionConfigReader As XmlValidatingReader = New XmlValidatingReader(xtr)
            'promotionConfigReader.ValidationType = ValidationType.Schema
            'AddHandler promotionConfigReader.ValidationEventHandler, AddressOf Me.PromotionConfigValidationHandler
            promotionConfigXPD = New XPathDocument(promotionConfigFile)
            Dim xpn As XPathNavigator = promotionConfigXPD.CreateNavigator()
            Dim selectStr As String = "//PromotionInsert"
            Dim xpni As XPathNodeIterator = xpn.Select(selectStr)
            Dim advertisement As Advertisement

            While xpni.MoveNext
                advertisement = New Advertisement("", xpni.Current.Value, "", False, 0)
                promotionInsertList.Add(advertisement)
            End While
            Dim advertisementInserts(promotionInsertList.Count - 1) As Advertisement
            promotionInsertList.CopyTo(advertisementInserts)
            Return advertisementInserts
        Catch ex As Exception
            Throw ex
        Finally
            'If Not xtr Is Nothing Then
            'xtr.Close()
            'End If
        End Try
    End Function

    Private Sub PromotionConfigValidationHandler(ByVal sender As Object, ByVal e As ValidationEventArgs)
        Throw New Exception(e.Message)
    End Sub
End Class
