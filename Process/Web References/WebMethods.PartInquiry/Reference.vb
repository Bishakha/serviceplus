﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'     Runtime Version:4.0.30319.42000
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated.
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict Off
Option Explicit On

Imports System
Imports System.ComponentModel
Imports System.Diagnostics
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Xml.Serialization

'
'This source code was auto-generated by Microsoft.VSDesigner, Version 4.0.30319.42000.
'
Namespace WebMethods.PartInquiry
    
    '''<remarks/>
    <System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.7.3062.0"),  _
     System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.ComponentModel.DesignerCategoryAttribute("code"),  _
     System.Web.Services.WebServiceBindingAttribute(Name:="SelPartInquiry_WebServices_Provider_partInquiry_1_Binder", [Namespace]:="http://ubedua702/SelPartInquiry/WebServices/Provider/partInquiry_1")>  _
    Partial Public Class partInquiry
        Inherits System.Web.Services.Protocols.SoapHttpClientProtocol
        
        Private ProcessPartInquiryResponseOperationCompleted As System.Threading.SendOrPostCallback
        
        Private useDefaultCredentialsSetExplicitly As Boolean
        
        '''<remarks/>
        Public Sub New()
            MyBase.New
            Me.Url = Global.Sony.US.ServicesPLUS.Process.My.MySettings.Default.Sony_US_ServicesPLUS_Process_WebMethods_PartInquiry_partInquiry
            If (Me.IsLocalFileSystemWebService(Me.Url) = true) Then
                Me.UseDefaultCredentials = true
                Me.useDefaultCredentialsSetExplicitly = false
            Else
                Me.useDefaultCredentialsSetExplicitly = true
            End If
        End Sub
        
        Public Shadows Property Url() As String
            Get
                Return MyBase.Url
            End Get
            Set
                If (((Me.IsLocalFileSystemWebService(MyBase.Url) = true)  _
                            AndAlso (Me.useDefaultCredentialsSetExplicitly = false))  _
                            AndAlso (Me.IsLocalFileSystemWebService(value) = false)) Then
                    MyBase.UseDefaultCredentials = false
                End If
                MyBase.Url = value
            End Set
        End Property
        
        Public Shadows Property UseDefaultCredentials() As Boolean
            Get
                Return MyBase.UseDefaultCredentials
            End Get
            Set
                MyBase.UseDefaultCredentials = value
                Me.useDefaultCredentialsSetExplicitly = true
            End Set
        End Property
        
        '''<remarks/>
        Public Event ProcessPartInquiryResponseCompleted As ProcessPartInquiryResponseCompletedEventHandler
        
        '''<remarks/>
        <System.Web.Services.Protocols.SoapDocumentMethodAttribute("SelPartInquiry_WebServices_Provider_partInquiry_1_Binder_ProcessPartInquiryRespon"& _ 
            "se", RequestNamespace:="http://ubedua702/SelPartInquiry/WebServices/Provider/partInquiry_1", ResponseNamespace:="http://ubedua702/SelPartInquiry/WebServices/Provider/partInquiry_1", Use:=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle:=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)>  _
        Public Function ProcessPartInquiryResponse(<System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable:=true)> ByVal PartInquiryRequest As PartInquiryRequest) As <System.Xml.Serialization.XmlElementAttribute("PartInquiryResponse", Form:=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable:=true)> PartInquiryResponse
            Dim results() As Object = Me.Invoke("ProcessPartInquiryResponse", New Object() {PartInquiryRequest})
            Return CType(results(0),PartInquiryResponse)
        End Function
        
        '''<remarks/>
        Public Overloads Sub ProcessPartInquiryResponseAsync(ByVal PartInquiryRequest As PartInquiryRequest)
            Me.ProcessPartInquiryResponseAsync(PartInquiryRequest, Nothing)
        End Sub
        
        '''<remarks/>
        Public Overloads Sub ProcessPartInquiryResponseAsync(ByVal PartInquiryRequest As PartInquiryRequest, ByVal userState As Object)
            If (Me.ProcessPartInquiryResponseOperationCompleted Is Nothing) Then
                Me.ProcessPartInquiryResponseOperationCompleted = AddressOf Me.OnProcessPartInquiryResponseOperationCompleted
            End If
            Me.InvokeAsync("ProcessPartInquiryResponse", New Object() {PartInquiryRequest}, Me.ProcessPartInquiryResponseOperationCompleted, userState)
        End Sub
        
        Private Sub OnProcessPartInquiryResponseOperationCompleted(ByVal arg As Object)
            If (Not (Me.ProcessPartInquiryResponseCompletedEvent) Is Nothing) Then
                Dim invokeArgs As System.Web.Services.Protocols.InvokeCompletedEventArgs = CType(arg,System.Web.Services.Protocols.InvokeCompletedEventArgs)
                RaiseEvent ProcessPartInquiryResponseCompleted(Me, New ProcessPartInquiryResponseCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState))
            End If
        End Sub
        
        '''<remarks/>
        Public Shadows Sub CancelAsync(ByVal userState As Object)
            MyBase.CancelAsync(userState)
        End Sub
        
        Private Function IsLocalFileSystemWebService(ByVal url As String) As Boolean
            If ((url Is Nothing)  _
                        OrElse (url Is String.Empty)) Then
                Return false
            End If
            Dim wsUri As System.Uri = New System.Uri(url)
            If ((wsUri.Port >= 1024)  _
                        AndAlso (String.Compare(wsUri.Host, "localHost", System.StringComparison.OrdinalIgnoreCase) = 0)) Then
                Return true
            End If
            Return false
        End Function
    End Class
    
    '''<remarks/>
    <System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.7.3221.0"),  _
     System.SerializableAttribute(),  _
     System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.ComponentModel.DesignerCategoryAttribute("code"),  _
     System.Xml.Serialization.XmlTypeAttribute([Namespace]:="http://ubedua702/SelPartInquiry/WebServices/Provider/partInquiry_1")>  _
    Partial Public Class PartInquiryRequest
        
        Private partNumberField() As PartNumber
        
        Private locationField As String
        
        Private sapAcNumberField As String
        
        Private inventoryInfoField As String
        
        Private priceInfoField As String
        
        Private salesOrganizationField As String
        
        Private distributionChannelField As String
        
        Private divisionField As String
        
        Private languageField As String
        
        '''<remarks/>
        <System.Xml.Serialization.XmlArrayAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable:=true),  _
         System.Xml.Serialization.XmlArrayItemAttribute("ArrayOfPartNumberItem", Form:=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable:=false)>  _
        Public Property PartNumber() As PartNumber()
            Get
                Return Me.partNumberField
            End Get
            Set
                Me.partNumberField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable:=true)>  _
        Public Property Location() As String
            Get
                Return Me.locationField
            End Get
            Set
                Me.locationField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable:=true)>  _
        Public Property SapAcNumber() As String
            Get
                Return Me.sapAcNumberField
            End Get
            Set
                Me.sapAcNumberField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable:=true)>  _
        Public Property InventoryInfo() As String
            Get
                Return Me.inventoryInfoField
            End Get
            Set
                Me.inventoryInfoField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable:=true)>  _
        Public Property PriceInfo() As String
            Get
                Return Me.priceInfoField
            End Get
            Set
                Me.priceInfoField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable:=true)>  _
        Public Property SalesOrganization() As String
            Get
                Return Me.salesOrganizationField
            End Get
            Set
                Me.salesOrganizationField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable:=true)>  _
        Public Property DistributionChannel() As String
            Get
                Return Me.distributionChannelField
            End Get
            Set
                Me.distributionChannelField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable:=true)>  _
        Public Property Division() As String
            Get
                Return Me.divisionField
            End Get
            Set
                Me.divisionField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable:=true)>  _
        Public Property Language() As String
            Get
                Return Me.languageField
            End Get
            Set
                Me.languageField = value
            End Set
        End Property
    End Class
    
    '''<remarks/>
    <System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.7.3221.0"),  _
     System.SerializableAttribute(),  _
     System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.ComponentModel.DesignerCategoryAttribute("code"),  _
     System.Xml.Serialization.XmlTypeAttribute([Namespace]:="http://ubedua702/SelPartInquiry/WebServices/Provider/partInquiry_1")>  _
    Partial Public Class PartNumber
        
        Private partNumber1Field As String
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute("PartNumber", Form:=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable:=true)>  _
        Public Property PartNumber1() As String
            Get
                Return Me.partNumber1Field
            End Get
            Set
                Me.partNumber1Field = value
            End Set
        End Property
    End Class
    
    '''<remarks/>
    <System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.7.3221.0"),  _
     System.SerializableAttribute(),  _
     System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.ComponentModel.DesignerCategoryAttribute("code"),  _
     System.Xml.Serialization.XmlTypeAttribute([Namespace]:="http://ubedua702/SelPartInquiry/WebServices/Provider/partInquiry_1")>  _
    Partial Public Class Kitinfo
        
        Private itemNumberField As String
        
        Private statusFlagField As String
        
        Private statusMessageField As String
        
        Private itemQuantityField As String
        
        Private descriptionField As String
        
        Private availabilityField As String
        
        Private itemListPriceField As String
        
        Private itemYourPriceField As String
        
        Private categoryField As String
        
        Private oUTPUT1AField As String
        
        Private oUTPUT2AField As String
        
        Private oUTPUT3AField As String
        
        Private oUTPUT4AField As String
        
        Private oUTPUT5AField As String
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable:=true)>  _
        Public Property ItemNumber() As String
            Get
                Return Me.itemNumberField
            End Get
            Set
                Me.itemNumberField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable:=true)>  _
        Public Property StatusFlag() As String
            Get
                Return Me.statusFlagField
            End Get
            Set
                Me.statusFlagField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable:=true)>  _
        Public Property StatusMessage() As String
            Get
                Return Me.statusMessageField
            End Get
            Set
                Me.statusMessageField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable:=true)>  _
        Public Property ItemQuantity() As String
            Get
                Return Me.itemQuantityField
            End Get
            Set
                Me.itemQuantityField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable:=true)>  _
        Public Property Description() As String
            Get
                Return Me.descriptionField
            End Get
            Set
                Me.descriptionField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable:=true)>  _
        Public Property Availability() As String
            Get
                Return Me.availabilityField
            End Get
            Set
                Me.availabilityField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable:=true)>  _
        Public Property ItemListPrice() As String
            Get
                Return Me.itemListPriceField
            End Get
            Set
                Me.itemListPriceField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable:=true)>  _
        Public Property ItemYourPrice() As String
            Get
                Return Me.itemYourPriceField
            End Get
            Set
                Me.itemYourPriceField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable:=true)>  _
        Public Property Category() As String
            Get
                Return Me.categoryField
            End Get
            Set
                Me.categoryField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable:=true)>  _
        Public Property OUTPUT1A() As String
            Get
                Return Me.oUTPUT1AField
            End Get
            Set
                Me.oUTPUT1AField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable:=true)>  _
        Public Property OUTPUT2A() As String
            Get
                Return Me.oUTPUT2AField
            End Get
            Set
                Me.oUTPUT2AField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable:=true)>  _
        Public Property OUTPUT3A() As String
            Get
                Return Me.oUTPUT3AField
            End Get
            Set
                Me.oUTPUT3AField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable:=true)>  _
        Public Property OUTPUT4A() As String
            Get
                Return Me.oUTPUT4AField
            End Get
            Set
                Me.oUTPUT4AField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable:=true)>  _
        Public Property OUTPUT5A() As String
            Get
                Return Me.oUTPUT5AField
            End Get
            Set
                Me.oUTPUT5AField = value
            End Set
        End Property
    End Class
    
    '''<remarks/>
    <System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.7.3221.0"),  _
     System.SerializableAttribute(),  _
     System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.ComponentModel.DesignerCategoryAttribute("code"),  _
     System.Xml.Serialization.XmlTypeAttribute([Namespace]:="http://ubedua702/SelPartInquiry/WebServices/Provider/partInquiry_1")>  _
    Partial Public Class PartInquiryResponse2
        
        Private numberAvailableField As String
        
        Private newPartNumberField As String
        
        Private replacementPartNumberField As String
        
        Private internalPriceField As String
        
        Private effectiveDateField As String
        
        Private lastDeliveryDateField As String
        
        Private item2ndLastDeliveryDateField As String
        
        Private lastDeliveryShippingCodeField As String
        
        Private deliveryQtyField As String
        
        Private item2ndLastShippingCodeField As String
        
        Private deliveryQty2Field As String
        
        Private lastSaleDateField As String
        
        Private lastReceivedDateField As String
        
        Private pOPendingQtyField As String
        
        Private onOrderQtyField As String
        
        Private intransitQtyField As String
        
        Private consumptionTotalField As String
        
        Private warningErrorField As String
        
        Private partDescriptionField As String
        
        Private errorMessageField As String
        
        Private errorNumberField As String
        
        Private replacementField As String
        
        Private recyclingFlagField As String
        
        Private programCodeField As String
        
        Private discountCodeField As String
        
        Private discountPercentageField As String
        
        Private coreChargeField As String
        
        Private listPriceField As String
        
        Private yourPriceField As String
        
        Private itemShippedFromField As String
        
        Private billingOnlyField As String
        
        Private oUTPUT1Field As String
        
        Private oUTPUT2Field As String
        
        Private oUTPUT3Field As String
        
        Private oUTPUT4Field As String
        
        Private oUTPUT5Field As String
        
        Private kitinfoField() As Kitinfo
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable:=true)>  _
        Public Property NumberAvailable() As String
            Get
                Return Me.numberAvailableField
            End Get
            Set
                Me.numberAvailableField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable:=true)>  _
        Public Property NewPartNumber() As String
            Get
                Return Me.newPartNumberField
            End Get
            Set
                Me.newPartNumberField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable:=true)>  _
        Public Property ReplacementPartNumber() As String
            Get
                Return Me.replacementPartNumberField
            End Get
            Set
                Me.replacementPartNumberField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable:=true)>  _
        Public Property InternalPrice() As String
            Get
                Return Me.internalPriceField
            End Get
            Set
                Me.internalPriceField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable:=true)>  _
        Public Property EffectiveDate() As String
            Get
                Return Me.effectiveDateField
            End Get
            Set
                Me.effectiveDateField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable:=true)>  _
        Public Property LastDeliveryDate() As String
            Get
                Return Me.lastDeliveryDateField
            End Get
            Set
                Me.lastDeliveryDateField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute("2ndLastDeliveryDate", Form:=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable:=true)>  _
        Public Property Item2ndLastDeliveryDate() As String
            Get
                Return Me.item2ndLastDeliveryDateField
            End Get
            Set
                Me.item2ndLastDeliveryDateField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable:=true)>  _
        Public Property LastDeliveryShippingCode() As String
            Get
                Return Me.lastDeliveryShippingCodeField
            End Get
            Set
                Me.lastDeliveryShippingCodeField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable:=true)>  _
        Public Property DeliveryQty() As String
            Get
                Return Me.deliveryQtyField
            End Get
            Set
                Me.deliveryQtyField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute("2ndLastShippingCode", Form:=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable:=true)>  _
        Public Property Item2ndLastShippingCode() As String
            Get
                Return Me.item2ndLastShippingCodeField
            End Get
            Set
                Me.item2ndLastShippingCodeField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable:=true)>  _
        Public Property DeliveryQty2() As String
            Get
                Return Me.deliveryQty2Field
            End Get
            Set
                Me.deliveryQty2Field = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable:=true)>  _
        Public Property LastSaleDate() As String
            Get
                Return Me.lastSaleDateField
            End Get
            Set
                Me.lastSaleDateField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable:=true)>  _
        Public Property LastReceivedDate() As String
            Get
                Return Me.lastReceivedDateField
            End Get
            Set
                Me.lastReceivedDateField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable:=true)>  _
        Public Property POPendingQty() As String
            Get
                Return Me.pOPendingQtyField
            End Get
            Set
                Me.pOPendingQtyField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable:=true)>  _
        Public Property OnOrderQty() As String
            Get
                Return Me.onOrderQtyField
            End Get
            Set
                Me.onOrderQtyField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable:=true)>  _
        Public Property IntransitQty() As String
            Get
                Return Me.intransitQtyField
            End Get
            Set
                Me.intransitQtyField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable:=true)>  _
        Public Property ConsumptionTotal() As String
            Get
                Return Me.consumptionTotalField
            End Get
            Set
                Me.consumptionTotalField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable:=true)>  _
        Public Property WarningError() As String
            Get
                Return Me.warningErrorField
            End Get
            Set
                Me.warningErrorField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable:=true)>  _
        Public Property PartDescription() As String
            Get
                Return Me.partDescriptionField
            End Get
            Set
                Me.partDescriptionField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable:=true)>  _
        Public Property ErrorMessage() As String
            Get
                Return Me.errorMessageField
            End Get
            Set
                Me.errorMessageField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable:=true)>  _
        Public Property ErrorNumber() As String
            Get
                Return Me.errorNumberField
            End Get
            Set
                Me.errorNumberField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable:=true)>  _
        Public Property Replacement() As String
            Get
                Return Me.replacementField
            End Get
            Set
                Me.replacementField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable:=true)>  _
        Public Property RecyclingFlag() As String
            Get
                Return Me.recyclingFlagField
            End Get
            Set
                Me.recyclingFlagField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable:=true)>  _
        Public Property ProgramCode() As String
            Get
                Return Me.programCodeField
            End Get
            Set
                Me.programCodeField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable:=true)>  _
        Public Property DiscountCode() As String
            Get
                Return Me.discountCodeField
            End Get
            Set
                Me.discountCodeField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable:=true)>  _
        Public Property DiscountPercentage() As String
            Get
                Return Me.discountPercentageField
            End Get
            Set
                Me.discountPercentageField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable:=true)>  _
        Public Property CoreCharge() As String
            Get
                Return Me.coreChargeField
            End Get
            Set
                Me.coreChargeField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable:=true)>  _
        Public Property ListPrice() As String
            Get
                Return Me.listPriceField
            End Get
            Set
                Me.listPriceField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable:=true)>  _
        Public Property YourPrice() As String
            Get
                Return Me.yourPriceField
            End Get
            Set
                Me.yourPriceField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable:=true)>  _
        Public Property ItemShippedFrom() As String
            Get
                Return Me.itemShippedFromField
            End Get
            Set
                Me.itemShippedFromField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable:=true)>  _
        Public Property BillingOnly() As String
            Get
                Return Me.billingOnlyField
            End Get
            Set
                Me.billingOnlyField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable:=true)>  _
        Public Property OUTPUT1() As String
            Get
                Return Me.oUTPUT1Field
            End Get
            Set
                Me.oUTPUT1Field = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable:=true)>  _
        Public Property OUTPUT2() As String
            Get
                Return Me.oUTPUT2Field
            End Get
            Set
                Me.oUTPUT2Field = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable:=true)>  _
        Public Property OUTPUT3() As String
            Get
                Return Me.oUTPUT3Field
            End Get
            Set
                Me.oUTPUT3Field = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable:=true)>  _
        Public Property OUTPUT4() As String
            Get
                Return Me.oUTPUT4Field
            End Get
            Set
                Me.oUTPUT4Field = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable:=true)>  _
        Public Property OUTPUT5() As String
            Get
                Return Me.oUTPUT5Field
            End Get
            Set
                Me.oUTPUT5Field = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlArrayAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable:=true),  _
         System.Xml.Serialization.XmlArrayItemAttribute("ArrayOfKitinfoItem", Form:=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable:=false)>  _
        Public Property Kitinfo() As Kitinfo()
            Get
                Return Me.kitinfoField
            End Get
            Set
                Me.kitinfoField = value
            End Set
        End Property
    End Class
    
    '''<remarks/>
    <System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.7.3221.0"),  _
     System.SerializableAttribute(),  _
     System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.ComponentModel.DesignerCategoryAttribute("code"),  _
     System.Xml.Serialization.XmlTypeAttribute([Namespace]:="http://ubedua702/SelPartInquiry/WebServices/Provider/partInquiry_1")>  _
    Partial Public Class PartInquiryResponse
        
        Private partInquiryResponse1Field() As PartInquiryResponse2
        
        '''<remarks/>
        <System.Xml.Serialization.XmlArrayAttribute("PartInquiryResponse", Form:=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable:=true),  _
         System.Xml.Serialization.XmlArrayItemAttribute("ArrayOfPartInquiryResponseItem", Form:=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable:=false)>  _
        Public Property PartInquiryResponse1() As PartInquiryResponse2()
            Get
                Return Me.partInquiryResponse1Field
            End Get
            Set
                Me.partInquiryResponse1Field = value
            End Set
        End Property
    End Class
    
    '''<remarks/>
    <System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.7.3062.0")>  _
    Public Delegate Sub ProcessPartInquiryResponseCompletedEventHandler(ByVal sender As Object, ByVal e As ProcessPartInquiryResponseCompletedEventArgs)
    
    '''<remarks/>
    <System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.7.3062.0"),  _
     System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.ComponentModel.DesignerCategoryAttribute("code")>  _
    Partial Public Class ProcessPartInquiryResponseCompletedEventArgs
        Inherits System.ComponentModel.AsyncCompletedEventArgs
        
        Private results() As Object
        
        Friend Sub New(ByVal results() As Object, ByVal exception As System.Exception, ByVal cancelled As Boolean, ByVal userState As Object)
            MyBase.New(exception, cancelled, userState)
            Me.results = results
        End Sub
        
        '''<remarks/>
        Public ReadOnly Property Result() As PartInquiryResponse
            Get
                Me.RaiseExceptionIfNecessary
                Return CType(Me.results(0),PartInquiryResponse)
            End Get
        End Property
    End Class
End Namespace
