Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Data
Imports System.Collections.Generic

Public Class TechBulletinManager
    '------- get all tech bulletins

    Public Function searchTechBulletins(ByVal searchString As String) As TechBulletin()
        '------- get tech bulletins based on search criterion 
        Dim data_store As New TechBulletinDataManager
        Dim techBulletins As TechBulletin()
        Try
            techBulletins = data_store.GetTechBulletin(searchString)
        Catch ex As Exception
            Throw ex
        End Try
        Return techBulletins
    End Function

    Public Function GetKnowledgeBaseSolution(ByVal SolutionID As String) As DataSet
        Dim data_store As New TechBulletinDataManager
        Dim Solution As DataSet
        Try
            Solution = data_store.GetKnowledgeBaseSolution(SolutionID)
        Catch ex As Exception
            Throw ex
        End Try
        Return Solution
    End Function

    Public Function LogKBSearchResult(ByVal TBINCLUDED As String, ByVal ID As String, ByVal Model As String, ByVal SerialNumber As Integer, ByVal Keyword As String, ByVal DateFrom As Date, ByVal DateTo As Date, ByVal Hits As Integer, ByVal xCUSTOMERID As Integer _
            , ByVal xCUSTOMERSEQUENCENUMBER As Integer, ByVal xHTTP_X_FORWARDED_FOR As String, ByVal xREMOTE_ADDR As String, ByVal xHTTP_REFERER As String _
            , ByVal xHTTP_URL As String, ByVal xHTTP_USER_AGENT As String) As Boolean
        Dim data_store As New TechBulletinDataManager
        Try
            Return data_store.LogKBSearchResult(TBINCLUDED, ID, Model, SerialNumber, Keyword, DateFrom, DateTo, Hits, xCUSTOMERID, xCUSTOMERSEQUENCENUMBER, xHTTP_X_FORWARDED_FOR, xREMOTE_ADDR, xHTTP_REFERER, xHTTP_URL, xHTTP_USER_AGENT)
            Exit Function
        Catch ex As Exception
            Throw ex
        End Try
        Return False
    End Function

    Public Function LogKBView() As Boolean
        Return False
    End Function

    Public Function UpdateKBFeedBack() As Boolean
        Return False
    End Function

    Public Function searchTechBulletinsByRows(ByVal searchString As String, ByVal minRow As Long, ByVal maxRow As Long) As TechBulletin()
        '------- get tech bulletins based on search criterion  and the rows to be fetched
        Dim data_store As New TechBulletinDataManager
        Dim techBulletins As TechBulletin()
        Try
            techBulletins = data_store.GetTechBulletinByRow(searchString, minRow, maxRow)
        Catch ex As Exception
            Throw 'ex
        End Try
        Return techBulletins
    End Function

    Public Function getTechBulletinRecCount(ByVal searchString As String, ByVal searchSerialNo As String) As Long
        '------- get tech bulletins record count
        Dim data_store As New TechBulletinDataManager
        Dim techBulletinsCount As Long
        Try
            techBulletinsCount = data_store.GetTechBulletinCount(searchString, searchSerialNo)
        Catch ex As Exception
            Throw 'ex
        End Try
        Return techBulletinsCount
    End Function

    Public Function SaveFeedBack(ByRef TechBulletinObject As TechBulletin, ByVal FeedBack As Short, ByVal FeedBackID As Long, ByVal TransactionType As Short, ByVal Suggestion As String, ByVal xCUSTOMERID As Integer _
            , ByVal xCUSTOMERSEQUENCENUMBER As Integer, ByVal xHTTP_X_FORWARDED_FOR As String, ByVal xREMOTE_ADDR As String, ByVal xHTTP_REFERER As String _
            , ByVal xHTTP_URL As String, ByVal xHTTP_USER_AGENT As String) As Long
        Dim data_store As New TechBulletinDataManager
        Dim intFeedbackID As Long = 0
        Try
            intFeedbackID = data_store.SaveFeedBack(TechBulletinObject, FeedBack, FeedBackID, TransactionType, Suggestion, xCUSTOMERID, xCUSTOMERSEQUENCENUMBER, xHTTP_X_FORWARDED_FOR, xREMOTE_ADDR, xHTTP_REFERER, xHTTP_URL, xHTTP_USER_AGENT)
        Catch ex As Exception
            Throw 'ex
        End Try
        Return intFeedbackID
    End Function

    Public Function getProductLineItems() As TechBulletin.ProductLines()
        '------- get product line items
        Dim data_store As New TechBulletinDataManager
        Dim ProductLineItems As TechBulletin.ProductLines()
        Try
            ProductLineItems = data_store.GetProductLines()
        Catch ex As Exception
            Throw 'ex
        End Try
        Return ProductLineItems
    End Function

    'Added By Sneha on 31/10/2013
    'To get conflicted models fro database
    Public Function getConflictedModels(ByVal model_id As String) As List(Of String)
        '------- get product line items
        Dim lstModels As List(Of String)
        Dim data_store As New TechBulletinDataManager
        Try
            lstModels = data_store.getConflictedModels(model_id)
        Catch ex As Exception
            Throw 'ex
        End Try
        Return lstModels
    End Function

End Class
