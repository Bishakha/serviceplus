﻿Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Data

Public Class SIAMOperation

    Public Function GetServiceCenterList() As DataSet
        Dim dsServiceCenterList As DataSet
        Dim objSIAMOperation As New SIAMOperationDataManager()
        dsServiceCenterList = objSIAMOperation.GetServiceCenterList()
        Return dsServiceCenterList
    End Function

    Public Function GET_PR_SEC_ServiceCenterMapping() As DataSet
        Dim dsServiceCenterMapping As DataSet
        Dim objSIAMOperation As New SIAMOperationDataManager()
        dsServiceCenterMapping = objSIAMOperation.GET_PR_SEC_ServiceCenterMapping()
        Return dsServiceCenterMapping
    End Function
    Public Function GETShippingMethods() As DataSet
        Dim dsShippingMethods As DataSet
        Dim objSIAMOperation As New SIAMOperationDataManager()
        dsShippingMethods = objSIAMOperation.GETShippingMethods()
        Return dsShippingMethods
    End Function

    Public Function GETServiceAgreementModel(ByRef objGlobalData As GlobalData) As DataSet
        Dim dsServiceAgreementModel As DataSet
        Dim objSIAMOperation As New SIAMOperationDataManager()
        dsServiceAgreementModel = objSIAMOperation.GETServiceAgreementModel(objGlobalData)
        Return dsServiceAgreementModel
    End Function

    Public Function GETAllServiceAgreementModel() As DataSet
        Dim dsServiceAgreementModel As DataSet
        Dim objSIAMOperation As New SIAMOperationDataManager()
        dsServiceAgreementModel = objSIAMOperation.GETAllServiceAgreementModel()
        Return dsServiceAgreementModel
    End Function

    Public Function GETCertificateNumbers() As DataSet
        Dim dsCertificateNumbers As DataSet
        Dim objSIAMOperation As New SIAMOperationDataManager()
        dsCertificateNumbers = objSIAMOperation.GETCertificateNumbers()
        Return dsCertificateNumbers
    End Function

    Public Function GETPROCESSNAME() As DataSet
        Dim dsPROCESSNAME As DataSet
        Dim objSIAMOperation As New SIAMOperationDataManager()
        dsPROCESSNAME = objSIAMOperation.GETPROCESSNAME()
        Return dsPROCESSNAME
    End Function

    Public Function GETPROCESSLOG(ByVal PROCESSNAME As String, ByVal PROCESSDATE As String) As DataSet
        Dim dsPROCESSLOG As DataSet
        Dim objSIAMOperation As New SIAMOperationDataManager()
        dsPROCESSLOG = objSIAMOperation.GETPROCESSLOG(PROCESSNAME, PROCESSDATE)
        Return dsPROCESSLOG
    End Function

    Public Function GETPROCESSORERROR(ByVal PROCESSERRORID As Integer) As DataSet
        Dim dsPROCESSLOG As DataSet
        Dim objSIAMOperation As New SIAMOperationDataManager()
        dsPROCESSLOG = objSIAMOperation.GETPROCESSORERROR(PROCESSERRORID)
        Return dsPROCESSLOG
    End Function

    Public Function StoreServiceCenterMapping(ByVal PrimaryServiceCenterID As Integer, ByVal SecondaryServiceCenterID As Integer, ByVal Action As Short) As Boolean
        Dim objSIAMOperation As New SIAMOperationDataManager()
        Dim txn_context As TransactionContext = Nothing

        Try
            Try
                txn_context = objSIAMOperation.BeginTransaction
                Dim xRowsEffected As Integer = objSIAMOperation.StoreServiceCenterMapping(PrimaryServiceCenterID, SecondaryServiceCenterID, Action, txn_context)
                objSIAMOperation.CommitTransaction(txn_context)
                Return xRowsEffected
            Catch ex As Exception
                objSIAMOperation.RollbackTransaction(txn_context)
                Throw ex
            End Try
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function StoreShippingMethod(ByVal SHIPPINGMETHODID As Integer, ByVal STATUSCODE As Integer, ByVal DISPLAYORDER As Integer, ByVal SISBOCARRIERCODE As String, ByVal TRACKINGURL As String, ByVal SISBOPICKGROUPCODE As String, ByVal BACKORDEROVERRIDE As Integer, ByVal DESCRIPTION As String, ByVal SISCARRIERCODE As String, ByVal SISPICKGROUPCODE As String, ByVal PRSVCARRIERCODE As String, ByVal SALESORG As String, ByVal LANGUAGEID As String, ByVal Action As Short) As Integer
        Dim objSIAMOperation As New SIAMOperationDataManager()
        Dim txn_context As TransactionContext = Nothing

        Try
            Try
                txn_context = objSIAMOperation.BeginTransaction
                Dim xRowsEffected As Integer = objSIAMOperation.StoreShippingMethod(SHIPPINGMETHODID, STATUSCODE, DISPLAYORDER, SISBOCARRIERCODE, TRACKINGURL, SISBOPICKGROUPCODE, BACKORDEROVERRIDE, DESCRIPTION, SISCARRIERCODE, SISPICKGROUPCODE, PRSVCARRIERCODE, SALESORG, LANGUAGEID, Action, txn_context)
                objSIAMOperation.CommitTransaction(txn_context)
                Return xRowsEffected
            Catch ex As Exception
                objSIAMOperation.RollbackTransaction(txn_context)
                Throw ex
            End Try
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Sub InsertCertificateNumbers(ByVal strProductCode As String, ByVal StarCertNumber As Integer, ByVal EndCertNumber As Integer)
        Dim objSIAMOperation As New SIAMOperationDataManager()
        Dim txn_context As TransactionContext = Nothing

        Try
            Try
                txn_context = objSIAMOperation.BeginTransaction
                objSIAMOperation.InsertCertificateNumbers(strProductCode, StarCertNumber, EndCertNumber, txn_context)
                objSIAMOperation.CommitTransaction(txn_context)
            Catch ex As Exception
                objSIAMOperation.RollbackTransaction(txn_context)
                Throw ex
            End Try
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub UpdateCertificateNumbers(ByVal strProductCode As String, ByVal StarCertNumber As Integer, ByVal EndCertNumber As Integer)
        Dim objSIAMOperation As New SIAMOperationDataManager()
        Dim txn_context As TransactionContext = Nothing
        Try
            Try
                txn_context = objSIAMOperation.BeginTransaction
                objSIAMOperation.UpdateCertificateNumbers(strProductCode, StarCertNumber, EndCertNumber, txn_context)
                objSIAMOperation.CommitTransaction(txn_context)
            Catch ex As Exception
                objSIAMOperation.RollbackTransaction(txn_context)
                Throw ex
            End Try
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub DeleteCertificateNumbers(ByVal strProductCode As String)
        Dim objSIAMOperation As New SIAMOperationDataManager()
        Dim txn_context As TransactionContext = Nothing
        Try
            Try
                txn_context = objSIAMOperation.BeginTransaction
                objSIAMOperation.DeleteCertificateNumbers(strProductCode, txn_context)
                objSIAMOperation.CommitTransaction(txn_context)
            Catch ex As Exception
                objSIAMOperation.RollbackTransaction(txn_context)
                Throw ex
            End Try
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub InsertServiceAgreementModel(ByVal ProductCatagory As String, ByVal ProductGroup As String, ByVal ProductCode As String, ByVal ModelNumber As String, ByRef objGlobalData As GlobalData)
        Dim objSIAMOperation As New SIAMOperationDataManager()
        Dim txn_context As TransactionContext = Nothing

        Try
            Try
                txn_context = objSIAMOperation.BeginTransaction
                objSIAMOperation.InsertServiceAgreementModel(ProductCatagory, ProductGroup, ProductCode, ModelNumber, txn_context, objGlobalData)
                objSIAMOperation.CommitTransaction(txn_context)
            Catch ex As Exception
                objSIAMOperation.RollbackTransaction(txn_context)
                Throw ex
            End Try
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub UpdateServiceAgreementModel(ByVal ID As String, ByVal ProductCatagory As String, ByVal ProductGroup As String, ByVal ProductCode As String, ByVal ModelNumber As String, ByRef objGlobalData As GlobalData)
        Dim objSIAMOperation As New SIAMOperationDataManager()
        Dim txn_context As TransactionContext = Nothing

        Try
            Try
                txn_context = objSIAMOperation.BeginTransaction
                objSIAMOperation.UpdateServiceAgreementModel(ID, ProductCatagory, ProductGroup, ProductCode, ModelNumber, txn_context, objGlobalData)
                objSIAMOperation.CommitTransaction(txn_context)
            Catch ex As Exception
                objSIAMOperation.RollbackTransaction(txn_context)
                Throw ex
            End Try
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub DeleteServiceAgreementModel(ByVal ID As String)
        Dim objSIAMOperation As New SIAMOperationDataManager()
        Dim txn_context As TransactionContext = Nothing

        Try
            Try
                txn_context = objSIAMOperation.BeginTransaction
                objSIAMOperation.DeleteServiceAgreementModel(ID, txn_context)
                objSIAMOperation.CommitTransaction(txn_context)
            Catch ex As Exception
                objSIAMOperation.RollbackTransaction(txn_context)
                Throw ex
            End Try
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Function CheckProductCode(ByVal strProductCode As String) As Boolean
        Dim objSIAMOperation As New SIAMOperationDataManager()

        Try
            Return objSIAMOperation.checkProductCode(strProductCode)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    '''Added By Sneha to get the conflicts
    Public Function GetConflictedModel(ByVal Model_Number As String, ByRef objGlobalData As GlobalData) As String
        Dim objSIAMOperation As New SIAMOperationDataManager()
        Try
            Return objSIAMOperation.GetConflictedModel(Model_Number, objGlobalData)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

End Class
