
Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Data

Public Class SurveyManager

    Public Function GetSurvey(ByVal survey_id As String) As Survey
        Dim data_store As New SurveyDataManager
        Dim obj As Object

        Try
            obj = data_store.Restore(survey_id)
        Catch ex As Exception
            Throw ex
        End Try
        Return obj
    End Function

    Public Function GetResponses(ByVal survey As Survey, ByVal customer As Customer) As Response()
        Dim data_store As New SurveyDataManager
        Dim responses() As Sony.US.ServicesPLUS.Core.Response

        Try
            responses = data_store.GetCustomerResponses(survey.SurveyID, customer)
        Catch ex As Exception
            Throw ex
        End Try

        Return responses
    End Function

    Public Sub AddSurveyResponse(ByVal response As Response)
        ValidateResponse(response)
        StoreResponse(response, False)
    End Sub

    Public Sub UpdateSurveyResponse(ByVal response As Response)
        ValidateResponse(response)
        StoreResponse(response, True)
    End Sub

    Private Sub ValidateResponse(ByRef response As Response)
        If response.Respondent Is Nothing Then
            Throw New Exception("Respondent property (i.e. customer) must be set on survey response object.")
        End If

        If response.Question Is Nothing Then
            Throw New Exception("Question property must be set on survey response object.")
        End If

        If response.Survey Is Nothing Then
            Throw New Exception("Survey property must be set on survey response object.")
        End If

        If response.TextResponse Is Nothing And response.Question.AnswerType = Question.AnswerDisplayType.Textbox Then
            Throw New Exception("A text based response must be supplied for questions with textual answer types.")
        End If

        If response.TextResponse IsNot Nothing Then
            If response.Question.AnswerType = Question.AnswerDisplayType.RadioButtons Then
                Throw New Exception("Textual responses should not be supplied for questions that require radio button responses.")
            End If
        End If

        If response.RadioButtonResponse <> Response.FixedRadioButtonResponseType.NotSpecified And response.Question.AnswerType = Question.AnswerDisplayType.Textbox Then
            Throw New Exception("Radio button responses should not be supplied for questions that require a textual response")
        End If

        If response.RadioButtonResponse = Response.FixedRadioButtonResponseType.NotSpecified And response.Question.AnswerType = Question.AnswerDisplayType.RadioButtons Then
            Throw New Exception("A radio button must be selected for questions with radio button answer types.")
        End If
    End Sub

    Private Sub StoreResponse(ByVal response As Response, ByVal update As Boolean)
        Dim data_store As New SurveyDataManager
        Dim txn_context As TransactionContext = Nothing

        Try
            txn_context = data_store.BeginTransaction
            data_store.Store(response, txn_context, update)
            data_store.CommitTransaction(txn_context)
        Catch ex As Exception
            data_store.RollbackTransaction(txn_context)
            Throw ex
        End Try
    End Sub

End Class
