﻿Imports Sony.US.ServicesPLUS.Data
Imports System.Xml
Imports Sony.US.ServicesPLUS.Core
Imports ServicesPlusException

Public Class TaxExemptionManager

    Public Function GetTaxExemptions(ByVal siamId As String) As ArrayList
        Dim data_store As New TaxExemptionDataManager
        Dim data As ArrayList

        Try
            data = data_store.GetTaxExemptions(siamId)
        Catch ex As Exception
            Throw ex   ' ASleight - I'm no fan of Try-Catch-Throw, but it's how they do it everywhere else...
        End Try

        Return data
    End Function

    Public Function UpdateTaxExemption(ByVal item As TaxExemption) As Boolean
        Dim data_store As New TaxExemptionDataManager
        Dim retVal As Boolean = False

        Try
            retVal = data_store.UpdateTaxExemption(item)
        Catch ex As Exception
            Throw ex
        End Try

        Return retVal
    End Function

    Public Function AddTaxExemption(ByVal item As TaxExemption) As Boolean
        Dim data_store As New TaxExemptionDataManager
        Dim retVal As Boolean = False

        Try
            retVal = data_store.AddTaxExemption(item)
        Catch ex As Exception
            Throw ex
        End Try

        Return retVal
    End Function

    Public Sub DeleteTaxExemption(ByVal rowID As String)
        Dim data_store As New TaxExemptionDataManager

        Try
            data_store.DeleteTaxExemption(rowID)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
End Class
