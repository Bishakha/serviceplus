Public Class Category

    Private m_category_id As String
    Private m_description As String

    Public Property CategoryID() As String
        Get
            Return m_category_id
        End Get
        Set(ByVal Value As String)
            m_category_id = Value
        End Set
    End Property

    Public Property Description() As String
        Get
            Return m_description
        End Get
        Set(ByVal Value As String)
            m_description = Value
        End Set
    End Property

End Class
