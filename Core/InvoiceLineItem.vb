Public Class InvoiceLineItem

    Private m_invoice As Invoice
    Private m_invoice_line_number As String
    Private m_order_line_number As String
    Private m_part_number_shipped As String
    Private m_part_description As String
    Private m_quantity As Integer
    Private m_item_price As Double

    ' added to include order number and invoice number - Deepa V, Mar 24,2006
    Private m_invoice_number As String
    Private m_order_number As String

    Public Sub New(ByVal invoice As Invoice)
        m_invoice = invoice
    End Sub

    Public Property Invoice() As Invoice
        Get
            Return m_invoice
        End Get
        Set(ByVal Value As Invoice)
            m_invoice = Value
        End Set
    End Property

    Public Property InvoiceNumber() As String
        Get
            Return m_invoice_number
        End Get
        Set(ByVal Value As String)
            m_invoice_number = Value
        End Set
    End Property

    Public Property OrderNumber() As String
        Get
            Return m_order_number
        End Get
        Set(ByVal Value As String)
            m_order_number = Value
        End Set
    End Property

    Public Property InvoiceLineNumber() As String
        Get
            Return m_invoice_line_number
        End Get
        Set(ByVal Value As String)
            m_invoice_line_number = Value
        End Set
    End Property

    Public Property OrderLineNumber() As String
        Get
            Return m_order_line_number
        End Get
        Set(ByVal Value As String)
            m_order_line_number = Value
        End Set
    End Property

    Public Property PartNumberShipped() As String
        Get
            Return m_part_number_shipped
        End Get
        Set(ByVal Value As String)
            m_part_number_shipped = Value
        End Set
    End Property

    Public Property PartDescription() As String
        Get
            Return m_part_description
        End Get
        Set(ByVal Value As String)
            m_part_description = Value
        End Set
    End Property

    Public Property Quantity() As Integer
        Get
            Return m_quantity
        End Get
        Set(ByVal Value As Integer)
            m_quantity = Value
        End Set
    End Property

    Public Property ItemPrice() As Double
        Get
            Return m_item_price
        End Get
        Set(ByVal Value As Double)
            m_item_price = Value
        End Set
    End Property

    Public ReadOnly Property TotalPrice() As Double
        Get
            Return m_item_price * Quantity
        End Get
    End Property

End Class
