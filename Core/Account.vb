Public Class Account
    Inherits CoreObject
    Public Enum AddressOverrideType
        ShipTo
        ShipToAndBillTo
        NotAllowed
    End Enum
    Private m_account_number As String
    Private m_account_type As Account.Type
    Private m_customer As New Customer
    Private m_validated As Boolean
    Private m_sequence_number As String
    Private m_address As Address
    Private m_SAPaccount As String
    Private m_address_override As String
    Private m_override_description As String
    Private m_CACalifornia As String
    Private m_ship_to_address As Address
    Private m_ShipTO As Account()
    ''Added for bug 6219
    Private m_soldToAccount As String
    Private m_ShipToAccount As String
    Private m_SoldToAccountbyShipToParty As String '7980V1



    Public Enum Type
        SAP_BILL_TO_ACCOUNT = 0
        SIS_LEGACY_BILL_TO_ACCOUNT = 1
        SIS_COMA_ACCOUNT = 2 'obtained from SIS at runtime, not stored locally
    End Enum

    Public Sub New()
        m_validated = False
        m_address = New Address
    End Sub

    Public Property Customer() As Customer
        Get
            Return m_customer
        End Get
        Set(ByVal Value As Customer)
            m_customer = Value
        End Set
    End Property
    Public Property AddressOverride() As AddressOverrideType
        Get
            Return m_address_override
        End Get
        Set(ByVal Value As AddressOverrideType)
            m_address_override = Value
        End Set
    End Property
    Public Property TaxExempt() As String
        Get
            Return m_CACalifornia
        End Get
        Set(ByVal Value As String)
            m_CACalifornia = Value
        End Set
    End Property
    Public Property OverrideDescription() As String
        Get
            Return m_override_description
        End Get
        Set(ByVal Value As String)
            m_override_description = Value
        End Set
    End Property

    Public Property AccountNumber() As String
        Get
            Return m_account_number
        End Get
        Set(ByVal Value As String)
            SetProperty(m_account_number, Value)
        End Set
    End Property
    Public ReadOnly Property BillTo() As Address
        Get
            Return m_address
        End Get

    End Property
    Public Property Address() As Address
        Get
            Return m_address
        End Get
        Set(ByVal Value As Address)
            m_address = Value
        End Set
    End Property

    Public Property SequenceNumber() As String
        Get
            Return m_sequence_number
        End Get
        Set(ByVal Value As String)
            m_sequence_number = Value
        End Set
    End Property

    'Public Property AccountType() As Account.Type
    '    Get
    '        Return m_account_type
    '    End Get
    '    Set(ByVal Value As Account.Type)
    '        SetProperty(m_account_type, Value)
    '    End Set
    'End Property

    Public Property Validated() As Boolean
        Get
            Return m_validated
        End Get
        Set(ByVal Value As Boolean)
			m_validated = Value
        End Set
    End Property

    Public Property SAPAccount() As String
        Get
            Return m_SAPaccount
        End Get
        Set(ByVal Value As String)
            m_SAPaccount = Value
        End Set
    End Property
    Public Property SoldToAccount() As String
        Get
            Return m_soldToAccount
        End Get
        Set(ByVal Value As String)
            m_soldToAccount = Value
        End Set
    End Property
    Public Property ShipToAccount() As Account()
        Get
            Return m_ShipTO
        End Get
        Set(ByVal Value As Account())
            m_ShipTO = Value
        End Set
    End Property
    Public Property ShipTo() As Address
        Get
            Return m_ship_to_address
        End Get
        Set(ByVal Value As Address)
            m_ship_to_address = Value
        End Set
    End Property
    Public Property ShipToAccountNumber() As String
        Get
            Return m_ShipToAccount
        End Get
        Set(ByVal Value As String)
            m_ShipToAccount = Value
        End Set
    End Property
    '7980V1 starts
    Public Property SoldtoAccNumbyShipParty() As String
        Get
            Return m_SoldToAccountbyShipToParty
        End Get
        Set(ByVal Value As String)
            m_SoldToAccountbyShipToParty = Value
        End Set
    End Property
    '7980V1 ends

End Class
