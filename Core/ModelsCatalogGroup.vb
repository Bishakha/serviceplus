Public Class ModelsCatalogGroup
    Inherits CoreObject

    Private m_range As String
    Private m_group_id As String

    Public Property Range() As String
        Get
            Return m_range
        End Get
        Set(ByVal Value As String)
            SetProperty(m_range, Value)
        End Set
    End Property

    Public Property GroupID() As String
        Get
            Return m_group_id
        End Get
        Set(ByVal Value As String)
            SetProperty(m_group_id, Value)
        End Set
    End Property

End Class
