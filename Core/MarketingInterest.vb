Public Class MarketingInterest
    Inherits CoreObject

    Private m_marketing_interest_id As String
    Private m_description As String
    Private m_customer As Customer
    Private m_sequence_number As String

    Public Sub New()
    End Sub

    Public Property MarketingInterestID() As String
        Get
            Return m_marketing_interest_id
        End Get
        Set(ByVal Value As String)
            m_marketing_interest_id = Value
        End Set
    End Property

    Public Property Description() As String
        Get
            Return m_description
        End Get
        Set(ByVal Value As String)
            m_description = Value
        End Set
    End Property

    Public Property Customer() As Customer
        Get
            Return m_customer
        End Get
        Set(ByVal Value As Customer)
            m_customer = Value
        End Set
    End Property

    Public Property SequenceNumber() As String
        Get
            Return m_sequence_number
        End Get
        Set(ByVal Value As String)
            m_sequence_number = Value
        End Set
    End Property

End Class
