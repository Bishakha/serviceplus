Public Class CreditCardType

    Private m_credit_card_type_code As String
    Private m_description As String
    Private m_cardtype As String


    Public Sub New()

    End Sub

    Public Property CreditCardTypeCode() As String
        Get
            Return m_credit_card_type_code
        End Get
        Set(ByVal Value As String)
            m_credit_card_type_code = Value
        End Set
    End Property

    Public Property Description() As String
        Get
            Return m_description
        End Get
        Set(ByVal Value As String)
            m_description = Value
        End Set
    End Property

    Public Property CardType() As String
        Get
            Return m_cardtype
        End Get
        Set(ByVal Value As String)
            m_cardtype = Value
        End Set
    End Property

End Class
