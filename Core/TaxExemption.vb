﻿Public Class TaxExemption
    Inherits CoreObject

#Region "   Private Variables   "
    Private _rowID As String
    Private _siamID As String
    Private _name As String
    Private _city As String
    Private _state As String
    Private _documentState As String
    Private _documentStatus As Char
    Private _documentDate As Date
    Private _expirationDate As Date
    Private _updateDate As Date
    Private _documentIDNumber As String
#End Region

#Region "   Properties   "
    Public Property RowID() As String
        Get
            Return _rowID
        End Get
        Set(ByVal value As String)
            _rowID = value
        End Set
    End Property

    Public Property SiamID() As String
        Get
            Return _siamID
        End Get
        Set(ByVal value As String)
            _siamID = value
        End Set
    End Property

    Public Property Name() As String
        Get
            Return _name
        End Get
        Set(ByVal value As String)
            _name = value
        End Set
    End Property

    Public Property City() As String
        Get
            Return _city
        End Get
        Set(ByVal value As String)
            _city = value
        End Set
    End Property

    Public Property State() As String
        Get
            Return _state
        End Get
        Set(ByVal value As String)
            _state = value
        End Set
    End Property

    Public Property DocumentState() As String
        Get
            Return _documentState
        End Get
        Set(ByVal value As String)
            _documentState = value
        End Set
    End Property

    Public Property DocumentStatus() As Char
        Get
            Return _documentStatus
        End Get
        Set(ByVal value As Char)
            _documentStatus = value
        End Set
    End Property

    Public Property DocumentDate() As Date
        Get
            Return _documentDate.Date
        End Get
        Set(ByVal value As Date)
            _documentDate = value.Date
        End Set
    End Property

    Public Property ExpirationDate() As Date
        Get
            Return _expirationDate.Date
        End Get
        Set(ByVal value As Date)
            _expirationDate = value.Date
        End Set
    End Property

    Public Property UpdateDate() As Date
        Get
            Return _updateDate.Date
        End Get
        Set(ByVal value As Date)
            _updateDate = value.Date
        End Set
    End Property

    Public Property DocumentIDNumber() As String
        Get
            Return _documentIDNumber
        End Get
        Set(ByVal value As String)
            _documentIDNumber = value
        End Set
    End Property
#End Region

End Class
