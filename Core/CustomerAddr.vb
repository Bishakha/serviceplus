' Class: CustomerAddr
' Customer address, holds the customer address details, can be ship to or bill to address
'
Public Class CustomerAddr
    Inherits CoreObject

    Private m_company As String = String.Empty
    Private m_attnName As String = String.Empty
    Private m_address1 As String = String.Empty
    Private m_address2 As String = String.Empty
    Private m_address3 As String = String.Empty
    Private m_address4 As String = String.Empty
    Private m_city As String = String.Empty
    Private m_state As String = String.Empty
    Private m_zipcode As String = String.Empty
    Private m_phone As String = String.Empty
    Private m_phextension As String = String.Empty
    Private m_fax As String = String.Empty
    'Commented after Issue E793 reopen
    ''Start Modification for E793
    'Private m_emailaddress As String
    ''End Modification for E793

    Public Property Fax() As String
        Get
            Return m_fax

        End Get
        Set(ByVal value As String)
            m_fax = value
        End Set
    End Property
    'Commented after Issue E793 reopen
    ''Start Modification for E793
    'Public Property EmailAddress() As String
    '    Get
    '        Return m_emailaddress

    '    End Get
    '    Set(ByVal value As String)
    '        m_emailaddress = value
    '    End Set
    'End Property
    ''End Modification for E793
    Public Property Company() As String
        Get
            Return m_company
        End Get
        Set(ByVal value As String)
            m_company = value
        End Set
    End Property

    Public Property AttnName() As String
        Get
            Return m_attnName
        End Get
        Set(ByVal value As String)
            m_attnName = value
        End Set
    End Property

    Public Property Address1() As String
        Get
            Return m_address1
        End Get
        Set(ByVal value As String)
            m_address1 = value
        End Set
    End Property

    Public Property Address2() As String
        Get
            Return m_address2
        End Get
        Set(ByVal value As String)
            m_address2 = value
        End Set
    End Property
    Public Property Address3() As String
        Get
            Return m_address3
        End Get
        Set(ByVal value As String)
            m_address3 = value
        End Set
    End Property
    Public Property Address4() As String
        Get
            Return m_address4
        End Get
        Set(ByVal value As String)
            m_address4 = value
        End Set
    End Property
    Public Property City() As String
        Get
            Return m_city
        End Get
        Set(ByVal value As String)
            m_city = value
        End Set
    End Property

    Public Property State() As String
        Get
            Return m_state
        End Get
        Set(ByVal value As String)
            m_state = value
        End Set
    End Property

    Public Property ZipCode() As String
        Get
            Return m_zipcode
        End Get
        Set(ByVal value As String)
            m_zipcode = value
        End Set
    End Property

    Public Property Phone() As String
        Get
            Return m_phone
        End Get
        Set(ByVal value As String)
            m_phone = value
        End Set
    End Property

    Public Property PhoneExtn() As String
        Get
            Return m_phextension
        End Get
        Set(ByVal value As String)
            m_phextension = value
        End Set
    End Property

End Class
