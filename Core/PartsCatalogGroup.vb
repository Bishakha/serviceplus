Public Class PartsCatalogGroup
    Inherits CoreObject

    Private m_range_start As String
    Private m_range_end As String
    Private m_group_id As String

    Public Property RangeStart() As String
        Get
            Return m_range_start
        End Get
        Set(ByVal Value As String)
            SetProperty(m_range_start, Value)
        End Set
    End Property

    Public Property RangeEnd() As String
        Get
            Return m_range_end
        End Get
        Set(ByVal Value As String)
            SetProperty(m_range_end, Value)
        End Set
    End Property

    Public Property GroupID() As String
        Get
            Return m_group_id
        End Get
        Set(ByVal Value As String)
            SetProperty(m_group_id, Value)
        End Set
    End Property

End Class
