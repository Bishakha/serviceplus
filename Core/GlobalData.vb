﻿Public Class GlobalData

    Private _salesOrganization As String
    Private _distributionChannel As String
    Private _division As String
    Private _language As String
    Private _languageForIdp As String

    Public Property SalesOrganization() As String
        Get
            Return _salesOrganization
        End Get
        Set(ByVal Value As String)
            _salesOrganization = Value
        End Set
    End Property

    Public Property DistributionChannel() As String
        Get
            Return _distributionChannel
        End Get
        Set(ByVal Value As String)
            _distributionChannel = Value
        End Set
    End Property

    Public Property Division() As String
        Get
            Return _division
        End Get
        Set(ByVal Value As String)
            _division = Value
        End Set
    End Property

    Public Property Language() As String
        Get
            If (String.IsNullOrEmpty(_language)) Then _language = "en-US"
            Return _language
        End Get
        Set(ByVal Value As String)
            _language = Value
        End Set
    End Property

    Public ReadOnly Property LanguageForIDP() As String
        Get
            If String.IsNullOrEmpty(_languageForIdp) Then
                _languageForIdp = _language.Replace("-", "_")
            End If

            Return _languageForIdp
        End Get
    End Property

    Public ReadOnly Property IsAmerica() As Boolean
        Get
            Return (_language.EndsWith("US"))
        End Get
    End Property

    Public ReadOnly Property IsCanada() As Boolean
        Get
            Return (_language.EndsWith("CA"))
        End Get
    End Property

    Public ReadOnly Property IsEnglish() As Boolean
        Get
            Return (_language.StartsWith("en"))
        End Get
    End Property

    Public ReadOnly Property IsFrench() As Boolean
        Get
            Return (_language.StartsWith("fr"))
        End Get
    End Property

    Public Overrides Function ToString() As String
        Dim format As String = "Language = {0}, LanguageForIdp = {1}, SalesOrg = {2}, DistributionChannel = {3}, Division = {4}"
        Return String.Format(format, _language, _languageForIdp, _salesOrganization, _distributionChannel, _division)
    End Function
End Class
