'
' class: Service detail
' provide the functionality for storing, reterving service details entered by customer
'

Public Class ServiceDetail
    Inherits CoreObject

    Private m_modelName As String
    Private m_serialno As String
    Private m_accessories As String
    Private m_contractno As String
    Private m_productWarranty As Boolean
    Private m_serviceWrranty As Boolean
    Private m_outofWrranty As Boolean
    Private m_customertype As String
    Private m_billAddr As CustomerAddr
    Private m_shipAddr As CustomerAddr
    Private m_ponumber As String
    Private m_approved As Boolean
    Private m_preapproval As Boolean
    Private m_preAppAmount As String
    Private m_taxexempt As Boolean
    Private m_paymentType As String
    Private m_intermittentProb As Boolean
    Private m_prevntMaintenance As Boolean
    Private m_probDescr As String

    Public Property ModelName() As String
        Get
            Return m_modelName
        End Get
        Set(ByVal value As String)
            m_modelName = value
        End Set
    End Property

    Public Property SerialNo() As String
        Get
            Return m_serialno
        End Get
        Set(ByVal value As String)
            m_serialno = value
        End Set
    End Property

    Public Property Accessories() As String
        Get
            Return m_accessories
        End Get
        Set(ByVal value As String)
            m_accessories = value
        End Set
    End Property

    Public Property ContractNo() As String
        Get
            Return m_contractno
        End Get
        Set(ByVal value As String)
            m_contractno = value
        End Set
    End Property

    Public Property ProductWarranty() As Boolean
        Get
            Return m_productWarranty
        End Get
        Set(ByVal value As Boolean)
            m_productWarranty = value
        End Set
    End Property

    Public Property ServiceWarranty() As Boolean
        Get
            Return m_serviceWrranty
        End Get
        Set(ByVal value As Boolean)
            m_serviceWrranty = value
        End Set
    End Property

    Public Property OutofWrranty() As Boolean
        Get
            Return m_outofWrranty
        End Get
        Set(ByVal value As Boolean)
            m_outofWrranty = value
        End Set
    End Property

    Public Property CustomerType() As String
        Get
            Return m_customertype
        End Get
        Set(ByVal value As String)
            m_customertype = value
        End Set
    End Property

    Public Property BillToAddress() As CustomerAddr
        Get
            Return m_billAddr
        End Get
        Set(ByVal value As CustomerAddr)
            m_billAddr = value
        End Set
    End Property

    Public Property ShipToAddress() As CustomerAddr
        Get
            Return m_shipAddr
        End Get
        Set(ByVal value As CustomerAddr)
            m_shipAddr = value
        End Set
    End Property

    Public Property PONumber() As String
        Get
            Return m_ponumber
        End Get
        Set(ByVal value As String)
            m_ponumber = value
        End Set
    End Property

    Public Property Approved() As Boolean
        Get
            Return m_approved
        End Get
        Set(ByVal value As Boolean)
            m_approved = value
        End Set
    End Property

    Public Property PreApproval() As Boolean
        Get
            Return m_preapproval
        End Get
        Set(ByVal value As Boolean)
            m_preapproval = value
        End Set
    End Property

    Public Property PreAppAmount() As String
        Get
            Return m_preAppAmount
        End Get
        Set(ByVal value As String)
            m_preAppAmount = value
        End Set
    End Property

    Public Property TaxExempt() As Boolean
        Get
            Return m_taxexempt
        End Get
        Set(ByVal value As Boolean)
            m_taxexempt = value
        End Set
    End Property
    Public Property PaymentType() As String
        Get
            Return m_paymentType
        End Get
        Set(ByVal value As String)
            m_paymentType = value
        End Set
    End Property

    Public Property IsIntermittentProblem() As Boolean
        Get
            Return m_intermittentProb
        End Get
        Set(ByVal value As Boolean)
            m_intermittentProb = value
        End Set
    End Property
    Public Property IsPreventiveMaintence() As Boolean
        Get
            Return m_prevntMaintenance
        End Get
        Set(ByVal value As Boolean)
            m_prevntMaintenance = value
        End Set
    End Property

    Public Property ProblemDescription() As String
        Get
            Return m_probDescr
        End Get
        Set(ByVal value As String)
            m_probDescr = value
        End Set
    End Property

End Class
