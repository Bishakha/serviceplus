Public Class Address
    Inherits CoreObject

    Private m_name As String = ""
    Private m_line_1 As String = ""
    Private m_line_2 As String = ""
    Private m_line_3 As String = ""
    Private m_line_4 As String = ""
    Private m_city As String = ""
    Private m_state As String = ""
    Private m_postal_code As String = ""
    Private m_attn As String = ""
    Private m_PhoneNumber As String = String.Empty
    Private m_OfficeNumber As String = String.Empty 'bug 8346
    Private m_PhoneExt As String = String.Empty
    Private m_CACalifornia As String
    Private m_s_sap_account As String
    Private m_country As String = String.Empty 'bug 8346


    Public Sub New()

    End Sub

    Public Property SapAccountNumber() As String
        Get
            Return m_s_sap_account
        End Get
        Set(ByVal Value As String)
            m_s_sap_account = Value
        End Set
    End Property
    Public Property Attn() As String
        Get
            Return m_attn
        End Get
        Set(ByVal Value As String)
            m_attn = Value
        End Set
    End Property

    'Public Property CACalifornia1() As String
    '    Get
    '        Return m_CACalifornia
    '    End Get
    '    Set(ByVal Value As String)
    '        m_CACalifornia = Value
    '    End Set
    'End Property


    Public Property PhoneNumber() As String
        Get
            Return m_PhoneNumber
        End Get
        Set(ByVal Value As String)
            m_PhoneNumber = Value
        End Set
    End Property
    Public Property OfficeNumber() As String
        Get
            Return m_OfficeNumber

        End Get
        Set(ByVal Value As String)
            m_OfficeNumber = Value
        End Set
    End Property
    Public Property PhoneExt() As String
        Get
            Return m_PhoneExt
        End Get
        Set(ByVal Value As String)
            m_PhoneExt = Value
        End Set
    End Property
    Public Property Name() As String
        Get
            Return m_name
        End Get
        Set(ByVal Value As String)
            m_name = Value
        End Set
    End Property
    Public ReadOnly Property Address1() As String
        Get
            Return m_line_1
        End Get
        
    End Property


    Public ReadOnly Property Address2() As String
        Get
            Return m_line_2
        End Get
       
    End Property

    Public ReadOnly Property Address3() As String
        Get
            Return m_line_3
        End Get

    End Property
    Public ReadOnly Property Address4() As String
        Get
            Return m_line_4
        End Get

    End Property
    Public ReadOnly Property Zip() As String
        Get
            Return m_postal_code
        End Get
    End Property
    Public Property Line1() As String
        Get
            Return m_line_1
        End Get
        Set(ByVal Value As String)
            SetProperty(m_line_1, Value)
        End Set
    End Property

    Public Property Line2() As String
        Get
            Return m_line_2
        End Get
        Set(ByVal Value As String)
            SetProperty(m_line_2, Value)
        End Set
    End Property

    Public Property Line3() As String
        Get
            Return m_line_3
        End Get
        Set(ByVal Value As String)
            SetProperty(m_line_3, Value)
        End Set
    End Property
    Public Property Line4() As String
        Get
            Return m_line_4
        End Get
        Set(ByVal Value As String)
            SetProperty(m_line_4, Value)
        End Set
    End Property
    Public Property City() As String
        Get
            Return m_city
        End Get
        Set(ByVal Value As String)
            SetProperty(m_city, Value)
        End Set
    End Property

    Public Property State() As String
        Get
            Return m_state
        End Get
        Set(ByVal Value As String)
            SetProperty(m_state, Value)
        End Set
    End Property

    Public Property PostalCode() As String
        Get
            Return m_postal_code
        End Get
        Set(ByVal Value As String)
            SetProperty(m_postal_code, Value)
        End Set
    End Property

    Public Property Country() As String
        Get
            Return m_country
        End Get
        Set(ByVal Value As String)
            SetProperty(m_country, Value)
        End Set
    End Property
End Class
