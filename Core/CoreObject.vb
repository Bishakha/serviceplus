Public Class CoreObject
    Implements IComparable

    Private m_action As ActionType
    Private m_dirty As Boolean
    Protected NullDateTime() As String = {"01/01/0001", "01/01/2001"}

    Public Enum ActionType
        ADD
        RESTORE
        DELETE
        UPDATE
    End Enum

    Public Sub New()
        m_action = ActionType.ADD
        m_dirty = False
        ReturnDeletedObjects = False
    End Sub

    Public Property Action() As ActionType
        Get
            Return m_action
        End Get
        Set(ByVal Value As ActionType)
            m_action = Value
            If Value = ActionType.DELETE Then
                m_dirty = True
            End If
        End Set
    End Property

    Public Property Dirty() As Boolean
        Get
            Return m_dirty
        End Get
        Set(ByVal Value As Boolean)
            If Action = ActionType.RESTORE Then
                m_dirty = Value
            End If
        End Set
    End Property

    Public Property ReturnDeletedObjects() As Boolean

    Protected Sub SetProperty(ByRef old_value As String, ByRef new_value As String)
		If old_value <> new_value Then
			old_value = new_value
			Dirty = True
		End If
	End Sub

    Protected Overridable Function FormatDate(ByRef time As Date) As String
        Dim strTime As String = time.ToString("MM/dd/yyyy")
        For Each str As String In NullDateTime
            If strTime = str Then
                Return ""
            End If
        Next
        Return strTime
    End Function

    Public Function CompareTo(ByVal obj As Object) As Integer Implements System.IComparable.CompareTo

    End Function
End Class
