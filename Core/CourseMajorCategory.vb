Public Class CourseMajorCategory
    Inherits CoreObject

    Private m_code As Integer
    Private m_description As String

    Public Sub New(Optional ByVal description As String = "")
        If Not String.IsNullOrWhiteSpace(description) Then m_description = description
    End Sub

    Public Property Code() As Integer
        Get
            Return m_code
        End Get
        Set(ByVal Value As Integer)
            m_code = Value
        End Set
    End Property

    Public Property Description() As String
        Get
            Return m_description
        End Get
        Set(ByVal Value As String)
            m_description = Value
        End Set
    End Property

End Class
