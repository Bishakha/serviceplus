Public Class CreditCard
    Inherits CoreObject

    Public Enum CreditCardAction
        Authorization
        Credit
        DelayedCapture
    End Enum

    Private m_credit_card_number As String = String.Empty
    Private m_credit_card_number_masked As String
    Private m_expiration_date As String
    Private m_csc_code As String = String.Empty
    Private m_credit_card_type As CreditCardType
    Private m_customer As Customer
    Private m_sequence_number As String
    Private m_nickname As String
    Private iCounter As Short
    Private m_hidecardflag As Boolean

    Public Sub New(ByVal type As CreditCardType)
        m_credit_card_type = type
    End Sub

    Public Property Customer() As Customer
        Get
            Return m_customer
        End Get
        Set(ByVal Value As Customer)
            m_customer = Value
        End Set
    End Property

    Public Property CreditCardNumber() As String
        Get
            Return m_credit_card_number
        End Get
        Set(ByVal Value As String)
            SetProperty(m_credit_card_number, Value)
            'Added by chandra on 22-06-07 for implementation of Enahancement :E684
            m_credit_card_number_masked = String.Empty
            For iCounter = 0 To m_credit_card_number.Trim().Length - 1
                If iCounter <= 3 Or iCounter >= m_credit_card_number.Trim().Length - 4 Then
                    m_credit_card_number_masked = m_credit_card_number_masked & m_credit_card_number.Trim().Substring(iCounter, 1)
                Else
                    m_credit_card_number_masked = m_credit_card_number_masked & "X"
                End If

            Next
        End Set
    End Property
    'Added by chandra on 22-06-07 for implementation of Enahancement :E684
    Public ReadOnly Property CreditCardNumberMasked() As String
        Get
            Return m_credit_card_number_masked
        End Get
    End Property

    Public Property SequenceNumber() As String
        Get
            Return m_sequence_number
        End Get
        Set(ByVal Value As String)
            m_sequence_number = Value
        End Set
    End Property

    Public Property ExpirationDate() As String
        Get
            Return m_expiration_date
        End Get
        Set(ByVal Value As String)
            SetProperty(m_expiration_date, Value)
        End Set
    End Property

    Public Property CSCCode() As String
        Get
            Return m_csc_code
        End Get
        Set(ByVal Value As String)
            SetProperty(m_csc_code, Value)
        End Set
    End Property

    Public Property Type() As CreditCardType
        Get
            Return m_credit_card_type
        End Get
        Set(ByVal Value As CreditCardType)
            m_credit_card_type = Value
        End Set
    End Property

    Public Property NickName() As String
        Get
            Return m_nickname
        End Get
        Set(ByVal Value As String)
            m_nickname = Value
        End Set
    End Property

    'new hidecardflag added for the bug 166
    'the hidecardflag would enable/disable the display of the credit card
    'thereby provide ability to delete the credit card by customer
    Public Property HideCardFlag() As Boolean
        Get
            Return m_hidecardflag
        End Get
        Set(ByVal Value As Boolean)
            m_hidecardflag = Value
        End Set
    End Property
End Class
