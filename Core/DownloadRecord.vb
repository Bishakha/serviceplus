Public Class DownloadRecord
    Inherits CoreObject

    Private m_SIAMID As String
    Private m_kitPartNumber As String
    Private m_time As Date
    Private m_deliveryMethod As String
    Private m_CustomerID As Integer
    Private m_CustomerSequenceNumber As Integer
    Private m_modelID As String
    Private m_modelDesc As String
    Private m_Version As String
    Private m_FreeOfCharge As String
    Private m_KitDesc As String
    Private m_OrderNumber As String
    Private m_OrderLineItemSequence As Integer
    Private m_HttpXForwardFor As String
    Private m_RemoteADDR As String
    Private m_HTTPReferer As String
    Private m_HTTPURL As String
    Private m_HTTPUserAgent As String


    Public Property CustomerID() As Integer
        Get
            Return m_CustomerID
        End Get
        Set(ByVal Value As Integer)
            m_CustomerID = Value
        End Set
    End Property
    Public Property CustomerSequenceNumber() As Integer
        Get
            Return m_CustomerSequenceNumber
        End Get
        Set(ByVal Value As Integer)
            m_CustomerSequenceNumber = Value
        End Set
    End Property
    Public Property SIAMID() As String
        Get
            Return m_SIAMID
        End Get
        Set(ByVal Value As String)
            m_SIAMID = Value
        End Set
    End Property
    Public Property KitPartNumber() As String
        Get
            Return m_kitPartNumber
        End Get
        Set(ByVal Value As String)
            m_kitPartNumber = Value
        End Set
    End Property
    Public Property DeliveryMethod() As String
        Get
            Return m_deliveryMethod
        End Get
        Set(ByVal Value As String)
            m_deliveryMethod = Value
        End Set
    End Property
    Public Property Time() As Date
        Get
            Return m_time
        End Get
        Set(ByVal Value As Date)
            m_time = Value
        End Set
    End Property
    Public Property ModelID() As String
        Get
            Return m_modelID
        End Get
        Set(ByVal Value As String)
            m_modelID = Value
        End Set
    End Property
    Public Property ModelDescription() As String
        Get
            Return m_modelDesc
        End Get
        Set(ByVal Value As String)
            m_modelDesc = Value
        End Set
    End Property
    Public Property Version() As String
        Get
            Return m_Version
        End Get
        Set(ByVal Value As String)
            m_Version = Value
        End Set
    End Property
    Public Property FreeOfCharge() As String
        Get
            Return m_FreeOfCharge
        End Get
        Set(ByVal Value As String)
            m_FreeOfCharge = Value
        End Set
    End Property
    Public Property KitDescription() As String
        Get
            Return m_KitDesc
        End Get
        Set(ByVal Value As String)
            m_KitDesc = Value
        End Set
    End Property
    Public Property OrderNumber() As String
        Get
            Return m_OrderNumber
        End Get
        Set(ByVal Value As String)
            m_OrderNumber = Value
        End Set
    End Property
    Public Property OrderLineItemSequence() As Integer
        Get
            Return m_OrderLineItemSequence
        End Get
        Set(ByVal Value As Integer)
            m_OrderLineItemSequence = Value
        End Set
    End Property
    Public Property HTTP_X_Forward_For() As String
        Get
            Return m_HttpXForwardFor
        End Get
        Set(ByVal Value As String)
            m_HttpXForwardFor = Value
        End Set
    End Property
    Public Property RemoteADDR() As String
        Get
            Return m_RemoteADDR
        End Get
        Set(ByVal Value As String)
            m_RemoteADDR = Value
        End Set
    End Property
    Public Property HTTPReferer() As String
        Get
            Return m_HTTPReferer
        End Get
        Set(ByVal Value As String)
            m_HTTPReferer = Value
        End Set
    End Property
    Public Property HTTPURL() As String
        Get
            Return m_HTTPURL
        End Get
        Set(ByVal Value As String)
            m_HTTPURL = Value
        End Set
    End Property
    Public Property HTTPUserAgent() As String
        Get
            Return m_HTTPUserAgent
        End Get
        Set(ByVal Value As String)
            m_HTTPUserAgent = Value
        End Set
    End Property
End Class
