
Imports System.Collections

Public Class Order    
    Inherits CoreObject
    Implements IComparable

    Private m_yourReference As String
    Private m_customer As Customer
    Private m_credit_card As CreditCard
    Private m_shipping_method As ShippingMethod
    Private m_order_date As Date
    Private m_update_date As Date
    Private m_order_number As String
    'Commented By Navdeep kaur on 19th May 2011 for resolving 6397
    'Private m_confirmed_in_sis As Boolean
    Private m_sequence_number As String
    Private m_carrier_code As String
    Private m_group_number As String

    Private m_bill_to As Address
    Private m_ship_to As Address
    Private m_ShipToAccount As Account
    Private m_BillToAccount As Account

    Private m_credit_reason As String
    Private m_credit_switch As Boolean
    Private m_customer_class As String
    Private m_internal_order As Boolean
    Private m_legacy_account_number As String
    Private m_sap_bill_to_account_number As String
    Private m_po_reference As String
    Private m_allocated_shipping_amt As Double
    Private m_allocated_tax_amt As Double
    Private m_allocated_specialtax_amt As Double
    Private m_allocated_grand_total_amt As Double
    Private m_allocated_part_subtotal_amt As Double
    Private m_verisign_authorization_code As String
    Private m_verisign_transaction_code As String
    Private m_verisign_VOid_transaction_code As String
    Private m_invoices As ArrayList
    Private m_order_line_items As ArrayList
    Private m_tax_exempt As Boolean
    Private m_contain_downloadship_item As Boolean
    Private m_messagetype As String
    Private m_messagetext As String
    Private m_orderstatus As String
    Private m_paymentcardresulttext As String
    Private arrOrderLineItem As Sony.US.ServicesPLUS.Core.OrderLineItem()
    Public Shared m_SortOrder As Integer

    ' included firstname and lastname
    ' modified by Deepa V, Mar 23,2006 for E331
    Private m_first_name As String
    Private m_last_name As String
    Private m_billingonly As Short

    Private m_HttpXForwardFor As String = String.Empty
    Private m_RemoteADDR As String = String.Empty
    Private m_HTTPReferer As String = String.Empty
    Private m_HTTPURL As String = String.Empty
    Private m_HTTPUserAgent As String = String.Empty

    Private m_TransactionType As Short = 0
    Private m_alt_tax_classification As String
    Private m_EnableDocumentIDNumber As String
    Private m_soldToAccount As String

    Private m_AccountNumber As String = String.Empty

    Public Enum enumSortOrder
        OrderNumberASC = 0
        OrderNumberDESC = 1
        PayerNumberASC = 2
        PayerNumberDESC = 3
        TransactionDateASC = 4
        TransactionDateDESC = 5
        PoNumberASC = 6
        PoNumberDESC = 7
        BillingAddressASC = 8
        BillingAddressDESC = 9
    End Enum

    Public Sub New()

    End Sub
    Public Sub New(ByVal order_number As String)
        'Commented By Navdeep Kaur for 6397
        'm_confirmed_in_sis = False
        m_sequence_number = 1
        m_order_number = order_number
        m_invoices = New ArrayList
        m_order_line_items = New ArrayList
        m_bill_to = New Address
        m_ship_to = New Address
        m_ShipToAccount = New Account()
        m_BillToAccount = New Account()
    End Sub

    Public Property AccountNumber() As String
        Get
            Return m_AccountNumber
        End Get
        Set(ByVal value As String)
            m_AccountNumber = value
        End Set
    End Property
    Public Property soldToAccount() As String
        Get
            Return m_soldToAccount
        End Get
        Set(ByVal value As String)
            m_soldToAccount = value
        End Set
    End Property
    Public Property BillingOnly() As Short
        Get
            Return m_billingonly
        End Get
        Set(ByVal Value As Short)
            m_billingonly = Value
        End Set
    End Property
    Public Property YourReference() As String
        Get
            Return m_yourReference
        End Get
        Set(ByVal value As String)
            m_yourReference = value
        End Set
    End Property
    Public Property MessageType() As String
        Get
            Return m_messagetype
        End Get
        Set(ByVal value As String)
            m_messagetype = value
        End Set
    End Property
    Public Property MessageText() As String
        Get
            Return m_messagetext
        End Get
        Set(ByVal value As String)
            m_messagetext = value
        End Set
    End Property
    Public Property OrderStatus() As String
        Get
            Return m_orderstatus
        End Get
        Set(ByVal value As String)
            m_orderstatus = value
        End Set
    End Property
    Public Property Paymentcardresulttext() As String
        Get
            Return m_paymentcardresulttext
        End Get
        Set(ByVal value As String)
            m_paymentcardresulttext = value
        End Set
    End Property
    Public Property FirstName() As String
        Get
            Return m_first_name
        End Get
        Set(ByVal Value As String)
            m_first_name = Value
        End Set
    End Property
    Public Property Alt_Tax_Classification() As String
        Get
            Return m_alt_tax_classification
        End Get
        Set(ByVal Value As String)
            m_alt_tax_classification = Value
        End Set
    End Property
    Public Property EnableDocumentIDNumber() As String
        Get
            Return m_EnableDocumentIDNumber
        End Get
        Set(ByVal Value As String)
            m_EnableDocumentIDNumber = Value
        End Set
    End Property
    Public Property LastName() As String
        Get
            Return m_last_name
        End Get
        Set(ByVal Value As String)
            m_last_name = Value
        End Set
    End Property

    Public Property Customer() As Customer
        Get
            Return m_customer
        End Get
        Set(ByVal Value As Customer)
            m_customer = Value
        End Set
    End Property

    Public Property CreditCard() As CreditCard
        Get
            Return m_credit_card
        End Get
        Set(ByVal Value As CreditCard)
            m_credit_card = Value
        End Set
    End Property

    Public Property ShippingMethod() As ShippingMethod
        Get
            Return m_shipping_method
        End Get
        Set(ByVal Value As ShippingMethod)
            m_shipping_method = Value
        End Set
    End Property

    Public Property SequenceNumber() As String
        Get
            Return m_sequence_number
        End Get
        Set(ByVal Value As String)
            m_sequence_number = Value
        End Set
    End Property

    'Public Property TaxExempt2() As String
    '    Get
    '        Return m_tax_exempt
    '    End Get
    '    Set(ByVal Value As String)
    '        m_tax_exempt = Value
    '    End Set
    'End Property

    Public ReadOnly Property LineItems() As OrderLineItem()
        Get
            Dim items(m_order_line_items.Count - 1) As OrderLineItem
            m_order_line_items.CopyTo(items)
            Return items
        End Get
    End Property
    Public Property LineItems1() As OrderLineItem()
        Get
       
            Return arrOrderLineItem
        End Get
        Set(ByVal value As OrderLineItem())
            arrOrderLineItem = value
        End Set
    End Property
    'Commented By Navdeep kaur on 19th May 2011 for resolving 6397
    'Public Property ConfirmedInSIS() As Boolean
    '    Get
    '        Return m_confirmed_in_sis
    '    End Get
    '    Set(ByVal Value As Boolean)
    '        SetProperty(m_confirmed_in_sis, Value)
    '    End Set
    'End Property

    Public ReadOnly Property Invoices() As Invoice()
        Get
            If m_invoices Is Nothing Then
                Return Nothing
            Else
                Dim i(m_invoices.Count - 1) As Invoice
                m_invoices.CopyTo(i)
                Return i
            End If
        End Get
    End Property

    Public ReadOnly Property ContainsDownloadableItems() As Boolean
        Get
            Return downloadableItems()
        End Get
    End Property
    Public ReadOnly Property ContainsCertificateItems() As Boolean
        Get
            Return certificateItems()
        End Get
    End Property
    Public ReadOnly Property ContainsOnlineTrainingItems() As Boolean
        Get
            Return onlineTrainingItems()
        End Get
    End Property
    Public Function FindInvoice(ByVal invoice_number As String) As Invoice
        Dim inv As Invoice
        For Each i As Invoice In Invoices
            If i.InvoiceNumber = invoice_number Then
                inv = i
                Exit For
            End If
        Next
        Return inv
    End Function

    Private Function downloadableItems() As Boolean
        Dim returnValue As Boolean = False
        For Each thisLineItem As OrderLineItem In m_order_line_items
            If thisLineItem.Downloadable Then
                returnValue = True
            End If
        Next
        Return returnValue
    End Function
    Private Function certificateItems() As Boolean
        Dim returnValue As Boolean = False
        For Each thisLineItem As OrderLineItem In m_order_line_items
            If thisLineItem.Product.SoftwareItem.Type = Product.ProductType.Certificate Then
                returnValue = True
            End If
        Next
        Return returnValue
    End Function
    Private Function onlineTrainingItems() As Boolean
        Dim returnValue As Boolean = False
        For Each thisLineItem As OrderLineItem In m_order_line_items
            If thisLineItem.Product.SoftwareItem.Type = Product.ProductType.OnlineTraining Then
                returnValue = True
            End If
        Next
        Return returnValue
    End Function

    Public Sub AddLineItem(ByVal line_item As OrderLineItem)
        If m_order_line_items Is Nothing Then m_order_line_items = New ArrayList()
        m_order_line_items.Add(line_item)
    End Sub

    Public Sub AddInvoice(ByVal invoice As Invoice)
        m_invoices.Add(invoice)
    End Sub

    Public Sub ClearInvoiceList()
        m_invoices.Clear()
    End Sub

    Public Property OrderNumber() As String
        Get
            Return m_order_number
        End Get
        Set(ByVal Value As String)
            m_order_number = Value
        End Set
    End Property

    Public Property OrderDate() As Date
        Get
            Return m_order_date
        End Get
        Set(ByVal Value As Date)
            m_order_date = Value
        End Set
    End Property
    Public Property UpdateDate() As Date
        Get
            Return m_update_date
        End Get
        Set(ByVal Value As Date)
            m_update_date = Value
        End Set
    End Property

    Public Property VerisgnAuthorizationCode() As String
        Get
            Return m_verisign_authorization_code
        End Get
        Set(ByVal Value As String)
            m_verisign_authorization_code = Value
        End Set
    End Property
    'Commented By Navdeep kaur on 19th May 2011 for resolving 6397
    'Public Property VerisignTransactionCode() As String
    '    Get
    '        Return m_verisign_transaction_code
    '    End Get
    '    Set(ByVal Value As String)
    '        m_verisign_transaction_code = Value
    '    End Set
    'End Property
    Public Property VerisignVoidTransactionCode() As String
        Get
            Return m_verisign_VOid_transaction_code
        End Get
        Set(ByVal Value As String)
            m_verisign_VOid_transaction_code = Value
        End Set
    End Property

    Public Property CarrierCode() As String
        Get
            Return m_carrier_code
        End Get
        Set(ByVal Value As String)
            m_carrier_code = Value
        End Set
    End Property

    Public Property GroupNumber() As String
        Get
            Return m_group_number
        End Get
        Set(ByVal Value As String)
            m_group_number = Value
        End Set
    End Property

    Public Property BillTo() As Address
        Get
            Return m_bill_to
        End Get
        Set(ByVal Value As Address)
            m_bill_to = Value
        End Set
    End Property

    Public Property SAPBillToAccountNumber() As String
        Get
            Return m_sap_bill_to_account_number
        End Get
        Set(ByVal Value As String)
            m_sap_bill_to_account_number = Value
        End Set
    End Property

    Public Property ShipTo() As Address
        Get
            Return m_ship_to
        End Get
        Set(ByVal Value As Address)
            m_ship_to = Value
        End Set
    End Property
    Public Property ShipToAccount() As Account
        Get
            Return m_ShipToAccount
        End Get
        Set(ByVal Value As Account)
            m_ShipToAccount = Value
        End Set
    End Property
    Public Property BillToAccount() As Account
        Get
            Return m_BillToAccount
        End Get
        Set(ByVal Value As Account)
            m_BillToAccount = Value
        End Set
    End Property

    Public Property CreditReason() As String
        Get
            Return m_credit_reason
        End Get
        Set(ByVal Value As String)
            m_credit_reason = Value
        End Set
    End Property

    Public Property CreditSwitch() As Boolean
        Get
            Return m_credit_switch
        End Get
        Set(ByVal Value As Boolean)
            m_credit_switch = Value
        End Set
    End Property

    Public Property CustomerClass() As String
        Get
            Return m_customer_class
        End Get
        Set(ByVal Value As String)
            m_customer_class = Value
        End Set
    End Property

    Public Property InternalOrder() As Boolean
        Get
            Return m_internal_order
        End Get
        Set(ByVal Value As Boolean)
            m_internal_order = Value
        End Set
    End Property

    Public Property LegacyAccountNumber() As String
        Get
            Return m_legacy_account_number
        End Get
        Set(ByVal Value As String)
            m_legacy_account_number = Value
        End Set
    End Property

    Public Property POReference() As String
        Get
            Return m_po_reference
        End Get
        Set(ByVal Value As String)
            m_po_reference = Value
        End Set
    End Property

    Public Property AllocatedShipping() As Double
        Get
            Return m_allocated_shipping_amt
        End Get
        Set(ByVal Value As Double)
            m_allocated_shipping_amt = Value
        End Set
    End Property

    Public Property AllocatedTax() As Double
        Get
            Return m_allocated_tax_amt
        End Get
        Set(ByVal Value As Double)
            m_allocated_tax_amt = Value
        End Set
    End Property
    Public Property AllocatedSpecialTax() As Double
        Get
            Return m_allocated_specialtax_amt
        End Get
        Set(ByVal Value As Double)
            m_allocated_specialtax_amt = Value
        End Set
    End Property

    Public Property AllocatedRecyclingFee() As Double
        Get
            Return m_allocated_specialtax_amt
        End Get
        Set(ByVal Value As Double)
            m_allocated_specialtax_amt = Value
        End Set
    End Property


    Public Property AllocatedGrandTotal() As Double
        Get
            Return m_allocated_grand_total_amt
        End Get
        Set(ByVal Value As Double)
            m_allocated_grand_total_amt = Value
        End Set
    End Property
    Public Property ContainsDownloadAndShipItem() As Boolean
        Get
            Return m_contain_downloadship_item
        End Get
        Set(ByVal Value As Boolean)
            m_contain_downloadship_item = Value
        End Set
    End Property

    Public Property AllocatedPartSubTotal() As Double
        Get
            Return m_allocated_part_subtotal_amt
        End Get
        Set(ByVal Value As Double)
            m_allocated_part_subtotal_amt = Value
        End Set
    End Property

    Public Shared Property SortOrder() As enumSortOrder
        Get
            SortOrder = m_SortOrder
        End Get
        Set(ByVal Value As enumSortOrder)
            m_SortOrder = Value
        End Set
    End Property

    Public Property HTTP_X_Forward_For() As String
        Get
            Return m_HttpXForwardFor
        End Get
        Set(ByVal Value As String)
            m_HttpXForwardFor = Value
        End Set
    End Property
    Public Property RemoteADDR() As String
        Get
            Return m_RemoteADDR
        End Get
        Set(ByVal Value As String)
            m_RemoteADDR = Value
        End Set
    End Property
    Public Property HTTPReferer() As String
        Get
            Return m_HTTPReferer
        End Get
        Set(ByVal Value As String)
            m_HTTPReferer = Value
        End Set
    End Property
    Public Property HTTPURL() As String
        Get
            Return m_HTTPURL
        End Get
        Set(ByVal Value As String)
            m_HTTPURL = Value
        End Set
    End Property
    Public Property HTTPUserAgent() As String
        Get
            Return m_HTTPUserAgent
        End Get
        Set(ByVal Value As String)
            m_HTTPUserAgent = Value
        End Set
    End Property
    Public Property TransactionType() As Short
        Get
            Return m_TransactionType
        End Get
        Set(ByVal Value As Short)
            m_TransactionType = Value
        End Set
    End Property

    Public Function CompareTo(ByVal obj As Object) As Integer Implements System.IComparable.CompareTo
        Select Case (m_SortOrder)
            Case 0
                CompareTo = OrderNumber < CType(obj, Order).OrderNumber
            Case 1
                CompareTo = OrderNumber > CType(obj, Order).OrderNumber
            Case 2
                CompareTo = SAPBillToAccountNumber < CType(obj, Order).SAPBillToAccountNumber
            Case 3
                CompareTo = SAPBillToAccountNumber > CType(obj, Order).SAPBillToAccountNumber
            Case 4
                CompareTo = OrderDate < CType(obj, Order).OrderDate
            Case 5
                CompareTo = OrderDate > CType(obj, Order).OrderDate
            Case 6
                CompareTo = POReference < CType(obj, Order).POReference
            Case 7
                CompareTo = POReference > CType(obj, Order).POReference
            Case 8
                CompareTo = BillTo.Line1 < CType(obj, Order).BillTo.Line1
            Case 9
                CompareTo = BillTo.Line1 > CType(obj, Order).BillTo.Line1
        End Select
    End Function
End Class
