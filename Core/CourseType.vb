Public Class CourseType
    Inherits CoreObject

    Private m_code As String
    Private m_description As String
    Private m_PartNumber As String
    Private m_TypeCode As Integer = -1
    Public Property TypeCode() As Integer
        Get
            Return m_TypeCode
        End Get
        Set(ByVal Value As Integer)
            m_TypeCode = Value
        End Set
    End Property


    Public Property Code() As String
        Get
            Return m_code
        End Get
        Set(ByVal Value As String)
            m_code = Value
        End Set
    End Property

    Public Property Description() As String
        Get
            Return m_description
        End Get
        Set(ByVal Value As String)
            m_description = Value
        End Set
    End Property

    Public Property PartNumber() As String
        Get
            Return m_PartNumber
        End Get
        Set(ByVal Value As String)
            m_PartNumber = Value
        End Set
    End Property
End Class
