﻿'''<remarks/>

Public Class SAPPartInquiryMapping

    Private errorNumberField As Long

    Private errorMessageField As String

    Private partsField() As SAPProxyPart

    '''<remarks/>
    Public Property ErrorNumber() As Long
        Get
            Return errorNumberField
        End Get
        Set(ByVal value As Long)
            errorNumberField = value
        End Set
    End Property

    '''<remarks/>
    Public Property ErrorMessage() As String
        Get
            Return errorMessageField
        End Get
        Set(ByVal value As String)
            errorMessageField = value
        End Set
    End Property

    '''<remarks/>
    Public Property Parts() As SAPProxyPart()
        Get
            Return partsField
        End Get
        Set(ByVal value As SAPProxyPart())
            partsField = value
        End Set
    End Property
End Class
'This is a copy of Part class from SIS web reference
Public Class SAPProxyPart

    Private numberField As String
	Private statusFlagField As String
	Private statusMessageField As String
	Private replacementField As String
	Private descriptionField As String
	Private availabilityField As Long
	Private listPriceField As Double
	Private yourPriceField As Double
	Private coreChargeField As Double
	Private programCodeField As String
	Private categoryField As String
    Private billingOnlyField As String
    Private _kit() As SAPProxyKit
    Private _hasKit As Boolean
    Private recyclingFlagField As String
    Private _lineItem As ShoppingCartItem()
	Private _materialtype As String

	Public Property MaterialType() As String
		Get
			Return _materialtype
        End Get
		Set(ByVal value As String)
            _materialtype = value
		End Set
	End Property
	Public Property BillingOnly() As String
		Get
			Return billingOnlyField
        End Get
		Set(ByVal value As String)
            billingOnlyField = value
		End Set
	End Property
    Public Property hasKit() As Boolean
        Get
            Return _hasKit
        End Get
        Set(ByVal value As Boolean)
            _hasKit = value
        End Set
    End Property
    Public Property kit() As SAPProxyKit()
        Get
            Return _kit
        End Get
        Set(ByVal value As SAPProxyKit())
            _kit = value
        End Set
    End Property
    Public Property Number() As String
        Get
            Return numberField
        End Get
        Set(ByVal value As String)
            numberField = value
        End Set
    End Property
    Public Property lineItem() As ShoppingCartItem()
        Get
            Return _lineItem
        End Get
        Set(ByVal value As ShoppingCartItem())
            _lineItem = value
        End Set
    End Property

	Public Property StatusFlag() As String
		Get
			Return statusFlagField
        End Get
		Set(ByVal value As String)
            statusFlagField = value
		End Set
	End Property

	Public Property StatusMessage() As String
		Get
			Return statusMessageField
        End Get
		Set(ByVal value As String)
            statusMessageField = value
		End Set
	End Property

	Public Property Replacement() As String
		Get
			Return replacementField
        End Get
		Set(ByVal value As String)
            replacementField = value
		End Set
	End Property

	Public Property Description() As String
		Get
			Return descriptionField
        End Get
		Set(ByVal value As String)
            descriptionField = value
		End Set
	End Property

	Public Property Availability() As Long
		Get
			Return availabilityField
        End Get
		Set(ByVal value As Long)
            availabilityField = value
		End Set
	End Property

	Public Property ListPrice() As Double
		Get
			Return listPriceField
        End Get
		Set(ByVal value As Double)
            listPriceField = value
		End Set
	End Property

	Public Property YourPrice() As Double
		Get
			Return yourPriceField
        End Get
		Set(ByVal value As Double)
            yourPriceField = value
		End Set
	End Property

	Public Property CoreCharge() As Double
		Get
			Return coreChargeField
        End Get
		Set(ByVal value As Double)
            coreChargeField = value
		End Set
	End Property

	Public Property ProgramCode() As String
		Get
			Return programCodeField
        End Get
		Set(ByVal value As String)
            programCodeField = value
		End Set
	End Property

	Public Property Category() As String
		Get
			Return categoryField
        End Get
		Set(ByVal value As String)
            categoryField = value
		End Set
	End Property

	Public Property RecyclingFlag() As Double
		Get
			Return recyclingFlagField
        End Get
		Set(ByVal value As Double)
            recyclingFlagField = value
		End Set
	End Property
End Class
