Public Enum CertificateUseType
    Valid_NotInUse = 1
    UsedBySameCustomer = 2
    UsedByAnotherCustomer = 3
    NotAValidSerialNumber = 4
End Enum

Public Class ProductRegistration
    Inherits CoreObject
    Private m_location_address As RegisteredLocationAddress
    Private m_customer_address As RegisteredCustomerAddress
    Private m_products As ArrayList
    'Private m_shipto_address As ArrayList
    Private m_dealer As String
    Private m_optin As Boolean
    Private m_completed As Boolean = False
    Private m_IsAccountHolder As Boolean = False
    Private m_customerID As Integer
    Private m_customersequencenumber As Long = 0
    Private mValid As Boolean

    Private m_HttpXForwardFor As String = String.Empty
    Private m_RemoteADDR As String = String.Empty
    Private m_HTTPReferer As String = String.Empty
    Private m_HTTPURL As String = String.Empty
    Private m_HTTPUserAgent As String = String.Empty

    Public Sub New()
        m_location_address = New RegisteredLocationAddress
        m_customer_address = New RegisteredCustomerAddress
        m_products = New ArrayList
        'm_shipto_address = New ArrayList
    End Sub

    Public ReadOnly Property RegisteredProducts() As Product()
        Get
            Dim lproducts(m_products.Count - 1) As Product
            m_products.CopyTo(lproducts)
            Return lproducts
        End Get
    End Property
    'Public ReadOnly Property ShiptToAddress() As Product()
    '    Get
    '        Dim lproducts(m_products.Count - 1) As Product
    '        m_products.CopyTo(lproducts)
    '        Return lproducts
    '    End Get
    'End Property
    Public Property LocationAddress() As RegisteredLocationAddress
        Get
            If m_location_address Is Nothing Then
                mValid = False
                Return New RegisteredLocationAddress
            End If
            Return m_location_address
        End Get
        Set(ByVal Value As RegisteredLocationAddress)
            m_location_address = Value
        End Set
    End Property

    Public Property CustomerAddress() As RegisteredCustomerAddress
        Get
            If m_customer_address Is Nothing Then
                mValid = False
                Return New RegisteredCustomerAddress
            End If
            Return m_customer_address
        End Get
        Set(ByVal Value As RegisteredCustomerAddress)
            m_customer_address = Value
        End Set
    End Property


    Public Property HTTP_X_Forward_For() As String
        Get
            Return m_HttpXForwardFor
        End Get
        Set(ByVal Value As String)
            m_HttpXForwardFor = Value
        End Set
    End Property
    Public Property RemoteADDR() As String
        Get
            Return m_RemoteADDR
        End Get
        Set(ByVal Value As String)
            m_RemoteADDR = Value
        End Set
    End Property
    Public Property HTTPReferer() As String
        Get
            Return m_HTTPReferer
        End Get
        Set(ByVal Value As String)
            m_HTTPReferer = Value
        End Set
    End Property
    Public Property HTTPURL() As String
        Get
            Return m_HTTPURL
        End Get
        Set(ByVal Value As String)
            m_HTTPURL = Value
        End Set
    End Property
    Public Property HTTPUserAgent() As String
        Get
            Return m_HTTPUserAgent
        End Get
        Set(ByVal Value As String)
            m_HTTPUserAgent = Value
        End Set
    End Property


#Region "Individual Property"

    Public ReadOnly Property Valid() As Boolean
        Get
            Return mValid
        End Get
    End Property

    Public Property Dealer() As String
        Get
            Return m_dealer
        End Get
        Set(ByVal value As String)
            m_dealer = value
        End Set
    End Property

    Public Property OptIN() As Boolean
        Get
            Return m_optin
        End Get
        Set(ByVal value As Boolean)
            m_optin = value
        End Set
    End Property

    Public Property Completed() As Boolean
        Get
            Return m_completed
        End Get
        Set(ByVal value As Boolean)
            m_completed = value
        End Set
    End Property

    Public Property CustomerID() As Long
        Get
            Return m_customerID
        End Get
        Set(ByVal value As Long)
            m_customerID = value
        End Set
    End Property

    Public Property CustomerSequenceNumber() As Integer
        Get
            Return m_customersequencenumber
        End Get
        Set(ByVal value As Integer)
            m_customersequencenumber = value
        End Set
    End Property


    Public Property IsAccountHolder() As Boolean
        Get
            Return m_IsAccountHolder
        End Get
        Set(ByVal value As Boolean)
            m_IsAccountHolder = value
        End Set
    End Property

#End Region

#Region "Method"
    Public Function AddProduct(ByVal regProduct As Product) As Boolean
        If m_products.Count = 1 Then
            If regProduct.IsDefault = True Then
                Return False
            Else
                If CType(m_products(0), Product).IsDefault = True Then
                    m_products.Remove(m_products(0))
                End If
            End If
        End If
        regProduct.ItemID = m_products.Count + 1
        m_products.Add(regProduct)
        Return True
    End Function

    Public Function AddProduct(ByVal ProductCataogry As String, ByVal CertificateNumber As String, ByVal ProductCode As String, ByVal ModelNum As String, ByVal PurchaseDate As String, ByVal SerialNumber As String, ByVal IsDefault As Boolean) As Boolean
        If m_products.Count = 1 Then
            If IsDefault = True Then
                Return False
            Else
                If CType(m_products(0), Product).IsDefault = True Then
                    m_products.Remove(m_products(0))
                End If
            End If
        End If
        m_products.Add(New Product(ProductCataogry, CertificateNumber, ProductCode, ModelNum, PurchaseDate, SerialNumber, m_products.Count + 1))
        Return True
    End Function

    Public Sub RemoveProduct(ByVal ProductToRemove As Product)
        m_products.Remove(ProductToRemove)
    End Sub
#End Region

#Region "Inner Class"
    Class Product
        Inherits CoreObject
        Private m_model_number As String
        Private m_product_code As String
        Private m_purchase_date As String
        Private m_serial_number As String
        Private m_certificate_number As String
        Private m_isdefault As Boolean = False
        Private m_itemNumber As Integer = 0
        Private m_duplicateCert As CertificateUseType = CertificateUseType.Valid_NotInUse
        Private m_product_catagory As String

        Public Sub New(ByVal ProductCataogry As String, ByVal CertificateNumber As String, ByVal ProductCode As String, ByVal ModelNum As String, ByVal PurchaseDate As String, ByVal SerialNumber As String, ByVal ItemNumber As Integer)
            m_model_number = ModelNum
            m_product_code = ProductCode
            m_purchase_date = PurchaseDate
            m_serial_number = SerialNumber
            m_certificate_number = CertificateNumber
            m_itemNumber = ItemNumber
            m_product_catagory = ProductCataogry
            If (SerialNumber = String.Empty Or SerialNumber = "") Then
                m_isdefault = True
            End If
        End Sub

        Public ReadOnly Property IsDefault() As Boolean
            Get
                Return m_isdefault
            End Get
        End Property

        Public Property CertificateStatus() As CertificateUseType
            Get
                Return m_duplicateCert
            End Get
            Set(ByVal Value As CertificateUseType)
                m_duplicateCert = Value
            End Set
        End Property
        
        Public Property CertificateNumber() As String
            Get
                Return m_certificate_number
            End Get
            Set(ByVal Value As String)
                SetProperty(m_certificate_number, Value)
            End Set
        End Property

        Public Property SerialNumber() As String
            Get
                Return m_serial_number
            End Get
            Set(ByVal Value As String)
                SetProperty(m_serial_number, Value)
            End Set
        End Property

        Public Property ModelNumber() As String
            Get
                Return m_model_number
            End Get
            Set(ByVal Value As String)
                SetProperty(m_model_number, Value)
            End Set
        End Property

        Public Property ProductCode() As String
            Get
                Return m_product_code
            End Get
            Set(ByVal Value As String)
                SetProperty(m_product_code, Value)
            End Set
        End Property

        Public Property PurchaseDate() As String
            Get
                Return m_purchase_date
            End Get
            Set(ByVal Value As String)
                SetProperty(m_purchase_date, Value)
            End Set
        End Property

        Public Property ItemID() As Integer
            Get
                Return m_itemNumber
            End Get
            Set(ByVal Value As Integer)
                m_itemNumber = Value
            End Set
        End Property

        Public Property ProductCatagory() As String
            Get
                Return m_product_catagory
            End Get
            Set(ByVal Value As String)
                SetProperty(m_product_catagory, Value)
            End Set
        End Property

    End Class

    Class RegisteredCustomerAddress
        Inherits CustomerAddr
        Private m_CustomerContactEmailAddress As String = String.Empty
        Private m_company_name As String = ""
        Private m_first_name As String = ""
        Private m_last_name As String = ""
        Private m_email_address As String = ""

        Public Property ContactEmailAddress() As String
            Get
                Return m_CustomerContactEmailAddress
            End Get
            Set(ByVal value As String)
                m_CustomerContactEmailAddress = value
            End Set
        End Property

        Public Property CompanyName() As String
            Get
                Return m_company_name
            End Get
            Set(ByVal Value As String)
                SetProperty(m_company_name, Value)
            End Set
        End Property
        Public Property FirstName() As String
            Get
                Return m_first_name
            End Get
            Set(ByVal Value As String)
                SetProperty(m_first_name, Value)
            End Set
        End Property

        Public Property LastName() As String
            Get
                Return m_last_name
            End Get
            Set(ByVal Value As String)
                SetProperty(m_last_name, Value)
            End Set
        End Property

        Public Property EmailAddress() As String
            Get
                Return m_email_address
            End Get
            Set(ByVal Value As String)
                SetProperty(m_email_address, Value)
            End Set
        End Property
    End Class

    Class RegisteredLocationAddress
        Inherits CustomerAddr
        Private m_LocationContactEmailAddress As String = String.Empty
        Private m_company_name As String = ""
        Private m_first_name As String = ""
        Private m_last_name As String = ""
        Private m_email_address As String = ""
        Public Property ContactEmailAddress() As String
            Get
                Return m_LocationContactEmailAddress
            End Get
            Set(ByVal value As String)
                m_LocationContactEmailAddress = value
            End Set
        End Property
        Public Property CompanyName() As String
            Get
                Return m_company_name
            End Get
            Set(ByVal Value As String)
                SetProperty(m_company_name, Value)
            End Set
        End Property
        Public Property FirstName() As String
            Get
                Return m_first_name
            End Get
            Set(ByVal Value As String)
                SetProperty(m_first_name, Value)
            End Set
        End Property

        Public Property LastName() As String
            Get
                Return m_last_name
            End Get
            Set(ByVal Value As String)
                SetProperty(m_last_name, Value)
            End Set
        End Property

        Public Property EmailAddress() As String
            Get
                Return m_email_address
            End Get
            Set(ByVal Value As String)
                SetProperty(m_email_address, Value)
            End Set
        End Property
    End Class
#End Region
End Class

