'
' Class: defines the model details as Model Name and Description
' used in Model Catalog of SPS.
' 
Public Class ModelDetails
    Inherits CoreObject

    Private m_modelname As String
    Private m_description As String
    Private m_discont As String
    Private m_to_discont As Short

    Public Property ModelName() As String
        Get
            Return m_modelname
        End Get
        Set(ByVal Value As String)
            SetProperty(m_modelname, Value)
        End Set
    End Property

    Public Property ModelDescription() As String
        Get
            Return m_description
        End Get
        Set(ByVal Value As String)
            SetProperty(m_description, Value)
        End Set
    End Property


    Public Property DiscontinuedFlag() As String
        Get
            Return m_discont
        End Get
        Set(ByVal Value As String)
            SetProperty(m_discont, Value)
        End Set
    End Property

    'If a model do not have services center any one of Teaneck, Los Angeles, San Jose then Model should be treated as discontinued
    Public Property To_DISCONT() As Boolean
        Get
            Return Not m_to_discont
        End Get
        Set(ByVal Value As Boolean)
            m_to_discont = Value
        End Set
    End Property
End Class
