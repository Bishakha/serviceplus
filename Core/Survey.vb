
Imports System.Collections

Public Class Survey

    Private m_description As String
    Private m_survey_id As String
    Private m_effective_date As Date
    Private m_expiration_date As Date
    Private m_questions As ArrayList

    Public Sub New()
        m_questions = New ArrayList
    End Sub

    Public Property Description() As String
        Get
            Return m_description
        End Get
        Set(ByVal Value As String)
            m_description = Value
        End Set
    End Property

    Public Property SurveyID() As String
        Get
            Return m_survey_id
        End Get
        Set(ByVal Value As String)
            m_survey_id = Value
        End Set
    End Property

    Public Property EffectiveDate() As Date
        Get
            Return m_effective_date
        End Get
        Set(ByVal Value As Date)
            m_effective_date = Value
        End Set
    End Property

    Public Property ExpirationDate() As Date
        Get
            Return m_expiration_date
        End Get
        Set(ByVal Value As Date)
            m_expiration_date = Value
        End Set
    End Property

    Public ReadOnly Property Questions() As Question()
        Get
            Dim questions_tmp(m_questions.Count - 1) As Question
            m_questions.CopyTo(questions_tmp)
            Return questions_tmp
        End Get
    End Property

    Public Sub AddQuestion(ByVal question As Question)
        m_questions.Add(question)
    End Sub

End Class
