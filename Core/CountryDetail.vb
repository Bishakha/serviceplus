﻿Public Class CountryDetail
    Inherits CoreObject

    Private m_Country_name As String
    Private m_country_Code As String


    Public Property CountryName() As String
        Get
            Return m_Country_name
        End Get
        Set(ByVal Value As String)
            SetProperty(m_Country_name, Value)
        End Set
    End Property

    Public Property CountryCode() As String
        Get
            Return m_country_Code
        End Get
        Set(ByVal Value As String)
            SetProperty(m_country_Code, Value)
        End Set
    End Property
End Class

