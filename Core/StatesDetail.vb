'
' State Class, to define the state details
' used to populate DDL in application
'
Public Class StatesDetail
    Inherits CoreObject

    Private m_state_name As String
    Private m_state_abbr As String

    Public Property StateName() As String
        Get
            Return m_state_name
        End Get
        Set(ByVal Value As String)
            SetProperty(m_state_name, Value)
        End Set
    End Property

    Public Property StateAbbr() As String
        Get
            Return m_state_abbr
        End Get
        Set(ByVal Value As String)
            SetProperty(m_state_abbr, Value)
        End Set
    End Property

End Class
