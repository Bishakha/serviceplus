Public Class CourseMinorCategory
    Inherits CoreObject

    Private m_code As Integer
    Private m_MajorCategoryCode As Integer
    Private m_description As String
    Private m_mejor_minor_description As String

    Public Sub New(Optional ByVal description As String = "")
        If Not String.IsNullOrWhiteSpace(description) Then m_description = description
    End Sub

    Public Property Code() As Integer
        Get
            Return m_code
        End Get
        Set(ByVal Value As Integer)
            m_code = Value
        End Set
    End Property

    Public Property MajorCategoryCode() As Integer
        Get
            Return m_MajorCategoryCode
        End Get
        Set(ByVal Value As Integer)
            m_MajorCategoryCode = Value
        End Set
    End Property

    Public Property Description() As String
        Get
            Return m_description
        End Get
        Set(ByVal Value As String)
            m_description = Value
        End Set
    End Property

    Public Property Mejor_Minor_Description() As String
        Get
            Return m_mejor_minor_description
        End Get
        Set(ByVal Value As String)
            m_mejor_minor_description = Value
        End Set
    End Property
End Class
