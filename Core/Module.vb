Public Class _Module

    Private m_module_id As String
    Private m_module_name As String
    Private m_has_diagram As Boolean

    Public Property ModuleID() As String
        Get
            Return m_module_id
        End Get
        Set(ByVal Value As String)
            m_module_id = Value
        End Set
    End Property

    Public Property Name() As String
        Get
            Return m_module_name
        End Get
        Set(ByVal Value As String)
            m_module_name = Value
        End Set
    End Property

    Public Property HasDiagram() As Boolean
        Get
            Return m_has_diagram
        End Get
        Set(ByVal Value As Boolean)
            m_has_diagram = Value
        End Set
    End Property

End Class
