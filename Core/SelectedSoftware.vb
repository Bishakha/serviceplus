Public Class SelectedSoftware
    Inherits Software


    Private m_certfication As Boolean
    Private m_onlinetraining As Boolean
    Private m_classroomtraining As Boolean

 
    Public Property Certification() As Boolean
        Get
            Return m_certfication
        End Get
        Set(ByVal Value As Boolean)
            m_certfication = Value
        End Set
    End Property
    Public Property OnlineTraining() As Boolean
        Get
            Return m_onlinetraining
        End Get
        Set(ByVal Value As Boolean)
            m_onlinetraining = Value
        End Set
    End Property
    Public Property ClassRoomTraining() As Boolean
        Get
            Return m_classroomtraining
        End Get
        Set(ByVal Value As Boolean)
            m_classroomtraining = Value
        End Set
    End Property
 

End Class
