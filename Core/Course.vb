Public Class Course
    Inherits CoreObject

    Private m_CourseNumber As String = String.Empty
    Private m_Title As String = String.Empty
    Private m_Description As String = String.Empty
    Private m_Type As CourseType
    Private m_Notes As String = String.Empty
    Private m_SequenceNumber As Integer
    Private m_StatusCode As Integer
    Private m_MinorCategory As CourseMinorCategory
    Private m_MajorCategory As CourseMajorCategory
    Private m_UpdateDate As Date
    Private m_PreRequisite As String
    Private m_Model As String = String.Empty
    Private m_DefaultLocationList As String = String.Empty
    Private m_ListPrice As Double
    Private m_Hide As Integer
    Private m_TrainingURL As String = String.Empty
    Private m_URL As String = String.Empty

    Public Property TrainingURL() As String
        Get
            Return m_TrainingURL
        End Get
        Set(ByVal Value As String)
            m_TrainingURL = Value
        End Set
    End Property


    Public Property Number() As String
        Get
            Return m_CourseNumber
        End Get
        Set(ByVal Value As String)
            m_CourseNumber = Value
        End Set
    End Property

    Public Property Title() As String
        Get
            Return m_Title
        End Get
        Set(ByVal Value As String)
            m_Title = Value
        End Set
    End Property
    Public Property ListPrice() As Double
        Get
            Return m_ListPrice
        End Get
        Set(ByVal Value As Double)
            m_ListPrice = Value
        End Set
    End Property

    Public Property Description() As String
        Get
            Return m_Description
        End Get
        Set(ByVal Value As String)
            m_Description = Value
        End Set
    End Property

    Public Property Type() As CourseType
        Get
            If m_Type Is Nothing Then
                m_Type = New CourseType
            End If
            Return m_Type
        End Get
        Set(ByVal Value As CourseType)
            m_Type = Value
        End Set
    End Property

    Public Property Notes() As String
        Get
            Return m_Notes
        End Get
        Set(ByVal Value As String)
            m_Notes = Value
        End Set
    End Property

    Public Property SequenceNumber() As Integer
        Get
            Return m_SequenceNumber
        End Get
        Set(ByVal Value As Integer)
            m_SequenceNumber = Value
        End Set
    End Property

    Public Property StatusCode() As Integer
        Get
            Return m_StatusCode
        End Get
        Set(ByVal Value As Integer)
            m_StatusCode = Value
        End Set
    End Property

    Public Property MinorCategory() As CourseMinorCategory
        Get
            If m_MinorCategory Is Nothing Then
                m_MinorCategory = New CourseMinorCategory
            End If
            Return m_MinorCategory
        End Get
        Set(ByVal Value As CourseMinorCategory)
            m_MinorCategory = Value
        End Set
    End Property
    Public Property MajorCategory() As CourseMajorCategory
        Get
            If m_MajorCategory Is Nothing Then
                m_MajorCategory = New CourseMajorCategory
            End If
            Return m_MajorCategory
        End Get
        Set(ByVal Value As CourseMajorCategory)
            m_MajorCategory = Value
        End Set
    End Property
    Public Property UpdateDate() As Date
        Get
            Return m_UpdateDate
        End Get
        Set(ByVal Value As Date)
            m_UpdateDate = Value
        End Set
    End Property

    Public Property PreRequisite() As String
        Get
            Return m_PreRequisite
        End Get
        Set(ByVal Value As String)
            m_PreRequisite = Value
        End Set
    End Property

    Public Property Model() As String
        Get
            Return m_Model
        End Get
        Set(ByVal Value As String)
            m_Model = Value
        End Set
    End Property
    Public Property DefaultLocationList() As String
        Get
            Return m_DefaultLocationList
        End Get
        Set(ByVal Value As String)
            m_DefaultLocationList = Value
        End Set
    End Property

    Public Property Hide() As Integer
        Get
            Return m_Hide
        End Get
        Set(ByVal Value As Integer)
            m_Hide = Value
        End Set
    End Property

    Public Property URL() As String
        Get
            Return m_URL
        End Get
        Set(ByVal Value As String)
            m_URL = Value
        End Set
    End Property

End Class
