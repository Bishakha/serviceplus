Public Class ExtendedWarrantyModel
    Inherits Software

    Private m_EWPurchaseDate As Date
    Private m_EWModelNumber As String = String.Empty
    Private m_EWSerialNumber As String = String.Empty
    Private m_CertificateNumber As String = String.Empty
    Private m_CertificateGUID As String = String.Empty


    Public Sub New(ByVal EWProductType As Product.ProductType)
        m_type = EWProductType
    End Sub

    Public Property CertificateNumber() As String
        Get
            Return m_CertificateNumber
        End Get
        Set(ByVal Value As String)
            m_CertificateNumber = Value
        End Set
    End Property
    Public Property CertificateGUID() As String
        Get
            Return m_CertificateGUID
        End Get
        Set(ByVal Value As String)
            m_CertificateGUID = Value
        End Set
    End Property
    Public Property EWPurchaseDate() As Date
        Get
            Return m_EWPurchaseDate
        End Get
        Set(ByVal Value As Date)
            m_EWPurchaseDate = Value
        End Set
    End Property
    Public Property EWModelNumber() As String
        Get
            Return m_EWModelNumber
        End Get
        Set(ByVal Value As String)
            m_EWModelNumber = Value
        End Set
    End Property
    Public Property EWSerialNumber() As String
        Get
            Return m_EWSerialNumber
        End Get
        Set(ByVal Value As String)
            m_EWSerialNumber = Value
        End Set
    End Property
End Class
