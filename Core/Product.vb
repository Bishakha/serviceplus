Public MustInherit Class Product

    'adding new types of products requires development work so specifying derivations via enum
    Public Enum ProductType
        Part = 0
        Software = 1
        Kit = 2
        ClassRoomTraining = 3
        ComputerBasedTraining = 4
        Certificate = 5
        OnlineTraining = 6
        'Not in Use = 7
        ExtendedWarranty = 8
    End Enum

    Private m_frb As String
    Protected m_type As ProductType
    Private m_TrainingCertificationCode As String = String.Empty
    Private m_isReplaced As Boolean = False

    Public Sub New()
        SpecialTax = 0.0
    End Sub

    Public Property Download() As Boolean

    Public Property BillingOnly() As Boolean

    Public Property Ship() As Boolean

    Public Property ListPrice() As Double

    Public Property ProgramCode() As String

    Public Function IsFRB() As Boolean
        If (String.IsNullOrEmpty(ProgramCode)) Then
            Return False
        Else
            Return ProgramCode = "F" '  Or ProgramCode = "I" Or ProgramCode = "H"
        End If
    End Function

    Public Function HasCoreCharge() As Boolean
        If (String.IsNullOrEmpty(ProgramCode)) Then
            Return False
        Else
            Return "IMSYZT".Contains(ProgramCode)
        End If
    End Function

    Public Property CoreCharge() As Double

    Public Property Description() As String

    Public Property PartNumber() As String
    'Added By Sneha
    Public Property ReplacementPartNumber() As String


    Public Property Category() As ProductCategory

    Public Property Type() As ProductType
        Get
            Return m_type
        End Get
        Set(ByVal Value As ProductType)
            m_type = Value
        End Set
    End Property

    Public Property BackOrderQuantity() As Short

    Public Property SpecialTax() As Double

    Public Property SoftwareItem() As Software
    Public Property TrainingActivationCode() As String

    Public Property TrainingTypeCode() As Short

    Public Property TrainingTitle() As String

    Public Property TrainingURL() As String

    Public Property TrainingCertificationCode() As String

    Public ReadOnly Property NumberOfCodeAvailable() As Short
        Get
            If String.IsNullOrWhiteSpace(m_TrainingCertificationCode) Then
                Return 0
            Else
                Return m_TrainingCertificationCode.Split(",").Length
            End If
        End Get
    End Property

    ' Added by Andrew Sleight for Shopping Cart Enhancement to track replaced parts
    Public Property IsReplaced() As Boolean

    Public Function DownloadFileName() As String
        Throw New NotImplementedException()
    End Function
End Class
