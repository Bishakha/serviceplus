Public Class Invoice
    Inherits CoreObject

    Private m_invoice_number As String
    Private m_order As Order
    Private m_tracking_number As String '5325
    'Private m_verisign_capture_txn_code As String
    'Private m_verisign_capture_auth_code As String
    'Private m_verisign_credit_txn_code As String
    'Private m_verisign_credit_auth_code As String
    Private m_total As Double
    'Private m_parts_sub_total As Double
    Private m_tax As Double
    Private m_specialTax As Double
    'Private m_shipping As Double
    Private m_invoice_date As Date
    'Private m_credit_order As Boolean
    'Private m_credit_restock_amount As Double
    Private m_credit_original_invoice_number As String
    Private m_invoice_line_items As ArrayList
    Private m_carriercode As String '5325
    Private m_shipping_method As ShippingMethod

    Public Sub New(ByRef invoiceNumber As String)
        m_invoice_number = invoiceNumber
    End Sub
    Public Sub New(ByVal order As Order)
        m_order = order
        m_invoice_line_items = New ArrayList
    End Sub

    Public Property InvoiceNumber() As String
        Get
            Return m_invoice_number
        End Get
        Set(ByVal Value As String)
            m_invoice_number = Value
        End Set
    End Property
    'Commented By Navdeep kaur on 18th May for resolving 6396
    '5325 starts
    Public Property TrackingNumber() As String
        Get
            Return m_tracking_number
        End Get
        Set(ByVal Value As String)
            m_tracking_number = Value
        End Set
    End Property
    '5325 ends

    'Public Property VerisignCaptureTxnCode() As String
    '    Get
    '        Return m_verisign_capture_txn_code
    '    End Get
    '    Set(ByVal Value As String)
    '        m_verisign_capture_txn_code = Value
    '    End Set
    'End Property

    'Public Property VerisignCaptureAuthCode() As String
    '    Get
    '        Return m_verisign_capture_auth_code
    '    End Get
    '    Set(ByVal Value As String)
    '        m_verisign_capture_auth_code = Value
    '    End Set
    'End Property

    'Public Property VerisignCreditTxnCode() As String
    '    Get
    '        Return m_verisign_credit_txn_code
    '    End Get
    '    Set(ByVal Value As String)
    '        m_verisign_credit_txn_code = Value
    '    End Set
    'End Property

    'Public Property VerisignCreditAuthCode() As String
    '    Get
    '        Return m_verisign_credit_auth_code
    '    End Get
    '    Set(ByVal Value As String)
    '        m_verisign_credit_auth_code = Value
    '    End Set
    'End Property
    'End of Comment By Navdeep kaur on 18th May for resolving 6396

    Public Property Total() As Double
        Get
            Return m_total
        End Get
        Set(ByVal Value As Double)
            m_total = Value
        End Set
    End Property
    'Commented By Navdeep kaur on 18th May for resolving 6396
    'Public Property PartsSubTotal() As Double
    '    Get
    '        Return m_parts_sub_total
    '    End Get
    '    Set(ByVal Value As Double)
    '        m_parts_sub_total = Value
    '    End Set
    'End Property

    'Public Property Tax() As Double
    '    Get
    '        Return m_tax
    '    End Get
    '    Set(ByVal Value As Double)
    '        m_tax = Value
    '    End Set
    'End Property
    'End of Comment By Navdeep kaur on 18th May for resolving 6396
    Public Property SpecialTax() As Double
        Get
            Return m_specialTax
        End Get
        Set(ByVal Value As Double)
            m_specialTax = Value
        End Set
    End Property
    'Commented By Navdeep kaur on 18th May for resolving 6396
    'Public Property Shipping() As Double
    '    Get
    '        Return m_shipping
    '    End Get
    '    Set(ByVal Value As Double)
    '        m_shipping = Value
    '    End Set
    'End Property
    'End of Comment By Navdeep kaur on 18th May for resolving 6396

    Public Property InvoiceDate() As Date
        Get
            Return m_invoice_date
        End Get
        Set(ByVal Value As Date)
            m_invoice_date = Value
        End Set
    End Property
    'Commented By Navdeep kaur on 18th May for resolving 6396
    'Public Property CreditOrder() As Boolean
    '    Get
    '        Return m_credit_order
    '    End Get
    '    Set(ByVal Value As Boolean)
    '        m_credit_order = Value
    '    End Set
    'End Property

    'Public Property CreditRestockAmount() As Double
    '    Get
    '        Return m_credit_restock_amount
    '    End Get
    '    Set(ByVal Value As Double)
    '        m_credit_restock_amount = Value
    '    End Set
    'End Property
    'End of Comment By Navdeep kaur on 18th May for resolving 6396
    Public Property Order() As Order
        Get
            Return m_order
        End Get
        Set(ByVal value As Order)
            m_order = value
        End Set
    End Property

    Public Sub AddLineItem(ByVal line_item As InvoiceLineItem)
        m_invoice_line_items.Add(line_item)
    End Sub

    Public ReadOnly Property LineItems() As InvoiceLineItem()
        Get
            Dim items(m_invoice_line_items.Count - 1) As InvoiceLineItem
            m_invoice_line_items.CopyTo(items)
            Return items
        End Get
    End Property
    Public Property CreditOriginalInvoiceNumber() As String
        Get
            Return m_credit_original_invoice_number
        End Get
        Set(ByVal Value As String)
            m_credit_original_invoice_number = Value
        End Set
    End Property
    'Commented By Navdeep kaur on 18th May for resolving 6396
    '5325 starts
    Public Property CarrierCode() As String
        Get
            Return m_carriercode
        End Get
        Set(ByVal Value As String)
            m_carriercode = Value
        End Set
    End Property
    '5325 ends

    Public Property ShippingMethod() As ShippingMethod
        Get
            Return m_shipping_method
        End Get
        Set(ByVal Value As ShippingMethod)
            m_shipping_method = Value
        End Set
    End Property
End Class
