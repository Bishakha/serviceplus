﻿using System;
using System.Data;
using Oracle.ManagedDataAccess.Client;
using SoftwarePlus.DataAccess;


namespace SoftwarePlus.BusinessAccess {
    public class Model {
        SoftwarePlusConfigManager configManager;
        string connString;

        #region Constructor
        public Model() {
            configManager = new SoftwarePlusConfigManager();
            connString = configManager.SoftwarePlusDBConnectionString();
        }
        #endregion

        #region Public Methods
        public void AddModel(string ModelID, int ModelStatus, string ModelType, string ModelDescription, int MainAssembly, int PriorVersionRequied, int CustomerTracking, string ProductionSource, string UpgradeEstimate, string Comments, string Notes, string CreatedBy, DateTime DateCreated) {
            try {
                OracleConnection orcleCon = new OracleConnection(connString);
                OracleParameter[] oraParam = new OracleParameter[13];

                oraParam[0] = new OracleParameter("xModelID", ModelID);
                oraParam[1] = new OracleParameter("xModelStatus", ModelStatus);
                oraParam[2] = new OracleParameter("xModelType", ModelType);
                oraParam[3] = new OracleParameter("xModelDescription", ModelDescription);
                oraParam[4] = new OracleParameter("xMainAssembly", MainAssembly);
                oraParam[5] = new OracleParameter("xPriorVersionRequied", PriorVersionRequied);
                oraParam[6] = new OracleParameter("xCustomerTracking", CustomerTracking);
                oraParam[7] = new OracleParameter("xProductionSource", ProductionSource);
                oraParam[8] = new OracleParameter("xUpgradeEstimate", UpgradeEstimate);
                oraParam[9] = new OracleParameter("xComments", Comments);
                oraParam[10] = new OracleParameter("xNotes", Notes);
                oraParam[11] = new OracleParameter("xCreatedBy", CreatedBy);
                oraParam[12] = new OracleParameter("xDateCreated", DateCreated);
                // oraParam[13] = new OracleParameter("xUpdateBy", UpdateBy);
                // oraParam[14] = new OracleParameter("xLastUpdate", LastUpdate);

                OracleDataAccess.ExecuteNonQuery(orcleCon, CommandType.StoredProcedure, "ADDMODEL", oraParam);
            } catch (Exception ex) {
                throw ex;
            }
        }

        public void AddChildModel(string modelId, string childModelId) {
            try {
                OracleConnection orcleCon = new OracleConnection(connString);
                OracleParameter[] oraParam = new OracleParameter[2];
                oraParam[0] = new OracleParameter("xModelID", modelId);
                oraParam[1] = new OracleParameter("xChildModel", childModelId);
                OracleDataAccess.ExecuteNonQuery(orcleCon, CommandType.StoredProcedure, "ADDCHILDMODEL", oraParam);
            } catch (Exception ex) {
                throw ex;
            }
        }

        public void DeleteChildModel(string modelId, string childModelId) {
            try {
                OracleConnection orcleCon = new OracleConnection(connString);
                OracleParameter[] oraParam = new OracleParameter[2];
                oraParam[0] = new OracleParameter("xModelID", modelId);
                oraParam[1] = new OracleParameter("xChildModel", childModelId);
                OracleDataAccess.ExecuteNonQuery(orcleCon, CommandType.StoredProcedure, "DELETECHILDMODEL ", oraParam);
            } catch (Exception ex) {
                throw ex;
            }
        }

        public void DeleteModel(string modelId) {
            try {
                OracleConnection orcleCon = new OracleConnection(connString);
                OracleParameter[] oraParam = new OracleParameter[1];
                oraParam[0] = new OracleParameter("xModelID", modelId);

                OracleDataAccess.ExecuteNonQuery(orcleCon, CommandType.StoredProcedure, "DELETEMODEL", oraParam);
            } catch (Exception ex) {
                throw ex;
            }
        }

        public void UpdateModel(string ModelID, int ModelStatus, string ModelType, string ModelDescription, int MainAssembly, int PriorVersionRequied, int CustomerTracking, string ProductionSource, int UpgradeEstimate, string Comments, string Notes, string UpdateBy, DateTime LastUpdate) {
            try {
                OracleConnection orcleCon = new OracleConnection(connString);
                OracleParameter[] oraParam = new OracleParameter[13];

                oraParam[0] = new OracleParameter("xModelID", ModelID);
                oraParam[1] = new OracleParameter("xModelStatus", ModelStatus);
                oraParam[2] = new OracleParameter("xModelType", ModelType);
                oraParam[3] = new OracleParameter("xModelDescription", ModelDescription);
                oraParam[4] = new OracleParameter("xMainAssembly", MainAssembly);
                oraParam[5] = new OracleParameter("xPriorVersionRequied", PriorVersionRequied);
                oraParam[6] = new OracleParameter("xCustomerTracking", CustomerTracking);
                oraParam[7] = new OracleParameter("xProductionSource", ProductionSource);
                oraParam[8] = new OracleParameter("xUpgradeEstimate", UpgradeEstimate);
                oraParam[9] = new OracleParameter("xComments", Comments);
                oraParam[10] = new OracleParameter("xNotes", Notes);
                //oraParam[11] = new OracleParameter("xCreatedBy", CreatedBy);
                //oraParam[12] = new OracleParameter("xDateCreated", DateCreated);
                oraParam[11] = new OracleParameter("xUpdateBy", UpdateBy);
                oraParam[12] = new OracleParameter("xLastUpdate", LastUpdate);

                OracleDataAccess.ExecuteNonQuery(orcleCon, CommandType.StoredProcedure, "UPDATEMODEL", oraParam);
            } catch (Exception ex) {
                throw ex;
            }
        }

        public void UpdateParentModel(string ModelID, int ModelStatus, string ModelType, string ModelDescription, int MainAssembly, int PriorVersionRequied, int CustomerTracking, string ProductionSource, int UpgradeEstimate) {
            try {
                OracleConnection orcleCon = new OracleConnection(connString);
                OracleParameter[] oraParam = new OracleParameter[9];

                oraParam[0] = new OracleParameter("xModelID", ModelID);
                oraParam[1] = new OracleParameter("xModelStatus", ModelStatus);
                oraParam[2] = new OracleParameter("xModelType", ModelType);
                oraParam[3] = new OracleParameter("xModelDescription", ModelDescription);
                oraParam[4] = new OracleParameter("xMainAssembly", MainAssembly);
                oraParam[5] = new OracleParameter("xPriorVersionRequied", PriorVersionRequied);
                oraParam[6] = new OracleParameter("xCustomerTracking", CustomerTracking);
                oraParam[7] = new OracleParameter("xProductionSource", ProductionSource);
                oraParam[8] = new OracleParameter("xUpgradeEstimate", UpgradeEstimate);

                OracleDataAccess.ExecuteNonQuery(orcleCon, CommandType.StoredProcedure, "UPDATEPARENTMODEL", oraParam);
            } catch (Exception ex) {
                throw ex;
            }
        }

        public DataSet GetChildModels(string modelId) {
            try {
                OracleParameter[] oraParam = new OracleParameter[2];
                oraParam[0] = new OracleParameter("xModelID", modelId);
                oraParam[1] = new OracleParameter("xChildModelInfo", OracleDbType.RefCursor);

                oraParam[1].Direction = ParameterDirection.Output;

                DataSet ObjDS = OracleDataAccess.ExecuteDataset(connString,
                             CommandType.StoredProcedure,
                             "MODELINFO.GetChildModelData", oraParam);
                return ObjDS;
            } catch (Exception ex) {
                throw ex;
            }
        }

        public DataSet GetChildModelInfo(string modelId) {
            try {
                OracleParameter[] oraParam = new OracleParameter[2];
                oraParam[0] = new OracleParameter("xModelID", modelId);
                oraParam[1] = new OracleParameter("xChildModelInfo", OracleDbType.RefCursor);
                oraParam[1].Direction = ParameterDirection.Output;

                DataSet ObjDS = OracleDataAccess.ExecuteDataset(connString,
                             CommandType.StoredProcedure,
                             "MODELINFO.GetChildModelInfo", oraParam);
                return ObjDS;
            } catch (Exception ex) {
                throw ex;
            }
        }

        public DataSet GetChildModelsByModelID(string modelId) {
            try {
                OracleParameter[] oraParam = new OracleParameter[2];
                oraParam[0] = new OracleParameter("xModelID", modelId);
                oraParam[1] = new OracleParameter("xChildModelInfoByModelId", OracleDbType.RefCursor);
                oraParam[1].Direction = ParameterDirection.Output;

                DataSet ObjDS = OracleDataAccess.ExecuteDataset(connString,
                             CommandType.StoredProcedure,
                             "MODELINFO.GetChildModelDataByModelId", oraParam);
                return ObjDS;
            } catch (Exception ex) {
                throw ex;
            }
        }

        public DataSet GetReleaseInfoForModel(string modelId) {
            try {
                OracleParameter[] oraParam = new OracleParameter[2];
                oraParam[0] = new OracleParameter("xModelID", modelId);
                oraParam[1] = new OracleParameter("xRealeaseModelInfo", OracleDbType.RefCursor);
                oraParam[1].Direction = ParameterDirection.Output;

                DataSet ObjDS = OracleDataAccess.ExecuteDataset(connString,
                             CommandType.StoredProcedure,
                             "MODELINFO.GetReleaseInfoForModel", oraParam);
                return ObjDS;
            } catch (Exception ex) {
                throw ex;
            }
        }

        public DataSet GetModelIds() {
            try {
                OracleParameter[] oraParam = new OracleParameter[1];
                oraParam[0] = new OracleParameter("xModelIds", OracleDbType.RefCursor);
                oraParam[0].Direction = ParameterDirection.Output;

                DataSet ObjDS = OracleDataAccess.ExecuteDataset(connString,
                             CommandType.StoredProcedure,
                             "MODELINFO.GetModelIds", oraParam);
                return ObjDS;
            } catch (Exception ex) {
                throw ex;
            }
        }

        public DataSet GetModelInfo(string modelId) {
            try {
                OracleParameter[] oraParam = new OracleParameter[2];
                oraParam[0] = new OracleParameter("xModelId", modelId);
                oraParam[1] = new OracleParameter("xModelInfo", OracleDbType.RefCursor);
                oraParam[1].Direction = ParameterDirection.Output;
                DataSet ObjDS = OracleDataAccess.ExecuteDataset(connString,
                             CommandType.StoredProcedure,
                             "MODELINFO.GetModelInfo", oraParam);
                return ObjDS;
            } catch (Exception ex) {
                throw ex;
            }
        }

        public DataSet GetKitInfoByModelNo(string modelId) {
            try {
                OracleParameter[] oraParam = new OracleParameter[2];
                oraParam[0] = new OracleParameter("XMODEL_NUMBER", modelId);
                oraParam[1] = new OracleParameter("xKitInfo", OracleDbType.RefCursor);
                oraParam[1].Direction = ParameterDirection.Output;
                DataSet ObjDS = OracleDataAccess.ExecuteDataset(connString,
                             CommandType.StoredProcedure,
                             "MODELINFO.GetKitInfoByModelNo", oraParam);
                return ObjDS;
            } catch (Exception ex) {
                throw ex;
            }
        }

        public DataSet GetReleaseKitInfoByModelNo(string modelId) {
            try {
                OracleParameter[] oraParam = new OracleParameter[2];
                oraParam[0] = new OracleParameter("XMODEL_ID", modelId);
                oraParam[1] = new OracleParameter("xReleaseKitInfo", OracleDbType.RefCursor);
                oraParam[1].Direction = ParameterDirection.Output;
                DataSet ObjDS = OracleDataAccess.ExecuteDataset(connString,
                             CommandType.StoredProcedure,
                             "MODELINFO.GetReleaseKitInfoByModelNo", oraParam);
                return ObjDS;
            } catch (Exception ex) {
                throw ex;
            }
        }

        public DataSet GetRelKitInfoByKitPartNo(string kitpartnumber) {
            try {
                OracleParameter[] oraParam = new OracleParameter[2];
                oraParam[0] = new OracleParameter("XKIT_PART_NUMBER", kitpartnumber);
                oraParam[1] = new OracleParameter("xRelKitInfo", OracleDbType.RefCursor);
                oraParam[1].Direction = ParameterDirection.Output;
                DataSet ObjDS = OracleDataAccess.ExecuteDataset(connString,
                             CommandType.StoredProcedure,
                             "MODELINFO.GetRelKitInfoByModelNo", oraParam);
                return ObjDS;
            } catch (Exception ex) {
                throw ex;
            }
        }

        public void UpdateNewModel(string ModelID, int ModelStatus, string ModelType, string ModelDescription, int MainAssembly, int PriorVersionRequied, int CustomerTracking, string ProductionSource, int UpgradeEstimate, string Comments, string Notes, string CreatedBy, DateTime DateCreated) {
            try {
                OracleConnection orcleCon = new OracleConnection(connString);
                OracleParameter[] oraParam = new OracleParameter[13];

                oraParam[0] = new OracleParameter("xModelID", ModelID);
                oraParam[1] = new OracleParameter("xModelStatus", ModelStatus);
                oraParam[2] = new OracleParameter("xModelType", ModelType);
                oraParam[3] = new OracleParameter("xModelDescription", ModelDescription);
                oraParam[4] = new OracleParameter("xMainAssembly", MainAssembly);
                oraParam[5] = new OracleParameter("xPriorVersionRequied", PriorVersionRequied);
                oraParam[6] = new OracleParameter("xCustomerTracking", CustomerTracking);
                oraParam[7] = new OracleParameter("xProductionSource", ProductionSource);
                oraParam[8] = new OracleParameter("xUpgradeEstimate", UpgradeEstimate);
                oraParam[9] = new OracleParameter("xComments", Comments);
                oraParam[10] = new OracleParameter("xNotes", Notes);
                oraParam[11] = new OracleParameter("xCreatedBy", CreatedBy);
                oraParam[12] = new OracleParameter("xDateCreated", DateCreated);
                OracleDataAccess.ExecuteNonQuery(orcleCon, CommandType.StoredProcedure, "UPDATENEWMODEL", oraParam);
            } catch (Exception ex) {
                throw ex;
            }
        }
        #endregion
    }
}