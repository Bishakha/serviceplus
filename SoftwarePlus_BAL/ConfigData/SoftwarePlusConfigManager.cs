﻿/*
FileName: SoftwarePlusConfigManager.cs
 Date Created : 11/10/2011
 Description: All Config related information will be available
 */

using System.Collections;
using System.Linq;
using System.Xml.Linq;
using System.Data;
using Microsoft.Win32;
using Sony.US.SIAMUtilities;
namespace SoftwarePlus.BusinessAccess {

    public class SoftwarePlusConfigManager {

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        public SoftwarePlusConfigManager() { }
        #endregion
        #region Public Methods
        /// <summary>
        /// This method will provide the model types.
        /// </summary>
        /// <returns></returns>
        public ArrayList ModelType() {
            ArrayList modelTypeList = new ArrayList();
            XDocument xmlDoc = XDocument.Load(ConfiguraionFilePath());
            var q = from c in xmlDoc.Descendants("Type")
                    select (string)c.Element("name");

            foreach (string name in q) {
                modelTypeList.Add(name);
            }
            return modelTypeList;
        }
        public ArrayList ModelSource() {
            ArrayList modelTypeList = new ArrayList();
            XDocument xmlDoc = XDocument.Load(ConfiguraionFilePath());
            var q = from c in xmlDoc.Descendants("Source")
                    select (string)c.Element("name");

            foreach (string name in q) {
                modelTypeList.Add(name);
            }
            return modelTypeList;
        }
        public ArrayList ModelStatus() {
            ArrayList modelStatus = new ArrayList();
            XDocument xmlDoc = XDocument.Load(ConfiguraionFilePath());
            var q = from c in xmlDoc.Descendants("Status")
                    select (string)c.Element("name");

            foreach (string name in q) {
                modelStatus.Add(name);
            }
            return modelStatus;
        }
        public ArrayList ReleaseStatus() {
            ArrayList ReleaseStatus = new ArrayList();
            XDocument xmlDoc = XDocument.Load(ConfiguraionFilePath());
            var q = from c in xmlDoc.Descendants("ReleaseStatus")
                    select (string)c.Element("name");

            foreach (string name in q) {
                ReleaseStatus.Add(name);
            }
            return ReleaseStatus;
        }
        public ArrayList ReleaseFOC() {
            ArrayList ReleaseFOC = new ArrayList();
            XDocument xmlDoc = XDocument.Load(ConfiguraionFilePath());
            var q = from c in xmlDoc.Descendants("ReleaseFOC")
                    select (string)c.Element("name");

            foreach (string name in q) {
                ReleaseFOC.Add(name);
            }
            return ReleaseFOC;
        }

        public string SoftwarePlusDBConnectionString() {
            string regSoftwarePlus = @"SOFTWARE\ServicesPLUS";
            string configFileKey = @"ConnectionString";
            string m_filename = string.Empty;

            using (var hklm = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry64)) {
                using (var subKey = hklm.OpenSubKey(regSoftwarePlus)) {
                    if (subKey == null) {
                        m_filename = @"C:\Program Files (x86)\ServicesPLUS\Sony BSSC Core Services\Configuration\ServicesPLUSConfig.xml";
                    } else {
                        m_filename = subKey.GetValue(configFileKey) as string;
                    }
                }
            }

            return SoftwareplusEncryption.DeCrypt(m_filename, PWDKey());
        }

        public string SoftwarePlusPassword(string myPassword) {

            return SoftwareplusEncryption.DeCrypt(myPassword, PWDKey());
        }
        public string PWDKey() {

            //string regSoftwarePlus = @"SOFTWARE\SoftwarePlus";
            //string pwdKey = @"PWDKey";
            //RegistryKey rk = Registry.LocalMachine.OpenSubKey(regSoftwarePlus);
            //string key = rk.GetValue(pwdKey) as string;
            //return key;

            //ArrayList modelTypeList = new ArrayList();
            XDocument xmlDoc = XDocument.Load(ConfiguraionFilePath());
            var q = from c in xmlDoc.Descendants("PWDKey")
                    select (string)c.Element("name");
            string pathname = string.Empty;
            foreach (string name in q) {
                pathname = name;
            }
            return pathname;
        }
        public string SoftwarePlusErrorLogFile() {

            return ConfigurationData.Environment.SoftwarePlusConfiguration.ErrorLogFile;
        }
        public string SoftwarePlusEnvironment() {
            string regSoftwarePlus = @"SOFTWARE\ServicesPLUS";
            string environment = @"CurrentEnvironment";
            RegistryKey rk = Registry.LocalMachine.OpenSubKey(regSoftwarePlus);
            string keyEnvironment = rk.GetValue(environment) as string;

            using (var hklm = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry64)) {
                using (var subKey = hklm.OpenSubKey(regSoftwarePlus)) {
                    if (subKey == null) {
                        keyEnvironment = @"Development";
                    } else {
                        keyEnvironment = subKey.GetValue(environment) as string;
                    }
                }
            }

            return keyEnvironment;
        }
        public ArrayList SoftwarePlusUserRoles() {
            ArrayList role = new ArrayList();
            XDocument xmlDoc = XDocument.Load(ConfiguraionFilePath());
            var q = from c in xmlDoc.Descendants("UserRole")
                    select (string)c.Element("name");
            string pathname = string.Empty;
            foreach (string name in q) {
                role.Add(name);
            }
            return role;
        }
        public ArrayList SystemLookupTables() {
            ArrayList lookup = new ArrayList();
            XDocument xmlDoc = XDocument.Load(ConfiguraionFilePath());
            var q = from c in xmlDoc.Descendants("LookupTables")
                    select (string)c.Element("name");
            string pathname = string.Empty;
            foreach (string name in q) {
                lookup.Add(name);
            }
            return lookup;
        }
        public ArrayList ReleaseType() {
            ArrayList releaseType = new ArrayList();
            XDocument xmlDoc = XDocument.Load(ConfiguraionFilePath());
            var q = from c in xmlDoc.Descendants("ReleaseType")
                    select (string)c.Element("name");
            string pathname = string.Empty;
            foreach (string name in q) {
                releaseType.Add(name);
            }
            return releaseType;
        }
        public ArrayList ReleaseDistribution() {
            ArrayList ReleaseDistribution = new ArrayList();
            XDocument xmlDoc = XDocument.Load(ConfiguraionFilePath());
            var q = from c in xmlDoc.Descendants("Distribution")
                    select (string)c.Element("name");
            string pathname = string.Empty;
            foreach (string name in q) {
                ReleaseDistribution.Add(name);
            }
            return ReleaseDistribution;
        }
        public ArrayList ReleaseVisibility() {
            ArrayList ReleaseVisibility = new ArrayList();
            XDocument xmlDoc = XDocument.Load(ConfiguraionFilePath());
            var q = from c in xmlDoc.Descendants("Visibility")
                    select (string)c.Element("name");
            string pathname = string.Empty;
            foreach (string name in q) {
                ReleaseVisibility.Add(name);
            }
            return ReleaseVisibility;
        }

        public string SoftwarePLUSDevelopmentEnvironment() {
            // ArrayList modelStatus = new ArrayList();
            string environment = string.Empty;
            XDocument xmlDoc = XDocument.Load(ConfiguraionFilePath());
            var q = from c in xmlDoc.Descendants("SoftwarePLUSDevelopment")
                    select (string)c.Element("name");

            foreach (string name in q) {
                // modelStatus.Add(name);
                environment = name;
            }
            return environment;
        }
        public string SoftwarePLUSProductionEnvironment() {
            // ArrayList modelStatus = new ArrayList();
            string environment = string.Empty;
            XDocument xmlDoc = XDocument.Load(ConfiguraionFilePath());
            var q = from c in xmlDoc.Descendants("SoftwarePLUSProduction")
                    select (string)c.Element("name");

            foreach (string name in q) {
                // modelStatus.Add(name);
                environment = name;
            }
            return environment;
        }

        public string SoftwarePLUSDownloadFilepath() {
            // ArrayList modelStatus = new ArrayList();
            string environment = string.Empty;
            XDocument xmlDoc = XDocument.Load(ConfiguraionFilePath());
            var q = from c in xmlDoc.Descendants("SoftwarePLUSDownloadFilepath")
                    select (string)c.Element("name");

            foreach (string name in q) {
                // modelStatus.Add(name);
                environment = name;
            }
            return environment;
        }

        public string SoftwarePLUSUserManualsPath() {
            // ArrayList modelStatus = new ArrayList();
            string environment = string.Empty;
            XDocument xmlDoc = XDocument.Load(ConfiguraionFilePath());
            var q = from c in xmlDoc.Descendants("SoftwarePLUSUserManualsPath")
                    select (string)c.Element("name");

            foreach (string name in q) {
                // modelStatus.Add(name);
                environment = name;
            }
            return environment;
        }
        public string SoftwarePLUSReleaseNotesPath() {
            // ArrayList modelStatus = new ArrayList();
            string environment = string.Empty;
            XDocument xmlDoc = XDocument.Load(ConfiguraionFilePath());
            var q = from c in xmlDoc.Descendants("SoftwarePLUSReleaseNotesPath")
                    select (string)c.Element("name");

            foreach (string name in q) {
                // modelStatus.Add(name);
                environment = name;
            }
            return environment;
        }
        #endregion


        #region Private Members
        private string ConfiguraionFilePath() {
            string regSoftwarePlus = @"SOFTWARE\ServicesPLUS";
            string configFileKey = @"ConfigFileLocation";
            //RegistryKey rk = Registry.LocalMachine.OpenSubKey(regSoftwarePlus);
            string m_filename = string.Empty;

            using (var hklm = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry64)) {
                using (var subKey = hklm.OpenSubKey(regSoftwarePlus)) {
                    if (subKey == null) {
                        m_filename = @"C:\Program Files (x86)\ServicesPLUS\Sony BSSC Core Services\Configuration\ServicesPLUSConfig.xml";
                    } else {
                        m_filename = subKey.GetValue(configFileKey) as string;
                    }
                }
            }
            return m_filename;
        }
        #endregion
        #region Fields

        #endregion

    }

}


