﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using System.Data;
using Oracle.ManagedDataAccess.Client;
using SoftwarePlus.DataAccess;

namespace SoftwarePlus.BusinessAccess
{
   public class Configuration
    {
       #region Constructor
       public Configuration()
        { 
        
        }
        #endregion
       SoftwarePlusConfigManager softwareConfigMgr = new SoftwarePlusConfigManager();
       public DataSet GetSystemConfigData()
       {
           try
           {
               string connection = softwareConfigMgr.SoftwarePlusDBConnectionString();
               OracleParameter[] oraParam = new OracleParameter[1];

               oraParam[0] = new OracleParameter("xSystemConfigData", OracleDbType.RefCursor);
               oraParam[0].Direction = ParameterDirection.Output;

               DataSet ObjDS = OracleDataAccess.ExecuteDataset(connection,
                            CommandType.StoredProcedure,
                            "SYSTEM.GetSystemConfigData", oraParam);

               return ObjDS;
           }
           catch (Exception ex)
           {
               throw ex;
           }

       }

       public DataSet GetStatesInfo()
       {
           softwareConfigMgr = new SoftwarePlusConfigManager();
           string con = softwareConfigMgr.SoftwarePlusDBConnectionString();
           OracleParameter[] oraParam = new OracleParameter[1];



           oraParam[0] = new OracleParameter("xStatesInfo", OracleDbType.RefCursor);

           oraParam[0].Direction = ParameterDirection.Output;

           DataSet ObjDS = OracleDataAccess.ExecuteDataset(con,
                        CommandType.StoredProcedure,
                        "SYSTEM.GetStatesInfo", oraParam);

           return ObjDS;
       }
       public DataSet GetCountryInfo()
       {
           softwareConfigMgr = new SoftwarePlusConfigManager();
           string con = softwareConfigMgr.SoftwarePlusDBConnectionString();
           OracleParameter[] oraParam = new OracleParameter[1];



           oraParam[0] = new OracleParameter("xCountryInfo", OracleDbType.RefCursor);

           oraParam[0].Direction = ParameterDirection.Output;

           DataSet ObjDS = OracleDataAccess.ExecuteDataset(con,
                        CommandType.StoredProcedure,
                        "SYSTEM.GetCountryInfo", oraParam);

           return ObjDS;
       }
       public void UpdSysConfig(int SECURITY_CHECK,string ADDRESS,string ADDRESS_2,string CITY,string STATE,string ZIP,string COUNTRY,string OFFICE_PHONE,string FAX_NUMBER,string INTERNET_ADDRESS,string REPORT_FAX,string REPORT_PHONE,string REPORT_OPTION,string FTP_SERVER,string FTP_USER_ID,string FTP_PASSWORD,string FTP_LOCATION,string FTP_LOCATION_2,int DELETE_LOCAL,int DELETE_LOCAL_2,string FTP_LOCATION_3,int DELETE_LOCAL_3)
       {
           try
           {
               string connection = softwareConfigMgr.SoftwarePlusDBConnectionString();

               OracleConnection orcleCon = new OracleConnection(connection);
               OracleParameter[] oraParam = new OracleParameter[22];

               oraParam[0] = new OracleParameter("XSECURITY_CHECK", SECURITY_CHECK);
               oraParam[1] = new OracleParameter("XADDRESS", ADDRESS);
               oraParam[2] = new OracleParameter("XADDRESS_2", ADDRESS_2);
               oraParam[3] = new OracleParameter("XCITY", CITY);
               oraParam[4] = new OracleParameter("XSTATE", STATE);
               oraParam[5] = new OracleParameter("XZIP", ZIP);
               oraParam[6] = new OracleParameter("XCOUNTRY", COUNTRY);
               oraParam[7] = new OracleParameter("XOFFICE_PHONE", OFFICE_PHONE);
               oraParam[8] = new OracleParameter("XFAX_NUMBER", FAX_NUMBER);
               oraParam[9] = new OracleParameter("XINTERNET_ADDRESS", INTERNET_ADDRESS);
               oraParam[10] = new OracleParameter("XREPORT_FAX", REPORT_FAX);
               oraParam[11] = new OracleParameter("XREPORT_PHONE", REPORT_PHONE);
               oraParam[12] = new OracleParameter("XREPORT_OPTION", REPORT_OPTION);
               oraParam[13] = new OracleParameter("XFTP_SERVER", FTP_SERVER);
               oraParam[14] = new OracleParameter("XFTP_USER_ID", FTP_USER_ID);
               oraParam[15] = new OracleParameter("XFTP_PASSWORD", FTP_PASSWORD);
               oraParam[16] = new OracleParameter("XFTP_LOCATION", FTP_LOCATION);
               oraParam[17] = new OracleParameter("XFTP_LOCATION_2", FTP_LOCATION_2);
               oraParam[18] = new OracleParameter("XDELETE_LOCAL", DELETE_LOCAL);
               oraParam[19] = new OracleParameter("XDELETE_LOCAL_2", DELETE_LOCAL_2);
               oraParam[20] = new OracleParameter("XFTP_LOCATION_3", FTP_LOCATION_3);
               oraParam[21] = new OracleParameter("XDELETE_LOCAL_3", DELETE_LOCAL_3);
               


               OracleDataAccess.ExecuteNonQuery(orcleCon, CommandType.StoredProcedure, "UpdSysConfig", oraParam);
           }
           catch (Exception ex)
           {
               throw ex;
           }

       }
    }
}
