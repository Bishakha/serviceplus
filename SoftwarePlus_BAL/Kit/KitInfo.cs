﻿using System;
using System.Data;
using Oracle.ManagedDataAccess.Client;
using SoftwarePlus.DataAccess;

namespace SoftwarePlus.BusinessAccess {
    public class KitInfo
    {
        SoftwarePlusConfigManager softwareConfigMgr = new SoftwarePlusConfigManager();
        #region Constructor
        public KitInfo()
        {
        }
        #endregion
        #region Public Methods
        public DataSet GetKitPartNumbers()
        {
            softwareConfigMgr = new SoftwarePlusConfigManager();
            string con = softwareConfigMgr.SoftwarePlusDBConnectionString();
            OracleParameter[] oraParam = new OracleParameter[1];
            oraParam[0] = new OracleParameter("xKit_Part_Number", OracleDbType.RefCursor);
            oraParam[0].Direction = ParameterDirection.Output;

            DataSet ObjDS = OracleDataAccess.ExecuteDataset(con,
                         CommandType.StoredProcedure,
                         "KITINFO.GetKitPartNumbers", oraParam);

            return ObjDS;

        }
        public void AddKitInfo(string KIT_PART_NUMBER, string KIT_DESCRIPTION, string SUPERCEDED_KIT, string MANUFACTURER_REMARKS, string CREATED_BY, DateTime DATE_CREATED, string DOWNLOAD_FILE, string RELEASE_NOTES_FILE,string requiredFile,int downloadFreeOfCharge,int tracking,string notes)
        {
            try
            {
                string connection = softwareConfigMgr.SoftwarePlusDBConnectionString();

                OracleConnection orcleCon = new OracleConnection(connection);
                OracleParameter[] oraParam = new OracleParameter[12];
                oraParam[0] = new OracleParameter("XKIT_PART_NUMBER", KIT_PART_NUMBER);
                oraParam[1] = new OracleParameter("XKIT_DESCRIPTION", KIT_DESCRIPTION);
                oraParam[2] = new OracleParameter("XSUPERCEDED_KIT", SUPERCEDED_KIT);
                oraParam[3] = new OracleParameter("XMANUFACTURER_REMARKS", MANUFACTURER_REMARKS);
               // oraParam[4] = new OracleParameter("XNOTES", NOTES);
                oraParam[4] = new OracleParameter("XCREATED_BY", CREATED_BY);
                oraParam[5] = new OracleParameter("XDATE_CREATED", DATE_CREATED);
              //  oraParam[7] = new OracleParameter("XUPDATED_BY", UPDATED_BY);
             //   oraParam[8] = new OracleParameter("XLAST_UPDATED", LAST_UPDATED);
                oraParam[6] = new OracleParameter("XDOWNLOAD_FILE", DOWNLOAD_FILE);
                oraParam[7] = new OracleParameter("XRELEASE_NOTES_FILE", RELEASE_NOTES_FILE);
                oraParam[8] = new OracleParameter("XREQUIREDKEY", requiredFile);
                oraParam[9] = new OracleParameter("XFREE_OF_CHARGE", downloadFreeOfCharge);
                oraParam[10] = new OracleParameter("XTRACKING", tracking);
                oraParam[11] = new OracleParameter("XNOTES", notes);
                //oraParam[11] = new OracleParameter("XUSER_MANUAL_FILE", USER_MANUAL_FILE);
               
                OracleDataAccess.ExecuteNonQuery(orcleCon, CommandType.StoredProcedure, "ADDKITINFO", oraParam);
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }
        public void DeleteKitInfo(string KIT_PART_NUMBER)
        {
            try
            {
                string connection = softwareConfigMgr.SoftwarePlusDBConnectionString();

                OracleConnection orcleCon = new OracleConnection(connection);
                OracleParameter[] oraParam = new OracleParameter[1];
                oraParam[0] = new OracleParameter("xKIT_PART_NUMBER", KIT_PART_NUMBER);


                OracleDataAccess.ExecuteNonQuery(orcleCon, CommandType.StoredProcedure, "DELETEKITINFO", oraParam);
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }
        public void UpdateKitInfo(string KIT_PART_NUMBER, string KIT_DESCRIPTION, string SUPERCEDED_KIT, string MANUFACTURER_REMARKS, string updateBy, DateTime dateUpdated, string DOWNLOAD_FILE, string RELEASE_NOTES_FILE, string requiredFile, int downloadFreeOfCharge, int tracking,string notes)
        {
            try
            {
                string connection = softwareConfigMgr.SoftwarePlusDBConnectionString();

                OracleConnection orcleCon = new OracleConnection(connection);
                OracleParameter[] oraParam = new OracleParameter[12];
                oraParam[0] = new OracleParameter("XKIT_PART_NUMBER", KIT_PART_NUMBER);
                oraParam[1] = new OracleParameter("XKIT_DESCRIPTION", KIT_DESCRIPTION);
                oraParam[2] = new OracleParameter("XSUPERCEDED_KIT", SUPERCEDED_KIT);
                oraParam[3] = new OracleParameter("XMANUFACTURER_REMARKS", MANUFACTURER_REMARKS);
                // oraParam[4] = new OracleParameter("XNOTES", NOTES);
                oraParam[4] = new OracleParameter("XUPDATED_BY", updateBy);
                oraParam[5] = new OracleParameter("XLAST_UPDATED", dateUpdated);
                //  oraParam[7] = new OracleParameter("XUPDATED_BY", UPDATED_BY);
                //   oraParam[8] = new OracleParameter("XLAST_UPDATED", LAST_UPDATED);
                oraParam[6] = new OracleParameter("XDOWNLOAD_FILE", DOWNLOAD_FILE);
                oraParam[7] = new OracleParameter("XRELEASE_NOTES_FILE", RELEASE_NOTES_FILE);
                oraParam[8] = new OracleParameter("XREQUIREDKEY", requiredFile);
                oraParam[9] = new OracleParameter("XFREE_OF_CHARGE", downloadFreeOfCharge);
                oraParam[10] = new OracleParameter("XTRACKING", tracking);
                //oraParam[11] = new OracleParameter("XUSER_MANUAL_FILE", USER_MANUAL_FILE);
                oraParam[11] = new OracleParameter("XNOTES", notes);
                OracleDataAccess.ExecuteNonQuery(orcleCon, CommandType.StoredProcedure, "UPDATEKITINFO", oraParam);
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }
        public DataSet GetKitInfoByKitPartNo(string KIT_PART_NUMBER)
        {
            try
            {
                string connection = softwareConfigMgr.SoftwarePlusDBConnectionString();
                OracleParameter[] oraParam = new OracleParameter[2];

                oraParam[0] = new OracleParameter("XKIT_PART_NUMBER", KIT_PART_NUMBER);
               
                oraParam[1] = new OracleParameter("xKitInfo", OracleDbType.RefCursor);

                oraParam[1].Direction = ParameterDirection.Output;

                DataSet ObjDS = OracleDataAccess.ExecuteDataset(connection,
                             CommandType.StoredProcedure,
                             "KITINFO.GetKitInfoByKitPartNo", oraParam);

                return ObjDS;
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }
        public DataSet GetKitInfoForAttachment()
        {
            try
            {
                string connection = softwareConfigMgr.SoftwarePlusDBConnectionString();
                OracleParameter[] oraParam = new OracleParameter[1];

                //oraParam[0] = new OracleParameter("XKIT_PART_NUMBER", KIT_PART_NUMBER);

                oraParam[0] = new OracleParameter("xKitInfo", OracleDbType.RefCursor);

                oraParam[0].Direction = ParameterDirection.Output;

                DataSet ObjDS = OracleDataAccess.ExecuteDataset(connection,
                             CommandType.StoredProcedure,
                             "KITINFO.GetKitInfoForAttachment", oraParam);

                return ObjDS;
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }
        public DataSet GetFTPDownloadFile()
        {
            try
            {
                string connection = softwareConfigMgr.SoftwarePlusDBConnectionString();
                OracleParameter[] oraParam = new OracleParameter[1];

             

                oraParam[0] = new OracleParameter("xFTPDownLoadFile", OracleDbType.RefCursor);

                oraParam[0].Direction = ParameterDirection.Output;

                DataSet ObjDS = OracleDataAccess.ExecuteDataset(connection,
                             CommandType.StoredProcedure,
                             "KITINFO.GetFTPDownloadFile", oraParam);

                return ObjDS;
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }
        public DataSet GetFTPReleaseFile()
        {
            try
            {
                string connection = softwareConfigMgr.SoftwarePlusDBConnectionString();
                OracleParameter[] oraParam = new OracleParameter[1];



                oraParam[0] = new OracleParameter("xFTPReleaseLoadFile", OracleDbType.RefCursor);

                oraParam[0].Direction = ParameterDirection.Output;

                DataSet ObjDS = OracleDataAccess.ExecuteDataset(connection,
                             CommandType.StoredProcedure,
                             "KITINFO.GetFTPReleaseFile", oraParam);

                return ObjDS;
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }
        #endregion

    } 
}
