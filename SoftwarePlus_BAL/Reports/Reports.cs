﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using System.Data;
using Oracle.ManagedDataAccess.Client;
using SoftwarePlus.DataAccess;


namespace SoftwarePlus.BusinessAccess
{
    public class Reports
    {
        SoftwarePlusConfigManager configManager;

        public DataSet GetEngineerName()
        {
            configManager = new SoftwarePlusConfigManager();
            string con = configManager.SoftwarePlusDBConnectionString();
            OracleParameter[] oraParam = new OracleParameter[1];

            oraParam[0] = new OracleParameter("xEngineerName", OracleDbType.RefCursor);

            oraParam[0].Direction = ParameterDirection.Output;

            DataSet ObjDS = OracleDataAccess.ExecuteDataset(con,
                         CommandType.StoredProcedure,
                         "REPORTS.GetEngineerName", oraParam);

            return ObjDS;

        }

        public DataSet GetKitPartNoDesc()
        {
            configManager = new SoftwarePlusConfigManager();
            string con = configManager.SoftwarePlusDBConnectionString();
            OracleParameter[] oraParam = new OracleParameter[1];

            oraParam[0] = new OracleParameter("xKitDescription", OracleDbType.RefCursor);

            oraParam[0].Direction = ParameterDirection.Output;

            DataSet ObjDS = OracleDataAccess.ExecuteDataset(con,
                         CommandType.StoredProcedure,
                         "REPORTS.GetKitPartNoDescription", oraParam);

            return ObjDS;

        }

        public DataSet GetProductManager()
        {
            configManager = new SoftwarePlusConfigManager();
            string con = configManager.SoftwarePlusDBConnectionString();
            OracleParameter[] oraParam = new OracleParameter[1];

            oraParam[0] = new OracleParameter("xProductManager", OracleDbType.RefCursor);

            oraParam[0].Direction = ParameterDirection.Output;

            DataSet ObjDS = OracleDataAccess.ExecuteDataset(con,
                         CommandType.StoredProcedure,
                         "REPORTS.GetProductManager", oraParam);

            return ObjDS;

        }

        public DataSet RPTCurrentVersion(string ReleaseType, string DistributionType, string VisibilityType, DateTime StartDate, DateTime EndDate)
        {
            configManager = new SoftwarePlusConfigManager();
            string con = configManager.SoftwarePlusDBConnectionString();
            OracleParameter[] oraParam = new OracleParameter[6];
            oraParam[0] = new OracleParameter("xReleaseType", ReleaseType);
            oraParam[1] = new OracleParameter("xDistributionType", DistributionType);
            oraParam[2] = new OracleParameter("xVisibilityType", VisibilityType);
            oraParam[3] = new OracleParameter("xRELEASE_StartDATE", StartDate);
            oraParam[4] = new OracleParameter("xRELEASE_EndDATE", EndDate);
            oraParam[5] = new OracleParameter("xRPT_CurrentVersionData", OracleDbType.RefCursor);

            oraParam[5].Direction = ParameterDirection.Output;

            DataSet ObjDS = OracleDataAccess.ExecuteDataset(con,
                         CommandType.StoredProcedure,
                         "REPORTS.RPT_CurrentVersion", oraParam);

            return ObjDS;
        }

        public DataSet RPTModelsNotAssigned()
        {
            configManager = new SoftwarePlusConfigManager();
            string con = configManager.SoftwarePlusDBConnectionString();
            OracleParameter[] oraParam = new OracleParameter[1];

            oraParam[0] = new OracleParameter("xRPT_ModelsNotAssigned", OracleDbType.RefCursor);

            oraParam[0].Direction = ParameterDirection.Output;

            DataSet ObjDS = OracleDataAccess.ExecuteDataset(con,
                         CommandType.StoredProcedure,
                         "REPORTS.RPT_ModelsNotAssigned", oraParam);

            return ObjDS;

        }

        public DataSet RPTEngineerModels(string ReleaseType, string DistributionType, string VisibilityType, string engineername)
        {
            configManager = new SoftwarePlusConfigManager();
            string con = configManager.SoftwarePlusDBConnectionString();
            OracleParameter[] oraParam = new OracleParameter[5];
            oraParam[0] = new OracleParameter("xReleaseType", ReleaseType);
            oraParam[1] = new OracleParameter("xDistributionType", DistributionType);
            oraParam[2] = new OracleParameter("xVisibilityType", VisibilityType);
            oraParam[3] = new OracleParameter("xEngineerName", engineername);
            oraParam[4] = new OracleParameter("xRPT_EngineerModels", OracleDbType.RefCursor);

            oraParam[4].Direction = ParameterDirection.Output;

            DataSet ObjDS = OracleDataAccess.ExecuteDataset(con,
                         CommandType.StoredProcedure,
                         "REPORTS.RPT_EngineerModels", oraParam);

            return ObjDS;
        }

        public DataSet RPTEPROMKits(string ReleaseType, string DistributionType, string VisibilityType)
        {
            configManager = new SoftwarePlusConfigManager();
            string con = configManager.SoftwarePlusDBConnectionString();
            OracleParameter[] oraParam = new OracleParameter[4];

            oraParam[0] = new OracleParameter("xReleaseType", ReleaseType);
            oraParam[1] = new OracleParameter("xDistributionType", DistributionType);
            oraParam[2] = new OracleParameter("xVisibilityType", VisibilityType);
            oraParam[3] = new OracleParameter("xRPT_EPROMKits", OracleDbType.RefCursor);

            oraParam[3].Direction = ParameterDirection.Output;

            DataSet ObjDS = OracleDataAccess.ExecuteDataset(con,
                         CommandType.StoredProcedure,
                         "REPORTS.RPT_EPROMKits", oraParam);

            return ObjDS;

        }

        public DataSet RPTKitAvailability(string ReleaseType, string DistributionType, string VisibilityType, string symbol, string version)
        {
            configManager = new SoftwarePlusConfigManager();
            string con = configManager.SoftwarePlusDBConnectionString();
            OracleParameter[] oraParam = new OracleParameter[6];
            oraParam[0] = new OracleParameter("xReleaseType", ReleaseType);
            oraParam[1] = new OracleParameter("xDistributionType", DistributionType);
            oraParam[2] = new OracleParameter("xVisibilityType", VisibilityType);
            oraParam[3] = new OracleParameter("xSymbol", symbol);
            oraParam[4] = new OracleParameter("xVersion", version);
            oraParam[5] = new OracleParameter("xRPT_KitAvailability", OracleDbType.RefCursor);

            oraParam[5].Direction = ParameterDirection.Output;

            DataSet ObjDS = OracleDataAccess.ExecuteDataset(con,
                         CommandType.StoredProcedure,
                         "REPORTS.RPT_KitAvailability", oraParam);

            return ObjDS;
        }

        public DataSet RPTProductManagerModels(string ReleaseType, string DistributionType, string VisibilityType, string productmangaer)
        {
            configManager = new SoftwarePlusConfigManager();
            string con = configManager.SoftwarePlusDBConnectionString();
            OracleParameter[] oraParam = new OracleParameter[5];
            oraParam[0] = new OracleParameter("xReleaseType", ReleaseType);
            oraParam[1] = new OracleParameter("xDistributionType", DistributionType);
            oraParam[2] = new OracleParameter("xVisibilityType", VisibilityType);
            oraParam[3] = new OracleParameter("xProductManager", productmangaer);
            oraParam[4] = new OracleParameter("xRPT_ProductManagerModels", OracleDbType.RefCursor);

            oraParam[4].Direction = ParameterDirection.Output;

            DataSet ObjDS = OracleDataAccess.ExecuteDataset(con,
                         CommandType.StoredProcedure,
                         "REPORTS.RPT_ProductManagerModels", oraParam);

            return ObjDS;
        }

        public DataSet RPTSoftwareRegistration(string KitPartNumber)
        {
            configManager = new SoftwarePlusConfigManager();
            string con = configManager.SoftwarePlusDBConnectionString();
            OracleParameter[] oraParam = new OracleParameter[2];

            oraParam[0] = new OracleParameter("xKitPartNumbr", KitPartNumber);
            oraParam[1] = new OracleParameter("xRPT_SoftwareRegistration", OracleDbType.RefCursor);

            oraParam[1].Direction = ParameterDirection.Output;

            DataSet ObjDS = OracleDataAccess.ExecuteDataset(con,
                         CommandType.StoredProcedure,
                         "REPORTS.RPT_SoftwareRegistration", oraParam);

            return ObjDS;
        }

        public DataSet RPTSoftwareRegistrationReleases(string KitPartNumber)
        {
            configManager = new SoftwarePlusConfigManager();
            string con = configManager.SoftwarePlusDBConnectionString();
            OracleParameter[] oraParam = new OracleParameter[2];

            oraParam[0] = new OracleParameter("xKitPartNumbr", KitPartNumber);
            oraParam[1] = new OracleParameter("xRPT_SWRegistrationReleases", OracleDbType.RefCursor);

            oraParam[1].Direction = ParameterDirection.Output;

            DataSet ObjDS = OracleDataAccess.ExecuteDataset(con,
                         CommandType.StoredProcedure,
                         "REPORTS.RPT_SWRegistrationReleases", oraParam);

            return ObjDS;
        }

        public DataSet RPTSoftwareRegistrationItems(string KitPartNumber)
        {
            configManager = new SoftwarePlusConfigManager();
            string con = configManager.SoftwarePlusDBConnectionString();
            OracleParameter[] oraParam = new OracleParameter[2];

            oraParam[0] = new OracleParameter("xKitPartNumbr", KitPartNumber);
            oraParam[1] = new OracleParameter("xRPT_SWRegistrationItems", OracleDbType.RefCursor);

            oraParam[1].Direction = ParameterDirection.Output;

            DataSet ObjDS = OracleDataAccess.ExecuteDataset(con,
                         CommandType.StoredProcedure,
                         "REPORTS.RPT_SWRegistrationItems", oraParam);

            return ObjDS;
        }

        public DataSet RPTInternetModels(string ReleaseType, string DistributionType, string VisibilityType)
        {
            configManager = new SoftwarePlusConfigManager();
            string con = configManager.SoftwarePlusDBConnectionString();
            OracleParameter[] oraParam = new OracleParameter[4];
            oraParam[0] = new OracleParameter("xReleaseType", ReleaseType);
            oraParam[1] = new OracleParameter("xDistributionType", DistributionType);
            oraParam[2] = new OracleParameter("xVisibilityType", VisibilityType);
            oraParam[3] = new OracleParameter("xRPT_InternetModels", OracleDbType.RefCursor);

            oraParam[3].Direction = ParameterDirection.Output;

            DataSet ObjDS = OracleDataAccess.ExecuteDataset(con,
                         CommandType.StoredProcedure,
                         "REPORTS.RPT_InternetModels", oraParam);

            return ObjDS;
        }

        public DataSet RPTIntranetKitItems(string ReleaseType, string DistributionType, string VisibilityType)
        {
            configManager = new SoftwarePlusConfigManager();
            string con = configManager.SoftwarePlusDBConnectionString();
            OracleParameter[] oraParam = new OracleParameter[4];
            oraParam[0] = new OracleParameter("xReleaseType", ReleaseType);
            oraParam[1] = new OracleParameter("xDistributionType", DistributionType);
            oraParam[2] = new OracleParameter("xVisibilityType", VisibilityType);
            oraParam[3] = new OracleParameter("xRPT_IntranetKitItems", OracleDbType.RefCursor);

            oraParam[3].Direction = ParameterDirection.Output;

            DataSet ObjDS = OracleDataAccess.ExecuteDataset(con,
                         CommandType.StoredProcedure,
                         "REPORTS.RPT_IntranetKitItems", oraParam);

            return ObjDS;
        }

        public DataSet RPTSystemDeleteLog(DateTime StartData, DateTime EndDate)
        {
            configManager = new SoftwarePlusConfigManager();
            string con = configManager.SoftwarePlusDBConnectionString();
            OracleParameter[] oraParam = new OracleParameter[3];
            oraParam[0] = new OracleParameter("xStartDATE", StartData);
            oraParam[1] = new OracleParameter("xEndDATE", EndDate);
            oraParam[2] = new OracleParameter("xRPT_SystemDeleteLog", OracleDbType.RefCursor);

            oraParam[2].Direction = ParameterDirection.Output;

            DataSet ObjDS = OracleDataAccess.ExecuteDataset(con,
                         CommandType.StoredProcedure,
                         "REPORTS.RPT_SystemDeleteLog", oraParam);

            return ObjDS;
        }
        
        public DataSet RPTSystemErrorLog(DateTime StartData, DateTime EndDate)
        {
            configManager = new SoftwarePlusConfigManager();
            string con = configManager.SoftwarePlusDBConnectionString();
            OracleParameter[] oraParam = new OracleParameter[3];
            oraParam[0] = new OracleParameter("xStartDATE", StartData);
            oraParam[1] = new OracleParameter("xEndDATE", EndDate);
            oraParam[2] = new OracleParameter("xRPT_SystemErrorLog", OracleDbType.RefCursor);

            oraParam[2].Direction = ParameterDirection.Output;

            DataSet ObjDS = OracleDataAccess.ExecuteDataset(con,
                         CommandType.StoredProcedure,
                         "REPORTS.RPT_SystemErrorLog", oraParam);

            return ObjDS;
        }
    }
}
