﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using System.Data;
using Oracle.ManagedDataAccess.Client;
using SoftwarePlus.DataAccess;

namespace SoftwarePlus.BusinessAccess
{
    public class clsKitAvailability
    {
        public clsKitAvailability()
	    {
		    //
		    // TODO: Add constructor logic here
		    //
	    }
        public string Symbolchar
        {
            get;
            set;

        }
        public string KitVersion
        {
            get;
            set;

        }
        public int Beta
        {
            get;
            set;

        }
        public int Shipping
        {
            get;
            set;

        }
        public int Upgrade
        {
            get;
            set;

        }
        public int Special
        {
            get;
            set;

        }
        public DateTime ReleaseDate
        {
            get;
            set;

        }
        public string Engineer
        {
            get;
            set;

        }
        public int priorVersionRequired
        {
            get;
            set;

        }
        public string ModelID
        {
            get;
            set;

        }
        public string Version
        {
            get;
            set;

        }
        public string KitPartNumber
        {
            get;
            set;

        }
        public string KitDiscription
        {
            get;
            set;

        }
        public string SupercededKit
        {
            get;
            set;

        }
        public string ManufacturerRemarks
        {
            get;
            set;

        }
        public string ReleaseType
        {
            get;
            set;
        }
        public string DistributionType
        {
            get;
            set;
        }
        public string VisibilityType
        {
            get;
            set;
        }
    }
}
