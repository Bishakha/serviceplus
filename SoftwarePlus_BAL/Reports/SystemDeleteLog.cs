﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using System.Data;
using Oracle.ManagedDataAccess.Client;
using SoftwarePlus.DataAccess;


namespace SoftwarePlus.BusinessAccess
{
    public  class clsSystemDeleteLog
    {
        public clsSystemDeleteLog()
	    {
		    //
		    // TODO: Add constructor logic here
		    //
	    }
        public DateTime StartDate
        {
            get;
            set;
        }
        public DateTime Enddate
        {
            get;
            set;
        }
        public string  UserID
        {
            get;
            set;
        }
        public DateTime DeleteDate
        {
            get;
            set;
        }
        public string FormName
        {
            get;
            set;
        }
        public string FormValue
        {
            get;
            set;
        }
    }
}
