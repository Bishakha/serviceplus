﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using System.Data;
using Oracle.ManagedDataAccess.Client;
using SoftwarePlus.DataAccess;


namespace SoftwarePlus.BusinessAccess
{
    public  class clsIntranetKitItems
    {
        public clsIntranetKitItems()
	    {
		    //
		    // TODO: Add constructor logic here
		    //
	    }
        public string ReleaseType
        {
            get;
            set;
        }
        public string DistributionType
        {
            get;
            set;
        }
        public string VisibilityType
        {
            get;
            set;
        }
        public string ModelID
        {
            get;
            set;
        }
        public string ModelDescription
        {
            get;
            set;
        }
        public string Comments
        {
            get;
            set;
        }
        public string Version
        {
            get;
            set;
        }
        public DateTime ReleaseDate
        {
            get;
            set;
        }
        public string KitpartNumber
        {
            get;
            set;
        }
        public string KitDescription
        {
            get;
            set;
        }
        public string ItemDescription
        {
            get;
            set;
        }
        public string Subassembly
        {
            get;
            set;
        }
        public string Location
        {
            get;
            set;
        }
        public string KitVersion
        {
            get;
            set;
        }
    }
}
