﻿using System;
using System.Data;
using Oracle.ManagedDataAccess.Client;
using SoftwarePlus.DataAccess;


namespace SoftwarePlus.BusinessAccess {
    public class Release {
        SoftwarePlusConfigManager softwareConfigMgr = new SoftwarePlusConfigManager();
        #region Constructor
        public Release() {

        }
        #endregion
        #region Public Methods

        public void AddReleaseLog(string MODEL_ID, string VERSION, DateTime RELEASE_DATE, DateTime SHIP_DATE, string RELEASE_TYPE, string VISIBILITY_TYPE, string DISTRIBUTION_TYPE, int RELEASE_QUANTITY, int MINIMUM_ON_HAND, string RELEASE_STATUS, string START_NUMBER, string END_NUMBER, string TECHNICAL_REFERENCE, string COMMENTS, string COMPATIBILITY, string NOTES, string CREATED_BY, DateTime DATE_CREATED, string UPDATED_BY, DateTime LAST_UPDATED, string USER_MANUAL_FILE) {
            try {
                string connection = softwareConfigMgr.SoftwarePlusDBConnectionString();

                OracleConnection orcleCon = new OracleConnection(connection);
                OracleParameter[] oraParam = new OracleParameter[19];

                oraParam[0] = new OracleParameter("xMODEL_ID", MODEL_ID);
                oraParam[1] = new OracleParameter("xVERSION", VERSION);
                oraParam[2] = new OracleParameter("xRELEASE_DATE", RELEASE_DATE);
                oraParam[3] = new OracleParameter("xSHIP_DATE", SHIP_DATE);
                oraParam[4] = new OracleParameter("xRELEASE_TYPE", RELEASE_TYPE);
                oraParam[5] = new OracleParameter("xVISIBILITY_TYPE", VISIBILITY_TYPE);
                oraParam[6] = new OracleParameter("xDISTRIBUTION_TYPE", DISTRIBUTION_TYPE);
                oraParam[7] = new OracleParameter("xRELEASE_QUANTITY", RELEASE_QUANTITY);
                oraParam[8] = new OracleParameter("xMINIMUM_ON_HAND", MINIMUM_ON_HAND);
                oraParam[9] = new OracleParameter("xRELEASE_STATUS", RELEASE_STATUS);
                oraParam[10] = new OracleParameter("xSTART_NUMBER", START_NUMBER);
                oraParam[11] = new OracleParameter("xEND_NUMBER", END_NUMBER);
                oraParam[12] = new OracleParameter("xTECHNICAL_REFERENCE", TECHNICAL_REFERENCE);
                oraParam[13] = new OracleParameter("xCOMMENTS", COMMENTS);
                oraParam[14] = new OracleParameter("xCOMPATIBILITY", COMPATIBILITY);
                oraParam[15] = new OracleParameter("xNOTES", NOTES);
                oraParam[16] = new OracleParameter("xCREATED_BY", CREATED_BY);
                oraParam[17] = new OracleParameter("xDATE_CREATED", DATE_CREATED);
                // oraParam[18] = new OracleParameter("xUPDATED_BY", UPDATED_BY);
                // oraParam[19] = new OracleParameter("xLAST_UPDATED", LAST_UPDATED);
                oraParam[18] = new OracleParameter("xUSER_MANUAL_FILE", USER_MANUAL_FILE);
                OracleDataAccess.ExecuteNonQuery(orcleCon, CommandType.StoredProcedure, "ADDRELEASELOG", oraParam);
            } catch (Exception ex) {
                throw ex;
            }


        }

        public void AddKitReleaseLog(string modelID, string version, string kitPartNumber) {
            string connection = softwareConfigMgr.SoftwarePlusDBConnectionString();

            OracleConnection orcleCon = new OracleConnection(connection);
            OracleParameter[] oraParam = new OracleParameter[3];

            oraParam[0] = new OracleParameter("xMODEL_ID", modelID);
            oraParam[1] = new OracleParameter("xVERSION", version);
            oraParam[2] = new OracleParameter("xKIT_PART_NUMBER", kitPartNumber);
            OracleDataAccess.ExecuteNonQuery(orcleCon, CommandType.StoredProcedure, "ADDRELEASELOGKITS", oraParam);
        }
        public void DeleteKitReleaseLog(string modelID, string version, string kitPartNumber) {
            string connection = softwareConfigMgr.SoftwarePlusDBConnectionString();

            OracleConnection orcleCon = new OracleConnection(connection);
            OracleParameter[] oraParam = new OracleParameter[3];

            oraParam[0] = new OracleParameter("xMODEL_ID", modelID);
            oraParam[1] = new OracleParameter("xVERSION", version);
            oraParam[2] = new OracleParameter("xKIT_PART_NUMBER", kitPartNumber);
            OracleDataAccess.ExecuteNonQuery(orcleCon, CommandType.StoredProcedure, "DELETERELEASELOGKITS", oraParam);
        }
        public void DeleteRelatedReleaseLogID(string modelID, string version, string releaseID) {
            string connection = softwareConfigMgr.SoftwarePlusDBConnectionString();

            OracleConnection orcleCon = new OracleConnection(connection);
            OracleParameter[] oraParam = new OracleParameter[3];

            oraParam[0] = new OracleParameter("xMODEL_ID", modelID);
            oraParam[1] = new OracleParameter("xVERSION", version);
            oraParam[2] = new OracleParameter("xRELEASE_ID", releaseID);
            OracleDataAccess.ExecuteNonQuery(orcleCon, CommandType.StoredProcedure, "DeleteRelatedReleaseLogID", oraParam);
        }
        public void UpdateReleaseLog(string MODEL_ID, string VERSION, DateTime RELEASE_DATE, DateTime SHIP_DATE, string RELEASE_TYPE, string VISIBILITY_TYPE, string DISTRIBUTION_TYPE, int RELEASE_QUANTITY, int MINIMUM_ON_HAND, string RELEASE_STATUS, string START_NUMBER, string END_NUMBER, string TECHNICAL_REFERENCE, string COMMENTS, string COMPATIBILITY, string NOTES, string CREATED_BY, DateTime DATE_CREATED, string UPDATED_BY, DateTime LAST_UPDATED, string USER_MANUAL_FILE) {
            try {
                string connection = softwareConfigMgr.SoftwarePlusDBConnectionString();

                OracleConnection orcleCon = new OracleConnection(connection);
                OracleParameter[] oraParam = new OracleParameter[19];

                oraParam[0] = new OracleParameter("xMODEL_ID", MODEL_ID);
                oraParam[1] = new OracleParameter("xVERSION", VERSION);
                oraParam[2] = new OracleParameter("xRELEASE_DATE", RELEASE_DATE);
                oraParam[3] = new OracleParameter("xSHIP_DATE", SHIP_DATE);
                oraParam[4] = new OracleParameter("xRELEASE_TYPE", RELEASE_TYPE);
                oraParam[5] = new OracleParameter("xVISIBILITY_TYPE", VISIBILITY_TYPE);
                oraParam[6] = new OracleParameter("xDISTRIBUTION_TYPE", DISTRIBUTION_TYPE);
                oraParam[7] = new OracleParameter("xRELEASE_QUANTITY", RELEASE_QUANTITY);
                oraParam[8] = new OracleParameter("xMINIMUM_ON_HAND", MINIMUM_ON_HAND);
                oraParam[9] = new OracleParameter("xRELEASE_STATUS", RELEASE_STATUS);
                oraParam[10] = new OracleParameter("xSTART_NUMBER", START_NUMBER);
                oraParam[11] = new OracleParameter("xEND_NUMBER", END_NUMBER);
                oraParam[12] = new OracleParameter("xTECHNICAL_REFERENCE", TECHNICAL_REFERENCE);
                oraParam[13] = new OracleParameter("xCOMMENTS", COMMENTS);
                oraParam[14] = new OracleParameter("xCOMPATIBILITY", COMPATIBILITY);
                oraParam[15] = new OracleParameter("xNOTES", NOTES);
                //  oraParam[16] = new OracleParameter("xCREATED_BY", CREATED_BY);
                //    oraParam[17] = new OracleParameter("xDATE_CREATED", DATE_CREATED);
                oraParam[16] = new OracleParameter("xUPDATED_BY", UPDATED_BY);
                oraParam[17] = new OracleParameter("xLAST_UPDATED", LAST_UPDATED);
                oraParam[18] = new OracleParameter("xUSER_MANUAL_FILE", USER_MANUAL_FILE);
                OracleDataAccess.ExecuteNonQuery(orcleCon, CommandType.StoredProcedure, "UPDATERELEASELOG", oraParam);
            } catch (Exception ex) {
                throw ex;
            }
        }

        public DataSet GetRelatedReleaseId(string modelId) {
            try {
                string connection = softwareConfigMgr.SoftwarePlusDBConnectionString();
                OracleParameter[] oraParam = new OracleParameter[2];

                oraParam[0] = new OracleParameter("xmodelnumber", modelId);
                oraParam[1] = new OracleParameter("xRelatedReleaseId", OracleDbType.RefCursor);
                oraParam[1].Direction = ParameterDirection.Output;

                DataSet ObjDS = OracleDataAccess.ExecuteDataset(connection,
                             CommandType.StoredProcedure,
                             "RELEASELOG.GetRelatedReleaseId", oraParam);

                return ObjDS;
            } catch (Exception ex) {
                throw ex;
            }
        }

        public void AddRelatedReleaseLog(string MODEL_ID, string VERSION, string relatedreleaseid) {
            try {
                string connection = softwareConfigMgr.SoftwarePlusDBConnectionString();

                OracleConnection orcleCon = new OracleConnection(connection);
                OracleParameter[] oraParam = new OracleParameter[3];

                oraParam[0] = new OracleParameter("xMODEL_ID", MODEL_ID);
                oraParam[1] = new OracleParameter("xVERSION", VERSION);
                oraParam[2] = new OracleParameter("xRELEASE_ID", relatedreleaseid);

                OracleDataAccess.ExecuteNonQuery(orcleCon, CommandType.StoredProcedure, "ADDRELATEDRELEASELOG", oraParam);
            } catch (Exception ex) {
                throw ex;
            }
        }

        public int ValidateReleaseLogData(string MODEL_ID, string VERSION) {
            try {
                string connStr = softwareConfigMgr.SoftwarePlusDBConnectionString();

                OracleConnection conn = new OracleConnection(connStr);
                OracleParameter[] oraParam = new OracleParameter[3];

                oraParam[0] = new OracleParameter("xModelId", MODEL_ID);
                oraParam[1] = new OracleParameter("xVersion", VERSION);
                oraParam[2] = new OracleParameter("retVal", OracleDbType.Int32);
                oraParam[2].Direction = ParameterDirection.Output;

                int retVal = OracleDataAccess.ExecuteNonQuery(conn, CommandType.StoredProcedure, "VALIDATERELEASEDATA", "true", oraParam);

                return retVal;
            } catch (Exception ex) {
                throw ex;
            }
        }

        public DataSet GetVersionsData(string modelId, string version) {
            try {
                string connection = softwareConfigMgr.SoftwarePlusDBConnectionString();
                OracleParameter[] oraParam = new OracleParameter[3];

                oraParam[0] = new OracleParameter("xModelId", modelId);
                oraParam[1] = new OracleParameter("xVersion", version);
                oraParam[2] = new OracleParameter("xVersionInfo", OracleDbType.RefCursor);
                oraParam[2].Direction = ParameterDirection.Output;

                DataSet ObjDS = OracleDataAccess.ExecuteDataset(connection, CommandType.StoredProcedure, "RELEASELOG.GetVersionsData", oraParam);

                return ObjDS;
            } catch (Exception ex) {
                throw ex;
            }
        }

        public DataSet GetChildReleaseID(string modelId, string version) {
            try {
                string connection = softwareConfigMgr.SoftwarePlusDBConnectionString();
                OracleParameter[] oraParam = new OracleParameter[3];

                oraParam[0] = new OracleParameter("xModelId", modelId);
                oraParam[1] = new OracleParameter("xVersion", version);
                oraParam[2] = new OracleParameter("xChildReleaseInfo", OracleDbType.RefCursor);
                oraParam[2].Direction = ParameterDirection.Output;

                DataSet ObjDS = OracleDataAccess.ExecuteDataset(connection,
                             CommandType.StoredProcedure,
                             "RELEASELOG.GetChildReleaseId", oraParam);

                return ObjDS;
            } catch (Exception ex) {
                throw ex;
            }

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="modelId"></param>
        /// <param name="version"></param>
        public void UpdateVersionInfo(int shipping, int upgrade, int special, string modelId, string version) {
            try {
                string connection = softwareConfigMgr.SoftwarePlusDBConnectionString();
                OracleParameter[] oraParam = new OracleParameter[4];

                oraParam[0] = new OracleParameter("xShipping", shipping);
                oraParam[1] = new OracleParameter("xUpgrde", upgrade);
                oraParam[2] = new OracleParameter("xSpecial", special);
                oraParam[3] = new OracleParameter("xModelid", modelId);
                oraParam[4] = new OracleParameter("xVersion", version);


                OracleDataAccess.ExecuteNonQuery(connection,
                              CommandType.StoredProcedure,
                              "RELEASELOG.UPDATEVERSIONINFO", oraParam);


            } catch (Exception ex) {
                throw ex;
            }

        }
        #endregion
        public DataSet GetReleaseIdVersions() {
            try {
                string connection = softwareConfigMgr.SoftwarePlusDBConnectionString();
                OracleParameter[] oraParam = new OracleParameter[1];

                oraParam[0] = new OracleParameter("xModelIds", OracleDbType.RefCursor);
                oraParam[0].Direction = ParameterDirection.Output;


                DataSet ObjDS = OracleDataAccess.ExecuteDataset(connection,
                             CommandType.StoredProcedure,
                             "RELEASELOG.GetReleaseIdVersions", oraParam);

                return ObjDS;
            } catch (Exception ex) {
                throw ex;
            }


        }
        public DataSet GetReleaseIdVersionsData(string modelId, string version) {
            try {
                string connection = softwareConfigMgr.SoftwarePlusDBConnectionString();
                OracleParameter[] oraParam = new OracleParameter[3];

                oraParam[0] = new OracleParameter("xModelId", modelId);
                oraParam[1] = new OracleParameter("xVersion", version);

                oraParam[2] = new OracleParameter("xReleaseInfo", OracleDbType.RefCursor);

                oraParam[2].Direction = ParameterDirection.Output;

                DataSet ObjDS = OracleDataAccess.ExecuteDataset(connection,
                             CommandType.StoredProcedure,
                             "RELEASELOG.GetReleaseIdVersionsData", oraParam);

                return ObjDS;
            } catch (Exception ex) {
                throw ex;
            }


        }
        public void DeleteReleaseLog(string modelId, string version) {
            try {
                string connection = softwareConfigMgr.SoftwarePlusDBConnectionString();
                OracleParameter[] oraParam = new OracleParameter[2];

                oraParam[0] = new OracleParameter("xModelId", modelId);
                oraParam[1] = new OracleParameter("xVersion", version);

                //oraParam[2] = new OracleParameter("xReleaseInfo", OracleDbType.RefCursor);

                // oraParam[2].Direction = ParameterDirection.Output;

                OracleDataAccess.ExecuteNonQuery(connection,
                             CommandType.StoredProcedure,
                             "DELETERELEASELOG", oraParam);

                //  return ObjDS;
            } catch (Exception ex) {
                throw ex;
            }
        }
        public DataSet GetKitInfoForReleaseLog(string modelId, string version) {
            try {
                string connection = softwareConfigMgr.SoftwarePlusDBConnectionString();
                OracleParameter[] oraParam = new OracleParameter[3];
                oraParam[0] = new OracleParameter("xModelId", modelId);
                oraParam[1] = new OracleParameter("xVersion", version);

                oraParam[2] = new OracleParameter("xKitInfoForRelease", OracleDbType.RefCursor);
                oraParam[2].Direction = ParameterDirection.Output;


                DataSet ObjDS = OracleDataAccess.ExecuteDataset(connection,
                             CommandType.StoredProcedure,
                             "KITINFO.GetKitInfoForReleaseLog", oraParam);

                return ObjDS;
            } catch (Exception ex) {
                throw ex;
            }


        }
        public DataSet GetKitItemInfoByReleaseLog(string modelId, string version) {
            try {
                string connection = softwareConfigMgr.SoftwarePlusDBConnectionString();
                OracleParameter[] oraParam = new OracleParameter[3];
                oraParam[0] = new OracleParameter("xModelId", modelId);
                oraParam[1] = new OracleParameter("xVersion", version);

                oraParam[2] = new OracleParameter("xKitInfoForRelease", OracleDbType.RefCursor);
                oraParam[2].Direction = ParameterDirection.Output;


                DataSet ObjDS = OracleDataAccess.ExecuteDataset(connection,
                             CommandType.StoredProcedure,
                             "RELEASELOG.GetKitItemInfoByReleaseLog", oraParam);

                return ObjDS;
            } catch (Exception ex) {
                throw ex;
            }


        }
        public DataSet GetReleaseLogData(string modelId) {
            try {
                string connection = softwareConfigMgr.SoftwarePlusDBConnectionString();
                OracleParameter[] oraParam = new OracleParameter[2];

                oraParam[0] = new OracleParameter("xModelId", modelId);
                oraParam[1] = new OracleParameter("xReleaseLogInfo", OracleDbType.RefCursor);
                oraParam[1].Direction = ParameterDirection.Output;

                DataSet ObjDS = OracleDataAccess.ExecuteDataset(connection,
                             CommandType.StoredProcedure,
                             "RELEASELOG.GETRELEASELOGDATA", oraParam);

                return ObjDS;
            } catch (Exception ex) {
                throw ex;
            }


        }
        public DataSet GetFTPReleaseLogData() {
            try {
                string connection = softwareConfigMgr.SoftwarePlusDBConnectionString();
                OracleParameter[] oraParam = new OracleParameter[1];

                //  oraParam[0] = new OracleParameter("xModelId", modelId);
                oraParam[0] = new OracleParameter("xFTPReleaseLogData", OracleDbType.RefCursor);
                oraParam[0].Direction = ParameterDirection.Output;

                DataSet ObjDS = OracleDataAccess.ExecuteDataset(connection,
                             CommandType.StoredProcedure,
                             "RELEASELOG.GetFTPReleaseLogData", oraParam);

                return ObjDS;
            } catch (Exception ex) {
                throw ex;
            }


        }
    }
}