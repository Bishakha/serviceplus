<%@ Page Language="VB" AutoEventWireup="false" EnableViewStateMac="true" SmartNavigation="False" CodeFile="sony-repair-maintenance-service.aspx.vb" Inherits="ServicePLUSWebApp.sony_repair_maintenance_service1" %>

<%@ Reference Page="~/SSL.aspx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="SonyFooter" Src="SonyFooter.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="TopSonyHeader" Src="TopSonyHeader.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="PersonalMessage" Src="PersonalMessage.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="ServicePlusNav" Src="ServicePlusNav.ascx" %>
<%@ Register Src="~\UserControl\SPSPromotion.ascx" TagName="SPSPromotion" TagPrefix="uc1" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="<%=PageLanguage %>">
<head>
    <title><%=Resources.Resource.repair_mn_hdr_msg()%></title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <meta content="Repair, Maintenance, Services, Sony Models, Sony Professional Models, Sony Broadcast Models, Sony Business Models, Sony Software, Sony Professional Software, Sony Broadcast Software, Sony Business Software"
        name="keywords">
    <link href="includes/ServicesPLUS_style.css" type="text/css" rel="stylesheet">

    <script src="includes/jquery-1.10.2.min.js" type="text/javascript"></script>
    <script src="includes/ServicesPLUS.js" type="text/javascript"></script>
    <script src="includes/svcplus_globalization.js" type="text/javascript"></script>

    <script type="text/javascript">
        function Show() {
            alert('Sai');
        }
        function GoDepot() {
            if (document.Form1.ModelNumber.value === "") {
                alert('<%=Resources.Resource.repair_sny_pg_mdlsrch_msg()%>');
            }
        }

        function SetFocus() {
            document.Form1.ModelNumber.focus();
        }
        var minutes = 1000 * 60
        var hours = minutes * 60
        var days = hours * 24
        var years = days * 365
        var d = new Date()
        var t = d.getTime()
        var y = t / years
        //alert(y);
        function setHdn() {
            //            var minutes = 1000*60
            //            var hours = minutes*60
            //            var days = hours*24
            //            var years = days*365
            //            var d = new Date()
            //            var t = d.getTime()
            //            var y = t/years
            document.getElementById("hndLogin").value = y;
            //alert(y);
        }
    </script>
</head>
<body style="color: #000000; background: #5d7180; margin: 0px;" onload="javascript:SetFocus();ShowLogin();">
    <form id="Form1" method="post" runat="server">
        <center>
            <table width="710" border="0" role="presentation">
                <tr>
                    <td width="25" style="background: url('images/sp_left_bkgd.gif')">
                        <img height="25" src="images/spacer.gif" width="25" alt="">
                    </td>
                    <td width="689" bgcolor="#ffffff">
                        <table width="710" border="0" role="presentation">
                            <tr>
                                <td width="582">
                                    <ServicePLUSWebApp:TopSonyHeader ID="TopSonyHeader" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td width="582" height="23">
                                    <nav><ServicePLUSWebApp:ServicePlusNav ID="SonyNav" runat="server" /></nav>
                                </td>
                            </tr>
                            <tr>
                                <td width="582">
                                    <table width="710" border="0" role="presentation">
                                        <tr>
                                            <td align="right" background="images/repair_lrg.gif" bgcolor="#363d45"
                                                height="82" style="width: 465px">
                                                <br>
                                                <h1 class="headerText"><%=Resources.Resource.repair_snysvc_srl_msg_4 %></h1>
                                            </td>
                                            <td valign="middle" width="238" bgcolor="#363d45">
                                                <ServicePLUSWebApp:PersonalMessage ID="PersonalMessageDisplay" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td bgcolor="#f2f5f8" style="width: 465px">
                                                <h2 class="headerTitle" style="padding-right: 20px; text-align: right;"><%=Resources.Resource.repair_sny_pg_svcrqst_msg %></h2>
                                            </td>
                                            <td width="238" bgcolor="#99a8b5">
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr height="9">
                                            <td bgcolor="#f2f5f8" style="width: 465px">
                                                <img height="9" src="images/sp_int_header_btm_left_onepix.gif" width="464" alt="">
                                            </td>
                                            <td width="238" bgcolor="#99a8b5">
                                                <img height="9" src="images/sp_int_header_btm_right.gif" width="246" alt="">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="100%" border="0" role="presentation">
                                        <tr>
                                            <td style="width: 12px; height: 22px;">
                                                <img height="20" src="images/spacer.gif" width="20" alt="">
                                            </td>
                                            <td style="height: 22px; width: 1046px;">&nbsp;<asp:Label ID="ErrorLabel" runat="server" Text="" CssClass="redAsterick" EnableViewState="false" /></td>
                                            <td style="width: 436px; height: 22px;">
                                                <img height="20" src="images/spacer.gif" width="20" alt="">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 12px; height: 24px;">
                                                <img height="20" src="images/spacer.gif" width="20" alt="">
                                            </td>
                                            <td style="width: 1046px; height: 24px;">
                                                <p class="promoCopyBold">
                                                    <%=Resources.Resource.repair_svc_main_msg()%>
                                                </p>
                                            </td>
                                            <td align="center" style="width: 436px; height: 24px;">
                                                &nbsp;&nbsp;
								                    <img height="20" src="images/spacer.gif" width="20" alt="">
                                                &nbsp;&nbsp;
                                            </td>
                                        </tr>
                                        <tr valign="top">
                                            <td style="width: 12px">
                                                <img height="20" src="images/spacer.gif" width="20" alt="">
                                            </td>
                                            <td style="width: 1046px">
                                                <p class="promoCopy">
                                                    <table border="0" cellpadding="2" cellspacing="1" style="width: 100%" role="presentation">
                                                        <tr>
                                                            <td style="height: 23px; width: 446px;">
                                                                &nbsp;
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 446px; height: 14px">
                                                                <p class="promoCopyBold">
                                                                    <%=Resources.Resource.repair_dptsvc_msg()%>
                                                                </p>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 446px">
                                                                <span style="font-size: 8pt; color: #666666; font-family: Arial"><%=Resources.Resource.repair_dptsvc_msg_1()%></span>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 446px">
                                                                <p class="bodyCopy">
                                                                    <strong><%=Resources.Resource.repair_md_msg()%><SPS:SPSTextBox ID="ModelNumber" runat="server" CssClass="bodyCopy"></SPS:SPSTextBox></strong>&nbsp;
                                                                    <asp:Label ID="lblErrMsgModel" runat="server" Font-Bold="True" ForeColor="Red" Text="Label"
                                                                        Visible="False"></asp:Label>
                                                                </p>
                                                            </td>
                                                        </tr>
                                                        <tr style="font-size: 8pt; color: #666666">
                                                            <td style="width: 446px">
                                                                <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="<%$Resources:Resource,repair_snypg_mdl_type_3_img%>"
                                                                    ToolTip="Request Depot Serviec" />
                                                            </td>
                                                        </tr>

                                                        <tr>
                                                            <td style="width: 443px">
                                                                <span class="redAsterick"><%=Resources.Resource.repair_newchksts_msg()%></span>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <a href="#">
                                                                    <img src="<%=Resources.Resource.repair_snypg_mdl_type_2_img()%>" onclick="javascript:window.location='sony-repair-status.aspx';" border="0" alt="Repair Service Status" id="imbSearchRepairStatus" /></a><br />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <img height="20" src="images/spacer.gif" width="20" alt="">
                                                            </td>
                                                        </tr>
                                                        <tr style="font-size: 8pt; color: #666666">
                                                            <td style="width: 446px">
                                                                <p class="promoCopyBold">
                                                                    <span style="color: #666666">repair_fs_msg</span>
                                                                </p>
                                                            </td>
                                                        </tr>
                                                        <tr style="font-size: 8pt; color: #666666">
                                                            <td style="width: 446px">
                                                                <span style="font-size: 8pt; color: #666666; font-family: Arial"><%=Resources.Resource.repair_fs_msg_1()%><%=Resources.Resource.repair_fs_msg_2()%> <%=Resources.Resource.repair_fs_msg_3()%></span>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="height: 13px; width: 446px;">
                                                                <span class="bodyCopy"><strong><%=Resources.Resource.repair_state_msg()%>&nbsp;</strong></span>
                                                                <asp:DropDownList ID="ddlStates" runat="server" Style="font-size: 11px; color: black; line-height: 13px; font-family: Arial, Helvetica, sans-serif"
                                                                   >
                                                                </asp:DropDownList>
                                                                <asp:Label ID="lblErrMsgState" runat="server" CssClass="bodycopy" Font-Bold="True"
                                                                    ForeColor="Red" Text="Label" Visible="False"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 446px">
                                                                <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="<%$Resources:Resource,repair_snypg_mdl_type_1_img%>"
                                                                    ToolTip="Request Field Service" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </p>
                                            </td>
                                            <td height="260" width="230" valign="top" align="center">
                                                <table id="Table2" style="height: 240px" border="0" align="center" role="presentation">
                                                    <tr valign="top">
                                                        <td>
                                                            <table border="0" align="left" style="width: 100%" role="presentation">
                                                                <tr valign="top">
                                                                    <td style="height: 24px">
                                                                        <asp:HyperLink ID="topPreviousImg" runat="server" CssClass="tableData"><IMG src="images/sp_int_leftArrow_btn.gif" border="0" alt="Previous Page"></asp:HyperLink>&nbsp;
                                                        <asp:HyperLink ID="topPrevious" runat="server" CssClass="tableData"><%=Resources.Resource.el_Previous()%></asp:HyperLink>
                                                                        <img height="20" src="images/spacer.gif" width="20" alt="" />
                                                                        <asp:HyperLink ID="topNext" runat="server" CssClass="tableData"><%=Resources.Resource.el_Next()%></asp:HyperLink>&nbsp;
                                                        <asp:HyperLink ID="topNextImg" runat="server" CssClass="tableData"><IMG src="images/sp_int_rightArrow_btn.gif" border="0" alt="Next Page"></asp:HyperLink>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>&nbsp;</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:GridView ID="GridPartsGroup" runat="server" AutoGenerateColumns="False">
                                                                            <Columns>
                                                                                <asp:BoundField AccessibleHeaderText="Group ID" DataField="ModelName" HeaderText="Group ID"
                                                                                    Visible="False" />
                                                                                <asp:HyperLinkField AccessibleHeaderText="From" DataTextField="ModelName" HeaderText="Model Number">
                                                                                    <HeaderStyle HorizontalAlign="Center" Wrap="false" />
                                                                                </asp:HyperLinkField>
                                                                                <asp:HyperLinkField AccessibleHeaderText="To" DataTextField="ModelDescription" HeaderText="Description">
                                                                                    <HeaderStyle HorizontalAlign="Center" Wrap="false" />
                                                                                </asp:HyperLinkField>
                                                                            </Columns>
                                                                            <RowStyle BackColor="#F2F5F8" CssClass="tableData" />
                                                                            <HeaderStyle BackColor="#D5DEE9" CssClass="tableHeader" />
                                                                            <AlternatingRowStyle BackColor="White" />
                                                                        </asp:GridView>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="height: 26px">
                                                                        <asp:HyperLink ID="btmPreviousImg" runat="server" CssClass="tableData"><IMG src="images/sp_int_leftArrow_btn.gif" border="0" alt="Previous Page"></asp:HyperLink>&nbsp;
                                                        <asp:HyperLink ID="btmPrevious" runat="server" CssClass="tableData"><%=Resources.Resource.el_Previous()%></asp:HyperLink>
                                                                        <img height="20" src="images/spacer.gif" width="20" alt="" />
                                                                        <asp:HyperLink ID="btmNext" runat="server" CssClass="tableData"><%=Resources.Resource.el_Next()%></asp:HyperLink>&nbsp;
                                                        <asp:HyperLink ID="btmNextImg" runat="server" CssClass="tableData"><IMG src="images/sp_int_rightArrow_btn.gif" border="0" alt="Next Page"></asp:HyperLink>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <a href="#top">
                                                                            <img src="images/sp_int_back2top_btn.gif" width="78" height="28" border="0" alt="Back to Top"></a>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>

                                            <td width="10">
                                                <img height="10" src="images/spacer.gif" width="10" alt="">
                                            </td>
                                            <td colspan="2">
                                                <%If divrprmainShow = True Then%>
                                                <table border="0" role="presentation">
                                                    <tr>
                                                        <td>
                                                            <uc1:SPSPromotion ID="Promotions1" DisplayPage="sony-repair-maintenance-service" Type="2" runat="server" />
                                                        </td>
                                                        <td width="16">
                                                            <img src="images/spacer.gif" width="15" alt="">
                                                        </td>
                                                        <td>
                                                            <uc1:SPSPromotion ID="SPSPromotionBottomRight" DisplayPage="sony-repair-maintenance-service" Type="3" runat="server" />
                                                        </td>
                                                    </tr>
                                                </table>
                                                <%End If%>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>

                                <td width="582">
                                    <footer><ServicePLUSWebApp:SonyFooter ID="SonyFooter1" runat="server" /></footer>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="25" style="background: url('images/sp_right_bkgd.gif')">
                        <img src="images/spacer.gif" width="25" height="20" alt="">
                    </td>
                </tr>
            </table>
            <input type="hidden" id="hndLogin" runat="server">
        </center>
    </form>

</body>
</html>
