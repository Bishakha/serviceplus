Imports System.Data
Imports System.Drawing
Imports ServicesPlusException
Imports Sony.US.AuditLog
Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Data
Imports Sony.US.ServicesPLUS.Process
Imports Sony.US.siam
Imports Sony.US.SIAMUtilities

Namespace ServicePLUSWebApp


    Partial Class ContactInformation
        Inherits SSL

#Region " Web Form Designer Generated Code "
        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As EventArgs) Handles MyBase.Init
            isProtectedPage(True)
            'config_settings = handler.GetConfigSettings("//item[@name='SecurityManagement']")
            InitializeComponent()
            Page.ID = Request.Url.AbsolutePath
        End Sub
#End Region

#Region "Page members"
        Const conStyleSheetForInterestControls As String = "bodyCopy"
        Const conBgColorForMarketingInterest As String = "#ffffff"
        Const conBgColorForMarketingInterestAlt As String = "#f2f5f8"
        Const conColorTextError As String = "#ea132e"
        Const conColorTextRegular As String = "#2a3d47"
        Const conControlsPerRow As Int32 = 3
        Const conNumberOfCCsToSave As Int16 = 4
        Const conExpiresInDays As Int16 = 45
        Const conDefaultFormFieldFocus = "txtFirstName"

        Protected WithEvents ExpirationDate As Sony.US.ServicesPLUS.Controls.SPSTextBox
        Private updatePassword As Boolean = False
        Private updateCreditCard As Boolean = False
        Public focusFormField As String = ""
        Public CCNo As String = ""
        Private cm As New CustomerManager
#End Region

#Region "Events"
        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As EventArgs) Handles MyBase.Load
            Try
                If Not IsPostBack Then
                    Session.Remove("vcode")
                    InitZipControls()
                    ProcessPage(sender, e)
                    FormFieldInitialization()
                    setFocusField(conDefaultFormFieldFocus)
                End If
            Catch ex As Exception
                MasterErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                MasterErrorLabel.Focus()
            End Try
        End Sub

        Private Sub UpdateProfile_Click(ByVal sender As System.Object, ByVal e As ImageClickEventArgs) Handles btnUpdateProfile.Click
            Dim debugMsg As String = $"Contact-Information.UpdateProfile_Click - START at {DateTime.Now.ToShortTimeString()}.{Environment.NewLine}"
            Try
                If HttpContextManager.Customer IsNot Nothing Then
                    Dim customer As Customer = HttpContextManager.Customer
                    If customer.EmailAddress <> txtEmailAddress.Text.Trim().ToLower() Then 'If customer changing email address 
                        If Session("vcode") Is Nothing Then
                            ' Check if the new email is already in the DB
                            Dim isValid As Boolean = True
                            Dim custMgr As New CustomerManager

                            txtVerificationCode.Visible = False
                            lblVerificationCode.Visible = False
                            lblVerificationCodeError.Visible = False
                            If (custMgr.ValidateCustomerData(txtEmailAddress.Text.Trim().ToUpper())) = 1 Then
                                isValid = False
                                ' Emailaddress already exists with statuscode = 1, hence show error "Requested username is unavailable."
                                MasterErrorLabel.Text = ConfigurationData.GeneralSettings.SecurityManagement.UserNameUnAvailable
                                MasterErrorLabel.Focus()
                            End If

                            If (isValid) Then 'email id is available then send verification code .
                                txtVerificationCode.Visible = True
                                lblVerificationCode.Visible = True
                                lblVerificationCodeError.Visible = True
                                AutogenerateCode(txtFirstName.Text, txtLastName.Text, txtEmailAddress.Text)
                                'stop processing update and ask user to enter verification code .
                                MasterErrorLabel.Text = Resources.Resource.el_msg_vcode_error
                                MasterErrorLabel.Focus()
                            End If
                        Else
                            'check vcode and compare with textbox val
                            If Session("vcode").ToString() = txtVerificationCode.Text.Trim() Then
                                ProcessPage(sender, e)
                                'Update  Usertable isverified to true =1

                                Dim cm_email As New TempCustomerValues With {
                                    .EmailAddress = txtEmailAddress.Text.Trim(),
                                    .IsEmailVerified = 1
                                }
                                Dim customer_email As New TempCustomer()
                                customer_email.UpdateCustomer(cm_email)
                            Else
                                If String.IsNullOrWhiteSpace(txtVerificationCode.Text) Then
                                    MasterErrorLabel.Text = Resources.Resource.el_msg_vcode_error
                                    MasterErrorLabel.Focus()
                                Else
                                    MasterErrorLabel.Text = Resources.Resource.el_msg_vcode_valid_error
                                    MasterErrorLabel.Focus()
                                End If
                            End If
                        End If
                    ElseIf customer.EmailAddress.Equals(txtEmailAddress.Text.Trim()) Then
                        'Need to check existing useremail address verified or not if not send verification code.
                        Dim temp_customer As New TempCustomer()
                        Dim temp As String = temp_customer.GetCustomerIsverified(txtEmailAddress.Text.Trim())

                        If String.IsNullOrEmpty(temp) OrElse temp = "0" Then
                            'send verification code to email id
                            txtVerificationCode.Visible = True
                            lblVerificationCode.Visible = True
                            lblVerificationCodeError.Visible = True

                            If String.IsNullOrWhiteSpace(Session("vcode")?.ToString()) Then
                                AutogenerateCode(txtFirstName.Text, txtLastName.Text, txtEmailAddress.Text)
                                'stop processing update and ask user to enter verification code .
                                MasterErrorLabel.Text = Resources.Resource.el_msg_vcode_error
                                MasterErrorLabel.Focus()
                            Else 'If Session("vcode") IsNot Nothing Then ' ASleight - Redundant "Else If"; can just be "Else"
                                'check vcode and compare with textbox val
                                Dim str_vc As String = Session("vcode").ToString()
                                Dim Str_txt_v As String = txtVerificationCode.Text
                                If str_vc.Equals(Str_txt_v, StringComparison.InvariantCultureIgnoreCase) Then
                                    'Update  Usertable isverified to true =1
                                    ProcessPage(sender, e)
                                    Dim cm_email As New TempCustomerValues With {
                                        .EmailAddress = txtEmailAddress.Text.Trim().ToLower(),
                                        .IsEmailVerified = 1
                                    }
                                    Dim customer_email As New TempCustomer()
                                    customer_email.UpdateCustomer(cm_email)
                                Else
                                    If String.IsNullOrWhiteSpace(txtVerificationCode.Text) Then
                                        MasterErrorLabel.Text = Resources.Resource.el_msg_vcode_error
                                        MasterErrorLabel.Focus()
                                    Else
                                        MasterErrorLabel.Text = Resources.Resource.el_msg_vcode_valid_error
                                        MasterErrorLabel.Focus()
                                    End If
                                End If
                            End If
                        ElseIf temp = "1" Then
                            ProcessPage(sender, e)
                        End If
                    End If
                End If
            Catch ex As Exception
                MasterErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                MasterErrorLabel.Focus()
            End Try
        End Sub

#End Region

#Region "Methods"

#Region "control method"
        Private Sub AutogenerateCode(ByVal firstname As String, ByVal Secondname As String, ByVal Emailaddress As String)
            Session("vcode") = ""
            ' code for generating verification code Start
            Try
                Dim CustomerDB As TempCustomer = New TempCustomer
                Dim temp_user As TempCustomerValues = New TempCustomerValues
                'If Not IsPostBack Then
                temp_user.FirstName = firstname
                temp_user.LastName = Secondname
                temp_user.EmailAddress = Emailaddress
                'If user exists check start
                Dim ds_TempUser As DataSet = CustomerDB.GetCustomerDetails(temp_user)
                If ds_TempUser.Tables(0).Rows.Count > 0 Then
                    'User Exist 
                    Dim existing_user_FirstName As String = Convert.ToString(ds_TempUser.Tables(0).Rows(0).Item("FIRSTNAME"))
                    Dim existing_user_LastName As String = Convert.ToString(ds_TempUser.Tables(0).Rows(0).Item("LASTNAME"))
                    Dim existing_user_EmailAddress As String = Convert.ToString(ds_TempUser.Tables(0).Rows(0).Item("EMAILADDRESS"))
                    Dim existing_user_VerificationCode As String = Convert.ToString(ds_TempUser.Tables(0).Rows(0).Item("VERIFICATIONCODE"))
                    Dim existing_user_createddate As DateTime = Convert.ToDateTime(ds_TempUser.Tables(0).Rows(0).Item("CREATEDDATE"))
                    'check expired or not 
                    'Generate code,update andsent to customer start 
                    Dim curdate As DateTime = DateTime.Now
                    Dim duration As TimeSpan = curdate - existing_user_createddate
                    If duration.TotalHours > 24 Then
                        Dim verificationcode As String
                        verificationcode = AutoGeneratedCode.GetString()
                        ' Response.Write(verificationcode)
                        Session("vcode") = verificationcode
                        ' Response.Write("Newly generated code " + Session("vcode"))
                        temp_user.VerificationCode = Session("vcode")
                        CustomerDB.AddCustomer(temp_user, True)
                        'Send email to User Start
                        Dim cm As New CustomerManager
                        Dim EmailBody As New StringBuilder(5000)
                        Dim Subject As String = "Welcome to  Servicesplus "
                        BuildMailBody(EmailBody, Subject, temp_user.FirstName, temp_user.LastName)
                        cm.SendRegisterWelcomeEmail(temp_user.EmailAddress, EmailBody.ToString(), Subject) 'Enhanc 
                        'Send email to User END
                    ElseIf duration.TotalHours < 24 Then
                        'existing code will sent to customer START 
                        Session("vcode") = Convert.ToString(ds_TempUser.Tables(0).Rows(0).Item("VERIFICATIONCODE"))
                        Dim cm As New CustomerManager
                        Dim EmailBody As New StringBuilder(5000)
                        Dim Subject As String = "Welcome to Servicesplus "
                        BuildMailBody(EmailBody, Subject, temp_user.FirstName, temp_user.LastName)
                        cm.SendRegisterWelcomeEmail(temp_user.EmailAddress, EmailBody.ToString(), Subject) 'Enhanc 

                        'existing code will sent to customer END 
                    End If

                    'If user exists check end
                Else

                    'If New user registering Start

                    Dim verificationcode As String
                    verificationcode = AutoGeneratedCode.GetString()
                    ' Response.Write(verificationcode)
                    Session("vcode") = verificationcode
                    temp_user.VerificationCode = Session("vcode")
                    CustomerDB.AddCustomer(temp_user, False)
                    'Send Email 
                    Dim cm As New CustomerManager
                    Dim EmailBody As New StringBuilder(5000)
                    Dim Subject As String = "Welcome to Servicesplus."
                    BuildMailBody(EmailBody, Subject, temp_user.FirstName, temp_user.LastName)
                    cm.SendRegisterWelcomeEmail(temp_user.EmailAddress, EmailBody.ToString(), Subject) 'Enhanc 
                    'If New user registering End
                End If
            Catch ex As Exception
                'Need to change.

                If (ex.Message.Contains(Resources.Resource.el_Err_TimeOut) Or ex.Message.Contains(Resources.Resource.el_Err_Exception) Or ex.Message.Contains(Resources.Resource.el_Err_Request)) Then
                    MasterErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                    Dim er As String
                    er = MasterErrorLabel.Text.Replace("Unexpected error encountered. For assistance, please", Resources.Resource.el_Err_Unexp_Replace) 'Operation to register timed out. Please try again or")
                    MasterErrorLabel.Text = er
                    MasterErrorLabel.Focus()
                Else
                    Utilities.WrapExceptionforUI(ex)
                    'ErrorLabel.Text = "Registration has encountered an error: First Name, Last Name, Company, Email Address, and/or Password are not valid. Please try again."'6918
                    MasterErrorLabel.Text = "Profile update process has encountered an error: Please try again." '6918
                    MasterErrorLabel.Focus()
                End If
            End Try

            ' Response.Write(verificationcode)


            ' code for generating verification code end
        End Sub

        Sub BuildMailBody(ByRef Emailbody As StringBuilder, ByRef Subject As String, ByVal FirstName As String, ByVal Lastname As String)
            Dim privacy_policy_link As String = ConfigurationData.GeneralSettings.URL.Privacy_Policy_Link
            Dim terms_and_conditions_link As String = ConfigurationData.GeneralSettings.URL.Terms_And_Conditions_Link
            Dim contact_us_link As String = ConfigurationData.GeneralSettings.URL.Contact_Us_Link
            Dim terms_and_conditions_link_ca As String = ConfigurationData.GeneralSettings.URL.Terms_And_Conditions_Link_ca
            Dim contact_us_link_ca As String = ConfigurationData.GeneralSettings.URL.Contact_Us_Link_ca

            Emailbody.Append(Resources.Resource_mail.ml_common_Dear_en + FirstName + " " + Lastname + "," + vbCrLf + vbCrLf)
            Emailbody.Append(Resources.Resource_mail.ml_Verificationcode_en + " " + Convert.ToString(Session("vcode")) + "" + vbCrLf + vbCrLf)
            Emailbody.Append(Resources.Resource_mail.ml_newuserconfirm_body_line1_en)
            Emailbody.Append(Resources.Resource_mail.ml_newuserconfirm_body_line2_en)
            Emailbody.Append(Resources.Resource_mail.ml_newuserconfirm_body_line3_en)
            Emailbody.Append("www.sony.com/servicesplus" + vbCrLf + vbCrLf)
            Emailbody.Append(Resources.Resource_mail.ml_newuserconfirm_body_line8_en + vbCrLf + vbCrLf)
            Emailbody.Append(Resources.Resource_mail.ml_common_Sincerely_en + vbCrLf)
            Emailbody.Append(Resources.Resource_mail.ml_common_ServicesPLUSTeam_en + vbCrLf + vbCrLf)
            Emailbody.Append(Resources.Resource_mail.ml_common_CopyRights_en + vbCrLf + vbCrLf)
            Emailbody.Append(Resources.Resource_mail.ml_common_ContactUs_en + contact_us_link_ca + vbCrLf)
            Emailbody.Append(Resources.Resource_mail.ml_common_TC_en + terms_and_conditions_link_ca + vbCrLf)
            Emailbody.Append(Resources.Resource_mail.ml_common_PrivacyPolicy_en + privacy_policy_link + vbCrLf)
            Subject = Resources.Resource_mail.ml_newuserconfirm_subject_en
        End Sub

        Private Sub ProcessPage(ByVal sender As System.Object, ByRef e As EventArgs)
            Dim pciLog As New PCILogger()
            Dim customer As Customer = HttpContextManager.Customer
            Dim accountType As enumAccountType
            Dim callingControl As String = String.Empty
            Dim userType As String
            Dim errorstring As String = String.Empty

            Try
                If HttpContextManager.Customer IsNot Nothing Then
                    customer = HttpContextManager.Customer
                    pciLog.EventOriginApplication = "ServicesPLUS"
                    pciLog.EventOriginApplicationLocation = Me.Page.GetType().Name
                    pciLog.EventOriginMethod = "processPage"
                    pciLog.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
                    pciLog.OperationalUser = Environment.UserName
                    pciLog.CustomerID = customer.CustomerID
                    pciLog.CustomerID_SequenceNumber = customer.SequenceNumber
                    pciLog.EmailAddress = customer.EmailAddress
                    pciLog.SIAM_ID = customer.SIAMIdentity
                    pciLog.LDAP_ID = customer.LdapID
                    pciLog.EventType = EventType.Update
                    pciLog.EventDateTime = DateTime.Now.ToLongTimeString()
                    pciLog.HTTPRequestObjectValues = pciLog.GetRequestContextValue()
                    pciLog.IndicationSuccessFailure = "Success"
                    pciLog.Message = ""

                    userType = customer.UserType
                    If userType = "A" Or userType = "P" Then
                        accountType = enumAccountType.AccountHolder
                    ElseIf userType = "I" Then
                        accountType = enumAccountType.Internal
                    ElseIf userType = "C" Then
                        accountType = enumAccountType.NonAccount
                    ElseIf userType = "U" Then
                        accountType = enumAccountType.Unknown
                    Else
                        accountType = enumAccountType.Unknown
                    End If

                    If sender.ID IsNot Nothing Then callingControl = sender.ID.ToString()
                    Select Case callingControl
                        Case Page.ID.ToString()
                            DisplayThisCustomerProfile(customer, accountType)
                            pciLog.Message = "Profile displayed successfully."
                        Case btnUpdateProfile.ID.ToString()
                            If ValidatePersonalData() Then
                                UpdateThisCustomerProfile(customer, accountType)
                                Dim secAdmin As New SecurityAdministrator
                                secAdmin.ModifyCustomerInfo(customer, errorstring)
                                pciLog.Message = "Customer Contact Information updated successfully."
                            End If
                    End Select
                End If
            Catch ex As Exception
                pciLog.IndicationSuccessFailure = "Failure"
                pciLog.Message = "Customer Contact Information Update Failure. " & ex.Message.ToString()
                'If (ex.Message.Contains("The operation has timed out")) Then
                MasterErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                MasterErrorLabel.Focus()
                'End If
                'handleError(ex)
            Finally
                pciLog.PushLogToMSMQ()
            End Try
        End Sub

        Private Sub DisplayThisCustomerProfile(ByRef customer As Customer, ByVal accountType As enumAccountType)
            If Not customer Is Nothing And accountType <> enumAccountType.Unknown Then '6668
                populateCustomerPersonalDataSection(customer)
                populateBillToData(customer, accountType)
            End If
        End Sub

        Private Sub UpdateThisCustomerProfile(ByRef customer As Customer, ByVal accountType As enumAccountType)
            Dim pciLog As New PCILogger()
            Dim m_SequenceNumber As Int32
            Dim oldUserIdSequenceNumber As String
            Dim debugMsg As String = $"Contact-Information.UpdateThisCustomerProfile - START at {DateTime.Now.ToShortTimeString()}{Environment.NewLine}"

            Try
                pciLog.IndicationSuccessFailure = "Failure"
                pciLog.EventOriginApplication = "ServicesPLUS"
                pciLog.EventOriginApplicationLocation = Me.Page.GetType().Name
                pciLog.EventOriginMethod = "updateThisCustomerProfile"
                pciLog.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
                pciLog.OperationalUser = Environment.UserName
                pciLog.EventType = EventType.Update
                pciLog.EventDateTime = DateTime.Now.ToLongTimeString()
                pciLog.HTTPRequestObjectValues = GetRequestContextValue()

                If customer IsNot Nothing Then
                    pciLog.CustomerID = customer.CustomerID
                    pciLog.CustomerID_SequenceNumber = customer.SequenceNumber
                    pciLog.EmailAddress = customer.EmailAddress '6524
                    pciLog.SIAM_ID = customer.SIAMIdentity
                    pciLog.LDAP_ID = customer.LdapID '6524

                    focusFormField = ""
                    MasterErrorLabel.Text = ""
                    oldUserIdSequenceNumber = customer.SequenceNumber
                    debugMsg &= $"- Calling UpdateTheCustomerObject{Environment.NewLine}"
                    If UpdateTheCustomerObject(customer, accountType) Then
                        m_SequenceNumber = updateSiam(customer)
                        debugMsg &= $"- UpdateSiam() called. Sequence Number: {m_SequenceNumber}.{Environment.NewLine}"
                        If m_SequenceNumber > 0 Then
                            customer.SequenceNumber = m_SequenceNumber
                            HttpContextManager.Customer = customer
                            If MasterErrorLabel.Text = String.Empty Then
                                MasterErrorLabel.Text = "Your profile has been sucessfully updated."
                                MasterErrorLabel.Focus()
                            End If
                            pciLog.IndicationSuccessFailure = "Success"
                            pciLog.CustomerID_SequenceNumber = customer.SequenceNumber
                            pciLog.Message = "Customer updated profile. Old sequence number " & oldUserIdSequenceNumber & " is now " & customer.SequenceNumber
                        Else
                            debugMsg &= $"- Inner Else block hit.{Environment.NewLine}"
                            MasterErrorLabel.Text = "Unable to update profile. " + MasterErrorLabel.Text
                            MasterErrorLabel.Focus()
                            pciLog.Message = MasterErrorLabel.Text
                        End If
                    Else
                        debugMsg &= $"- Outer Else block hit.{Environment.NewLine}"
                        MasterErrorLabel.Text = "Unable to update profile. " + MasterErrorLabel.Text
                        MasterErrorLabel.Focus()
                        pciLog.Message = MasterErrorLabel.Text
                    End If
                Else
                    pciLog.Message = "Unable to fetch Customer Information"
                End If
            Catch ex As Exception
                MasterErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                MasterErrorLabel.Focus()
                pciLog.Message = "Unable to update profile. " + ex.Message.ToString()
            Finally
                Utilities.LogDebug(debugMsg)
                pciLog.PushLogToMSMQ()
            End Try
        End Sub

        Private Function UpdateTheCustomerObject(ByRef customer As Customer, ByVal accountType As enumAccountType) As Boolean
            Dim returnValue As Boolean = False
            If customer IsNot Nothing Then
                returnValue = updateCustomerData(customer)
            End If
            Return returnValue
        End Function

        Private Function GenerateSiamIDImageString(ByVal strSiamIdentity As String) As String

            Dim strImg As String = "<img src='images/ImgName.JPG' style='border:0;' alt='' />"
            Dim strValue As String = ""

            Try
                For Each strChar In strSiamIdentity.Replace(" ", "").ToCharArray()
                    strValue += strImg.Replace("ImgName", strChar.ToString())
                Next
            Catch ex As Exception
                Return strValue
            End Try

            Return strValue

        End Function

#End Region


#Region "Form validation section"

        Private Sub InitZipControls()
            Dim intLength As Int32
            Dim strValidate As String = String.Empty

            If HttpContextManager.GlobalData.SalesOrganization = ConfigurationData.GeneralSettings.GlobalData.SalesOrganization.US Then
                lblZipCode.InnerText = "Zip Code :"
                intLength = Convert.ToInt32(ConfigurationData.GeneralSettings.GlobalData.ZipCode.US)
                strValidate = "\d{" + intLength.ToString() + "}(-\d{4})?$"
            Else
                lblZipCode.InnerText = "Postal Code :"
                intLength = Convert.ToInt32(ConfigurationData.GeneralSettings.GlobalData.ZipCode.CA)
                strValidate = "^[ABCEGHJKLMNPRSTVXY]{1}\d{1}[A-Z]{1} *\d{1}[A-Z]{1}\d{1}$"
            End If
            txtZipCode.Text = ""
            txtZipCode.MaxLength = intLength

            'Upper case - Zip code 
            txtZipCode.Attributes.Add("onblur", "this.value = this.value.toUpperCase()")
        End Sub

        Private Function ValidatePersonalData() As Boolean
            Dim thisPattern As String
            Dim isSectionValid As Boolean = True

            MasterErrorLabel.Text = ""
            lblAddress1Error.Text = ""
            lblAddress2Error.Text = ""
            lblCityError.Text = ""

            ' -- first name
            If String.IsNullOrWhiteSpace(txtFirstName.Text) Then
                isSectionValid = False
                If String.IsNullOrWhiteSpace(focusFormField) Then focusFormField = txtFirstName.ID
                lblFirstName.Style.Item("color") = conColorTextError
            Else
                lblFirstName.Style.Item("color") = conColorTextRegular
            End If

            ' -- last name
            If String.IsNullOrWhiteSpace(txtLastName.Text) Then
                isSectionValid = False
                If String.IsNullOrWhiteSpace(focusFormField) Then focusFormField = txtLastName.ID
                lblLastName.Style.Item("color") = conColorTextError
            Else
                lblLastName.Style.Item("color") = conColorTextRegular
            End If

            '-- email
            'thisPattern = "\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
            'If Not Regex.IsMatch(EmailAddress.Text, thisPattern) Then
            If New SecurityAdministrator().IsValidEmail(txtEmailAddress.Text) = False Then
                isSectionValid = False
                If String.IsNullOrWhiteSpace(focusFormField) Then focusFormField = txtEmailAddress.ID
                lblEmailAddress.Style.Item("color") = conColorTextError
                MasterErrorLabel.Text = Resources.Resource.el_InvalidEmail '"You have entered an invalid email address."
                MasterErrorLabel.Focus()
            Else
                lblEmailAddress.Style.Item("color") = conColorTextRegular
            End If

            ' -- company name
            If String.IsNullOrWhiteSpace(txtCompanyName.Text) Then
                isSectionValid = False
                If String.IsNullOrWhiteSpace(focusFormField) Then focusFormField = txtCompanyName.ID
                lblCompanyName.Style.Item("color") = conColorTextError
            Else
                lblCompanyName.Style.Item("color") = conColorTextRegular
            End If

            '-- shipping address 1
            If String.IsNullOrWhiteSpace(txtAddress1.Text) Then
                isSectionValid = False
                If String.IsNullOrWhiteSpace(focusFormField) Then focusFormField = txtAddress1.ID
                lblAddress1.Style.Item("color") = conColorTextError
            ElseIf txtAddress1.Text.Length > 35 Then
                isSectionValid = False
                If String.IsNullOrWhiteSpace(focusFormField) Then focusFormField = txtAddress1.ID
                lblAddress1.Style.Item("color") = conColorTextError
                lblAddress1Error.Text = "<span class=""redAsterick"">&nbsp;&nbsp;&nbsp;" + Resources.Resource.el_ValidLength_StreetAddress + "</span>"
                lblAddress1Error.Focus()
            Else
                lblAddress1.Style.Item("color") = conColorTextRegular
            End If

            '-- shipping address 2
            If txtAddress2.Text.Length > 35 Then
                isSectionValid = False
                If String.IsNullOrWhiteSpace(focusFormField) Then focusFormField = txtAddress2.ID
                lblAddress2.Style.Item("color") = conColorTextError
                lblAddress2Error.Text = "<span class=""redAsterick"">&nbsp;&nbsp;&nbsp;" + Resources.Resource.el_ValidLength_Address2 + "</span>"
                lblAddress2Error.Focus()
            Else
                lblAddress2.Style.Item("color") = conColorTextRegular
            End If

            'If ShippingAddress3.Text.Length > 35 Then
            '    isSectionValid = False
            '    If string.IsNullOrWhiteSpace(focusFormField) Then focusFormField = ShippingAddress3.ID
            '    ShippingAddressLabel3.ForeColor = Color.Red
            '    LabelLine3Star.Text = "<span class=""redAsterick"">&nbsp;&nbsp;&nbsp;Address 3rd Line must be 35 characters or less in length.</span>"
            'Else
            '    ShippingAddressLabel3.Style.Item("color") = conColorTextRegular
            'End If

            '-- City
            If String.IsNullOrWhiteSpace(txtCity.Text) Then
                isSectionValid = False
                If String.IsNullOrWhiteSpace(focusFormField) Then focusFormField = txtCity.ID
                lblCity.Style.Item("color") = conColorTextError
                lblCityError.Text = "<span class=""redAsterick"">*</span>"
            ElseIf txtCity.Text.Length > 35 Then
                isSectionValid = False
                If String.IsNullOrWhiteSpace(focusFormField) Then focusFormField = txtCity.ID
                lblCity.Style.Item("color") = conColorTextError
                lblCityError.Text = "<span class=""redAsterick"">&nbsp;&nbsp;&nbsp;" + Resources.Resource.el_ValidLength_City + "</span>"
                lblCityError.Focus()
            Else
                lblCity.Style.Item("color") = conColorTextRegular
            End If

            '-- State
            If ddlStates.SelectedValue.Length = 0 Then
                isSectionValid = False
                If String.IsNullOrWhiteSpace(focusFormField) Then focusFormField = ddlStates.ID
                lblState.Style.Item("color") = conColorTextError
            Else
                lblState.Style.Item("color") = conColorTextRegular
            End If

            ' -- Zip
            thisPattern = "\d{5}|\d{5}(-\d{4})?"
            Dim intLength As Integer
            If HttpContextManager.GlobalData.SalesOrganization = ConfigurationData.GeneralSettings.GlobalData.SalesOrganization.US Then
                intLength = Convert.ToInt32(ConfigurationData.GeneralSettings.GlobalData.ZipCode.US)
                thisPattern = "\d{" + intLength.ToString() + "}(-\d{4})?$"
            Else
                intLength = Convert.ToInt32(ConfigurationData.GeneralSettings.GlobalData.ZipCode.CA)
                thisPattern = "^[ABCEGHJKLMNPRSTVXY]{1}\d{1}[A-Z]{1} *\d{1}[A-Z]{1}\d{1}$"  ' Postal Code's first letter cannot be D,F,I,O,Q,U,Z
            End If

            If Not Regex.IsMatch(txtZipCode.Text, thisPattern) Then
                isSectionValid = False
                If String.IsNullOrWhiteSpace(focusFormField) Then focusFormField = txtZipCode.ID
                lblZipCode.Style.Item("color") = conColorTextError
            Else
                lblZipCode.Style.Item("color") = conColorTextRegular
            End If

            ' -- phone number
            thisPattern = "((\(\d{3}\) ?)|(\d{3}-))?\d{3}-\d{4}"
            If Not Regex.IsMatch(txtPhone.Text.ToString(), thisPattern) Then
                isSectionValid = False
                If String.IsNullOrWhiteSpace(focusFormField) Then focusFormField = txtPhone.ID
                lblPhone.Style.Item("color") = conColorTextError
            Else
                lblPhone.Style.Item("color") = conColorTextRegular
            End If
            ' -- extension
            If txtExtension.Text.Trim.ToString.Length > 0 Then
                thisPattern = "^[0-9]*$"
                If Not Regex.IsMatch(txtExtension.Text.Trim().ToString(), thisPattern) Or txtExtension.Text.Trim.Length > 6 Then
                    isSectionValid = False
                    If String.IsNullOrWhiteSpace(focusFormField) Then focusFormField = txtExtension.ID
                    lblExtension.Style.Item("color") = conColorTextError
                Else
                    lblExtension.Style.Item("color") = conColorTextRegular
                End If
            End If
            ' -- fax
            If txtFax.Text.Trim.ToString.Length > 0 Then
                thisPattern = "((\(\d{3}\) ?)|(\d{3}-))?\d{3}-\d{4}"
                If Not Regex.IsMatch(txtFax.Text.ToString(), thisPattern) Then
                    isSectionValid = False
                    If String.IsNullOrWhiteSpace(focusFormField) Then focusFormField = txtFax.ID
                    lblFax.Style.Item("color") = conColorTextError
                Else
                    lblFax.Style.Item("color") = conColorTextRegular
                End If
            End If

            Return isSectionValid
        End Function

#End Region

#Region "Populate customer Section"
        Private Sub populateCustomerPersonalDataSection(ByRef customer As Customer)
            Try
                If Not customer Is Nothing Then
                    If Not customer.FirstName Is Nothing Then txtFirstName.Text = customer.FirstName.ToString()
                    If Not customer.LastName Is Nothing Then txtLastName.Text = customer.LastName.ToString()
                    If Not customer.EmailAddress Is Nothing Then txtEmailAddress.Text = customer.EmailAddress.ToString()
                    If Not customer.CompanyName Is Nothing Then txtCompanyName.Text = customer.CompanyName.ToString()
                    If Not customer.Address.Line1 Is Nothing Then txtAddress1.Text = customer.Address.Line1.ToString()
                    If Not customer.Address.Line2 Is Nothing Then txtAddress2.Text = customer.Address.Line2.ToString()
                    'If Not customer.Address.Line3 Is Nothing Then ShippingAddress3.Text = customer.Address.Line3.ToString()'7901
                    If Not customer.Address.City Is Nothing Then txtCity.Text = customer.Address.City.ToString()
                    If Not customer.Address.State Is Nothing Then selectedState(customer.Address.State.ToString())
                    If Not customer.Address.PostalCode Is Nothing Then txtZipCode.Text = customer.Address.PostalCode.ToString()
                    If Not customer.PhoneNumber Is Nothing Then txtPhone.Text = FormatPhoneNumber(customer.PhoneNumber.ToString())
                    If Not customer.PhoneExtension Is Nothing And Not txtExtension Is Nothing Then txtExtension.Text = customer.PhoneExtension.Trim().ToString()
                    If Not customer.FaxNumber Is Nothing Then txtFax.Text = FormatPhoneNumber(customer.FaxNumber.ToString())
                    If Not customer.CustomerID Is Nothing Then lblCustomerID.Text = GenerateSiamIDImageString(customer.SIAMIdentity.ToString())

                    ' display the Email Ok from stored customer information
                    ' Added by Deepa V, Apr 27, 2006
                    'If customer.EmailOk = True Then
                    '    Me.okToContact.SelectedIndex = 0
                    'Else
                    '    Me.okToContact.SelectedIndex = 1
                    'End If
                End If
            Catch ex As Exception
                MasterErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                MasterErrorLabel.Focus()
            End Try
        End Sub
#End Region

#Region "populate bill to data"

        Private Sub populateBillToData(ByRef customer As Customer, ByVal accountType As enumAccountType)
            Try
                '***************Changes done for Bug# 1212 Starts here***************
                '**********Kannapiran S****************
                '
                'Dim sis As New 'SISHelper(objSISParameter)
                '***************Changes done for Bug# 1212 Ends here***************

                If Not customer Is Nothing And accountType = enumAccountType.AccountHolder Then
                    lblBillTo.Visible = True
                    lblBillToData.Visible = True
                    lblBillToCSNumber.Visible = True
                    BillTo.Visible = True
                    Dim tempString As New StringBuilder
                    'Dim account As Account
                    Dim x As Integer = 0

                    'For Each account In customer.SAPBillToAccounts
                    '    If account.Validated Then
                    '        sis.SAPPayorInquiry(account, True)
                    '    End If

                    '    If Not account.AccountNumber Is Nothing Then tempString.Append(account.AccountNumber.ToString() + " -")
                    '    If Not account.Address.Name Is Nothing Then tempString.Append(" " + account.Address.Name.ToString())
                    '    If Not account.Address.Line1 Is Nothing Then tempString.Append(", " + account.Address.Line1.ToString())
                    '    If Not account.Address.Line2 Is Nothing Then tempString.Append(", " + account.Address.Line2.ToString())
                    '    If Not account.Address.Line3 Is Nothing Then tempString.Append(", " + account.Address.Line3.ToString())
                    '    tempString.Append("<br/>")
                    'Next

                    'For Each account In customer.SISLegacyBillToAccounts
                    '    If account.Validated Then
                    '        sis.LegacyAccountInquiry(account)
                    '    End If

                    '    If Not account.AccountNumber Is Nothing Then tempString.Append(account.AccountNumber.ToString() + " -")
                    '    If Not account.Address.Name Is Nothing Then tempString.Append(" " + account.Address.Name.ToString())
                    '    If Not account.Address.Line1 Is Nothing Then tempString.Append(", " + account.Address.Line1.ToString())
                    '    If Not account.Address.Line2 Is Nothing Then tempString.Append(", " + account.Address.Line2.ToString())
                    '    If Not account.Address.Line3 Is Nothing Then tempString.Append(", " + account.Address.Line3.ToString())
                    '    tempString.Append("<br/>")
                    'Next

                    'If customer.StatusID = 3 Then'6668
                    If customer.UserType = "P" Then '6668
                        For Each objLoopAccount As Account In customer.SAPBillToAccounts
                            'If objLoopAccount.Validated Then
                            If Not objLoopAccount.AccountNumber Is Nothing Then tempString.Append(objLoopAccount.AccountNumber.ToString())
                            tempString.Append("<br/>")
                            'End If

                        Next
                    Else
                        For Each objLoopAccount As Account In customer.SAPBillToAccounts
                            'If objLoopAccount.Validated Then
                            ''changed for bug 6219
                            objLoopAccount = cm.PopulateCustomerDetailWithSAPAccount(objLoopAccount)

                            If Not objLoopAccount.AccountNumber Is Nothing Then tempString.Append(objLoopAccount.AccountNumber.ToString() + "-")
                            If Not objLoopAccount.Address Is Nothing Then
                                If Not objLoopAccount.Address.Name Is Nothing Then tempString.Append(" " + objLoopAccount.Address.Name.ToString())
                                If Not objLoopAccount.Address.Line1 Is Nothing Then tempString.Append(" " + objLoopAccount.Address.Line1.ToString())
                                If Not objLoopAccount.Address.Line2 Is Nothing Then tempString.Append(" " + objLoopAccount.Address.Line2.ToString())
                                If Not objLoopAccount.Address.Line3 Is Nothing Then tempString.Append(" " + objLoopAccount.Address.Line3.ToString())
                            End If
                            tempString.Append("<br/>")
                            'End If

                        Next
                    End If

                    lblBillToData.Text = tempString.ToString()
                Else
                    lblBillTo.Visible = False
                    lblBillToData.Visible = False
                    lblBillToCSNumber.Visible = False
                    BillTo.Visible = False
                    lblBillToData.Text = ""
                    lblBillTo.Text = ""
                End If
            Catch ex As Exception
                MasterErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                MasterErrorLabel.Focus()
            End Try
        End Sub

#End Region

#Region "Selected dropdown list item"
        Private Sub selectedState(ByVal state As String)
            If Not IsPostBack Then
                PopulateStateDropdownList(ddlStates)
                ddlStates.Items.FindByValue(state).Selected = True
            End If
        End Sub

#End Region

#Region "Customer data"
        Private Function updateCustomerData(ByRef thisCustomer As Customer) As Boolean
            Dim returnValue As Boolean = False
            Try
                If thisCustomer IsNot Nothing Then
                    '-- first name
                    If thisCustomer.FirstName IsNot Nothing Then
                        If thisCustomer.FirstName <> txtFirstName.Text.Trim() Then thisCustomer.FirstName = txtFirstName.Text.Trim()
                    End If

                    '-- last name
                    If thisCustomer.LastName IsNot Nothing Then
                        If thisCustomer.LastName <> txtLastName.Text.Trim() Then thisCustomer.LastName = txtLastName.Text.Trim()
                    End If

                    '-- email address
                    If thisCustomer.EmailAddress IsNot Nothing Then
                        If thisCustomer.EmailAddress <> txtEmailAddress.Text.Trim() Then thisCustomer.EmailAddress = txtEmailAddress.Text.Trim()
                    End If

                    '-- company name
                    If thisCustomer.CompanyName IsNot Nothing Then
                        If thisCustomer.CompanyName <> txtCompanyName.Text.Trim() Then thisCustomer.CompanyName = txtCompanyName.Text.Trim()
                    End If

                    '-- shipping address 1
                    If thisCustomer.Address.Line1 IsNot Nothing Then
                        If thisCustomer.Address.Line1 <> txtAddress1.Text.Trim() Then thisCustomer.Address.Line1 = txtAddress1.Text.Trim()
                    End If

                    '-- shipping address 2
                    If thisCustomer.Address.Line2 IsNot Nothing Then
                        If thisCustomer.Address.Line2 <> txtAddress2.Text.Trim() Then thisCustomer.Address.Line2 = txtAddress2.Text.Trim()
                    Else
                        thisCustomer.Address.Line2 = txtAddress2.Text.Trim()
                    End If
                    '7901 starts
                    ''-- shipping address 3
                    'If thisCustomer.Address.Line3 IsNot Nothing Then
                    '    If thisCustomer.Address.Line3 <> ShippingAddress3.Text.Trim() Then thisCustomer.Address.Line3 = ShippingAddress3.Text.Trim()
                    'Else
                    '    thisCustomer.Address.Line3 = ShippingAddress3.Text.Trim()
                    'End If
                    '7901 ends
                    '-- city
                    If thisCustomer.Address.City IsNot Nothing Then
                        If thisCustomer.Address.City <> txtCity.Text.Trim() Then thisCustomer.Address.City = txtCity.Text.Trim()
                    End If

                    '-- state
                    If thisCustomer.Address.State IsNot Nothing Then
                        If thisCustomer.Address.State <> ddlStates.SelectedValue Then thisCustomer.Address.State = ddlStates.SelectedValue
                    End If

                    '-- zip code
                    If thisCustomer.Address.PostalCode IsNot Nothing Then
                        If thisCustomer.Address.PostalCode <> txtZipCode.Text.Trim() Then thisCustomer.Address.PostalCode = txtZipCode.Text.Trim()
                    End If

                    '-- phone number
                    If thisCustomer.PhoneNumber IsNot Nothing Then
                        If thisCustomer.PhoneNumber <> txtPhone.Text.Trim() Then thisCustomer.PhoneNumber = txtPhone.Text.Trim()
                    End If

                    '-- phone extentsion
                    If thisCustomer.PhoneExtension IsNot Nothing Then
                        If thisCustomer.PhoneExtension <> txtExtension.Text.Trim() Then thisCustomer.PhoneExtension = txtExtension.Text.Trim()
                    Else
                        thisCustomer.PhoneExtension = txtExtension.Text.Trim()
                    End If

                    '-- fax number
                    If thisCustomer.FaxNumber IsNot Nothing Then
                        If thisCustomer.FaxNumber <> txtFax.Text.Trim() Then thisCustomer.FaxNumber = txtFax.Text.Trim()
                    Else
                        thisCustomer.FaxNumber = txtFax.Text.Trim()
                    End If

                    '-- update the selected ok to email option
                    '-- Email ok
                    'If Me.okToContact.SelectedIndex = 0 Then
                    '    thisCustomer.EmailOk = True
                    'Else
                    '    thisCustomer.EmailOk = False
                    'End If

                    returnValue = True
                End If
            Catch ex As Exception
                handleError(ex)
            End Try

            Return returnValue
        End Function
#End Region

#Region "Update Siam"

        Private Function updateSiam(ByRef customer As Customer) As Int32
            Try
                Dim thisSA As New SecurityAdministrator
                Dim customerManager As New CustomerManager
                Dim thisUser As User = thisSA.GetUser(customer.SIAMIdentity).TheUser
                Dim oResAddUser As UpdateUserResponse

                thisUser.FirstName = customer.FirstName
                thisUser.LastName = customer.LastName
                thisUser.EmailAddress = customer.EmailAddress
                thisUser.CompanyName = customer.CompanyName
                thisUser.AddressLine1 = customer.Address.Line1
                thisUser.AddressLine2 = customer.Address.Line2
                thisUser.AddressLine3 = customer.Address.Line3
                thisUser.City = customer.Address.City
                thisUser.State = customer.Address.State
                thisUser.Zip = customer.Address.PostalCode
                thisUser.Phone = customer.PhoneNumber
                thisUser.Fax = customer.FaxNumber
                thisUser.Password = "-1-1-1-1"
                thisUser.CustomerID = customer.CustomerID
                thisUser.SequenceNumber = customer.SequenceNumber
                thisUser.EmailOK = IIf(customer.EmailOk, 1, 0)
                thisUser.PhoneExtension = customer.PhoneExtension
                thisUser.SubscriptionID = customer.SubscriptionID
                thisUser.Country = customer.CountryCode

                If (New SecurityAdministrator().IsDuplicateUser(thisUser)) Then
                    oResAddUser = thisSA.UpdateUser(thisUser)
                    Utilities.LogDebug($"Contact-Information.updateSiam - Siam Response: {oResAddUser.SiamResponseMessage}")
                    If (oResAddUser.SiamResponseMessage = Messages.UPDATE_USER_COMPLETED.ToString()) Then
                        Return thisUser.SequenceNumber
                    Else
                        Return 0
                    End If
                Else


                End If
                customerManager.UpdateCustomerSubscription(customer)
                Return customer.SequenceNumber
            Catch ex As Exception
                MasterErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                MasterErrorLabel.Focus()
                Return 0
            End Try
        End Function

#End Region

#Region "helpers"
        Private Sub setFocusField(ByVal callingControl As String)
            '-- if the form validation has already set the focus field then don't do anything. 
            If String.IsNullOrWhiteSpace(focusFormField) Then
                focusFormField = conDefaultFormFieldFocus
                'Select Case callingControl
                '    Case Page.ID.ToString()
                '        focusFormField = conDefaultFormFieldFocus
                '    Case btnUpdateProfile.ID.ToString()
                '        focusFormField = conDefaultFormFieldFocus
                '    Case Else
                '        focusFormField = conDefaultFormFieldFocus
                'End Select
            End If
        End Sub
        Private Function FormatPhoneNumber(ByVal thisNumber As String) As String
            Dim returnValue As String = String.Empty
            Try
                If thisNumber.Length = 10 Then
                    Dim thisSB As New StringBuilder

                    thisSB.Append(thisNumber.Substring(1, 3).ToString() + "-")
                    thisSB.Append(thisNumber.Substring(4, 3).ToString() + "-")
                    thisSB.Append(thisNumber.Substring(8, 4).ToString())
                    returnValue = thisSB.ToString()
                Else
                    returnValue = thisNumber
                End If
            Catch ex As Exception
                returnValue = ""
            End Try
            Return returnValue
        End Function

        Private Function RemovePhoneFormat(ByVal thisNumber As String) As String
            Dim returnValue As String = String.Empty
            Try
                returnValue = thisNumber.Replace("-", "").ToString()
            Catch ex As Exception
                returnValue = ""
            End Try
            Return returnValue
        End Function

#End Region

#End Region

        Private Sub FormFieldInitialization()
            txtFirstName.Attributes("onkeydown") = "SetTheFocusButton(event, 'UpdateProfile')"
            txtLastName.Attributes("onkeydown") = "SetTheFocusButton(event, 'UpdateProfile')"
            txtEmailAddress.Attributes("onkeydown") = "SetTheFocusButton(event, 'UpdateProfile')"
            txtCompanyName.Attributes("onkeydown") = "SetTheFocusButton(event, 'UpdateProfile')"
            txtAddress1.Attributes("onkeydown") = "SetTheFocusButton(event, 'UpdateProfile')"
            txtAddress2.Attributes("onkeydown") = "SetTheFocusButton(event, 'UpdateProfile')"
            'ShippingAddress3.Attributes("onkeydown") = "SetTheFocusButton(event, 'UpdateProfile')"'7901
            txtCity.Attributes("onkeydown") = "SetTheFocusButton(event, 'UpdateProfile')"
            txtZipCode.Attributes("onkeydown") = "SetTheFocusButton(event, 'UpdateProfile')"
            txtPhone.Attributes("onkeydown") = "SetTheFocusButton(event, 'UpdateProfile')"
            txtExtension.Attributes("onkeydown") = "SetTheFocusButton(event, 'UpdateProfile')"
            txtFax.Attributes("onkeydown") = "SetTheFocusButton(event, 'UpdateProfile')"
        End Sub

    End Class

End Namespace
