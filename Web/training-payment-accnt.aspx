<%@ Reference Page="~/SSL.aspx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="SonyFooter" Src="SonyFooter.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="PersonalMessage" Src="PersonalMessage.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="ServicePlusNav" Src="ServicePlusNav.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="TopSonyHeader" Src="TopSonyHeader.ascx" %>

<%@ Page Language="vb" SmartNavigation="true" AutoEventWireup="false" Inherits="ServicePLUSWebApp.training_payment_accnt" CodeFile="training-payment-accnt.aspx.vb" CodeFileBaseClass="ServicePLUSWebApp.SSL" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="<%=PageLanguage %>">
<head>
    <title>Sony Training Institute � Class Payment</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
    <meta name="vs_defaultClientScript" content="JavaScript">
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
    <link href="includes/ServicesPLUS_style.css" type="text/css" rel="stylesheet">

    <script src="includes/jquery-1.10.2.min.js" type="text/javascript"></script>
    <script src="includes/ServicesPLUS.js" type="text/javascript"></script>
    <script src="includes/svcplus_globalization.js" type="text/javascript"></script>

    <script type="text/javascript">
        function poNotRequired() {
            if (document.getElementById('noPoRequiredCheckBox').checked === true) {
                document.getElementById("TextBox4").value = "NO-PO-REQ";
            }
            else {
                document.getElementById("TextBox4").value = "";
            }
        }

        function showHideCC() {
            var rdolist = document.getElementById("rbBillTo_1");
            if (rdolist.checked) {
                document.getElementById("trCCName").style.display = 'inline';
                document.getElementById("trCCBilling").style.display = 'inline';
                document.getElementById("trCCAddress1").style.display = 'inline';
                document.getElementById("trCCAddress2").style.display = 'inline';
                document.getElementById("trCCAddress3").style.display = 'inline';
                document.getElementById("trCCCity").style.display = 'inline';
                document.getElementById("trCCState").style.display = 'inline';
                document.getElementById("trCCZip").style.display = 'inline';
            }
            else {
                document.getElementById("trCCName").style.display = 'none';
                document.getElementById("trCCBilling").style.display = 'none';
                document.getElementById("trCCAddress1").style.display = 'none';
                document.getElementById("trCCAddress2").style.display = 'none';
                document.getElementById("trCCAddress3").style.display = 'none';
                document.getElementById("trCCCity").style.display = 'none';
                document.getElementById("trCCState").style.display = 'none';
                document.getElementById("trCCZip").style.display = 'none';
            }
        }
    </script>
</head>
<body style="color: #000000; background: #5d7180; margin: 0px;">
    <center>
        <form id="frm4" method="post" runat="server">
            <table width="760" border="0" role="presentation">
                <tr>
                    <td width="25" style="background: url('images/sp_left_bkgd.gif');">
                        <img src="images/spacer.gif" width="25" height="25" alt="">
                    </td>
                    <td width="710" bgcolor="#ffffff">
                        <table width="710" border="0" role="presentation">
                            <tr>
                                <td>
                                    <table width="710" border="0" role="presentation">
                                        <tr>
                                            <td width="391">
                                                <ServicePLUSWebApp:TopSonyHeader ID="TopSonyHeader" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <nav><ServicePLUSWebApp:ServicePlusNav ID="SonyNav" runat="server" /></nav>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table width="710" border="0" role="presentation">
                                                    <tr>
                                                        <%--<td width="464" bgColor="#363d45"><IMG height="40" src="images/sp_int_header_top_ServicesPLUS_short.gif" width="464"></td>
                                                        --%>
                                                        <td width="464" bgcolor="#363d45" height="1" background="images/sp_int_header_top_ServicesPLUS_onepix.gif"
                                                            align="right" valign="top">
                                                            <br>
                                                            <h1 class="headerText">ServicesPLUS&nbsp;&nbsp;</h1>
                                                        </td>
                                                        <td valign="top" bgcolor="#363d45">
                                                            <ServicePLUSWebApp:PersonalMessage ID="PersonalMessageDisplay" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td bgcolor="#f2f5f8">
                                                            <h2 class="headerTitle" style="padding-right: 20px; text-align: right;">Payment</h2>
                                                        </td>
                                                        <td bgcolor="#99a8b5">&nbsp;</td>
                                                    </tr>
                                                    <tr height="9">
                                                        <td bgcolor="#f2f5f8">
                                                            <img height="9" src="images/sp_int_header_btm_left_onepix.gif" width="464">
                                                        </td>
                                                        <td bgcolor="#99a8b5">
                                                            <img height="9" src="images/sp_int_header_btm_right.gif" width="246">
                                                        </td>
                                                    </tr>
                                                </table>
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ID="ErrorLabel" runat="server" CssClass="redAsterick"></asp:Label><asp:Label ID="ErrorValidation" runat="server" CssClass="redAsterick"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table width="710" border="0" role="presentation">
                                                    <tr>
                                                        <td width="20" height="20">
                                                            <img height="20" src="images/spacer.gif" width="20">
                                                        </td>
                                                        <td width="670" height="20">
                                                            <img height="20" src="images/spacer.gif" width="670">
                                                        </td>
                                                        <td width="20" height="20">
                                                            <img height="20" src="images/spacer.gif" width="20">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="20">
                                                            <img height="20" src="images/spacer.gif" width="20">
                                                        </td>
                                                        <td valign="top" width="670">
                                                            <div id="dMain" runat="server">
                                                                <table cellpadding="3" width="664" border="0" role="presentation">
                                                                    <tr>
                                                                        <td width="500" colspan="3">
                                                                            <span class="tableData"><strong>Please complete required fields
																					below marked with an asterisk.</strong></span>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="tableHeader" align="right" width="92">
                                                                            <asp:Label ID="LabelBillTo" runat="server">Bill to:</asp:Label>
                                                                        </td>
                                                                        <td width="572" colspan="2">
                                                                            <asp:DropDownList ID="ddlBillTo" runat="server" CssClass="tableData" AutoPostBack="True"></asp:DropDownList><asp:Label ID="LabelBillToStar" runat="server" CssClass="redAsterick">
																				<span class="redAsterick">*</span>
                                                                            </asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="tableData" valign="top" align="right" width="92" height="48" style="height: 48px">
                                                                            <asp:Label ID="LabelPoNumber" runat="server" CssClass="tableData">PO Number:</asp:Label>
                                                                        </td>
                                                                        <td width="572" height="48" style="height: 48px" valign="top" class="tableData">
                                                                            <SPS:SPSTextBox ID="TextBox4" runat="server" CssClass="tableData" MaxLength="20"></SPS:SPSTextBox><asp:Label ID="LabelPoNumberStar" runat="server" CssClass="redAsterick">
																				<span class="redAsterick">*</span>
                                                                            </asp:Label><br>
                                                                            <%--<asp:checkbox id="noPoRequiredCheckBox" runat="server" CssClass="tabledata" Text="Company does not require a purchase order."></asp:checkbox>--%>
                                                                            <input type="checkbox" id="noPoRequiredCheckBox" runat="server" class="tableData" onclick="javascript: poNotRequired();" />Company does not require a purchase order
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="right" width="84" style="height: 64px">
                                                                            <img height="1" src="images/spacer.gif" width="1">
                                                                        </td>
                                                                        <td colspan="2" style="height: 64px">
                                                                            <asp:RadioButtonList ID="rbBillTo" runat="server" CssClass="tableData" onclick="showHideCC();">
                                                                                <asp:ListItem Value="1" Selected="True">Bill to my Sony Account number</asp:ListItem>
                                                                                <asp:ListItem Value="2">Bill to my Credit Card</asp:ListItem>
                                                                                <asp:ListItem Value="3">Send me an Invoice to my Email Address</asp:ListItem>
                                                                            </asp:RadioButtonList>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                            <br>
                                                            <table id="tblBillAddress" cellpadding="3" border="0" style="width: 568px" role="presentation">

                                                                <tr id="trCCName" runat="server">
                                                                    <td class="tableData" align="right" width="92" height="25">
                                                                        <asp:Label ID="LabelCCName" runat="server" CssClass="tableData">Name as it appears on your credit card:</asp:Label>
                                                                    </td>
                                                                    <td width="572" height="25">
                                                                        <SPS:SPSTextBox ID="CCName" runat="server" CssClass="tableData" MaxLength="50"></SPS:SPSTextBox><asp:Label ID="LabelCCNameStar" runat="server" CssClass="redAsterick"></asp:Label>
                                                                    </td>
                                                                </tr>

                                                                <tr id="trCCBilling" runat="server">
                                                                    <td class="tableData" align="right" width="380" colspan="3">
                                                                        <asp:Label ID="LabelBillingAddress" runat="server" CssClass="tableData">Billing Address - Must match the address on your credit card statement. </asp:Label>
                                                                    </td>
                                                                </tr>

                                                                <tr id="trCCAddress1" runat="server">
                                                                    <td class="tableData" align="right" width="92">
                                                                        <asp:Label ID="LabelCCAddress1" runat="server" CssClass="tableData"> Street Address: </asp:Label>
                                                                    </td>
                                                                    <td width="562">
                                                                        <SPS:SPSTextBox ID="CCAddress1" runat="server" CssClass="tableData" MaxLength="50" Width="184px"></SPS:SPSTextBox><asp:Label ID="LabelCCAddress1Star" runat="server" CssClass="redAsterick"></asp:Label>
                                                                    </td>
                                                                </tr>

                                                                <tr id="trCCAddress2" runat="server">
                                                                    <td class="tableData" align="right" width="92">
                                                                        <asp:Label ID="LabelCCAddress2" runat="server" CssClass="tableData"> Address 2nd Line: </asp:Label>
                                                                    </td>
                                                                    <td width="562">
                                                                        <SPS:SPSTextBox ID="CCAddress2" runat="server" CssClass="tableData" MaxLength="50" Width="184px"></SPS:SPSTextBox>
                                                                        <asp:Label ID="LabelCCAddress2Star" runat="server" CssClass="redAsterick"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr id="trCCAddress3" runat="server">
                                                                    <td class="tableData" align="right" width="92">
                                                                        <asp:Label ID="LabelCCAddress3" runat="server" CssClass="tableData"> Address 3rd Line: </asp:Label>
                                                                    </td>
                                                                    <td width="562">
                                                                        <SPS:SPSTextBox ID="CCAddress3" runat="server" CssClass="tableData" MaxLength="50" Width="184px"></SPS:SPSTextBox>
                                                                        <asp:Label ID="LabelCCAddress3Star" runat="server" CssClass="redAsterick"></asp:Label>
                                                                    </td>
                                                                </tr>

                                                                <tr id="trCCCity" runat="server">
                                                                    <td class="tableData" align="right" width="92">
                                                                        &nbsp;&nbsp;&nbsp;
                                                                        <asp:Label ID="LabelCCCity" runat="server" CssClass="tableData">City:</asp:Label>
                                                                    </td>
                                                                    <td style="width: 580px">
                                                                        <SPS:SPSTextBox ID="CCCity" runat="server" CssClass="tableData" Width="160px"></SPS:SPSTextBox>
                                                                        <asp:Label ID="LabelCCCityStar" runat="server" CssClass="redAsterick"></asp:Label>
                                                                    </td>
                                                                </tr>

                                                                <tr id="trCCState" runat="server">
                                                                    <td class="tableData" align="right" width="92" height="29">
                                                                        &nbsp;
																			<asp:Label ID="LabelCCState" runat="server" CssClass="tableData">State:</asp:Label>
                                                                    </td>
                                                                    <td width="562" height="29">
                                                                        <asp:DropDownList ID="ddlCCState" runat="server" CssClass="tableData"></asp:DropDownList>
                                                                        <asp:Label ID="LabelCCStateStar" runat="server" CssClass="redAsterick"></asp:Label>
                                                                    </td>
                                                                </tr>

                                                                <tr id="trCCZip" runat="server">
                                                                    <td class="tableData" align="right" width="92" height="30">
                                                                        <asp:Label ID="LabelCCZip" runat="server" CssClass="tableData">Zip Code:</asp:Label>
                                                                    </td>
                                                                    <td width="562" height="30">
                                                                        <SPS:SPSTextBox ID="CCZip" runat="server" CssClass="tableData" MaxLength="10"></SPS:SPSTextBox>
                                                                        <asp:Label ID="LabelCCZipStar" runat="server" CssClass="redAsterick"></asp:Label>
                                                                    </td>
                                                                </tr>

                                                                <tr>
                                                                    <td class="tableData" align="right" style="width: 92px"></td>
                                                                    <td valign="top" colspan="1" style="width: 367px"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="right" style="height: 55px; width: 92px;"></td>
                                                                    <td style="height: 55px; width: 367px;">
                                                                        <table width="240" border="0" role="presentation">
                                                                            <tr>
                                                                                <td width="5"></td>
                                                                                <td>
                                                                                    <asp:ImageButton ID="btnNext" runat="server" AlternateText="Next" ImageUrl="images/sp_int_next_btn.gif"></asp:ImageButton>
                                                                                </td>
                                                                                <td>
                                                                                    <asp:ImageButton ID="btnCancel" runat="server" AlternateText="Cancel" ImageUrl="images/sp_int_cancel_btn.gif"></asp:ImageButton>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td width="20">
                                                            <img height="20" src="images/spacer.gif" width="20">
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>

                                            <td>
                                                <footer><ServicePLUSWebApp:SonyFooter ID="SonyFooter1" runat="server" /></footer>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="25" style="background: url('images/sp_right_bkgd.gif')">
                        <img src="images/spacer.gif" width="25" height="20" alt="">
                    </td>
                </tr>
            </table>
        </form>
    </center>
</body>
</html>