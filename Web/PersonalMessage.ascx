<%@ Control Language="vb" AutoEventWireup="false" Inherits="ServicePLUSWebApp.PersonalMessage" CodeFile="PersonalMessage.ascx.vb" %>
<link href="includes/ServicesPLUS_style.css" type="text/css" rel="stylesheet">

<div style="height: 100%; width: 100%; text-align: center; vertical-align: middle;">
    <a href="/ViewMyCatalog.aspx" class="memberMessage"
        aria-label="Go to the My Catalog page to view your reminders and more."><%=Resources.Resource.mem_hdr_msg & NumReminders & Resources.Resource.mem_hdr_msg_1 %></a>
</div>