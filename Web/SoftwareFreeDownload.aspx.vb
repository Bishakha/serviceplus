Imports System.IO
Imports Sony.US.ServicesPLUS.Process
Imports Sony.US.ServicesPLUS.Core
Imports System.Web.HttpUtility
Imports ServicesPlusException


Namespace ServicePLUSWebApp


    Partial Class SoftwareFreeDownload
        Inherits SSL


        Private downloadPath As String
        Private downloadURL As String
        'Private Const conSoftwareDownloadPath As String = "Software"
        'Private Const conReleaseNotesPath As String = "ReleaseNotes"

        'modified to read from config file by Deepa V, Mar 27,2006
        'file path
        Private conSoftwareDownloadPath As String
        Private conReleaseNotesPath As String

        'file size denominator
        Private Const MegaBytes As Integer = 1048576
        Private Const KiloBytes As Integer = 1024
        Private Const conFileNamePlaceHolder As String = "##FILENAME##"
        Private Const conContentType As String = "APPLICATION/OCTET-STREAM"
        Private Const conHeaderType As String = "Attachment; Filename=""" + conFileNamePlaceHolder + """"
        Private Const conAppendedHeaderType As String = "Content-Disposition"
        'Dim objUtilties As Utilities = New Utilities

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Protected Overrides Sub OnInit(ByVal e As EventArgs)
            MyBase.OnInit(e)
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.Cache.SetNoStore()
            Response.Cache.SetExpires(DateTime.Now)
            Response.Cache.SetLastModified(DateTime.Now)
        End Sub


        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
            setFileDownloadPaths()                                  '-- set the file download path variables 
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            'Put user code to initialize the page here
            If Not Me.IsPostBack Then
                Try
                    If Session("FreeDownloadSoftware") IsNot Nothing Then
                        Dim freeDownloadSoftware As Software = CType(Session("FreeDownloadSoftware"), Software)
                        FreeSoftwareDownloadTable.Controls.Add(buildDownloadFreeSoftwareRow(freeDownloadSoftware))

                        ' Redirect the user to Sign In or Register if they're trying to download a Tracked item
                        If (HttpContextManager.Customer Is Nothing) And (Not freeDownloadSoftware.FreeOfCharge Or freeDownloadSoftware.Tracking) Then
                            Response.Redirect("SignIn-Register.aspx?post=ssmfree", False)
                        End If
                    End If
                Catch ex As Exception
                    errorMessageLabel.Text = Utilities.WrapExceptionforUI(ex)
                End Try
            End If
        End Sub

        Private Function buildDownloadFreeSoftwareRow(ByVal software As Software) As TableRow
            Dim tr As New TableRow

            Try
                Dim td As New TableCell
                Dim productCategory As New Label With {.CssClass = "tableData", .Text = "&nbsp;"}
                Dim modelNumber As New Label With {.CssClass = "tableData", .Text = "&nbsp;"}
                Dim modelDescription As New Label With {.CssClass = "tableData", .Text = "&nbsp;"}
                Dim upgradeVersion As New Label With {.CssClass = "tableData", .Text = "&nbsp;"}
                Dim kitPartNumber As New Label With {.CssClass = "tableData", .Text = "&nbsp;"}
                Dim kitPartDescription As New Label With {.CssClass = "tableData", .Text = "&nbsp;"}
                Dim fileSize As New Label With {.CssClass = "tableData", .Text = ""}
                Dim releaseNotes As New Label With {.CssClass = "tableData", .Text = "&nbsp;"}

                If software.ProductCategory IsNot Nothing Then productCategory.Text = software.ProductCategory.ToString()
                If software.ModelNumber IsNot Nothing Then modelNumber.Text = software.ModelNumber.ToString()
                If software.ModelDescription IsNot Nothing Then modelDescription.Text = software.ModelDescription.ToString()
                If software.Version IsNot Nothing Then upgradeVersion.Text = software.Version.ToString()
                If software.KitPartNumber IsNot Nothing Then kitPartNumber.Text = software.KitPartNumber.ToString()
                If software.KitDescription IsNot Nothing Then kitPartDescription.Text = software.KitDescription.ToString()

                If (Not String.IsNullOrEmpty(software.DownloadFileName)) Then
                    fileSize.Text = getFileSize(downloadPath + "/" + conSoftwareDownloadPath + "/" + software.DownloadFileName)
                End If

                If Not software.ReleaseNotesFileName Is Nothing Then
                    releaseNotes.Text = buildReleaseNotesData(software.ReleaseNotesFileName.ToString())
                    If releaseNotes.Text.Length = 0 Then releaseNotes.Text = "&nbsp;"
                End If

                ' -- first search result row
                ' -- product category
                td.Controls.Add(New LiteralControl(vbCrLf + "<table cellpadding=""0"" cellspacing=""0"" border=""0"">" + vbCrLf + "<tr>" + vbCrLf))
                td.Controls.Add(New LiteralControl(vbTab + "<td width=""1"" bgColor=""#d5dee9""><img height=""20"" src=""images/spacer.gif"" width=""1"" alt=""""></td>" + vbCrLf))
                td.Controls.Add(New LiteralControl(vbTab + "<td class=""tableData"" width=""130"">&nbsp;&nbsp;"))
                td.Controls.Add(productCategory)
                td.Controls.Add(New LiteralControl("</td>" + vbCrLf))
                ' -- model number
                td.Controls.Add(New LiteralControl(vbTab + "<td width=""1"" bgColor=""#d5dee9""><img height=""1"" src=""images/spacer.gif"" width=""1"" alt=""""></td>" + vbCrLf))
                td.Controls.Add(New LiteralControl(vbTab + "<td class=""tableData"" width=""70"">&nbsp;&nbsp;"))
                td.Controls.Add(modelNumber)
                td.Controls.Add(New LiteralControl("</td>" + vbCrLf))
                ' -- model description
                td.Controls.Add(New LiteralControl(vbTab + "<td width=""1"" bgColor=""#d5dee9""><img height=""1"" src=""images/spacer.gif"" width=""1"" alt=""""></td>" + vbCrLf))
                td.Controls.Add(New LiteralControl(vbTab + "<td class=""tableData"" width=""145"">&nbsp;&nbsp;"))
                td.Controls.Add(modelDescription)
                td.Controls.Add(New LiteralControl("</td>" + vbCrLf))
                ' -- upgrade version 
                td.Controls.Add(New LiteralControl(vbTab + "<td width=""1"" bgColor=""#d5dee9""><img height=""1"" src=""images/spacer.gif"" width=""1"" alt=""""></td>" + vbCrLf))
                td.Controls.Add(New LiteralControl(vbTab + "<td class=""tableData"" align=""center"" width=""55"">"))
                td.Controls.Add(upgradeVersion)
                td.Controls.Add(New LiteralControl("</td>" + vbCrLf))
                ' -- kit part number
                td.Controls.Add(New LiteralControl(vbTab + "<td width=""1"" bgColor=""#d5dee9""><img height=""1"" src=""images/spacer.gif"" width=""1"" alt=""""></td>" + vbCrLf))
                td.Controls.Add(New LiteralControl(vbTab + "<td class=""tableData"" width=""70"">&nbsp;&nbsp;"))
                td.Controls.Add(kitPartNumber)
                td.Controls.Add(New LiteralControl("</td>" + vbCrLf))
                ' -- kit part description
                td.Controls.Add(New LiteralControl(vbTab + "<td width=""1"" bgColor=""#d5dee9""><img height=""1"" src=""images/spacer.gif"" width=""1"" alt=""""></td>" + vbCrLf))
                td.Controls.Add(New LiteralControl(vbTab + "<td class=""tableData"" width=""193"">&nbsp;&nbsp;"))
                td.Controls.Add(kitPartDescription)
                td.Controls.Add(New LiteralControl("</td>" + vbCrLf))
                ' -- spacer
                td.Controls.Add(New LiteralControl(vbTab + "<td width=""1"" bgColor=""#d5dee9""><img height=""20"" src=""images/spacer.gif"" width=""1"" alt=""""></td>" + vbCrLf))
                td.Controls.Add(New LiteralControl("</tr>" + vbCrLf))

                '-- second search result row
                td.Controls.Add(New LiteralControl("<tr>" + vbCrLf))
                td.Controls.Add(New LiteralControl(vbTab + "<td bgcolor=""D5DEE9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>" + vbCrLf))
                td.Controls.Add(New LiteralControl(vbTab + "<td colspan=""11"" bgcolor=""F2F5F8"">" + vbCrLf))
                td.Controls.Add(New LiteralControl(vbTab + vbTab + "<table width=""668"" border=""0"" cellspacing=""0"" cellpadding=""0"">" + vbCrLf))
                td.Controls.Add(New LiteralControl(vbTab + vbTab + vbTab + "<tr>" + vbCrLf))
                td.Controls.Add(New LiteralControl(vbTab + vbTab + vbTab + vbTab + "<td><img src=""images/spacer.gif"" width=""80"" height=""4"" alt=""""></td>" + vbCrLf))

                ' -- file size
                'td.Controls.Add(New LiteralControl(vbTab + vbTab + vbTab + vbTab + vbCrLf))

                Dim fileSizeText As String = "&nbsp;"
                If fileSize.Text.Length > 0 Then
                    fileSizeText = IIf(HttpContextManager.GlobalData.IsFrench, "Taille du Fichier:", "File Size:")
                End If
                'If fileSize.Text.Length > 0 Then fileSizeText = "File Size:"
                td.Controls.Add(New LiteralControl("<td class=""tableHeader"" width=""80"">" + fileSizeText + "</td>" + vbCrLf))
                td.Controls.Add(New LiteralControl("<td class=""tableData"" width=""80"">&nbsp;"))
                td.Controls.Add(fileSize)
                td.Controls.Add(New LiteralControl("</td>" + vbCrLf))

                ' -- release notes
                td.Controls.Add(New LiteralControl(vbTab + vbTab + vbTab + vbTab + "<td><img src=""images/spacer.gif"" width=""20"" height=""4"" alt=""""></td>" + vbCrLf))
                td.Controls.Add(New LiteralControl(vbTab + vbTab + vbTab + vbTab + "<td width=""20"">"))
                td.Controls.Add(releaseNotes)
                td.Controls.Add(New LiteralControl("</td>" + vbCrLf))
                ' -- spacers
                td.Controls.Add(New LiteralControl(vbTab + vbTab + vbTab + vbTab + "<td><img src=""images/spacer.gif"" width=""10"" height=""4"" alt=""""></td>" + vbCrLf))
                td.Controls.Add(New LiteralControl(vbTab + vbTab + vbTab + "</tr>" + vbCrLf))
                td.Controls.Add(New LiteralControl(vbTab + vbTab + vbTab + "<tr bgcolor=""D5DEE9"">" + vbCrLf))
                td.Controls.Add(New LiteralControl(vbTab + vbTab + vbTab + vbTab + "<td colspan=""18""><img src=""images/spacer.gif"" width=""1"" height=""3"" alt=""""></td>" + vbCrLf))
                td.Controls.Add(New LiteralControl(vbTab + vbTab + vbTab + "</tr>" + vbCrLf))
                td.Controls.Add(New LiteralControl(vbTab + vbTab + "</table>" + vbCrLf))
                td.Controls.Add(New LiteralControl(vbTab + "</td>" + vbCrLf))
                td.Controls.Add(New LiteralControl(vbTab + "<td bgcolor=""D5DEE9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>" + vbCrLf))
                td.Controls.Add(New LiteralControl("</tr>" + vbCrLf + "</table>" + vbCrLf))

                tr.Controls.Add(td)
            Catch ex As Exception
                errorMessageLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try

            Return tr
        End Function

        Private Sub setFileDownloadPaths()
            Try
                'Dim config_settings As Hashtable
                'Dim handler As New Sony.US.SIAMUtilities.ConfigHandler("SOFTWARE\SERVICESPLUS")
                'config_settings = handler.GetConfigSettings("//item[@name='Download']")
                'downloadURL = config_settings.Item("url").ToString()
                'downloadPath = config_settings.Item("physical").ToString()
                'conReleaseNotesPath = config_settings.Item("releasenotes").ToString()
                'conSoftwareDownloadPath = config_settings.Item("downloadfiles").ToString()

                downloadURL = Sony.US.SIAMUtilities.ConfigurationData.Environment.Download.Url
                downloadPath = Sony.US.SIAMUtilities.ConfigurationData.Environment.Download.Physical
                conReleaseNotesPath = Sony.US.SIAMUtilities.ConfigurationData.Environment.Download.ReleaseNotes
                conSoftwareDownloadPath = Sony.US.SIAMUtilities.ConfigurationData.Environment.Download.DownloadFiles
            Catch ex As Exception
                errorMessageLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Function getFileSize(ByVal path As String) As String
            Dim fileSize As String = ""

            Try
                If File.Exists(path) Then
                    Dim thisFile As New FileInfo(path.ToString())

                    If thisFile.Length() >= MegaBytes Then
                        fileSize = (String.Format("{0:N}", (thisFile.Length / MegaBytes))).ToString() + " MB"
                    ElseIf thisFile.Length() > KiloBytes Then
                        fileSize = (String.Format("{0:N}", (thisFile.Length / KiloBytes))).ToString() + " KB"
                    Else
                        fileSize = thisFile.Length.ToString() + " Bytes"
                    End If
                End If

            Catch ex As Exception
                fileSize = "N/A"
            End Try

            Return fileSize
        End Function

        Private Function buildReleaseNotesData(ByVal fileName As String) As String
            Dim returnValue As String = String.Empty
            If File.Exists(downloadPath + "/" + conReleaseNotesPath + "/" + fileName) Then
                returnValue = "<a href=""" + downloadURL + "/" + conReleaseNotesPath + "/" + fileName + """ target=""_blank""><img name=""releaseNotes"" src=""images/sp_int_releaseNotes_btn.gif"" border=""0"" height=""13"" width=""18"" alt=""Release Notes""/></a>"
            End If
            Return returnValue
        End Function

        Private Sub pushFileToClient(ByVal thisFileName As String)
            Try
                If thisFileName.Length > 0 Then
                    Response.ClearHeaders()
                    Dim headerInfo As String = conHeaderType.Replace(conFileNamePlaceHolder, thisFileName)
                    '-- set content type
                    Dim file_path As String
                    Response.ContentType = conContentType
                    Response.AppendHeader(conAppendedHeaderType, headerInfo)
                    file_path = downloadPath + "/" + Me.conSoftwareDownloadPath + "/" + thisFileName
                    If File.Exists(file_path) Then
                        file_path = Replace(file_path, "\", "/")
                        file_path = UrlDecode(UrlEncode(file_path))
                        Response.TransmitFile(file_path)
                    End If
                End If
            Catch ex As Exception
                handleError(ex)
            End Try
        End Sub

        Private Sub btnSearch_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
            Response.Redirect("sony-software.aspx", True)
        End Sub

        Private Sub btnDownload_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDownload.Click
            Try
                If Session("FreeDownloadSoftware") IsNot Nothing Then
                    Dim freeDownloadSoftware As Software = CType(Session("FreeDownloadSoftware"), Software)
                    pushFileToClient(freeDownloadSoftware.DownloadFileName)
                    If (freeDownloadSoftware.Tracking) Then LogDownLoad(freeDownloadSoftware)
                End If
            Catch ex As Exception
                errorMessageLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub
        Private Sub LogDownLoad(ByVal freeDownloadSoftware As Software)
            'Dim freeDownloadSoftware As Software = CType(Session("FreeDownloadSoftware"), Software)
            Dim om As OrderManager = New OrderManager
            Dim downloadRecord As DownloadRecord = New DownloadRecord
            If (freeDownloadSoftware IsNot Nothing) Then
                If HttpContextManager.Customer IsNot Nothing Then
                    Session.Item("Accept") = True
                    Dim customer = HttpContextManager.Customer
                    downloadRecord.CustomerID = customer.CustomerID
                    downloadRecord.CustomerSequenceNumber = customer.SequenceNumber
                    downloadRecord.SIAMID = customer.SIAMIdentity
                Else  ' This should only happen if the Session has expired between Page_Load and now.
                    Session.Item("Accept") = True
                    downloadRecord.CustomerID = -1
                    downloadRecord.CustomerSequenceNumber = -1
                    downloadRecord.SIAMID = "Anonymous"
                    downloadRecord.CustomerID = -1
                End If
                downloadRecord.KitPartNumber = freeDownloadSoftware.KitPartNumber
                downloadRecord.DeliveryMethod = "Download"
                downloadRecord.Time = Now
                downloadRecord.ModelID = freeDownloadSoftware.ModelNumber
                downloadRecord.ModelDescription = freeDownloadSoftware.ModelDescription
                downloadRecord.Version = freeDownloadSoftware.Version
                downloadRecord.FreeOfCharge = Convert.ToInt32(freeDownloadSoftware.FreeOfCharge)
                downloadRecord.KitDescription = freeDownloadSoftware.KitDescription
                downloadRecord.OrderNumber = ""
                downloadRecord.OrderLineItemSequence = -1

                downloadRecord.HTTP_X_Forward_For = HttpContextManager.GetServerVariableValue("HTTP_X_FORWARDED_FOR")
                downloadRecord.RemoteADDR = HttpContextManager.GetServerVariableValue("REMOTE_ADDR")
                downloadRecord.HTTPReferer = HttpContextManager.GetServerVariableValue("HTTP_REFERER")
                downloadRecord.HTTPURL = HttpContextManager.GetServerVariableValue("HTTP_URL")
                If String.IsNullOrEmpty(downloadRecord.HTTPURL) Then
                    downloadRecord.HTTPURL = HttpContextManager.GetServerVariableValue("URL") + ReturnQueryString()
                End If
                downloadRecord.HTTPUserAgent = HttpContextManager.GetServerVariableValue("HTTP_USER_AGENT")

                om.LogDownload(downloadRecord)
            End If
        End Sub
    End Class
End Namespace
