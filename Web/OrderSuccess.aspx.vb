Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Process
Imports Sony.US.ServicesPLUS.Data
Imports Sony.US.siam
Imports System.Xml
Imports System.Text
Imports System.IO
Imports System.Configuration
Imports Sony.US.SIAMUtilities
Imports System.Collections
Imports ServicesPlusException
Imports Sony.US.AuditLog

Namespace ServicePLUSWebApp


    Partial Class OrderSuccess
        Inherits SSL
        'Dim objUtilties As Utilities = New Utilities
#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub


        Protected WithEvents ContentLabel As System.Web.UI.WebControls.Label
        Protected WithEvents OrderNumber As System.Web.UI.WebControls.Label
        'Dim objGlobalData As New GlobalData


        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

#Region "Page Variables"
        'Private Const conSoftwareDownloadPath As String = "\downloads\software"
        'modified to read from config file by Deepa V, Mar 24 ,2006
        'file path
        Private conSoftwareDownloadPath As String
        Private haveDownloadableItems As Boolean = False
        Private anySpecialTax As Boolean = False
        Private Const conFileDownloadPage As String = "FileDownloadArea.aspx"
        Private order As Sony.US.ServicesPLUS.Core.Order
        Private SoftwareItemLinkText() As String
        Private softwareHeaderCreated As Boolean = False
        Private ssnEWCollection As NameValueCollection = New NameValueCollection()
        Private strWebSiteURL As String = String.Empty
        Private tableColumnCount As Integer

#End Region

#Region "Events"

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Dim objPCILogger As New PCILogger() '6524
            Dim customer = HttpContextManager.Customer
            Try
                'If Not Session.Item("GlobalData") Is Nothing Then
                '    objGlobalData = Session.Item("GlobalData")
                'End If

                '6524 start
                objPCILogger.EventOriginApplication = "ServicesPLUS"
                objPCILogger.EventOriginApplicationLocation = Page.GetType().Name
                objPCILogger.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
                objPCILogger.OperationalUser = Environment.UserName
                objPCILogger.EventDateTime = DateTime.Now.ToLongTimeString()
                objPCILogger.HTTPRequestObjectValues = objPCILogger.GetRequestContextValue()
                objPCILogger.IndicationSuccessFailure = "Success"
                objPCILogger.EventOriginMethod = "Page_Load"
                objPCILogger.Message = "Page_Load Success."
                objPCILogger.EventType = EventType.Update
                objPCILogger.CustomerID = customer.CustomerID
                objPCILogger.CustomerID_SequenceNumber = customer.SequenceNumber
                objPCILogger.EmailAddress = customer.EmailAddress
                objPCILogger.SIAM_ID = customer.SIAMIdentity
                objPCILogger.LDAP_ID = customer.LdapID
                '6524 end

                strWebSiteURL = ConfigurationData.Environment.WebSiteURL
                If ConfigurationData.GeneralSettings.Environment = "Development" Then
                    strWebSiteURL = HttpContext.Current.Request.Url.OriginalString.Substring(0, HttpContext.Current.Request.Url.OriginalString.IndexOf("/ServicesPLUSWebApp")) + "/ServicesPLUSWebApp/"
                End If
                Session.Remove("CertificateInSSN")

                'modified by Deepa V Mar 27,2006 to read the download path from config file
                If Not IsPostBack Then
                    setFileDownloadPaths()
                End If

                If Session.Item("order") IsNot Nothing Then
                    order = Session.Item("order")
                Else
                    'if session timeout then go to default page
                    Response.Redirect("default.aspx")
                End If

                LabelOrderNumber.Text = order.OrderNumber
                errorMessage.Text = ""

                Dim om As New OrderDataManager
                Dim strURL As String = om.GetSpecialTaxURL("USA", "CA", "*", "Recycling")

                specialTaxHyperLink.Attributes.Add("onclick", "javascript: window.open('" + strURL + "','','toolbar=1,location=1,directories=1,status=1,menubar=1,scrollbars=1,resize=1,width=900,height=720');")
                specialTaxHyperLink.Text = strURL
                specialTaxHyperLink.NavigateUrl = "#"

                TableSuccess.Rows.Clear()

                'order.LineItems(0).Downloadable

                Dim TRPart As New TableRow
                Dim TRSoftware As New TableRow
                Dim TRCertificate As New TableRow
                Dim TRExtendedWarranty As New TableRow
                Dim TROnlineTraining As New TableRow
                Dim TRTotal As New TableRow

                Dim sbInnerHTML As StringBuilder = New StringBuilder()
                Dim SBTotal As StringBuilder = New StringBuilder()
                Dim SBPart As StringBuilder = New StringBuilder()
                Dim SBSoftware As StringBuilder = New StringBuilder()
                Dim SBCertificate As StringBuilder = New StringBuilder()
                Dim SBExtendedWarranty As StringBuilder = New StringBuilder()
                Dim SBOnlineTraining As StringBuilder = New StringBuilder()

                Dim TDTotal As New TableCell
                Dim TDPart As New TableCell
                Dim TDSoftware As New TableCell
                Dim TDCertificate As New TableCell
                Dim TDExtendedWarranty As New TableCell
                Dim TDOnlineTraining As New TableCell

                Dim partHeaderCreated As Boolean = False
                Dim certificateHeaderCreated As Boolean = False
                Dim extendedWarrantyHeaderCreated As Boolean = False
                Dim onlineTrainingHeaderCreated As Boolean = False

                Dim objEWFileNameCollection As New NameValueCollection()
                lblOrderInProcess.Visible = False

                For Each iOrderLine As OrderLineItem In order.LineItems1
                    If iOrderLine.Downloadable Then
                        Select Case iOrderLine.Product.Type
                            Case Product.ProductType.Software, Product.ProductType.Certificate, Product.ProductType.ClassRoomTraining, Product.ProductType.OnlineTraining, Product.ProductType.ExtendedWarranty
                                Select Case iOrderLine.Product.SoftwareItem.Type
                                    Case Product.ProductType.ComputerBasedTraining
                                        If Not softwareHeaderCreated Then
                                            AddHeader(SBSoftware, Product.ProductType.Software)
                                            softwareHeaderCreated = True
                                        End If
                                        AddSoftwareItem(SBSoftware, iOrderLine)
                                    Case Product.ProductType.Software
                                        If Not softwareHeaderCreated Then
                                            AddHeader(SBSoftware, Product.ProductType.Software)
                                            softwareHeaderCreated = True
                                        End If
                                        AddSoftwareItem(SBSoftware, iOrderLine)
                                    Case Product.ProductType.Certificate
                                        If Not certificateHeaderCreated Then
                                            AddHeader(SBCertificate, Product.ProductType.Certificate)
                                            certificateHeaderCreated = True
                                        End If
                                        AddCertificateItem(SBCertificate, iOrderLine)
                                    Case Product.ProductType.ExtendedWarranty
                                        If Not extendedWarrantyHeaderCreated Then
                                            AddHeader(SBExtendedWarranty, Product.ProductType.ExtendedWarranty)
                                            extendedWarrantyHeaderCreated = True
                                        End If
                                        AddExtendedWarrantyItem(SBExtendedWarranty, iOrderLine)
                                        Dim filename As String = order.OrderNumber + CType(iOrderLine.Product, ExtendedWarrantyModel).CertificateNumber + ".pdf"
                                        objEWFileNameCollection.Add(CType(iOrderLine.Product, ExtendedWarrantyModel).CertificateNumber, filename)
                                    Case Product.ProductType.OnlineTraining
                                        If Not onlineTrainingHeaderCreated Then
                                            AddHeader(SBOnlineTraining, Product.ProductType.OnlineTraining)
                                            onlineTrainingHeaderCreated = True
                                        End If
                                        AddOnlineItem(SBOnlineTraining, iOrderLine)
                                End Select
                        End Select
                    Else
                        Select Case iOrderLine.Product.Type
                            Case Product.ProductType.Part, Product.ProductType.Kit
                                If Not partHeaderCreated Then
                                    AddHeader(SBPart, Product.ProductType.Part)
                                    partHeaderCreated = True
                                    lblOrderInProcess.Visible = True
                                End If
                                AddShipableItem(SBPart, iOrderLine)
                            Case Product.ProductType.ComputerBasedTraining
                                If Not partHeaderCreated Then
                                    AddHeader(SBPart, Product.ProductType.Part)
                                    partHeaderCreated = True
                                    lblOrderInProcess.Visible = True
                                End If
                                AddShipableItem(SBPart, iOrderLine)
                            Case Product.ProductType.Software
                                If Not partHeaderCreated Then
                                    AddHeader(SBPart, Product.ProductType.Part)
                                    partHeaderCreated = True
                                    lblOrderInProcess.Visible = True
                                End If
                                AddShipableItem(SBPart, iOrderLine)
                                'Case Product.ProductType.Software
                                '    Select Case iOrderLine.Product.SoftwareItem.Type
                                '    End Select
                        End Select
                    End If
                Next

                If onlineTrainingHeaderCreated Then
                    AddTotal(SBOnlineTraining, order)
                ElseIf certificateHeaderCreated Then
                    AddTotal(SBCertificate, order)
                ElseIf softwareHeaderCreated Then
                    AddTotal(SBSoftware, order)
                ElseIf partHeaderCreated Then
                    AddTotal(SBPart, order)
                ElseIf extendedWarrantyHeaderCreated Then
                    AddTotal(SBExtendedWarranty, order)
                End If

                If partHeaderCreated Then
                    SBPart.Append("</table>")
                    TDPart.Controls.Add(New LiteralControl(SBPart.ToString()))
                    TRPart.Controls.Add(TDPart)
                    sbInnerHTML.Append("<tr><td>" + SBPart.ToString() + "</td></tr>")

                End If

                If softwareHeaderCreated Then
                    SBSoftware.Append("</table>")
                    TDSoftware.Controls.Add(New LiteralControl(SBSoftware.ToString()))
                    TRSoftware.Controls.Add(TDSoftware)
                    sbInnerHTML.Append("<tr><td>" + SBSoftware.ToString() + "</td></tr>")
                End If

                If extendedWarrantyHeaderCreated Then
                    SBExtendedWarranty.Append("</table>")
                    TDExtendedWarranty.Controls.Add(New LiteralControl(SBExtendedWarranty.ToString()))
                    TRExtendedWarranty.Controls.Add(TDExtendedWarranty)
                    sbInnerHTML.Append("<tr><td>" + SBExtendedWarranty.ToString() + "</td></tr>")
                End If

                If certificateHeaderCreated Then
                    SBCertificate.Append("</table>")
                    TDCertificate.Controls.Add(New LiteralControl(SBCertificate.ToString()))
                    TRCertificate.Controls.Add(TDCertificate)
                    sbInnerHTML.Append("<tr><td>" + SBCertificate.ToString() + "</td></tr>")
                End If

                If onlineTrainingHeaderCreated Then
                    SBOnlineTraining.Append("</table>")
                    TDOnlineTraining.Controls.Add(New LiteralControl(SBOnlineTraining.ToString()))
                    TROnlineTraining.Controls.Add(TDOnlineTraining)
                    sbInnerHTML.Append("<tr><td>" + SBOnlineTraining.ToString() + "</td></tr>")
                End If

                If partHeaderCreated Then TableSuccess.Rows.Add(TRPart)
                If softwareHeaderCreated Then TableSuccess.Rows.Add(TRSoftware)
                If extendedWarrantyHeaderCreated Then TableSuccess.Rows.Add(TRExtendedWarranty)
                If certificateHeaderCreated Then TableSuccess.Rows.Add(TRCertificate)
                If onlineTrainingHeaderCreated Then TableSuccess.Rows.Add(TROnlineTraining)

                hasCustomerTakenSurvey(HttpContextManager.Customer, "1")
                Session.Add("CertificateInSSN", ssnEWCollection)
                Dim blnSendMail As Boolean = False
                If Session(order.OrderNumber) IsNot Nothing Then
                    If Session(order.OrderNumber).ToString() = "True" Then
                        blnSendMail = False
                    Else
                        blnSendMail = True
                    End If
                Else
                    blnSendMail = True
                End If
                'Mails not to be sent for Shippable items - Bug 4520
                If partHeaderCreated Then blnSendMail = False

                blnSendMail = Convert.ToBoolean(IIf(String.IsNullOrEmpty(order.BillingOnly), "False", order.BillingOnly))
                If blnSendMail Then
                    Try
                        Dim objOrderManager As New OrderManager()
                        objOrderManager.SendOrderConfirmationEmail(order, BuildSuccessMailBody(order, sbInnerHTML.ToString()), Nothing)
                        Session.Add(order.OrderNumber, "True")
                    Catch ex As Exception
                        Session.Add(order.OrderNumber, "False")
                        errorMessage.Text = Utilities.WrapExceptionforUI(ex)
                    End Try
                End If
            Catch ex As Exception
                objPCILogger.IndicationSuccessFailure = "Failure" '6524
                objPCILogger.Message = "Page_Load Failed. " & ex.Message.ToString() '6524
                errorMessage.Text = Utilities.WrapExceptionforUI(ex)
            Finally
                objPCILogger.PushLogToMSMQ() '6524
            End Try

            labelSpecialTax.Visible = anySpecialTax
            specialTaxHyperLink.Visible = anySpecialTax
        End Sub

        Private Function BuildSuccessMailBody(ByRef order As Order, ByRef InnerTableHTML As String) As String
            Dim sb As StringBuilder = New StringBuilder()
            sb.Append("<table cellpadding=""3"" cellspacing=""1"" border=""0"" style=""width: 93%;font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: #666666; border:1px #666666 solid;"" role=""presentation"">")
            sb.Append("<tr valign=""top"">")
            sb.Append("<td colspan=""2""  bgcolor=""black"" style=""border-bottom-style:solid; border-bottom-width:thin;""><img src=""" + strWebSiteURL + "images/hdrlogo_sonymb.jpg"" alt=""Sony Logo""/></td>")
            sb.Append("</tr>")
            sb.Append("<tr valign=""top"">")
            sb.Append("<td colspan=""2"" style=""border-bottom-style:solid; border-bottom-width:thin;"">")
            sb.Append("<table border=""0"" cellpadding=""1"" cellspacing=""1"" style=""width: 100%"" role=""presentation"">")
            sb.Append("<tr>")
            sb.Append("<td colspan=""2"" style=""height: 16px"" align=""center""><strong>ServicesPLUS -" + vbCrLf + " " + Resources.Resource.el_OrderInfo + " (" + order.OrderNumber + ")</strong></td>")
            sb.Append("</tr>")
            sb.Append("<tr>")
            sb.Append("<td style=""width: 100px""></td>")
            sb.Append("<td style=""width: 100px""></td>")
            sb.Append("</tr>")
            sb.Append("</table>")
            sb.Append("</td>")
            sb.Append("</tr>")
            sb.Append("<tr valign=""top"">")
            sb.Append("<td colspan=""2"" >")
            sb.Append($"{Resources.Resource.el_OrderSuccessMail1} {order.Customer.FirstName} {order.Customer.LastName},<br /><br /><br />" + vbCrLf)

            If (order.ContainsDownloadAndShipItem) Then
                sb.Append(Resources.Resource.el_OrderSuccessMail2 + "<br\>" + vbCrLf + vbNewLine)
            End If
            If (order.ContainsDownloadableItems()) Then
                'Dim dDate As Date = Date.Now.AddMonths(3)
                sb.Append(Resources.Resource.el_OrderSuccessMail3 + " " + vbCrLf + vbNewLine)
                If softwareHeaderCreated Then
                    sb.Append(Resources.Resource.el_OrderSuccessMail4 + "<br\><br\>" + vbCrLf + vbNewLine)
                Else
                    sb.Append(Resources.Resource.el_OrderSuccessMail5 + "<br\><br\>" + vbCrLf + vbNewLine)
                End If
                sb.Append($"<li>{Resources.Resource.el_OrderSuccessMail6} {ConfigurationData.GeneralSettings.URL.Servicesplus_Link}</li>")
                sb.Append($"<li>{Resources.Resource.el_OrderSuccessMail7}</li>" + vbCrLf)
                sb.Append($"<li>{Resources.Resource.el_OrderSuccessMail8} {order.OrderNumber}.</li><br\><br\>" + vbCrLf)
                sb.Append(vbCrLf)
            Else
                sb.Append(Resources.Resource.el_OrderSuccessMail2 + "<br\>" + vbCrLf + vbNewLine)
            End If

            If softwareHeaderCreated Then sb.Append($"<br\><br\>{Resources.Resource.el_OrderSuccessMail9} {System.DateTime.Now.AddMonths(3).ToLongDateString()}{vbCrLf}")

            sb.Append($"<br/><br/>{Resources.Resource.el_OrderSuccessMail10} <a href=""{ConfigurationData.GeneralSettings.URL.Promotions_Link}"">{Resources.Resource.el_OrderSuccessMail11}</a><br/>")

            sb.Append("<br/><br/>" + Resources.Resource.el_OrderSuccessMail12 + " " + vbCrLf)

            sb.Append("<br/><br/>" + Resources.Resource.el_OrderSuccessMail13 + vbCrLf)

            sb.Append("</td>")
            sb.Append("</tr>")

            sb.Append("<tr valign=""top"">")
            sb.Append("<td colspan=""2"" >")
            sb.Append("<a href=""" + ConfigurationData.GeneralSettings.URL.Privacy_Policy_Link + """>" + Resources.Resource.el_OrderSuccessMail14 + "</a>" + vbCrLf + vbNewLine)
            sb.Append("&nbsp;|&nbsp;<a href=""" + "http://pro.sony.com/bbsc/ssr/services.servicesprograms.bbsccms-services-other-legal.shtml" + """>" + Resources.Resource.el_OrderSuccessMail15 + "</a>" + vbCrLf + vbNewLine)
            sb.Append("&nbsp;|&nbsp;<a href=""" + ConfigurationData.GeneralSettings.URL.Terms_And_Conditions_Link + """>" + Resources.Resource.el_TermsConditions + "</a>" + vbCrLf + vbNewLine)
            sb.Append("&nbsp;|&nbsp;<a href=""" + strWebSiteURL + "Help.aspx" + """>" + Resources.Resource.el_Help + "</a>" + vbCrLf + vbNewLine)
            sb.Append("&nbsp;|&nbsp;<a href=""" + ConfigurationData.GeneralSettings.URL.Contact_Us_Link + """>" + Resources.Resource.el_ContactUs + "</a>" + vbCrLf + vbNewLine)

            sb.Append("</td>")
            sb.Append("</tr>")

            sb.Append("<tr valign=""top"">")
            sb.Append("<td colspan=""2"" style=""border-bottom-style:solid; border-bottom-width:thin;""><br\><br\>")
            InnerTableHTML = InnerTableHTML.Replace("<span style=""font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: red;"">*</span>" + Resources.Resource.el_DownloadSW, "")
            InnerTableHTML = InnerTableHTML.Replace("<span style=""font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: red;"">*</span>" + "Click item number link to download software.", "")
            InnerTableHTML = InnerTableHTML.Replace("color: blue;"" href=", "color: #666666;""")
            InnerTableHTML = InnerTableHTML.Replace("id=""dwnlInstrctn"" style=""visibility:visible""", "id=""dwnlInstrctn"" style=""visibility:hidden""")
            InnerTableHTML = InnerTableHTML.Replace("</a><span style=""font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: red;"">*</span>", "</a>")
            InnerTableHTML = InnerTableHTML.Replace("href=""#ew""", "")
            'InnerTableHTML = InnerTableHTML.Replace("View Certificate", "")

            'InnerTableHTML = InnerTableHTML + 
            sb.Append("</td>")
            sb.Append("</tr>")
            sb.Append("<tr><td colspan=""2"" ><table width=""100%"" role=""presentation"">")
            sb.Append(vbCrLf + InnerTableHTML)
            sb.Append("</table>")
            sb.Append("</td>")
            sb.Append("</tr>")

            Return sb.ToString()
        End Function

        Private Sub AddHeader(ByRef TD As StringBuilder, ByVal HeaderType As Product.ProductType)
            '-- open table
            TD.Append("<table border=""0"" cellpadding=""0"" cellspacing=""0"" width=""100%"" style=""font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: #d5dee9; border:1px #d5dee9 solid;"" role=""presentation"">")

            'If haveDownloadableItems Then errorMessage.Text = "Item marked with an * can be downloaded by clicking on the part number."
            '-- header line 
            TD.Append("<tr >" + vbCrLf)
            TD.Append("<caption style=""font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 14px; font-weight: bold; color: #000000;"">")
            Select Case HeaderType
                Case Product.ProductType.Part, Product.ProductType.Kit, Product.ProductType.ComputerBasedTraining
                    tableColumnCount = 17
                    TD.Append(Resources.Resource.el_ShippingItem)
                Case Product.ProductType.Software
                    tableColumnCount = 13
                    TD.Append(Resources.Resource.el_DownloadSoftware)
                Case Product.ProductType.ExtendedWarranty
                    tableColumnCount = 13
                    TD.Append(Resources.Resource.el_ExtendedWarranty)
                Case Product.ProductType.Certificate
                    tableColumnCount = 13
                    TD.Append(Resources.Resource.el_Certificates)
                Case Product.ProductType.OnlineTraining
                    tableColumnCount = 13
                    TD.Append(Resources.Resource.el_OnlineTraining)
                Case Else
                    tableColumnCount = 15
            End Select
            TD.Append("</caption>" + vbCrLf)
            TD.Append("</tr>")
            TD.Append(getSpacerRow(tableColumnCount))

            TD.Append(("<tr bgcolor=""#d5dee9"">" + vbCrLf))
            TD.Append(("<td width=""1""><img height=""35"" src=""images/spacer.gif"" width=""1"" alt=""""></td>" + vbCrLf))
            If (HeaderType = Product.ProductType.Part Or HeaderType = Product.ProductType.Kit Or HeaderType = Product.ProductType.ComputerBasedTraining) Then
                TD.Append(("<td style="" font-family: Arial, Helvetica, sans-serif; font-size: 11px; line-height: 12px; color: #2a3d47; font-weight: bold ;"" width=""95"" height=""35"">&nbsp;&nbsp;" + Resources.Resource.el_PartsRequesterd + "</td>" + vbCrLf))
                TD.Append(("<td width=""1"" height=""35""><img height=""35"" src=""images/spacer.gif"" width=""1"" alt=""""></td>" + vbCrLf))
                TD.Append(("<td style="" font-family: Arial, Helvetica, sans-serif; font-size: 11px; line-height: 12px; color: #2a3d47; font-weight: bold ;"" width=""95"" height=""35"">&nbsp;&nbsp;" + Resources.Resource.el_PartsSupplied + "</td>" + vbCrLf))
            Else
                TD.Append(("<td style="" font-family: Arial, Helvetica, sans-serif; font-size: 11px; line-height: 12px; color: #2a3d47; font-weight: bold ;"" width=""95"" height=""35"">&nbsp;&nbsp;" + Resources.Resource.el_itemNumber.Replace(":", "") + "</td>" + vbCrLf))
            End If
            TD.Append(("<td width=""1"" height=""35""><img height=""35"" src=""images/spacer.gif"" width=""1"" alt=""""></td>" + vbCrLf))
            TD.Append(("<td style="" font-family: Arial, Helvetica, sans-serif; font-size: 11px; line-height: 12px; color: #2a3d47; font-weight: bold;"" width=""1""  height=""35"" align=""center"">&nbsp;&nbsp;" + Resources.Resource.el_Description + "</td>" + vbCrLf))
            TD.Append(("<td width=""1""  height=""35""><img height=""35"" src=""images/spacer.gif"" width=""1"" alt=""""></td>" + vbCrLf))
            TD.Append(("<td style="" font-family: Arial, Helvetica, sans-serif; font-size: 11px; line-height: 12px; color: #2a3d47; font-weight: bold ;"" align=""center"" width=""90""  height=""35"">" + Resources.Resource.el_Quantity + "<br/>" + Resources.Resource.el_Requested + "</td>" + vbCrLf))
            TD.Append(("<td width=""1""  height=""35""><img height=""28"" src=""images/spacer.gif"" width=""1"" alt=""""></td>" + vbCrLf))
            TD.Append(("<td style="" font-family: Arial, Helvetica, sans-serif; font-size: 11px; line-height: 12px; color: #2a3d47; font-weight: bold ;"" align=""center"" width=""90""  height=""35"">" + Resources.Resource.el_Quantity + "<br/>" + Resources.Resource.el_Confirmed + "</td>" + vbCrLf))
            TD.Append(("<td width=""1""  height=""35""><img height=""28"" src=""images/spacer.gif"" width=""1"" alt=""""></td>" + vbCrLf))
            TD.Append(("<td style="" font-family: Arial, Helvetica, sans-serif; font-size: 11px; line-height: 12px; color: #2a3d47; font-weight: bold ;"" align=""center"" width=""90""  height=""35"">" + Resources.Resource.el_Quantity + "<br/>" + Resources.Resource.el_BackOrder + "</td>" + vbCrLf))
            TD.Append(("<td width=""1""  height=""35""><img height=""28"" src=""images/spacer.gif"" width=""1"" alt=""""></td>" + vbCrLf))

            If HttpContextManager.GlobalData.SalesOrganization = ConfigurationData.GeneralSettings.GlobalData.SalesOrganization.CA Then
                TD.Append(("<td style="" font-family: Arial, Helvetica, sans-serif; font-size: 11px; line-height: 12px; color: #2a3d47; font-weight: bold ;"" align=""center"" width=""70""  height=""35"">" + Resources.Resource.el_ItemPrice_CAD + "</td>" + vbCrLf))
                TD.Append(("<td width=""1"" height=""35""><img height=""28"" src=""images/spacer.gif"" width=""1"" alt=""""></td>" + vbCrLf))
                TD.Append(("<td style="" font-family: Arial, Helvetica, sans-serif; font-size: 11px; line-height: 12px; color: #2a3d47; font-weight: bold ;"" align=""center"" width=""70""  height=""35"">" + Resources.Resource.el_TotalPrice_CAD + "</td>" + vbCrLf))
            Else
                TD.Append(("<td style="" font-family: Arial, Helvetica, sans-serif; font-size: 11px; line-height: 12px; color: #2a3d47; font-weight: bold ;"" align=""center"" width=""70""  height=""35"">" + Resources.Resource.el_ItemPrice + "</td>" + vbCrLf))
                TD.Append(("<td width=""1"" height=""35""><img height=""28"" src=""images/spacer.gif"" width=""1"" alt=""""></td>" + vbCrLf))
                TD.Append(("<td style="" font-family: Arial, Helvetica, sans-serif; font-size: 11px; line-height: 12px; color: #2a3d47; font-weight: bold ;"" align=""center"" width=""70""  height=""35"">" + Resources.Resource.el_TotalPrice + "</td>" + vbCrLf))
            End If

            ' Moved these into the If statement above, since there is no difference between "ExtendedWarranty" and others.
            'If HeaderType = Product.ProductType.ExtendedWarranty Then
            '    If HttpContextManager.GlobalData.SalesOrganization = ConfigurationData.GeneralSettings.GlobalData.SalesOrganization.CA Then
            '        TD.Append(("<td style="" font-family: Arial, Helvetica, sans-serif; font-size: 11px; line-height: 12px; color: #2a3d47; font-weight: bold ;"" align=""center"" width=""70""  height=""35"">" + Resources.Resource.el_TotalPrice_CAD + "</td>" + vbCrLf))
            '    Else
            '        TD.Append(("<td style="" font-family: Arial, Helvetica, sans-serif; font-size: 11px; line-height: 12px; color: #2a3d47; font-weight: bold ;"" align=""center"" width=""70""  height=""35"">" + Resources.Resource.el_TotalPrice + "</td>" + vbCrLf))
            '    End If
            'Else
            '    If HttpContextManager.GlobalData.SalesOrganization = ConfigurationData.GeneralSettings.GlobalData.SalesOrganization.CA Then
            '        TD.Append(("<td style="" font-family: Arial, Helvetica, sans-serif; font-size: 11px; line-height: 12px; color: #2a3d47; font-weight: bold ;"" align=""center"" width=""70""  height=""35"">" + Resources.Resource.el_TotalPrice_CAD + "</td>" + vbCrLf))
            '    Else
            '        TD.Append(("<td style="" font-family: Arial, Helvetica, sans-serif; font-size: 11px; line-height: 12px; color: #2a3d47; font-weight: bold ;"" align=""center"" width=""70""  height=""35"">" + Resources.Resource.el_TotalPrice + "</td>" + vbCrLf))
            '    End If
            'End If

            TD.Append(("<td width=""1"" height=""35""><img height=""28"" src=""images/spacer.gif"" width=""1"" alt=""""></td>" + vbCrLf))
            Select Case HeaderType
                'Case Product.ProductType.Part, Product.ProductType.Kit, Product.ProductType.ComputerBasedTraining
                '    TD.Append(("<td style="" font-family: Arial, Helvetica, sans-serif; font-size: 11px; line-height: 12px; color: #2a3d47; font-weight: bold ;"" align=""center"" width=""80"" height=""35"">Backordered</td>" + vbCrLf))
                'Case Product.ProductType.Software
                '    TD.Append(("<td style="" font-family: Arial, Helvetica, sans-serif; font-size: 11px; line-height: 12px; color: #2a3d47; font-weight: bold ;"" align=""center"" width=""80"" height=""35""></td>" + vbCrLf))
                Case Product.ProductType.ExtendedWarranty
                    TD.Append(("<td style="" font-family: Arial, Helvetica, sans-serif; font-size: 11px; line-height: 12px; color: #2a3d47; font-weight: bold ;"" align=""center"" width=""110"" height=""35"">" + Resources.Resource.el_Certificate + "</td>" + vbCrLf))
                    TD.Append(("<td width=""1"" bgColor=""#d5dee9"" height=""35""><img height=""35"" src=""images/spacer.gif"" width=""1"" alt=""""></td>" + vbCrLf))
                Case Product.ProductType.Certificate
                    TD.Append(("<td style="" font-family: Arial, Helvetica, sans-serif; font-size: 11px; line-height: 12px; color: #2a3d47; font-weight: bold ;"" align=""center"" width=""80"" height=""35"">" + Resources.Resource.el_Activation + "<br/>Code</td>" + vbCrLf))
                    TD.Append(("<td width=""1"" bgColor=""#d5dee9"" height=""35""><img height=""35"" src=""images/spacer.gif"" width=""1"" alt=""""></td>" + vbCrLf))
                    'Case Product.ProductType.OnlineTraining
                    '    TD.Append(("<td style="" font-family: Arial, Helvetica, sans-serif; font-size: 11px; line-height: 12px; color: #2a3d47; font-weight: bold ;"" align=""center"" width=""80"" height=""35""></td>" + vbCrLf))
                    'Case Else
                    '    TD.Append(("<td style="" font-family: Arial, Helvetica, sans-serif; font-size: 11px; line-height: 12px; color: #2a3d47; font-weight: bold ;"" align=""center"" width=""80"" height=""35""></td>" + vbCrLf))
            End Select
            TD.Append(("</tr>"))

            '-- spacer line 
            TD.Append((getSpacerRow(tableColumnCount)))
        End Sub

        Private Sub AddTotal(ByRef TD As StringBuilder, ByRef order As Order)
            '-- subtotal line
            TD.Append(("<tr>"))
            TD.Append(("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>" + vbCrLf))
            TD.Append(("<td bgcolor=""#ffffff"" colspan=""" & (tableColumnCount - 7).ToString() & """><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))

            'price difference for CA and US 
            If HttpContextManager.GlobalData.SalesOrganization = ConfigurationData.GeneralSettings.GlobalData.SalesOrganization.CA Then
                TD.Append(("<td bgcolor=""#d5dee9""  colspan=3  width=""190"" align=""right""  style=""font-family: Arial, Helvetica, sans-serif; font-size: 11px; line-height: 12px; color: #2a3d47; font-weight: bold "">" + Resources.Resource.el_SubTotal_CAD + "</td>" + vbCrLf))
            Else
                TD.Append(("<td bgcolor=""#d5dee9""  colspan=3  width=""190"" align=""right""  style=""font-family: Arial, Helvetica, sans-serif; font-size: 11px; line-height: 12px; color: #2a3d47; font-weight: bold "">" + Resources.Resource.el_SubTotal + "</td>" + vbCrLf))
            End If
            TD.Append(("<td bgcolor=""#ffffff"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
            'TD.Append(("<td bgcolor=""#d5dee9"" width=""80"" align=""right"" style=""font-family: Arial, Helvetica, sans-serif; font-size: 11px; line-height: 12px; color: #2a3d47; font-weight: bold "">$" + Format(order.AllocatedPartSubTotal, "##0.00") + "&nbsp;</td>" + vbCrLf))
            TD.Append(("<td bgcolor=""#d5dee9"" width=""80"" align=""right"" style=""font-family: Arial, Helvetica, sans-serif; font-size: 11px; line-height: 12px; color: #2a3d47; font-weight: bold "">" + String.Format("{0:C}", order.AllocatedPartSubTotal) + "&nbsp;</td>" + vbCrLf))
            TD.Append(("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>" + vbCrLf))
            TD.Append(("</tr>"))

            TD.Append(getSpacerRow(tableColumnCount))

            '-- shipping total line
            TD.Append(("<tr>"))
            TD.Append(("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>" + vbCrLf))
            TD.Append(("<td bgcolor=""#ffffff"" colspan=""" & (tableColumnCount - 7).ToString() & """><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
            TD.Append(("<td bgcolor=""#d5dee9""  colspan=3  width=""190"" align=""right""  style=""font-family: Arial, Helvetica, sans-serif; font-size: 11px; line-height: 12px; color: #2a3d47; font-weight: bold "">" + Resources.Resource.el_SHIPPING + "&nbsp;</td>" + vbCrLf))
            TD.Append(("<td bgcolor=""#ffffff"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
            TD.Append(("<td bgcolor=""#d5dee9"" width=""80"" align=""right"" style=""font-family: Arial, Helvetica, sans-serif; font-size: 11px; line-height: 12px; color: #2a3d47; font-weight: bold "">" + String.Format(" {0:C}", order.AllocatedShipping) + "&nbsp;</td>" + vbCrLf))
            TD.Append(("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>" + vbCrLf))
            TD.Append(("</tr>"))

            TD.Append(getSpacerRow(tableColumnCount))

            '-- special tax line
            If order.AllocatedSpecialTax > 0.0R Then
                TD.Append(("<tr>"))
                TD.Append(("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>" + vbCrLf))
                TD.Append(("<td bgcolor=""#ffffff"" colspan=""" & (tableColumnCount - 7).ToString() & """><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
                TD.Append(("<td bgcolor=""#d5dee9""  colspan=3  width=""190"" align=""right""  style=""font-family: Arial, Helvetica, sans-serif; font-size: 11px; line-height: 12px; color: #2a3d47; font-weight: bold "">" + Resources.Resource.el_STATERECYCLINGFEE + "&nbsp;<span  style=""font-family: Arial, Helvetica, sans-serif; font-size: 11px; line-height: 12px; color: #2a3d47; font-weight: bold ""><span style=""font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: red;"">*</span></span>&nbsp;</td>" + vbCrLf))
                TD.Append(("<td bgcolor=""#ffffff"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
                TD.Append(("<td bgcolor=""#d5dee9"" width=""80"" align=""right""  style=""font-family: Arial, Helvetica, sans-serif; font-size: 11px; line-height: 12px; color: #2a3d47; font-weight: bold "">"))
                TD.Append(((String.Format("{0:c}", order.AllocatedSpecialTax)).ToString() + "&nbsp;</td>" + vbCrLf))
                TD.Append(("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>" + vbCrLf))
                TD.Append(("</tr>"))

                TD.Append(getSpacerRow(tableColumnCount))
            End If

            '-- tax line 
            TD.Append(("<tr>" + vbCrLf))
            TD.Append(("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>" + vbCrLf))
            TD.Append(("<td bgcolor=""#ffffff"" colspan=""" & (tableColumnCount - 7).ToString() & """><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
            TD.Append(("<td bgcolor=""#d5dee9"" colspan=3  width=""190"" align=""right""  style=""font-family: Arial, Helvetica, sans-serif; font-size: 11px; line-height: 12px; color: #2a3d47; font-weight: bold "">" + Resources.Resource.el_TAX + "&nbsp;</td>" + vbCrLf))
            TD.Append(("<td bgcolor=""#ffffff"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
            ''TD.Append(("<td bgcolor=""#d5dee9"" width=""80"" align=""right""  style=""font-family: Arial, Helvetica, sans-serif; font-size: 11px; line-height: 12px; color: #2a3d47; font-weight: bold "">$" + Format(order.AllocatedTax, "##0.00") + "&nbsp;</td>" + vbCrLf))
            TD.Append(("<td bgcolor=""#d5dee9"" width=""80"" align=""right""  style=""font-family: Arial, Helvetica, sans-serif; font-size: 11px; line-height: 12px; color: #2a3d47; font-weight: bold "">" + String.Format("{0:C}", order.AllocatedTax) + "&nbsp;</td>" + vbCrLf))
            TD.Append(("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>" + vbCrLf))
            TD.Append(("</tr>" + vbCrLf))

            TD.Append(getSpacerRow(tableColumnCount))

            '-- invoice total line 
            TD.Append(("<tr>" + vbCrLf))
            TD.Append(("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>" + vbCrLf))
            TD.Append(("<td bgcolor=""#ffffff"" colspan=""" & (tableColumnCount - 7).ToString() & """><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))

            'price difference for CA and US 
            If HttpContextManager.GlobalData.SalesOrganization = ConfigurationData.GeneralSettings.GlobalData.SalesOrganization.CA Then
                TD.Append(("<td bgcolor=""#d5dee9"" colspan=3 width=""190"" align=""right""  style=""font-family: Arial, Helvetica, sans-serif; font-size: 11px; line-height: 12px; color: #2a3d47; font-weight: bold "">" + Resources.Resource.el_ORDERTOTAL_CAD + "&nbsp;</td>" + vbCrLf))
            Else
                TD.Append(("<td bgcolor=""#d5dee9"" colspan=3 width=""190"" align=""right""  style=""font-family: Arial, Helvetica, sans-serif; font-size: 11px; line-height: 12px; color: #2a3d47; font-weight: bold "">" + Resources.Resource.el_ORDERTOTAL + "&nbsp;</td>" + vbCrLf))
            End If

            TD.Append(("<td bgcolor=""#ffffff"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
            ''TD.Append(("<td bgcolor=""#d5dee9"" width=""80"" align=""right""  style=""font-family: Arial, Helvetica, sans-serif; font-size: 11px; line-height: 12px; color: #2a3d47; font-weight: bold "">$" + Format(order.AllocatedGrandTotal, "##0.00") + "&nbsp;</td>" + vbCrLf))
            TD.Append(("<td bgcolor=""#d5dee9"" width=""80"" align=""right""  style=""font-family: Arial, Helvetica, sans-serif; font-size: 11px; line-height: 12px; color: #2a3d47; font-weight: bold "">" + String.Format("{0:C}", order.AllocatedGrandTotal) + "&nbsp;</td>" + vbCrLf))
            TD.Append(("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>" + vbCrLf))
            TD.Append(("</tr>" + vbCrLf))

            TD.Append(getSpacerRow(tableColumnCount))
        End Sub

        Private Sub AddShipableItem(ByRef TD As StringBuilder, ByRef cartLineItem As OrderLineItem)
            Dim strshippablelistprice As String = String.Empty
            Dim strshippabletotprice As String = String.Empty
            Dim strshiptaxlistprice As String = String.Empty
            Dim strshiptaxtotprice As String = String.Empty

            TD.Append("<tr>")
            TD.Append("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>" + vbCrLf)

            '-- Part Number Requested
            TD.Append("<td bgcolor=""#f2f5f8"" width=""95""  align=""center""><span  style=""font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: #666666;"" >" + cartLineItem.Product.PartNumber + "</span></td>")
            TD.Append("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>" + vbCrLf)

            '-- Part Number Provided
            TD.Append("<td bgcolor=""#f2f5f8"" width=""95""  align=""center""><span  style=""font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: #666666;"" >" + vbCrLf)
            If isDownloadable(cartLineItem) Then
                '-- download link will go here.
                TD.Append("<a href=""" + conFileDownloadPage + "?ln=" + cartLineItem.LineNumber + """ class=""body"">" + vbCrLf)
                TD.Append(cartLineItem.Product.ReplacementPartNumber + vbCrLf)
                TD.Append("</a>&nbsp;")
                TD.Append("<span style=""font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: red;"">*</span>")
                If Not haveDownloadableItems Then haveDownloadableItems = True
            Else
                TD.Append(cartLineItem.Product.ReplacementPartNumber)
            End If
            TD.Append("</span></td>")
            TD.Append("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>" + vbCrLf)

            '-- Part Description
            TD.Append(("<td bgcolor=""#f2f5f8"" width=""304"" style=""font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: #666666;"" align=""center"">&nbsp;" + cartLineItem.Product.Description + "</td>") + vbCrLf)
            TD.Append(("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>") + vbCrLf)

            '-- Ordered Quantity
            'TD.Append(("<td bgcolor=""#f2f5f8"" width=""85"" style=""font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: #666666;"" align=""center"">" + iOrderLine.Quantity.ToString + "</td>") + vbCrLf)
            If cartLineItem.Order.OrderStatus.Equals("Restricted Party") Then
                TD.Append(("<td bgcolor=""#f2f5f8"" width=""85"" style=""font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: #666666;"" align=""center"">" + cartLineItem.OrderQty.ToString + "</td>") + vbCrLf)
            Else
                TD.Append(("<td bgcolor=""#f2f5f8"" width=""85"" style=""font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: #666666;"" align=""center"">" + cartLineItem.Quantity.ToString + "</td>") + vbCrLf)
            End If
            TD.Append(("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>") + vbCrLf)

            '-- Confirmed Quantity 
            TD.Append(("<td bgcolor=""#f2f5f8"" width=""85"" style=""font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: #666666;"" align=""center"">" + cartLineItem.ConfirmedQty.ToString + "</td>") + vbCrLf)
            TD.Append(("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>") + vbCrLf)

            '-- Backordered Quantity
            If cartLineItem.Order.OrderStatus.Equals("Restricted Party") Then
                TD.Append(("<td bgcolor=""#f2f5f8"" width=""90"" style=""font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: #666666;"" align=""center"">" + Format(cartLineItem.OrderQty - cartLineItem.ConfirmedQty, "##0") + "</td>") + vbCrLf)
            Else
                TD.Append(("<td bgcolor=""#f2f5f8"" width=""90"" style=""font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: #666666;"" align=""center"">" + Format(cartLineItem.Quantity - cartLineItem.ConfirmedQty, "##0") + "</td>") + vbCrLf)
            End If
            TD.Append(("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>") + vbCrLf)

            '-- list price
            If cartLineItem.Product.ListPrice > 0.0R Then strshippablelistprice = String.Format("{0:C}", cartLineItem.Product.ListPrice / cartLineItem.Quantity)
            TD.Append(("<td bgcolor=""#f2f5f8"" width=""90"" style=""font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: #666666;"" align=""center"">" + strshippablelistprice + "</td>") + vbCrLf) '7502
            TD.Append(("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>") + vbCrLf)

            '-- total price
            Dim totalprice As Double
            If cartLineItem.Order.OrderStatus.Equals("Restricted Party") Then
                totalprice = cartLineItem.OrderQty * (cartLineItem.Product.ListPrice / cartLineItem.Quantity)
            Else
                totalprice = cartLineItem.Product.ListPrice
            End If
            If totalprice > 0.0R Then strshippabletotprice = String.Format("{0:C}", totalprice)
            TD.Append(("<td bgcolor=""#f2f5f8"" width=""90"" style=""font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: #666666;"" align=""center"">" + strshippabletotprice + "</td>") + vbCrLf) '7502
            TD.Append(("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>") + vbCrLf)

            ' -- is item on backorder - 
            'TD.Append(("<td bgcolor=""#f2f5f8"" width=""80"" style=""font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: red;"" align=""center"">&nbsp;") + vbCrLf)
            'If iOrderLine.LineStatus = OrderLineItem.Status.Backordered Then TD.Append(("* Yes") + vbCrLf)
            'TD.Append(("&nbsp;</td>" + vbCrLf))
            'TD.Append(("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>") + vbCrLf)
            TD.Append(("</tr>"))

            TD.Append(getSpacerRow(tableColumnCount))

            If cartLineItem.Product.SpecialTax > 0.0R Then
                anySpecialTax = True

                TD.Append("<tr>" + vbCrLf)
                TD.Append("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>" + vbCrLf)
                '-- Part Number Requested + Spacer + Provided (3 columns)
                TD.Append("<td bgcolor=""#f2f5f8"" colspan=""3"">&nbsp;</td>" + vbCrLf)
                TD.Append("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>" + vbCrLf)
                '-- Description + Spacer + Ordered Quantity (3 columns)
                TD.Append("<td bgcolor=""#f2f5f8"" colspan=""3"" width=""399"" style=""font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: #666666;"">&nbsp;" + Resources.Resource.el_RecyclingFee3 + "&nbsp;<span style=""color: red;"">*</span></td>" + vbCrLf)
                TD.Append("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>" + vbCrLf)

                '-- Confirmed Quantity 
                TD.Append("<td bgcolor=""#f2f5f8"" width=""85"" style=""font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: #666666;"" align=""center"">" + cartLineItem.Quantity.ToString + "</td>" + vbCrLf)
                TD.Append("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>" + vbCrLf)

                '-- Backordered Quantity
                TD.Append("<td bgcolor=""#f2f5f8"">&nbsp;</td>" + vbCrLf)
                TD.Append("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>" + vbCrLf)

                '-- list price
                If cartLineItem.Product.SpecialTax > 0.0R Then strshiptaxlistprice = String.Format("{0:c}", cartLineItem.Product.SpecialTax)
                TD.Append("<td bgcolor=""#f2f5f8"" width=""90"" style=""font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: #666666;"" align=""right"">")
                TD.Append(strshiptaxlistprice + "</td>" + vbCrLf)
                TD.Append("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>" + vbCrLf)

                '-- total price
                If cartLineItem.Product.SpecialTax > 0.0R Then strshiptaxtotprice = (String.Format("{0:c}", cartLineItem.Quantity * cartLineItem.Product.SpecialTax))
                TD.Append(("<td bgcolor=""#f2f5f8"" width=""100"" style=""font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: #666666;"" align=""right"">"))
                TD.Append((strshiptaxtotprice + "</td>" + vbCrLf)) '7502
                TD.Append(("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>" + vbCrLf))

                ' -- is item on backorder - 
                'TD.Append(("<td bgcolor=""#f2f5f8"" width=""80"" style=""font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: red;"" align=""center"">&nbsp;</td>" + vbCrLf))
                'TD.Append(("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>" + vbCrLf))
                TD.Append(("</tr>" + vbCrLf))

                TD.Append((getSpacerRow(17)))
            End If

        End Sub

        Private Sub AddSoftwareItem(ByRef TD As StringBuilder, ByRef iOrderLine As OrderLineItem)
            Dim strswarelistprice As String = String.Empty
            Dim totalprice As Double

            TD.Append(("<tr>" + vbCrLf))
            TD.Append(("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>") + vbCrLf)

            '-- part number 
            TD.Append(("<td bgcolor=""#f2f5f8"" width=""95""align=""center""><span  style=""font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: #666666;""  >") + vbCrLf)
            If isDownloadable(iOrderLine) Then
                '-- download link will go here.
                TD.Append(("<a style=""font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: blue;"" href=""" + conFileDownloadPage + "?ln=" + iOrderLine.LineNumber + """ >") + vbCrLf)
                TD.Append((iOrderLine.Product.PartNumber.ToString()))
                TD.Append(("</a><span style=""font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: red;"">*</span>"))
                If Not haveDownloadableItems Then haveDownloadableItems = True
            Else
                TD.Append(("<span style=""font-family: Arial, Helvetica, sans-serif; font-size: 11px; color:666666;"">" + iOrderLine.Product.PartNumber + "</span>"))
            End If
            TD.Append(("</span></td>"))
            TD.Append(("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>") + vbCrLf)
            '-- part description
            TD.Append(("<td bgcolor=""#f2f5f8"" width=""304"" style=""font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: #666666;"">&nbsp;" + iOrderLine.Product.Description + "</td>") + vbCrLf)
            TD.Append(("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>") + vbCrLf)
            '-- quantity
            TD.Append(("<td bgcolor=""#f2f5f8"" width=""85"" style=""font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: #666666;"" align=""center"">" + iOrderLine.Quantity.ToString + "</td>") + vbCrLf)
            TD.Append(("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>") + vbCrLf)
            '-- list price
            If iOrderLine.Product.ListPrice > 0.0R Then strswarelistprice = String.Format("{0:C}", iOrderLine.Product.ListPrice)
            TD.Append(("<td bgcolor=""#f2f5f8"" width=""90"" style=""font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: #666666;"" align=""right"">" + strswarelistprice + "</td>") + vbCrLf) '7502
            TD.Append(("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>") + vbCrLf)
            totalprice = iOrderLine.Quantity * iOrderLine.Product.ListPrice
            '-- total price
            Dim strswaretotprice As String = String.Empty
            If totalprice > 0.0R Then strswaretotprice = String.Format("{0:C}", totalprice)
            TD.Append(("<td bgcolor=""#f2f5f8"" width=""90"" style=""font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: #666666;"" align=""right"">" + strswaretotprice + "</td>") + vbCrLf) '7502
            TD.Append(("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>") + vbCrLf)
            ' -- is item on backorder - No longer used?
            'TD.Append(("<td bgcolor=""#f2f5f8"" width=""80"" style=""font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: red;"" align=""center"">&nbsp;") + vbCrLf)
            'If iOrderLine.LineStatus = OrderLineItem.Status.Backordered Then TD.Append(("* Yes") + vbCrLf)
            'TD.Append(("&nbsp;</td>" + vbCrLf))
            'TD.Append(("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>") + vbCrLf)
            TD.Append(("</tr>" + vbCrLf))

            TD.Append((getSpacerRow(tableColumnCount)))

            If isDownloadable(iOrderLine) Then
                TD.Append("<tr id=""dwnlInstrctn"" style=""visibility:visible"">" + vbCrLf)
                TD.Append("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>" + vbCrLf)
                TD.Append("<td bgcolor=""#ffffff"" width=""95""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf)
                TD.Append("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>")
                TD.Append("<td bgcolor=""#f2f5f8"" colspan=""9"" width=""100%"">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style=""font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: red;"">*</span><span style=""font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: #666666;"">Click item number link to download software.</span></td>" + vbCrLf)
                TD.Append("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>" + vbCrLf)
                TD.Append("</tr>" + vbCrLf)

                TD.Append((getSpacerRow(tableColumnCount)))
            End If
        End Sub

        Private Sub AddExtendedWarrantyItem(ByRef TD As StringBuilder, ByRef iOrderLine As OrderLineItem)
            Dim strextwarlistprice As String = String.Empty
            Dim strextwartotprice As String = String.Empty

            If Not haveDownloadableItems Then haveDownloadableItems = True

            TD.Append("<tr>")
            TD.Append("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>" + vbCrLf)

            '-- part number 
            TD.Append("<td bgcolor=""#f2f5f8"" width=""95"" align=""center""><span style=""font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: #666666;"">" + iOrderLine.Product.PartNumber + "</span></td>" + vbCrLf)
            TD.Append("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>" + vbCrLf)
            '-- part description
            TD.Append(("<td bgcolor=""#f2f5f8"" width=""304"" style=""font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: #666666;"">&nbsp;" + iOrderLine.Product.Description + "</td>"))
            TD.Append(("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>") + vbCrLf)
            '-- quantity
            TD.Append(("<td bgcolor=""#f2f5f8"" width=""85"" style=""font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: #666666;"" align=""center"">" + iOrderLine.Quantity.ToString + "</td>"))
            TD.Append(("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>") + vbCrLf)
            '-- list price
            If iOrderLine.Product.ListPrice > 0.0R Then strextwarlistprice = String.Format("{0:C}", iOrderLine.Product.ListPrice)
            TD.Append(("<td bgcolor=""#f2f5f8"" width=""90"" style=""font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: #666666;"" align=""right"">" + strextwarlistprice + "</td>") + vbCrLf) '7502
            TD.Append(("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>") + vbCrLf)
            '-- total price
            If iOrderLine.Product.ListPrice > 0.0R Then strextwartotprice = String.Format("{0:C}", iOrderLine.Quantity * iOrderLine.Product.ListPrice)
            TD.Append(("<td bgcolor=""#f2f5f8"" width=""90"" style=""font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: #666666;"" align=""right"">" + strextwartotprice + "</td>") + vbCrLf) '7502
            TD.Append(("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>") + vbCrLf)
            ' -- is item on backorder - 
            TD.Append(("<td bgcolor=""#f2f5f8"" width=""80"" style=""font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: #666666;"" align=""center"">") + vbCrLf)
            TD.Append("<a href=""" + strWebSiteURL + "Sony-EWCertificate.aspx?onum=" + CType(iOrderLine.Product, ExtendedWarrantyModel).CertificateGUID + """ target=_blank >" + vbCrLf)
            TD.Append("<span style=""font-size: 9px;"" >" + Resources.Resource.el_DownloadCertificate + "</span></a>" + vbCrLf)
            TD.Append(("</td>" + vbCrLf))
            TD.Append(("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>") + vbCrLf)
            TD.Append(("</tr>"))

            TD.Append(getSpacerRow(tableColumnCount))
            'If iOrderLine.Product.SoftwareItem.NumberOfCodeAvailable < iOrderLine.Quantity Then

            'End If
        End Sub

        Private Sub AddCertificateItem(ByRef TD As StringBuilder, ByRef iOrderLine As OrderLineItem)
            Dim strcertlistprice As String = String.Empty
            Dim strcerttotalprice As String = String.Empty

            TD.Append(("<tr>"))
            TD.Append(("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>") + vbCrLf)

            '-- part number 
            TD.Append(("<td bgcolor=""#f2f5f8"" width=""95"" align=""center""><span style=""font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: #666666;"">") + vbCrLf)
            If isDownloadable(iOrderLine) Then
                '-- download link will go here.
                TD.Append("<a href=""" + conFileDownloadPage + "?ln=" + iOrderLine.LineNumber + """>" + iOrderLine.Product.PartNumber + "</a>&nbsp;")
                TD.Append(("<span style=""font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: red;"">*</span>") + vbCrLf)
                haveDownloadableItems = True
            Else
                TD.Append(("<span style=""font-family: Arial, Helvetica, sans-serif; font-size: 11px; color:666666;"">" + iOrderLine.Product.PartNumber + "</span>") + vbCrLf)
                'TD.Append((iOrderLine.Product.PartNumber) + vbCrLf)
            End If
            TD.Append("</span></td>" + vbCrLf)
            TD.Append(("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>") + vbCrLf)

            '-- part description
            TD.Append(("<td bgcolor=""#f2f5f8"" width=""304"" style=""font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: #666666;"">&nbsp;" + iOrderLine.Product.Description + "</td>"))
            TD.Append(("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>") + vbCrLf)
            '-- quantity
            TD.Append(("<td bgcolor=""#f2f5f8"" width=""85"" style=""font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: #666666;"" align=""center"">" + iOrderLine.Quantity.ToString + "</td>"))
            TD.Append(("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>") + vbCrLf)
            '-- list price
            If iOrderLine.Product.ListPrice > 0.0R Then strcertlistprice = String.Format("{0:C}", iOrderLine.Product.ListPrice)
            TD.Append(("<td bgcolor=""#f2f5f8"" width=""90"" style=""font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: #666666;"" align=""right"">" + strcertlistprice + "</td>") + vbCrLf) '7502
            TD.Append(("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>") + vbCrLf)
            '-- total price
            If iOrderLine.Product.ListPrice > 0.0R Then strcerttotalprice = String.Format("{0:C}", iOrderLine.Quantity * iOrderLine.Product.ListPrice)
            TD.Append(("<td bgcolor=""#f2f5f8"" width=""90"" style=""font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: #666666;"" align=""right"">" + strcerttotalprice + "</td>") + vbCrLf) '7502
            TD.Append(("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>") + vbCrLf)
            ' -- is item on backorder - 
            TD.Append(("<td bgcolor=""#f2f5f8"" width=""80"" style=""font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: #666666;"" align=""center"">") + vbCrLf)
            If iOrderLine.Product.SoftwareItem.TrainingCertificationCode.Trim() = String.Empty Then
                TD.Append("Call<Span  style=""font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: red;"">**</Span>")
            Else
                TD.Append((iOrderLine.Product.SoftwareItem.TrainingCertificationCode.Replace(",", vbNewLine)) + vbCrLf)
            End If
            TD.Append("&nbsp;</td>" + vbCrLf)
            TD.Append("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>" + vbCrLf)
            TD.Append("</tr>" + vbCrLf)

            TD.Append(getSpacerRow(tableColumnCount))

            TD.Append("<tr>")
            TD.Append("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>" + vbCrLf)
            TD.Append("<td bgcolor=""#ffffff"" width=""95""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf)
            TD.Append("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>" + vbCrLf)
            'If iOrderLine.Product.TrainingActivationCode Is Nothing Then
            '    iOrderLine.Product.TrainingActivationCode = ""
            'End If
            If String.IsNullOrEmpty(iOrderLine.Product.TrainingActivationCode) Then
                TD.Append(("<td bgcolor=""#f2f5f8"" colspan=""9"" width=""100%"">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style=""font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: #666666;""> " + Resources.Resource.el_Goto + " <a  href=""#"" onclick=""window.open('http://" + iOrderLine.Product.TrainingURL + "'); return false;"">" + iOrderLine.Product.TrainingURL + "</a> " + Resources.Resource.el_AddCertificateItem1 + "</span><br/> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style=""font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: #666666;""><span style=""font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: red;"">**</Span>" + Resources.Resource.el_AddCertificateItem3 + " " + ConfigurationData.Environment.Contacts.TrainingInstitute + ".</span></td>") + vbCrLf)
            Else
                TD.Append(("<td bgcolor=""#f2f5f8"" colspan=""9"" width=""100%"">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style=""font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: #666666;"">" + Resources.Resource.el_Goto + "<a  href=""#"" onclick=""window.open('http://" + iOrderLine.Product.TrainingURL + "'); return false;"">" + iOrderLine.Product.TrainingURL + "</a> " + Resources.Resource.el_AddCertificateItem1 + "</span><br/> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style=""font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: #666666;"">" + Resources.Resource.el_AddCertificateItem2 + iOrderLine.Product.TrainingActivationCode + ".</span></td>") + vbCrLf)
            End If

            TD.Append(("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>") + vbCrLf)
            TD.Append(("</tr>"))
            TD.Append((getSpacerRow(tableColumnCount)))
            'If iOrderLine.Product.SoftwareItem.NumberOfCodeAvailable < iOrderLine.Quantity Then

            'End If
        End Sub

        Private Sub AddOnlineItem(ByRef TD As StringBuilder, ByRef iOrderLine As OrderLineItem)
            Dim stronlinelistprice As String = String.Empty
            Dim stronlinetotalprice As String = String.Empty

            TD.Append(("<tr>"))
            TD.Append(("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>") + vbCrLf)

            '-- part number 
            TD.Append(("<td bgcolor=""#f2f5f8"" width=""95"" align=""center""><span style=""font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: #666666;"">") + vbCrLf)
            If isDownloadable(iOrderLine) Then
                '-- download link will go here.
                TD.Append(("<a href=""" + conFileDownloadPage + "?ln=" + iOrderLine.LineNumber + """ >") + vbCrLf)
                TD.Append((iOrderLine.Product.PartNumber.ToString()) + vbCrLf)
                TD.Append(("</a>&nbsp;"))
                TD.Append(("<span style=""font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: red;"">*</span>") + vbCrLf)
                If Not haveDownloadableItems Then haveDownloadableItems = True
            Else
                TD.Append(("<span style=""font-family: Arial, Helvetica, sans-serif; font-size: 11px; color:666666;"">" + iOrderLine.Product.PartNumber + "</span>") + vbCrLf)
            End If
            TD.Append(("</span></td>"))
            TD.Append(("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>") + vbCrLf)

            '-- part description
            TD.Append(("<td bgcolor=""#f2f5f8"" width=""304"" style=""font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: #666666;"">&nbsp;" + iOrderLine.Product.Description + "</td>") + vbCrLf)
            TD.Append(("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>") + vbCrLf)

            '-- quantity
            TD.Append(("<td bgcolor=""#f2f5f8"" width=""85"" style=""font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: #666666;"" align=""center"">" + iOrderLine.Quantity.ToString + "</td>") + vbCrLf)
            TD.Append(("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>") + vbCrLf)

            '-- list price
            If iOrderLine.Product.ListPrice > 0.0R Then stronlinelistprice = String.Format("{0:C}", iOrderLine.Product.ListPrice)
            TD.Append(("<td bgcolor=""#f2f5f8"" width=""90"" style=""font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: #666666;"" align=""right"">" + stronlinelistprice + "</td>") + vbCrLf) '7502
            TD.Append(("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>") + vbCrLf)

            '-- total price
            If iOrderLine.Product.ListPrice > 0.0R Then stronlinetotalprice = String.Format("{0:C}", iOrderLine.Quantity * iOrderLine.Product.ListPrice)
            TD.Append(("<td bgcolor=""#f2f5f8"" width=""90"" style=""font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: #666666;"" align=""right"">" + stronlinetotalprice + "</td>") + vbCrLf) '7502
            TD.Append(("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>") + vbCrLf)

            ' -- is item on backorder - 
            'TD.Append(("<td bgcolor=""#f2f5f8"" width=""80"" style=""font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: #666666;"" align=""center"">&nbsp;") + vbCrLf)
            'If iOrderLine.LineStatus = OrderLineItem.Status.Backordered Then TD.Append(("* Yes") + vbCrLf)
            'TD.Append(("&nbsp;</td>" + vbCrLf))
            'TD.Append(("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>") + vbCrLf)
            TD.Append(("</tr>"))

            TD.Append(getSpacerRow(tableColumnCount) + vbCrLf)

            TD.Append(("<tr>"))
            TD.Append(("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>" + vbCrLf))
            TD.Append(("<td bgcolor=""#ffffff"" width=""95""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
            TD.Append(("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>"))
            If iOrderLine.Product.SoftwareItem.TrainingURL Is Nothing Then iOrderLine.Product.SoftwareItem.TrainingURL = ""
            If String.IsNullOrEmpty(iOrderLine.Product.SoftwareItem.TrainingURL.Trim()) Then
                TD.Append(("<td bgcolor=""#f2f5f8"" colspan=""9"" width=""100%""><span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + Resources.Resource.el_trainingInst1 + " " + ConfigurationData.Environment.Contacts.TrainingInstitute + Resources.Resource.el_trainingInst2 + " " + iOrderLine.Order.OrderNumber + Resources.Resource.el_trainingInst3 + "</span></td>") + vbCrLf)
            Else
                TD.Append(("<td bgcolor=""#f2f5f8"" colspan=""9"" width=""100%"">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style=""font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: #666666;"">" + Resources.Resource.el_OnlineReg1 + "<a  href=""#"" onclick=""window.open('http://" + iOrderLine.Product.TrainingURL + "'); return false;"">" + iOrderLine.Product.TrainingURL + "</a>" + Resources.Resource.el_OnlineReg2 + " " + iOrderLine.Order.OrderNumber + Resources.Resource.el_OnlineReg3 + "</span><br/></td>") + vbCrLf)
            End If

            TD.Append(("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>") + vbCrLf)
            TD.Append(("</tr>"))

            TD.Append(getSpacerRow(tableColumnCount) + vbCrLf)
        End Sub

#End Region

#Region "Methods"
        Private Sub setFileDownloadPaths()
            Try
                'Dim config_settings As Hashtable
                Dim downloadPath As String = Sony.US.SIAMUtilities.ConfigurationData.Environment.Download.Physical
                ' the download path has to be from the root. 
                conSoftwareDownloadPath = downloadPath + "/" + Sony.US.SIAMUtilities.ConfigurationData.Environment.Download.DownloadFiles
            Catch ex As Exception
                errorMessage.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Function getSpacerRow(ByVal columns As Integer) As String
            Dim row As New StringBuilder

            row.Append("<tr>" + vbCrLf)
            row.Append("<td bgcolor=""#ffffff"" colspan=""" & columns.ToString() & """><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf)
            row.Append("</tr>" + vbCrLf)

            Return row.ToString()
        End Function

        Private Function getSpacerRow() As String
            Dim row As New StringBuilder

            row.Append("<tr>" + vbCrLf)
            row.Append("<td bgcolor=""#ffffff"" colspan=""17""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf)
            row.Append("</tr>" + vbCrLf)

            Return row.ToString()
        End Function

        Private Function getSpacerRow2() As String
            Dim row As New StringBuilder

            row.Append("<tr>" + vbCrLf)
            row.Append("<td bgcolor=""#ffffff"" colspan=""13""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf)
            row.Append("</tr>" + vbCrLf)

            Return row.ToString()
        End Function

        Private Function isDownloadable(ByRef thisLineOrderItem As OrderLineItem) As Boolean
            Dim returnValue As Boolean = False

            If Not thisLineOrderItem Is Nothing Then
                If thisLineOrderItem.Downloadable And thisLineOrderItem.Product.Type = Product.ProductType.Software Then
                    'If File.Exists(Server.MapPath("").ToString() + conSoftwareDownloadPath + "/" + thisLineOrderItem.SoftwareDownloadFileName.ToString()) Then
                    If File.Exists(conSoftwareDownloadPath + "/" + thisLineOrderItem.SoftwareDownloadFileName.ToString()) Then
                        returnValue = True
                    End If
                End If
            End If
            Return returnValue
        End Function


        Private Function hasCustomerTakenSurvey(ByRef thisCustomer As Customer, ByVal surveyID As String) As Boolean
            Dim returnValue As Boolean = False

            Try
                If Not thisCustomer Is Nothing And surveyID <> String.Empty Then
                    Dim thisSurveyResponse() As Sony.US.ServicesPLUS.Core.Response = New SurveyManager().GetResponses(New SurveyManager().GetSurvey(surveyID), thisCustomer)

                    If thisSurveyResponse.Length = 0 Then SurveyLink.Text = "<a href=""./survey.aspx?survey=1"" style=""font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: #666666;"" > ServicesPLUS " + Resources.Resource.el_SatisfactionSurvey + "</a>.<br/>"
                End If
            Catch ex As Exception
                errorMessage.Text = Utilities.WrapExceptionforUI(ex)
            End Try

            Return returnValue
        End Function
#End Region

    End Class

End Namespace
