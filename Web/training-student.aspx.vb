Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Process
Imports Sony.US.ServicesPLUS.Data
Imports System.Text.RegularExpressions
Imports ServicesPlusException


Namespace ServicePLUSWebApp

    Partial Class training_student
        Inherits SSL

        Private IsNonClass As Boolean = False

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub


        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As EventArgs) Handles MyBase.Load

            formFieldInitialization()
            If Not IsPostBack Then
                PopulateStateDropdownList(ddlState)

                '2018-07-18 ASleight - Removed payment step from Training orders.
                If Session.Item("IsNonClassRoom") IsNot Nothing Then
                    IsNonClass = IIf(Session.Item("IsNonClassRoom").ToString() = "1", True, False)
                End If
            End If
        End Sub

        Private Sub btnNext_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnNext.Click
            'ValidateForm()
            If Not Page.IsValid() Then Return

            'If ErrorValidation.Visible Then
            '    ErrorValidation.Text = "Please complete the required fields."
            'Else
            Try
                If Session.Item("trainingreservation") Is Nothing Then
                    lblError.Text = ("No course reservation.")
                    Return
                End If

                Dim cr As CourseReservation = Session.Item("trainingreservation")
                Dim bAddToWaitList As Boolean = False
                If (cr.CourseSchedule.Vacancy <= 0 Or cr.CourseSchedule.StartDate.ToString("MM/dd/yyyy") = NullDateTime) Then
                    bAddToWaitList = True
                End If

                Dim participant As New CourseParticipant With {
                        .Company = txtCompanyName.Text.Trim(),
                        .FirstName = txtFirstName.Text.Trim(),
                        .LastName = txtLastName.Text.Trim(),
                        .Address1 = txtAddress1.Text.Trim(),
                        .Address2 = txtAddress2.Text.Trim(),
                        .Address3 = txtAddress3.Text.Trim(),
                        .City = txtCity.Text.Trim(),
                        .State = ddlState.SelectedValue,
                        .Zip = txtZip.Text.Trim(),
                        .Extension = txtExtension.Text.Trim(),
                        .EMail = txtEMail.Text.Trim(),
                        .Fax = txtFax.Text.Trim(),
                        .Phone = txtPhone.Text.Trim(),
                        .ReservationNumber = cr.ReservationNumber,
                        .Status = IIf(bAddToWaitList, "W", "R"),
                        .ISOCountryCode = "840"             ' default to US
                    }
                cr.AddParticipant(participant)

                Response.Redirect("training-tc.aspx")
            Catch ex As Exception
                lblError.Text = Utilities.WrapExceptionforUI(ex)
            End Try
            'End If
        End Sub

        Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As ImageClickEventArgs) Handles btnCancel.Click
            Try
                If Session("mincatid") IsNot Nothing Then
                    Dim id As Int32 = Convert.ToInt32(Session("mincatid").ToString())
                    Response.Redirect($"sony-training-catalog-2.aspx?mincatid={id}", True)
                Else
                    Response.Redirect("sony-training-search.aspx", True)
                End If
            Catch ex As Threading.ThreadAbortException
                ' Ignore it
            End Try
        End Sub

        Private Sub CheckBoxCopyProfile_CheckedChanged(ByVal sender As System.Object, ByVal e As EventArgs) Handles CheckBoxCopyProfile.CheckedChanged
            If CheckBoxCopyProfile.Checked And HttpContextManager.Customer IsNot Nothing Then
                Try
                    Dim customer As Customer = HttpContextManager.Customer
                    txtCompanyName.Text = customer.CompanyName
                    txtAddress1.Text = customer.Address.Line1
                    txtAddress2.Text = customer.Address.Line2 'Fix for Bug# 325
                    txtAddress3.Text = customer.Address.Line3
                    txtCity.Text = customer.Address.City
                    ddlState.SelectedValue = customer.Address.State
                    txtExtension.Text = customer.PhoneExtension
                    txtEMail.Text = customer.EmailAddress
                    txtFax.Text = customer.FaxNumber
                    txtPhone.Text = customer.PhoneNumber
                    txtZip.Text = customer.Address.PostalCode
                    txtFirstName.Text = customer.FirstName
                    txtLastName.Text = customer.LastName
                Catch ex As Exception
                    lblError.Text = Utilities.WrapExceptionforUI(ex)
                End Try
            End If
        End Sub

        '--- 2018-08-16 ASleight - Moving validation to the client.
        'Private Sub ValidateForm()
        '    Try
        '        ErrorValidation.Text = ""
        '        ErrorValidation.Visible = False

        '        LabelFirstName.CssClass = "tableData"
        '        LabelCompany.CssClass = "tableData"
        '        LabelLastName.CssClass = "tableData"
        '        LabelLine1.CssClass = "tableData"
        '        LabelLine2.CssClass = "tableData"
        '        LabelLine3.CssClass = "tableData"
        '        LabelCity.CssClass = "tableData"
        '        LabelState.CssClass = "tableData"
        '        LabelZip.CssClass = "tableData"
        '        LabelPhone.CssClass = "tableData"
        '        LabelPhone.CssClass = "tableData"
        '        LabelEMail.CssClass = "tableData"

        '        LabelCompanyStar.Text = ""
        '        LabelLine1Star.Text = ""
        '        LabelLine2Star.Text = ""
        '        LabelLine3Star.Text = ""
        '        LabelZipStar.Text = ""
        '        LabelCityStar.Text = ""
        '        LabelStateStar.Text = ""
        '        LabelFirstNameStar.Text = ""
        '        LabelLastNameStar.Text = ""
        '        LabelPhoneStar.Text = ""
        '        LabelEMailStar.Text = ""

        '        If txtCompanyName.Text = "" Then
        '            ErrorValidation.Visible = True
        '            LabelCompany.CssClass = "redAsterick"
        '            LabelCompanyStar.Text = "<span class=""redAsterick"">*</span>"
        '        End If

        '        If FirstName.Text = "" Then
        '            ErrorValidation.Visible = True
        '            LabelFirstName.CssClass = "redAsterick"
        '            LabelFirstNameStar.Text = "<span class=""redAsterick"">*</span>"
        '        End If

        '        If LastName.Text = "" Then
        '            ErrorValidation.Visible = True
        '            LabelLastName.CssClass = "redAsterick"
        '            LabelLastNameStar.Text = "<span class=""redAsterick"">*</span>"
        '        End If

        '        If txtLine1.Text = "" Then
        '            ErrorValidation.Visible = True
        '            LabelLine1.CssClass = "redAsterick"
        '            LabelLine1Star.Text = "<span class=""redAsterick"">*</span>"
        '        End If

        '        If txtLine1.Text.Length > 35 Then
        '            LabelLine1.CssClass = "redAsterick"
        '            ErrorValidation.Visible = True
        '            LabelLine1Star.Text = "<span class=""redAsterick"">&nbsp;&nbsp;Street Address must be 35 characters or less in length.</span>"
        '            txtLine1.Focus()
        '        End If

        '        If txtLine2.Text.Length > 35 Then
        '            LabelLine2.CssClass = "redAsterick"
        '            ErrorValidation.Visible = True
        '            LabelLine2Star.Text = "<span class=""redAsterick"">Address 2nd Line must be 35 characters or less in length.</span>"
        '            txtLine2.Focus()
        '        End If

        '        If txtLine3.Text.Length > 35 Then
        '            LabelLine3.CssClass = "redAsterick"
        '            ErrorValidation.Visible = True
        '            LabelLine3Star.Text = "<span class=""redAsterick"">Address 3rd Line must be 35 characters or less in length.</span>"
        '            txtLine3.Focus()
        '        End If


        '        If txtEMail.Text = "" Then
        '            ErrorValidation.Visible = True
        '            LabelEMail.CssClass = "redAsterick"
        '            LabelEMailStar.Text = "<span class=""redAsterick"">*</span>"
        '        End If

        '        If txtCity.Text = "" Then
        '            ErrorValidation.Visible = True
        '            LabelCity.CssClass = "redAsterick"
        '            LabelCityStar.Text = "<span class=""redAsterick"">*</span>"
        '        End If

        '        If txtCity.Text.Length > 35 Then
        '            LabelCity.CssClass = "redAsterick"
        '            ErrorValidation.Visible = True
        '            LabelCityStar.Text = "<span class=""redAsterick"">&nbsp;&nbsp;City must be 35 characters or less in length.</span>"
        '            txtCity.Focus()
        '        End If

        '        If ddlState.SelectedIndex = 0 Then
        '            ErrorValidation.Visible = True
        '            LabelState.CssClass = "redAsterick"
        '            LabelStateStar.Text = "<span class=""redAsterick"">*</span>"
        '        End If

        '        Dim sZip As String = txtZip.Text.Trim()
        '        Dim objZipCodePattern As New Regex("\d{5}(-\d{4})?")
        '        If Not objZipCodePattern.IsMatch(sZip) Then
        '            ErrorValidation.Visible = True
        '            LabelZip.CssClass = "redAsterick"
        '            LabelZipStar.Text = "<span class=""redAsterick"">*</span>"
        '        End If

        '        Dim sPhoneNumber As String = txtPhone.Text.Trim()
        '        If sPhoneNumber = "" Then
        '            lblError.Visible = True
        '            LabelPhone.CssClass = "redAsterick"
        '            LabelPhoneStar.Text = "<span class=""redAsterick"">*</span>"
        '        Else
        '            Dim objHPhoneNumber As New Regex("(\d{3}[-]{1}\d{3}[-]{1}\d{4})")
        '            If Not objHPhoneNumber.IsMatch(sPhoneNumber) Then
        '                lblError.Visible = True
        '                lblError.Text = "Invalid Phone number."
        '            End If
        '        End If

        '        Dim sExtension As String = txtExtension.Text.Trim.ToString()
        '        If sExtension.Length > 0 Then
        '            Dim objExt As New Regex("^[0-9]*$")
        '            If Not objExt.IsMatch(sExtension) Or sExtension.Length > 4 Then
        '                lblError.Text = "Extension must be numeric and 4 or fewer digits."
        '            End If
        '        End If
        '    Catch ex As Exception
        '        lblError.Text = Utilities.WrapExceptionforUI(ex)
        '    End Try
        'End Sub

        '-- 2018-08-16 ASleight - State Dropdown building is now done in base class "SSL"
        'Private Sub BuildStateDropDown()
        '    ddlState.Items.Add(New ListItem("Select One", ""))
        '    ddlState.Items.Add(New ListItem("AK", "AK"))
        '    ddlState.Items.Add(New ListItem("AL", "AL"))
        '    ddlState.Items.Add(New ListItem("AR", "AR"))
        '    ddlState.Items.Add(New ListItem("AZ", "AZ"))
        '    ddlState.Items.Add(New ListItem("CA", "CA"))
        '    ddlState.Items.Add(New ListItem("CO", "CO"))
        '    ddlState.Items.Add(New ListItem("CT", "CT"))
        '    ddlState.Items.Add(New ListItem("DC", "DC"))
        '    ddlState.Items.Add(New ListItem("DE", "DE"))
        '    ddlState.Items.Add(New ListItem("FL", "FL"))
        '    ddlState.Items.Add(New ListItem("GA", "GA"))
        '    ddlState.Items.Add(New ListItem("HI", "HI"))
        '    ddlState.Items.Add(New ListItem("IA", "IA"))
        '    ddlState.Items.Add(New ListItem("ID", "ID"))
        '    ddlState.Items.Add(New ListItem("IL", "IL"))
        '    ddlState.Items.Add(New ListItem("IN", "IN"))
        '    ddlState.Items.Add(New ListItem("KS", "KS"))
        '    ddlState.Items.Add(New ListItem("KY", "KY"))
        '    ddlState.Items.Add(New ListItem("LA", "LA"))
        '    ddlState.Items.Add(New ListItem("MA", "MA"))
        '    ddlState.Items.Add(New ListItem("MD", "MD"))
        '    ddlState.Items.Add(New ListItem("ME", "ME"))
        '    ddlState.Items.Add(New ListItem("MI", "MI"))
        '    ddlState.Items.Add(New ListItem("MN", "MN"))
        '    ddlState.Items.Add(New ListItem("MO", "MO"))
        '    ddlState.Items.Add(New ListItem("MS", "MS"))
        '    ddlState.Items.Add(New ListItem("MT", "MT"))
        '    ddlState.Items.Add(New ListItem("NC", "NC"))
        '    ddlState.Items.Add(New ListItem("ND", "ND"))
        '    ddlState.Items.Add(New ListItem("NE", "NE"))
        '    ddlState.Items.Add(New ListItem("NH", "NH"))
        '    ddlState.Items.Add(New ListItem("NJ", "NJ"))
        '    ddlState.Items.Add(New ListItem("NM", "NM"))
        '    ddlState.Items.Add(New ListItem("NV", "NV"))
        '    ddlState.Items.Add(New ListItem("NY", "NY"))
        '    ddlState.Items.Add(New ListItem("OH", "OH"))
        '    ddlState.Items.Add(New ListItem("OK", "OK"))
        '    ddlState.Items.Add(New ListItem("OR", "OR"))
        '    ddlState.Items.Add(New ListItem("PA", "PA"))
        '    ddlState.Items.Add(New ListItem("RI", "RI"))
        '    ddlState.Items.Add(New ListItem("SC", "SC"))
        '    ddlState.Items.Add(New ListItem("SD", "SD"))
        '    ddlState.Items.Add(New ListItem("TN", "TN"))
        '    ddlState.Items.Add(New ListItem("TX", "TX"))
        '    ddlState.Items.Add(New ListItem("UT", "UT"))
        '    ddlState.Items.Add(New ListItem("VA", "VA"))
        '    ddlState.Items.Add(New ListItem("VT", "VT"))
        '    ddlState.Items.Add(New ListItem("WA", "WA"))
        '    ddlState.Items.Add(New ListItem("WI", "WI"))
        '    ddlState.Items.Add(New ListItem("WV", "WV"))
        '    ddlState.Items.Add(New ListItem("WY", "WY"))
        'End Sub

        Private Sub formFieldInitialization()
            txtCompanyName.Attributes("onkeydown") = "SetTheFocusButton(event, 'btnNext')"
            txtFirstName.Attributes("onkeydown") = "SetTheFocusButton(event, 'btnNext')"
            txtLastName.Attributes("onkeydown") = "SetTheFocusButton(event, 'btnNext')"
            txtAddress1.Attributes("onkeydown") = "SetTheFocusButton(event, 'btnNext')"
            txtAddress2.Attributes("onkeydown") = "SetTheFocusButton(event, 'btnNext')"
            txtAddress3.Attributes("onkeydown") = "SetTheFocusButton(event, 'btnNext')"
            txtZip.Attributes("onkeydown") = "SetTheFocusButton(event, 'btnNext')"
            txtPhone.Attributes("onkeydown") = "SetTheFocusButton(event, 'btnNext')"
            txtExtension.Attributes("onkeydown") = "SetTheFocusButton(event, 'btnNext')"
            txtFax.Attributes("onkeydown") = "SetTheFocusButton(event, 'btnNext')"
            txtEMail.Attributes("onkeydown") = "SetTheFocusButton(event, 'btnNext')"
        End Sub

    End Class

End Namespace
