Imports System
Imports System.IO
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Drawing
Imports System.Web
Imports System.Web.SessionState
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports System.Text
Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Process
Imports Sony.US.ServicesPLUS.Data
Imports Sony.US.siam
Imports Sony.US.SIAMUtilities
Imports ServicesPlusException
'6994 starts
Imports System.Web.Script.Services
Imports System.Web.Services
Imports Sony.US.AuditLog
'6994 ends


Namespace ServicePLUSWebApp


    Partial Class SoftwarePLUSModelSearch
        Inherits SSL

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.

        Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.      
            'If Request.QueryString("model") Is Nothing Then
            '    Response.Redirect("sony-software-model.aspx")
            'End If
            isProtectedPage(False)
            InitializeComponent()
            FormFieldInitialization()
        End Sub

#End Region

#Region "Page Constants"
        'search parameter array length
        Private Const conSearchParmsArrayLength As Short = 2
        Private Const conMaxResultsPerPage As Short = 50
        'file size denominator
        Private Const MegaBytes As Integer = 1064960
        Private Const KiloBytes As Integer = 1024

        'file path
        Private conSoftwareDownloadPath As String
        Private conReleaseNotesPath As String

        'use to tell if the addToCart icon was clicked.
        Private Const conAddToCart As String = "addToCart"
#End Region

#Region "Page Variables"
        'result page variables
        Private currentResultPage As Integer = 1
        Private endingResultPage As Integer = 1
        Private startingResultItem As Integer = 1
        Private resultPageRequested As Integer = 3
        Private totalSearchResults As Integer = 0
        'sort order variables
        Private sortOrder As enumSortOrder = enumSortOrder.VersionDESC
        Private IsSort As Boolean = True
        'file download and release notes directory    
        Private downloadURL As String
        Private downloadPath As String
        Public sModel As String = String.Empty
        Public sPartnumber As String = String.Empty
        Public sModelDescription As String = ""
        Public sProductCategory As String = ""
        Private isPartSearch As Boolean = False
        Public imageURL As String = "images/sp_int_header_top_SoftwarePLUS.jpg"
        Public headerText As String
        Protected WithEvents lblSubmitError As Label
        Protected WithEvents lblSoftwareResults As Label
        Protected WithEvents lblNextResult As Label
        'Dim objUtilties As Utilities = New Utilities
#End Region

#Region "Page Enumerators"
        '-- result page
        Private Enum enumResultPageRequested
            PreviousPage = 0
            NextPage = 1
            UserEnteredPage = 2
            FirstPage = 3
            LastPage = 4
            Current = 5
        End Enum

        '-- sort order 
        Private Enum enumSortOrder
            ProductCategoryASC = 0
            ProductCategoryDESC = 1
            ModelNumberASC = 2
            ModelNumberDESC = 3
            KitPartNumberASC = 4
            KitPartNumberDESC = 5
            VersionASC = 6
            VersionDESC = 7
        End Enum

        '-- sort type
        Private Enum enumSortBy
            ProductCatagory = 0
            ModelNumber = 1
            KitPartNumber = 2
            Version = 3
        End Enum
#End Region

#Region "Events"
        Private Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
            'Put user code to initialize the page here
            Dim isValidInput As Boolean = True
            errorMessageLabel.Text = ""     '-- clear the error label  
            headerText = Resources.Resource.hplnk_software_frmwre
            Try
                '-- if not post back the we want to check the event target form field to see if the add to cart button was click.
                '-- if it was then we change the id of the send and call the control method (processPageRequest). The ID is change
                '-- to make it easy to determine what method need to be called on the control method.
                If Not Page.IsPostBack Then
                    If HttpContextManager.Customer IsNot Nothing And Session("SelectedItemsBeforeLoggedIn") IsNot Nothing Then
                        AddToCartAfterLogin(errorMessageLabel)
                    End If

                    If Request.QueryString("partsearch") IsNot Nothing Then
                        Try
                            isPartSearch = Convert.ToInt32(Request.QueryString("partsearch"))
                        Catch fex As FormatException
                            isValidInput = False
                        End Try
                    End If

                    If Request.QueryString("Partnumber") IsNot Nothing Then
                        sPartnumber = Request.QueryString("Partnumber")
                        If (Not IsValidString(sPartnumber)) Then
                            isValidInput = False
                            sPartnumber = HttpUtility.HtmlEncode(sPartnumber)
                        Else
                            isPartSearch = True
                            ViewState("PartNumber") = sPartnumber
                            imageURL = "images/sp_int_header_top_trainist.jpg"
                            headerText = Resources.Resource.hplnk_sony_training_inst
                            If Session("SelectedItemsBeforeLoggedIn") Is Nothing Then
                                Session.Remove("SortOrder")
                            End If
                        End If
                    End If

                    If Request.QueryString("model") IsNot Nothing Then
                        sModel = Trim(Request.QueryString("model").Replace("+", "/"))
                        If (Not IsValidString(sModel)) Then
                            isValidInput = False
                            sModel = HttpUtility.HtmlEncode(sModel)
                        Else
                            Session("ModelNumber") = sModel
                            If Session("SelectedItemsBeforeLoggedIn") Is Nothing Then
                                Session.Remove("SortOrder")
                            End If
                        End If
                    End If

                    'If Not Request.QueryString("model") Is Nothing And Not Request.Form("__EVENTTARGET") Is Nothing And Not Request.Form("__EVENTARGUMENT") Is Nothing Then
                    If Request.QueryString("addtocart") IsNot Nothing Then
                        If Session("SelectedItemsBeforeLoggedIn") IsNot Nothing Then
                            Session.Remove("SelectedItemsBeforeLoggedIn")
                        End If
                        'processAddToCartRequest()'6994
                    End If

                    If Request.QueryString("nodelivermethod") IsNot Nothing And Session.Item("customer") Is Nothing Then
                        '-- if no delivery option has been selected and software is downloadable then display message to customer.                 
                        errorMessageLabel.Text = "Please select a delivery method (download, ship) before adding to cart."
                    End If

                    If Request.QueryString("delivermethod") IsNot Nothing And Session.Item("customer") IsNot Nothing Then
                        '-- if no delivery option has been selected and software is downloadable then display message to customer.                 
                        errorMessageLabel.Text = "Please select a delivery method (download, ship) before adding to cart."
                    End If

                    ProcessPageRequest(sender, e)

                Else
                    If ViewState("PartNumber") IsNot Nothing Then
                        sPartnumber = ViewState.Item("PartNumber").ToString().Trim().Replace("-", "")
                        isPartSearch = True
                    ElseIf Session("ModelNumber") IsNot Nothing Then
                        sModel = Session("ModelNumber").ToString().Trim().Replace("-", "")
                    End If

                    If Request.QueryString("partnumber") IsNot Nothing Then
                        sPartnumber = Request.QueryString("partnumber")
                        If (Not IsValidString(sPartnumber)) Then
                            isValidInput = False
                            sPartnumber = HttpUtility.HtmlEncode(sPartnumber)
                        Else
                            isPartSearch = True
                            ViewState("PartNumber") = Request.QueryString("partnumber")
                        End If
                    Else
                        isPartSearch = False
                    End If

                    If Request.Form("__EVENTTARGET") IsNot Nothing And Request.Form("__EVENTARGUMENT") IsNot Nothing Then
                        If Request.Form("__EVENTTARGET").ToString() = conAddToCart Then
                            sender = New UserControl
                            sender.ID = conAddToCart
                            ProcessPageRequest(sender, e)
                        End If
                    End If
                End If
                If (Not isValidInput) Then
                    errorMessageLabel.Text = "Modified URL detected. Please go back and try again."
                End If
            Catch ex As Exception
                errorMessageLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Sub allImage_Click(ByVal sender As Object, ByVal e As ImageClickEventArgs) Handles btnPreviousPage.Click, btnNextPage.Click, btnSearch.Click, btnGoToPage.Click, btnPreviousPage2.Click, btnNextPage2.Click, btnGoToPage2.Click
            ProcessPageRequest(sender, e)
        End Sub

        Private Sub allLink_Click(ByVal sender As Object, ByVal e As EventArgs) Handles PreviousLinkButton.Click, NextLinkButton.Click, PreviousLinkButton2.Click, NextLinkButton2.Click, lnkKitPart1.Click, lnkVersion.Click   ' , lnkKitPart2.Click
            ProcessPageRequest(sender, e)
        End Sub

#End Region

#Region "Methods"
        Private Sub ProcessPageRequest(ByVal sender As Object, ByVal e As EventArgs)
            Try
                Dim callingControl As String = String.Empty

                If sender.ID IsNot Nothing Then callingControl = sender.ID.ToString()
                SetFileDownloadPaths()                                  '-- set the file download path variables 

                Select Case (callingControl)
                    Case btnSearch.ID.ToString()
                        Session.Remove("ModelNumber")
                        Response.Redirect("sony-software.aspx")
                    Case conAddToCart
                        'processAddToCartRequest()    '6994                   '-- add the item to a shopping cart
                End Select

                resultPageRequested = GetRequestedPage(callingControl)  '-- set the result page requested             
                SetSortOrder(callingControl)                            '-- set the sort order
                SearchForSoftware()                                     '-- search for software
            Catch ex As Exception
                errorMessageLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

#Region "Search Functionality"
        Private Sub SearchForSoftware()
            If isPartSearch Then
                BuildSearchResultsTable(sPartnumber)
            Else
                BuildSearchResultsTable(sModel)
            End If
        End Sub

        Private Function GetSearchParms() As String()
            Dim returnSearchParms(conSearchParmsArrayLength) As String
            Dim modelDesc As String = ""
            Dim categoryName As String = ""
            Dim modelNumber As String = ""

            Try
                modelNumber = Session("ModelNumber").ToString().Trim().Replace("-", "")

                '-- just a check to make sure we are not passing in to much data. --
                If modelNumber.Length > 30 Then modelNumber.Substring(0, 30)
                If modelDesc.Length > 100 Then modelDesc.Substring(0, 100)
                If categoryName.Length > 100 Then categoryName.Substring(0, 100)

                returnSearchParms.SetValue(modelNumber, 0)
                returnSearchParms.SetValue(modelDesc, 1)
                returnSearchParms.SetValue(categoryName, 2)
            Catch ex As Exception
                errorMessageLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try

            Return returnSearchParms
        End Function

        Private Function BuildSearchResultsTable(ByVal searchParms As String) As Table
            Dim softwareResults() As Software
            Dim softwareResultsDisplayed As New ArrayList
            Dim resultTable As New Table
            Dim currentResultItem As Integer = 1
            Dim x As Integer = 0

            Try
                Dim accountType As enumAccountType = SSL.enumAccountType.Unknown '6668
                If HttpContextManager.Customer IsNot Nothing Then accountType = getUsersAccountType(HttpContextManager.Customer.SIAMIdentity)

                '-- get search results collection
                softwareResults = GetSoftwareResultCollection(searchParms)

                If (softwareResults IsNot Nothing) AndAlso (softwareResults.Length <> 0) Then
                    '-- calculate pagination 
                    totalSearchResults = softwareResults.Length
                    CalcStaringResultItem(resultPageRequested)

                    'sort the collection of software
                    Software.SortOrder = sortOrder
                    Array.Sort(softwareResults)

                    '-- loop through all the software 
                    For Each softwareItem As Software In softwareResults
                        softwareItem.Download = softwareItem.BillingOnly
                        softwareItem.Ship = Not softwareItem.BillingOnly

                        If softwareItem.ModelNumber = Session("ModelNumber") Or softwareItem.PartNumber = sPartnumber Then
                            sModel = softwareItem.ModelNumber
                            sModelDescription = softwareItem.ModelDescription
                            If Not String.IsNullOrEmpty(softwareItem.ProductCategory) Then
                                sProductCategory = softwareItem.ProductCategory
                            End If
                        End If
                        If currentResultItem >= startingResultItem Then
                            SoftwareSearchResultTable.Controls.Add(BuildSoftwareResultRow(softwareItem, x, accountType))
                            softwareResultsDisplayed.Add(softwareItem)
                            x = (x + 1)
                        End If
                        currentResultItem = (currentResultItem + 1)
                        If currentResultItem >= (startingResultItem + conMaxResultsPerPage) Then Exit For
                    Next

                    If Not isPartSearch Then lblSubHdr.InnerText = sModel + " " + sModelDescription
                    '-- add collection of displayed software to array - this is used for add to cart functionality.
                    Session.Add("software", softwareResultsDisplayed)
                Else
                    '-- no software found --
                    SoftwareSearchResultTable.Controls.Add(BuildSoftwareResultRow(totalSearchResults))
                End If

                '-- update the pagination controlsl                 
                UpdateResultPageNav()

            Catch ex As Exception
                errorMessageLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try

            Return resultTable
        End Function

        Private Function GetSoftwareResultCollection(ByVal searchParms As String) As Software()
            Dim resultCollection As Software() = Nothing
            Try
                Dim catalogManager As New CatalogManager
                resultCollection = catalogManager.SoftwareSearch(searchParms, HttpContextManager.GlobalData.SalesOrganization, isPartSearch)

            Catch ex As Exception
                errorMessageLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
            Return resultCollection
        End Function

        Private Function GetRequestedPage(ByVal callingControl As String) As enumResultPageRequested
            Dim returnValue As enumResultPageRequested = enumResultPageRequested.Current
            Try
                Select Case (callingControl)
                    Case Page.ID.ToString(), conAddToCart
                        returnValue = enumResultPageRequested.Current
                    Case btnSearch.ID.ToString()
                        returnValue = enumResultPageRequested.FirstPage
                    Case btnGoToPage.ID.ToString, btnGoToPage2.ID.ToString
                        returnValue = enumResultPageRequested.UserEnteredPage
                    Case btnPreviousPage.ID.ToString, PreviousLinkButton.ID.ToString(), btnPreviousPage2.ID.ToString, PreviousLinkButton2.ID.ToString()
                        returnValue = enumResultPageRequested.PreviousPage
                    Case btnNextPage.ID.ToString(), NextLinkButton.ID.ToString(), btnNextPage2.ID.ToString(), NextLinkButton2.ID.ToString()
                        returnValue = enumResultPageRequested.NextPage
                        'Case ProductCategorySortLink1.ID.ToString, ProductCategorySortLink2.ID.ToString, ModelSortLink.ID.ToString
                        '    returnValue = enumResultPageRequested.UserEnteredPage
                End Select
            Catch ex As Exception
                errorMessageLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try

            Return returnValue
        End Function
#End Region

#Region "result table"
        Private Function BuildSoftwareResultRow(ByVal software As Software, ByVal x As Integer, ByVal accountType As enumAccountType) As TableRow
            Dim tr As New TableRow
            Dim td As New TableCell

            Try
                Dim upgradeVersion As New Label With {.ID = "upgradeVersion" + x.ToString, .CssClass = "tableData", .Text = "&nbsp;"}
                Dim kitPartNumber As New Label With {.ID = "kitPartNumber" + x.ToString, .CssClass = "tableData", .Text = "&nbsp;"}
                Dim kitPartDescription As New Label With {.ID = "kitPartDescription" + x.ToString, .CssClass = "tableData", .Text = "&nbsp;"}
                Dim listPrice As New Label With {.ID = "listPrice" + x.ToString, .CssClass = "tableData", .Text = "&nbsp;"}
                Dim yourPrice As New Label With {.ID = "yourPrice" + x.ToString, .CssClass = "tableData", .Text = "&nbsp;"}
                Dim fileSize As New Label With {.ID = "fileSize" + x.ToString, .CssClass = "tableData", .Text = ""}
                Dim download As New RadioButton With {.ID = "download" + x.ToString, .CssClass = "tableData", .Text = "&nbsp;"}
                Dim ship As New RadioButton With {.ID = "ship" + x.ToString, .CssClass = "tableData", .Text = ""}
                Dim addToCart As New ImageButton With {.ID = "addToCart" + x.ToString, .CssClass = "tableData", .CommandArgument = kitPartNumber.ID.ToString(), .EnableViewState = True}
                Dim addToCart2 As New LiteralControl
                Dim releaseNoteslink As New Label With {.ID = "releaseNoteslink" + x.ToString, .CssClass = "tableData", .Text = "&nbsp;"}
                Dim releaseNotesIcon As New Label With {.ID = "releaseNotesIcon" + x.ToString, .CssClass = "tableData", .Text = "&nbsp;"}
                Dim addToCart1 As New Label With {.ID = "addToCart" + x.ToString, .CssClass = "tableData", .Text = "&nbsp;"}
                Dim addToCart3 As New LiteralControl
                Dim hasFile, hasPrice, isFree, isFreeReqKey, isAccountHolder As Boolean
                Dim showTextFileSize, showFileSize, showListPrice, showAvailabilityLink, showYourPriceLink, showCallNumber, showDownloadLink As Boolean

                addToCart.AlternateText = "Add to cart"

                If isPartSearch Then
                    addToCart2.Text = "<a href=""#"" onclick=""javascript:selectSoftwareItemWithoutLoggedIn('NotLoggedInAddToCart.aspx','400','400','post=ssp&ModelNumber=" + sModel + "&PartNumber=" + software.KitPartNumber + "'," + "'" + x.ToString() + "');"" class=""tableHeaderLink2""><img class=""tableHeaderLink2"" src=""images/sp_int_add2Cart_btn.gif"" border=""0"" alt=""Add to cart""></a>"
                    addToCart3.Text = "<a href=""#"" onclick=""javascript:selectSoftwareItemWithLoggedIn('" + x.ToString() + "',1,'" + software.KitPartNumber + "');"" class=""tableHeaderLink2""><img class=""tableHeaderLink2"" src=""images/sp_int_add2Cart_btn.gif"" border=""0"" alt=""Add to cart""></a>"
                Else
                    addToCart2.Text = "<a href=""#"" onclick=""javascript:selectSoftwareItemWithoutLoggedIn('NotLoggedInAddToCart.aspx','400','400','post=ssm&ModelNumber=" + sModel + "&PartNumber=" + software.KitPartNumber + "'," + "'" + x.ToString() + "');"" class=""tableHeaderLink2""><img class=""tableHeaderLink2"" src=""images/sp_int_add2Cart_btn.gif"" border=""0"" alt=""Add to cart""></a>"
                    addToCart3.Text = "<a href=""#"" onclick=""javascript:selectSoftwareItemWithLoggedIn('" + x.ToString() + "',0,'" + software.KitPartNumber + "');"" class=""tableHeaderLink2""><img class=""tableHeaderLink2"" src=""images/sp_int_add2Cart_btn.gif"" border=""0"" alt=""Add to cart""></a>" '6994
                End If

                addToCart.Attributes.Add("onclick", "return AddToCart_onclick('" + software.KitPartNumber + "')")
                addToCart1.Text = "<a href=""sony-software-model.aspx?addtocart=" + x.ToString() + """><img name=""addcart1"" src=""images/sp_int_add2Cart_btn.gif"" border=""0"" alt=""add to cart"" onclick=""return AddToCart_onclick('" + software.KitPartNumber.ToString() + "')""/></a>"

                ' -- the donwload and ship checkboxs are only displayed if the software can be downloaded. 
                download.Visible = False
                ship.Visible = False
                download.GroupName = "deliverymethod" + x.ToString()
                ship.GroupName = "deliverymethod" + x.ToString()

                If software.Version IsNot Nothing Then upgradeVersion.Text = software.Version
                If software.KitPartNumber IsNot Nothing Then kitPartNumber.Text = software.KitPartNumber
                If software.KitDescription IsNot Nothing Then kitPartDescription.Text = software.KitDescription
                If software.ListPrice > 0.0R Then
                    hasPrice = True
                    listPrice.Text = String.Format("{0:c}", software.ListPrice)
                End If

                If software.FreeOfCharge Then
                    If software.RequiredKey = "No Key Required" Then
                        isFree = True
                    Else
                        isFreeReqKey = True
                    End If
                End If

                If HttpContextManager.Customer IsNot Nothing And accountType = enumAccountType.AccountHolder Then
                    isAccountHolder = True
                    yourPrice.Text = "<A class=""tableHeader"" href=""#"" onclick=""mywin=window.open('yourprice.aspx?softwareItem=" + x.ToString() + "','','" + CON_POPUP_FEATURES + "');"" ToolTip=""View Your Price"">" + Resources.Resource.el_YourPrice + "</A>"
                Else
                    yourPrice.Text = "<A class=""tableHeader"" href=""#"" onclick=""mywin=window.open('Availability.aspx?softwareItem=" + x.ToString() + "','','" + CON_POPUP_FEATURES + "');"" ToolTip=""View Availability"">" + Resources.Resource.el_Availability + "</A>"
                End If

                If (Not String.IsNullOrEmpty(software.DownloadFileName)) And software.DownloadFileName <> "NA" Then
                    'fileSize.Text = getFileSize(downloadPath + "/" + conSoftwareDownloadPath + "/" + software.DownloadFileName.ToString())
                    'If fileSize.Text.Trim() <> String.Empty Then 
                    hasFile = True
                Else
                    fileSize.Text = ""
                End If

                addToCart.CommandArgument = x.ToString()
                addToCart.ImageUrl = "images/sp_int_add2Cart_btn.gif"
                If (software.ReleaseNotesFileName IsNot Nothing) Then
                    releaseNoteslink.Text = BuildReleaseNotesData(software.ReleaseNotesFileName, Resources.Resource.el_ReleaseNotes, software)
                    releaseNotesIcon.Text = BuildReleaseNotesData(software.ReleaseNotesFileName, Nothing, software)
                    If releaseNoteslink.Text.Length = 0 Then releaseNoteslink.Text = "&nbsp;"
                    If releaseNotesIcon.Text.Length = 0 Then releaseNotesIcon.Text = "&nbsp;"
                ElseIf software.Type = Product.ProductType.ComputerBasedTraining Then
                    releaseNoteslink.Text = BuildReleaseNotesData("", "Product&nbsp;Details", software)
                    releaseNotesIcon.Text = BuildReleaseNotesData("", Nothing, software)
                    If releaseNoteslink.Text.Length = 0 Then releaseNoteslink.Text = "&nbsp;"
                    If releaseNotesIcon.Text.Length = 0 Then releaseNotesIcon.Text = "&nbsp;"
                End If

                ' -- Begin FIRST Row - Software item details
                ' -- Version
                td.Controls.Add(New LiteralControl(vbCrLf + "<table cellpadding=""0"" cellspacing=""0"" border=""0"">" + vbCrLf + "<tr height=""25"">" + vbCrLf))
                td.Controls.Add(New LiteralControl(vbTab + "<td class=""tableData"" style=""border: 1px solid #d5dee9; width: 80px; text-align: center;"">" + vbCrLf))
                td.Controls.Add(upgradeVersion)
                td.Controls.Add(New LiteralControl(vbTab + "</td>" + vbCrLf))
                ' -- Kit Part Number
                td.Controls.Add(New LiteralControl(vbTab + "<td class=""tableData"" style=""border: 1px solid #d5dee9; width: 80px; text-align: center;"">" + vbCrLf))
                td.Controls.Add(kitPartNumber)
                td.Controls.Add(New LiteralControl(vbTab + "</td>" + vbCrLf))
                ' -- Kit Part Description
                td.Controls.Add(New LiteralControl(vbTab + "<td class=""tableData"" style=""border: 1px solid #d5dee9; padding-left: 3px;"">" + vbCrLf))
                td.Controls.Add(kitPartDescription)
                td.Controls.Add(New LiteralControl(vbTab + "</td>" + vbCrLf))
                ' -- Release Notes Link
                td.Controls.Add(New LiteralControl(vbTab + "<td class=""tableData"" style=""border: 1px solid #d5dee9; width: 95px; text-align: center;"">" + vbCrLf))
                td.Controls.Add(releaseNoteslink)
                td.Controls.Add(New LiteralControl(vbTab + "</td>" + vbCrLf))
                ' -- Release Notes Icon
                td.Controls.Add(New LiteralControl(vbTab + "<td class=""tableData"" style=""border: 1px solid #d5dee9; width: 45px; text-align: center; vertical-align: middle;"">" + vbCrLf))
                td.Controls.Add(releaseNotesIcon)
                td.Controls.Add(New LiteralControl(vbTab + "</td>" + vbCrLf))
                ' -- End FIRST Row
                td.Controls.Add(New LiteralControl("</tr>" + vbCrLf))

                '-- Begin SECOND Row - Shows file size, download links, add to cart, price
                td.Controls.Add(New LiteralControl("<tr style=""height: 30px;"">" + vbCrLf))
                td.Controls.Add(New LiteralControl(vbTab + "<td colspan=""5"" bgcolor=""F2F5F8"">" + vbCrLf))
                td.Controls.Add(New LiteralControl(vbTab + vbTab + "<table width=""668"" border=""0"" cellspacing=""0"" cellpadding=""0"">" + vbCrLf))
                td.Controls.Add(New LiteralControl(vbTab + vbTab + vbTab + "<tr height=""25"">" + vbCrLf))
                td.Controls.Add(New LiteralControl(vbTab + vbTab + vbTab + vbTab + "<td><img src=""images/spacer.gif"" width=""30"" height=""4"" alt=""""></td>" + vbCrLf))

                'Show download file link?*
                'If ((blnFree And blnPrice And blnFile And (blnAccountHolder Or Not blnAccountHolder)) Or (blnFree And Not blnPrice And blnFile And blnAccountHolder) Or_
                ' (blnFree And Not blnPrice And blnFile And Not blnAccountHolder)) Then
                If (isFree And hasFile) Then
                    showDownloadLink = True
                End If

                'Show "Download" and "Ship" radio buttons?**
                'If ((Not blnFree And blnPrice And blnFile And (blnAccountHolder Or Not blnAccountHolder))) Then
                '    blnShowRedioBtn = True
                'End If

                'Show download file size?***
                'If ((blnFree And blnPrice And blnFile And (blnAccountHolder Or Not blnAccountHolder)) Or (blnFree And Not blnPrice And blnFile And blnAccountHolder) Or_
                ' (Not blnFree And blnPrice And blnFile And blnAccountHolder) Or (Not blnFree And blnPrice And blnFile And Not blnAccountHolder) Or_
                ' (blnFree And Not blnPrice And blnFile And Not blnAccountHolder) ) Then
                If hasFile Then
                    showTextFileSize = False
                    showFileSize = True
                End If

                'Show list price (SISPART.LISTPRICE)?****
                'If ((blnFree And blnPrice And blnFile And (blnAccountHolder Or Not blnAccountHolder)) Or (blnFree And blnPrice And Not blnFile And blnAccountHolder) Or_
                ' (Not blnFree And blnPrice And blnFile And blnAccountHolder) Or (blnFree And blnPrice And Not blnFile And Not blnAccountHolder) Or_
                ' (Not blnFree And blnPrice And blnFile And Not blnAccountHolder) Or (Not blnFree And blnPrice And Not blnFile And blnAccountHolder) Or_
                ' (Not blnFree And blnPrice And Not blnFile And Not blnAccountHolder) ) Then
                If hasPrice Then
                    showListPrice = True
                End If

                'Show "Availability" link?*****
                'If ((blnFree And blnPrice And blnFile And Not blnAccountHolder) Or (blnFree And blnPrice And Not blnFile And Not blnAccountHolder) Or_
                ' (Not blnFree And blnPrice And blnFile And Not blnAccountHolder) Or (Not blnFree And blnPrice And Not blnFile And Not blnAccountHolder)) Then
                If (hasPrice And Not isAccountHolder) Then
                    showAvailabilityLink = True
                End If

                'Show "Your Price" link?******
                'If ((blnFree And blnPrice And blnFile And blnAccountHolder) Or (blnFree And blnPrice And Not blnFile And blnAccountHolder) Or_
                ' (Not blnFree And blnPrice And blnFile And blnAccountHolder) Or (Not blnFree And blnPrice And Not blnFile And blnAccountHolder)) Then
                If (hasPrice And isAccountHolder) Then
                    showYourPriceLink = True
                End If

                'Show "Call 1-800-538-7550 for information"?*******
                'If ((Not blnFree And Not blnPrice And blnFile And blnAccountHolder) Or (Not blnFree And Not blnPrice And blnFile And Not blnAccountHolder) Or_
                ' (blnFree And Not blnPrice And Not blnFile And blnAccountHolder) Or (blnFree And Not blnPrice And Not blnFile And Not blnAccountHolder) Or_
                ' (Not blnFree And Not blnPrice And Not blnFile And blnAccountHolder) Or (Not blnFree And Not blnPrice And Not blnFile And Not blnAccountHolder)) Then
                If (Not hasPrice And (Not isFree Or (isFree And Not hasFile))) Then
                    showCallNumber = True
                End If

                'Display Size Text
                Dim fileSizeText As String = IIf(HttpContextManager.GlobalData.IsFrench, "Taille du Fichier:", "File Size:")
                If Not showTextFileSize Then
                    fileSizeText = "&nbsp;"
                    fileSize.Text = String.Empty
                End If
                td.Controls.Add(New LiteralControl("<td class=""tableHeader"">" + fileSizeText + "</td>" + vbCrLf))
                td.Controls.Add(New LiteralControl("<td class=""tableData"">&nbsp;"))
                td.Controls.Add(fileSize)
                td.Controls.Add(New LiteralControl("</td>" + vbCrLf))

                If showDownloadLink Then
                    td.Controls.Add(New LiteralControl(vbTab + vbTab + vbTab + vbTab + "<td class=""tableData""><img src=""images/spacer.gif"" width=""10"" height=""16"" alt=""""><a href=""sony-foc-software-tc.aspx?ln=" + x.ToString() + """>" + Resources.Resource.el_DownloadNowFreeOfCharge + "</a></td>" + vbCrLf))
                End If

                If isFreeReqKey Then
                    td.Controls.Add(New LiteralControl(vbTab + vbTab + vbTab + vbTab + "<td class=""tableData""><img src=""images/spacer.gif"" width=""10"" height=""16"" alt=""""><a href=""sony-foc-software-tc.aspx?ln=" + x.ToString() + """>Download Installer</a></td>" + vbCrLf))
                End If

                download.Visible = False
                ship.Visible = False

                'Display List Price
                If showListPrice Then
                    td.Controls.Add(New LiteralControl(vbTab + vbTab + vbTab + vbTab + "<td><img src=""images/spacer.gif"" width=""40"" height=""4"" alt=""""></td>" + "<td class=""tableHeader"" style=""white-space:nowrap; text-align: right;"">" + Resources.Resource.el_ListPrice + ":&nbsp;</td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl(vbTab + vbTab + vbTab + vbTab + "<td class=""tableData"" style=""white-space:nowrap;"">"))
                    td.Controls.Add(listPrice)
                    td.Controls.Add(New LiteralControl("</td>" + vbCrLf))
                End If

                'Display Availability or Your Price Link
                If showAvailabilityLink Then
                    td.Controls.Add(New LiteralControl(vbTab + vbTab + vbTab + vbTab + "<td class=""tableHeader"" style=""white-space:nowrap;"">"))
                    td.Controls.Add(yourPrice)
                    td.Controls.Add(New LiteralControl("</td>" + vbCrLf))
                ElseIf showYourPriceLink Then
                    td.Controls.Add(New LiteralControl(vbTab + vbTab + vbTab + vbTab + "<td class=""tableHeader"" style=""white-space:nowrap;"">"))
                    td.Controls.Add(yourPrice)
                    td.Controls.Add(New LiteralControl("</td>" + vbCrLf))
                End If

                'Display Call Number
                If showCallNumber Then
                    td.Controls.Add(New LiteralControl(vbTab + vbTab + vbTab + vbTab + "<td class=""tableData""><img src=""images/spacer.gif"" width=""10"" height=""14"" alt="""">&nbsp;Call 1-800-538-7550 for information.</td>" + vbCrLf))
                End If

                'Display Shopping Cart
                If hasPrice Then
                    td.Controls.Add(New LiteralControl(vbTab + vbTab + vbTab + vbTab + "<td><img src=""images/spacer.gif"" width=""10"" height=""4"" alt=""""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl(vbTab + vbTab + vbTab + vbTab + "<td width=""30"">"))
                    td.Controls.Add(IIf(HttpContextManager.Customer Is Nothing, addToCart2, addToCart3))
                    td.Controls.Add(New LiteralControl(vbTab + vbTab + vbTab + vbTab + "</td>" + vbCrLf))
                End If

                ' -- End of SECOND row and Software Item sub-table.
                td.Controls.Add(New LiteralControl(vbTab + vbTab + vbTab + vbTab + "<td><img src=""images/spacer.gif"" width=""10"" height=""4"" alt=""""></td>" + vbCrLf))
                td.Controls.Add(New LiteralControl(vbTab + vbTab + vbTab + "</tr>" + vbCrLf))
                td.Controls.Add(New LiteralControl(vbTab + vbTab + "</table>" + vbCrLf))
                td.Controls.Add(New LiteralControl(vbTab + "</td>" + vbCrLf))
                td.Controls.Add(New LiteralControl("</tr>" + vbCrLf + "</table>" + vbCrLf))

                tr.Controls.Add(td)
            Catch ex As Exception
                errorMessageLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try

            Return tr
        End Function

        '-- this method is a override used when no results are found.
        Private Function BuildSoftwareResultRow(ByVal totalResults As Integer) As TableRow
            Dim tr As New TableRow

            Dim td As New TableCell

            '-- no result for search - show search parameters
            td.Controls.Add(New LiteralControl(vbCrLf + "<table cellpadding=""0"" cellspacing=""0"" border=""0"">" + vbTab + "<tr>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td colspan=""9""><span class=""bodyCopy""><strong>No match found for</strong></td>"))
            td.Controls.Add(New LiteralControl("</tr>"))
            If Not String.IsNullOrEmpty(sProductCategory) Then
                td.Controls.Add(New LiteralControl("<tr>"))
                td.Controls.Add(New LiteralControl("<td colspan=""9""><span class=""bodyCopy""><strong>Product Category: </strong>" + sProductCategory.ToString() + "</span></td>" + vbCrLf))
                td.Controls.Add(New LiteralControl("</tr>" + vbCrLf))
            End If

            If Session("ModelNumber") IsNot Nothing Then
                td.Controls.Add(New LiteralControl("<tr>"))
                td.Controls.Add(New LiteralControl("<td colspan=""9""><span class=""bodyCopy""><strong>Model Number: </strong>" + Session("ModelNumber").ToString() + "</span></td>" + vbCrLf))
                td.Controls.Add(New LiteralControl("</tr>" + vbCrLf))
            End If

            If Not String.IsNullOrEmpty(sModelDescription) Then
                td.Controls.Add(New LiteralControl("<tr>"))
                td.Controls.Add(New LiteralControl("<td colspan=""9""><span class=""bodyCopy""><strong>Model Description: </strong>" + sModelDescription.ToString() + "</span></td>" + vbCrLf))
                td.Controls.Add(New LiteralControl("</tr>" + vbCrLf))
            End If
            td.Controls.Add(New LiteralControl("</table>" + vbCrLf))
            tr.Controls.Add(td)
            Return tr
        End Function

        Private Function BuildReleaseNotesData(ByRef fileName As String, ByRef description As String, ByVal SoftwareItem As Software) As String
            Dim returnValue As String = String.Empty
            If SoftwareItem.Type = Product.ProductType.ComputerBasedTraining Then
                returnValue = "<a href=""sony-training-class-details.aspx?CourseNumber=" + SoftwareItem.PartNumber + ",-1"">"
                If description IsNot Nothing Then
                    returnValue &= description & "</a>"
                Else
                    returnValue &= "<img name=""releaseNotes"" src=""images/sp_int_releaseNotes_btn.gif"" border=""0"" height=""13"" width=""18"" alt=""Product Details""/></a>"
                End If
            Else
                'If File.Exists(downloadPath + "/" + conReleaseNotesPath + "/" + fileName) Then
                If (Not String.IsNullOrEmpty(fileName)) Then
                    returnValue = $"<a href=""{downloadURL}/{conReleaseNotesPath}/{fileName}"" target=""_blank"">"
                    If Not description Is Nothing Then
                        returnValue = returnValue + description + "</a>"
                    Else
                        returnValue = returnValue + "<img name=""releaseNotes"" src=""images/sp_int_releaseNotes_btn.gif"" border=""0"" height=""13"" width=""18"" alt=""Release Notes""/></a>"
                    End If
                End If
            End If

            Return returnValue
        End Function
#End Region

#Region "Pagination"
        Private Sub CalcCurrentResultPage()
            Try
                currentResultPage = Int(startingResultItem / conMaxResultsPerPage) + IIf((startingResultItem Mod conMaxResultsPerPage) = 0, 0, 1)
                If currentResultPage > endingResultPage Then currentResultPage = endingResultPage
            Catch ex As Exception
                currentResultPage = 1
            End Try
        End Sub

        Private Sub CalcEndingResultPage()
            Try
                endingResultPage = Int(totalSearchResults / conMaxResultsPerPage) + IIf((totalSearchResults Mod conMaxResultsPerPage) = 0, 0, 1)
            Catch ex As Exception
                endingResultPage = 1
            End Try
        End Sub

        Private Sub CalcStaringResultItem(ByVal thisPage As enumResultPageRequested)
            Dim tempPageNumber As Integer = 1
            Dim tempCurrentPageNumber As Integer = 1
            CalcEndingResultPage()

            Try
                If txtPageNumber.Text.ToString().Trim() <> "" Or txtPageNumber2.Text.ToString().Trim() <> "" Then
                    If IsNumeric(txtPageNumber.Text.ToString()) Then
                        tempPageNumber = Integer.Parse(txtPageNumber.Text.ToString())
                    ElseIf IsNumeric(txtPageNumber2.Text.ToString()) Then
                        tempPageNumber = Integer.Parse(txtPageNumber2.Text.ToString())
                    End If
                    If tempPageNumber > endingResultPage Then tempPageNumber = endingResultPage
                End If

                If lblCurrentPage.Text.ToString().Trim() <> "" Then
                    If IsNumeric(lblCurrentPage.Text.ToString()) Then tempCurrentPageNumber = Integer.Parse(lblCurrentPage.Text.ToString())
                End If

                Select Case (thisPage)
                    Case enumResultPageRequested.FirstPage
                        startingResultItem = 1
                    Case enumResultPageRequested.LastPage
                        If endingResultPage > 1 Then
                            startingResultItem = (((endingResultPage - 1) * conMaxResultsPerPage) + 1)
                        Else
                            startingResultItem = 1
                        End If

                    Case enumResultPageRequested.NextPage
                        If (tempCurrentPageNumber + 1) > endingResultPage Then
                            startingResultItem = (((endingResultPage - 1) * conMaxResultsPerPage) + 1)
                        Else
                            startingResultItem = (((tempCurrentPageNumber) * conMaxResultsPerPage) + 1)
                        End If

                    Case enumResultPageRequested.PreviousPage
                        If tempCurrentPageNumber > 2 Then
                            startingResultItem = (((tempCurrentPageNumber - 2) * conMaxResultsPerPage) + 1)
                        Else
                            startingResultItem = 1
                        End If

                    Case enumResultPageRequested.UserEnteredPage
                        If tempPageNumber <= 1 Then
                            startingResultItem = 1
                        Else
                            startingResultItem = (((tempPageNumber - 1) * conMaxResultsPerPage) + 1)
                        End If

                    Case enumResultPageRequested.Current
                        startingResultItem = (((tempCurrentPageNumber - 1) * conMaxResultsPerPage) + 1)

                End Select

                CalcCurrentResultPage()

            Catch ex As Exception
                startingResultItem = 1
            End Try
        End Sub

        Private Sub UpdateResultPageNav()
            Try
                '2018-10-12 ASleight - Hide all pagination when there is only one page, to reduce screen clutter and confusion.
                If endingResultPage = 1 Then
                    tblPagerTop.Visible = False
                    tblPagerBottom.Visible = False
                    Exit Sub
                End If

                Dim hidePrevButtons As Boolean = False
                Dim hideNextButtons As Boolean = False

                PreviousLinkButton.Visible = True
                btnPreviousPage.Visible = True
                PreviousLinkButton2.Visible = True
                btnPreviousPage2.Visible = True
                NextLinkButton.Visible = True
                btnNextPage.Visible = True
                NextLinkButton2.Visible = True
                btnNextPage2.Visible = True

                Select Case resultPageRequested
                    Case enumResultPageRequested.FirstPage
                        hidePrevButtons = True
                        If endingResultPage = currentResultPage Then hideNextButtons = True
                        lblCurrentPage.Text = currentResultPage.ToString()
                        lblCurrentPage2.Text = currentResultPage.ToString()
                    Case enumResultPageRequested.LastPage
                        hideNextButtons = True
                        lblCurrentPage.Text = endingResultPage.ToString()
                        lblCurrentPage2.Text = endingResultPage.ToString()
                    Case enumResultPageRequested.NextPage
                        If endingResultPage = currentResultPage Then hideNextButtons = True

                        lblCurrentPage.Text = currentResultPage.ToString()
                        lblCurrentPage2.Text = currentResultPage.ToString()
                    Case enumResultPageRequested.PreviousPage
                        If currentResultPage = 1 Then hidePrevButtons = True
                        lblCurrentPage.Text = currentResultPage.ToString()
                        lblCurrentPage2.Text = currentResultPage.ToString()
                    Case enumResultPageRequested.UserEnteredPage, enumResultPageRequested.Current
                        If currentResultPage = 1 Then hidePrevButtons = True
                        If endingResultPage = currentResultPage Then hideNextButtons = True
                        lblCurrentPage.Text = currentResultPage.ToString()
                        lblCurrentPage2.Text = currentResultPage.ToString()
                    Case Else
                        lblCurrentPage.Text = "1"
                End Select

                If hidePrevButtons Then
                    PreviousLinkButton.Visible = False
                    btnPreviousPage.Visible = False
                    PreviousLinkButton2.Visible = False
                    btnPreviousPage2.Visible = False
                End If
                If hideNextButtons Then
                    NextLinkButton.Visible = False
                    btnNextPage.Visible = False
                    NextLinkButton2.Visible = False
                    btnNextPage2.Visible = False
                End If

                lblEndingPage.Text = endingResultPage.ToString()
                lblEndingPage2.Text = endingResultPage.ToString()
                txtPageNumber.Text = ""
                txtPageNumber2.Text = ""
            Catch ex As Exception
                errorMessageLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub
#End Region

#Region "File Info"
        Private Function GetFileSize(ByVal path As String) As String
            Dim fileSize As String = ""

            Try
                If File.Exists(path) Then
                    Dim thisFile As New FileInfo(path.ToString())

                    If thisFile.Length() >= MegaBytes Then
                        fileSize = (String.Format("{0:N}", (thisFile.Length / MegaBytes))).ToString() + " MB"
                    ElseIf thisFile.Length() > KiloBytes Then
                        fileSize = (String.Format("{0:N}", (thisFile.Length / KiloBytes))).ToString() + " KB"
                    Else
                        fileSize = thisFile.Length.ToString() + " Bytes"
                    End If
                End If

            Catch ex As Exception
                fileSize = "N/A"
            End Try

            Return fileSize.ToString()
        End Function


        Private Sub SetFileDownloadPaths()
            Try
                'Dim config_settings As Hashtable
                'Dim handler As New Sony.US.SIAMUtilities.ConfigHandler("SOFTWARE\SERVICESPLUS")
                'config_settings = handler.GetConfigSettings("//item[@name='Download']")
                'downloadURL = config_settings.Item("url").ToString()
                'downloadPath = config_settings.Item("physical").ToString()
                'conReleaseNotesPath = config_settings.Item("releasenotes").ToString()
                'conSoftwareDownloadPath = config_settings.Item("downloadfiles").ToString()
                downloadURL = Sony.US.SIAMUtilities.ConfigurationData.Environment.Download.Url
                downloadPath = Sony.US.SIAMUtilities.ConfigurationData.Environment.Download.Physical
                conReleaseNotesPath = Sony.US.SIAMUtilities.ConfigurationData.Environment.Download.ReleaseNotes
                conSoftwareDownloadPath = Sony.US.SIAMUtilities.ConfigurationData.Environment.Download.DownloadFiles

            Catch ex As Exception
                errorMessageLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub
#End Region

#Region "software collection sorting"
        Private Sub SetSortOrder(ByVal callingControl As String)

            Try
                Select Case (callingControl)
                    Case lnkKitPart1.ID.ToString() ', lnkKitPart2.ID.ToString()
                        Me.imgVersion.Visible = False
                        Me.imgKitPartSort.Visible = True

                        If lnkKitPart1.CommandArgument.ToString() = enumSortOrder.KitPartNumberASC.ToString() Then  '  Or lnkKitPart2.CommandArgument.ToString() = enumSortOrder.KitPartNumberASC.ToString()
                            sortOrder = enumSortOrder.KitPartNumberASC
                            lnkKitPart1.CommandArgument = enumSortOrder.KitPartNumberDESC.ToString()
                            'lnkKitPart2.CommandArgument = enumSortOrder.KitPartNumberDESC.ToString()
                            Me.imgKitPartSort.Src = "images/arrow_up.gif"
                            Session("SortOrder") = "KitPartNumberASC"
                        Else
                            sortOrder = enumSortOrder.KitPartNumberDESC
                            lnkKitPart1.CommandArgument = enumSortOrder.KitPartNumberASC.ToString()
                            'lnkKitPart2.CommandArgument = enumSortOrder.KitPartNumberASC.ToString()
                            Me.imgKitPartSort.Src = "images/arrow_down.gif"
                            Session("SortOrder") = "KitPartNumberDESC"
                        End If

                        btnSearch.CommandArgument = enumSortBy.KitPartNumber.ToString()

                    Case lnkVersion.ID.ToString()
                        Me.imgVersion.Visible = True
                        Me.imgKitPartSort.Visible = False

                        If lnkVersion.CommandArgument.ToString() = enumSortOrder.VersionASC.ToString() Then
                            sortOrder = enumSortOrder.VersionASC
                            lnkVersion.CommandArgument = enumSortOrder.VersionDESC.ToString()
                            Me.imgVersion.Src = "images/arrow_up.gif"
                            Session("SortOrder") = "VersionASC"
                        Else
                            sortOrder = enumSortOrder.VersionDESC
                            lnkVersion.CommandArgument = enumSortOrder.VersionASC.ToString()
                            Me.imgVersion.Src = "images/arrow_down.gif"
                            Session.Remove("SortOrder")
                        End If

                        btnSearch.CommandArgument = enumSortBy.Version.ToString()

                        '' sorting is done in the stored proc, disable sorting
                    Case btnGoToPage.ID.ToString(), btnNextPage.ID.ToString(), btnPreviousPage.ID.ToString(), NextLinkButton.ID.ToString(), PreviousLinkButton.ID.ToString(), btnGoToPage2.ID.ToString(), btnNextPage2.ID.ToString(), btnPreviousPage2.ID.ToString(), NextLinkButton2.ID.ToString(), PreviousLinkButton2.ID.ToString(), conAddToCart.ToString()
                        If btnSearch.CommandArgument = enumSortBy.KitPartNumber.ToString() Then
                            Me.imgKitPartSort.Visible = True
                            Me.imgVersion.Visible = False
                            If lnkKitPart1.CommandArgument.ToString() = enumSortOrder.KitPartNumberASC.ToString() Then   ' Or lnkKitPart2.CommandArgument.ToString() = enumSortOrder.KitPartNumberASC.ToString()
                                sortOrder = enumSortOrder.KitPartNumberDESC
                                Me.imgKitPartSort.Src = "images/arrow_down.gif"
                                Session("SortOrder") = "KitPartNumberDESC"
                            Else
                                sortOrder = enumSortOrder.KitPartNumberASC
                                Me.imgKitPartSort.Src = "images/arrow_up.gif"
                                Session("SortOrder") = "KitPartNumberASC"
                            End If
                            btnSearch.CommandArgument = enumSortBy.KitPartNumber.ToString()

                        ElseIf btnSearch.CommandArgument = enumSortBy.Version.ToString() Then
                            Me.imgKitPartSort.Visible = False
                            Me.imgVersion.Visible = True
                            If lnkVersion.CommandArgument.ToString() = enumSortOrder.VersionASC.ToString() Then
                                sortOrder = enumSortOrder.VersionDESC
                                Me.imgVersion.Src = "images/arrow_down.gif"
                                Session.Remove("SortOrder")
                            Else
                                sortOrder = enumSortOrder.VersionASC
                                Me.imgVersion.Src = "images/arrow_up.gif"
                                Session("SortOrder") = "VersionASC"
                            End If
                            btnSearch.CommandArgument = enumSortBy.Version.ToString()
                        End If

                    Case Page.ID.ToString()
                        If Session("SortOrder") IsNot Nothing Then
                            Dim sortBy As String = Session("SortOrder").ToString()
                            Select Case (sortBy)
                                Case "KitPartNumberASC"
                                    sortOrder = enumSortOrder.KitPartNumberASC
                                    lnkKitPart1.CommandArgument = enumSortOrder.KitPartNumberDESC.ToString()
                                    'lnkKitPart2.CommandArgument = enumSortOrder.KitPartNumberDESC.ToString()
                                    Me.imgKitPartSort.Src = "images/arrow_up.gif"
                                    Me.imgVersion.Visible = False
                                    Me.imgKitPartSort.Visible = True

                                Case "KitPartNumberDESC'"
                                    sortOrder = enumSortOrder.KitPartNumberDESC
                                    lnkKitPart1.CommandArgument = enumSortOrder.KitPartNumberASC.ToString()
                                    'lnkKitPart2.CommandArgument = enumSortOrder.KitPartNumberASC.ToString()
                                    Me.imgKitPartSort.Src = "images/arrow_down.gif"
                                    Me.imgVersion.Visible = False
                                    Me.imgKitPartSort.Visible = True

                                Case "VersionASC"
                                    sortOrder = enumSortOrder.VersionASC
                                    lnkVersion.CommandArgument = enumSortOrder.VersionDESC.ToString()
                                    Me.imgVersion.Src = "images/arrow_up.gif"
                                    Me.imgVersion.Visible = True
                                    Me.imgKitPartSort.Visible = False
                            End Select
                        End If

                End Select
            Catch ex As Exception
                errorMessageLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try

        End Sub
#End Region

#Region "add to cart"
        Private Sub ProcessAddToCartRequest()
            Try
                If Not Request.QueryString("addtocart") Is Nothing And HttpContextManager.Customer IsNot Nothing And Not Session.Item("software") Is Nothing Then
                    Dim itemNumber As Integer = Integer.Parse(Request.QueryString("addtocart"))
                    Dim software As Software = CType(Session.Item("software"), ArrayList)(itemNumber)
                    sModel = software.ModelNumber
                    sPartnumber = software.PartNumber

                    '-- make sure we have a price for item 
                    If software.ListPrice > 0 Then
                        Dim deliveryMethod As ShoppingCartItem.DeliveryMethod
                        Dim customer As Customer = HttpContextManager.Customer
                        If software.Type = Product.ProductType.Certificate Or software.Type = Product.ProductType.ClassRoomTraining Or software.Type = Product.ProductType.OnlineTraining Then
                            deliveryMethod = ShoppingCartItem.DeliveryMethod.Download
                            'AddToCart(customer, software, deliveryMethod, Me.errorMessageLabel)
                            AddToCart(software, customer, errorMessageLabel)
                        Else
                            If software.BillingOnly Then
                                deliveryMethod = ShoppingCartItem.DeliveryMethod.Download
                            Else
                                deliveryMethod = ShoppingCartItem.DeliveryMethod.Ship
                            End If
                            'AddToCart(customer, software, deliveryMethod, Me.errorMessageLabel)
                            AddToCart(software, customer, errorMessageLabel)
                            'If didCustomerSelectDeliveryMethod(software, itemNumber) Then
                            '    If software.FreeOfCharge = software.FreeOfChargeType.NotFree Then
                            '        deliveryMethod = getDeliveryMethod(itemNumber)
                            '    Else
                            '        deliveryMethod = ShoppingCartItem.DeliveryMethod.Ship 'if free download and still click add to cart, means want to ship
                            '    End If
                            '    AddToCart(customer, software, deliveryMethod, Me.errorMessageLabel)
                            'Else
                            '    '-- if no delivery option has been selected and software is downloadable then display message to customer.                 
                            '    errorMessageLabel.Text = "Please select a delivery method (download, ship) before adding to cart."
                            'End If
                        End If
                    Else
                        '-- not price display message to user on what to do.
                        'Modified by prasad for 2517
                        errorMessageLabel.Text = "Requested software not available for shipment. Please contact ServicesPLUS support at 1-800-538-7550 and request status on release availability."
                    End If
                End If
            Catch ex As Exception
                errorMessageLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

#End Region

        '-- helper functions 
#Region "Helpers"

        Private Sub FormFieldInitialization()
            Page.ID = Request.Url.AbsolutePath.ToString()
            txtPageNumber.Attributes("onkeydown") = "SetTheFocusButton(event, 'btnGoToPage')"
            txtPageNumber2.Attributes("onkeydown") = "SetTheFocusButton(event, 'btnGoToPage2')"
            errorMessageLabel.Text = ""
        End Sub

        Private Function GetDeliveryMethod(ByVal itemNumber As Integer) As ShoppingCartItem.DeliveryMethod
            Dim returnValue As ShoppingCartItem.DeliveryMethod = ShoppingCartItem.DeliveryMethod.Ship
            Dim willCustomerDownload As Boolean = False
            Dim willCustomerBeShipped As Boolean = False

            Try

                'If Not Request.Form("deliverymethod" + itemNumber.ToString()) Is Nothing Then
                '-- is the download selected.
                'If Request.Form("deliverymethod" + itemNumber.ToString()) = ("download" + itemNumber.ToString()) Then
                If Request.QueryString("Download") = "Y" Then
                    willCustomerDownload = True
                End If
                'If Request.Form("deliverymethod" + itemNumber.ToString()) = ("ship" + itemNumber.ToString()) Then
                If Request.QueryString("Ship") = "Y" Then
                    willCustomerBeShipped = True
                End If
                'End If

                If willCustomerDownload And willCustomerBeShipped Then
                    returnValue = ShoppingCartItem.DeliveryMethod.DownloadAndShip
                Else
                    If willCustomerDownload And Not willCustomerBeShipped Then
                        returnValue = ShoppingCartItem.DeliveryMethod.Download
                    ElseIf Not willCustomerDownload And Not willCustomerBeShipped Then
                        returnValue = Nothing
                    End If
                End If
            Catch ex As Exception
                errorMessageLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try

            Return returnValue
        End Function

        Private Function DidCustomerSelectDeliveryMethod(ByRef thisSoftware As Software, ByVal itemNumber As Integer) As Boolean
            Dim returnValue As Boolean = True
            Try
                If thisSoftware IsNot Nothing Then
                    Dim willCustomerDownload As Boolean = False
                    Dim willCustomerBeShipped As Boolean = False

                    '-- does the file exist for download.         
                    If thisSoftware.Type = Product.ProductType.Certificate Or thisSoftware.Type = Product.ProductType.ClassRoomTraining Or thisSoftware.Type = Product.ProductType.OnlineTraining Then
                        willCustomerDownload = True
                        If Not willCustomerDownload And Not willCustomerBeShipped Then returnValue = False
                    Else
                        If (String.IsNullOrEmpty(thisSoftware.DownloadFileName.ToString())) Then
                            If Not thisSoftware.FreeOfCharge Then
                                '-- is the download selected.
                                If Request.QueryString("Download") = "Y" Then
                                    willCustomerDownload = True
                                End If
                                If Request.QueryString("Ship") = "Y" Then
                                    willCustomerBeShipped = True
                                End If
                            Else 'if free download, and still click on add cart, means want to shipped
                                willCustomerBeShipped = True
                            End If
                            If Not willCustomerDownload And Not willCustomerBeShipped Then returnValue = False
                        End If
                    End If
                Else
                    errorMessageLabel.Text = "Error getting delivery method. Software does not exist."
                End If
            Catch ex As Exception
                errorMessageLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
            Return returnValue
        End Function

#End Region

#End Region

        <WebMethod(EnableSession:=True)>
        Public Shared Function addPartToCartwSSoftware(ByVal sPartNumber As String, ByVal sPartQuantity As String) As PartresultSoftware '6994
            Dim objPCILogger As New PCILogger() '6524
            Dim sPartNumberUpperCase As String = sPartNumber.ToUpper()
            Dim objResult As New PartresultSoftware '6994
            'Dim objUtilties As Utilities = New Utilities
            Dim message As String
            Dim carts As ShoppingCart()
            Dim customer As New Customer
            Dim lmessage As New Label
            Dim objSSL As New SSL
            Dim sm As New ShoppingCartManager
            Dim cart As ShoppingCart
            Dim differentCart As Boolean = False
            Dim cm As CourseManager = Nothing
            ''Output1 value return form SAP helpler
            Dim output1 As String = String.Empty

            If HttpContextManager.Customer IsNot Nothing Then
                customer = HttpContextManager.Customer
            End If
            carts = objSSL.getAllShoppingCarts(customer, lmessage)
            message = lmessage.Text
            objResult.success = False
            objResult.partnumber = sPartNumberUpperCase
            objResult.Quantity = 1 '6994

            Try
                cart = sm.AddProductToCartQuickCart(carts, sPartNumberUpperCase, sPartQuantity, False, output1)
                Dim bProperpart As Boolean = True
                If Not objSSL.IsCurrentCartSameType(cart) Then
                    differentCart = True
                    objResult.differentCart = True
                End If

                For Each i As ShoppingCartItem In cart.ShoppingCartItems
                    If Not i.Product.PartNumber.Equals(sPartNumberUpperCase) Then
                        HttpContext.Current.Session("carts") = cart
                        objResult.items = cart.ShoppingCartItems.Length
                        objResult.totalAmount = String.Format("{0:c}", cart.ShoppingCartTotalPrice)
                        objResult.success = 2
                        message = "Replacement item " + i.Product.PartNumber + " added to cart in place of requested item  " + sPartNumberUpperCase + " which is no longer available."
                        objResult.message = message
                    Else
                        HttpContext.Current.Session("carts") = cart
                        objResult.items = cart.ShoppingCartItems.Length
                        objResult.totalAmount = String.Format("{0:c}", cart.ShoppingCartTotalPrice)
                        objResult.success = 1
                        message = "Item " + sPartNumberUpperCase + " added to cart."
                        objResult.message = message
                    End If
                Next

            Catch exArgumentalExceptio As ArgumentException
                If exArgumentalExceptio.ParamName = "SomePartNotFound" Then
                    message = exArgumentalExceptio.Message.Replace("Parameter name: SomePartNotFound", "")
                ElseIf exArgumentalExceptio.ParamName = "PartNotFound" Then
                    message = exArgumentalExceptio.Message.Replace("Parameter name: PartNotFound", "")
                ElseIf exArgumentalExceptio.ParamName = "BillingOnlyItem" Then
                    message = exArgumentalExceptio.Message.Replace("Parameter name: BillingOnlyItem", "")
                End If
            Catch ex As Exception
                '6524 V11 starts
                objPCILogger.EventOriginApplication = "ServicesPLUS"
                'objPCILogger.EventOriginApplicationLocation = Me.Page.GetType().Name
                objPCILogger.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
                objPCILogger.OperationalUser = Environment.UserName
                objPCILogger.EventDateTime = Date.Now.ToLongTimeString()
                objPCILogger.HTTPRequestObjectValues = objPCILogger.GetRequestContextValue()
                objPCILogger.CustomerID = customer.CustomerID
                objPCILogger.CustomerID_SequenceNumber = customer.SequenceNumber
                'objPCILogger.UserName = customer.UserName
                objPCILogger.EmailAddress = customer.EmailAddress '6524
                objPCILogger.SIAM_ID = customer.SIAMIdentity
                objPCILogger.LDAP_ID = customer.LdapID '6524
                objPCILogger.EventOriginMethod = "addPartToCartwSSoftware"
                objPCILogger.EventType = EventType.View
                '6524 V11 ends
                objPCILogger.IndicationSuccessFailure = "Failure" '6524
                objPCILogger.Message = "addPartToCart Failed. " & ex.Message.ToString() '6524
                objPCILogger.PushLogToMSMQ() '6524'6524 V11
                '7813 starts
                'message = Utilities.WrapExceptionforUI(ex)'7813
                Dim strUniqueKey As String = Utilities.GetUniqueKey
                message = Utilities.WrapExceptionforUINumber(ex, strUniqueKey)
                message = message.Remove(message.Length - 1)
                message = message + strUniqueKey + "."
                '7813 ends
            Finally
                'objPCILogger.PushLogToMSMQ() '6524
            End Try
            objResult.message = message
            Return objResult
        End Function
    End Class

    Public Class PartresultSoftware '6994
        Public _message As String
        Public _totalAmount As String
        Public _items As String
        Public _success As Integer
        Public _quantity As String
        Public _partnumber As String
        Public _differentCart As Boolean

        Property partnumber() As String
            Get
                Return _partnumber
            End Get
            Set(ByVal value As String)
                _partnumber = value
            End Set
        End Property
        Property message() As String
            Get
                Return _message
            End Get
            Set(ByVal value As String)
                _message = value
            End Set
        End Property
        Property Quantity() As String
            Get
                Return _quantity
            End Get
            Set(ByVal value As String)
                _quantity = value
            End Set
        End Property
        ''' <summary>
        ''' 0-Failure
        ''' 1-Success
        ''' 2-warning
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Property success() As Integer
            Get
                Return _success
            End Get
            Set(ByVal value As Integer)
                _success = value
            End Set
        End Property
        Property totalAmount() As String
            Get
                Return _totalAmount
            End Get
            Set(ByVal value As String)
                _totalAmount = value
            End Set
        End Property
        Property items() As String
            Get
                Return _items
            End Get
            Set(ByVal value As String)
                _items = value
            End Set
        End Property

        Property differentCart() As Boolean
            Get
                Return _differentCart
            End Get
            Set(ByVal value As Boolean)
                _differentCart = value
            End Set
        End Property

    End Class

End Namespace
