<%@ Page Language="vb" AutoEventWireup="false" SmartNavigation="true" Inherits="ServicePLUSWebApp.PunchOutSesionExpire"  EnableViewStateMac="true" CodeFile="PunchOutSesionExpire.aspx.vb" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="SonyFooter" Src="SonyFooter.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="TopSonyHeader" Src="TopSonyHeader.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="ServicePlusNav" Src="ServicePlusNav.ascx" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>ServicesPLUS promotions for Sony parts and software</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<meta content="Sony Parts, Sony Professional Parts, Sony Broadcast Parts, Sony Business Parts, Sony Software, Sony Professional Software, Sony Broadcast Software, Sony Business Software"
			name="keywords">
		<LINK href="includes/ServicesPLUS_style.css" type="text/css" rel="stylesheet">
			<script language="javascript" src="includes/ServicesPLUS.js" type="text/javascript"></script>
			<script type="text/javascript" src="includes/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="includes/svcplus_globalization.js"></script>
	</HEAD>
	<body text="#000000" bgColor="#5d7180" leftMargin="0" topMargin="0">
	<center>
		<form id="Form1" method="post" runat="server">
			<table  width="710" border="0">
					<tr>
						<td width="25" background="images/sp_left_bkgd.gif"><img height="25" src="images/spacer.gif" width="25"></td>
						<td bgColor="#ffffff" width= "689">
							<table width="710" border="0">
								<tr>
									<td width="582"><ServicePLUSWebApp:TopSonyHeader ID="TopSonyHeader" runat="server" /></td>
								</tr>
								<tr>
									<td height="23" width="582"><nav><SERVICEPLUSWEBAPP:ServicePlusNav ID="SonyNav" runat="server" /></nav></td>
								</tr>
								<tr>
									<td width="582">
										<table width="710" border="0">
											<tr>
												<td width="464" bgColor="#363d45" background="images/sp_int_header_top_ServicesPLUS_onepix.gif" height="57" align="right" valign="top"><br/>
													<h1 class="headerText">ServicesPLUS&nbsp;&nbsp;</h1></td>
												<td vAlign="top" width="238" bgColor="#363d45">
													<table width="246" border="0">
														<tr>
															<td colSpan="3"><img height="24" src="images/spacer.gif" width="246"></td>
														</tr>
														<tr>
															<td width="5">&nbsp;</td>
															<td width="236"></td>
															<td width="5">&nbsp;</td>
														</tr>
													</table>
												</td>
											</tr>
											<tr>
												<td bgColor="#f2f5f8">
													<table width="464" border="0">
														<tr>
															<td width="20"><img height="20" src="images/spacer.gif" width="20"></td>
															<td><h2 class="headerTitle">PunchOut Error&nbsp;&nbsp;</h2></td>
														</tr>
													</table>
												</td>
												<td width="238" bgColor="#99a8b5">&nbsp;</td>
											</tr>
											<tr height="9">
												<td bgColor="#f2f5f8"><img height="9" src="images/sp_int_header_btm_left_onepix.gif" width="464"></td>
												<td width="238" bgColor="#99a8b5"><img height="9" src="images/sp_int_header_btm_right.gif" width="246"></td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td style="width: 100%" >
										<table  width="100%" height=200  border="0">
											<tr>
												<td><img height="20" src="images/spacer.gif" width="20"></td>
												<td width="100%" valign="top">
												    <img height="20" src="images/spacer.gif" width="20"><br />
												        <span class="bodyCopyEPurchaseBold">Your punchout session has been expired. Please use your purchasing system to start a new session.</span>
</td>
												<td><img height="20" src="images/spacer.gif" width="20"></td>
											</tr>
											
										</table>
									</td>
								</tr>
								<tr>
									
									<td style="width: 582px"><footer><ServicePLUSWebApp:SonyFooter ID="SonyFooter1" runat="server" /></footer></td>
								</tr>
							</table>
						</td>
						<td background="images/sp_right_bkgd.gif" width="25"><img height="20" src="images/spacer.gif" width="25"></td>
					</tr>
				</table>
		    </form>
	    </center>
	</body>
</HTML>
