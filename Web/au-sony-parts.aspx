<%@ Reference Page="~/SSL.aspx" %>

<%@ Page Language="vb" AutoEventWireup="false" Inherits="ServicePLUSWebApp.PartsPLUS"
    EnableViewStateMac="true" CodeFile="au-sony-parts.aspx.vb" CodeFileBaseClass="ServicePLUSWebApp.SSL" %>

<%@ Register TagPrefix="ServicePLUSWebApp" TagName="SonyFooter" Src="SonyFooter.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="PersonalMessage" Src="PersonalMessage.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="ServicePlusNav" Src="ServicePlusNav.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="TopSonyHeader" Src="TopSonyHeader.ascx" %>
<%@ Register Src="~/UserControl/SPSPromotion.ascx" TagName="SPSPromotion" TagPrefix="uc1" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="<%=PageLanguage %>">
<head>
    <title>
        <%=Resources.Resource.ttl_Parts %></title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <meta content="Sony Parts, Sony Professional Parts, Sony Broadcast Parts, Sony Business Parts, Sony Software, Sony Professional Software, Sony Broadcast Software, Sony Business Software"
        name="keywords" />
    <link href="themes/base/jquery.ui.all.css" rel="stylesheet" type="text/css" />
    <link href="includes/ServicesPLUS_style.css" type="text/css" rel="stylesheet" />
    <link href="includes/jquery-ui.min.css" rel="stylesheet" type="text/css" />
    <link href="includes/jquery-ui.structure.min.css" rel="stylesheet" type="text/css" />

    <script src="includes/jquery-3.1.1.min.js" type="text/javascript"></script>
    <script src="includes/jquery-ui.min.js" type="text/javascript"></script>
    <script src="includes/ServicesPLUS.js" type="text/javascript"></script>
    <script src="includes/svcplus_globalization.js" type="text/javascript"></script>
    <script src="includes/ShoppingCart.js" type="text/javascript"></script>

    <script type="text/javascript">
        //Model  PopUp window
        function OrderLookUp(cnt, models) {
            var lstModels = new Array(models.split(','));
            $("#divMessage").text('');
            var buttons = $('.ui-dialog-buttonpane').children('button');
            buttons.remove();
            $("#divMessage").append(' <br />');
            $("divMessage").dialog("destroy");
            $("#divMessage").dialog({ // dialog box
                title: '<%=Resources.Resource.el_Pts_ModelRenamed %>',
                height: 260,
                width: 560,
                modal: true,
                position: 'top',
                close: function () {
                    redirectTo();
                }

            });
            var content = "<span class='modalpopup-content'><%=Resources.Resource.el_Pts_modelConflict %><br /><%=Resources.Resource.el_Pts_modelConflict2 %></span>";
            content += "<br />" + "<table class='modalpopup-content' style='margin-left:100px;'>";
            content += '<tr><td width="100px"><u><%=Resources.Resource.el_Pts_OldModelName %></u></td><td width="100px"><u><%=Resources.Resource.el_Pts_NewModelName %></u></td></tr>';
            for (i = 0; i < cnt; i++) {
                var lstmodel = new Array(lstModels[0][i].split(':'));
                content += '<tr><td>' + lstmodel[0][0] + '</td><td>' + lstmodel[0][1] + '</td></tr>';
            }
            content += "</table>";
            $("#divMessage").append(content); //message to display in divmessage div
            $("#divMessage").append('<br /> <br /><a ><img src ="<%=Resources.Resource.img_btnCloseWindow %>" alt ="Close this popup box." onclick="javascript:return ClosePopUp();" id="messagego" /></a>');
            return false;
        }

        function redirectTo() {
            var url = window.location.href;
            var finalSlashLength = url.substring(url.lastIndexOf('/') + 1).length;
            var newUrl = url.substring(0, url.length - finalSlashLength);
            newUrl += "PartsPLUSResults.aspx?stype=models";
            window.location.href = newUrl;
            return true;
        }
    </script>
</head>
<body style="margin: 0px; background: #5d7180; color: Black;">
    <div id="ValidationMessageDiv"></div>
    <div id="dialog" title="<%=Resources.Resource.atc_title_QuickOrderStatus %>">
        <div id="divPrg" style="font-size: 11px; font-family: Arial; overflow: auto;"></div>
        <div id="divMessage" style="font-size: 11px; font-family: Arial; overflow: auto;"></div>
        <div id="divException" style="font-family: Arial, Helvetica, sans-serif; font-size: 11px; line-height: 14px; overflow: auto; display: none;">
        </div>
        <div id="divCloseButton" style="display: none;">
            <br />
            <a href="#">
                <img src="<%=Resources.Resource.img_btnCloseWindow %>" alt="Close" onclick="javascript:return closedilog();"
                    id="messageclose" /></a>
        </div>
    </div>
    <center>
        <form id="Form1" method="post" runat="server">
        <%-- Hidden Fields are for holding onto Resource strings so that the external .js files can use them. --%>
        <asp:HiddenField ID="atc_ex_InvalidPartNumber" runat="server" Value="<%$ Resources:Resource, atc_ex_InvalidPartNumber %>" />
        <asp:HiddenField ID="atc_ex_InvalidQuantityForPart" runat="server" Value="<%$ Resources:Resource, atc_ex_InvalidQuantityForPart %>" />
        <asp:HiddenField ID="atc_ex_InvalidPartAndQuantity" runat="server" Value="<%$ Resources:Resource, atc_ex_InvalidPartAndQuantity %>" />
        <asp:HiddenField ID="atc_ex_DifferentCart" runat="server" Value="<%$ Resources:Resource, atc_ex_DifferentCart %>" />
        <asp:HiddenField ID="atc_title_OrderStatus" runat="server" Value="<%$ Resources:Resource, atc_title_OrderStatus %>" />
        <asp:HiddenField ID="atc_title_OrderExceptions" runat="server" Value="<%$ Resources:Resource, atc_title_OrderExceptions %>" />
        <asp:HiddenField ID="atc_title_QuickOrderStatus" runat="server" Value="<%$ Resources:Resource, atc_title_QuickOrderStatus %>" />
        <asp:HiddenField ID="atc_title_QuickOrderExceptions" runat="server" Value="<%$ Resources:Resource, atc_title_QuickOrderExceptions %>" />
        <asp:HiddenField ID="atc_AddingItem" runat="server" Value="<%$ Resources:Resource, atc_AddingItem %>" />
        <asp:HiddenField ID="atc_ToCart" runat="server" Value="<%$ Resources:Resource, atc_ToCart %>" />
        <asp:HiddenField ID="atc_warn_LongWait" runat="server" Value="<%$ Resources:Resource, atc_warn_LongWait %>" />
        <asp:HiddenField ID="prg_PleaseWait" runat="server" Value="<%$ Resources:Resource, prg_PleaseWait %>" />
        <asp:HiddenField ID="img_btnCloseWindow" runat="server" Value="<%$ Resources:Resource, img_btnCloseWindow %>" />
        <asp:HiddenField ID="quickOrderMaxItems" runat="server" Value="" />
        <asp:HiddenField ID="orderUploadMaxItems" runat="server" Value="" />

        <table width="760" border="0" role="presentation">
            <tr>
                <td width="25" style="background: url('images/sp_left_bkgd.gif')">
                    <img height="25" src="images/spacer.gif" width="25" alt="">
                </td>
                <td width="689" bgcolor="#ffffff">
                    <table width="710" border="0" role="presentation">
                        <tr>
                            <td>
                                <header><ServicePLUSWebApp:TopSonyHeader ID="TopSonyHeader" runat="server" /></header>
                            </td>
                        </tr>
                        <tr>
                            <td width="700">
                                <nav><ServicePLUSWebApp:ServicePlusNav ID="ServicePlusNavDisplay" runat="server" CallingPage="partsPlus" /></nav>
                            </td>
                        </tr>
                        <tr>
                            <td width="700">
                                <table width="710" border="0" role="presentation">
                                    <tbody>
                                        <tr>
                                            <td style="width: 464px; height: 82px; background: #363d45 url('images/Parts_Banner.JPG'); text-align: right; vertical-align: middle;">
                                                <h1 class="headerText"><%=Resources.Resource.el_Pts_MainHeading%></h1>
                                            </td>
                                            <td style="vertical-align: middle; text-align: center; background: #363d45;">
                                                <ServicePLUSWebApp:PersonalMessage ID="PersonalMessageDisplay" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top" width="418" bgcolor="#f2f5f8">
                                                <table style="height: 284px; width: 448px; border: none;" role="search">
                                                    <tr>
                                                        <td width="20">
                                                            <img height="20" src="images/spacer.gif" width="20" alt="">
                                                        </td>
                                                        <td width="427">
                                                            <h2 class="headerTitle"><%=Resources.Resource.el_Pts_SubHeading%></h2><br />
                                                            <img height="2" src="images/spacer.gif" width="20" alt="">
                                                            <table height="236" width="433" border="0" role="presentation">
                                                                <tr>
                                                                    <td valign="top" width="190" style="height: 236px">
                                                                        <table width="190" border="0" role="presentation">
                                                                            <tr>
                                                                                <td>
                                                                                    <h3 class="finderSubhead"><%=Resources.Resource.el_Pts_H1%></h3><br />
                                                                                    <span class="finderCopyDark"><%=Resources.Resource.el_Pts_P1%></span>
                                                                                </td>
                                                                            </tr>
                                                                            <tr style="height: 15px;">
                                                                                <td>
                                                                                    <asp:Label ID="Search1ErrorLabel" runat="server" CssClass="redAsterick" />
                                                                                </td>
                                                                            </tr>
                                                                            <tr style="height: 12px;">
                                                                                <td>
                                                                                    <label class="finderCopyDark" for="txtModelNumber"><%=Resources.Resource.el_ModelNumber%></label>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <SPS:SPSTextBox ID="txtModelNumber" runat="server" />
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <img height="5" src="images/spacer.gif" width="4" alt=""><br />
                                                                                    <asp:ImageButton ID="btnSearchModel" runat="server" ImageUrl="<%$ Resources:Resource,img_btnSearch%>"
                                                                                        AlternateText="Search Parts Finder" aria-label="Perform a Parts Finder search and view the results."
                                                                                        aria-role="search"/>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                    <td width="18" style="height: 236px">
                                                                        <img height="1" src="images/spacer.gif" width="9" alt="">
                                                                    </td>
                                                                    <td valign="top" width="190" style="height: 236px">
                                                                        <table width="190" border="0" role="presentation">
                                                                            <tr>
                                                                                <td>
                                                                                    <h3 class="finderSubhead"><%=Resources.Resource.el_Pts_Search%></h3><br />
                                                                                    <span class="finderCopyDark"><%=Resources.Resource.el_Pts_Search_p1%></span>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td height="15">
                                                                                    <asp:Label ID="Search2ErrorLabel" runat="server" CssClass="redAsterick" />
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <label class="finderCopyDark" for="txtPrefix"><%=Resources.Resource.el_Pts_Srch_Model%></label>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <SPS:SPSTextBox ID="txtPrefix" runat="server" CssClass="finderCopyDark" />
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <label class="finderCopyDark" for="txtPartNumber"><%=Resources.Resource.el_Pts_Srch_Part%></label>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <SPS:SPSTextBox ID="txtPartNumber" runat="server" CssClass="finderCopyDark" />
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <label class="finderCopyDark" for="txtDescription"><%=Resources.Resource.el_Pts_Srch_Desc%></label>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <SPS:SPSTextBox ID="txtDescription" runat="server" CssClass="finderCopyDark" />
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <img height="5" src="images/spacer.gif" width="4" alt=""><br />
                                                                                    <asp:ImageButton ID="btnSearchPart" runat="server" ImageUrl="<%$ Resources:Resource,img_btnSearch%>"
                                                                                        AlternateText="<%$ Resources:Resource, el_Pts_Srch%>" ToolTip="<%$ Resources:Resource, el_Pts_Srch%>"
                                                                                        aria-label="Perform a Parts Search and view the results." aria-role="search"/>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                    <td width="7" style="height: 236px">
                                                                        <img height="10" src="images/spacer.gif" width="10" alt="">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <img src="images/spacer.gif" width="20" alt="">
                                                                    </td>
                                                                    <td width="18">
                                                                        <img height="1" src="images/spacer.gif" width="9" alt="">
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <%-- QUICK ORDER SECTION --%>
                                            <td valign="top" width="174" bgcolor="#99a8b5">
                                                <img height="5" src="images/spacer.gif" width="4" alt=""><br />
                                                <table height="218" width="246" border="0" role="form">
                                                    <tr>
                                                        <td width="2">
                                                        </td>
                                                        <td colspan="4">
                                                            <h3 class="finderSubhead"><%=Resources.Resource.el_pts_QuickOrder%></h3><br />
                                                            <span class="finderCopyDark"><%=Resources.Resource.el_Pts_QuickInfo1%></span>
                                                        </td>
                                                    </tr>
                                                    <tr style="height: 4px;">
                                                        <td>
                                                        </td>
                                                        <td colspan="4">
                                                            <p><asp:Label ID="lblQuickOrderError" runat="server" CssClass="redAsterick" /></p>
                                                        </td>
                                                    </tr>
                                                    <tr style="height: 14px;">
                                                        <td>
                                                        </td>
                                                        <td width="90">
                                                            <asp:Label ID="lblPartOrKit" CssClass="finderCopyDark" runat="server"><%=Resources.Resource.el_Pts_PartsorKit%></asp:Label>
                                                        </td>
                                                        <td width="30">
                                                            <asp:Label ID="lblQuantity" CssClass="finderCopyDark" runat="server"><%=Resources.Resource.el_Pts_Qty%></asp:Label>
                                                        </td>
                                                        <td width="90">
                                                            <asp:Label ID="lblPartOrKit2" CssClass="finderCopyDark" runat="server"><%=Resources.Resource.el_Pts_PartsorKit%></asp:Label>
                                                        </td>
                                                        <td width="30">
                                                            <asp:Label ID="lblQuantity2" CssClass="finderCopyDark" runat="server"><%=Resources.Resource.el_Pts_Qty%></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr style="height: 24px;">
                                                        <td>
                                                        </td>
                                                        <td>
                                                            <SPS:SPSTextBox ID="PartNumber1" runat="server" CssClass="finderCopyDark"
                                                                MaxLength="18" Width="80" aria-labelledby="lblPartOrKit" />
                                                        </td>
                                                        <td>
                                                            <SPS:SPSTextBox ID="Qty1" runat="server" CssClass="finderCopyDark"
                                                                MaxLength="3" Width="24" aria-labelledby="lblQuantity" />
                                                        </td>
                                                        <td>
                                                            <SPS:SPSTextBox ID="PartNumber6" runat="server" CssClass="finderCopyDark"
                                                                MaxLength="18" Width="80" aria-labelledby="lblPartOrKit" />
                                                        </td>
                                                        <td>
                                                            <SPS:SPSTextBox ID="Qty6" runat="server" CssClass="finderCopyDark"
                                                                MaxLength="3" Width="24" aria-labelledby="lblQuantity" />
                                                        </td>
                                                    </tr>
                                                    <tr style="height: 24px;">
                                                        <td>
                                                        </td>
                                                        <td>
                                                            <SPS:SPSTextBox ID="PartNumber2" runat="server" CssClass="finderCopyDark"
                                                                MaxLength="18" Width="80" aria-labelledby="lblPartOrKit" />
                                                        </td>
                                                        <td>
                                                            <SPS:SPSTextBox ID="Qty2" runat="server" CssClass="finderCopyDark"
                                                                MaxLength="3" Width="24" aria-labelledby="lblQuantity" />
                                                        </td>
                                                        <td>
                                                            <SPS:SPSTextBox ID="PartNumber7" runat="server" CssClass="finderCopyDark"
                                                                MaxLength="18" Width="80" aria-labelledby="lblPartOrKit" />
                                                        </td>
                                                        <td>
                                                            <SPS:SPSTextBox ID="Qty7" runat="server" CssClass="finderCopyDark"
                                                                MaxLength="3" Width="24" aria-labelledby="lblQuantity" />
                                                        </td>
                                                    </tr>
                                                    <tr style="height: 24px;">
                                                        <td>
                                                        </td>
                                                        <td>
                                                            <SPS:SPSTextBox ID="PartNumber3" runat="server" CssClass="finderCopyDark"
                                                                MaxLength="18" Width="80" aria-labelledby="lblPartOrKit" />
                                                        </td>
                                                        <td>
                                                            <SPS:SPSTextBox ID="Qty3" runat="server" CssClass="finderCopyDark"
                                                                MaxLength="3" Width="24" aria-labelledby="lblQuantity" />
                                                        </td>
                                                        <td>
                                                            <SPS:SPSTextBox ID="PartNumber8" runat="server" CssClass="finderCopyDark"
                                                                MaxLength="18" Width="80" aria-labelledby="lblPartOrKit" />
                                                        </td>
                                                        <td>
                                                            <SPS:SPSTextBox ID="Qty8" runat="server" CssClass="finderCopyDark"
                                                                MaxLength="3" Width="24" aria-labelledby="lblQuantity" />
                                                        </td>
                                                    </tr>
                                                    <tr style="height: 24px;">
                                                        <td>
                                                        </td>
                                                        <td>
                                                            <SPS:SPSTextBox ID="PartNumber4" runat="server" CssClass="finderCopyDark"
                                                                MaxLength="18" Width="80" aria-labelledby="lblPartOrKit" />
                                                        </td>
                                                        <td>
                                                            <SPS:SPSTextBox ID="Qty4" runat="server" CssClass="finderCopyDark"
                                                                MaxLength="3" Width="24" aria-labelledby="lblQuantity" />
                                                        </td>
                                                        <td>
                                                            <SPS:SPSTextBox ID="PartNumber9" runat="server" CssClass="finderCopyDark"
                                                                MaxLength="18" Width="80" aria-labelledby="lblPartOrKit" />
                                                        </td>
                                                        <td>
                                                            <SPS:SPSTextBox ID="Qty9" runat="server" CssClass="finderCopyDark"
                                                                MaxLength="3" Width="24" aria-labelledby="lblQuantity" />
                                                        </td>
                                                    </tr>
                                                    <tr style="height: 24px;">
                                                        <td>
                                                        </td>
                                                        <td>
                                                            <SPS:SPSTextBox ID="PartNumber5" runat="server" CssClass="finderCopyDark"
                                                                MaxLength="13" Width="80" aria-labelledby="lblPartOrKit" />
                                                        </td>
                                                        <td>
                                                            <SPS:SPSTextBox ID="Qty5" runat="server" CssClass="finderCopyDark"
                                                                MaxLength="3" Width="24" aria-labelledby="lblQuantity" />
                                                        </td>
                                                        <td>
                                                            <SPS:SPSTextBox ID="PartNumber10" runat="server" CssClass="finderCopyDark"
                                                                MaxLength="18" Width="80" aria-labelledby="lblPartOrKit" />
                                                        </td>
                                                        <td>
                                                            <SPS:SPSTextBox ID="Qty10" runat="server" CssClass="finderCopyDark"
                                                                MaxLength="3" Width="24" aria-labelledby="lblQuantity" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                        <td colspan="4">
                                                            <%-- <img id='AddToCart' src='<%= Resources.Resource.sna_svc_img_26 %>' alt='Add to Cart'
                                                                onclick="return QuickOrder_OnClick()" /> Replace with ImageButton for WCAG --%>
                                                            <asp:ImageButton ID="btnAddToCart" ImageUrl="<%$Resources:Resource,sna_svc_img_26 %>" OnClientClick="return QuickOrder_OnClick();"
                                                                AlternateText="<%$Resources:Resource,el_AddCart %>" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <%-- FILE UPLOAD SECTION --%>
                                                    <tr>
                                                        <td class="uploadBgStyle">
                                                            &nbsp;
                                                        </td>
                                                        <td colspan="3" class="uploadBgStyle">
                                                            <span class="finderCopyDark" style="color: #222;">You may also submit an Excel spreadsheet file (.xls/.xlsx/.csv) for a Quick Order:</span>
                                                            <br />
                                                            <asp:FileUpload ID="fuQuickOrder" runat="server" CssClass="finderCopyDark" ForeColor="#222222" />
                                                            <!--accept="application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, text/csv"-->
                                                        </td>
                                                        <td class="uploadBgStyle" style="vertical-align: top">
                                                            <a href="javascript:window.open('OrderUploadHelp.aspx', '_blank','toolbar=0,width=404,height=382,centerscreen,menubar=no,scrollbar=no,status=no,location=no,titlebar=no');">
                                                                <img src="<%=Resources.Resource.img_btnHelp %>" alt="File Upload Help" style="margin-top: 5px" /></a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="uploadBgStyle">
                                                            &nbsp;
                                                        </td>
                                                        <td colspan="2" class="uploadBgStyle">
                                                            <asp:ImageButton ID="btnUpload" runat="server" ImageUrl="<%$Resources:Resource,img_btnFileUpload %>"
                                                                AlternateText="Upload File" Style="margin-left: -3px; margin-top: 5px" />
                                                        </td>
                                                        <td colspan="2" class="uploadBgStyle">
                                                            <a class="promoCopyBold" style="color: #222;" mimetype="application/octet-stream" href="Shared/sample_parts_template.csv">
                                                                Sample .csv file&nbsp;&nbsp;<img src="images/sp_int_releaseNotes_btn.gif" width="18"
                                                                    height="13" alt="Sample CSV" /></a>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr height="9">
                                            <td width="418" bgcolor="#f2f5f8">
                                                <img height="9" src="images/sp_int_header_btm_left_onepix.gif" width="464" alt="">
                                            </td>
                                            <td width="174" bgcolor="#99a8b5">
                                                <img height="9" src="images/sp_int_header_btm_right.gif" width="246" alt="">
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <asp:Label ID="ErrorLabel" runat="server" CssClass="redAsterick" />
                                <%--<asp:Label ID="NotFoundLabel" runat="server" CssClass="redAsterick" />--%>
                            </td>
                        </tr>
                        <tr>
                            <td width="700">
                                <table width="710" border="0" role="presentation">
                                    <tr>
                                        <td width="20" height="20">
                                            <img height="20" src="images/spacer.gif" width="20" alt="">
                                        </td>
                                        <td width="670" height="20">
                                            <img height="20" src="images/spacer.gif" width="670" alt="">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="10">
                                            <img height="10" src="images/spacer.gif" width="10" alt="">
                                        </td>
                                        <td colspan="2">
                                            <% If isAmerica Then %>
                                            <table border="0" role="presentation">
                                                <tr>
                                                    <td>
                                                        <uc1:SPSPromotion ID="Promotions1" DisplayPage="sony-parts" Type="2" runat="server" />
                                                    </td>
                                                    <td width="16">
                                                        <img src="images/spacer.gif" width="15" alt="">
                                                    </td>
                                                    <td>
                                                        <uc1:SPSPromotion ID="SPSPromotionBottomRight" DisplayPage="sony-parts" Type="3"
                                                            runat="server" />
                                                    </td>
                                                </tr>
                                            </table>
                                            <% End If %>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td width="700">
                                <footer><ServicePLUSWebApp:SonyFooter ID="SonyFooter1" runat="server" /></footer>
                            </td>
                        </tr>
                    </table>
                </td>
                <td width="25" style="background: url('images/sp_right_bkgd.gif')">
                    <img src="images/spacer.gif" width="25" height="20" alt="">
                </td>
            </tr>
        </table>
        </form>
    </center>
</body>
</html>