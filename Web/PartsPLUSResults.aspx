<%@ Reference Page="~/PartsPlusSearch.aspx" %>
<%@ Reference Page="~/SSL.aspx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="TopSonyHeader" Src="TopSonyHeader.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="ServicePlusNav" Src="ServicePlusNav.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="PersonalMessage" Src="PersonalMessage.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="SonyFooter" Src="SonyFooter.ascx" %>

<%@ Page Language="vb" AutoEventWireup="false" Inherits="ServicePLUSWebApp.PartsPLUSResults"
    EnableViewStateMac="true" CodeFile="PartsPLUSResults.aspx.vb" CodeFileBaseClass="ServicePLUSWebApp.SSL" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="<%=PageLanguage %>">
<head>
    <title><%=PageTitle%></title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

    <link href="themes/base/jquery.ui.all.css" rel="stylesheet" type="text/css" />
    <link href="includes/jquery-ui.min.css" rel="stylesheet" type="text/css" />
    <link href="includes/jquery-ui.structure.min.css" rel="stylesheet" type="text/css" />
    <link href="includes/ServicesPLUS_style.css" type="text/css" rel="stylesheet" />

    <script src="includes/jquery-3.1.1.min.js" type="text/javascript"></script>
    <script src="includes/jquery-ui.min.js" type="text/javascript"></script>
    <script src="includes/ServicesPLUS.js" type="text/javascript"></script>
    <script src="includes/svcplus_globalization.js" type="text/javascript"></script>
    <script src="includes/ShoppingCart.js" type="text/javascript"></script>
</head>

<body style="margin: 0px; background: #5d7180; color: Black;">
    <div id="ValidationMessageDiv"></div>
    <%-- Shopping Cart Modal Window --%>
    <div id="dialog" title="<%= Resources.Resource.atc_title_QuickOrderStatus%>">
        <div id="divPrg" style="font-size: 11px; font-family: Arial; overflow: auto;"></div>
        <div id="divMessage" style="font-size: 11px; font-family: Arial; overflow: auto;"></div>
        <div id="divException" style="font-family: Arial, Helvetica, sans-serif; font-size: 11px; line-height: 14px; overflow: auto; display: none;"></div>
        <div id="divCloseButton" style="display: none;">
            <br />
            <a href="#"><img src="<%=Resources.Resource.img_btnCloseWindow %>" alt="Close"
                    onclick="javascript:return closedilog();" id="messageclose" /></a>
        </div>
    </div>
    <center>
        <form method="post" runat="server" id="form1">
            <asp:HiddenField ID="atc_ex_InvalidPartNumber" runat="server" Value="<%$ Resources:Resource, atc_ex_InvalidPartNumber %>" />
            <asp:HiddenField ID="atc_ex_InvalidQuantityForPart" runat="server" Value="<%$ Resources:Resource, atc_ex_InvalidQuantityForPart %>" />
            <asp:HiddenField ID="atc_ex_InvalidPartAndQuantity" runat="server" Value="<%$ Resources:Resource, atc_ex_InvalidPartAndQuantity %>" />
            <asp:HiddenField ID="atc_ex_DifferentCart" runat="server" Value="<%$ Resources:Resource, atc_ex_DifferentCart %>" />
            <asp:HiddenField ID="atc_title_OrderStatus" runat="server" Value="<%$ Resources:Resource, atc_title_OrderStatus %>" />
            <asp:HiddenField ID="atc_title_OrderExceptions" runat="server" Value="<%$ Resources:Resource, atc_title_OrderExceptions %>" />
            <asp:HiddenField ID="atc_title_QuickOrderStatus" runat="server" Value="<%$ Resources:Resource, atc_title_QuickOrderStatus %>" />
            <asp:HiddenField ID="atc_title_QuickOrderExceptions" runat="server" Value="<%$ Resources:Resource, atc_title_QuickOrderExceptions %>" />
            <asp:HiddenField ID="atc_AddingItem" runat="server" Value="<%$ Resources:Resource, atc_AddingItem %>" />
            <asp:HiddenField ID="atc_ToCart" runat="server" Value="<%$ Resources:Resource, atc_ToCart %>" />
            <asp:HiddenField ID="atc_warn_LongWait" runat="server" Value="<%$ Resources:Resource, atc_warn_LongWait %>" />
            <asp:HiddenField ID="prg_PleaseWait" runat="server" Value="<%$ Resources:Resource, prg_PleaseWait %>" />
            <asp:HiddenField ID="img_btnCloseWindow" runat="server" Value="<%$ Resources:Resource, img_btnCloseWindow %>" />
            <asp:HiddenField ID="quickOrderMaxItems" runat="server" Value="" />
            <asp:HiddenField ID="orderUploadMaxItems" runat="server" Value="" />

            <table width="760" border="0" role="presentation">
                <tr>
                    <td width="25" style="background: url('images/sp_left_bkgd.gif')">
                        <img height="25" src="images/spacer.gif" width="25" alt="">
                    </td>
                    <td width="710" bgcolor="#ffffff">
                        <table width="710" border="0" role="presentation">
                            <tr>
                                <td>
                                    <ServicePLUSWebApp:TopSonyHeader ID="TopSonyHeader" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <nav><ServicePLUSWebApp:ServicePlusNav ID="SonyNav" runat="server" /></nav>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="710" border="0" role="presentation">
                                        <tr>
                                            <td width="464" bgcolor="#363d45" height="40" background="images/sp_int_header_top_partsPLUS_short.gif"
                                                align="right" valign="top">
                                                <br />
                                                <h1 class="headerText"><%=Resources.Resource.el_Pts_MainHeading%></h1>
                                            </td>
                                            <td valign="top" bgcolor="#363d45">
                                                <table width="246" border="0" role="presentation">
                                                    <tr>
                                                        <td colspan="3">
                                                            <img height="5" src="images/spacer.gif" width="246" alt="">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="5">
                                                            &nbsp;
                                                        </td>
                                                        <td width="236">
                                                            <span class="memberName">
                                                                <ServicePLUSWebApp:PersonalMessage ID="PersonalMessageDisplay" runat="server" />
                                                            </span>
                                                        </td>
                                                        <td width="5">
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td bgcolor="#f2f5f8">
                                                <h2 id="lblSubHdr" class="headerTitle" style="padding-right: 20px; text-align: right;" runat="server"></h2>
                                            </td>
                                            <td bgcolor="#99a8b5">
                                                <asp:ImageButton ID="btnNewSearch" runat="server" ImageUrl="<%$ Resources:Resource,img_btnNewSearch%>" role="button"
                                                    AlternateText="<%$ Resources:Resource,el_newSearch%>" title="Go back and start a new search" /><a name="top"></a>
                                            </td>
                                        </tr>
                                        <tr height="9">
                                            <td bgcolor="#f2f5f8">
                                                <img height="9" src="images/sp_int_header_btm_left_onepix.gif" width="464" alt="">
                                            </td>
                                            <td bgcolor="#99a8b5">
                                                <img height="9" src="images/sp_int_header_btm_right.gif" width="246" alt="">
                                            </td>
                                        </tr>
                                    </table>
                                    &nbsp;&nbsp;&nbsp;
                                <asp:Label ID="ErrorLabel" runat="server" CssClass="redAsterick"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="710" border="0" role="presentation">
                                        <tr>
                                            <td width="20" height="20">
                                                <img src="images/spacer.gif" width="20" height="20" alt="">
                                            </td>
                                            <td width="670" height="20">
                                                <img src="images/spacer.gif" width="670" height="20" alt="">
                                            </td>
                                            <td width="20" height="20">
                                                <img src="images/spacer.gif" width="20" height="20" alt="">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="20">
                                                <img src="images/spacer.gif" width="20" height="20" alt=""></td>
                                            <td width="670" valign="top">
                                                <span class="bodyCopy">
                                                    <asp:Label ID="SearchCopyLabel" runat="server"></asp:Label>
                                                </span>
                                            </td>
                                            <td width="20" height="20">
                                                <img src="images/spacer.gif" width="20" height="20" alt=""></td>
                                        </tr>
                                        <tr>
                                            <td width="20">
                                                <img src="images/spacer.gif" width="20" alt="">
                                            </td>
                                            <td colspan="2">
                                                &nbsp;&nbsp;&nbsp;<asp:Label ID="PageLabel" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" colspan="3">
                                                <asp:Table ID="DisplayResultsTable" runat="server" Width="670" Caption="Search results table" />
                                                <asp:Label ID="KitLabel" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="20">
                                                <img src="images/spacer.gif" width="20" alt="">
                                            </td>
                                            <td colspan="2">
                                                &nbsp;&nbsp;&nbsp;<asp:Label ID="PageLabel2" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" colspan="3">
                                                <asp:Label ID="BackToTopLabel" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <%--6994 starts--%>
                                        <tr>
                                            <td align="left" colspan="3">
                                                <SPS:SPSTextBox ID="hidespstextitemno" runat="server" Style="display: none" />
                                                <SPS:SPSTextBox ID="hidespstextqty" runat="server" Style="display: none" />
                                            </td>
                                        </tr>
                                        <%--6994 ends--%>
                                        <tr>
                                            <td width="20">
                                                <img src="images/spacer.gif" width="20" alt="">
                                            </td>
                                            <td colspan="2" id="tdLegend" style="text-align: left;" runat="server">
                                                <span style="font-family: Wingdings; color: red; font-size: 12px;">v</span><span class="bodyCopy"> &nbsp;<a
                                                    href="parts-promotions.aspx"><%=Resources.Resource.el_LimitedPromoPrice%></a></span>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <footer><ServicePLUSWebApp:SonyFooter ID="SonyFooter1" runat="server" /></footer>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="25" style="background: url('images/sp_right_bkgd.gif')">
                        <img src="images/spacer.gif" width="25" height="20" alt="">
                    </td>
                </tr>
            </table>
        </form>
    </center>
</body>
</html>
