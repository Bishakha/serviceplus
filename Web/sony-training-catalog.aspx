<%@ Reference Page="~/SSL.aspx" %>

<%@ Page Language="vb" AutoEventWireup="false" Inherits="ServicePLUSWebApp.sony_training_catalog" CodeFile="sony-training-catalog.aspx.vb" CodeFileBaseClass="ServicePLUSWebApp.SSL" %>

<%@ Register TagPrefix="uc1" TagName="SPSPromotion" Src="~/UserControl/SPSPromotion.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="TopSonyHeader" Src="TopSonyHeader.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="ServicePlusNav" Src="ServicePlusNav.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="PersonalMessage" Src="PersonalMessage.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="SonyFooter" Src="SonyFooter.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//Dtd HTML 4.0 Transitional//EN">
<html lang="<%=PageLanguage %>">
<head>
    <title>Sony Training Institute - Classes</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <meta content="sony, training, video, audio, broadcast" name="keywords">
    <link href="includes/ServicesPLUS_style.css" type="text/css" rel="stylesheet">
    <script type="text/javascript" src="includes/ServicesPLUS.js"></script>
    <script type="text/javascript" src="includes/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="includes/svcplus_globalization.js"></script>

</head>
<body style="color: #000000; background: #5d7180; margin: 0px;">
    <div id="styleDiv" style="display: none;">
        <div style="position: absolute; background: #ffffff; border: 0px solid #6699cc; width: auto; height: 20px; font-family: verdana; font-size: 8pt; border: 0px solid black; z-index: 3;">
            <div id="oTitle" style="position: relative; width: 100%; background: #ffffff; height: 10px; font-weight: bold; padding: 5px; font-size: 8pt; filter: progid:DXImageTransform.Microsoft.Gradient(endColorstr='#00ffffff', startColorstr='#FF99CCFF', gradientType='1');">Click Here...</div>
            <div id="oText" style="padding: 5px"></div>
            <div id="DIV1" style="cursor: hand; font-weight: normal; color: darkblue; padding: 5px" onclick="parent.location.href = 'sony-training-class-details.aspx?ClassNumber=trAIN0909'">SD class:  January 15-18, 2007 (4 days)</div>
            <div id="DIV12" style="cursor: hand; font-weight: normal; color: darkblue; padding: 5px" onclick="parent.location.href ='sony-training-class-details.aspx?ClassNumber=trAIN0878'">HD class:  January 22-24, 2007 (3 days)</div>
            <div id="oLinkStore" style="display: none"></div>
        </div>
    </div>

    <form id="Form1" method="post" runat="server">
        <center>
            <table style="width: 680px; border: none;" role="presentation">
                <tr>
                    <td style="width: 25px; background: url('images/sp_left_bkgd.gif');">
                        <img src="images/spacer.gif" height="25" width="25" alt="">
                    </td>
                    <td style="width: 727px; background: #ffffff;">
                        <table width="708" border="0" role="presentation">
                            <tr>
                                <td>
                                    <ServicePLUSWebApp:TopSonyHeader ID="TopSonyHeader" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 708px; height: 1px">
                                    <nav><ServicePLUSWebApp:ServicePlusNav ID="SonyNav" runat="server" /></nav>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table style="width: 710px; height: 96px" width="710" border="0" role="presentation">
                                        <tr style="height: 82px;">
                                            <td class="headerText" style="width: 464px; vertical-align: middle; text-align: right; background: #363d45 url('images/Sony_TrainingInstitute_Banner.jpg');">
                                                <h1 class="headerText"><%=Resources.Resource.hplnk_sony_training_inst()%><sup>&reg;</sup>&nbsp;</h1>
                                            </td>
                                            <td style="width: 246px; vertical-align: top; background: #363d45">
                                                <ServicePLUSWebApp:PersonalMessage ID="PersonalMessageDisplay" runat="server" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div id="hiddenSonyTraining" runat="server">
                                        <table width="710" border="0" role="presentation">
                                            <tr>
                                                <td width="466" bgcolor="#f2f5f8">
                                                    <table width="464" border="0" role="presentation">
                                                        <tr>
                                                            <td width="20">
                                                                &nbsp;
                                                            </td>
                                                            <td class="bodyCopySM">
                                                                <%=Resources.Resource.contact_canada_support %><a href="mailto:<%=Resources.Resource.contact_canada_support_email%>" title="Send an email to <%=Resources.Resource.contact_canada_support_email%>"><%=Resources.Resource.contact_canada_support_email%></a>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td bgcolor="#99a8b5" valign="bottom">
                                                </td>
                                            </tr>
                                            <tr style="height: 9px;">
                                                <td style="width: 466px; background: #f2f5f8;">
                                                    <img height="9" src="images/sp_int_header_btm_left_onepix.gif" width="464" alt="">
                                                </td>
                                                <td style="background: #99a8b5;">
                                                    <img height="9" src="images/sp_int_header_btm_right.gif" width="246" alt="">
                                                </td>
                                            </tr>
                                        </table>
                                        <br />
                                    </div>
                                </td>
                            </tr>
                            <div id="DefaultSonyTraining" runat="server">
                                <tr>
                                    <td style="width: 708px;" valign="top">
                                        <table width="710" border="0" role="presentation">
                                            <tr>
                                                <td style="width: 464px; background: #f2f5f8">
                                                    <h2 class="headerTitle" style="padding-right: 20px; text-align: right;">Sony Training Catalog</h2>
                                                </td>
                                                <td style="width: 246px; background: #99a8b5">&nbsp;</td>
                                            </tr>
                                            <tr style="height: 9px;">
                                                <td style="width: 464px; background: #f2f5f8">
                                                    <img src="images/sp_int_header_btm_left_onepix.gif" style="height: 9px; width: 464px;" alt="">
                                                </td>
                                                <td style="width: 246px; background: #99a8b5">
                                                    <img src="images/sp_int_header_btm_right.gif" style="height: 9px; width: 246px;" alt="">
                                                </td>
                                            </tr>
                                        </table>
                                        <img src="images/spacer.gif" width="16" alt="">
                                        <asp:Label ID="errorMessageLabel" runat="server" CssClass="tableData" Width="641px" ForeColor="Red"
                                            EnableViewState="False"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 708px; height: 202px">
                                        <table id="Table1" style="width: 696px; height: 399px; border: none;" role="presentation">
                                            <tr>
                                                <td style="width: 24px; vertical-align: top;"></td>
                                                <td style="width: 274px; vertical-align: top;">
                                                    <div id="tableCategory" runat="server">&nbsp;</div>
                                                </td>
                                                <td valign="top">
                                                    <p class="promoCopyBold">
                                                        Since 1970, Sony Training Institute, the industry education leader, has trained
                                                        thousands of professionals like you on the latest service technologies and
                                                        production techniques.
                                                    </p>
                                                    <p class="promoCopy">
                                                        Sony Training Institute provides workshops, onsite and eLearning designed specifically to put you on the right track for your transition to digital and to high definition technology.
                                                    </p>
                                                    <p class="promoCopy">
                                                        To browse our course offerings by area of interest, click a link on the left
                                                        side of this page. To search for classes by location, month, course title, or
                                                        other criteria, see our <a href="sony-training-search.aspx" title="Visit the Sony training search page.">Training Search</a> page.
                                                        To register or access online courses, go to the <a href="http://www.sony.com/dcinematraining" target="_blank"
                                                            title="Visit the Sony Learning Management System in a new tab.">Sony Learning Management System</a> site.
                                                    </p>
                                                    <p class="promoCopy">
                                                        Interested in a course that's not listed? Please <a href="mailto:training@am.sony.com" title="Click to send an email to Training@am.sony.com">contact us</a> and let us know.
                                                    </p>
                                                    <uc1:SPSPromotion ID="TrainingPromotion1" runat="server" DisplayPage="sony-training-catalog" Type="2" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </div>
                            <tr>
                                <td style="width: 708px">
                                    <footer><ServicePLUSWebApp:SonyFooter ID="SonyFooter1" runat="server" /></footer>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td style="width: 25px; background: url('images/sp_right_bkgd.gif');">
                        <img height="5" src="images/spacer.gif" width="25" alt="">
                    </td>
                </tr>
            </table>
        </center>
    </form>

</body>
</html>
