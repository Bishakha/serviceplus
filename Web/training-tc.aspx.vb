Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Process


Namespace ServicePLUSWebApp

    Partial Class training_tc
        Inherits SSL

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub



        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region
#Region "Page Variables"
        'store class type globally
        Public IsNonClass As Integer
        Public budgetCode As String
#End Region
        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            'Put user code to initialize the page here
            Try
                If Not Request.QueryString("nonclass") Is Nothing Then
                    If Convert.ToInt32(Request.QueryString("nonclass")) = 1 Then
                        IsNonClass = 1
                        If Not Request.QueryString("budgetcode") Is Nothing Then
                            budgetCode = Request.QueryString("budgetcode")
                        End If
                    Else
                        IsNonClass = 0
                        budgetCode = ""
                    End If
                End If
            Catch ex As Exception
                IsNonClass = 0
                budgetCode = ""
            End Try

            ' do what is done in the student page
            If IsNonClass = 1 Then
                If Session.Item("trainingreservation") IsNot Nothing Then
                    Dim cr As CourseReservation = Session.Item("trainingreservation")
                    Dim participant As New CourseParticipant With {
                        .Address1 = "N/A",
                        .Address2 = "N/A",
                        .City = "N/A",
                        .State = "N/A",
                        .Zip = "0",
                        .Extension = "N/A",
                        .EMail = "N/A",
                        .Fax = "N/A",
                        .Phone = "N/A",
                        .ISOCountryCode = "840", 'npw default to US
                        .FirstName = "N/A",
                        .LastName = "N/A",
                        .Status = "N/A",
                        .Company = "N/A",
                        .ReservationNumber = cr.ReservationNumber
                    }

                    cr.AddParticipant(participant)
                End If

            End If
        End Sub

        Private Sub Accept_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Accept.Click
            If IsNonClass = 1 Then
                Response.Redirect("training-registration-information.aspx?nonclass=1&budgetcode=" + budgetCode)
            Else
                Response.Redirect("training-registration-information.aspx")
            End If

        End Sub

        Private Sub Decline_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Decline.Click
            Response.Redirect("sony-training-search.aspx")
        End Sub
    End Class

End Namespace
