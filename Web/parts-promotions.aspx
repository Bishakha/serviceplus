<%@ Reference Page="~/SSL.aspx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="SonyFooter" Src="SonyFooter.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="TopSonyHeader" Src="TopSonyHeader.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="PersonalMessage" Src="PersonalMessage.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="ServicePlusNav" Src="ServicePlusNav.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="PromotionalItem" Src="~/UserControl/PromotionalItem.ascx" %>
<%@ Reference Control="~/TrainingPromotionInsert.ascx" %>

<%@ Page Language="vb" AutoEventWireup="false" Inherits="ServicePLUSWebApp.partspromotions" EnableViewStateMac="true" CodeFile="parts-promotions.aspx.vb" CodeFileBaseClass="ServicePLUSWebApp.SSL" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="<%=PageLanguage %>">
<head>
    <title>ServicesPLUS promotions for Sony parts</title>
    <meta content="Sony Parts, Sony Professional Parts, Sony Broadcast Parts, Sony Business Parts, Sony Software, Sony Professional Software, Sony Broadcast Software, Sony Business Software"
        name="keywords">
    <link href="themes/base/jquery.ui.all.css" rel="stylesheet" type="text/css" />
    <link href="includes/jquery.cluetip.css" rel="stylesheet" type="text/css" />
    <link href="includes/ServicesPLUS_style.css" type="text/css" rel="stylesheet">

    <script src="includes/jquery-1.4.2.min.js" type="text/javascript"></script>
    <script src="includes/jquery.ui.core.js" type="text/javascript"></script>
    <script src="includes/jquery.ui.widget.js" type="text/javascript"></script>
    <script src="includes/jquery.cluetip.js" type="text/javascript"></script>
    <script src="includes/jquery.ui.mouse.js" type="text/javascript"></script>
    <script src="includes/jquery.ui.draggable.js" type="text/javascript"></script>
    <script src="includes/jquery.ui.position.js" type="text/javascript"></script>
    <script src="includes/jquery.ui.resizable.js" type="text/javascript"></script>
    <script src="includes/jquery.ui.dialog.js" type="text/javascript"></script>
    <script src="includes/jquery-ui-1.8.1.custom.min.js" type="text/javascript"></script>
    <script src="includes/ServicesPLUS.js" type="text/javascript"></script>

    <script type="text/javascript">
        var partItem;
        var kitItem;
        function addPartToCart(varPartNumber) {
            partspromotions.AddPartToCart(varPartNumber, DoCallBack);
        }

        function addPart() {
            DoCallBack(partspromotions.AddPartToCart(partItem));
        }

        function DoCallBack(result) {
            if (result.error !== null) {
                alert('We are experiencing some technical issue,\nPlease refresh the page and if you are not able to see the item in the cart please contact to help desk');
            }
            document.getElementById('ServicePlusNavDisplay_topItemInCart').innerHTML = result.value[0];
            document.getElementById('ServicePlusNavDisplay_topItemValueInCart').innerHTML = result.value[1];
        }

        function addKit() {
            DoCallBack(partspromotions.AddKitToCart(kitItem));
        }

        function addKitToCart(varKitNumber) {
            partspromotions.AddKitToCart(varKitNumber, DoCallBack);
        }

        function pageLoad(sender, args) {
            var sm = Sys.WebForms.PageRequestManager.getInstance();
            if (!sm.get_isInAsyncPostBack()) {
                sm.add_beginRequest(onBeginRequest);
                sm.add_endRequest(onRequestDone);
            }
        }

        function onBeginRequest(sender, args) {
            var send = args.get_postBackElement().value;
            if (displayWait(send) === "yes") {
                $find('PleaseWaitPopup').show();
            }
        }

        function onRequestDone() {
            //$find('PleaseWaitPopup').hide();
        }

        function displayWait(send) {
            switch (send) {
                case "Save":
                    return "yes";
                case "Save and Close":
                    return "yes";
                default:
                    return "no";
            }
        }

        function do_totals1() {
            document.all.pleasewaitScreen.style.pixelTop = document.body.scrollTop + 50;
            document.all.pleasewaitScreen.style.visibility = "visible";
            window.setTimeout('do_totals2()', 1);
        }

        function do_totals2() {
            calc_totals();
            document.all.pleasewaitScreen.style.visibility = "hidden";
        }
    </script>

    <script type="text/javascript">
        var exception = false;

        function closedilog() {
            $("#dialog").dialog("close");
            return true;
        }

        $(document).ready(function () {
            $("#messageclose").click(function () {
                $("#dialog").dialog("close");
            });
        });                       //Document ready

        function replaceAll(txt, replace, with_this) { return txt.replace(new RegExp(replace, 'g'), with_this); }

        function validPart(partnumber, qty) {
            if (jQuery.trim(partnumber.val()) === "" && jQuery.trim(qty.val()) === "") {
                return false;
            } else {
                if (jQuery.trim(partnumber.val()) === "" || jQuery.trim(qty.val()) === "") {
                    if (jQuery.trim(partnumber.val()) === "") {
                        exception = true;
                        $("#divException").append("<strong>Please enter a valid part." + qty.val().tostring() + "</strong><br/>");
                        $("#divException").append("<br/>");

                    } else {
                        exception = true;
                        $("#divException").append("<br/>");
                        $("#divException").append("<strong>Please enter a valid quantity." + partnumber.val() + "</strong><br/>");
                        $("#divException").append("<br/>");
                    }
                    return false;
                }
                if (jQuery.trim(partnumber.val()) !== "" && jQuery.trim(qty.val()) !== "") {
                    if (jQuery.trim(qty.val()).match("[^0-9]")) {
                        exception = true;
                        $("#divException").append("<strong> Item " + partnumber.val() + ": Please enter a valid order quantity. </strong><br/><br/>");
                        return false;
                    }
                    if (jQuery.trim(qty.val()) <= 0) {
                        exception = true;
                        $("#divException").append("<strong> Item " + partnumber.val() + ": Please enter a valid order quantity. </strong><br/><br/>");
                        return false;
                    }
                }
                return true;
            }
        }

        function AddToCart_onclick(val, type) {
            //debugger;
            exception = false;

            $("#divMessage").text('');
            var buttons = $('.ui-dialog-buttonpane').children('button');
            buttons.remove();
            $("#divException").text('');
            $("#divException").hide();
            $("#divPrg").html('<span id="progress"> Please wait...<br/> <img src="images/progbar.gif" alt="" /></span>');
            $("#divMessage").append(' <br/>');
            var totalNull = "";
            $('#hidespstextitemno').val(val);
            $('#hidespstextqty').val(1);
            var sQty = '#hidespstextqty';
            var sPart = '#hidespstextitemno';
            var qty = $(sQty);
            var part = $(sPart);

            totalNull = totalNull + qty.val() + part.val();

            if (totalNull === "" || jQuery.trim(part.val()) === "" || jQuery.trim(qty.val()) === "") {
                $("#divMessage").text('');
                ///   $("#divMessage").html("Please enter at least one part number.");
                $("#dialog").dialog({
                    title: 'Order Add to Cart Exceptions'
                });
                $("#divMessage").append('<br/> <br/><a href="" onclick="javascript:return closedilog();"><img src="images/sp_int_closeWindow_btn.gif" alt="Close this browser window." id="messageclose" /></a>');
            }
            var itemsInCart = $("#itemsInCart");
            //var totalPriceOfCart = $("#totalPriceOfCart");
            //var destination = itemsInCart.position();
            //var position = part.position();

            $("#dialog").dialog({
                title: '<strong>Order Add to Cart Status</strong>',
                height: 200,
                width: 560,
                modal: true,
                position: 'top',
                resizable: false
            });

            if (validPart(part, qty)) {
                var condPart;
                if (type === "Kit") {
                    condPart = part.val() + ":" + part.val();
                    CallingService(type, condPart);
                }
                else {
                    jQuery.ajax({
                        type: 'POST',
                        contentType: 'application/json; charset=utf-8',
                        data: "{ 'part': '" + part.val() + "' }",
                        dataType: 'json',
                        url: 'parts-promotions.aspx/GetConflictPart',
                        success: function (result) {

                            if (jQuery.trim(result.d).length > 0) {
                                condPart = result.d;
                            } //End result.d
                            CallingService(type, condPart);
                        } //End Function(result)

                    });
                }
            }
            return false;
        }

        function CallingService(type, condPart) {
            // debugger;
            $("#divMessage").text('');
            var buttons = $('.ui-dialog-buttonpane').children('button');
            buttons.remove();
            $("#divException").text('');
            $("#divException").hide();
            $("#divPrg").html('<span id="progress"> Please wait...<br/> <img src="images/progbar.gif" alt="" /></span>');
            $("#divMessage").append(' <br/>');

            //var loc = PartList;
            // for (i = 0; i <PartList.length; i++) {
            // var connd = PartList[i];
            var split = condPart.split(":");
            $("#divMessage").html("<br/><strong>Adding item " + replaceAll(split[0], "-", "") + " to cart... </strong><br/>");
            var parametervalues = '{sOldPartNumber: "' + split[0] + '",sNewPartNumber: "' + split[1] + '",type: "' + type + '"}';
            jQuery.ajax({
                type: 'POST',
                contentType: 'application/json; charset=utf-8',
                data: parametervalues,
                dataType: 'json',
                url: 'parts-promotions.aspx/addPartToCartwSPromotions',
                success: function (result) {
                    if (result.d.differentCart === true) {
                        exception = true;
                        $("#divException").append("<br/><strong>The item has been added to a cart for items to be shipped; your previous cart with items for electronic delivery has been saved</strong><br/>");
                    }

                    if (result.d.success === 1) {
                        $("#itemsInCart").text(result.d.items);
                        $("#totalPriceOfCart").text(result.d.totalAmount);
                    }
                    else if (result.d.success === 2) {
                        exception = true;
                        $("#itemsInCart").text(result.d.items);
                        $("#totalPriceOfCart").text(result.d.totalAmount);
                        $("#divException").append("<br/><br/><strong>" + result.d.message + "</strong><br/>");
                    }
                    else {
                        exception = true;

                        $("#divException").append("<br/><br/><strong>" + result.d.message + "</strong><br/>");
                    }
                    //$("#divMessage").html("<br/><strong>" + result.d.message + "</strong><br/>");
                    if (exception) {
                        var sHeight = $("#divException").height() + 200;
                        $("#progress").remove();
                        $("#dialog").dialog({
                            title: 'Order Add to Cart Exceptions',
                            height: sHeight.toString(),
                            position: 'top'
                        }).dialog("moveToTop");
                        $("#divMessage").text('');
                        $("#divException").append('<br/> <br/><a href="#" onclick="javascript:return closedilog(); return false;"><img src="images/sp_int_closeWindow_btn.gif" alt="Close this browser popup window."  id="messageclose" /></a>');
                        $("#divException").show("fast");
                    }
                    else {
                        $("#dialog").dialog("close");
                    }
                }
            });  //Jquery

            //  }//for loop
            return true;
        } //End CallingService

        function ValidatePartAndKits() {
            $("#progress").remove();
            $("#dialog").dialog({
                title: 'Add to Cart Exceptions',
                position: 'top'
            }).dialog("moveToTop");
            $("#divMessage").text('Invalid Item can not be added to Cart.');
            $("#divMessage").append(' <br/>');
            $("#divMessage").append('<br/> <br/><a href="#" onclick="javascript:return refreshPage(); return false;"><img src="images/sp_int_closeWindow_btn.gif" alt="Close this browser popup window." id="messageclose" /></a>');
        }

        function refreshPage() {
            $("#dialog").dialog("close");
            window.location.href = "parts-promotions.aspx";
        }
    </script>
</head>
<body style="color: #000000; background: #5d7180; margin: 0px;">
    <div id="ValidationMessageDiv">
    </div>
    <div id="dialog" title="<strong>Order Add to Cart Status</strong>">
        <div id="divPrg" style="font: 11px; font-family: Arial; overflow: auto;"></div>
        <div id="divMessage" style="font: 11px; font-family: Arial; overflow: auto;"></div>
        <div id="divException" style="font-family: Arial, Helvetica, sans-serif; font-size: 11px; line-height: 14px; overflow: auto; display: none;">
            <img src="images/sp_int_closeWindow_btn.gif" alt="Close" onclick="javascript:return closedilog();" id="messageclose" />
        </div>
    </div>

    <center>
        <form id="Form1" method="post" runat="server">
            <table id="tOuter" width="664" border="0" role="presentation">
                <tr>
                    <td width="25" style="background: url('images/sp_left_bkgd.gif');">
                        <img src="images/spacer.gif" width="25" height="25" alt="">
                    </td>
                    <td width="689" bgcolor="#ffffff">
                        <table border="0" role="presentation">
                            <tr>
                                <td width="582">
                                    <ServicePLUSWebApp:TopSonyHeader ID="TopSonyHeader" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td width="582" height="23">
                                    <nav><ServicePLUSWebApp:ServicePlusNav ID="SonyNav" runat="server" /></nav>
                                </td>
                            </tr>
                            <tr>
                                <td width="582">
                                    <table style="width: 710px; border: none;" role="main">
                                        <tr style="height: 57px;">
                                            <td style="width: 464px; vertical-align: middle; background: #363d45 url('images/sp_int_header_top_ServicesPLUS_onepix.gif');">
                                                <h1 class="headerText" style="padding-right: 20px; text-align: right;">ServicesPLUS</h1>
                                            </td>
                                            <td style="vertical-align: top; background: #363d45;">
                                                <ServicePLUSWebApp:PersonalMessage ID="PersonalMessageDisplay" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="background: #f2f5f8;">
                                                <h2 class="headerTitle" style="padding-right: 20px; text-align: right;">Parts Promotions</h2>
                                            </td>
                                            <td style="background: #99a8b5;">
                                            </td>
                                        </tr>
                                        <tr style="height: 9px;">
                                            <td style="background: #f2f5f8;">
                                                <img height="9" src="images/sp_int_header_btm_left_onepix.gif" width="464" alt="">
                                            </td>
                                            <td style="background: #99a8b5;">
                                                <img height="9" src="images/sp_int_header_btm_right.gif" width="246" alt="">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td width="582">
                                    <asp:Label ID="ErrorLabel" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr valign="top">
                                <td width="582">
                                    <ServicePLUSWebApp:PromotionalItem ID="objPromotionalItem" runat="server" />
                                </td>
                            </tr>
                            <%--6994 starts--%>
                            <tr>
                                <td align="left">
                                    <SPS:SPSTextBox ID="hidespstextitemno" runat="server" Style="display: none" />
                                    <SPS:SPSTextBox ID="hidespstextqty" runat="server" Style="display: none" />
                                </td>
                            </tr>
                            <%--6994 ends--%>
                            <tr>
                                <td width="582">
                                    <footer><ServicePLUSWebApp:SonyFooter ID="SonyFooter1" runat="server" /></footer>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="25" style="background: url('images/sp_right_bkgd.gif')">
                        <img src="images/spacer.gif" width="25" height="20" alt="">
                    </td>
                </tr>
            </table>
        </form>
    </center>
</body>
</html>
