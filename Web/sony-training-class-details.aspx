<%@ Reference Page="~/SSL.aspx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="SonyFooter" Src="SonyFooter.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="PersonalMessage" Src="PersonalMessage.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="ServicePlusNav" Src="ServicePlusNav.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="TopSonyHeader" Src="TopSonyHeader.ascx" %>

<%@ Page Language="vb" AutoEventWireup="false" Inherits="ServicePLUSWebApp.sony_training_class_details"
    CodeFile="sony-training-class-details.aspx.vb" CodeFileBaseClass="ServicePLUSWebApp.SSL" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="<%=PageLanguage %>">
<head>
    <title>Sony Training Institute � Class Details and Availability</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <meta content="sony, training, video, audio, broadcast" name="keywords">
    <link href="includes/ServicesPLUS_style.css" type="text/css" rel="stylesheet">

    <script language="javascript" src="includes/ServicesPLUS.js" type="text/javascript"></script>
    <%--6994 starts--%>
    <link href="themes/base/jquery.ui.all.css" rel="stylesheet" type="text/css" />
    <link href="includes/jquery.cluetip.css" rel="stylesheet" type="text/css" />
    <script src="includes/jquery-1.4.2.min.js" type="text/javascript"></script>
    <script src="includes/jquery.ui.core.js" type="text/javascript"></script>
    <script src="includes/jquery.ui.widget.js" type="text/javascript"></script>
    <script src="includes/jquery.cluetip.js" type="text/javascript"></script>
    <script src="includes/jquery.ui.mouse.js" type="text/javascript"></script>
    <script src="includes/jquery.ui.draggable.js" type="text/javascript"></script>
    <script src="includes/jquery.ui.position.js" type="text/javascript"></script>
    <script src="includes/jquery.ui.resizable.js" type="text/javascript"></script>
    <script src="includes/jquery.ui.dialog.js" type="text/javascript"></script>
    <script src="includes/jquery-ui-1.8.1.custom.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="includes/svcplus_globalization.js"></script>
    <script type="text/javascript">
        function closedilog() {
            $("#dialog").dialog("close");
            return true;
        }
        var exception = false;
        $(document).ready(function () {
            $("#messageclose").click(function () {

                $("#dialog").dialog("close");
            });


        });                       //Document ready

        function replaceAll(txt, replace, with_this) { return txt.replace(new RegExp(replace, 'g'), with_this); }

        function validPart(partnumber, qty) {

            if (jQuery.trim(partnumber.val()) === "" && jQuery.trim(qty.val()) === "") {
                return false;
            }
            else {

                if (jQuery.trim(partnumber.val()) === "" || jQuery.trim(qty.val()) === "") {
                    if (jQuery.trim(partnumber.val()) === "") {
                        exception = true;

                        $("#divException").append("<strong>Please enter a valid part." + qty.val().tostring() + "</strong><br/>");
                        //$("#divMessage").append("<strong>Please enter a valid part." + qty.val().tostring() + "</strong>");
                        $("#divException").append("<br/>");

                    } else {
                        exception = true;
                        $("#divException").append("<br/>");
                        $("#divException").append("<strong>Please enter a valid quantity." + partnumber.val() + "</strong><br/>");
                        // $("#divMessage").append("<strong>Please enter a valid quantity." + partnumber.val() + "</strong>");
                        $("#divException").append("<br/>");
                    }
                    return false;
                }
                if (jQuery.trim(partnumber.val()) !== "" && jQuery.trim(qty.val()) !== "") {
                    if (jQuery.trim(qty.val()).match("[^0-9]")) {
                        exception = true;

                        $("#divException").append("<strong> Item " + partnumber.val() + ": Please enter a valid order quantity. </strong><br/><br/>");

                        return false;
                    }
                    if (jQuery.trim(qty.val()) <= 0) {
                        exception = true;

                        $("#divException").append("<strong> Item " + partnumber.val() + ": Please enter a valid order quantity. </strong><br/><br/>");

                        return false;
                    }
                }
                return true;
            }
        }
        function AddToCart_onclick(val) {

            exception = false;
            $("#divMessage").text('');
            var buttons = $('.ui-dialog-buttonpane').children('button');
            buttons.remove();
            $("#divException").text('');
            $("#divException").hide();
            $("#divPrg").html('<span id="progress"> Please wait...<br> <img   src="images/progbar.gif" alt ="Progres" /></span>');
            $("#divMessage").append(' <br/>');
            var totalNull = "";
            var count = 0;
            $('#hidespstextitemno').val(val);
            $('#hidespstextqty').val(1);
            var sQty = '#hidespstextqty';
            var sPart = '#hidespstextitemno';

            var qty = $(sQty);
            var part = $(sPart);

            totalNull = totalNull + qty.val() + part.val();

            if (totalNull === "" || jQuery.trim(part.val()) === "" || jQuery.trim(qty.val()) === "") {

                $("#divMessage").text('');
                ///   $("#divMessage").html("Please enter at least one part number.");
                $("#dialog").dialog({
                    //title: 'Quick Order Add to Cart Exceptions'
                    title: 'Order Add to Cart Exceptions'

                });
                $("#divMessage").append('<br/> <br/><a  ><img src ="images/sp_int_closeWindow_btn.gif" alt ="Close" onclick="javascript:return closedilog();"  id="messageclose" /></a>');
            }
            var itemsInCart = $("#itemsInCart");
            var totalPriceOfCart = $("#totalPriceOfCart");
            var destination = itemsInCart.position();
            var position = part.position();

            $("#dialog").dialog({
                //title: '<strong>Quick Order Add to Cart Status</strong>',
                title: '<strong>Order Add to Cart Status</strong>',
                height: 200,
                width: 560,
                modal: true,
                position: 'top',
                resizable: false
            });
            count = count + 1;
            if (validPart(part, qty)) {
                count = count + 1;
                $("#divMessage").html("<br/><strong>Adding item " + replaceAll(part.val(), "-", "") + " to cart... </strong><br/>");
                var parametervalues = '{sPartNumber: "' + jQuery.trim(replaceAll(part.val(), "-", "")) + '",sPartQuantity: "' + jQuery.trim(qty[0].value) + '" }';
                //Call webmethod using ajax
                jQuery.ajax({
                    type: 'POST',
                    contentType: 'application/json; charset=utf-8',
                    data: parametervalues,
                    dataType: 'json',
                    url: 'sony-training-class-details.aspx/addPartToCartwSTrainingClassDetails',
                    success: function (result) {
                        count = count - 1;
                        if (result.d.differentCart === true) {
                            exception = true;
                            $("#divException").append("<br/><strong>The item has been added to a cart for items to be shipped; your previous cart with items for electronic delivery has been saved</strong><br/>");
                        }

                        if (result.d.success === 1) {
                            $("#divMessage").html("<br/><strong>Adding item " + replaceAll(result.d.partnumber, "-", "") + " to cart... </strong><br/>");
                            result.d.partnumber = '';
                            result.d.Quantity = '';
                            itemsInCart.text(result.d.items);
                            totalPriceOfCart.text(result.d.totalAmount);
                        }
                        else if (result.d.success === 2) {
                            exception = true;
                            $("#divMessage").html("<br/><strong>Adding item " + replaceAll(result.d.partnumber, "-", "") + " to cart... </strong><br/>");
                            result.d.partnumber = '';
                            result.d.Quantity = '';
                            itemsInCart.text(result.d.items);
                            totalPriceOfCart.text(result.d.totalAmount);
                            $("#divException").append("<br/><strong>" + result.d.message + "</strong><br/>");
                        }
                        else {
                            exception = true;

                            $("#divException").append("<br/><strong>" + result.d.message + "</strong><br/>");
                        }
                        //$("#divMessage").html("<br/><strong>" + result.d.message + "</strong><br/>");

                        if (count === 0) {
                            if (exception) {
                                var sHeight = $("#divException").height() + 200;

                                $("#progress").remove();
                                $("#dialog").dialog({
                                    //title: 'Quick Order Add to Cart Exceptions',
                                    title: 'Order Add to Cart Exceptions',
                                    height: sHeight.toString(),
                                    position: 'top'
                                }).dialog("moveToTop");
                                $("#divMessage").text('');
                                $("#divException").append('<br/> <br/><a ><img src ="images/sp_int_closeWindow_btn.gif" alt ="Close" onclick="javascript:return closedilog();"  id="messageclose" /></a>');
                                $("#divException").show("fast");
                            }
                            else {
                                $("#dialog").dialog("close");
                            }
                        }
                    }
                });
            }
            count = count - 1;

            if (count === 0) {
                $("#progress").remove();

                if (exception) {
                    var sHeight = $("#divException").height() + 200;

                    $("#progress").remove();
                    $("#dialog").dialog({
                        //title: 'Quick Order Add to Cart Exceptions',
                        title: 'Order Add to Cart Exceptions',
                        height: sHeight.toString(),
                        position: 'top'
                    }).dialog("moveToTop");
                    $("#divMessage").text('');
                    $("#divException").append('<br/> <br/><a ><img src ="images/sp_int_closeWindow_btn.gif" alt ="Close" onclick="javascript:return closedilog();"  id="messageclose" /></a>');
                    $("#divException").show("fast");
                    return false;
                }
            }
            return false;
        }
    </script>
</head>

<body style="color: #000000; background: #5d7180; margin: 0px;">
    <div id="ValidationMessageDiv"></div>
    <div id="dialog" title="<strong>Order Add to Cart Status</strong>">
        <div id="divPrg" style="font: 11px; font-family: Arial; overflow: auto;"></div>
        <div id="divMessage" style="font: 11px; font-family: Arial; overflow: auto;"></div>
        <div id="divException" style="font-family: Arial, Helvetica, sans-serif; font-size: 11px; line-height: 14px; overflow: auto; display: none;">
            <img src="images/sp_int_closeWindow_btn.gif" alt="Close" onclick="javascript:return closedilog();" id="messageclose" />
        </div>
    </div>

    <center>
        <form id="form2" method="post" runat="server">
            <table width="760" border="0" role="presentation">
                <tr>
                    <td style="width: 25px; background: url('images/sp_left_bkgd.gif');">
                        <img height="25" src="images/spacer.gif" width="25" alt="">
                    </td>
                    <td width="710" bgcolor="#ffffff">
                        <table width="710" border="0" role="presentation">
                            <tr>
                                <td>
                                    <ServicePLUSWebApp:TopSonyHeader ID="TopSonyHeader" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td>

                                    <nav><ServicePLUSWebApp:ServicePlusNav ID="SonyNav" runat="server" /></nav>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="710" border="0" role="presentation">
                                        <tr>
                                            <td style="width: 464px; height: 40px; background: #363d45 url('images/sp_int_header_top_ServicesPLUS_onepix.gif'); text-align: right; vertical-align: middle;">
                                                <h1 class="headerText" style="padding-right: 20px;">ServicesPLUS&nbsp;&nbsp;</h1>
                                            </td>
                                            <td valign="top" bgcolor="#363d45">
                                                <ServicePLUSWebApp:PersonalMessage ID="PersonalMessageDisplay" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td bgcolor="#f2f5f8">
                                                <h2 id="imgHeader" class="headerTitle" style="padding-right: 20px; text-align: right;" runat="server">Class Details and Availability</h2>
                                            </td>
                                            <td bgcolor="#99a8b5">
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr style="height: 9px;">
                                            <td bgcolor="#f2f5f8">
                                                <img height="9" src="images/sp_int_header_btm_left_onepix.gif" width="464" alt="">
                                            </td>
                                            <td bgcolor="#99a8b5">
                                                <img height="9" src="images/sp_int_header_btm_right.gif" width="246" alt="">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="710" border="0" style="width: 710px; padding: 20px 0px;" role="presentation">
                                        <tr>
                                            <td style="height: 44px; vertical-align: top;">
                                                <asp:Label ID="labelAction" runat="server" CssClass="redAsterick" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="ErrorLabel" runat="server" CssClass="redAsterick" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top">
                                                <table id="tblData" width="100%" style="vertical-align: top;" runat="server" role="presentation">
                                                    <tr style="height: 22px;">
                                                        <td style="width: 182px; text-align: right; vertical-align: top;" class="tableHeader">
                                                            Title:
                                                        </td>
                                                        <td style="vertical-align: top;">
                                                            <asp:Label ID="lblTitle" runat="server" CssClass="tableData" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align: right; vertical-align: top;" class="tableHeader">
                                                            Description:
                                                        </td>
                                                        <td style="vertical-align: top;">
                                                            &nbsp;<asp:Label ID="lblDescription" runat="server" CssClass="tableData" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align: right; vertical-align: top;" class="tableHeader">
                                                            Recommended Prerequisite(s):
                                                        </td>
                                                        <td style="vertical-align: top;">
                                                            &nbsp;<asp:Label ID="lblPrerequisite" runat="server" CssClass="tableData" />
                                                        </td>
                                                    </tr>
                                                    <tr id="startDate" visible="false" runat="server">
                                                        <td style="text-align: right; vertical-align: top;" class="tableHeader">
                                                            Start Date:
                                                        </td>
                                                        <td style="vertical-align: top;">
                                                            &nbsp;<asp:Label ID="lblStartDate" runat="server" CssClass="tabledata" />
                                                        </td>
                                                    </tr>
                                                    <tr id="endDate" visible="false" runat="server">
                                                        <td style="text-align: right; vertical-align: top;" class="tableHeader">
                                                            End Date:
                                                        </td>
                                                        <td style="vertical-align: top;">
                                                            &nbsp;<asp:Label ID="lblEndDate" runat="server" CssClass="tableData" />
                                                        </td>
                                                    </tr>
                                                    <tr id="trainingTime" visible="false" runat="server">
                                                        <td style="text-align: right; vertical-align: top;" class="tableHeader">
                                                            Training Time:
                                                        </td>
                                                        <td style="vertical-align: top;">
                                                            &nbsp;<asp:Label ID="lblTrainingTime" runat="server" CssClass="tableData" />
                                                        </td>
                                                    </tr>
                                                    <tr id="trainingLocation" runat="server">
                                                        <td style="text-align: right; vertical-align: top;" class="tableHeader">
                                                            <asp:Label ID="lblLocationTitle" runat="server">Location:</asp:Label>
                                                        </td>
                                                        <td style="vertical-align: top;">
                                                            <asp:Label ID="lblLocation" runat="server" CssClass="tableData" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align: right; vertical-align: top;" class="tableHeader">
                                                            <asp:Label ID="lblPriceTitle" runat="server">Price per Student:</asp:Label>
                                                        </td>
                                                        <td style="vertical-align: top;">
                                                            &nbsp;<asp:Label ID="lblListPrice" runat="server" CssClass="tableData" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr runat="server" id="trButton">
                                            <td width="670">
                                                <br />
                                                <img height="5" src="images/spacer.gif" width="670" alt=""><br />
                                                <table border="0" style="width: 440px" role="presentation">
                                                    <tr>
                                                        <td valign="top" align="left" width="440">
                                                            <asp:ImageButton ID="btnRegister" runat="server" AlternateText="Register" ImageUrl="images/sp_int_register_btn.gif" />
                                                            <asp:ImageButton ID="btnCancel" runat="server" AlternateText="Cancel" ImageUrl="images/sp_int_cancel_btn.gif" CommandName="Cancel" />
                                                        </td>
                                                    </tr>
                                                </table>
                                                <br>
                                                <br>
                                                <br>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <footer><ServicePLUSWebApp:SonyFooter ID="SonyFooter1" runat="server" /></footer>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td style="width: 25px; background: url('images/sp_right_bkgd.gif');">
                        <img height="25" src="images/spacer.gif" width="25" alt="">
                    </td>
                </tr>
            </table>
            <SPS:SPSTextBox ID="hidespstextitemno" runat="server" Style="display: none" />
            <SPS:SPSTextBox ID="hidespstextqty" runat="server" Style="display: none" />
        </form>
    </center>
</body>
</html>
