<%@ Page Language="VB" AutoEventWireup="false" CodeFile="service-naccntholder.aspx.vb" CodeFileBaseClass="ServicePLUSWebApp.SSL" Inherits="ServicePLUSWebApp.service_naccntholder" %>

<%@ Reference Page="~/SSL.aspx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="SonyFooter" Src="SonyFooter.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="TopSonyHeader" Src="TopSonyHeader.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="PersonalMessage" Src="PersonalMessage.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="ServicePlusNav" Src="ServicePlusNav.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="<%=PageLanguage %>">
<head>
    <title><%=Resources.Resource.header_serviceplus()%> - <%=Resources.Resource.repair_svcacc_hdr_msg() %></title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <meta content="Repair, Maintenance, Services, Sony Models, Sony Professional Models, Sony Broadcast Models, Sony Business Models, Sony Software, Sony Professional Software, Sony Broadcast Software, Sony Business Software"
        name="keywords">
    <link href="includes/ServicesPLUS_style.css" rel="stylesheet" type="text/css" />
    <script language="javascript" src="includes/ServicesPLUS.js" type="text/javascript"></script>
    <script type="text/javascript" src="includes/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="includes/svcplus_globalization.js"></script>

    <script type="text/javascript">
        function checkServiceContract() {
            var radioObj = document.forms['Form1'].elements['group1']
            var radioLength = radioObj.length;
            var iSelect = 0;

            if (radioLength === undefined) {
                if (radioObj.checked)
                    return radioObj.value;
                else
                    return "";
            }
            for (var i = 0; i < radioLength; i++) {
                if (radioObj[i].checked) {
                    //return group1[i].value;
                    iSelect = radioObj[i].value;
                }
            }

            if (iSelect === 1) {
                if (document.forms['Form1'].txtContractNo.value === "") {
                    document.forms['Form1'].cvServiceContract.value = '<%=Resources.Resource.repair_acchldr_cntc_msg()%>';
                    return false;
                }
            }
        }

        function ValidateApproval(source, args) {
            event.returnValue = document.getElementById('chkApproval').checked
        }

        function copyAddress() {
            if (document.getElementById('chkCopy').checked === true) {
                document.getElementById('txtShpAddr1').value = document.getElementById('txtAddr1').value;
                document.getElementById('txtShpAddr2').value = document.getElementById('txtAddr2').value;
                //By Sneha on 25/03/2014: This line is commented for bug fix 8955(bugzilla)
                //document.getElementById('txtShpAddr3').value = document.getElementById('txtAddr3').value;
                document.getElementById('txtShpAttnName').value = document.getElementById('txtAttnName').value;
                document.getElementById('txtShpCity').value = document.getElementById('txtCity').value;
                document.getElementById('txtShpCompany').value = document.getElementById('txtCompany').value;
                document.getElementById('txtShpExtension').value = document.getElementById('txtPhExtn').value;
                document.getElementById('txtShpPhone').value = document.getElementById('txtPhone').value;
                document.getElementById('txtShpZip').value = document.getElementById('txtZip').value;
                document.getElementById('txtShpFax').value = document.getElementById('txtFax').value;

                document.getElementById('ddlShpState').selectedIndex = document.getElementById('ddlBillToState').selectedIndex;
            }
        }

        function poNotRequired() {
            if (document.getElementById('noPoRequiredCheckBox').checked === true) {
                document.getElementById("txtPONumber").value = "NO-PO-REQ";
            } else {
                document.getElementById("txtPONumber").value = "";
            }
        }
    </script>
</head>
<body style="color: #000000; background: #5d7180; margin: 0px;">
    <form id="Form1" method="post" runat="server">
        <center>
            <table width="710" border="0" role="presentation">
                <tr>
                    <td width="25" style="background: url('images/sp_left_bkgd.gif');">
                        <img src="images/spacer.gif" width="25" height="25" alt="">
                    </td>
                    <td width="689" bgcolor="#ffffff">
                        <table width="710" border="0" role="presentation">
                            <tr>
                                <td width="582">
                                    <ServicePLUSWebApp:TopSonyHeader ID="TopSonyHeader" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td width="582" height="23">
                                    <nav><ServicePLUSWebApp:ServicePlusNav ID="SonyNav" runat="server" /></nav>
                                </td>
                            </tr>
                            <tr>
                                <td width="582">
                                    <table width="710" border="0" role="presentation">
                                        <tr>
                                            <td align="right" background="images/repair_lrg.gif" bgcolor="#363d45"
                                                height="82" style="width: 465px">
                                                <br />
                                                <h1 class="headerText"><%=Resources.Resource.repair_snysvc_srl_msg_4 %></h1>
                                            </td>
                                            <td valign="middle" width="238" bgcolor="#363d45">
                                                <ServicePLUSWebApp:PersonalMessage ID="PersonalMessageDisplay" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td bgcolor="#f2f5f8" style="width: 465px">
                                                <h2 id="lblSubHdr" class="headerTitle" style="padding-right: 20px; text-align: right;" runat="server"></h2>
                                            </td>
                                            <td width="238" bgcolor="#99a8b5">
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr height="9">
                                            <td bgcolor="#f2f5f8" style="width: 465px">
                                                <img height="9" src="images/sp_int_header_btm_left_onepix.gif" width="464" alt="">
                                            </td>
                                            <td width="238" bgcolor="#99a8b5">
                                                <img height="9" src="images/sp_int_header_btm_right.gif" width="246" alt="">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="100%" border="0" role="presentation">
                                        <tr>
                                            <td style="width: 5px; height: 22px;">
                                                <img height="20" src="images/spacer.gif" width="20" alt="">
                                            </td>
                                            <td style="width: 796px; height: 22px;">
                                                <p class="promoCopyBold">
                                                    <%=Resources.Resource.repair_svcacc_lblvld_msg() %>
                                                </p>
                                            </td>
                                            <td style="width: 436px; height: 22px;">
                                                <img height="20" src="images/spacer.gif" width="20" alt="">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 5px; height: 14px;">
                                                <img height="20" src="images/spacer.gif" width="20" alt="">
                                            </td>
                                            <td style="width: 796px; height: 14px;">
                                                <asp:Label ID="ErrorValidation" runat="server" CssClass="redAsterick" Text="<%$Resources:Resource,repair_svcacc_invld_msg%>" Visible="False" />
                                            </td>
                                            <td align="center" style="width: 436px; height: 14px;">&nbsp;&nbsp;<img height="20" src="images/spacer.gif" width="20" alt="">&nbsp;&nbsp;</td>
                                        </tr>
                                        <tr valign="top">
                                            <td style="width: 5px; height: 107px;">
                                                <img height="20" src="images/spacer.gif" width="20" alt="">
                                            </td>
                                            <td style="height: 107px;" colspan="2">
                                                <table cellpadding="3" border="0" width="100%" role="presentation">
                                                    <%If bShow = True Then%>
                                                    <tr valign="middle">
                                                        <td bgcolor="#d5dee9" colspan="2" style="height: 23px"><span class="tableHeader"><%=Resources.Resource.repair_svcacc_bilto_msg() %></span></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 97px;" class="tableData" align="right">
                                                            <asp:Label ID="lblBillCompany" runat="server"><%=Resources.Resource.repair_svcacc_cmyname_msg() %> </asp:Label>
                                                        </td>
                                                        <td style="width: 376px;">
                                                            <SPS:SPSTextBox ID="txtCompany" runat="server" CssClass="tableData" MaxLength="100" Width="184px"></SPS:SPSTextBox><span class="redAsterick">*</span>&nbsp;&nbsp;
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 97px" class="tableData" align="right">
                                                            <asp:Label ID="lblBillAttn" runat="server"><%=Resources.Resource.repair_svcacc_attname_msg()%></asp:Label>
                                                        </td>
                                                        <td>
                                                            <SPS:SPSTextBox ID="txtAttnName" runat="server" CssClass="tableData" Width="184px"></SPS:SPSTextBox><span class="redAsterick">*</span>&nbsp;&nbsp;
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 97px" class="tableData" align="right">
                                                            <asp:Label ID="lblBillAddr1" runat="server"><%=Resources.Resource.repair_svcacc_adrs_msg()%></asp:Label>
                                                        </td>
                                                        <td>
                                                            <SPS:SPSTextBox ID="txtAddr1" runat="server" CssClass="tableData" Width="184px" MaxLength="50"></SPS:SPSTextBox><asp:Label ID="LabelLineBill1Star" runat="server" CssClass="redAsterick"></asp:Label><span class="redAsterick">*</span>&nbsp;&nbsp;
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 97px" class="tableData" align="right">
                                                            <asp:Label ID="lblBillAddr2" runat="server"><%=Resources.Resource.repair_svcacc_adrs_msg_1()%></asp:Label>
                                                        </td>
                                                        <td>
                                                            <SPS:SPSTextBox ID="txtAddr2" runat="server" CssClass="tableData" Width="184px" MaxLength="50"></SPS:SPSTextBox><asp:Label ID="LabelLineBill2Star" runat="server" CssClass="redAsterick"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <%-- By Sneha on 25/03/2014: The following row is hidden from user to fix bug 8955 (bugzilla)--%>
                                                    <tr runat="server" style="display: none;">
                                                        <td style="width: 97px" class="tableData" align="right">
                                                            <asp:Label ID="lblBillAddr3" runat="server"><%=Resources.Resource.repair_svcacc_adrs_msg_2()%></asp:Label>
                                                        </td>
                                                        <td>
                                                            <SPS:SPSTextBox ID="txtAddr3" runat="server" CssClass="tableData" Width="184px" MaxLength="50"></SPS:SPSTextBox><asp:Label ID="LabelLineBill3Star" runat="server" CssClass="redAsterick"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 97px" class="tableData" align="right">
                                                            <asp:Label ID="lblBillCity" runat="server"><%=Resources.Resource.repair_svcacc_city_msg()%></asp:Label>
                                                        </td>
                                                        <td>
                                                            <SPS:SPSTextBox ID="txtCity" runat="server" CssClass="tableData" Width="184px" MaxLength="50"></SPS:SPSTextBox><asp:Label ID="LabelLineBill4Star" runat="server" CssClass="redAsterick"></asp:Label><span class="redAsterick">*</span>&nbsp;&nbsp;
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 97px" class="tableData" align="right">
                                                            <asp:Label ID="lblBillState" runat="server"><%=Resources.Resource.repair_svcacc_state_msg()%></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlBillToState" runat="server" CssClass="tableData"></asp:DropDownList><span class="redAsterick">*</span>&nbsp;&nbsp;
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 97px" class="tableData" align="right">
                                                            <asp:Label ID="lblBillZip" runat="server"><%=Resources.Resource.repair_svcacc_zip_msg()%></asp:Label>
                                                        </td>
                                                        <td>
                                                            <SPS:SPSTextBox ID="txtZip" runat="server" CssClass="tableData" MaxLength="10"></SPS:SPSTextBox><span class="redAsterick">*</span>&nbsp;&nbsp;
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 97px" class="tableData" align="right">
                                                            <asp:Label ID="lblBillPhNo" runat="server"><%=Resources.Resource.repair_svcacc_phno_msg()%></asp:Label>
                                                        </td>
                                                        <td>
                                                            <SPS:SPSTextBox ID="txtPhone" runat="server" CssClass="tableData" Width="100px"></SPS:SPSTextBox><span class="redAsterick">*</span>&nbsp;&nbsp;<span class="bodyCopy">xxx-xxx-xxxx
                                                                <asp:Label ID="lblErrMsgBillPhone" runat="server" ForeColor="Red" Visible="False" Width="189px"><%=Resources.Resource.repair_svcacc_plsphno_msg()%></asp:Label></span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 97px" class="tableData" align="right"><%=Resources.Resource.repair_svcacc_phnextn_msg() %></td>
                                                        <td>
                                                            <SPS:SPSTextBox ID="txtPhExtn" runat="server" CssClass="tableData" MaxLength="4" Width="64px"></SPS:SPSTextBox>&nbsp;&nbsp;<span class="bodyCopy">xxxx</span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 97px" class="tableData" align="right">
                                                            <asp:Label ID="lblBillFax" runat="server" Text="FAX:"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <SPS:SPSTextBox ID="txtFax" runat="server" CssClass="tableData" MaxLength="12" Width="100px"></SPS:SPSTextBox>&nbsp;&nbsp;<span class="bodyCopy">xxx-xxx-xxxx</span>&nbsp;
                                                                <asp:Label ID="lblErrmsgBillFax" runat="server" CssClass="bodycopy" ForeColor="Red"
                                                                    Text="Please enter valid FAX no." Visible="False"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">&nbsp;</td>
                                                    </tr>
                                                    <%end if %>
                                                    <tr valign="middle" bgcolor="#d5dee9">
                                                        <td style="width: 97px"><span class="tableHeader"><%=Resources.Resource.repair_svcacc_shipto_msg() %></span></td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                    <%If bShow = True Then%>
                                                    <tr>
                                                        <td style="width: 97px" class="tableData"></td>
                                                        <td class="tableData">
                                                            <input id="chkCopy" type="checkbox" runat="server" class="tableData" onclick="javascript: copyAddress();" /><%=Resources.Resource.repair_snysvc_nachldrcpy_msg() %>
                                                        </td>
                                                    </tr>
                                                    <%end if %>

                                                    <tr>
                                                        <td style="width: 97px" class="tableData" align="right">
                                                        </td>
                                                        <td>
                                                            <asp:RadioButtonList ID="rdoShipOption" CssClass="tabledata" runat="server">
                                                                <asp:ListItem Selected="True" Value="0" Text="<%$ Resources:Resource,repair_svcacc_shipaddrs_msg %>"></asp:ListItem>
                                                                <asp:ListItem Value="1" Text="<%$ Resources:Resource,repair_svcacc_hldsvc_msg %>"></asp:ListItem>
                                                            </asp:RadioButtonList>

                                                        <td>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 97px" class="tableData" align="right">
                                                            <asp:Label ID="lblShpCompany" runat="server"><%=Resources.Resource.repair_svcacc_cmyname_msg() %></asp:Label>
                                                        </td>
                                                        <td>
                                                            <SPS:SPSTextBox ID="txtShpCompany" runat="server" CssClass="tableData" MaxLength="100" Width="184px"></SPS:SPSTextBox><span class="redAsterick">*</span>&nbsp;&nbsp;
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style="width: 97px" class="tableData" align="right">
                                                            <asp:Label ID="lblShpAttn" runat="server"><%=Resources.Resource.repair_svcacc_attname_msg()%></asp:Label>
                                                        </td>
                                                        <td>
                                                            <SPS:SPSTextBox ID="txtShpAttnName" runat="server" CssClass="tableData" MaxLength="100" Width="184px"></SPS:SPSTextBox><span class="redAsterick">*</span>&nbsp;&nbsp;
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 97px" class="tableData" align="right">
                                                            <asp:Label ID="lblShpAddr1" runat="server"><%=Resources.Resource.repair_svcacc_adrs_msg()%></asp:Label>
                                                        </td>
                                                        <td>
                                                            <SPS:SPSTextBox ID="txtShpAddr1" runat="server" CssClass="tableData" MaxLength="50" Width="184px"></SPS:SPSTextBox><asp:Label ID="LabelLineShip1Star" runat="server" CssClass="redAsterick"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 97px" class="tableData" align="right">
                                                            <asp:Label ID="lblShpAddr2" runat="server"><%=Resources.Resource.repair_svcacc_adrs_msg_1 ()%></asp:Label>
                                                        </td>
                                                        <td>
                                                            <SPS:SPSTextBox ID="txtShpAddr2" runat="server" CssClass="tableData" Width="184px" MaxLength="50"></SPS:SPSTextBox><asp:Label ID="LabelLineShip2Star" runat="server" CssClass="redAsterick"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr runat="server" visible="false">
                                                        <td style="width: 97px" class="tableData" align="right">
                                                            <asp:Label ID="lblShpAddr3" runat="server"><%=Resources.Resource.repair_svcacc_adrs_msg_2()%></asp:Label>
                                                        </td>
                                                        <td>
                                                            <SPS:SPSTextBox ID="txtShpAddr3" runat="server" CssClass="tableData" Width="184px" MaxLength="50"></SPS:SPSTextBox><asp:Label ID="LabelLineShip3Star" runat="server" CssClass="redAsterick"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 97px" class="tableData" align="right">
                                                            <asp:Label ID="lblShpCity" runat="server"><%=Resources.Resource.repair_svcacc_city_msg() %></asp:Label>
                                                        </td>
                                                        <td>
                                                            <SPS:SPSTextBox ID="txtShpCity" runat="server" CssClass="tableData" Width="184px" MaxLength="50"></SPS:SPSTextBox><asp:Label ID="LabelLineShip4Star" runat="server" CssClass="redAsterick"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 97px" class="tableData" align="right">
                                                            <asp:Label ID="lblShpState" runat="server"><%=Resources.Resource.repair_svcacc_state_msg()%></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlShpState" runat="server" CssClass="tableData"></asp:DropDownList><span class="redAsterick">*</span>&nbsp;&nbsp;
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 97px" class="tableData" align="right">
                                                            <asp:Label ID="lblShpZip" runat="server"><%=Resources.Resource.repair_svcacc_zip_msg()%></asp:Label>
                                                        </td>
                                                        <td>
                                                            <SPS:SPSTextBox ID="txtShpZip" runat="server" CssClass="tableData" MaxLength="10"></SPS:SPSTextBox><span class="redAsterick">*</span>&nbsp;&nbsp;
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 97px" class="tableData" align="right">
                                                            <asp:Label ID="lblShpPhno" runat="server"><%=Resources.Resource.repair_svcacc_phno_msg()%></asp:Label>
                                                        </td>
                                                        <td>
                                                            <SPS:SPSTextBox ID="txtShpPhone" runat="server" CssClass="tableData" Width="100px"></SPS:SPSTextBox>
                                                            <span class="redAsterick">*</span>&nbsp;&nbsp;<span class="bodyCopy">xxx-xxx-xxxx
                                                                    <asp:Label ID="lblErrMsgShpPhoneno" runat="server" ForeColor="Red"
                                                                        Visible="False" Width="175px"><%=Resources.Resource.repair_svcacc_plsphno_msg()%></asp:Label></span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 97px" class="tableData" align="right"><%=Resources.Resource.repair_svcacc_phnextn_msg() %></td>
                                                        <td>
                                                            <SPS:SPSTextBox ID="txtShpExtension" runat="server" CssClass="tableData" MaxLength="4" Width="64px"></SPS:SPSTextBox>&nbsp;&nbsp;<span class="bodyCopy">xxxx</span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 97px" class="tableData" align="right">
                                                            <asp:Label ID="lblShpFax" runat="server"> <%=Resources.Resource.repair_svcacc_fxno_msg()%></asp:Label>
                                                        </td>
                                                        <td>
                                                            <SPS:SPSTextBox ID="txtShpFax" runat="server" CssClass="tableData" MaxLength="12" Width="100px"></SPS:SPSTextBox>&nbsp;&nbsp;<span class="bodyCopy">xxx-xxx-xxxx</span>
                                                            <asp:Label ID="lblMsgErrShpFaxno" runat="server" Width="190px" CssClass="bodycopy" ForeColor="Red" Visible="False"><%=Resources.Resource.repair_svcacc_plsfxno_msg()%></asp:Label>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td colspan="2">&nbsp;</td>
                                                    </tr>
                                                    <%If bShow = True Then%>
                                                    <tr>
                                                        <td style="width: 97px" class="tableData" align="right">
                                                            <asp:Label ID="lblPONumber" runat="server" Text="<%$ Resources:Resource,repair_svcacc_pono_msg%>"></asp:Label>
                                                        </td>
                                                        <td class="tableData">
                                                            <SPS:SPSTextBox ID="txtPONumber" runat="server" CssClass="tableData" MaxLength="20"></SPS:SPSTextBox>&nbsp;<span class="redAsterick">*</span>
                                                            <input type="checkbox" id="noPoRequiredCheckBox" runat="server" class="tableData" onclick="javascript: poNotRequired();" /><%=Resources.Resource.repair_svcacc_cmpnyordr_msg()%>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 97px" class="tableData" align="right">
                                                            <asp:Label ID="lblApprvl" runat="server" Text="<%$ Resources:Resource,repair_svcacc_aprvl_msg%>"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:CheckBox ID="chkApproval" runat="server" Text="<%$ Resources:Resource,repair_snysvc_cfrmpgt_msg_9%>" CssClass="bodyCopy" />
                                                            <span class="redAsterick">*</span>&nbsp;<div id="msgError" class="bodycopy" style="visibility: hidden; display: inline;"><span color="red">???</span></div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style="width: 97px" class="tableData" align="right"><%=Resources.Resource.repair_svcacc_txexe_msg() %></td>
                                                        <td>
                                                            <asp:CheckBox ID="chkTaxexempt" runat="server" CssClass="bodycopy" Text="<%$ Resources:Resource,repair_snysvc_nachldrcpy_msg_1%>" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 97px" class="tableData" align="right"><%=Resources.Resource.repair_svcacc_pytyp_msg() %></td>
                                                        <td>
                                                            <span class="bodyCopy">
                                                                <asp:RadioButtonList ID="rdPayment" runat="server" CssClass="bodycopy" RepeatDirection="Horizontal"
                                                                    RepeatLayout="Flow">
                                                                    <asp:ListItem Value="2" Selected="True" Text="<%$ Resources:Resource,repair_svcacc_crdtcrd_msg%>"></asp:ListItem>
                                                                    <asp:ListItem Value="3" Text="<%$ Resources:Resource,repair_svcacc_chk_msg%>"></asp:ListItem>
                                                                </asp:RadioButtonList></span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">&nbsp;</td>
                                                    </tr>
                                                    <%End If%>

                                                    <tr>
                                                        <td align="right" class="tableData" style="width: 97px; height: 30px;">
                                                            <asp:Label ID="Label1" runat="server"><%=Resources.Resource.repair_svcacc_ctname_msg()%></asp:Label>
                                                        </td>
                                                        <td style="width: 555px; height: 30px;">
                                                            <SPS:SPSTextBox ID="txtContactName" runat="server" CssClass="tableData" MaxLength="100"
                                                                Width="184px"></SPS:SPSTextBox><span class="redAsterick">*</span>&nbsp;&nbsp;
                                                                    <asp:Label ID="lblContactName" runat="server" CssClass="bodycopy" ForeColor="Red"
                                                                        Visible="False"><%=Resources.Resource.repair_svcacc_plscntpn_msg()%></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right" class="tableData" style="width: 97px">
                                                            <asp:Label ID="lblEmailAddr" runat="server"><%=Resources.Resource.repair_svcacc_cntemladrs_msg()%></asp:Label>
                                                        </td>
                                                        <td style="width: 555px">
                                                            <SPS:SPSTextBox ID="txtEmailAddr" runat="server" CssClass="tableData" MaxLength="100"
                                                                Width="184px"></SPS:SPSTextBox><span class="redAsterick">*</span>&nbsp;&nbsp;
                                                                    <asp:Label ID="lblErrMsgEmailAddr" runat="server" CssClass="bodycopy" ForeColor="Red"
                                                                        Visible="False"><%=Resources.Resource.repair_svcacc_plsemladrs_msg()%></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2" style="height: 34px">
                                                            <asp:ImageButton ID="imgBtnNext" runat="server" ImageUrl="<%$Resources:Resource,img_btnNext%>" />&nbsp;
                                                        <a href='sony-repair.aspx'>
                                                            <img src="<%=Resources.Resource.sna_svc_img_7()%>" alt='Click to cancel' border="0" /></a>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td style="width: 5px; height: 107px;">
                                                <img height="20" src="images/spacer.gif" width="20" alt="">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 5px">&nbsp;</td>
                                            <td colspan="2">&nbsp;</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>

                                <td width="582">
                                    <footer><ServicePLUSWebApp:SonyFooter ID="SonyFooter1" runat="server" /></footer>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="25" style="background: url('images/sp_right_bkgd.gif')">
                        <img src="images/spacer.gif" width="25" height="20" alt="">
                    </td>
                </tr>
            </table>
        </center>
    </form>
</body>
</html>
