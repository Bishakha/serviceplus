<%@ Reference Page="~/SSL.aspx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="SonyFooter" Src="SonyFooter.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="PersonalMessage" Src="PersonalMessage.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="ServicePlusNav" Src="ServicePlusNav.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="TopSonyHeader" Src="TopSonyHeader.ascx" %>

<%@ Page Language="vb" AutoEventWireup="false" Inherits="ServicePLUSWebApp.Sony_EWCertificate"
    EnableViewStateMac="false" CodeFile="Sony-EWCertificate.aspx.vb" CodeFileBaseClass="ServicePLUSWebApp.SSL" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="<%=PageLanguage %>">
<head>

<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta http-equiv="Pragma" content="no-cache">
    <link href="includes/ServicesPLUS_style.css" type="text/css" rel="stylesheet">

    <script language="javascript" src="includes/ServicesPLUS.js" type="text/javascript">  
        
    </script>
    <script type="text/javascript" src="includes/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="includes/svcplus_globalization.js"></script>
    
    <title>Sony Extended Warranties</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <meta content="Sony Parts, Sony Professional Parts, Sony Broadcast Parts, Sony Business Parts, Sony Software, Sony Professional Software, Sony Broadcast Software, Sony Business Software"
        name="keywords">
    <link href="includes/ServicesPLUS_style.css" type="text/css" rel="stylesheet" />
</head>
<body text="#000000" onload="window.location='<%=filename %>'" bgcolor="#5d7180" leftmargin="0" topmargin="0">
<%--<tr width="100%">
                            <td width="100%">
                                <iframe width="100%" src="<%=filename %>" height="100%"></iframe>
                                
                            </td>
                        </tr>--%>
   
</body>
</html>
