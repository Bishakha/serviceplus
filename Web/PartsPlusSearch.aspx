<%@ Reference Page="~/SSL.aspx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="ServicePlusNav" Src="ServicePlusNav.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="TopNav" Src="TopNav.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="PersonalMessage" Src="PersonalMessage.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="TopSonyHeader" Src="TopSonyHeader.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="SonyFooter" Src="SonyFooter.ascx" %>

<%@ Page Language="vb" AutoEventWireup="true" Inherits="ServicePLUSWebApp.PartsPlusSearch"
    EnableViewStateMac="true" CodeFile="PartsPlusSearch.aspx.vb" CodeFileBaseClass="ServicePLUSWebApp.SSL" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="<%=PageLanguage %>">
<head>
    <title>
        <%=Resources.Resource.ttl_Parts%></title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <link href="themes/base/jquery.ui.all.css" rel="stylesheet" type="text/css" />
    <%--<link href="includes/jquery.cluetip.css" rel="stylesheet" type="text/css" />--%>
    <link href="includes/ServicesPLUS_style.css" type="text/css" rel="stylesheet" />
    <link href="includes/jquery-ui.min.css" rel="stylesheet" type="text/css" />
    <link href="includes/jquery-ui.structure.min.css" rel="stylesheet" type="text/css" />

    <!--<script src="includes/jquery-1.4.2.min.js" type="text/javascript"></script>
    <script src="includes/jquery.ui.dialog.js" type="text/javascript"></script>
    <script src="includes/jquery.ui.widget.js" type="text/javascript"></script>
    <script src="includes/jquery.ui.core.js" type="text/javascript"></script>
    <script src="includes/jquery.cluetip.js" type="text/javascript"></script>
    <script src="includes/jquery.ui.mouse.js" type="text/javascript"></script>
    <script src="includes/jquery.ui.draggable.js" type="text/javascript"></script>
    <script src="includes/jquery.ui.position.js" type="text/javascript"></script>
    <script src="includes/jquery.ui.resizable.js" type="text/javascript"></script>-->
    <script src="includes/jquery-3.1.1.min.js" type="text/javascript"></script>
    <script src="includes/jquery-ui.min.js" type="text/javascript"></script>

    <script src="includes/ServicesPLUS.js" type="text/javascript"></script>
    <script src="includes/svcplus_globalization.js" type="text/javascript"></script>
    <script src="includes/ShoppingCart.js" type="text/javascript"></script>
</head>
<body style="margin: 0px; background: #5d7180; color: Black;">
    <div id="ValidationMessageDiv"></div>
    <div id="dialog" title="<%= Resources.Resource.atc_title_QuickOrderStatus%>">
        <div id="divPrg" style="font-size: 11px; font-family: Arial; overflow: auto;"></div>
        <div id="divMessage" style="font-size: 11px; font-family: Arial; overflow: auto;"></div>
        <div id="divException" style="font-family: Arial, Helvetica, sans-serif; font-size: 11px; line-height: 14px; overflow: auto; display: none;">
        </div>
        <div id="divCloseButton" style="display: none;">
            <br />
            <a href="#">
                <img src="<%=Resources.Resource.img_btnCloseWindow %>"
                    alt="Close" onclick="javascript:return closedilog();" id="messageclose" /></a>
        </div>
    </div>
    <form id="Form1" method="post" runat="server">
        <asp:HiddenField ID="atc_ex_InvalidPartNumber" runat="server" Value="<%$ Resources:Resource, atc_ex_InvalidPartNumber %>" />
        <asp:HiddenField ID="atc_ex_InvalidQuantityForPart" runat="server" Value="<%$ Resources:Resource, atc_ex_InvalidQuantityForPart %>" />
        <asp:HiddenField ID="atc_ex_InvalidPartAndQuantity" runat="server" Value="<%$ Resources:Resource, atc_ex_InvalidPartAndQuantity %>" />
        <asp:HiddenField ID="atc_ex_DifferentCart" runat="server" Value="<%$ Resources:Resource, atc_ex_DifferentCart %>" />
        <asp:HiddenField ID="atc_title_OrderStatus" runat="server" Value="<%$ Resources:Resource, atc_title_OrderStatus %>" />
        <asp:HiddenField ID="atc_title_OrderExceptions" runat="server" Value="<%$ Resources:Resource, atc_title_OrderExceptions %>" />
        <asp:HiddenField ID="atc_title_QuickOrderStatus" runat="server" Value="<%$ Resources:Resource, atc_title_QuickOrderStatus %>" />
        <asp:HiddenField ID="atc_title_QuickOrderExceptions" runat="server" Value="<%$ Resources:Resource, atc_title_QuickOrderExceptions %>" />
        <asp:HiddenField ID="atc_AddingItem" runat="server" Value="<%$ Resources:Resource, atc_AddingItem %>" />
        <asp:HiddenField ID="atc_ToCart" runat="server" Value="<%$ Resources:Resource, atc_ToCart %>" />
        <asp:HiddenField ID="atc_warn_LongWait" runat="server" Value="<%$ Resources:Resource, atc_warn_LongWait %>" />
        <asp:HiddenField ID="prg_PleaseWait" runat="server" Value="<%$ Resources:Resource, prg_PleaseWait %>" />
        <asp:HiddenField ID="img_btnCloseWindow" runat="server" Value="<%$ Resources:Resource, img_btnCloseWindow %>" />
        <asp:HiddenField ID="quickOrderMaxItems" runat="server" Value="" />
        <asp:HiddenField ID="orderUploadMaxItems" runat="server" Value="" />
        <center>
            <table width="760" border="0" role="presentation">
                <tr>
                    <td width="25" style="background: url('images/sp_left_bkgd.gif')">
                        <img height="25" src="images/spacer.gif" width="25" alt="">
                    </td>
                    <td width="710" bgcolor="#ffffff">
                        <table width="710" border="0" role="presentation">
                            <tr>
                                <td>
                                    <ServicePLUSWebApp:TopSonyHeader ID="TopSonyHeader1" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <nav><ServicePLUSWebApp:ServicePlusNav ID="SonyNav" runat="server" /></nav>
                                </td>
                            </tr>
                            <tr>
                                <td width="711">
                                    <table width="584" border="0" height="70" role="presentation">
                                        <tr>
                                            <td style="width: 464px; height: 60px; background: #363d45 url('images/sp_int_header_top_partsPLUS_short.gif'); text-align: right; vertical-align: middle;">
                                                <h1 class="headerText"><%=Resources.Resource.el_Pts_MainHeading%></h1>
                                            </td>
                                            <td style="vertical-align: middle; text-align: center; background: #363d45;">
                                                <ServicePLUSWebApp:PersonalMessage ID="PersonalMessageDisplay" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td bgcolor="#f2f5f8">
                                                <h2 class="headerTitle" style="padding-right: 20px; text-align: right;"><%=Resources.Resource.el_SearchResult%></h2>
                                            </td>
                                            <td bgcolor="#99a8b5">
                                                <asp:ImageButton ID="btnNewSearch" runat="server" AlternateText="New Search" ImageUrl="<%$ Resources:Resource,img_btnNewSearch%>" />
                                            </td>
                                        </tr>
                                        <tr height="9">
                                            <td bgcolor="#f2f5f8">
                                                <img height="9" src="images/sp_int_header_btm_left_onepix.gif" width="464" alt="">
                                            </td>
                                            <td bgcolor="#99a8b5">
                                                <img height="9" src="images/sp_int_header_btm_right.gif" width="246" alt="">
                                            </td>
                                        </tr>
                                    </table>
                                    <img height="20" src="images/spacer.gif" width="20" alt="">
                                    <asp:Label ID="ErrorLabel" runat="server" CssClass="redAsterick" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="710" border="0" role="presentation">
                                        <tr>
                                            <td width="20" height="20">
                                                <img width="20" height="20" src="images/spacer.gif" alt="">
                                            </td>
                                            <td width="670" height="20">
                                                <img width="670" height="20" src="images/spacer.gif" alt="">
                                            </td>
                                            <td width="20" height="20">
                                                <img width="20" height="20" src="images/spacer.gif" alt="">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="20">
                                                <img width="20" height="20" src="images/spacer.gif" alt="">
                                            </td>
                                            <td width="670">
                                                <a name="top"></a>
                                                <table width="670" border="0" role="presentation">
                                                    <tr bgcolor="#b7b7b7">
                                                        <td colspan="3" bgcolor="#b7b7b7">
                                                            <img height="1" src="images/spacer.gif" width="1" alt="">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="1" bgcolor="#b7b7b7">
                                                            <img height="25" src="images/spacer.gif" width="1" alt="" />
                                                        </td>
                                                        <td width="668" bgcolor="#ffffff">
                                                            <img height="25" src="<%=Resources.Resource.img_refine_search_header%>" width="668"
                                                                border="0" alt="" />
                                                        </td>
                                                        <td width="1" bgcolor="#b7b7b7">
                                                            <img height="25" src="images/spacer.gif" width="1" alt="" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="1" bgcolor="#b7b7b7">
                                                            <img height="20" src="images/spacer.gif" width="1" alt="">
                                                        </td>
                                                        <td class="tableHeader">
                                                            <table width="663" border="0" role="presentation">
                                                                <tr>
                                                                    <td width="5">
                                                                        <img height="8" src="images/spacer.gif" width="5" alt="" />
                                                                    </td>
                                                                    <td valign="top" width="260">
                                                                        <p>
                                                                            <img height="12" src="<%=Resources.Resource.sna_svc_img_31%>" width="65" alt="Current Model" /><br />
                                                                            <asp:Label ID="lblModel" runat="server" CssClass="popupCurrentModel" /><br />
                                                                            <asp:Label ID="lblSerial" runat="server" CssClass="bodyCopy" />
                                                                        </p>
                                                                        <p>
                                                                            &nbsp;
                                                                        </p>
                                                                    </td>
                                                                    <td valign="top" width="5">
                                                                        <img src="images/spacer.gif" width="5" height="4" alt="" />
                                                                    </td>
                                                                    <td valign="top" width="175">
                                                                        <table width="275" border="0" role="presentation">
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:Label ID="lblInstructions" runat="server" CssClass="bodyCopy" Text="<%$Resources:Resource,el_SearchValidate%>" />
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:Label ID="lblOptions" runat="server" CssClass="bodyCopy" Text="<%$Resources:Resource,el_Option%>" />
                                                                                    <asp:Label ID="lblSingleOption" runat="server" CssClass="bodyCopy" Visible="false" />
                                                                                    <asp:DropDownList ID="ddlOptions" runat="server" CssClass="bodyCopy" AutoPostBack="true"
                                                                                        AppendDataBoundItems="true" DataTextField="ModelID" DataValueField="ModelID" />
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:Label ID="lblCategories" runat="server" CssClass="bodyCopy" Text="<%$Resources:Resource,el_Category%>" />
                                                                                    <asp:Label ID="lblSingleCategory" runat="server" CssClass="bodyCopy" Visible="false" />
                                                                                    <asp:DropDownList ID="ddlCategories" runat="server" CssClass="bodyCopy" AutoPostBack="True"
                                                                                        AppendDataBoundItems="true" DataTextField="Description" DataValueField="CategoryID" />
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:Label ID="lblModules" runat="server" CssClass="bodyCopy" Text="<%$Resources:Resource,el_Module%>" />
                                                                                    <asp:Label ID="lblSingleModule" runat="server" CssClass="bodyCopy" Visible="false" />
                                                                                    <asp:DropDownList ID="ddlModules" runat="server" CssClass="bodyCopy" AutoPostBack="True"
                                                                                        AppendDataBoundItems="true" DataTextField="Name" DataValueField="ModuleID" />
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                    <td width="15">
                                                                        &nbsp;
                                                                    </td>
                                                                    <td valign="top">
                                                                        <asp:Panel ID="pnlRefineSearch" runat="server" Width="200">
                                                                            <asp:Label ID="lblRefine" runat="server" CssClass="redAsterick" />
                                                                            <SPS:SPSTextBox ID="txtRefineSearch" runat="server" CssClass="bodyCopy" />
                                                                            <br />
                                                                            <asp:ImageButton ID="btnRefineSearch" runat="server" AlternateText="Refine Search"
                                                                                ImageUrl="<%$Resources:Resource,img_btnRefinedSearch%>" />
                                                                            <br />
                                                                            <asp:RadioButtonList ID="rblRefineSearchType" Width="200px" CssClass="bodyCopy" runat="server">
                                                                                <asp:ListItem Value="1" Selected="True" Text="<%$Resources:Resource,el_ByDescription%>" />
                                                                                <asp:ListItem Value="2" Text="<%$Resources:Resource,el_ComponentRefNo%>" />
                                                                            </asp:RadioButtonList>
                                                                            <br />
                                                                            <asp:Label ID="lblClearFields" runat="server" CssClass="bodyCopy" Text="<%$Resources:Resource,search_clear_fields %>" />
                                                                            <asp:ImageButton ID="btnClearFields" runat="server" ImageUrl="<%$Resources:Resource,img_btnDelete%>"
                                                                                AlternateText="Delete Fields" />
                                                                        </asp:Panel>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td width="1" bgcolor="#b7b7b7">
                                                            <img height="20" src="images/spacer.gif" width="1" alt="">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3" bgcolor="#b7b7b7">
                                                            <img height="1" src="images/spacer.gif" width="1" alt="">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3" bgcolor="#b7b7b7">
                                                            <img height="1" src="images/spacer.gif" width="1" alt="">
                                                        </td>
                                                    </tr>
                                                </table>
                                                <img height="20" src="images/spacer.gif" width="670" alt="" /><br />
                                                <asp:Label ID="lblGoToError" CssClass="redAsterick" runat="server" />

                                                <%-- SEARCH RESULTS GRIDVIEW --%>
                                                <asp:GridView ID="grdPartsList" AllowPaging="true" PageSize="50" AutoGenerateColumns="false" Width="670"
                                                    DataKeyNames="PartNumber" HeaderStyle-CssClass="tableHeader" PagerStyle-CssClass="tableData"
                                                    runat="server" OnRowDataBound="grdPartsList_RowDataBound" OnPageIndexChanging="grdPartsList_PageIndexChanging">
                                                    <Columns>
                                                        <asp:BoundField HeaderText="<%$Resources:Resource,el_RefNo %>" DataField="PartReferenceID"
                                                            NullDisplayText="-" HtmlEncode="false" />
                                                        <asp:BoundField HeaderText="<%$Resources:Resource,el_PartsRequesterd %>" DataField="PartNumber"
                                                            NullDisplayText="-" HtmlEncode="false" />
                                                        <asp:TemplateField HeaderText="<%$Resources:Resource,el_PartsSupplied %>">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblReplacementPart" runat="server"><%# Eval("ReplacementPartNumber") %></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:BoundField HeaderText="<%$Resources:Resource,el_Description %>" DataField="Description"
                                                            NullDisplayText="-" HtmlEncode="false" />
                                                        <asp:TemplateField HeaderText="<%$Resources:Resource,el_PriceList %>">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblPrice" Text='<%# Eval("ListPrice", "{0:c}") %>' runat="server"
                                                                    Visible='<%# Eval("ListPrice") > 0.0R%>' />
                                                                <asp:HyperLink ID="lnkAvailability" CssClass="tableHeader" Visible='<%# Eval("ListPrice") > 0.0R%>' runat="server" />
                                                                <asp:Label ID="lblCallUs" runat="server" Text="<%$Resources:Resource,el_CallInfo%>"
                                                                    Visible='<%# Eval("ListPrice") = 0.0R%>' />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="<%$Resources:Resource,el_CartAdd %>">
                                                            <ItemTemplate>
                                                                <asp:ImageButton ID="cmdAddToCart" ImageUrl="images/sp_int_add2Cart_btn.gif"
                                                                    Visible='<%# Eval("ListPrice") > 0.0R %>' runat="server" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="<%$Resources:Resource,el_Cat %>">
                                                            <ItemTemplate>
                                                                <asp:ImageButton ID="cmdAddToCatalog" ImageUrl="images/sp_int_add2myCat_btn.gif"
                                                                    CommandName="AddToCatalog" CommandArgument="<%# Container.DataItemIndex %>"
                                                                    Visible='<%# Eval("ListPrice") > 0.0R %>' runat="server" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="<%$Resources:Resource,el_ExplView %>">
                                                            <ItemTemplate>
                                                                <asp:ImageButton ID="cmdExplodedView" CommandName="ExplodedView" CommandArgument="<%# Container.DataItemIndex %>"
                                                                    ImageUrl="images/sp_int_expandView_btn.gif" Visible='<%# Eval("HasExplodedView") %>'
                                                                    runat="server" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <RowStyle BackColor="#FFFFFF" BorderColor="#D5DEE9" CssClass="tableData" />
                                                    <AlternatingRowStyle BackColor="#F2F5F8" BorderColor="#D5DEE9" CssClass="tableData" />
                                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" />
                                                    <EmptyDataTemplate>
                                                        <asp:Label CssClass="tableHeader" runat="server" Text="<%$Resources:Resource,el_Search_NoResult%>" />
                                                    </EmptyDataTemplate>
                                                </asp:GridView>
                                                <asp:Label ID="BackToTopLabel" runat="server" />
                                            </td>
                                            <td width="20">
                                                <img height="20" src="images/spacer.gif" width="20" alt="" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" align="left" id="tdLegend" runat="server">
                                    <img height="20" src="images/spacer.gif" width="20" alt="" /><span style="font-family: Wingdings; color: red; font-size: 12px;">v</span>
                                    <span class="bodyCopy"> <asp:HyperLink runat="server" ID="lnkPromotions" NavigateUrl="promotions.aspx"
                                        Text="<%$Resources:Resource,el_LimitedPromoPrice%>" /></span>
                                </td>
                            </tr>
                            <%--<tr>
                                <td align="left" colspan="3">
                                    <SPS:SPSTextBox ID="hidespstextitemno" runat="server" Style="display: none" />
                                    <SPS:SPSTextBox ID="hidespstextqty" runat="server" Style="display: none" />
                                </td>
                            </tr>--%>
                            <tr>
                                <td>
                                    <footer><ServicePLUSWebApp:SonyFooter ID="SonyFooter1" runat="server" /></footer>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="25" style="background: url('images/sp_right_bkgd.gif')">
                        <img src="images/spacer.gif" width="25" height="20" alt="">
                    </td>
                </tr>
            </table>
        </center>
        <map name="Footer_map" id="Footer_map">
            <area shape="rect" coords="268,33,330,44" href="#" alt="" />
            <area shape="rect" coords="336,33,422,44" href="#" alt="" />
            <area shape="rect" coords="429,33,481,44" href="#" alt="" />
            <area shape="rect" coords="488,33,570,44" href="#" alt="" />
        </map>
    </form>
</body>
</html>
