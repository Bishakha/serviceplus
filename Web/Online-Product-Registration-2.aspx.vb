﻿Imports Sony.US.ServicesPLUS.Process
Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.SIAMUtilities
Imports ServicesPlusException

Namespace ServicePLUSWebApp
    Partial Class Online_Product_Registration_2
        Inherits SSL
        'Dim objUtilties As Utilities = New Utilities
        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            'Load Model Prefix
            Try
                'ddlModel.Items.Insert(0, New ListItem("Select a model prefix above before proceeding"))'6873
                If HttpContextManager.Customer IsNot Nothing Then
                    Dim thisCustomer As Customer = HttpContextManager.Customer
                    If Session("customerpr") Is Nothing Then Response.Redirect("Online-Product-Registration.aspx", True)
                    If Not IsPostBack Then
                        LoadModelPrefix()
                        ddlModel.Items.Clear()
                        ddlModel.Items.Insert(0, New ListItem("Select a model prefix above before proceeding"))
                        ddlModel.Attributes.Add("onchange", "javascript: return ModelPopup();")
                    End If
                Else
                    'Response.Redirect("SignIn.aspx?post=prr", True)
                    Response.Redirect("SignIn-Register.aspx?post=prr", True)
                    Return
                End If
            Catch ex As Exception
                errorMessageLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try


        End Sub

        Protected Sub ddlModelPrefix_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlModelPrefix.SelectedIndexChanged
            ''6873 starts
            ''Load Model
            ''If ddlModelPrefix.SelectedIndex > 0 Then
            'Dim txt As System.IO.TextWriter = New System.IO.StreamWriter("c:\\data.txt", True)
            'Try

            '    txt.WriteLine("Data Started")

            '    LoadModel()
            '    txt.WriteLine("Data Loaded")
            '    txt.Close()
            '    'Else
            '    'ddlModel.Items.Clear()
            '    ' ddlModel.Items.Insert(0, New ListItem("Select a model prefix above before proceeding"))
            '    'End If
            'Catch ex As Exception
            '    txt.WriteLine(ex.ToString())
            '    txt.Close()
            'End Try
            ''6873 ends
            '6873 starts
            Try
                LoadModel()
            Catch ex As Exception
                errorMessageLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
            '6873 ends
        End Sub

        Protected Sub ddlPurchasedFrom_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlPurchasedFrom.SelectedIndexChanged
            'Hide and display reseller name state and city field
            '<asp:ListItem Value="0">Click to select</asp:ListItem>
            '<asp:ListItem Value="1">Direct from Sony</asp:ListItem>
            '<asp:ListItem Value="2">Reseller</asp:ListItem>
            '<asp:ListItem Value="3">Other</asp:ListItem>
            Try
                Select Case ddlPurchasedFrom.SelectedIndex
                    Case 0
                        trResellerCity.Visible = False
                        trResellerName.Visible = False
                        trResellerState.Visible = False
                        trResellerOther.Visible = False
                    Case 1
                        trResellerCity.Visible = False
                        trResellerName.Visible = False
                        trResellerState.Visible = False
                        trResellerOther.Visible = False
                    Case 2
                        trResellerCity.Visible = True
                        trResellerName.Visible = True
                        trResellerState.Visible = True
                        trResellerOther.Visible = False
                        If ddlResellerName.Items.Count = 0 Then
                            LoadReseller()
                            populateStateDropdownList(ddlResellerState)
                        End If
                    Case 3
                        trResellerCity.Visible = False
                        trResellerName.Visible = False
                        trResellerState.Visible = False
                        trResellerOther.Visible = True
                    Case Else
                End Select
            Catch ex As Exception
                errorMessageLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Protected Sub ddlProductCount_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlProductCount.SelectedIndexChanged
            Try
                Select Case ddlProductCount.SelectedIndex
                    Case 0
                        trOtherSerial.Visible = False
                    Case 1
                        trOtherSerial.Visible = False
                    Case 2
                        trOtherSerial.Visible = True
                    Case Else
                End Select
            Catch ex As Exception
                errorMessageLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Protected Sub ddlNextProduct_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlNextProduct.SelectedIndexChanged
            Try
                Select Case ddlNextProduct.SelectedValue
                    Case "Other"
                        tlbOtherPurchase.Visible = True
                    Case Else
                        tlbOtherPurchase.Visible = False
                End Select
            Catch ex As Exception
                errorMessageLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Sub LoadModelPrefix()
            Try
                Dim objPRManager As New OnlineProductRegistrationManager()
                Dim oDS = objPRManager.GetModelPrefix()
                ddlModelPrefix.Items.Clear()
                If Not oDS Is Nothing Then
                    ddlModelPrefix.DataTextField = "MODELPREFIX"
                    ddlModelPrefix.DataValueField = "MODELPREFIX"
                    ddlModelPrefix.DataSource = oDS.Tables(0).DefaultView()
                    ddlModelPrefix.DataBind()
                Else
                    Throw New ApplicationException("Exception occurred")
                End If
                ddlModelPrefix.Items.Insert(0, New ListItem("Click..."))
            Catch ex As Exception
                errorMessageLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Sub LoadModel()
            Try
                Dim objPRManager As New OnlineProductRegistrationManager()
                Dim oDS = objPRManager.GetPRModels(ddlModelPrefix.SelectedValue.ToString())
                ddlModel.Items.Clear()
                If oDS IsNot Nothing Then
                    ddlModel.DataTextField = "MODELNUMBER"
                    ddlModel.DataValueField = "MODELNUMBER"
                    ddlModel.DataSource = oDS.Tables(0).DefaultView()
                    ddlModel.DataBind()
                Else
                    Throw New ApplicationException("Exception occurred")
                End If
                ddlModel.Items.Insert(0, New ListItem("Click to select"))
            Catch ex As Exception
                errorMessageLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Sub LoadReseller()
            Try
                Dim objPRManager As New OnlineProductRegistrationManager()
                Dim oDS = objPRManager.GetReseller()
                If oDS IsNot Nothing Then
                    ddlResellerName.DataTextField = "RESELLERNAME"
                    ddlResellerName.DataValueField = "RESELLERCODE"
                    ddlResellerName.DataSource = oDS.Tables(0).DefaultView()
                    ddlResellerName.DataBind()
                Else
                    Throw New ApplicationException("Exception occurred")
                End If
            Catch ex As Exception
                errorMessageLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Protected Sub imgSubmit_Click(ByVal sender As Object, ByVal e As ImageClickEventArgs) Handles imgSubmit.Click
            hdnNavigator.Value = 0
            If Not ValidateEntry() Then Return

            Dim objCustomerPR As New CustomerProductRegistration()
            If Session("customerpr") Is Nothing Then
                Response.Redirect("Online-Product-Registration.aspx", True)
            Else
                objCustomerPR = Session.Item("customerpr")
                objCustomerPR.MODEL_INTERESTED = ddlModel.Text.Trim().ToUpper()

                objCustomerPR.QUESTION1 = "PRODUCT CATEGORY"
                objCustomerPR.ANSWER1 = ddlModelPrefix.Text.Trim().ToUpper()

                objCustomerPR.QUESTION2 = "PURCHASED FROM"
                objCustomerPR.ANSWER2 = ddlPurchasedFrom.Text.Trim().ToUpper()

                ' 2018-05-30 ASleight - This was broken since June 2010, because this compares .ToUpper() to CamelCase values. Fixing it.
                Select Case ddlPurchasedFrom.Text.Trim().ToUpper()
                    Case "RESELLER" ' "Reseller"
                        objCustomerPR.QUESTION3 = "RESELLER NAME"
                        objCustomerPR.ANSWER3 = ddlResellerName.Text.Trim().ToUpper()
                    Case "OTHER" ' "Other"
                        objCustomerPR.QUESTION3 = "RESELLER OTHER FILL IN"
                        objCustomerPR.ANSWER3 = txtOtherPurchasedFrom.Text.Trim().ToUpper()
                    Case "DIRECT FROM SONY" ' "Direct from Sony"
                        objCustomerPR.QUESTION3 = "RESELLER OTHER FILL IN"
                        objCustomerPR.ANSWER3 = "DIRECT FROM SONY"
                End Select
                objCustomerPR.QUESTION4 = "RESELLER CITY"
                objCustomerPR.ANSWER4 = txtResellerCity.Text.Trim().ToUpper()

                objCustomerPR.QUESTION5 = "RESELLER STATE"
                objCustomerPR.ANSWER5 = ddlResellerState.Text.Trim().ToUpper()

                objCustomerPR.QUESTION6 = "PURCHASE DATE"
                objCustomerPR.ANSWER6 = txtDate.Text.Trim().ToUpper()

                objCustomerPR.QUESTION7 = "SERIAL NUMBER(S)"
                objCustomerPR.ANSWER7 = txtSerialNumber.Text.Trim.ToUpper()
                Select Case ddlProductCount.SelectedIndex
                    Case 2
                        If Not String.IsNullOrWhiteSpace(txtSerialNumber2.Text) Then
                            objCustomerPR.ANSWER7 += "," + txtSerialNumber2.Text.Trim.ToUpper()
                        End If
                        If Not String.IsNullOrWhiteSpace(txtSerialNumber3.Text) Then
                            objCustomerPR.ANSWER7 += "," + txtSerialNumber3.Text.Trim.ToUpper()
                        End If
                        If Not String.IsNullOrWhiteSpace(txtSerialNumber4.Text) Then
                            objCustomerPR.ANSWER7 += "," + txtSerialNumber4.Text.Trim.ToUpper()
                        End If
                        If Not String.IsNullOrWhiteSpace(txtSerialNumber5.Text) Then
                            objCustomerPR.ANSWER7 += "," + txtSerialNumber5.Text.Trim.ToUpper()
                        End If
                        If Not String.IsNullOrWhiteSpace(txtSerialNumber6.Text) Then
                            objCustomerPR.ANSWER7 += "," + txtSerialNumber6.Text.Trim.ToUpper()
                        End If
                        If Not String.IsNullOrWhiteSpace(txtSerialNumber7.Text) Then
                            objCustomerPR.ANSWER7 += "," + txtSerialNumber7.Text.Trim.ToUpper()
                        End If
                        If Not String.IsNullOrWhiteSpace(txtSerialNumber8.Text) Then
                            objCustomerPR.ANSWER7 += "," + txtSerialNumber8.Text.Trim.ToUpper()
                        End If
                        If Not String.IsNullOrWhiteSpace(txtSerialNumber9.Text) Then
                            objCustomerPR.ANSWER7 += "," + txtSerialNumber9.Text.Trim.ToUpper()
                        End If
                        If Not String.IsNullOrWhiteSpace(txtSerialNumber10.Text) Then
                            objCustomerPR.ANSWER7 += "," + txtSerialNumber10.Text.Trim.ToUpper()
                        End If
                    Case Else
                End Select

                objCustomerPR.QUESTION8 = "WHAT GENERAL PRODUCT CATEGORY WOULD YOU ANTICIPATE PURCHASING NEXT?"
                objCustomerPR.ANSWER8 = ddlNextProduct.Text.ToUpper()

                objCustomerPR.QUESTION9 = "WHAT GENERAL PRODUCT CATEGORY WOULD YOU ANTICIPATE PURCHASING NEXT? OTHER FILL IN"
                objCustomerPR.ANSWER9 = txtOtherPurchaseCat.Text.Trim().ToUpper()

                objCustomerPR.QUESTION10 = "EXTENDED WARRANTY"
                objCustomerPR.ANSWER10 = String.Empty

            End If
            Dim objPRManager As OnlineProductRegistrationManager = New OnlineProductRegistrationManager()
            Dim returnValue As Int64 = 0
            Try
                objPRManager.BeginTransaction()
                returnValue = objPRManager.SaveProductRegistration(objCustomerPR)
                If returnValue > 0 Then
                    Try
                        Dim objServicesPLUSMailObject As ServicesPLUSMailObject = New ServicesPLUSMailObject()
                        objServicesPLUSMailObject.IsHTML = True
                        objServicesPLUSMailObject.Mail_Priority = Net.Mail.MailPriority.Normal
                        objServicesPLUSMailObject.From = ConfigurationData.GeneralSettings.Email.Admin.ProductRegistrationFromID
                        objServicesPLUSMailObject.Body = GetMailBody(objCustomerPR)
                        objServicesPLUSMailObject.Subject = "Thank you for registering"
                        objServicesPLUSMailObject.SMTP = ConfigurationData.Environment.SMTP.EmailServer
                        objServicesPLUSMailObject.Port = ConfigurationData.Environment.SMTP.Port
                        objServicesPLUSMailObject.To = objCustomerPR.EMAIL

                        If ServicesPLUSMail.SendNetSMTPMail(objServicesPLUSMailObject) = True Then
                            objPRManager.CommitTransaction()
                            hdnNavigator.Value = 1
                        Else
                            Throw New ApplicationException("Error saving registration data.")
                        End If

                        Response.Redirect("Online-Product-Registration-Success.aspx", True)
                    Catch ex As Exception
                        Throw New ApplicationException("Error saving registration data.")
                    End Try
                End If
            Catch ex As Threading.ThreadAbortException
                ' do nothing
            Catch ex As Exception
                objPRManager.RollbackTransaction()
                errorMessageLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Function GetMailBody(ByRef CustomerPR As CustomerProductRegistration) As String
            Dim strMailBody As StringBuilder = New StringBuilder()
            Dim strURL As String = ConfigurationData.Environment.WebSiteURL

            If ConfigurationData.GeneralSettings.Environment = "Development" Then
                strURL = HttpContext.Current.Request.Url.OriginalString.Substring(0, HttpContext.Current.Request.Url.OriginalString.IndexOf("/ServicesPLUSWebApp")) + "/ServicesPLUSWebApp/"
            End If

            strMailBody.Append("<table cellpadding=0 cellspacing=0 border=""0"" style=""width: 93%;font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: Black; border-bottom-width:1px; border-right-color:666666; border-right-style:solid; border-right-width:1px; border-left-color:666666; border-left-style:solid; border-left-width:1px; border-top-color:666666; border-top-style:solid; border-top-width:1px; border-bottom-style:solid; border-bottom-width:1px; border-bottom-color:666666"">")

            strMailBody.Append("<tr><td colspan=""3""  bgcolor=""black"" style=""border-bottom-style:solid; border-bottom-width:thin;""><img src=""" + strURL + "images/hdrlogo_sonymb.jpg"" alt=""Sony Logo""/></td></tr>" + vbNewLine)
            strMailBody.Append("<tr><td colspan=""3"" ><img height=20 src=""" + strURL + "images/spacer.gif"" alt="""" /></td></tr>" + vbNewLine)

            strMailBody.Append("<tr><td colspan=""1""  ><img height=20 src=""" + strURL + "images/spacer.gif"" alt="""" /></td><td><span style=""font-family: Arial, Helvetica, sans-serif; font-size: 13px; line-height: 13px; color: Black ; font-weight: bold"">Sony Professional Product Registration Confirmation</span></td><td colspan=""1""  ><img width=10 src=""" + strURL + "images/spacer.gif"" alt="""" /></td></tr>" + vbNewLine)

            strMailBody.Append("<tr><td colspan=""3"" ><img height=20 src=""" + strURL + "images/spacer.gif"" alt="""" /></td></tr>" + vbNewLine)
            strMailBody.Append("<tr><td colspan=""1""  ><img width=10 src=""" + strURL + "images/spacer.gif"" alt="""" /></td><td><span style=""font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 13px; color: Black ; font-weight: bold"">Dear " + CustomerPR.FIRST_NAME + " " + CustomerPR.LAST_NAME + ",</span></td><td colspan=""1""  ><img width=10 src=""" + strURL + "images/spacer.gif"" alt="""" /></td></tr>" + vbNewLine)

            strMailBody.Append("<tr><td colspan=""3"" ><img height=30 src=""" + strURL + "images/spacer.gif"" alt="""" /></td></tr>" + vbNewLine)
            strMailBody.Append("<tr><td colspan=""1""  ><img width=10 src=""" + strURL + "images/spacer.gif"" alt="""" /></td><td><span style=""font-family: Arial, Helvetica, sans-serif; font-size: 11px; line-height: 15px; color: Black"">Thank you for registering your Sony Professional product. This is a confirmation that you registered the following:</span></td><td colspan=""1""  ><img width=10 src=""" + strURL + "images/spacer.gif"" alt="""" /></td></tr>" + vbNewLine)

            strMailBody.Append("<tr><td colspan=""3""  ><img height=30 src=""" + strURL + "images/spacer.gif"" alt="""" /></td></tr>" + vbNewLine)
            strMailBody.Append("<tr><td colspan=""1""  ><img width=10  src=""" + strURL + "images/spacer.gif"" alt="""" /></td><td><span style=""font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 13px; color: Black ; font-weight: bold"">" + CustomerPR.MODEL_INTERESTED + "</span><span style=""font-family: Arial, Helvetica, sans-serif; font-size: 11px; line-height: 15px; color: Black""> serial(s): " + CustomerPR.ANSWER7 + "</span></td><td colspan=""1""  ><img width=10 src=""" + strURL + "images/spacer.gif"" alt="""" /></td></tr>" + vbNewLine)

            strMailBody.Append("<tr><td colspan=""3""  ><img height=20 src=""" + strURL + "images/spacer.gif"" alt="""" /></td></tr>" + vbNewLine)
            strMailBody.Append("<tr><td colspan=""1""  ><img width=10 src=""" + strURL + "images/spacer.gif"" alt="""" /></td><td><span style=""font-family: Arial, Helvetica, sans-serif; font-size: 11px; line-height: 15px; color: Black"">You are registered to receive product and/or software updates that would apply to this model.</span></td><td colspan=""1""  ><img width=10 src=""" + strURL + "images/spacer.gif"" alt="""" /></td></tr>" + vbNewLine)
            If CustomerPR.SUCCESS_MESSAGE <> "" Then
                strMailBody.Append("<tr><td colspan=""3""  ><img height=20 src=""" + strURL + "images/spacer.gif"" alt="""" /></td></tr>" + vbNewLine)
                strMailBody.Append("<tr><td colspan=""1""  ><img width=10 src=""" + strURL + "images/spacer.gif"" alt="""" /></td><td><span style=""font-family: Arial, Helvetica, sans-serif; font-size: 11px; line-height: 15px; color: Black"">" + CustomerPR.SUCCESS_MESSAGE + "</span></td><td colspan=""1""  ><img width=10 src=""" + strURL + "images/spacer.gif"" alt="""" /></td></tr>" + vbNewLine)
            End If
            strMailBody.Append("<tr><td colspan=""3""  ><img height=20 src=""" + strURL + "images/spacer.gif"" alt="""" /></td></tr>" + vbNewLine)
            strMailBody.Append("<tr><td colspan=""1""  ><img width=10 src=""" + strURL + "images/spacer.gif"" alt="""" /></td><td><span style=""font-family: Arial, Helvetica, sans-serif; font-size: 11px; line-height: 15px; color: Black"">It is recommended that you print a copy of this e-mail and retain it for your records. In the event that your product requires service, please contact Sony at <span class=bodyCopyBold>866-766-9272</span>. At that time you will be instructed on where to ship or bring your Sony product for repair.</span></td><td colspan=""1""  ><img width=10 src=""" + strURL + "images/spacer.gif"" alt="""" /></td></tr>" + vbNewLine)

            strMailBody.Append("<tr><td colspan=""3""  ><img height=20 src=""" + strURL + "images/spacer.gif"" alt="""" /></td></tr>" + vbNewLine)
            strMailBody.Append("<tr><td colspan=""1""  ><img width=10 src=""" + strURL + "images/spacer.gif"" alt="""" /></td><td><span style=""font-family: Arial, Helvetica, sans-serif; font-size: 11px; line-height: 15px; color: Black"">To see complete Terms and Conditions of Sony's Limited Product Warranty, please go to:</span></td><td colspan=""1""  ><img width=10 src=""" + strURL + "images/spacer.gif"" alt="""" /></td></tr>" + vbNewLine)
            strMailBody.Append("<tr><td colspan=""1""  ><img width=10 src=""" + strURL + "images/spacer.gif"" alt="""" /></td><td><span style=""font-family: Arial, Helvetica, sans-serif; font-size: 11px; line-height: 15px; color: Black""><a href=""http://pro.sony.com/bbsc/ssr/services.servicesprograms.bbsccms-services-servicesprograms-warrantyinformation.shtml"">pro.sony.com/bbsc/ssr/services.servicesprograms.bbsccms-services-servicesprograms-warrantyinformation.shtml</a></span></td><td colspan=""1""  ><img width=10 src=""" + strURL + "images/spacer.gif"" alt="""" /></td></tr>" + vbNewLine)

            strMailBody.Append("<tr><td colspan=""3""  ><img height=20 src=""" + strURL + "images/spacer.gif"" alt="""" /></td></tr>" + vbNewLine)
            strMailBody.Append("<tr><td colspan=""1""  ><img width=10 src=""" + strURL + "images/spacer.gif"" alt="""" /></td><td><span style=""font-family: Arial, Helvetica, sans-serif; font-size: 11px; line-height: 15px; color: Black"">To learn more about Professional products, please visit:</span></td><td colspan=""1""  ><img width=10 src=""" + strURL + "images/spacer.gif"" alt="""" /></td></tr>" + vbNewLine)
            strMailBody.Append("<tr><td colspan=""1""  ><img width=10 src=""" + strURL + "images/spacer.gif"" alt="""" /></td><td><span style=""font-family: Arial, Helvetica, sans-serif; font-size: 11px; line-height: 15px; color: Black""><a href=""http://www.sony.com/professional"">www.sony.com/professional</a></span></td><td colspan=""1""  ><img width=10 src=""" + strURL + "images/spacer.gif"" alt="""" /></td></tr>" + vbNewLine)

            strMailBody.Append("<tr><td colspan=""3""  ><img height=20 src=""" + strURL + "images/spacer.gif"" alt="""" /></td></tr>" + vbNewLine)
            strMailBody.Append("<tr><td colspan=""1""  ><img width=10 src=""" + strURL + "images/spacer.gif"" alt="""" /></td><td><span style=""font-family: Arial, Helvetica, sans-serif; font-size: 11px; line-height: 15px; color: Black"">For Service and Support, please go to:</span></td><td colspan=""1""  ><img width=10 src=""" + strURL + "images/spacer.gif"" alt="""" /></td></tr>" + vbNewLine)
            strMailBody.Append("<tr><td colspan=""1""  ><img width=10 src=""" + strURL + "images/spacer.gif"" alt="""" /></td><td><span style=""font-family: Arial, Helvetica, sans-serif; font-size: 11px; line-height: 15px; color: Black""><a href=""http://www.sony.com/professionalservices"">www.sony.com/professionalservices</a></span></td><td colspan=""1""  ><img width=10 src=""" + strURL + "images/spacer.gif"" alt="""" /></td></tr>" + vbNewLine)

            strMailBody.Append("<tr><td colspan=""3""  ><img height=20 src=""" + strURL + "images/spacer.gif"" alt="""" /></td></tr>" + vbNewLine)
            strMailBody.Append("<tr><td colspan=""1""  ><img width=10 src=""" + strURL + "images/spacer.gif"" alt="""" /></td><td><span style=""font-family: Arial, Helvetica, sans-serif; font-size: 11px; line-height: 15px; color: Black"">For parts, software and training, please go to:</span></td><td colspan=""1""  ><img width=10 src=""" + strURL + "images/spacer.gif"" alt="""" /></td></tr>" + vbNewLine)
            strMailBody.Append("<tr><td colspan=""1""  ><img width=10 src=""" + strURL + "images/spacer.gif"" alt="""" /></td><td><span style=""font-family: Arial, Helvetica, sans-serif; font-size: 11px; line-height: 15px; color: Black""><a href=""http://www.sony.com/servicesplus"">www.sony.com/servicesplus</a></span></td><td colspan=""1""  ><img width=10 src=""" + strURL + "images/spacer.gif"" alt="""" /></td></tr>" + vbNewLine)

            strMailBody.Append("<tr><td colspan=""3""  ><img height=20 src=""" + strURL + "images/spacer.gif"" alt="""" /></td></tr>" + vbNewLine)
            strMailBody.Append("<tr><td colspan=""1""  ><img width=10 src=""" + strURL + "images/spacer.gif"" alt="""" /></td><td><span style=""font-family: Arial, Helvetica, sans-serif; font-size: 11px; line-height: 15px; color: Black"">For the location of the nearest Sony Service Center please go to:</span></td><td colspan=""1""  ><img width=10 src=""" + strURL + "images/spacer.gif"" alt="""" /></td></tr>" + vbNewLine)
            strMailBody.Append("<tr><td colspan=""1""  ><img width=10 src=""" + strURL + "images/spacer.gif"" alt="""" /></td><td><span style=""font-family: Arial, Helvetica, sans-serif; font-size: 11px; line-height: 15px; color: Black""><a href=""http://pro.sony.com/bbsc/ssr/services.servicesprograms.bbsccms-services-servicesprograms-servicecenterlocations.shtml"">pro.sony.com/bbsc/ssr/services.servicesprograms.bbsccms-services-servicesprograms-servicecenterlocations.shtml</a></span></td><td colspan=""1""  ><img width=10 src=""" + strURL + "images/spacer.gif"" alt="""" /></td></tr>" + vbNewLine)

            strMailBody.Append("<tr><td colspan=""3""  ><img height=20 src=""" + strURL + "images/spacer.gif"" alt="""" /></td></tr>" + vbNewLine)
            strMailBody.Append("<tr><td colspan=""1""  ><img width=10 src=""" + strURL + "images/spacer.gif"" alt="""" /></td><td><span style=""font-family: Arial, Helvetica, sans-serif; font-size: 11px; line-height: 15px; color: Black"">Once again, thank you for choosing Sony.</span></td><td colspan=""1""  ><img width=10 src=""" + strURL + "images/spacer.gif"" alt="""" /></td></tr>" + vbNewLine)

            strMailBody.Append("<tr><td colspan=""3""  ><img height=20 src=""" + strURL + "images/spacer.gif"" alt="""" /></td></tr>" + vbNewLine)
            strMailBody.Append("<tr><td colspan=""1""  ><img width=10 src=""" + strURL + "images/spacer.gif"" alt="""" /></td><td><span style=""font-family: Arial, Helvetica, sans-serif; font-size: 11px; line-height: 15px; color: Black"">Sony Electronics Inc.<br/>Broadcast and Business Solutions Company <br/>1 Sony Drive <br/>Park Ridge, New Jersey 07656 <br/>(201) 930-1000</span></td><td colspan=""1""  ><img width=10 src=""" + strURL + "images/spacer.gif"" alt="""" /></td></tr>" + vbNewLine)

            strMailBody.Append("</table>")

            Return strMailBody.ToString()
        End Function

        Private Function ValidateEntry() As Boolean
            errorMessageLabel.Text = String.Empty
            Dim objCustomerPR As CustomerProductRegistration = New CustomerProductRegistration()
            If ddlModelPrefix.SelectedIndex < 1 Then
                errorMessageLabel.Text = "Please select model prefix."
                Return False
            End If
            If ddlModel.SelectedIndex < 1 Then
                errorMessageLabel.Text = "Please select model."
                Return False
            End If
            If ddlPurchasedFrom.SelectedIndex < 1 Then
                errorMessageLabel.Text = "Please select purchased from option."
                Return False
            End If

            Select Case ddlPurchasedFrom.SelectedIndex
                Case 1
                    'Do nothing
                Case 2
                    If String.IsNullOrWhiteSpace(txtResellerCity.Text) Then
                        errorMessageLabel.Text = "Please enter reseller city name."
                        Return False
                    End If
                    If ddlResellerState.SelectedIndex < 1 Then
                        errorMessageLabel.Text = "Please select state."
                        Return False
                    End If
                Case 3
                    If String.IsNullOrWhiteSpace(txtOtherPurchasedFrom.Text) Then
                        errorMessageLabel.Text = "Please enter the location from where you purchased your product."
                        Return False
                    End If
            End Select

            If String.IsNullOrWhiteSpace(txtDate.Text) Then
                errorMessageLabel.Text = "Please enter purchase date."
                errorMessageLabel.Visible = True
                Return False
            End If
            If IsDate(txtDate.Text.ToString()) Then
                Dim thisPattern As String = "^(\d{2}\/\d{2}\/\d{4}$)"
                If Not System.Text.RegularExpressions.Regex.IsMatch(txtDate.Text.ToString(), thisPattern) Then
                    errorMessageLabel.Text = "Please enter date in MM/DD/YYYY format."
                    errorMessageLabel.Visible = True
                    Return False
                Else
                    If Convert.ToDateTime(txtDate.Text.ToString()) >= Now.Date Then
                        errorMessageLabel.Text = "Please enter date before or today."
                        errorMessageLabel.Visible = True
                        Return False
                    End If
                End If
            Else
                errorMessageLabel.Text = "Please enter valid date."
                errorMessageLabel.Visible = True
                Return False
            End If


            If String.IsNullOrWhiteSpace(txtSerialNumber.Text) Then
                errorMessageLabel.Text = "Please enter Serial Number."
                Return False
            Else
                If txtSerialNumber.Text.IndexOf(",") >= 0 Then
                    errorMessageLabel.Text = ""","" not allowed in serial number."
                    Return False
                End If
            End If

            If txtSerialNumber2.Text.IndexOf(",") >= 0 Or txtSerialNumber3.Text.IndexOf(",") >= 0 Or txtSerialNumber4.Text.IndexOf(",") >= 0 Or txtSerialNumber5.Text.IndexOf(",") >= 0 Or txtSerialNumber6.Text.IndexOf(",") >= 0 Or txtSerialNumber7.Text.IndexOf(",") >= 0 Or txtSerialNumber8.Text.IndexOf(",") >= 0 Or txtSerialNumber9.Text.IndexOf(",") >= 0 Or txtSerialNumber10.Text.IndexOf(",") >= 0 Then
                errorMessageLabel.Text = ""","" not allowed in serial number."
                Return False
            End If
            'Added by Prasad to validate Promotional code 
            If (Len(txtPromotionalCode.Text.Trim()) = 10) Then
                If (txtPromotionalCode.Text.Trim().ToUpper.StartsWith("IN") Or txtPromotionalCode.Text.Trim().ToUpper.StartsWith("MO") Or txtPromotionalCode.Text.Trim().ToUpper.StartsWith("RE") Or txtPromotionalCode.Text.Trim().ToUpper.StartsWith("CB")) Then
                    If (txtPromotionalCode.Text.Trim().ToUpper.StartsWith("RE") And (ddlPurchasedFrom.SelectedValue <> "2")) Then
                        errorMessageLabel.Text = "The promotion code you entered is not valid. Please retry."
                        Return False
                    Else
                        Dim objPRManager As OnlineProductRegistrationManager = New OnlineProductRegistrationManager()

                        Dim SUCCESS_MESSAGE As String
                        If Session("customerpr") Is Nothing Then
                            Response.Redirect("Online-Product-Registration.aspx", True)
                        Else
                            objCustomerPR = Session.Item("customerpr")
                        End If
                        If ddlResellerName.SelectedItem Is Nothing Then
                            SUCCESS_MESSAGE = objPRManager.ValidatePromotionalCode(txtPromotionalCode.Text.Trim().ToUpper(), txtDate.Text.Trim(), objCustomerPR.LINE_OF_WORK.Substring(0, 2), ddlModel.Text.Trim(), "")
                        Else
                            SUCCESS_MESSAGE = objPRManager.ValidatePromotionalCode(txtPromotionalCode.Text.Trim().ToUpper(), txtDate.Text.Trim(), objCustomerPR.LINE_OF_WORK.Substring(0, 2), ddlModel.Text.Trim(), ddlResellerName.SelectedItem.Value.Trim())
                        End If

                        If SUCCESS_MESSAGE = "Error" Then
                            errorMessageLabel.Text = "The promotion code you entered is not valid. Please retry."
                            Return False
                        Else
                            objCustomerPR.SUCCESS_MESSAGE = SUCCESS_MESSAGE
                            objCustomerPR.PROMO_DESC = txtPromotionalCode.Text.Trim()
                            Session.Item("customerpr") = objCustomerPR
                        End If
                    End If
                Else
                    errorMessageLabel.Text = "The promotion code you entered is not valid. Please retry."
                    Return False
                End If
            Else

                If Session("customerpr") Is Nothing Then
                    Response.Redirect("Online-Product-Registration.aspx", True)
                Else
                    objCustomerPR = Session.Item("customerpr")
                End If
                objCustomerPR.SUCCESS_MESSAGE = ""
                Session.Item("customerpr") = objCustomerPR
                If (txtPromotionalCode.Text.Trim() <> "") Then
                    errorMessageLabel.Text = "The promotion code you entered is not valid. Please retry."
                    Return False
                End If
            End If
            'Added by below Captcha Validation by Arshad.
            If Not Session("CaptchaImageText") Is Nothing Then
                If txtCaptchaValidation.Text.Trim() = Session("CaptchaImageText").ToString() Then
                    Session.Remove("CaptchaImageText")
                Else
                    errorMessageLabel.Text = "The code you entered is not valid."
                    Return False
                End If
            End If
            Return True
        End Function

        <System.Web.Services.WebMethod(EnableSession:=True)>
        Public Shared Function CheckConflicts(ByVal ModelNo As String) As String
            Try
                Dim objPRManager As OnlineProductRegistrationManager = New OnlineProductRegistrationManager()
                Return objPRManager.GetConflictedModel(ModelNo, HttpContextManager.GlobalData)
            Catch ex As Exception
                Return ex.Message
            End Try
        End Function

    End Class

End Namespace
