<%@ Reference Page="~/SSL.aspx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="SonyFooter" Src="SonyFooter.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="TopSonyHeader" Src="TopSonyHeader.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="PersonalMessage" Src="PersonalMessage.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="ServicePlusNav" Src="ServicePlusNav.ascx" %>

<%@ Page Language="vb" AutoEventWireup="false" Inherits="ServicePLUSWebApp.Sony_Parts_CatalogGroup" EnableViewStateMac="true" CodeFile="Sony_Parts_CatalogGroup.aspx.vb" CodeFileBaseClass="ServicePLUSWebApp.SSL" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="<%=PageLanguage %>">
<head>
    <title>Sony Part Catalog for Professional Products</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <meta content="Sony Parts, Sony Professional Parts, Sony Broadcast Parts, Sony Business Parts, Sony Software, Sony Professional Software, Sony Broadcast Software, Sony Business Software"
        name="keywords">
    <link href="includes/ServicesPLUS_style.css" type="text/css" rel="stylesheet">
    <script language="javascript" src="includes/ServicesPLUS.js" type="text/javascript"></script>
    <script type="text/javascript" src="includes/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="includes/svcplus_globalization.js"></script>
</head>
<body style="color: #000000; background: #5d7180; margin: 0px;">
    <form id="Form1" method="post" runat="server">
        <center>
            <table width="710" border="0" role="presentation">
                <tr>
                    <td width="25" style="background: url('images/sp_left_bkgd.gif');">
                        <img src="images/spacer.gif" width="25" height="25" alt="">
                    </td>
                    <td width="689" bgcolor="#ffffff">
                        <table width="710" border="0" role="presentation">
                            <tr>
                                <td width="582">
                                    <ServicePLUSWebApp:TopSonyHeader ID="TopSonyHeader" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td width="582" height="23">
                                    <nav><ServicePLUSWebApp:ServicePlusNav ID="SonyNav" runat="server" /></nav>
                                </td>
                            </tr>
                            <tr>
                                <td width="582">
                                    <table width="710" border="0" role="presentation">
                                        <tr>
                                            <td align="right" width="464" background="images/sp_int_header_top_PartsPLUS.jpg" bgcolor="#363d45"
                                                height="82">
                                                <br>
                                                <h1 class="headerText">Parts&nbsp;&amp;&nbsp;Accessories &nbsp;&nbsp;</h1>
                                            </td>
                                            <td valign="top" width="238" bgcolor="#363d45">
                                                <ServicePLUSWebApp:PersonalMessage ID="PersonalMessageDisplay" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td bgcolor="#f2f5f8">
                                                <table width="464" border="0" role="presentation">
                                                    <tr>
                                                        <td width="20">
                                                            <img height="20" src="images/spacer.gif" width="20" alt="">
                                                        </td>
                                                        <td>
                                                            <h2 class="headerTitle">Sony Part Catalog</h2>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="20" style="height: 105px">
                                                            <img height="20" src="images/spacer.gif" width="20" alt="">
                                                        </td>
                                                        <td style="height: 105px">
                                                            <table role="presentation">
                                                                <tr>
                                                                    <td class="finderSubhead">Quick Parts Search</td>
                                                                    <td width="20">
                                                                        <img height="20" src="images/spacer.gif" width="20" alt="">
                                                                    </td>
                                                                    <td><span class="finderCopyDark">&nbsp;Part Number:</span></td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="finderCopyDark" width="205" rowspan="2">
                                                                        Don't see the part you want listed below?
                                                                        <br />
                                                                        Enter part number you want here,
                                                                        <br />
                                                                        or try <a href="sony-parts.aspx" class="finderSubhead">advanced search</a>
                                                                    </td>
                                                                    <td width="20">
                                                                        <img height="20" src="images/spacer.gif" width="20" alt="">
                                                                    </td>
                                                                    <td>
                                                                        <SPS:SPSTextBox ID="PartNumber" runat="server" CssClass="finderCopyDark"></SPS:SPSTextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>

                                                                    <td width="20" style="height: 55px">
                                                                        <img height="20" src="images/spacer.gif" width="20" alt="">
                                                                    </td>
                                                                    <td style="height: 55px">
                                                                        <asp:ImageButton ID="btnSearch" runat="server" ImageUrl="images/sp_int_submitGreybkgd_btn.gif"
                                                                            AlternateText="Quick Search"></asp:ImageButton>
                                                                        <br />
                                                                        <asp:Label ID="SearchErrorLabel" runat="server" CssClass="redAsterick"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>

                                                    </tr>
                                                </table>
                                            </td>
                                            <td width="238" bgcolor="#99a8b5">
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr height="9">
                                            <td bgcolor="#f2f5f8">
                                                <img height="9" src="images/sp_int_header_btm_left_onepix.gif" width="464" alt="">
                                            </td>
                                            <td width="238" bgcolor="#99a8b5">
                                                <img height="9" src="images/sp_int_header_btm_right.gif" width="246" alt="">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>

                                <td>
                                    <table role="presentation">
                                        <tr>
                                            <td width="20">
                                                <img height="20" src="images/spacer.gif" width="20" alt="">
                                            </td>
                                            <td width="20">
                                                <img height="20" src="images/spacer.gif" width="20" alt="">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="20">
                                                <img height="20" src="images/spacer.gif" width="20" alt="">
                                            </td>
                                            <td>
                                                <asp:HyperLink ID="topPreviousImg" runat="server" CssClass="tableData"><IMG src="images/sp_int_leftArrow_btn.gif" border="0" alt=""></asp:HyperLink>&nbsp; 
								                    <asp:HyperLink ID="topPrevious" runat="server" CssClass="tableData">Previous</asp:HyperLink>
                                                <img height="20" src="images/spacer.gif" width="20" alt="">
                                                <asp:HyperLink ID="topNext" runat="server" CssClass="tableData">Next</asp:HyperLink>&nbsp; 
								                    <asp:HyperLink ID="topNextImg" runat="server" CssClass="tableData"><IMG src="images/sp_int_rightArrow_btn.gif" border="0" alt=""></asp:HyperLink>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="20">
                                                <img height="20" src="images/spacer.gif" width="20" alt="">
                                            </td>
                                            <td>
                                                <asp:GridView ID="GridPartsGroup" runat="server" AutoGenerateColumns="False">
                                                    <Columns>
                                                        <asp:BoundField AccessibleHeaderText="Group ID" DataField="GROUPID" HeaderText="Group ID" Visible="False" />
                                                        <asp:HyperLinkField AccessibleHeaderText="From" DataTextField="RANGESTART" HeaderText="Part Number Range">
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                        </asp:HyperLinkField>
                                                    </Columns>
                                                    <RowStyle BackColor="#F2F5F8" CssClass="tableData" />
                                                    <HeaderStyle BackColor="#D5DEE9" CssClass="tableHeader" />
                                                    <AlternatingRowStyle BackColor="White" />
                                                </asp:GridView>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="20">
                                                <img height="20" src="images/spacer.gif" width="20" alt="">
                                            </td>
                                            <td>
                                                <asp:HyperLink ID="btmPreviousImg" runat="server" CssClass="tableData"><IMG src="images/sp_int_leftArrow_btn.gif" border="0" alt=""></asp:HyperLink>&nbsp;  
								                    <asp:HyperLink ID="btmPrevious" runat="server" CssClass="tableData">Previous</asp:HyperLink>
                                                <img height="20" src="images/spacer.gif" width="20" alt="">
                                                <asp:HyperLink ID="btmNext" runat="server" CssClass="tableData">Next</asp:HyperLink>&nbsp;
								                    <asp:HyperLink ID="btmNextImg" runat="server" CssClass="tableData"><IMG src="images/sp_int_rightArrow_btn.gif" border="0" alt=""></asp:HyperLink>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="20">
                                                <img height="20" src="images/spacer.gif" width="20" alt="">
                                            </td>
                                            <td>
                                                <a href="#top">
                                                    <img src="images/sp_int_back2top_btn.gif" width="78" height="28" border="0" alt=""></a>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>

                                <td width="582">
                                    <footer><ServicePLUSWebApp:SonyFooter ID="SonyFooter1" runat="server" /></footer>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="25" style="background: url('images/sp_right_bkgd.gif')">
                        <img src="images/spacer.gif" width="25" height="20" alt="">
                    </td>
                </tr>
            </table>
        </center>
    </form>

</body>
</html>
