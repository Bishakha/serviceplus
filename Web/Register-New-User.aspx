<%@ Reference Page="~/SSL.aspx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="TopSonyHeader" Src="TopSonyHeader.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="SonyFooter" Src="SonyFooter.ascx" %>

<%@ Page Language="vb" AutoEventWireup="false" SmartNavigation="true" Inherits="ServicePLUSWebApp.Register_New_User"
    EnableViewStateMac="true" CodeFile="Register-New-User.aspx.vb" CodeFileBaseClass="ServicePLUSWebApp.SSL" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="<%=PageLanguage %>">
<head>
    <title>
        <%=Resources.Resource.ttl_NewUserRegisteration%></title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link href="includes/ServicesPLUS_style.css" type="text/css" rel="stylesheet">

    <script language="javascript" src="includes/ServicesPLUS.js" type="text/javascript"></script>

    <script type="text/javascript" src="includes/jquery-1.10.2.min.js"></script>

    <script type="text/javascript" src="includes/svcplus_globalization.js"></script>

    <style type="text/css">
        .style1 {
        }
    </style>

    <script type="text/javascript">
        function showStuff(id) {
            document.getElementById(id).style.display = 'block';
        }
        function hideStuff(id) {
            document.getElementById(id).style.display = 'none';
        }
    </script>

</head>
<body style="color: #000000; background: #5d7180; margin: 0px;">
    <form id="form1" autocomplete="off" method="post" runat="server">
        <center>
            <table width="760" border="0" role="presentation">
                <tr>
                    <td width="25" style="background: url('images/sp_left_bkgd.gif');">
                        <img src="images/spacer.gif" width="25" height="25" alt="">
                    </td>
                    <td width="710" bgcolor="#ffffff">
                        <table width="710" border="0" role="presentation">
                            <tr>
                                <td>
                                    <ServicePLUSWebApp:TopSonyHeader ID="TopSonyHeader" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <%--<img src="images/sp_int_register_hdr.gif" border="0">--%>
                                    <img src="<%=Resources.Resource.sna_svc_img_49%>" border="0" alt="ServicesPlus Registration">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <main>
                                        <table width="710" border="0" role="presentation">
                                            <tr>
                                                <td width="17" height="20">
                                                    <img height="20" src="images/spacer.gif" width="20" alt="">
                                                </td>
                                                <td width="678" height="20">
                                                    <img height="20" src="images/spacer.gif" width="670" alt="">
                                                </td>
                                                <td width="20" height="20">
                                                    <img height="20" src="images/spacer.gif" width="20" alt="">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="17">
                                                    <img height="20" src="images/spacer.gif" width="20" alt="">
                                                </td>
                                                <td width="678">
                                                    <table width="670" border="0" role="presentation">
                                                        <tr bgcolor="#ffffff">
                                                            <td width="3">
                                                                <img height="1" src="images/spacer.gif" width="3" alt="">
                                                            </td>
                                                            <td width="127">
                                                                <img height="1" src="images/spacer.gif" width="125" alt="">
                                                            </td>
                                                            <td width="542">
                                                                <img height="1" src="images/spacer.gif" width="542" alt="">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td valign="top" colspan="3" height="15">
                                                                <a class="bodyCopy" href="default.aspx">
                                                                    <%=Resources.Resource.el_HomePge%>
                                                                </a>
                                                            </td>
                                                        </tr>
                                                        <tr bgcolor="#d5dee9">
                                                            <td width="3" height="18">
                                                                <img height="15" src="images/spacer.gif" width="3" alt="">
                                                            </td>
                                                            <td colspan="2" height="18">
                                                                <span class="tableHeader">
                                                                    <%=Resources.Resource.el_rgn_RgnSubHdl1 %>
                                                                </span><span class="redAsterick">* </span><span class="tableHeader">
                                                                    <%=Resources.Resource.el_rgn_RgnSubHdl2 %></span>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <img height="7" src="images/spacer.gif" width="3" alt="">
                                                            </td>
                                                            <td width="127">
                                                                <img height="7" src="images/spacer.gif" width="3" alt="">
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="ErrorValidation" runat="server" CssClass="redAsterick"/>
                                                                <asp:Label ID="ErrorLabel" runat="server" CssClass="redAsterick"/>
                                                                <asp:Label ID="lblPhoneError" runat="server" CssClass="redAsterick"/>
                                                                <asp:Label ID="lblEtxErr" runat="server" CssClass="redAsterick"/>
                                                                <asp:Label ID="lblFaxError" runat="server" CssClass="redAsterick"/>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <img height="30" src="images/spacer.gif" width="3" alt="">
                                                            </td>
                                                            <td class="bodyCopy" align="right" width="127">
                                                                <label for="txtCompanyName" id="lblCompany" runat="server"><%=Resources.Resource.el_rgn_CompanyName%></label>&nbsp;
                                                            </td>
                                                            <td>
                                                                <SPS:SPSTextBox ID="txtCompanyName" runat="server" CssClass="bodyCopy" MaxLength="100" aria-required="true"/>
                                                                <span class="redAsterick"> *</span>
                                                            </td>
                                                        </tr>
                                                        <tr bgcolor="#f2f5f8">
                                                            <td>
                                                                <img height="30" src="images/spacer.gif" width="3" alt="">
                                                            </td>
                                                            <td class="bodyCopy" align="right" width="127">
                                                                <label for="txtAddress1" ID="lblAddress1" runat="server"><%=Resources.Resource.el_rgn_StreetAddress%></label>&nbsp;
                                                            </td>
                                                            <td>
                                                                <SPS:SPSTextBox ID="txtAddress1" runat="server" CssClass="bodyCopy" MaxLength="50" aria-required="true"/>
                                                                <asp:Label ID="lblAddress1Error" runat="server" CssClass="redAsterick" Text="*"/>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <img height="30" src="images/spacer.gif" width="3" alt="">
                                                            </td>
                                                            <td class="bodyCopy" align="right" width="127">
                                                                <label for="txtAddress2" id="lblAddress2" runat="server"><%=Resources.Resource.el_rgn_2ndAddress%></label>
                                                            </td>
                                                            <td>
                                                                <SPS:SPSTextBox ID="txtAddress2" runat="server" CssClass="bodyCopy" MaxLength="50" />
                                                                <asp:Label ID="lblAddress2Error" runat="server" CssClass="redAsterick"/>
                                                            </td>
                                                        </tr>
                                                        <tr bgcolor="#f2f5f8">
                                                            <td>
                                                                <img height="30" src="images/spacer.gif" width="3" alt="">
                                                            </td>
                                                            <td class="bodyCopy" align="right" width="127">
                                                                <label for="txtCity" id="lblCity" runat="server"><%=Resources.Resource.el_rgn_City %></label>
                                                            </td>
                                                            <td>
                                                                <SPS:SPSTextBox ID="txtCity" runat="server" CssClass="bodyCopy" MaxLength="50" aria-required="true" />
                                                                <asp:Label ID="lblCityError" runat="server" CssClass="redAsterick" Text="*"/>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <img height="30" src="images/spacer.gif" width="3" alt="">
                                                            </td>
                                                            <td class="bodyCopy" align="right" width="127">
                                                                <label for="ddlState" id="lblState" runat="server"><%=Resources.Resource.el_rgn_State%></label>
                                                            </td>
                                                            <td>
                                                                <asp:DropDownList ID="ddlState" runat="server" Width="130px" aria-required="true" />
                                                                <span class="redAsterick">*</span>
                                                            </td>
                                                        </tr>
                                                        <tr bgcolor="#f2f5f8">
                                                            <td>
                                                                <img height="30" src="images/spacer.gif" width="3" alt="">
                                                            </td>
                                                            <td class="bodyCopy" align="right" width="127">
                                                                <label for="txtZipCode" id="lblZipCode" runat="server">Zip Code :</label>
                                                            </td>
                                                            <td>
                                                                <SPS:SPSTextBox ID="txtZipCode" runat="server" CssClass="bodyCopy" MaxLength="5" aria-required="true"/>
                                                                <asp:Label ID="lblZipError" runat="server" CssClass="redAsterick" Text="*"/>
                                                                <asp:RegularExpressionValidator ID="rgxZipCode" runat="server"
                                                                    CssClass="redAsterick" ControlToValidate="txtZipCode" EnableClientScript="False" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <img height="30" src="images/spacer.gif" width="3" alt="">
                                                            </td>
                                                            <td class="bodyCopy" align="right" width="127">
                                                                <label for="txtPhone" id="lblPhone" runat="server"><%=Resources.Resource.el_rgn_Phone%></label>
                                                            </td>
                                                            <td>
                                                                <table width="460" border="0" role="presentation">
                                                                    <tr>
                                                                        <td class="bodyCopy">
                                                                            &nbsp;
                                                                            <SPS:SPSTextBox ID="txtPhone" runat="server" CssClass="bodyCopy" aria-required="true"/><span
                                                                                class="redAsterick"> *</span>&nbsp;&nbsp;xxx-xxx-xxxx
                                                                        </td>
                                                                        <%--<td class="bodyCopy">xxx-xxx-xxxx</td>--%>
                                                                        <td class="bodyCopy">
                                                                            <label for="txtExtension" id="lblExtension" runat="server"><%=Resources.Resource.el_rgn_Extension%></label>
                                                                        </td>
                                                                        <td class="bodyCopy">
                                                                            <SPS:SPSTextBox ID="txtExtension" runat="server" CssClass="bodyCopy" MaxLength="6"/>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr bgcolor="#f2f5f8">
                                                            <td>
                                                                <img height="30" src="images/spacer.gif" width="3" alt="">
                                                            </td>
                                                            <td class="bodyCopy" align="right" width="127">
                                                                <label for="txtFax" id="lblFax" runat="server"><%=Resources.Resource.el_rgn_Fax%></label>
                                                                &nbsp;
                                                            </td>
                                                            <td class="bodyCopy">
                                                                &nbsp;
                                                                <SPS:SPSTextBox ID="txtFax" runat="server" CssClass="bodyCopy"/>&nbsp;&nbsp;&nbsp;&nbsp;xxx-xxx-xxxx
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td width="20" height="20">
                                                    <img height="20" src="images/spacer.gif" width="20" alt="">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <img height="30" src="images/spacer.gif" width="3" alt="">
                                                </td>
                                                <td align="center" class="bodyCopy" colspan="2">
                                                    <asp:ImageButton ID="btnNext" runat="server" ImageUrl="<%$Resources:Resource,img_btnNext%>"
                                                        AlternateText="Go to the next page to continue your registration." role="button" />
                                                </td>
                                            </tr>
                                        </table>
                                    </main>
                                </td>
                                <td width="20">
                                    <img height="20" src="images/spacer.gif" width="20" alt="">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <footer><ServicePLUSWebApp:SonyFooter ID="SonyFooter1" runat="server" /></footer>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="25" style="background: url('images/sp_right_bkgd.gif')">
                        <img src="images/spacer.gif" width="25" height="20" alt="">
                    </td>
                </tr>
            </table>

        </center>
    </form>

</body>
</html>
