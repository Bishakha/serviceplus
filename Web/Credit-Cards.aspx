<%@ Reference Page="~/SSL.aspx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="SonyFooter" Src="SonyFooter.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="ServicePlusNav" Src="ServicePlusNav.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="TopSonyHeader" Src="TopSonyHeader.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="PersonalMessage" Src="PersonalMessage.ascx" %>

<%@ Page Language="vb" AutoEventWireup="false" Inherits="ServicePLUSWebApp.CreditCards"
    EnableViewStateMac="True" CodeFile="Credit-Cards.aspx.vb" CodeFileBaseClass="ServicePLUSWebApp.SSL" %>

<html lang="<%=PageLanguage %>">
<head>
    <title><%=Resources.Resource.ttl_CreditCard%> </title>
    <meta content="True" name="vs_showGrid">
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link href="/includes/ServicesPLUS_style.css" rel="stylesheet" type="text/css" />

    <script language="javascript" src="/includes/ServicesPLUS.js" type="text/javascript"></script>
    <script type="text/javascript" src="includes/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="includes/svcplus_globalization.js"></script>
    <style type="text/css">
        .style1 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: 11px;
            line-height: 12px;
            color: #2a3d47;
            font-weight: normal;
            width: 183px;
        }

        .style3 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: 11px;
            line-height: 12px;
            color: #2a3d47;
            font-weight: normal;
            width: 152px;
        }

        .style4 {
            width: 152px;
        }

        .style5 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: 11px;
            line-height: 12px;
            color: #2a3d47;
            font-weight: normal;
            height: 30px;
            width: 189px;
        }

        .style6 {
            width: 20px;
        }

        .style7 {
            width: 669px;
        }

        .style8 {
            width: 139px;
        }
    </style>
</head>
<body text="#000000" bgcolor="#5d7180" leftmargin="0" topmargin="0" marginwidth="0"
    marginheight="0">
    <form id="Form1" autocomplete="off" method="post" runat="server">
        <center>
            <table width="760" border="0" role="presentation">
                <tbody>
                    <tr>
                        <td width="25" background="images/sp_left_bkgd.gif">
                            <img height="25" src="images/spacer.gif" width="25" alt="">
                        </td>
                        <td width="710" bgcolor="#ffffff">
                            <table width="710" border="0" role="presentation">
                                <tbody>
                                    <tr>
                                        <td>
                                            <ServicePLUSWebApp:TopSonyHeader ID="TopSonyHeader" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <nav><ServicePLUSWebApp:ServicePlusNav ID="SonyNav" runat="server" /></nav>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table width="710" border="0" role="presentation">
                                                <tr style="height: 57px;">
                                                    <td style="width: 464px; background: #363d45 url('images/sp_int_header_top_ServicesPLUS_onepix.gif'); text-align: right; vertical-align: top;">
                                                        <br/>
                                                        <h1 class="headerText">ServicesPLUS &nbsp;&nbsp;</h1>
                                                    </td>
                                                    <td valign="top" bgcolor="#363d45">
                                                        <table width="246" border="0" role="presentation">
                                                            <tr>
                                                                <td colspan="3">
                                                                    <img height="24" src="images/sp_int_header_top_right.gif" width="246" alt="">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td width="5">
                                                                    &nbsp;
                                                                </td>
                                                                <td width="236">
                                                                    <span class="memberName">
                                                                        <ServicePLUSWebApp:PersonalMessage ID="PersonalMessageDisplay" runat="server" />
                                                                    </span>
                                                                </td>
                                                                <td width="5">
                                                                    &nbsp;
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td bgcolor="#f2f5f8">
                                                        <h2 class="headerTitle" style="padding-right: 20px; text-align: right;"><%=Resources.Resource.el_CreditCards%></h2>
                                                    </td>
                                                    <td style="background: #99a8b5;">
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                                <tr style="height: 9px;">
                                                    <td bgcolor="#f2f5f8">
                                                        <img height="9" src="images/sp_int_header_btm_left_onepix.gif" width="464" alt="">
                                                    </td>
                                                    <td style="background: #99a8b5;">
                                                        <img height="9" src="images/sp_int_header_btm_right.gif" width="246" alt="">
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table width="710" border="0" role="presentation">
                                                <tbody>
                                                    <tr>
                                                        <td height="20" class="style6">
                                                            <img height="20" src="images/spacer.gif" width="20" alt="">
                                                        </td>
                                                        <td width="670" height="20" class="style7">
                                                            <asp:Label ID="MasterErrorLabel" runat="server" CssClass="redAsterick" EnableViewState="False" />
                                                        </td>
                                                        <td width="20" height="20">
                                                            <img height="20" src="images/spacer.gif" width="20" alt="">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="style6">
                                                            <img height="20" src="images/spacer.gif" width="20" alt="">
                                                        </td>
                                                        <td valign="top" width="670" class="style7">
                                                            <table cellpadding="3" border="0" role="presentation">
                                                                <tr>
                                                                    <td width="190">
                                                                        <img height="1" src="images/spacer.gif" width="1" alt="">
                                                                    </td>
                                                                    <td width="480">
                                                                        <asp:Button ID="btnHidden" runat="Server" Style="display: none" />
                                                                        <asp:Button ID="btnNewCC" runat="server" OnClientClick="return popup();"
                                                                            Text="<%$Resources:Resource,el_EnterNewCreditcard %>"
                                                                            title="Click here to enter a new credit card's details in a new window."/>
                                                                        &nbsp;&nbsp;&nbsp;
                                                                        <asp:Button ID="btnCCSaved" runat="server" Text="<%$Resources:Resource,el_SelectSavedCredit%>"
                                                                            title="Click to load a previously saved credit card."/>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td width="190">
                                                                        <img height="1" src="images/spacer.gif" width="1" alt="">
                                                                    </td>
                                                                    <td width="480">
                                                                        <img height="1" src="images/spacer.gif" width="1" alt="">
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <asp:Panel ID="pnlNewCCrd" runat="server">
                                                                <table cellpadding="3" width="664" border="0" role="presentation">
                                                                    <tr bgcolor="#f2f5f8">
                                                                        <td class="style5" align="right" height="31">
                                                                            <asp:Label ID="CreditCardNickNameLabel" runat="server" Width="120px" Style="margin-left: 51px;" CssClass="tableData" Text="<%$ Resources:Resource, el_NickName%>">  </asp:Label>
                                                                        </td>
                                                                        <td class="style8">
                                                                            <SPS:SPSTextBox ID="CreditCardNickname" runat="server" CssClass="bodyCopy"
                                                                                MaxLength="16" Width="120px" Style="margin-left: 07px;" />

                                                                        </td>
                                                                        <td width="750" height="20" align="left">
                                                                            <asp:Label ID="Label6" runat="server" CssClass="redAsterick"><span class="redAsterick tableData">*</span></asp:Label>
                                                                            <asp:Label ID="Label7" runat="server" CssClass="tableData" Style="text-align: left" EnableViewState="False" Text="<%$ Resources:Resource, el_VarningNiceName%>">  </asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </asp:Panel>
                                                            <asp:Panel ID="PnlNickName" runat="server">
                                                                <table cellpadding="3" width="664" border="0" role="presentation">
                                                                    <tr bgcolor="#f2f5f8">
                                                                        <td class="style1" align="center" bgcolor="#f2f5f8" height="27">
                                                                            <asp:Label ID="CreditCardLabel" runat="server" CssClass="tableData">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=Resources.Resource.el_SavedCards%> </asp:Label>
                                                                            &nbsp;
                                                                        </td>
                                                                        <td height="27">
                                                                            <asp:DropDownList ID="CreditCardNicknameDropdownList" runat="server" AutoPostBack="True" CssClass="bodyCopy" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </asp:Panel>

                                                            <asp:Panel ID="pnlSavedCCrd" runat="server">
                                                                <table cellpadding="3" width="664" border="0" role="presentation">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td class="style3" align="right">
                                                                                <asp:Label ID="CreditCardTypeLabel" runat="server" CssClass="tableData" Text="<%$ Resources:Resource, el_CardType%>" />&nbsp;&nbsp;
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="txtCCType" runat="server" Width="100" CssClass="bodyCopy" Enabled="false" />
                                                                                <asp:DropDownList ID="CreditCardType" runat="server" CssClass="bodyCopy" Enabled="False" Visible="false" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr bgcolor="#f2f5f8">
                                                                            <td class="style3" align="right" height="28">
                                                                                <asp:Label ID="CardNumberLabel" runat="server" CssClass="tableData" Text="<%$ Resources:Resource, el_CardNumber%>" />
                                                                                &nbsp;&nbsp;
                                                                            </td>
                                                                            <td height="28">
                                                                                <SPS:SPSTextBox ID="CardNumber" runat="server" Width="133px" CssClass="bodyCopy" MaxLength="19" Enabled="False" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="style3" align="right" height="32">
                                                                                <asp:Label ID="ExpirationDateLabel" runat="server" CssClass="tableData" Text="<%$ Resources:Resource, el_ExpirationDate%>" />&nbsp;&nbsp;
                                                                            </td>
                                                                            <td height="32">
                                                                                <span class="tableData">
                                                                                    <asp:TextBox ID="txtMonth" runat="server" Width="80" CssClass="bodyCopy" Enabled="false" />
                                                                                    <asp:TextBox ID="txtYear" runat="server" Width="30" CssClass="bodyCopy" Enabled="false" />
                                                                                    <asp:DropDownList ID="CCMonth" runat="server" CssClass="bodyCopy" EnableTheming="True" Enabled="False" Visible="false" />
                                                                                    <asp:DropDownList ID="CCYear" runat="server" CssClass="bodyCopy" Enabled="False" Visible="false" />
                                                                                </span>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="right" class="style4">
                                                                                <img height="1" src="images/spacer.gif" width="1" alt="" />
                                                                                <asp:ImageButton ID="btnCreditcardSave" runat="server" AlternateText="Update"
                                                                                    Height="30" ImageUrl="<%$Resources:Resource,sna_svc_img_9%>" Width="92" />
                                                                            </td>
                                                                            <td width="190" align="right">
                                                                                <asp:ImageButton ID="btnDelete" ImageUrl="<%$Resources:Resource,img_btnDelete%>"
                                                                                    Height="30" Width="92" AlternateText="Delete" runat="server" />
                                                                                <asp:ImageButton ID="btnCancel" ImageUrl="<%$Resources:Resource,sna_svc_img_11%>"
                                                                                    Height="30" Width="92" AlternateText="Cancel" runat="server" />
                                                                            </td>
                                                                            <td height="28" width="190">
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="right" class="style4">
                                                                                &nbsp;
                                                                            </td>
                                                                            <td align="center" width="190">
                                                                                &nbsp;
                                                                            </td>
                                                                            <td height="28" width="190">
                                                                                &nbsp;
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </asp:Panel>
                                                        </td>
                                                        <td width="20">
                                                            <img height="20" src="images/spacer.gif" width="20" alt="">
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>

                                        <td>
                                            <footer><ServicePLUSWebApp:SonyFooter ID="SonyFooter1" runat="server" /></footer>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                        <td width="25" background="images/sp_right_bkgd.gif">
                            <img height="20" src="images/spacer.gif" width="25" alt="">
                        </td>
                    </tr>
                </tbody>
            </table>
        </center>
    </form>

    <%--<script language="javascript">
			<%
				If focusFormField <> "" Then
					Response.Write("document.getElementById('" + focusFormField + "').focus();")
				End If
			%>					
    </script>--%>
</body>
</html>
