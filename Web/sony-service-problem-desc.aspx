<%@ Page Language="VB" AutoEventWireup="false" CodeFile="sony-service-problem-desc.aspx.vb"
    Inherits="ServicePLUSWebApp.sony_service_problem_desc" %>

<%@ Reference Page="~/SSL.aspx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="SonyFooter" Src="SonyFooter.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="TopSonyHeader" Src="TopSonyHeader.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="PersonalMessage" Src="PersonalMessage.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="ServicePlusNav" Src="ServicePlusNav.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="<%=PageLanguage %>">
<head>
    <title>
        <%=Resources.Resource.header_serviceplus()%>
        -<%=Resources.Resource.repair_svcacc_hdr_msg()%>
    </title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <meta content="Repair, Maintenance, Services, Sony Models, Sony Professional Models, Sony Broadcast Models, Sony Business Models, Sony Software, Sony Professional Software, Sony Broadcast Software, Sony Business Software"
        name="keywords">
    <link type="text/css" rel="stylesheet" href="includes/ServicesPLUS_style.css">
    <script type="text/javascript" src="includes/ServicesPLUS.js"></script>
    <script type="text/javascript" src="includes/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="includes/svcplus_globalization.js"></script>
    <script type="text/javascript">
        function checkServiceContract() {
            var radioObj = document.forms['Form1'].elements['rdProbInter'];
            var radioLength = radioObj.length;
            var iSelect = 0;

            if (radioLength === undefined) {
                if (radioObj.checked)
                    return radioObj.value;
                else
                    return "";
            }
            for (var i = 0; i < radioLength; i++) {
                if (radioObj[i].checked) {
                    iSelect = radioObj[i].value;
                }
            }

            if (iSelect === 1) {
                document.forms['Form1'].cvProbInter.value = "Please enter select.";
                return false;
            }
        }
    </script>
</head>
<body text="#000000" bgcolor="#5d7180" leftmargin="0" topmargin="0">
    <form id="Form1" method="post" runat="server">
        <center>
        <table width="710" border="0" role="presentation">
            <tr>
                <td width="25" background="images/sp_left_bkgd.gif">
                    <img height="25" src="images/spacer.gif" width="25" alt="">
                </td>
                <td width="689" bgcolor="#ffffff">
                    <table width="710" border="0" role="presentation">
                        <tr>
                            <td width="582">
                                <ServicePLUSWebApp:TopSonyHeader ID="TopSonyHeader" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td width="582" height="23">
                                <nav><ServicePLUSWebApp:ServicePlusNav ID="SonyNav" runat="server" /></nav>
                            </td>
                        </tr>
                        <tr>
                            <td width="582">
                                <table width="710" border="0" role="presentation">
                                    <tr>
                                        <td align="right" background="images/repair_lrg.gif" bgcolor="#363d45" height="82"
                                            style="width: 465px">
                                            <br>
                                            <h1 class="headerText"><%=Resources.Resource.repair_mn_tg_msg_1()%></h1>
                                        </td>
                                        <td valign="middle" width="238" bgcolor="#363d45">
                                            <ServicePLUSWebApp:PersonalMessage ID="PersonalMessageDisplay" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td bgcolor="#f2f5f8" style="width: 465px">
                                            <h2 id="lblSubHdr" class="headerTitle" style="padding-right: 20px; text-align: right;" runat="server"></h2>
                                        </td>
                                        <td width="238" bgcolor="#99a8b5">
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr height="9">
                                        <td bgcolor="#f2f5f8" style="width: 465px">
                                            <img height="9" src="images/sp_int_header_btm_left_onepix.gif" width="464" alt="">
                                        </td>
                                        <td width="238" bgcolor="#99a8b5">
                                            <img height="9" src="images/sp_int_header_btm_right.gif" width="246" alt="">
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table width="100%" border="0" role="presentation">
                                    <tr>
                                        <td style="width: 5px; height: 22px;">
                                            <img height="20" src="images/spacer.gif" width="20" alt="">
                                        </td>
                                        <td style="width: 796px; height: 22px;">
                                            <p class="promoCopyBold">
                                                <%=Resources.Resource.repair_svcacc_lblvld_msg()%></p>
                                        </td>
                                        <td style="width: 436px; height: 22px;">
                                            <img height="20" src="images/spacer.gif" width="20" alt="">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 5px">
                                            <img height="20" src="images/spacer.gif" width="20" alt="">
                                        </td>
                                        <td style="width: 796px">
                                            &nbsp;<asp:Label ID="ErrorValidation" runat="server" CssClass="redAsterick" Text="<%$Resources:Resource,repair_svcacc_invld_msg%>"
                                                Visible="False"></asp:Label>
                                        </td>
                                        <td align="center" style="width: 436px">
                                            &nbsp;&nbsp;
                                            <img height="20" src="images/spacer.gif" width="20" alt="">
                                            &nbsp;&nbsp;
                                        </td>
                                    </tr>
                                    <tr valign="top">
                                        <td style="width: 5px; height: 107px;">
                                            <img height="20" src="images/spacer.gif" width="20" alt="">
                                        </td>
                                        <td style="height: 107px;" colspan="2">
                                            <table cellpadding="3" border="0" style="width: 100%" role="presentation">
                                                <tr valign="top">
                                                    <td style="width: 284px;" class="tableData">
                                                        <asp:Label ID="lblProbInter" runat="server" Text="<%$Resources:Resource,repair_svcspdsc_msg%>"></asp:Label>
                                                    </td>
                                                    <td style="width: 400px;">
                                                        <asp:RadioButtonList ID="rdProbInter" runat="server" CssClass="bodycopy" RepeatDirection="Horizontal"
                                                            RepeatLayout="Flow">
                                                            <asp:ListItem Value="1" Text="<%$Resources:Resource,repair_svcspdsc_ys_msg%>"></asp:ListItem>
                                                            <asp:ListItem Value="0" Text="<%$Resources:Resource,repair_svcspdsc_no_msg%>"></asp:ListItem>
                                                        </asp:RadioButtonList>
                                                        &nbsp;<span class="redAsterick">*</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 284px" class="tableData">
                                                        <asp:Label ID="lblProbMaint" runat="server" Text="<%$Resources:Resource,repair_svcspdsc_req_msg%>"></asp:Label>
                                                    </td>
                                                    <td style="width: 400px">
                                                        <asp:RadioButtonList ID="rdPrevntMaint" runat="server" CssClass="bodycopy" RepeatDirection="Horizontal"
                                                            RepeatLayout="Flow">
                                                            <asp:ListItem Value="1" Text="<%$Resources:Resource,repair_svcspdsc_ys_msg%>"></asp:ListItem>
                                                            <asp:ListItem Value="0" Text="<%$Resources:Resource,repair_svcspdsc_no_msg%>"></asp:ListItem>
                                                        </asp:RadioButtonList>
                                                        &nbsp;<span class="redAsterick">*</span>
                                                    </td>
                                                </tr>
                                                <tr valign="top">
                                                    <td style="width: 284px" class="tableData">
                                                        <asp:Label ID="lblDescr" runat="server" Text="<%$Resources:Resource,repair_svcspdsc_pd_msg%>"></asp:Label>
                                                    </td>
                                                    <td style="width: 400px">
                                                        <SPS:SPSTextBox ID="txtProbDescr" runat="server" CssClass="tableData"
                                                            MaxLength="150" TextMode="MultiLine" Width="195px" Height="99px"></SPS:SPSTextBox>
                                                        <span class="redAsterick">*</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 284px" class="tableData">
                                                    </td>
                                                    <td style="width: 400px">
                                                        <span class="bodyCopy"></span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2" style="height: 34px">
                                                        <asp:ImageButton ID="imgBtnNext" runat="server" ImageUrl="<%$ Resources:Resource,img_btnNext%>"
                                                            />&nbsp; <a href='sony-repair.aspx'>
                                                                <img src="<%=Resources.Resource.sna_svc_img_7()%>" alt='Click to cancel' border="0" /></a>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 5px">
                                            &nbsp;
                                        </td>
                                        <td colspan="2">
                                            &nbsp;
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td width="582">
                                <footer><ServicePLUSWebApp:SonyFooter ID="SonyFooter1" runat="server" /></footer>
                            </td>
                        </tr>
                    </table>
                </td>
                <td width="25" background="images/sp_right_bkgd.gif">
                    <img height="20" src="images/spacer.gif" width="25" alt="">
                </td>
            </tr>
        </table>
    </center>
    </form>
</body>
</html>
