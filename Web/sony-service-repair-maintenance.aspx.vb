Imports ServicesPlusException
Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Process

Namespace ServicePLUSWebApp

    Partial Class sony_service_repair_maintenance
        Inherits SSL

        Const conModelsCatalogDetailPath As String = "sony-repair-maintenance-service.aspx?groupid="
        Const conModelsCatalogGroupPath As String = "sony-service-repair-maintenance.aspx?pageno="
        Const conModelsSrchPath As String = "sony-repair-maintenance-service.aspx?srh="

        Private rangePerPage As Integer
        Private modelsPerRange As Integer
        Private blnRegisterforLogin As Boolean = False

        Protected Sub Page_Init1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
            'Promotion_single1.DisplayPage = "service_repair"
        End Sub

        Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try '6850
                'If Not (Session.Item("GlobalData")) Is Nothing Then
                '    objGlobalData = (Session.Item("GlobalData"))
                'End If
                If Not Page.IsPostBack Then
                    buildModelCatalogTable()
                    ddlStates.Items.Add(New ListItem("Select One", ""))
                    populateStateDropdownList(ddlStates)
                    'PopulateStatesDDL()
                End If
                If Not String.IsNullOrWhiteSpace(Request.QueryString("Msg")) Then
                    ChooseModel()
                End If
                ClientScript.RegisterHiddenField("__EVENTTARGET", btnRequestDepotService.ClientID)

                ' check if Service Item info object in session, if yes remove
                Session.Remove("snServiceInfo")
            Catch ex As Exception '6850 starts
                lbErrMsglModel.Text = Utilities.WrapExceptionforUI(ex)
                lbErrMsglModel.Visible = True
            End Try '6850 ends
        End Sub

        Private Sub buildModelCatalogTable()
            Dim td As New TableCell
            Dim tr As New TableRow
            Dim catMan As New CatalogManager
            Dim currPage As Integer = 1
            Dim prevPage As Integer = 1
            Dim nextPage As Integer = 1
            Dim prevPageRange As Long = 1
            Dim nextPageRange As Long = 1
            Dim maxGroup As Long = 1

            catMan.GetModelRanges(rangePerPage, modelsPerRange)

            Try
                If Not String.IsNullOrWhiteSpace(Request.QueryString("pageno")) Then
                    currPage = Integer.Parse(Request.QueryString("pageno"))
                End If
            Catch ex As Exception
                currPage = 1
            End Try

            maxGroup = catMan.GetModelMaxGroupID()

            ' add total into view state for next page use
            ViewState.Add("vsMaxModel", maxGroup)

            prevPageRange = (currPage - 1) * rangePerPage * modelsPerRange + 1
            nextPageRange = currPage * rangePerPage * modelsPerRange + 1

            If prevPageRange > 1 Then
                btmPrevious.Visible = True
                topPrevious.Visible = True
                btmPreviousImg.Visible = True
                topPreviousImg.Visible = True
                prevPage = currPage - 1
            Else
                btmPrevious.Visible = False
                topPrevious.Visible = False
                btmPreviousImg.Visible = False
                topPreviousImg.Visible = False
                prevPage = 1
            End If

            If nextPageRange < maxGroup Then
                btmNext.Visible = True
                topNext.Visible = True
                btmNextImg.Visible = True
                topNextImg.Visible = True
                nextPage = currPage + 1
            Else
                btmNext.Visible = False
                topNext.Visible = False
                btmNextImg.Visible = False
                topNextImg.Visible = False
                nextPage = 1
            End If


            btmPrevious.NavigateUrl = conModelsCatalogGroupPath + prevPage.ToString()
            topPrevious.NavigateUrl = conModelsCatalogGroupPath + prevPage.ToString()

            btmNext.NavigateUrl = conModelsCatalogGroupPath + nextPage.ToString()
            topNext.NavigateUrl = conModelsCatalogGroupPath + nextPage.ToString()

            btmPreviousImg.NavigateUrl = conModelsCatalogGroupPath + prevPage.ToString()
            topPreviousImg.NavigateUrl = conModelsCatalogGroupPath + prevPage.ToString()
            btmNextImg.NavigateUrl = conModelsCatalogGroupPath + nextPage.ToString()
            topNextImg.NavigateUrl = conModelsCatalogGroupPath + nextPage.ToString()

            btmPreviousImg.Style.Add("cursor", "hand")
            topPreviousImg.Style.Add("cursor", "hand")
            btmNextImg.Style.Add("cursor", "hand")
            topNextImg.Style.Add("cursor", "hand")

            Dim modelsGroups As ModelsCatalogGroup()
            modelsGroups = catMan.GetModelsGroupByPageNumber(currPage)

            Try
                GridPartsGroup.DataSource = modelsGroups
                GridPartsGroup.DataBind()
            Catch ex As Exception
                lbErrMsglModel.Text = Utilities.WrapExceptionforUI(ex)
                lbErrMsglModel.Visible = True
            End Try

        End Sub

        Protected Sub GridPartsGroup_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridPartsGroup.RowDataBound
            Try
                If e.Row.RowType = DataControlRowType.DataRow Then
                    Dim rec As ModelsCatalogGroup
                    rec = e.Row.DataItem
                    Dim lnkStartRange As HyperLink = CType(e.Row.Cells(1).Controls(0), HyperLink)
                    lnkStartRange.Style.Add("cursor", "hand")
                    lnkStartRange.Style.Add("text-decoration", "underline")
                    If Not rec.GroupID Is Nothing Then
                        lnkStartRange.NavigateUrl = "model-group-" + rec.GroupID.ToString() + ".aspx"
                    Else
                    End If
                End If
            Catch ex As Exception
                lbErrMsglModel.Text = Utilities.WrapExceptionforUI(ex)
                lbErrMsglModel.Visible = True
            End Try

        End Sub


        ' 2016-06-07 ASleight - This method is no longer used.
        Private Sub PopulateStatesDDL()

            Dim cm As CatalogManager = New CatalogManager
            Dim states() As StatesDetail
            Dim iState As StatesDetail

            Try
                states = cm.StatesBySalesOrg(HttpContextManager.GlobalData)
                'states = cm.GetStates()


                For Each iState In states
                    Dim newListItem As New ListItem()
                    newListItem.Text = ChangeCase(iState.StateName)
                    newListItem.Value = iState.StateAbbr
                    ddlStates.Items.Add(newListItem)
                Next
            Catch ex As Exception
                lbErrMsglModel.Text = Utilities.WrapExceptionforUI(ex)
                lbErrMsglModel.Visible = True
            End Try
        End Sub

        Private Function ChangeCase(ByVal pString As String) As String
            Dim str As String = pString.ToLower()
            Return System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(str)
        End Function

        Protected Sub ChooseModel()
            Dim str As String
            str = "<script language=javascript>alert ('<%=Resources.Resource.repair_svc_main_msg_2()%>');</script>"
            Response.Write(str)
        End Sub

        Protected Sub imgBtnGo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgBtnGo.Click
            Try '6850
                Dim sState As String
                sState = ddlStates.SelectedValue.ToString()

                If Len(Trim(sState)) = 0 Then
                    lblErrStateMsg.Text = Resources.Resource.repair_error_msg_2
                    lblErrStateMsg.Visible = True
                    ddlStates.Focus()
                Else
                    lblErrStateMsg.Visible = False
                    Session("ShowServiceLoc") = ddlStates.SelectedValue.ToString().Trim().ToUpper()
                    Response.Redirect("sony-pg-service-maintenance-repair.aspx", True)
                End If
            Catch ex As Exception '6850 starts
                lbErrMsglModel.Text = Utilities.WrapExceptionforUI(ex)
                lbErrMsglModel.Visible = True
            End Try '6850 ends

        End Sub

        Protected Sub btnRequestDepotService_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnRequestDepotService.Click
            Try '6850
                If Len(Trim(ModelNumber.Text)) = 0 Then
                    lbErrMsglModel.Text = Resources.Resource.repair_svc_main_msg_3
                    lbErrMsglModel.Visible = True
                Else
                    Dim modelsGroups As ModelDetails()
                    Dim cm As CatalogManager = New CatalogManager
                    'Dim objGlobalData As New GlobalData

                    'If Not Session.Item("GlobalData") Is Nothing Then
                    '    objGlobalData = Session.Item("GlobalData")
                    'End If

                    modelsGroups = cm.GetSrhModelDetails(Trim(ModelNumber.Text), HttpContextManager.GlobalData)

                    'Added = to avoid popup blocking
                    If modelsGroups.Length >= 1 Then
                        lbErrMsglModel.Text = ""
                        lbErrMsglModel.Visible = False
                        Response.Redirect("sony-Pg-ModelSearch-Result.aspx?srh=" + Trim(ModelNumber.Text))
                    ElseIf modelsGroups.Length = 1 Then
                        If modelsGroups(0).DiscontinuedFlag <> "Y3" And modelsGroups(0).DiscontinuedFlag <> "Y4" And modelsGroups(0).DiscontinuedFlag <> "Y6" And modelsGroups(0).DiscontinuedFlag <> "Y7" Then
                            If Session.Item("customer") Is Nothing Then
                                blnRegisterforLogin = True
                            Else
                                Response.Redirect("sony-service-sn.aspx?model=" + Trim(ModelNumber.Text.ToUpper()))
                            End If
                        Else
                            Response.Redirect("sony-pg-service-maintenance-repair.aspx?model=" + Trim(ModelNumber.Text.ToUpper()))
                        End If
                    Else
                        lbErrMsglModel.Text = Resources.Resource.el_pts_noMatch
                        ModelNumber.Focus()
                        lbErrMsglModel.Visible = True
                    End If
                End If
            Catch ex As Exception '6850 starts
                lbErrMsglModel.Text = Utilities.WrapExceptionforUI(ex)
                lbErrMsglModel.Visible = True
            End Try '6850 ends
        End Sub

    End Class
End Namespace