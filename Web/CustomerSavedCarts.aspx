<%@ Reference Page="~/SSL.aspx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="SonyFooter" Src="SonyFooter.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="ServicePlusNav" Src="ServicePlusNav.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="PersonalMessage" Src="PersonalMessage.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="TopSonyHeader" Src="TopSonyHeader.ascx" %>
<%@ OutputCache Duration="15" VaryByParam="None" %>

<%@ Page Language="vb" AutoEventWireup="false" Inherits="ServicePLUSWebApp.CustomerSavedCarts"
    EnableViewStateMac="true" CodeFile="CustomerSavedCarts.aspx.vb" %>

<html lang="<%=PageLanguage %>">
<head>
    <title>
        <%=Resources.Resource.ttl_MySavedCarts%>
    </title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link rel="stylesheet" href="includes/ServicesPLUS_style.css" type="text/css">
    <script type="text/javascript" src="includes/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="includes/svcplus_globalization.js"></script>

    <script language="javascript" src="includes/ServicesPLUS.js" type="text/javascript"></script>
</head>
<body style="color: #000000; background: #5d7180; margin: 0px;">
    <form name="form1" runat="server">
        <center>
            <asp:ScriptManager ID="ScriptManager1" runat="server" />
            <table width="760" border="0" role="presentation">
                <tr>
                    <td width="25" style="background: url('images/sp_left_bkgd.gif');">
                        <img src="images/spacer.gif" width="25" height="25" alt="">
                    </td>
                    <td width="710" bgcolor="#ffffff">
                        <table width="710" border="0" role="presentation">
                            <tr>
                                <td>
                                    <ServicePLUSWebApp:TopSonyHeader ID="TopSonyHeader" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <nav><ServicePLUSWebApp:ServicePlusNav ID="SonyNav" runat="server" /></nav>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="710" border="0" role="presentation">
                                        <tr>
                                            <td bgcolor="#363d45" background="images/sp_int_header_top_ServicesPLUS_onepix.gif"
                                                width="464" height="57" align="right" valign="top">
                                                <br />
                                                <h1 class="headerText">ServicesPLUS&nbsp;&nbsp;</h1>
                                            </td>
                                            <td bgcolor="#363d45" valign="top">
                                                <table width="246" border="0" role="presentation">
                                                    <tr>
                                                        <td colspan="3">
                                                            <img src="images/sp_int_header_top_right.gif" width="246" height="24" alt="">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="5">
                                                            &nbsp;
                                                        </td>
                                                        <td width="236">
                                                            <span class="memberName">
                                                                <ServicePLUSWebApp:PersonalMessage ID="PersonalMessageDisplay" runat="server" />
                                                            </span>
                                                        </td>
                                                        <td width="5">
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td bgcolor="#f2f5f8">
                                                <h2 class="headerTitle" style="padding-right: 20px; text-align: right;"><%=Resources.Resource.el_MySavedCarts%></h2>
                                            </td>
                                            <td bgcolor="#99a8b5">
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr height="9">
                                            <td bgcolor="#f2f5f8">
                                                <img src="images/sp_int_header_btm_left_onepix.gif" width="464" height="9" alt="">
                                            </td>
                                            <td bgcolor="#99a8b5">
                                                <img src="images/sp_int_header_btm_right.gif" width="246" height="9" alt="">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="710" border="0" role="presentation">
                                        <tr>
                                            <td width="20" height="20">
                                                <img src="images/spacer.gif" width="20" height="20" alt="">
                                            </td>
                                            <td width="670" height="20">
                                                <asp:Label ID="errorMessageLabel" runat="server" CssClass="tableData" ForeColor="Red"></asp:Label>
                                            </td>
                                            <td width="20" height="20">
                                                <img src="images/spacer.gif" width="20" height="20" alt="">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="20">
                                                <img src="images/spacer.gif" width="20" height="20" alt="">
                                            </td>
                                            <td width="670">
                                                <table width="670" border="0" role="presentation">
                                                    <tr>
                                                        <td>
                                                            <table align="center" border="0" width="670" role="presentation">
                                                                <tr>
                                                                    <td bgcolor="#ffffff" align="left" style="height: 25px">
                                                                        <asp:UpdateProgress ID="UpdateProgress1" runat="server">
                                                                            <ProgressTemplate>
                                                                                <span id="Span4" class="bodycopy"><%=Resources.Resource.prg_PleaseWait%>...</span>
                                                                                <img src="images/progbar.gif" width="100" alt="Please Wait" />
                                                                            </ProgressTemplate>
                                                                        </asp:UpdateProgress>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td width="670" align="center" valign="top">
                                                                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                                                            <ContentTemplate>
                                                                                <asp:DataGrid ID="dgCarts" runat="server" AutoGenerateColumns="False" CellPadding="4"
                                                                                    ForeColor="#333333" GridLines="None" Width="100%" AllowPaging="True" AllowSorting="True"
                                                                                    PageSize="8">
                                                                                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                                                                    <EditItemStyle BackColor="#2461BF" />
                                                                                    <SelectedItemStyle BackColor="#D1DDF1" Font-Bold="True" Font-Italic="False" Font-Overline="False"
                                                                                        Font-Strikeout="False" Font-Underline="False" ForeColor="#333333" />
                                                                                    <PagerStyle BackColor="#D5DEE9" CssClass="tableHeader" HorizontalAlign="Left" Mode="NumericPages" />
                                                                                    <AlternatingItemStyle BackColor="White" Font-Bold="False" Font-Italic="False" Font-Overline="False"
                                                                                        Font-Strikeout="False" Font-Underline="False" />
                                                                                    <ItemStyle BackColor="#EFF3FB" Font-Bold="False" Font-Italic="False" Font-Overline="False"
                                                                                        Font-Strikeout="False" Font-Underline="False" />
                                                                                    <Columns>
                                                                                        <asp:BoundColumn DataField="SEQUENCENUMBER" HeaderText="<%$ Resources:Resource, el_CartNumber%>"
                                                                                            SortExpression="SEQUENCENUMBER">
                                                                                            <HeaderStyle Width="20%" />
                                                                                            <ItemStyle CssClass="bodyCopy" Font-Bold="False" Font-Italic="False" Font-Overline="False"
                                                                                                Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Left" VerticalAlign="Middle" />
                                                                                        </asp:BoundColumn>
                                                                                        <asp:BoundColumn DataField="CartType" HeaderText="<%$ Resources:Resource, el_CartType%>"
                                                                                            SortExpression="CartType">
                                                                                            <HeaderStyle Width="20%" />
                                                                                            <ItemStyle CssClass="bodyCopy" Font-Bold="False" Font-Italic="False" Font-Overline="False"
                                                                                                Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Left" VerticalAlign="Middle" />
                                                                                        </asp:BoundColumn>
                                                                                        <asp:BoundColumn DataField="DATECREATED" HeaderText="<%$ Resources:Resource, el_DateSaved%>"
                                                                                            SortExpression="DATECREATED">
                                                                                            <HeaderStyle Width="20%" />
                                                                                            <ItemStyle CssClass="bodyCopy" Font-Bold="False" Font-Italic="False" Font-Overline="False"
                                                                                                Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Left" VerticalAlign="Middle" />
                                                                                        </asp:BoundColumn>
                                                                                        <asp:BoundColumn DataField="EXPIRATIONDATE" HeaderText="<%$ Resources:Resource, el_ExpirationDate%>">
                                                                                            <HeaderStyle Width="20%" />
                                                                                            <ItemStyle CssClass="bodyCopy" Font-Bold="False" Font-Italic="False" Font-Overline="False"
                                                                                                Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Left" VerticalAlign="Middle" />
                                                                                        </asp:BoundColumn>
                                                                                        <asp:BoundColumn DataField="CARTITEMS" HeaderText="<%$ Resources:Resource, el_NoOfItems%>">
                                                                                            <HeaderStyle Width="10%" HorizontalAlign="Center" />
                                                                                            <ItemStyle CssClass="bodyCopy" Font-Bold="False" Font-Italic="False" Font-Overline="False"
                                                                                                Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                        </asp:BoundColumn>
                                                                                        <asp:BoundColumn DataField="YOURPRICE" HeaderText="<%$ Resources:Resource, el_Total%>"
                                                                                            Visible="false" DataFormatString="{0:$#####.##}">
                                                                                            <HeaderStyle Width="10%" HorizontalAlign="Center" />
                                                                                            <ItemStyle CssClass="bodyCopy" Font-Bold="False" Font-Italic="False" Font-Overline="False"
                                                                                                Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                        </asp:BoundColumn>
                                                                                        <asp:TemplateColumn HeaderText="<%$ Resources:Resource, el_ViewCart%>">
                                                                                            <ItemTemplate>
                                                                                                <a href="#" class="bodycopy" onclick="window.location.href='vc.aspx?crtnum=<%#DataBinder.Eval(Container, "DataItem.SEQUENCENUMBER") %>';return false;">Open</a>
                                                                                                <%-- <asp:ImageButton runat="server" ImageUrl="~/images/viewdetail.GIF" CommandArgument="OpenCart" AlternateText='<%#DataBinder.Eval(Container, "DataItem.SEQUENCENUMBER") %>'>
                                                                                                </asp:ImageButton>--%>
                                                                                            </ItemTemplate>
                                                                                            <HeaderStyle Width="10%" HorizontalAlign="Center" />
                                                                                            <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                                                Font-Underline="False" HorizontalAlign="Center" />
                                                                                        </asp:TemplateColumn>
                                                                                    </Columns>
                                                                                    <HeaderStyle BackColor="#D5DEE9" CssClass="tableHeader" Font-Bold="True" />
                                                                                </asp:DataGrid>
                                                                            </ContentTemplate>
                                                                        </asp:UpdatePanel>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <img src="images/spacer.gif" width="670" height="30" alt="">
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td width="20">
                                                <img src="images/spacer.gif" width="20" height="20" alt="">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <footer><ServicePLUSWebApp:SonyFooter ID="SonyFooter1" runat="server" /></footer>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td background="images/sp_right_bkgd.gif" style="width: 25px">
                        <img src="images/spacer.gif" width="25" height="20" alt="">
                    </td>
                </tr>
            </table>
        </center>
    </form>
</body>
</html>
