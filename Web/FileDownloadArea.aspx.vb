Imports System
Imports System.IO
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Drawing
Imports System.Web
Imports System.Web.SessionState
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Process
Imports Sony.US.ServicesPLUS.Data
Imports Sony.US.siam
Imports System.Text
Imports System.Web.HttpUtility
Imports Sony.US.AuditLog


Namespace ServicePLUSWebApp


Partial Class FileDownloadArea
    Inherits SSL

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        isProtectedPage(True)
        InitializeComponent()
    End Sub

#End Region



'-- page members
#Region "page members"

        Private Const conFileNamePlaceHolder As String = "##FILENAME##"
        Private Const conContentType As String = "APPLICATION/OCTET-STREAM"
        Private Const conHeaderType As String = "Attachment; Filename=""" + conFileNamePlaceHolder + """"
        Private Const conAppendedHeaderType As String = "Content-Disposition"
        Private Const conQueryStringName As String = "ln"
        Private orderLineSequencenumber As Int32 = 0
        Dim conFileDownloadPath As String



#End Region



'-- events
#Region "Events"

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Not IsPostBack Then processPage(sender, e)
    End Sub

#End Region



'-- methods
#Region "Methods"



    Private Sub processPage(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try
                If HttpContextManager.Customer IsNot Nothing And Not Session.Item("Order") Is Nothing Then
                    Dim thisCustomer As Customer = HttpContextManager.Customer
                    Dim thisOrder As Sony.US.ServicesPLUS.Core.Order = Session.Item("Order")

                    If Not Request.QueryString(conQueryStringName) Is Nothing And IsNumeric(Request.QueryString(conQueryStringName)) Then
                        Dim thisFileName As String = getFileName(thisOrder, Integer.Parse(Request.QueryString(conQueryStringName)))

                        If thisFileName.Length > 0 Then pushFileToClient(thisCustomer, thisFileName)
                    End If
                End If
        Catch threadExp As Threading.ThreadAbortException
            'do nothing as response.redirect would throw this exception
        Catch ex As Exception
            handleError(ex)
        End Try
    End Sub
        Private Function getFileName(ByRef thisOrder As Sony.US.ServicesPLUS.Core.Order, ByVal OrderLineNumber As Integer) As String
            Dim returnValue As String = ""
            Try
                For Each thisLineItem As OrderLineItem In thisOrder.LineItems
                    If thisLineItem.LineNumber = OrderLineNumber.ToString() And thisLineItem.Product.Type = Product.ProductType.Software Then
                        returnValue = thisLineItem.SoftwareDownloadFileName.ToString()
                        orderLineSequencenumber = thisLineItem.SequenceNumber
                        LogDownLoad(thisOrder, thisLineItem)
                        Exit For
                    End If
                Next
            Catch ex As Exception
                handleError(ex)
            End Try
            Return returnValue
        End Function
        Private Sub LogDownLoad(ByRef thisOrder As Order, ByRef thisLineItem As OrderLineItem)
            Dim om As OrderManager = New OrderManager
            Dim downloadRecord As DownloadRecord = New DownloadRecord
            Dim customer As Customer = HttpContextManager.Customer

            downloadRecord.CustomerID = customer.CustomerID
            downloadRecord.CustomerSequenceNumber = customer.SequenceNumber
            downloadRecord.SIAMID = customer.SIAMIdentity
            downloadRecord.Time = Now
            downloadRecord.DeliveryMethod = "Download"

            downloadRecord.KitPartNumber = ""
            downloadRecord.ModelID = ""
            downloadRecord.ModelDescription = ""
            downloadRecord.Version = ""
            downloadRecord.FreeOfCharge = 0
            downloadRecord.KitDescription = ""

            downloadRecord.OrderNumber = thisOrder.OrderNumber
            downloadRecord.OrderLineItemSequence = thisLineItem.SequenceNumber

			downloadRecord.HTTP_X_Forward_For = HttpContextManager.GetServerVariableValue("HTTP_X_FORWARDED_FOR")
			downloadRecord.RemoteADDR = HttpContextManager.GetServerVariableValue("REMOTE_ADDR")
			downloadRecord.HTTPReferer = HttpContextManager.GetServerVariableValue("HTTP_REFERER")
			downloadRecord.HTTPURL = HttpContextManager.GetServerVariableValue("HTTP_URL")

			If downloadRecord.HTTPURL = "" Then
				downloadRecord.HTTPURL = HttpContextManager.GetServerVariableValue("URL") + ReturnQueryString()
			End If

			downloadRecord.HTTPUserAgent = HttpContextManager.GetServerVariableValue("HTTP_USER_AGENT")

            om.LogDownload(downloadRecord)
        End Sub

        Private Sub pushFileToClient(ByRef thisCustomer As Customer, ByVal thisFileName As String)
            Dim objPCILogger As New PCILogger() '6524
            Try
                '6524 start
                objPCILogger.EventOriginApplication = "ServicesPLUS"
                objPCILogger.EventOriginApplicationLocation = Me.Page.GetType().Name
                objPCILogger.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
                objPCILogger.OperationalUser = Environment.UserName
                objPCILogger.EventDateTime = DateTime.Now.ToLongTimeString()
                objPCILogger.HTTPRequestObjectValues = objPCILogger.GetRequestContextValue()
                objPCILogger.IndicationSuccessFailure = "Success"
                objPCILogger.EventOriginMethod = "pushFileToClient"
                objPCILogger.Message = "pushFileToClient Success."
                objPCILogger.EventType = EventType.Others
                objPCILogger.CustomerID = thisCustomer.CustomerID
                objPCILogger.CustomerID_SequenceNumber = thiscustomer.SequenceNumber
                'objPCILogger.UserName = thisCustomer.UserName
                objPCILogger.EmailAddress = thisCustomer.EmailAddress '6524
                objPCILogger.SIAM_ID = thisCustomer.SIAMIdentity
                objPCILogger.LDAP_ID = thisCustomer.LdapID '6524

                '6524 end

                If Not thisCustomer Is Nothing And thisFileName.Length > 0 Then
                    Dim downloadURL As String = String.Empty
                    Dim downloadPath As String = String.Empty


                    Try
                        'Dim config_settings As Hashtable
                        'Dim handler As New Sony.US.SIAMUtilities.ConfigHandler("SOFTWARE\SERVICESPLUS")
                        'config_settings = handler.GetConfigSettings("//item[@name='Download']")
                        'downloadURL = config_settings.Item("url").ToString()
                        'downloadPath = config_settings.Item("physical").ToString()
                        'conFileDownloadPath = config_settings.Item("downloadfiles").ToString()
                        downloadURL = Sony.US.SIAMUtilities.ConfigurationData.Environment.Download.Url
                        downloadPath = Sony.US.SIAMUtilities.ConfigurationData.Environment.Download.Physical
                        conFileDownloadPath = Sony.US.SIAMUtilities.ConfigurationData.Environment.Download.DownloadFiles
                    Catch ex As Exception
                        handleError(ex)
                    End Try
                    If File.Exists(downloadPath + "/" + Me.conFileDownloadPath + "/" + thisFileName) Then
                        Dim file_path As String = String.Empty
                        Dim headerInfo As String = conHeaderType.Replace(conFileNamePlaceHolder, thisFileName)
                        Try
                            file_path = downloadPath + "/" + Me.conFileDownloadPath + "/" + thisFileName
                        Catch ex As Exception
                            Response.Write("path not found")
                            Response.Write(file_path)
                        End Try
                        Try
                            Response.ContentType = conContentType
                            Response.AppendHeader(conAppendedHeaderType, headerInfo)
                            Response.TransmitFile(file_path)
                        Catch ex As Exception
                            handleError(ex)
                        End Try
                    End If
                End If
            Catch threadEx As Threading.ThreadAbortException
            Catch ex As Exception
                objPCILogger.IndicationSuccessFailure = "Failure" '6524
                objPCILogger.Message = "pushFileToClient Failed. " & ex.Message.ToString() '6524
                handleError(ex)
            Finally
                objPCILogger.PushLogToMSMQ() '6524
            End Try
        End Sub

#End Region
End Class

End Namespace
