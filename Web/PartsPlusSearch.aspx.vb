Imports System.Web.Services
Imports ServicesPlusException
Imports Sony.US.AuditLog
Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Process
Imports Sony.US.SIAMUtilities

Namespace ServicePLUSWebApp
    Partial Class PartsPlusSearch
        Inherits SSL

        Dim custMgr As New CustomerManager
        Dim promoManager As New PromotionManager
        Dim catMgr As New CatalogManager
        Dim siamID As String
        Dim TotalPerPage As Integer = 50
        Dim partList() As Sony.US.ServicesPLUS.Core.Part
        Dim customer As Customer

        Const URL_AVAILABILITY_NEWWINDOW As String = "javascript:window.open('{0}.aspx?ArrayVal={1}&isKit=0', '_blank', '" + CON_POPUP_FEATURES + "');"

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub


        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As EventArgs) Handles MyBase.Load
            Try
                customer = HttpContextManager.Customer
                AddToCartAfterLogin(ErrorLabel)
                lblInstructions.Visible = False
                txtRefineSearch.Attributes("onkeydown") = "SetTheFocusButton(event, 'btnRefineSearch')"
                'fnGetResourceData()
                ' Set up the Configuration values into the HiddenField objects for the JavaScript file to read.
                quickOrderMaxItems.Value = ConfigurationData.GeneralSettings.General.QuickOrderMaxItems
                orderUploadMaxItems.Value = ConfigurationData.GeneralSettings.General.OrderUploadMaxItems

                'Clear ErrorLabels
                lblRefine.Text = ""
                ErrorLabel.Text = ""
                lblGoToError.Visible = False

                ' Fetch the main Model from the Session if it was already set, or from the prior page's Array.
                If (SelectedModel Is Nothing) Then
                    If (Session.Item("models") IsNot Nothing) And (Request.QueryString("ArrayVal") IsNot Nothing) Then
                        Dim models As Array = CType(Session.Item("models"), Array)
                        ' This will throw a FormatException if the user modified the Query String to include a non-integer value
                        Dim arrayIndex As Int32 = Convert.ToInt32(Request.QueryString("ArrayVal").Trim())

                        If (0 <= arrayIndex < models.Length) Then
                            SelectedModel = models.GetValue(arrayIndex)
                        Else        ' The user probably modified the Query String. Handled below, along with non-integer values.
                            Throw New FormatException("ArrayVal Query String was outside of expected range. Value: " & arrayIndex)
                        End If
                    Else
                        ' If all of these Session variables are Null, then the Session has probably expired.
                        Response.Redirect("SessionExpired.aspx?error=searchexpired&returnto=partsearch", True)
                    End If
                End If

                If Not Page.IsPostBack() Then
                    tdLegend.Visible = False
                    PopulateModelOptions()
                    PopulateCategories()
                    PopulateModules()

                    ' If the user signed in and returned to the page, auto-fill all the selected dropdown items and text box
                    If (Request.QueryString("returnfrom") IsNot Nothing) AndAlso (Request.QueryString("returnfrom") = "signin") Then
                        FillDetailsFromSession()
                    End If
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

#Region "      Dropdown Events"
        Public Sub ddlOptions_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As EventArgs) Handles ddlOptions.SelectedIndexChanged
            Try
                If SelectedModel Is Nothing Then Response.Redirect("SessionExpired.aspx?error=searchexpired&returnto=partsearch", True)
                'rblRefineSearchType.Visible = True
                'txtRefineSearch.Visible = True
                'btnRefineSearch.Visible = True

                'Make chosen option the new "model" called submodel
                If ddlOptions.SelectedIndex > 0 Then
                    btnClearFields.Visible = True
                    lblInstructions.Visible = False
                    SelectedOption = SelectedModel.SubModels.GetValue(ddlOptions.SelectedIndex - 1)
                    SelectedCategory = Nothing
                    SelectedModule = Nothing
                Else  ' User selected "All Options"
                    SelectedOption = SelectedModel
                End If
                PopulateCategories()
                PopulateModules()
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Public Sub ddlCategories_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As EventArgs) Handles ddlCategories.SelectedIndexChanged
            Try
                If ddlCategories.SelectedIndex > 0 Then
                    btnClearFields.Visible = True
                    SelectedCategory = SelectedOption.Categories.GetValue(ddlCategories.SelectedIndex - 1)
                Else
                    lblInstructions.Visible = True
                    SelectedCategory = Nothing
                End If
                SelectedModule = Nothing
                ModuleList = Nothing
                PopulateModules()
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Public Sub ddlModules_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As EventArgs) Handles ddlModules.SelectedIndexChanged
            Try
                If ddlModules.SelectedIndex > 0 Then
                    pnlRefineSearch.Visible = True
                    If (ModuleList IsNot Nothing) AndAlso (ddlModules.SelectedIndex <= ModuleList.Length) Then
                        SelectedModule = ModuleList.GetValue(ddlModules.SelectedIndex - 1)
                    Else    ' Selected Index was outside the Array range, or the Array is null due to Session variable loss.
                        Dim listLength As String = "null"
                        If ModuleList IsNot Nothing Then listLength = ModuleList.Length
                        Utilities.LogDebug($"PartsPlusSearch.ddlModules_SelectedIndexChanged - Module length = {listLength}, SelectedIndex = {ddlModules.SelectedIndex}")
                        ' Clear the search results
                        pnlRefineSearch.Visible = False
                        SelectedModule = Nothing
                        partList = Nothing
                        Session.Remove("partList")
                        FillPartsGrid()
                        Return
                    End If
                    DoPartSearch()
                    FillPartsGrid()
                Else
                    pnlRefineSearch.Visible = False
                    SelectedModule = Nothing
                    partList = Nothing
                    Session.Remove("partList")
                    FillPartsGrid()
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub
#End Region

        Private Sub btnRefineSearch_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnRefineSearch.Click
            Try
                If String.IsNullOrWhiteSpace(txtRefineSearch.Text) Then
                    lblRefine.Text = Resources.Resource.el_SearchText ' "Please enter search text."
                Else
                    lblRefine.Text = ""
                    Session("refineSearchType") = rblRefineSearchType.SelectedValue
                    ' Ensure they aren't trying some script injections or similar intrusions.
                    If Not IsValidString(txtRefineSearch.Text.Trim()) Then
                        lblRefine.Text = "Invalid characters detected. Please try again."
                        txtRefineSearch.Text = ""
                        Return
                    End If
                    Session("refineSearchText") = txtRefineSearch.Text.Trim()

                    If SelectedModel Is Nothing Then
                        RedirectToPartSearch()
                    Else
                        grdPartsList.PageIndex = 0
                        DoPartSearch()
                        FillPartsGrid()
                    End If
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Sub btnClearFields_Click(ByVal sender As System.Object, ByVal e As ImageClickEventArgs) Handles btnClearFields.Click
            Try
                SelectedOption = Nothing
                SelectedCategory = Nothing
                SelectedModule = Nothing
                lblInstructions.Visible = True
                tdLegend.Visible = False
                PopulateModelOptions()
                PopulateCategories()
                PopulateModules()
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Sub btnNewSearch_Click(ByVal sender As System.Object, ByVal e As ImageClickEventArgs) Handles btnNewSearch.Click
            RedirectToPartSearch()
        End Sub

#Region "    Parts List GridView Events     "
        Public Sub grdPartsList_RowDataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles grdPartsList.RowDataBound
            If e.Row.RowType <> DataControlRowType.DataRow Then Exit Sub

            Try
                Dim newURL As String = ""
                Dim arrayVal As String = Request.QueryString("ArrayVal")
                Dim part As Part = CType(e.Row.DataItem, Part)
                Dim rowIndex As Integer = e.Row.DataItemIndex
                Dim isValidPart As Boolean = True
                Dim promoMarker As String = "<span style=""font-family: Wingdings; color: red; font-size: 12px;"">v</span>"
                Dim lnkAvailability As HyperLink = CType(e.Row.FindControl("lnkAvailability"), HyperLink)
                Dim cmdAddToCart As ImageButton = CType(e.Row.FindControl("cmdAddToCart"), ImageButton)

                If (part.ListPrice = 0.0R) Then isValidPart = False ' Cannot compare Double to integer 0, use 0.0R for Real/Double

                If Not IsValidString(arrayVal) Then
                    ErrorLabel.Text = "Modified URL detected. Please go back or try a New Search."
                    Return
                End If

                If isValidPart Then
                    If promoManager.IsPromotionPart(part.ReplacementPartNumber) Then
                        Dim lblPrice As Label = CType(e.Row.FindControl("lblPrice"), Label)
                        lblPrice.Text += promoMarker
                        tdLegend.Visible = True     ' Display the box explaining the Promotional Price marker
                    End If

                    ' 2018-12-14 - Change the background colour if the part has been replaced
                    If part.PartNumber <> part.ReplacementPartNumber Then
                        Dim lblReplacementPart As Label = CType(e.Row.FindControl("lblReplacementPart"), Label)
                        lblReplacementPart.BackColor = Drawing.ColorTranslator.FromHtml("#88ee88")
                    End If

                    ' For non-SAP customer, default to the "Availability" link.
                    newURL = String.Format(URL_AVAILABILITY_NEWWINDOW, "Availability", rowIndex)
                    lnkAvailability.NavigateUrl = newURL
                    lnkAvailability.Text = Resources.Resource.el_Availability

                    If customer IsNot Nothing Then
                        If (customer.SAPBillToAccounts.Length > 0 Or customer.SISLegacyBillToAccounts.Length > 0) Then
                            ' If the Customer has an SAP account, they may have a custom price. Show the "Your Price" link.
                            newURL = String.Format(URL_AVAILABILITY_NEWWINDOW, "YourPrice", rowIndex)
                            lnkAvailability.NavigateUrl = newURL
                            lnkAvailability.Text = Resources.Resource.el_YourPrice
                        End If
                        newURL = $"AddToCart_onclick('{part.ReplacementPartNumber}'); return false;"
                    Else
                        newURL = $"window.location.href='NotLoggedInAddToCart.aspx?post=pps&ArrayVal={arrayVal}&PartLineNo={rowIndex}'"
                    End If
                    cmdAddToCart.Attributes.Add("onclick", newURL)
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Public Sub grdPartsList_RowCommand(ByVal sender As Object, ByVal e As GridViewCommandEventArgs) Handles grdPartsList.RowCommand
            Dim validCommands As String = "AddToCatalog,ExplodedView" ' AddToCart,
            Dim rowIndex As Integer
            Dim arrayVal As Integer
            Dim partNumber As String = ""
            Dim newURL As String
            Dim debugMessage As String = $"PartsPlusSearch.aspx - grdPartsList.RowCommand - START at {DateTime.Now.ToShortTimeString}.{Environment.NewLine}"

            Try
                ErrorLabel.Text = ""
                If (Not String.IsNullOrEmpty(e.CommandName)) AndAlso (validCommands.Contains(e.CommandName)) Then
                    If Not IsValidString(Request.QueryString("ArrayVal")) Then
                        ErrorLabel.Text = "Modified URL detected. Please go back or try a New Search."
                        Return
                    Else
                        arrayVal = Integer.Parse(Request.QueryString("ArrayVal"))
                        debugMessage &= $"- ArrayVal = {arrayVal}, "
                    End If
                    ' Get the Part Number from the "DataKeys" field as it's bound in the GridView.
                    ' A GridView's Row.DataItem is not available after DataBinding event is complete.
                    debugMessage &= $"Command Argument = {e.CommandArgument}, "
                    rowIndex = Integer.Parse(e.CommandArgument.ToString())
                    rowIndex = rowIndex - (50 * (grdPartsList.PageIndex)) ' DataKeys are not paged like DataRowIndex is. This will lead to Index Out of Range Exceptions unless we subtract.
                    'rowIndex = TryCast(TryCast(sender, ImageButton).NamingContainer, GridViewRow).RowIndex
                    debugMessage &= $"Row Index = {rowIndex}, DataKeys.Count = {grdPartsList.DataKeys.Count}.{Environment.NewLine}"
                    partNumber = grdPartsList.DataKeys(rowIndex).Value.ToString()
                    'If e.CommandName = "AddToCart" Then
                    '    If customer Is Nothing Then
                    '        newURL = String.Format("NotLoggedInAddToCart.aspx?post=pps&ArrayVal={0}&PartLineNo={1}", arrayVal, rowIndex)
                    '        Response.Redirect(newURL, False)
                    '    Else
                    '        ' Call the AddToCart JavaScript function so it can display the jQuery Modal window
                    '        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "addToCartScript", "AddToCart_onclick('" & partNumber & "')", True)
                    '    End If
                    If e.CommandName = "AddToCatalog" Then
                        If HttpContextManager.Customer Is Nothing Then
                            newURL = $"NotLoggedInAddToCatalog.aspx?post=pps&ArrayVal={arrayVal}&PartLineNo={rowIndex}"
                            Response.Redirect(newURL, False)
                        Else
                            debugMessage &= $"- Add to Catalog - PartNumber = {partNumber}.{Environment.NewLine}"
                            AddToCatalog(partNumber, ErrorLabel)
                        End If
                    ElseIf e.CommandName = "ExplodedView" Then
                        Response.Redirect("View2.aspx?ArrayVal=" & arrayVal.ToString(), False)
                    End If
                End If
                'Catch fex As FormatException
                '    ErrorLabel.Text = "Modified URL or expired session detected. Please go back or try a new search."
            Catch ex As Exception
                Utilities.LogMessages(debugMessage)
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Public Sub grdPartsList_PageIndexChanging(ByVal sender As Object, ByVal e As GridViewPageEventArgs) Handles grdPartsList.PageIndexChanging
            Try
                grdPartsList.PageIndex = e.NewPageIndex
                FillPartsGrid()   ' Re-bind the Data so it refreshes properly.
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub
#End Region

        'Private Function GetSubModel(ByRef currentModel As Model) As Model
        Private Sub GetSubModel(ByRef currentModel As Model)
            If Session.Item("selectedOption") IsNot Nothing Then
                currentModel = Session.Item("selectedOption")
            Else
                currentModel = Session.Item("model")
            End If
            'Return currentModel
        End Sub

        Private Sub RedirectToPartSearch()
            If HttpContextManager.Customer Is Nothing Then
                Response.Redirect("sony-parts.aspx", False)
            Else
                Response.Redirect("au-sony-parts.aspx", False)
            End If
            HttpContext.Current.ApplicationInstance.CompleteRequest()
        End Sub

        Private Sub PopulateModelOptions()
            Try
                If SelectedModel Is Nothing Then Response.Redirect("SessionExpired.aspx?error=searchexpired&returnto=partsearch", True)

                lblModel.Text = SelectedModel.ModelID
                lblSerial.Text = SelectedModel.Destination + " " + SelectedModel.SerialRange

                If (SelectedModel.SubModels Is Nothing) OrElse (SelectedModel.SubModels.Length = 0) Then
                    catMgr.GetSubModels(SelectedModel)
                End If

                'Show Options as either Label or DropDownList depending on the length
                If SelectedModel.SubModels.Length = 1 Then
                    lblSingleOption.Visible = True
                    lblSingleOption.Text = SelectedModel.ModelID
                    ddlOptions.Visible = False
                    SelectedOption = SelectedModel
                ElseIf SelectedModel.SubModels.Length > 1 Then
                    lblInstructions.Visible = True
                    lblOptions.Visible = True
                    lblSingleOption.Visible = False
                    ddlOptions.Visible = True
                    ddlOptions.Items.Clear()
                    ddlOptions.Items.Add(New ListItem(Resources.Resource.el_AllOptions, "-1"))
                    ddlOptions.DataSource = SelectedModel.SubModels
                    ddlOptions.DataBind()
                    ddlOptions.SelectedValue = IIf(SelectedOption IsNot SelectedModel, SelectedOption.ModelID, "-1")
                Else    ' No Submodels
                    lblOptions.Visible = False
                    lblSingleOption.Visible = False
                    ddlOptions.Items.Clear()
                    ddlOptions.Visible = False
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Sub PopulateCategories()
            Try
                If (SelectedOption.Categories Is Nothing) OrElse (SelectedOption.Categories.Length = 0) Then
                    catMgr.GetModelCategories(SelectedOption)
                End If

                If SelectedOption.Categories.Length = 1 Then
                    ' Since there is only one category, set the category value to it automatically
                    SelectedCategory = SelectedOption.Categories.GetValue(0)
                    lblSingleCategory.Visible = True
                    lblSingleCategory.Text = SelectedCategory.Description
                    ddlCategories.Visible = False
                ElseIf SelectedOption.Categories.Length > 1 Then
                    lblSingleCategory.Visible = False
                    ddlCategories.Visible = True
                    ddlCategories.Items.Clear()
                    ddlCategories.Items.Add(New ListItem(Resources.Resource.el_SelectOne, "-1"))
                    ddlCategories.DataSource = SelectedOption.Categories
                    ddlCategories.DataBind()
                    If (SelectedCategory Is Nothing) Then
                        ddlCategories.SelectedValue = "-1"
                    Else
                        ddlCategories.SelectedValue = SelectedCategory.CategoryID
                    End If
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Sub PopulateModules()
            Try
                If SelectedCategory Is Nothing Then
                    ddlModules.Visible = False
                    lblModules.Visible = False
                    lblSingleModule.Visible = False
                    SelectedModule = Nothing
                    ModuleList = Nothing
                Else
                    ModuleList = catMgr.GetModules(SelectedOption, SelectedCategory)
                    If (ModuleList Is Nothing) OrElse (ModuleList.Length = 0) Then
                        lblModules.Visible = False
                        lblSingleModule.Visible = False
                        ddlModules.Visible = False
                    ElseIf ModuleList.Length > 1 Then
                        lblModules.Visible = True
                        lblSingleModule.Visible = False
                        ddlModules.Visible = True
                        ddlModules.Items.Clear()
                        ddlModules.Items.Add(New ListItem(Resources.Resource.el_SelectOne, "-1"))
                        ddlModules.DataSource = ModuleList
                        ddlModules.DataBind()
                        'ddlModules.SelectedValue = IIf(SelectedModule Is Nothing, "-1", SelectedModule.ModuleID)
                        If (SelectedModule IsNot Nothing) AndAlso (ddlModules.Items.FindByValue(SelectedModule.ModuleID) IsNot Nothing) Then
                            ddlModules.SelectedValue = SelectedModule.ModuleID
                        Else
                            If (SelectedModule IsNot Nothing) Then Utilities.LogMessages("SelectedModule.ModuleID = " & SelectedModule.ModuleID)
                            ddlModules.SelectedValue = "-1"
                        End If
                    Else
                        SelectedModule = ModuleList.GetValue(0)
                        lblSingleModule.Text = SelectedModule.Name
                        lblSingleModule.Visible = True
                        lblModules.Visible = True
                        ddlModules.Visible = False
                    End If
                End If

                If SelectedModule Is Nothing Then
                    partList = Nothing
                    Session.Remove("partList")
                    FillPartsGrid()
                Else
                    DoPartSearch()
                    FillPartsGrid()
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Sub DoPartSearch()   ' Optional ByVal pageNumber As Int16 = 1
            Try
                If String.IsNullOrWhiteSpace(txtRefineSearch.Text) Then
                    partList = catMgr.PartSearch2(SelectedOption, SelectedCategory, SelectedModule, "", HttpContextManager.GlobalData)
                Else
                    If (rblRefineSearchType.SelectedValue = 2) Then   ' By Component
                        partList = catMgr.PartSearch2(SelectedOption, SelectedCategory, SelectedModule, txtRefineSearch.Text, HttpContextManager.GlobalData, Sony.US.ServicesPLUS.Core.Part.PartsFinderRefineSearchType.ByComponentReference)
                    Else   ' By Description
                        partList = catMgr.PartSearch2(SelectedOption, SelectedCategory, SelectedModule, txtRefineSearch.Text, HttpContextManager.GlobalData)
                    End If
                End If
                Session("partList") = partList
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Sub FillPartsGrid()
            Try
                If (partList Is Nothing) And (Session.Item("partList") IsNot Nothing) Then
                    partList = CType(Session.Item("partList"), Part())
                End If

                If (partList Is Nothing) Then
                    pnlRefineSearch.Visible = False
                    grdPartsList.DataSource = Nothing
                Else
                    pnlRefineSearch.Visible = (partList.Length > 0)
                    grdPartsList.DataSource = partList
                End If
                grdPartsList.DataBind()
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        ''' <summary>
        '''  If the page has lost variable scope, attempt to reload information from Session Variables. Occurs if the user signed in and returns to the page.
        ''' </summary>
        Private Sub FillDetailsFromSession()
            Try
                If SelectedOption IsNot Nothing Then
                    ddlOptions.SelectedValue = SelectedOption.ModelID
                    ddlOptions_SelectedIndexChanged(Nothing, Nothing)
                End If

                If SelectedCategory IsNot Nothing Then
                    ddlCategories.SelectedValue = SelectedCategory.CategoryID
                    ddlCategories_SelectedIndexChanged(Nothing, Nothing)
                End If

                If SelectedModule IsNot Nothing Then
                    ddlModules.SelectedValue = SelectedModule.ModuleID
                    ddlModules_SelectedIndexChanged(Nothing, Nothing)
                End If

                If Session("refineSearchType") IsNot Nothing Then
                    rblRefineSearchType.SelectedValue = Session("refineSearchType").ToString()
                End If

                If Session("refineSearchText") IsNot Nothing Then
                    txtRefineSearch.Text = Session("refineSearchText").ToString()
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        'Private Sub btnNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNext.Click
        '	Try
        '		PageNum = btnNext.CommandArgument
        '		ShowPartsResults(PageNum, TotalPerPage)
        '	Catch ex As Exception
        '		ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
        '	End Try
        'End Sub

        'Private Sub btnPrevious_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrevious.Click
        '	Try
        '		PageNum = btnPrevious.CommandArgument
        '		ShowPartsResults(PageNum, TotalPerPage)
        '	Catch ex As Exception
        '		ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
        '	End Try
        'End Sub

        'Private Sub btnGoToPage_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnGoToPage.Click
        '	Try
        '		If txtPageNumber.Text = "" Or Not IsNumeric(txtPageNumber.Text) Then
        '			lblGoToError.Text = "Invalid page number."
        '			lblGoToError.Visible = True
        '			PageNum = Session.Item("CurrentPageNum")
        '			ShowPartsResults(PageNum, TotalPerPage)
        '		Else
        '			PageNum = Int(txtPageNumber.Text)
        '			txtPageNumber.Text = ""
        '			ShowPartsResults(PageNum, TotalPerPage)
        '		End If
        '	Catch ex As Exception
        '		ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
        '	End Try
        'End Sub

        'Private Sub btnNext2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNext2.Click
        '	Try
        '		PageNum = btnNext2.CommandArgument
        '		ShowPartsResults(PageNum, TotalPerPage)
        '	Catch ex As Exception
        '		ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
        '	End Try
        'End Sub

        'Private Sub btnPrevious2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrevious2.Click
        '	Try
        '		PageNum = btnPrevious2.CommandArgument
        '		ShowPartsResults(PageNum, TotalPerPage)
        '	Catch ex As Exception
        '		ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
        '	End Try
        'End Sub

        'Private Sub btnGoToPage2_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnGoToPage2.Click
        '	Try
        '		If txtPageNumber2.Text = "" Or Not IsNumeric(txtPageNumber2.Text) Then
        '			lblGoToError.Text = "Invalid page number."
        '			lblGoToError.Visible = True
        '			PageNum = Session.Item("CurrentPageNum")
        '			ShowPartsResults(PageNum, TotalPerPage)
        '		Else
        '			PageNum = Int(txtPageNumber2.Text)
        '			txtPageNumber2.Text = ""
        '			ShowPartsResults(PageNum, TotalPerPage)
        '		End If
        '	Catch ex As Exception
        '		ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
        '	End Try
        'End Sub

        'Private Sub formFieldInitialization()
        '    Me.txtPageNumber.Attributes("onkeydown") = "SetTheFocusButton(event, 'btnGoToPage')"
        '    Me.txtPageNumber2.Attributes("onkeydown") = "SetTheFocusButton(event, 'btnGoToPage2')"
        'End Sub

        'Public Sub AddToCartWithDialog(ByVal partNumber As String)
        '    Dim wasException As Boolean = False
        '    Dim plainPartNumber As String
        '    Dim totalNull = ""
        '    Dim count = 0
        '    Dim quantity As Integer = 1
        '    Dim itemsInCart As Label
        '    Dim totalPriceOfCart As Label
        '    Dim searchResult As PartresultSearch

        '    plainPartNumber = partNumber.Replace("-", "")
        '    dialog.Visible = True
        '    divMessage.InnerHtml = "<br/>"
        '    divException.InnerText = ""
        '    divException.Visible = False
        '    divPrg.InnerHtml = "<span id='progress'><%=Resources.Resource.prg_PleaseWait%><br/> <img src='images/progbar.gif' alt ='Progress' /></span>"


        '    If String.IsNullOrEmpty(partNumber) Then
        '        dialog.Attributes("title") = Resources.Resource.atc_title_OrderExceptions  ' Order Add to Cart Exceptions
        '        divMessage.InnerHtml = "Please enter at least one part number."
        '        divMessage.InnerHtml &= "<br/> <br/> <a><img src='<%=Resources.Resource.img_btnCloseWindow%>' alt ='Close' onclick='javascript:return closedilog();' id='messageclose' /></a>"
        '        Return
        '    End If

        '    itemsInCart = CType(Page.FindControl("itemsInCart"), Label)
        '    totalPriceOfCart = CType(Page.FindControl("totalPriceOfCart"), Label)
        '    dialog.Attributes("title") = Resources.Resource.atc_title_OrderStatus
        '    dialog.Attributes("height") = "200"
        '    dialog.Attributes("width") = "560"

        '    divMessage.InnerHtml = "<br/><b>Adding item " & plainPartNumber & " to cart... </b><br/>"
        '    searchResult = addPartToCartwSSearch(plainPartNumber, "1")
        '    If searchResult.differentCart = True Then
        '        wasException = True
        '        divException.InnerHtml &= "<br/><b>" & Resources.Resource.atc_AddingItem & "</b><br/>"
        '    End If

        '    If searchResult.success <> 1 Then
        '        wasException = True
        '        divException.InnerHtml &= "<br/><b>" & searchResult.message & "</b><br/>"
        '    End If

        '    If searchResult.success = 1 Or searchResult.success = 2 Then
        '        divMessage.InnerHtml = "<br/><b>" & Resources.Resource.atc_AddingItem & plainPartNumber & Resources.Resource.atc_ToCart & " </b><br/>"
        '        searchResult.partnumber = ""
        '        searchResult.Quantity = ""
        '        itemsInCart.Text = searchResult.items.Length
        '        totalPriceOfCart.Text = searchResult.totalAmount
        '    End If

        '    If wasException Then
        '        divPrg.InnerHtml = ""
        '        dialog.Attributes("title") = Resources.Resource.atc_title_OrderExceptions
        '        divMessage.InnerText = ""
        '        divException.InnerHtml &= "<br/> <br/><a ><img src =" & Resources.Resource.img_btnCloseWindow & " alt ='Close' onclick='javascript:return closedilog();'  id='messageclose' /></a>"
        '        divException.Visible = True
        '    Else
        '        dialog.Visible = False
        '    End If
        'End Sub

        <WebMethod(EnableSession:=True)>
        Public Shared Function addPartToCartwSSearch(ByVal partNumber As String, ByVal partQuantity As String) As AddToCartResult
            Dim objPCILogger As New PCILogger()
            'Dim objUtilties As Utilities = New Utilities
            Dim message As String = ""
            'Dim sPartNumberUpperCase As String = partNumber.ToUpper()
            Dim searchResult As New AddToCartResult
            Dim customer As New Customer
            'Dim lblMessage As New Label
            Dim objSSL As New SSL
            Dim carts As ShoppingCart()
            Dim cartManager As New ShoppingCartManager
            'Dim courseManager As CourseManager = Nothing
            Dim cart As ShoppingCart
            Dim output1 As String = String.Empty
            Dim bProperpart As Boolean = True
            Dim addedProduct As Product

            Try
                customer = HttpContextManager.Customer
                If customer Is Nothing Then
                    searchResult.success = -1
                    searchResult.message = "Must be logged in, or an error occured loading user data."
                    Return searchResult
                End If

                If HttpContextManager.GlobalData.IsCanada Then
                    If Not customer.UserType = "A" Then
                        searchResult.success = -1
                        searchResult.message = Resources.Resource.el_ATC_PendingCust
                        Return searchResult
                    End If
                End If

                ' If no Quantity was provided, assume a default value of one
                If String.IsNullOrEmpty(partQuantity) Then partQuantity = "1"
                partNumber = partNumber.ToUpper()
                carts = objSSL.getAllShoppingCarts(customer, New Label)
                cart = cartManager.AddProductToCartQuickCart(carts, partNumber, partQuantity, True, output1, HttpContextManager.GlobalData)
                HttpContext.Current.Session("carts") = cart
                ' Start setting up the return object values
                searchResult.success = 0
                searchResult.PartNumber = partNumber
                searchResult.Quantity = partQuantity
                searchResult.items = cart.ShoppingCartItems.Length
                searchResult.totalAmount = String.Format("{0:c}", cart.ShoppingCartTotalPrice)
                searchResult.differentCart = (Not objSSL.IsCurrentCartSameType(cart))

                addedProduct = cart.ShoppingCartItems(cart.ShoppingCartItems.Length - 1).Product

                'message &= String.Format("addedProduct.PartNumber = {0}, .ReplacementPartNumber = {1}, partNumber = {2}<br/>", addedProduct.PartNumber, addedProduct.ReplacementPartNumber, partNumber) & vbCrLf
                If (Not String.IsNullOrEmpty(addedProduct.ReplacementPartNumber)) AndAlso (addedProduct.ReplacementPartNumber.ToUpper() <> partNumber) Then
                    ' If ReplacementPartNumber is set, and it's not equal to the part we added, then the requested part was replaced.
                    searchResult.success = 2
                    message &= String.Format(Resources.Resource.atc_ReplacementAdded, addedProduct.ReplacementPartNumber, partNumber) & "<br/>" & Environment.NewLine
                ElseIf (Not String.IsNullOrEmpty(addedProduct.PartNumber)) AndAlso (addedProduct.PartNumber.ToUpper() = partNumber) Then
                    ' If the PartNumbers match and there is no ReplacementPartNumber, they got the part they requested.
                    searchResult.success = 1
                    message &= $"{addedProduct.ReplacementPartNumber} {Resources.Resource.atc_AddedToCart}<br/>{Environment.NewLine}"
                Else
                    message &= "Else condition triggered while checking for Replacement Part ... <br/>" & Environment.NewLine
                    message &= $"addedProduct.PartNumber = {addedProduct.PartNumber}, addedProduct.ReplacementPartNumber = {addedProduct.ReplacementPartNumber}, partNumber = {partNumber}<br/>{Environment.NewLine}"
                End If
            Catch ex As Exception
                objPCILogger.EventOriginApplication = "ServicesPLUS"
                objPCILogger.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
                objPCILogger.OperationalUser = Environment.UserName
                objPCILogger.EventDateTime = DateTime.Now.ToLongTimeString()
                objPCILogger.HTTPRequestObjectValues = objPCILogger.GetRequestContextValue()
                objPCILogger.CustomerID = customer.CustomerID
                objPCILogger.CustomerID_SequenceNumber = customer.SequenceNumber
                objPCILogger.EmailAddress = customer.EmailAddress
                objPCILogger.SIAM_ID = customer.SIAMIdentity
                objPCILogger.LDAP_ID = customer.LdapID
                objPCILogger.EventOriginMethod = "addPartToCartwSSearch"
                objPCILogger.EventType = EventType.View
                objPCILogger.IndicationSuccessFailure = "Failure"
                objPCILogger.Message = "addPartToCart Failed. " & ex.Message.ToString()
                objPCILogger.PushLogToMSMQ()
                message &= Utilities.WrapExceptionforUI(ex)
            Finally
                searchResult.message = message
            End Try

            Return searchResult
        End Function

#Region "     PROPERTIES"
        Private _selectedModel As Model
        Private _selectedOption As Model
        Private _selectedCategory As Category
        Private _selectedModule As _Module
        Private _moduleList As _Module()

        Public Property SelectedModel() As Model
            Get
                If (_selectedModel Is Nothing) And (Session("selectedModel") IsNot Nothing) Then _selectedModel = Session("selectedModel")
                Return _selectedModel
            End Get
            Set(ByVal value As Model)
                _selectedModel = value
                Session("selectedModel") = _selectedModel
            End Set
        End Property

        ''' <summary>
        ''' The current SubModel Option chosen in the DropDown List. If none exist, or none are selected,
        ''' it defaults to SelectedModel.
        ''' </summary>
        ''' <returns>
        ''' A Model object of the currently-selected SubModel, or the parent Model.
        ''' </returns>
        Public Property SelectedOption() As Model
            Get
                If (_selectedOption Is Nothing) Then
                    If (Session("selectedOption") IsNot Nothing) Then
                        _selectedOption = Session("selectedOption")
                    ElseIf (_selectedModel IsNot Nothing) Then
                        _selectedOption = _selectedModel
                    End If
                End If
                Return _selectedOption
            End Get
            Set(ByVal value As Model)
                _selectedOption = value
                Session("selectedOption") = _selectedOption
            End Set
        End Property

        Public Property SelectedCategory() As Category
            Get
                If (_selectedCategory Is Nothing) And (Session("selectedCategory") IsNot Nothing) Then _selectedCategory = Session("selectedCategory")
                Return _selectedCategory
            End Get
            Set(ByVal value As Category)
                _selectedCategory = value
                Session("selectedCategory") = _selectedCategory
            End Set
        End Property

        Public Property SelectedModule() As _Module
            Get
                If (_selectedModule Is Nothing) And (Session("selectedModule") IsNot Nothing) Then _selectedModule = Session("selectedModule")
                Return _selectedModule
            End Get
            Set(ByVal value As _Module)
                _selectedModule = value
                Session("selectedModule") = _selectedModule
            End Set
        End Property

        Public Property ModuleList() As _Module()
            Get
                If (_moduleList Is Nothing) And (Session("moduleList") IsNot Nothing) Then _moduleList = Session("moduleList")
                Return _moduleList
            End Get
            Set(ByVal value As _Module())
                _moduleList = value
                Session("moduleList") = _moduleList
            End Set
        End Property

#End Region

#Region "   OLD TABLE-BUILDING CODE from ShowPartsResults   "
        'Dim TR As New TableRow
        'Dim TD As New TableCell

        'TD.Controls.Add(New LiteralControl("<TR>"))
        ''TD.Controls.Add(New LiteralControl("<td bgcolor=""D5DEE9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""28""></td>"))
        ''TD.Controls.Add(New LiteralControl("<td bgcolor=""D5DEE9"" width=""80"" class=""tableHeader"" height=""28"">&nbsp;&nbsp;Model</td>"))
        'TD.Controls.Add(New LiteralControl("<td bgcolor=""D5DEE9"" width=""1"" height=""28""><img src=""images/spacer.gif"" width=""1"" height=""28""></td>"))
        'TD.Controls.Add(New LiteralControl("<td bgcolor=""D5DEE9"" class=""tableHeader"" height=""28"">" + Resources.Resource.el_RefNo + "</td>"))
        'TD.Controls.Add(New LiteralControl("<td bgcolor=""D5DEE9"" width=""1"" height=""28""><img src=""images/spacer.gif"" width=""1"" height=""28""></td>"))
        'TD.Controls.Add(New LiteralControl("<td bgcolor=""D5DEE9"" class=""tableHeader"" height=""28"">" + Resources.Resource.el_PartsRequesterd + "</td>"))
        'TD.Controls.Add(New LiteralControl("<td bgcolor=""D5DEE9"" width=""1"" height=""28""><img src=""images/spacer.gif"" width=""1"" height=""28""></td>"))
        'TD.Controls.Add(New LiteralControl("<td bgcolor=""D5DEE9"" class=""tableHeader"" height=""28"">" + Resources.Resource.el_PartsSupplied + "</td>"))
        'TD.Controls.Add(New LiteralControl("<td bgcolor=""D5DEE9"" width=""1"" height=""28""><img src=""images/spacer.gif"" width=""1"" height=""28""></td>"))
        'TD.Controls.Add(New LiteralControl("<td bgcolor=""D5DEE9"" class=""tableHeader"" height=""28"">" + Resources.Resource.el_Description + "</td>"))
        'TD.Controls.Add(New LiteralControl("<td bgcolor=""D5DEE9"" width=""1"" height=""28""><img src=""images/spacer.gif"" width=""1"" height=""28""></td>"))
        'TD.Controls.Add(New LiteralControl("<td bgcolor=""D5DEE9"" width=""120"" class=""tableHeader"" height=""28"">" + Resources.Resource.el_PriceList + "</td>"))
        'TD.Controls.Add(New LiteralControl("<td bgcolor=""D5DEE9"" width=""1"" height=""28""><img src=""images/spacer.gif"" width=""1"" height=""28""></td>"))
        'TD.Controls.Add(New LiteralControl("<td bgcolor=""D5DEE9"" width=""26"" class=""tableHeader"" align=""right"" height=""28"">" + Resources.Resource.el_CartAdd + "</td>"))
        'TD.Controls.Add(New LiteralControl("<td bgcolor=""D5DEE9"" width=""1"" height=""28""><img src=""images/spacer.gif"" width=""1"" height=""28""></td>"))
        'TD.Controls.Add(New LiteralControl("<td bgcolor=""D5DEE9"" width=""1"" class=""tableHeader"" align=""right"" height=""28"">" + Resources.Resource.el_Cat + "</td>"))
        'TD.Controls.Add(New LiteralControl("<td bgcolor=""D5DEE9"" width=""1"" class=""tableHeader"" align=""right"" height=""28""><img src=""images/spacer.gif"" width=""1"" height=""28""></td>"))
        'TD.Controls.Add(New LiteralControl("<td bgcolor=""D5DEE9"" width=""26"" class=""tableHeader"" align=""right"" height=""28"">" + Resources.Resource.el_ExplView + "</td>"))
        'TD.Controls.Add(New LiteralControl("<td bgcolor=""D5DEE9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""28""></td>"))
        'TD.Controls.Add(New LiteralControl("</TR>"))
        'TD.Controls.Add(New LiteralControl("<TR>"))
        ''TD.Controls.Add(New LiteralControl("<td bgcolor=""D5DEE9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""1""></td>"))
        ''TD.Controls.Add(New LiteralControl("<td bgcolor=""D5DEE9"" width=""80""><img src=""images/spacer.gif"" width=""1"" height=""1""></td>"))
        'TD.Controls.Add(New LiteralControl("<td bgcolor=""D5DEE9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""1""></td>"))
        'TD.Controls.Add(New LiteralControl("<td bgcolor=""D5DEE9""><img src=""images/spacer.gif"" width=""1"" height=""1""></td>"))
        'TD.Controls.Add(New LiteralControl(" <td bgcolor=""D5DEE9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""1""></td>"))
        'TD.Controls.Add(New LiteralControl("<td bgcolor=""D5DEE9""><img src=""images/spacer.gif"" width=""1"" height=""1""></td>"))
        'TD.Controls.Add(New LiteralControl("<td bgcolor=""D5DEE9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""1""></td>"))
        'TD.Controls.Add(New LiteralControl("<td bgcolor=""D5DEE9""><img src=""images/spacer.gif"" width=""1"" height=""1""></td>"))
        'TD.Controls.Add(New LiteralControl("<td bgcolor=""D5DEE9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""1""></td>"))
        'TD.Controls.Add(New LiteralControl("<td bgcolor=""D5DEE9""><img src=""images/spacer.gif"" width=""1"" height=""1""></td>"))
        'TD.Controls.Add(New LiteralControl("<td bgcolor=""D5DEE9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""1""></td>"))
        'TD.Controls.Add(New LiteralControl("<td bgcolor=""D5DEE9"" width=""120""><img src=""images/spacer.gif"" width=""1"" height=""1""></td>"))
        'TD.Controls.Add(New LiteralControl("<td bgcolor=""D5DEE9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""1""></td>"))
        'TD.Controls.Add(New LiteralControl("<td bgcolor=""D5DEE9"" width=""26""><img src=""images/spacer.gif"" width=""1"" height=""1""></td>"))
        'TD.Controls.Add(New LiteralControl("<td bgcolor=""D5DEE9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""1""></td>"))
        'TD.Controls.Add(New LiteralControl("<td bgcolor=""D5DEE9"" width=""26""><img src=""images/spacer.gif"" width=""1"" height=""1""></td>"))
        'TD.Controls.Add(New LiteralControl("<td bgcolor=""D5DEE9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""1""></td>"))
        'TD.Controls.Add(New LiteralControl("<td bgcolor=""D5DEE9"" width=""26""><img src=""images/spacer.gif"" width=""1"" height=""1""></td>"))
        'TD.Controls.Add(New LiteralControl("<td bgcolor=""D5DEE9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""1""></td>"))
        'TD.Controls.Add(New LiteralControl("</TR>"))

        'If parts.Length > 0 Then
        '	Dim p As Sony.US.ServicesPLUS.Core.Part
        '	Dim TotalPages As Integer

        '	If parts.Length > 50 Then
        '		TotalPages = Int(parts.Length / Total) + IIf(0 < (parts.Length Mod Total), 1, 0)
        '	Else
        '		TotalPages = 1
        '		PageNum = 1
        '	End If

        '	Dim StartPageNum As Integer = (PageNumber - 1) * Total
        '	Dim EndPageNum As Integer = (PageNumber * Total) - 1

        '	If (0 > StartPageNum) Then
        '		StartPageNum = 0
        '	End If

        '	If EndPageNum >= parts.Length Then
        '		EndPageNum = parts.Length - 1
        '	End If

        '	Dim PrevPage As Integer = PageNumber - 1
        '	Dim NextPage As Integer = PageNumber + 1

        '	Dim n As Integer
        '	Dim RowColor As String
        '	Dim isValidPart As Boolean
        '	Dim hasPromo As Boolean = False
        '	Me.tdLegend.Visible = False

        '	Dim promoManager As PromotionManager = New PromotionManager

        '	For n = StartPageNum To EndPageNum
        '		p = parts(n)
        '		isValidPart = True
        '		If p.ListPrice = 0 Then
        '			isValidPart = False
        '		End If
        '		RowColor = IIf((n Mod 2 = 0), "FFFFFF", "F2F5F8")

        '		Dim strPriceStar As String = ""

        '		If p.ReplacementPartNumber IsNot Nothing Then
        '			If promoManager.IsPromotionPart(p.ReplacementPartNumber) Then
        '				strPriceStar += "<span face=""wingdings"" color=""red"" size=""3px"">v</span>"
        '				hasPromo = True
        '			End If
        '		Else
        '			If promoManager.IsPromotionPart(p.PartNumber) Then
        '				strPriceStar += "<span face=""wingdings"" color=""red"" size=""3px"">v</span>"
        '				hasPromo = True
        '			End If
        '		End If

        '		TD.Controls.Add(New LiteralControl("<TR>"))
        '		'TD.Controls.Add(New LiteralControl("<td bgcolor=""D5DEE9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20""></td>"))
        '		'TD.Controls.Add(New LiteralControl("<td  bgcolor=" + RowColor + " width=""80"" class=""tableHeader"">&nbsp;&nbsp;" + p.Model + "</td>"))
        '		TD.Controls.Add(New LiteralControl("<td bgcolor=""D5DEE9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""1""></td>"))
        '		TD.Controls.Add(New LiteralControl("<td bgcolor=" + RowColor + " class=""tableData"">&nbsp;&nbsp;" + p.PartReferenceID + "</td>"))
        '		TD.Controls.Add(New LiteralControl("<td bgcolor=""D5DEE9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""1""></td>"))
        '		TD.Controls.Add(New LiteralControl("<td bgcolor=" + RowColor + " class=""tableData"">&nbsp;&nbsp;" + p.PartNumber + "</td>"))
        '		TD.Controls.Add(New LiteralControl("<td bgcolor=""D5DEE9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""1""></td>"))
        '		TD.Controls.Add(New LiteralControl("<td bgcolor=" + RowColor + " class=""tableData"">&nbsp;&nbsp;" + p.ReplacementPartNumber + "</td>"))
        '		TD.Controls.Add(New LiteralControl("<td bgcolor=""D5DEE9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""1""></td>"))
        '		TD.Controls.Add(New LiteralControl("<td bgcolor=" + RowColor + " class=""tableData"">&nbsp;&nbsp;" + p.Description + "&nbsp;</td>"))
        '		TD.Controls.Add(New LiteralControl("<td bgcolor=""D5DEE9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""1""></td>"))

        '		' AVAILABILITY
        '		If isValidPart Then
        '			If customer Is Nothing Then
        '				TD.Controls.Add(New LiteralControl("<td bgcolor=" + RowColor + " width=""120"" class=""tableData"">&nbsp;&nbsp;"))
        '				'TD.Controls.Add(New LiteralControl((String.Format("{0:c}", p.ListPrice)).ToString() + "</td>"))
        '				'added new link for B2C and other non logged in users
        '				'for E343 - Deepa V, Apr 28, 2006
        '				TD.Controls.Add(New LiteralControl((String.Format("{0:c}", p.ListPrice)).ToString() + strPriceStar + "&nbsp;&nbsp;&nbsp;<span class=""tableHeader""><a class=Body href=""#"" onclick=""mywin=window.open('Availability.aspx?ArrayVal=" + n.ToString() + "&isKit=0', '','" + CON_POPUP_FEATURES + "');"">" + Resources.Resource.el_Availability + "</a></span></td>"))
        '			Else

        '				If (customer.SAPBillToAccounts.Length > 0 Or customer.SISLegacyBillToAccounts.Length > 0) Then
        '					TD.Controls.Add(New LiteralControl("<td bgcolor=" + RowColor + " width=""120"" class=""tableData"">&nbsp;&nbsp;"))
        '					TD.Controls.Add(New LiteralControl((String.Format("{0:c}", p.ListPrice)).ToString() + strPriceStar + "&nbsp;&nbsp;&nbsp;<span class=""tableHeader""><a class=Body href=""#"" onclick=""mywin=window.open('yourprice.aspx?ArrayVal=" + n.ToString() + "&isKit=0', '', '" + CON_POPUP_FEATURES + "');"">" + Resources.Resource.el_YourPrice + "</a></span></td>"))
        '				Else 'Don't show Your Price link
        '					TD.Controls.Add(New LiteralControl("<td bgcolor=" + RowColor + " width=""120"" class=""tableData"">&nbsp;&nbsp;"))
        '					'added new link for B2C and other non logged in users
        '					'for E343 - Deepa V, Apr 28, 2006
        '					TD.Controls.Add(New LiteralControl((String.Format("{0:c}", p.ListPrice)).ToString() + strPriceStar + "&nbsp;&nbsp;&nbsp;<span class=""tableHeader""><a class=Body href=""#"" onclick=""mywin=window.open('Availability.aspx?ArrayVal=" + n.ToString() + "&isKit=0', '', '" + CON_POPUP_FEATURES + "');"">" + Resources.Resource.el_Availability + "</a></span></td>"))
        '				End If
        '			End If
        '		Else
        '			TD.Controls.Add(New LiteralControl("<td bgcolor=" + RowColor + " class=""legalCopy"">Call 1-800-538-7550 for information.</td>"))
        '		End If

        '		TD.Controls.Add(New LiteralControl("<td bgcolor=""D5DEE9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""1""></td>"))

        '		' ADD TO CART
        '		'Add To Cart Image Button
        '		Dim ib As New ImageButton
        '		ib.ImageUrl = "images/sp_int_add2Cart_btn.gif"
        '		ib.ID = "AddToCart" + n.ToString()
        'ib.CommandArgument = n.ToString()
        'ib.AlternateText = "Add to Cart"
        'hidespstextqty.Text = 1
        'ib.Attributes.Add("onclick", "return AddToCart_onclick('" + p.ReplacementPartNumber + "')")

        'If isValidPart Then
        '	If customer Is Nothing Then
        '		TD.Controls.Add(New LiteralControl("<td bgcolor=" + RowColor + " width=""26"" class=""tableData"" align=""center""><a href=""#"" onclick=""window.location.href='NotLoggedInAddToCart.aspx?post=pps&ArrayVal=" + Request.QueryString("ArrayVal") + "&PartLineNo=" + n.ToString() + "'; return false;""><img src=""images/sp_int_add2Cart_btn.gif"" width=""29"" height=""20"" border=""0"" alt=""Add to Cart""></a></td>"))
        '	Else
        '		TD.Controls.Add(New LiteralControl("<td bgcolor=" + RowColor + " width=""26"" class=""tableData"" align=""center"">"))
        '		TD.Controls.Add(ib)
        '		TD.Controls.Add(New LiteralControl("</td>"))
        '	End If
        'Else
        '	TD.Controls.Add(New LiteralControl("<td bgcolor=" + RowColor + " width=""1""><img src=""images/spacer.gif"" width=""1"" height=""1""></td>"))
        'End If

        '		TD.Controls.Add(New LiteralControl("<td bgcolor=""D5DEE9"" width=""1"" class=""tableData"" align=""right""><img src=""images/spacer.gif"" width=""1"" height=""1""></td>"))

        '		' ADD TO CATALOG
        '		'Add To MyCatalog Image Button
        '		Dim ib2 As New ImageButton
        '		ib2.ImageUrl = "images/sp_int_add2myCat_btn.gif"
        '		ib2.ID = "AddToMyCatalog" + n.ToString()
        '		ib2.CommandArgument = n.ToString()
        '		ib2.AlternateText = Resources.Resource.el_AddToMyCatalog '"Add to My Catalog"
        '		ib2.Attributes.Add("onclick", "var theform;if (window.navigator.appName.toLowerCase().indexOf('netscape') > -1) {theform = document.forms['Form1'];}else {theform = document.Form1;}theform.__EVENTTARGET.value = 'AddToMyCatalog';theform.__EVENTARGUMENT.value = '" + n.ToString() + "'")

        '		If isValidPart Then
        '			If customer Is Nothing Then
        '				TD.Controls.Add(New LiteralControl("<td bgcolor=" + RowColor + " width=""26"" class=""tableData"" align=""center""><a href=""#"" onclick=""window.location.href='NotLoggedInAddToCatalog.aspx?post=pps&ArrayVal=" + Request.QueryString("ArrayVal") + "&PartLineNo=" + n.ToString() + "'; return false;""><img src=""images/sp_int_add2myCat_btn.gif"" width=""29"" height=""20"" border=""0"" alt=""Add to My Catalog""></a></td>"))
        '			Else
        '				TD.Controls.Add(New LiteralControl("<td bgcolor=" + RowColor + " width=""26"" class=""tableData"" align=""center"">"))
        '				TD.Controls.Add(ib2)
        '				TD.Controls.Add(New LiteralControl("</td>"))
        '			End If
        '		Else
        '			TD.Controls.Add(New LiteralControl("<td bgcolor=" + RowColor + " width=""1""><img src=""images/spacer.gif"" width=""1"" height=""1""></td>"))
        '		End If

        '		TD.Controls.Add(New LiteralControl("<td bgcolor=""D5DEE9"" width=""1"" class=""tableData"" align=""right""><img src=""images/spacer.gif"" width=""1"" height=""1""></td>"))
        '		' EXPLODED VIEW
        '		If p.HasExplodedView Then
        '			'Dim baseurl As String
        '			''2016-05-11 ASleight Modifying for US/CA site root
        '			'If HttpContextManager.GlobalData.SalesOrganization = ConfigurationData.GeneralSettings.GlobalData.SalesOrganization.CA Then
        '			'	baseurl = Sony.US.SIAMUtilities.ConfigurationData.Environment.URL.CA
        '			'Else
        '			'	baseurl = Sony.US.SIAMUtilities.ConfigurationData.Environment.URL.US
        '			'End If

        '			Dim URLView As String = "View2.aspx?ArrayVal=" + n.ToString()
        '			TD.Controls.Add(New LiteralControl("<td bgcolor=" + RowColor + " width=""26"" class=""tableData"" align=""center""><A target=_blank  href=" + URLView + "><img src=""images/sp_int_expandView_btn.gif"" width=""29"" height=""20"" border=""0"" alt=""Exploded View""></A></td>"))
        '		Else
        '			TD.Controls.Add(New LiteralControl("<td bgcolor=" + RowColor + " width=""26"" class=""tableData"" align=""center""><img src=""images/spacer.gif"" width=""29"" height=""20""></td>"))
        '		End If
        '		TD.Controls.Add(New LiteralControl("<td bgcolor=""D5DEE9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20""></td>"))
        '		TD.Controls.Add(New LiteralControl("</TR>"))
        '	Next

        '	' -- display legend only if there is at least one part with promotion
        '	If hasPromo = True Then
        '		Me.tdLegend.Visible = True
        '	End If

        '	'Set values for pagination
        '	btnNext.CommandArgument = NextPage.ToString()
        '	btnRightArrow.CommandArgument = NextPage.ToString()
        '	btnPrevious.CommandArgument = PrevPage.ToString
        '	btnLeftArrow.CommandArgument = PrevPage.ToString

        '	btnNext2.CommandArgument = NextPage.ToString()
        '	btnRightArrow2.CommandArgument = NextPage.ToString()
        '	btnPrevious2.CommandArgument = PrevPage.ToString
        '	btnLeftArrow2.CommandArgument = PrevPage.ToString

        '	'Create text for paginate label
        '	Dim sb_pageLabel As New StringBuilder

        '	If StartPageNum = 0 Then 'On the first page - no show previous as a link 
        '		sb_pageLabel.Append("<span class=""tableData"">1&nbsp;of</span>")
        '		btnLeftArrow.Visible = False
        '		btnPrevious.Visible = False
        '		lblCurrentPage.Visible = False
        '		Session.Add("CurrentPageNum", 1)
        '	Else
        '		btnLeftArrow.Visible = True
        '		btnPrevious.Visible = True
        '		lblCurrentPage.Visible = True
        '		lblCurrentPage.Text = PageNum.ToString + "&nbsp;of&nbsp;"
        '		Session.Add("CurrentPageNum", PageNum)
        '	End If

        '	If EndPageNum = (parts.Length - 1) Then	'On the last page - no show next as a link
        '		'sb_pageLabel.Append("<span class=""tableData"">of &nbsp;" + TotalPages.ToString() + "&nbsp;&nbsp;&nbsp;&nbsp;Next&nbsp;<img src=""images/sp_int_rightArrow_btn.gif"" border=""0""><img src=""images/spacer.gif"" border=""0"" width=""22""></span>")
        '		lblEndingPage.Text = TotalPages.ToString()
        '		lblEndingPage.Visible = True
        '	Else
        '		btnRightArrow.Visible = True
        '		btnNext.Visible = True
        '		lblEndingPage.Visible = True
        '		btnNext.Text = "Next"
        '		lblEndingPage.Text = TotalPages.ToString()
        '	End If

        '	If TotalPages > 1 Then
        '		PageLabel.Text = sb_pageLabel.ToString()
        '		lblPage.Visible = True
        '		txtPageNumber.Visible = True
        '		btnGoToPage.Visible = True
        '	End If

        '	If StartPageNum = 0 Then 'On the first page - no show previous as a link 
        '		'sb_pageLabel.Append("<span class=""tableData"">1&nbsp;of</span>")
        '		btnLeftArrow2.Visible = False
        '		btnPrevious2.Visible = False
        '		lblCurrentPage2.Visible = False
        '		Session.Add("CurrentPageNum", 1)
        '	Else
        '		btnLeftArrow2.Visible = True
        '		btnPrevious2.Visible = True
        '		lblCurrentPage2.Visible = True
        '		lblCurrentPage2.Text = PageNum.ToString + "&nbsp;of&nbsp;"
        '		Session.Add("CurrentPageNum", PageNum)
        '	End If

        '	If EndPageNum = (parts.Length - 1) Then	'On the last page - no show next as a link
        '		'sb_pageLabel.Append("<span class=""tableData"">of &nbsp;" + TotalPages.ToString() + "&nbsp;&nbsp;&nbsp;&nbsp;Next&nbsp;<img src=""images/sp_int_rightArrow_btn.gif"" border=""0""><img src=""images/spacer.gif"" border=""0"" width=""22""></span>")
        '		lblEndingPage2.Visible = True
        '		lblEndingPage2.Text = TotalPages.ToString()
        '	Else
        '		btnRightArrow2.Visible = True
        '		btnNext2.Visible = True
        '		lblEndingPage2.Visible = True
        '		btnNext2.Text = "Next"
        '		lblEndingPage2.Text = TotalPages.ToString()
        '	End If

        '	If TotalPages > 1 Then
        '		PageLabel2.Text = sb_pageLabel.ToString()
        '		lblPage2.Visible = True
        '		txtPageNumber2.Visible = True
        '		btnGoToPage2.Visible = True

        '		'added by Deepa V feb 15,2006 to show the button when the no. of pages is > 1 as this button will be hidden on page load
        '		btnGoToPage.Visible = True
        '	Else 'added by Deepa V feb 15,2006 to hide the go buttons when the number of pages is <= 1
        '		btnGoToPage.Visible = False
        '		btnGoToPage2.Visible = False
        '	End If

        '	If parts.Length > 15 Then
        '		BackToTopLabel.Text = ("&nbsp;&nbsp;&nbsp;&nbsp;<a href=""#top""><img src=""images/sp_int_back2top_btn.gif"" width=""78"" height=""28"" border=""0""></a>")
        '	End If
        'Else
        'TD.Controls.Add(New LiteralControl("<TR>"))
        'TD.Controls.Add(New LiteralControl("<td bgcolor=""D5DEE9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""28""></td>"))
        'TD.Controls.Add(New LiteralControl("<td bgcolor=""FFFFFF"" Colspan = ""17"" class=""tableHeader"" height=""28"" Width=""430""><center>" + Resources.Resource.el_Search_NoResult + "</center></td>"))
        'TD.Controls.Add(New LiteralControl("<td bgcolor=""D5DEE9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""28""></td>"))
        'TD.Controls.Add(New LiteralControl("</TR>"))
        'TD.Controls.Add(New LiteralControl("<TR>"))
        'TD.Controls.Add(New LiteralControl("<td bgcolor=""D5DEE9"" width=""1"" Colspan = ""19""><img src=""images/spacer.gif"" width=""1"" height=""1""></td>"))
        'TD.Controls.Add(New LiteralControl("</TR>"))
        'End If

        'TR.Cells.Add(TD)
        'DisplayResultsTable.Rows.Add(TR)
#End Region

    End Class

    Public Class PartresultSearch
        Public _message As String
        Public _totalAmount As String
        Public _items As String
        Public _success As Integer
        Public _quantity As String
        Public _partnumber As String
        Public _differentCart As Boolean

        Property partnumber() As String
            Get
                Return _partnumber
            End Get
            Set(ByVal value As String)
                _partnumber = value
            End Set
        End Property
        Property message() As String
            Get
                Return _message
            End Get
            Set(ByVal value As String)
                _message = value
            End Set
        End Property
        Property Quantity() As String
            Get
                Return _quantity
            End Get
            Set(ByVal value As String)
                _quantity = value
            End Set
        End Property
        ''' <summary>
        ''' 0-Failure
        ''' 1-Success
        ''' 2-warning
        ''' </summary>
        Property success() As Integer
            Get
                Return _success
            End Get
            Set(ByVal value As Integer)
                _success = value
            End Set
        End Property
        Property totalAmount() As String
            Get
                Return _totalAmount
            End Get
            Set(ByVal value As String)
                _totalAmount = value
            End Set
        End Property
        Property items() As String
            Get
                Return _items
            End Get
            Set(ByVal value As String)
                _items = value
            End Set
        End Property

        Property differentCart() As Boolean
            Get
                Return _differentCart
            End Get
            Set(ByVal value As Boolean)
                _differentCart = value
            End Set
        End Property

    End Class

End Namespace
