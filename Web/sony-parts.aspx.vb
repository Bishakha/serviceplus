Imports ServicesPlusException
Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Process



Namespace ServicePLUSWebApp
    Partial Class sony_parts
        Inherits SSL
        Public sb As New StringBuilder
        'Dim objUtilties As Utilities = New Utilities

        Public LanguageSM As String = "en_US"
        'Dim objGlobalData As New GlobalData
        Public divspShow As Boolean

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Protected WithEvents ModelDescription As System.Web.UI.WebControls.Label


        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()

        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                lblNotFound.Text = ""
                Search1ErrorLabel.Text = ""
                Search2ErrorLabel.Text = ""

                divspShow = HttpContextManager.GlobalData.IsAmerica
                LanguageSM = HttpContextManager.GlobalData.LanguageForIDP

                'Grab customer from session if available
                If HttpContextManager.Customer IsNot Nothing Then
                    Response.Redirect("au-sony-parts.aspx", False)
                End If

                'Controls focus of textboxes
                formFieldInitialization()

                If Not Page.IsPostBack() Then
                    'Kill PartsFinder Sessions - category, modules, _module sessions...
                    Session.Remove("selectedOption")
                    Session.Remove("selectedCategory")
                    Session.Remove("selectedModule")
                    Session.Remove("selectedModel")
                    Session.Remove("refineSearchText")
                    Session.Remove("refineSearchType")
                    Session.Remove("partList")
                    Session.Remove("moduleList")
                    Session.Remove("models")
                End If
            Catch ex As Exception
                lblError.Text = Utilities.WrapExceptionforUI(ex)
                lblError.Visible = True
            End Try
        End Sub

        Private Sub btnSearchModel_Click(ByVal sender As System.Object, ByVal e As ImageClickEventArgs) Handles btnSearchModel.Click
            Try
                Dim lstConficts As List(Of String) = Nothing
                Dim strModels As String = String.Empty
                Dim catMan As New CatalogManager
                Dim models() As Model

                Search1ErrorLabel.Text = ""
                Session.Remove("SearchModel")
                Session.Remove("SearchParts")

                If txtModelNumber.Text = "" Then
                    Search1ErrorLabel.Text = Resources.Resource.el_Pts_Valid_ModelNo ' "You must enter a model number."
                Else
                    Session.Add("SearchModel", txtModelNumber.Text)
                    models = catMan.PartSearch(txtModelNumber.Text.Trim(), HttpContextManager.GlobalData, lstConficts)
                    If models.Length = 0 Then
                        Search1ErrorLabel.Text = Resources.Resource.el_Pts_NoModel '  "No models found."
                    Else
                        'Added by Snehalatha Ramamurthy on 17th Oct 2013
                        'Current logic: To call the modal popup that shows old model names and 
                        'its new modal names if there are conflicts else take the user to PartsPLUSResults.aspx
                        Session.Add("models", models)
                        If lstConficts.Count > 0 Then
                            'For Each item In lstConficts
                            '    strModels = strModels + item + ","
                            'Next
                            'If Not String.IsNullOrEmpty(strModels) Then strModels = strModels.Remove(strModels.Length - 1, 1)
                            strModels = String.Join(",", lstConficts)   ' 2018-08-21 Replaces the above for loop.
                            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "showPopup", $"javascript:OrderLookUp({lstConficts.Count},'{strModels}','0');", True)
                        Else
                            Response.Redirect("PartsPLUSResults.aspx?stype=models")
                        End If
                    End If
                End If
            Catch ex As Exception
                Search1ErrorLabel.Visible = True
                Search1ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Sub btnSearchPart_Click(ByVal sender As System.Object, ByVal e As ImageClickEventArgs) Handles btnSearchPart.Click
            Try
                Dim catm As New CatalogManager
                Dim parts() As Sony.US.ServicesPLUS.Core.Part
                Dim kits() As Sony.US.ServicesPLUS.Core.Kit
                Dim sMaximumvalue As String = String.Empty

                Search2ErrorLabel.Text = ""
                Session.Remove("SearchModel")
                Session.Remove("SearchParts")

                If (txtPrefix.Text = "" And txtPartNumber.Text = "" And txtDescription.Text = "") Then
                    Search2ErrorLabel.Text = Resources.Resource.el_pts_valid_srch ' "You must enter at least one search criteria."
                Else
                    'kill kits session if there is one so that shopping cart focus will be on this search
                    Session.Remove("kits")
                    parts = catm.QuickSearch(txtPrefix.Text.Trim(), txtPartNumber.Text.Trim(), txtDescription.Text.Trim(), sMaximumvalue, HttpContextManager.GlobalData)
                    kits = catm.KitSearch(txtPrefix.Text.Trim(), txtPartNumber.Text.Trim(), txtDescription.Text.Trim()) 'also search for kit now
                    Session("SearchParts") = txtPrefix.Text.Trim() + "�" + txtPartNumber.Text.Trim() + "�" + txtDescription.Text.Trim()

                    If parts.Length = 0 And Not String.IsNullOrEmpty(sMaximumvalue) Then
                        Search2ErrorLabel.Text = sMaximumvalue
                        Return
                    ElseIf parts.Length = 0 And kits.Length = 0 Then
                        Search2ErrorLabel.Text = (Resources.Resource.el_pts_noMatch) ' "No matches found.")
                        Return
                    Else
                        Session("partList") = parts
                        Session("kits") = kits
                        Response.Redirect("PartsPlusResults.aspx?stype=parts", False)
                    End If
                End If
            Catch ex As Exception
                Utilities.LogMessages($"sony-parts.btnSearchPart_Click - ERROR - Prefix: {txtPrefix.Text}, PartNumber: {txtPartNumber.Text}, Description: {txtDescription.Text}")
                Search2ErrorLabel.Visible = True
                Search2ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Sub formFieldInitialization()
            txtPrefix.Attributes("onkeydown") = "SetTheFocusButton(event, 'btnSearchPart')"
            txtPartNumber.Attributes("onkeydown") = "SetTheFocusButton(event, 'btnSearchPart')"
            txtDescription.Attributes("onkeydown") = "SetTheFocusButton(event, 'btnSearchPart')"
            txtModelNumber.Attributes("onkeydown") = "SetTheFocusButton(event, 'btnSearchModel')"
        End Sub

    End Class

End Namespace
