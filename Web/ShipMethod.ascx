<%@ Control Language="vb" AutoEventWireup="false" Inherits="ServicePLUSWebApp.ShipMethod" CodeFile="ShipMethod.ascx.vb" %>
<link href="includes/ServicesPLUS_style.css" type="text/css" rel="stylesheet">
<script type="text/javascript" src="includes/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="includes/svcplus_globalization.js"></script>
<table id="Table12" width="350" border="0">
    <tr bgcolor="#b7b7b7">
        <td>
            <img height="1" src="images/spacer.gif" width="1" alt=""></td>
        <td>
            <img height="1" src="images/spacer.gif" width="1" alt=""></td>
        <td>
            <img height="1" src="images/spacer.gif" width="1" alt=""></td>
        <td>
            <img height="1" src="images/spacer.gif" width="1" alt=""></td>
        <td>
            <img height="1" src="images/spacer.gif" width="1" alt=""></td>
        <td>
            <img height="1" src="images/spacer.gif" width="1" alt=""></td>
    </tr>
    <tr>
        <td width="1" bgcolor="#b7b7b7">
            <img height="1" src="images/spacer.gif" width="1" alt=""></td>
        <td colspan="4">
            <img height="27" src="images/sp_int_ShippingMethodTable_hdr.gif" width="348" alt="Select Shipping Method"></td>
        <td width="1" bgcolor="#b7b7b7">
            <img height="1" src="images/spacer.gif" width="1" alt=""></td>
    </tr>
    <tr>
        <td width="1" bgcolor="#b7b7b7">
            <img height="1" src="images/spacer.gif" width="1" alt=""></td>
        <td width="10">
            <img height="1" src="images/spacer.gif" width="10" alt=""></td>
        <td colspan="2">
            <asp:Label ID="LabelShipMethods" runat="server" CssClass="redAsterick"></asp:Label></td>
        <td width="10">
            <img height="1" src="images/spacer.gif" width="10" alt=""></td>
        <td width="1" bgcolor="#b7b7b7">
            <img height="1" src="images/spacer.gif" width="1" alt=""></td>
    </tr>
    <tr>
        <td width="1" bgcolor="#b7b7b7">
            <img height="1" src="images/spacer.gif" width="1" alt=""></td>
        <td width="10">
            <img height="1" src="images/spacer.gif" width="10" alt=""></td>
        <td colspan="2">
            <span class="tableHeader">
                <SPS:SPSLabel ID="lblShippingMessage" runat="server" Text=""></SPS:SPSLabel>
            </span>
        </td>
        <td width="10">
            <img height="1" src="images/spacer.gif" width="10" alt=""></td>
        <td width="1" bgcolor="#b7b7b7">
            <img height="1" src="images/spacer.gif" width="1" alt=""></td>
    </tr>
    <tr>
        <td width="1" bgcolor="#b7b7b7">
            <img height="1" src="images/spacer.gif" width="1" alt=""></td>
        <td width="10">
            <img height="1" src="images/spacer.gif" width="10" alt=""></td>
        <td class="tableData" colspan="3">
            &nbsp;
			<asp:RadioButtonList ID="rbShipMethods" runat="server" CssClass="tableData"></asp:RadioButtonList>
        </td>
        <td width="1" bgcolor="#b7b7b7">
            <img height="1" src="images/spacer.gif" width="1" alt=""></td>
    </tr>
    <%--<tr>
		<td width="1" bgColor="#b7b7b7"><img height="1" src="images/spacer.gif" width="1" alt=""></td>
		<td width="10"><img height="1" src="images/spacer.gif" width="10" alt=""></td>
		<td vAlign="top" width="28" bgColor="#d5dee9">&nbsp;
			<asp:checkbox id="cbShipBack" runat="server" CssClass="tableData"></asp:checkbox></td>
		<td class="tableData" width="300" bgColor="#d5dee9">Default for back order shipping 
			is ground transportation and is free of charge.&nbsp; If you would like the 
			back order to ship the same method as the previous shipping selection, please 
			check the box.</td>
		<td width="10"><img height="1" src="images/spacer.gif" width="10" alt=""></td>
		<td width="1" bgColor="#b7b7b7"><img height="1" src="images/spacer.gif" width="1" alt=""></td>
	</tr>--%>
    <tr>
        <td width="1" bgcolor="#b7b7b7">
            <img height="1" src="images/spacer.gif" width="1" alt=""></td>
        <td width="10">
            <img height="8" src="images/spacer.gif" width="10" alt=""></td>
        <td width="28">
            <img height="8" src="images/spacer.gif" width="10" alt=""></td>
        <td width="300">
            <img height="8" src="images/spacer.gif" width="10" alt=""></td>
        <td width="10">
            <img height="8" src="images/spacer.gif" width="10" alt=""></td>
        <td width="1" bgcolor="#b7b7b7">
            <img height="1" src="images/spacer.gif" width="1" alt=""></td>
    </tr>
    <tr bgcolor="#b7b7b7">
        <td>
            <img height="1" src="images/spacer.gif" width="1" alt=""></td>
        <td>
            <img height="1" src="images/spacer.gif" width="1" alt=""></td>
        <td>
            <img height="1" src="images/spacer.gif" width="1" alt=""></td>
        <td>
            <img height="1" src="images/spacer.gif" width="1" alt=""></td>
        <td>
            <img height="1" src="images/spacer.gif" width="1" alt=""></td>
        <td>
            <img height="1" src="images/spacer.gif" width="1" alt=""></td>
    </tr>
</table>
