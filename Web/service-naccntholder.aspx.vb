Imports ServicesPlusException
Imports Sony.US.AuditLog
Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Process
Imports Sony.US.SIAMUtilities

Namespace ServicePLUSWebApp

    Partial Class service_naccntholder
        Inherits SSL

        Public customer As Customer
        Public sModelName As String
        Public bShow As Boolean = True
        Public mDepotServiceCharge As String

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Dim oServiceInfo As ServiceItemInfo

            Try
                If Session.Item("ServiceModelNumber") IsNot Nothing Then
                    sModelName = Session.Item("ServiceModelNumber").ToString()
                ElseIf Request.QueryString("model") IsNot Nothing Then
                    sModelName = Request.QueryString("model").Trim()
                Else
                    sModelName = String.Empty
                    Utilities.LogMessages(Date.Now.ToShortTimeString & " - service-naccntholder.Page_Load - ServiceModelNumber and Model are empty")
                    Response.Redirect("sony-repair.aspx")
                    Exit Sub
                End If

                ' check for Service Contract/Warranty.
                If Session.Item("snServiceInfo") Is Nothing Then
                    Utilities.LogMessages(Date.Now.ToShortTimeString & " - service-naccntholder.Page_Load - snServiceInfo is empty")
                    Response.Redirect("sony-repair.aspx")
                    Exit Sub
                End If

                oServiceInfo = CType(Session("snServiceInfo"), ServiceItemInfo)
                If UCase(oServiceInfo.WarrantyType) = "C" Or UCase(oServiceInfo.WarrantyType) = "W" Then
                    bShow = False
                End If

                If Not IsPostBack Then
                    PopulateStatesDDL(ddlBillToState)
                    PopulateStatesDDL(ddlShpState)
                    LoadBillToAddress()
                End If

                'lblSubHdr.Text = sModelName + GetGlobalResourceObject("Resource", "repair_svcacc_dept_msg")
                lblSubHdr.InnerText = sModelName + Resources.Resource.repair_svcacc_dept_msg

                If HttpContextManager.GlobalData.IsCanada Then
                    mDepotServiceCharge = ConfigurationData.GeneralSettings.RepairCosting.DepotServiceChargeForCanada
                Else
                    mDepotServiceCharge = ConfigurationData.GeneralSettings.RepairCosting.DepotServiceCharge
                End If

                chkApproval.Text = Resources.Resource.repair_svcacc_info_msg + mDepotServiceCharge + Resources.Resource.repair_svcacc_info_msg_1
            Catch ex As Exception
                ErrorValidation.Text = Utilities.WrapExceptionforUI(ex)
                ErrorValidation.Visible = True
            End Try
        End Sub

        Private Sub LoadBillToAddress()
            customer = Session.Item("customer")

            If bShow = True Then
                txtAttnName.Text = customer.Name
                txtAddr1.Text = customer.Address.Line1
                txtAddr2.Text = customer.Address.Line2
                txtAddr3.Text = customer.Address.Line3
                txtCity.Text = customer.Address.City
                txtZip.Text = customer.Address.PostalCode
                txtCompany.Text = customer.CompanyName
                txtPhone.Text = customer.PhoneNumber
                txtPhExtn.Text = customer.PhoneExtension
                ddlBillToState.SelectedValue = customer.Address.State
                txtFax.Text = customer.FaxNumber.ToString()
                'Commented after Issue E793 reopen
                ''Start Modification for E793
                'txtEmail.Text = customer.EmailAddress.ToString()
                ''End Modification for E793
            Else
                txtShpAttnName.Text = customer.Name
                txtShpAddr1.Text = customer.Address.Line1
                txtShpAddr2.Text = customer.Address.Line2
                txtShpAddr3.Text = customer.Address.Line3
                txtShpCity.Text = customer.Address.City
                txtShpZip.Text = customer.Address.PostalCode
                txtShpCompany.Text = customer.CompanyName
                txtShpPhone.Text = customer.PhoneNumber
                txtShpExtension.Text = customer.PhoneExtension
                txtShpFax.Text = customer.FaxNumber.ToString()
                'Commented after Issue E793 reopen
                ''Start Modification for E793
                'txtShpEmail.Text = customer.EmailAddress.ToString()
                ''End Modification for E793
                ddlShpState.SelectedValue = customer.Address.State
            End If

            'Start Modification after Issue E793 reopen
            txtContactName.Text = customer.Name
            txtEmailAddr.Text = customer.EmailAddress
            'End Modification after Issue E793 reopen

            LabelLineShip1Star.Text = "*"
            LabelLineShip4Star.Text = "*"

        End Sub

        Private Function IsValidPhone(ByVal pPhno As String) As Boolean
            Dim reg As New Regex("^([(]?\d{3}[-)]\d{3}-\d{4})*$")

            Return reg.IsMatch(pPhno)
        End Function

        Private Function validateForm() As Boolean

            Dim bReturn As Boolean = True

            'Start Modification for E793
            Dim thisPattern As String = "\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
            'Start Modification for E793
            LabelLineBill1Star.Text = ""
            LabelLineBill2Star.Text = ""
            LabelLineBill3Star.Text = ""
            LabelLineBill4Star.Text = ""

            LabelLineShip1Star.Text = ""
            LabelLineShip2Star.Text = ""
            LabelLineShip3Star.Text = ""
            LabelLineShip4Star.Text = ""


            If Trim(txtCompany.Text) = "" Then
                lblBillCompany.CssClass = "redAsterick"
                bReturn = False
            Else
                lblBillCompany.CssClass = ""
            End If

            If Trim(txtAttnName.Text) = "" Then
                lblBillAttn.CssClass = "redAsterick"
                bReturn = False
            Else
                lblBillAttn.CssClass = ""
            End If

            If Trim(txtAddr1.Text) = "" Then
                lblBillAddr1.CssClass = "redAsterick"
                bReturn = False
            Else
                lblBillAddr1.CssClass = ""
            End If
            If txtAddr1.Text.Length > 35 Then
                lblBillAddr1.CssClass = "redAsterick"
                bReturn = False
                LabelLineBill1Star.Text = "<span class=""redAsterick"">&nbsp;&nbsp;" + GetGlobalResourceObject("Resource", "repair_snysvc_nachldrval_msg") + "</span>"
            Else
                lblBillAddr1.CssClass = ""
            End If

            If txtAddr2.Text.Length > 35 Then
                lblBillAddr2.CssClass = "redAsterick"
                bReturn = False
                LabelLineBill2Star.Text = "<span class=""redAsterick"">&nbsp;&nbsp;" + GetGlobalResourceObject("Resource", "repair_snysvc_nachldrval_msg_1") + "</span>"
            Else
                lblBillAddr2.CssClass = ""
            End If

            If txtAddr3.Text.Length > 35 Then
                lblBillAddr3.CssClass = "redAsterick"
                bReturn = False
                LabelLineBill3Star.Text = "<span class=""redAsterick"">&nbsp;&nbsp;" + GetGlobalResourceObject("Resource", "repair_snysvc_nachldrval_msg_2") + "</span>"
            Else
                lblBillAddr3.CssClass = ""
            End If

            If Trim(txtCity.Text) = "" Then
                lblBillCity.CssClass = "redAsterick"
                bReturn = False
            Else
                lblBillCity.CssClass = ""
            End If
            If txtCity.Text.Length > 35 Then
                lblBillCity.CssClass = "redAsterick"
                bReturn = False
                LabelLineBill4Star.Text = "<span class=""redAsterick"">" + GetGlobalResourceObject("Resource", "repair_snysvc_nachldrval_msg_3") + "</span>"
            Else
                lblBillCity.CssClass = ""
            End If

            If Trim(ddlBillToState.SelectedValue.ToString()) = "" Then
                lblBillState.CssClass = "redAsterick"
                bReturn = False
            Else
                lblBillState.CssClass = ""
            End If

            If Trim(txtZip.Text) = "" Then
                lblBillZip.CssClass = "redAsterick"
                bReturn = False
            Else
                lblBillZip.CssClass = ""
            End If

            If Trim(txtPhone.Text) = "" Then
                lblBillPhNo.CssClass = "redAsterick"
                bReturn = False
            Else
                lblBillPhNo.CssClass = ""
            End If

            If IsValidPhone(txtPhone.Text) = False Then
                lblBillPhNo.CssClass = "redAsterick"
                bReturn = False
                lblErrMsgBillPhone.Visible = True
            Else
                lblBillPhNo.CssClass = ""
                lblErrMsgBillPhone.Visible = False
            End If

            ' validating Fax no. If entered then validate it.

            If Trim(txtFax.Text) <> "" Then
                If IsValidPhone(txtFax.Text) = False Then
                    lblBillFax.CssClass = "redAsterick"
                    bReturn = False
                    lblErrmsgBillFax.Visible = True
                Else
                    lblErrmsgBillFax.Visible = False
                    lblBillFax.CssClass = ""
                End If
            Else
                lblErrmsgBillFax.Visible = False
                lblBillFax.CssClass = ""
            End If

            'Commented after Issue E793 reopen
            ''Start Modification for E793
            'If Trim(txtEmail.Text) <> "" Then
            '    If Not Regex.IsMatch(Me.txtEmail.Text, thisPattern) Then
            '        lblBillEmail.CssClass = "redAsterick"
            '        bReturn = False
            '        lblErrmsgBillEmail.Visible = True
            '    Else
            '        lblErrmsgBillEmail.Visible = False
            '        lblBillEmail.CssClass = ""
            '    End If
            'Else
            '    lblErrmsgBillEmail.Visible = False
            '    lblBillEmail.CssClass = ""
            'End If
            ''End Modification for E793

            If Trim(txtShpCompany.Text) = "" Then
                lblShpCompany.CssClass = "redAsterick"
                bReturn = False
            Else
                lblShpCompany.CssClass = ""
            End If

            If Trim(txtShpAttnName.Text) = "" Then
                lblShpAttn.CssClass = "redAsterick"
                bReturn = False
            Else
                lblShpAttn.CssClass = ""
            End If

            If Trim(txtShpAddr1.Text) = "" Then
                lblShpAddr1.CssClass = "redAsterick"
                bReturn = False
            Else
                lblShpAddr1.CssClass = ""
                'LabelLineShip1Star.Text = "*"
            End If

            If txtShpAddr1.Text.Length > 35 Then
                lblShpAddr1.CssClass = "redAsterick"
                bReturn = False
                LabelLineShip1Star.Text = "<span class=""redAsterick"">&nbsp;&nbsp;" + GetGlobalResourceObject("Resource", "repair_svcacc_vald_msg_1") + "</span>"
            Else
                lblShpAddr1.CssClass = ""
                LabelLineShip1Star.Text = "*"
            End If

            If txtShpAddr2.Text.Length > 35 Then
                lblShpAddr2.CssClass = "redAsterick"
                bReturn = False
                LabelLineShip2Star.Text = "<span class=""redAsterick"">&nbsp;&nbsp;" + GetGlobalResourceObject("Resource", "repair_svcacc_vald_msg_2") + "</span>"
            Else
                lblShpAddr2.CssClass = ""
                LabelLineShip2Star.Text = ""
            End If

            If txtShpAddr3.Text.Length > 35 Then
                lblShpAddr3.CssClass = "redAsterick"
                bReturn = False
                LabelLineShip3Star.Text = "<span class=""redAsterick"">&nbsp;&nbsp;" + GetGlobalResourceObject("Resource", "repair_svcacc_vald_msg_3") + "</span>"
            Else
                lblShpAddr3.CssClass = ""
                LabelLineShip3Star.Text = ""
            End If

            If Trim(txtShpCity.Text) = "" Then
                lblShpCity.CssClass = "redAsterick"
                bReturn = False
            Else
                lblShpCity.CssClass = ""
            End If
            If txtShpCity.Text.Length > 35 Then
                lblShpCity.CssClass = "redAsterick"
                bReturn = False
                LabelLineShip4Star.Text = "<span class=""redAsterick"">&nbsp;&nbsp;" + GetGlobalResourceObject("Resource", "repair_svcacc_vald_msg_4") + "</span>"
            Else
                lblShpCity.CssClass = ""
                LabelLineShip4Star.Text = "*"
            End If

            If Trim(ddlShpState.SelectedValue.ToString()) = "" Then
                lblShpState.CssClass = "redAsterick"
                bReturn = False
            Else
                lblShpState.CssClass = ""
            End If

            If Trim(txtShpPhone.Text) = "" Then
                lblShpPhno.CssClass = "redAsterick"
                bReturn = False
            Else
                lblShpPhno.CssClass = ""
            End If

            If IsValidPhone(Trim(txtShpPhone.Text)) = False Then
                lblShpPhno.CssClass = "redAsterick"
                bReturn = False
                lblErrMsgShpPhoneno.Visible = True
            Else
                lblShpPhno.CssClass = ""
                lblErrMsgShpPhoneno.Visible = False
            End If

            ' validating Fax no. If entered then validate it.

            If Trim(txtShpFax.Text) <> "" Then
                If IsValidPhone(txtShpFax.Text) = False Then
                    lblShpFax.CssClass = "redAsterick"
                    bReturn = False
                    lblMsgErrShpFaxno.Visible = True
                Else
                    lblMsgErrShpFaxno.Visible = False
                    lblShpFax.CssClass = ""
                End If
            Else
                lblMsgErrShpFaxno.Visible = False
                lblShpFax.CssClass = ""
            End If

            'Commented after Issue E793 reopen
            ''Start Modification for E793
            'If Trim(txtShpEmail.Text) <> "" Then
            '    If Not Regex.IsMatch(Me.txtShpEmail.Text, thisPattern) Then
            '        lblShpEmail.CssClass = "redAsterick"
            '        bReturn = False
            '        lblErrMsgShpEmail.Visible = True
            '    Else
            '        lblErrMsgShpEmail.Visible = False
            '        lblShpEmail.CssClass = ""
            '    End If
            'Else
            '    lblErrMsgShpEmail.Visible = False
            '    lblShpEmail.CssClass = ""
            'End If
            ''End Modification for E793

            If Trim(txtShpZip.Text) = "" Then
                lblShpZip.CssClass = "redAsterick"
                bReturn = False
            Else
                lblShpZip.CssClass = ""
            End If

            If Trim(txtPONumber.Text) = "" Then
                lblPONumber.CssClass = "redAsterick"
                bReturn = False
            Else
                lblPONumber.CssClass = ""
            End If

            If chkApproval.Checked = False Then
                lblApprvl.CssClass = "redAsterick"
                bReturn = False
            Else
                lblApprvl.CssClass = ""
            End If

            'If chkPreapproval.Checked = True And Trim(txtPreApprvAmt.Text) = "" Then
            'If chkPreapproval.Checked = True And Trim(txtPreApprvAmt.Text) = "" Then 'Commented to fix Bug# 610
            '    lblPreApprvl.CssClass = "redAsterick"
            '    lblPreApprvlMsg.Visible = True
            '    bReturn = False
            'ElseIf chkPreapproval.Checked = True And Not IsNumeric(txtPreApprvAmt.Text.Trim()) Then
            '    lblPreApprvl.CssClass = "redAsterick"
            '    lblPreApprvlMsg.Visible = True
            '    bReturn = False
            'Else
            '    lblPreApprvlMsg.Visible = False
            '    lblPreApprvl.CssClass = ""
            'End If


            If Trim(txtContactName.Text) = "" Then
                lblContactName.CssClass = "redAsterick"
                bReturn = False
                lblContactName.Visible = True
            Else
                lblContactName.Visible = False
                lblContactName.CssClass = ""
            End If


            'Start Modification after Issue E793 reopen
            If Trim(txtEmailAddr.Text) = "" Then
                lblEmailAddr.CssClass = "redAsterick"
                bReturn = False
                lblErrMsgEmailAddr.Visible = True
            ElseIf Not Regex.IsMatch(Me.txtEmailAddr.Text, thisPattern) Then
                lblEmailAddr.CssClass = "redAsterick"
                bReturn = False
                lblErrMsgEmailAddr.Visible = True
            Else
                lblErrMsgEmailAddr.Visible = False
                lblEmailAddr.CssClass = ""
            End If
            'End Modification after Issue E793 reopen 

            If bReturn = False Then
                ErrorValidation.Visible = True
            Else
                ErrorValidation.Visible = False
            End If

            Return bReturn

        End Function

        Private Function validateForm2() As Boolean

            Dim bReturn As Boolean = True
            'Start Modification for E793
            Dim thisPattern As String = "\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
            'Start Modification for E793

            If Trim(txtShpCompany.Text) = "" Then
                lblShpCompany.CssClass = "redAsterick"
                bReturn = False
            Else
                lblShpCompany.CssClass = ""
            End If

            If Trim(txtShpAttnName.Text) = "" Then
                lblShpAttn.CssClass = "redAsterick"
                bReturn = False
            Else
                lblShpAttn.CssClass = ""
            End If
            If Trim(txtShpAddr1.Text) = "" Then
                lblShpAddr1.CssClass = "redAsterick"
                bReturn = False
            Else
                lblShpAddr1.CssClass = ""
                'LabelLineShip1Star.Text = "*"
            End If

            If txtShpAddr1.Text.Length > 35 Then
                lblShpAddr1.CssClass = "redAsterick"
                bReturn = False
                LabelLineShip1Star.Text = "<span class=""redAsterick"">&nbsp;&nbsp;" + GetGlobalResourceObject("Resource", "repair_svcacc_vald_msg_1") + "</span>"
            Else
                lblShpAddr1.CssClass = ""
                LabelLineShip1Star.Text = "*"
            End If

            If txtShpAddr2.Text.Length > 35 Then
                lblShpAddr2.CssClass = "redAsterick"
                bReturn = False
                LabelLineShip2Star.Text = "<span class=""redAsterick"">&nbsp;&nbsp;" + GetGlobalResourceObject("Resource", "repair_svcacc_vald_msg_2") + "</span>"
            Else
                lblShpAddr2.CssClass = ""
                LabelLineShip2Star.Text = ""
            End If

            If txtShpAddr3.Text.Length > 35 Then
                lblShpAddr3.CssClass = "redAsterick"
                bReturn = False
                LabelLineShip3Star.Text = "<span class=""redAsterick"">&nbsp;&nbsp;" + GetGlobalResourceObject("Resource", "repair_svcacc_vald_msg_3") + "</span>"
            Else
                lblShpAddr3.CssClass = ""
                LabelLineShip3Star.Text = ""
            End If

            If Trim(txtShpCity.Text) = "" Then
                lblShpCity.CssClass = "redAsterick"
                bReturn = False
            Else
                lblShpCity.CssClass = ""
            End If
            If txtShpCity.Text.Length > 35 Then
                lblShpCity.CssClass = "redAsterick"
                bReturn = False
                LabelLineShip4Star.Text = "<span class=""redAsterick"">&nbsp;&nbsp;" + GetGlobalResourceObject("Resource", "repair_svcacc_vald_msg_4") + "</span>"
            Else
                lblShpCity.CssClass = ""
                LabelLineShip4Star.Text = "*"
            End If

            If Trim(ddlShpState.SelectedValue.ToString()) = "" Then
                lblShpState.CssClass = "redAsterick"
                bReturn = False
            Else
                lblShpState.CssClass = ""
            End If

            If Trim(txtShpPhone.Text) = "" Then
                lblShpPhno.CssClass = "redAsterick"
                bReturn = False
            Else
                lblShpPhno.CssClass = ""
            End If

            If Trim(txtShpPhone.Text) <> "" And IsValidPhone(Trim(txtShpPhone.Text)) = False Then
                lblShpPhno.CssClass = "redAsterick"
                bReturn = False
                lblErrMsgShpPhoneno.Visible = True
            Else
                lblShpPhno.CssClass = ""
                lblErrMsgShpPhoneno.Visible = False
            End If

            ' validating Fax no. If entered then validate it.

            If Trim(txtShpFax.Text) <> "" Then
                If IsValidPhone(txtShpFax.Text) = False Then
                    lblShpFax.CssClass = "redAsterick"
                    bReturn = False
                    lblMsgErrShpFaxno.Visible = True
                Else
                    lblMsgErrShpFaxno.Visible = False
                    lblShpFax.CssClass = ""
                End If
            Else
                lblMsgErrShpFaxno.Visible = False
                lblShpFax.CssClass = ""
            End If

            'Commented after Issue E793 reopen
            ''Start Modification for E793
            'If Trim(txtShpEmail.Text) <> "" Then
            '    If Not Regex.IsMatch(Me.txtShpEmail.Text, thisPattern) Then
            '        lblShpEmail.CssClass = "redAsterick"
            '        bReturn = False
            '        lblErrMsgShpEmail.Visible = True
            '    Else
            '        lblErrMsgShpEmail.Visible = False
            '        lblShpEmail.CssClass = ""
            '    End If
            'Else
            '    lblErrMsgShpEmail.Visible = False
            '    lblShpEmail.CssClass = ""
            'End If
            ''End Modification for E793

            If Trim(txtShpZip.Text) = "" Then
                lblShpZip.CssClass = "redAsterick"
                bReturn = False
            Else
                lblShpZip.CssClass = ""
            End If


            If Trim(txtContactName.Text) = "" Then
                lblContactName.CssClass = "redAsterick"
                bReturn = False
                lblContactName.Visible = True
            Else
                lblContactName.Visible = False
                lblContactName.CssClass = ""
            End If

            'Start Modification after Issue E793 reopen
            If Trim(txtEmailAddr.Text) = "" Then
                lblEmailAddr.CssClass = "redAsterick"
                bReturn = False
                lblErrMsgEmailAddr.Visible = True
            ElseIf Not Regex.IsMatch(Me.txtEmailAddr.Text, thisPattern) Then
                lblEmailAddr.CssClass = "redAsterick"
                bReturn = False
                lblErrMsgEmailAddr.Visible = True
            Else
                lblErrMsgEmailAddr.Visible = False
                lblEmailAddr.CssClass = ""
            End If
            'End Modification after Issue E793 reopen

            If bReturn = False Then
                ErrorValidation.Visible = True
            Else
                ErrorValidation.Visible = False
            End If

            Return bReturn

        End Function

        Protected Sub imgBtnNext_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgBtnNext.Click
            Dim objPCILogger As New PCILogger()
            Dim oServiceInfo As ServiceItemInfo
            Dim oBillTo As New CustomerAddr
            Dim oShipTo As New CustomerAddr
            Try
                objPCILogger.EventOriginApplication = GetGlobalResourceObject("Resource", "header_serviceplus")
                objPCILogger.EventOriginApplicationLocation = Me.Page.GetType().Name
                objPCILogger.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
                objPCILogger.OperationalUser = Environment.UserName
                objPCILogger.EventDateTime = DateTime.Now.ToLongTimeString()
                objPCILogger.HTTPRequestObjectValues = objPCILogger.GetRequestContextValue()
                objPCILogger.IndicationSuccessFailure = GetGlobalResourceObject("Resource", "repair_snysvc_nachldrerror_msg")
                objPCILogger.EventOriginMethod = GetGlobalResourceObject("Resource", "repair_snysvc_nachldrerror_msg_2")
                objPCILogger.Message = GetGlobalResourceObject("Resource", "repair_snysvc_nachldrerror_msg_2")
                objPCILogger.EventType = EventType.Update

                oServiceInfo = Session("snServiceInfo")

                If bShow Then
                    If Not validateForm() Then Return
                Else
                    If Not validateForm2() Then Return
                End If
                oServiceInfo.ModelNumber = sModelName
                FindDepotCenterAddress(Trim(ddlShpState.SelectedItem.Value), oServiceInfo)
                If String.IsNullOrWhiteSpace(oServiceInfo.DepotCenterAddress) Then
                    Response.Redirect("sony-service-centernotfound.aspx", True)
                Else
                    Session("snServiceInfo") = oServiceInfo
                    If oServiceInfo.DepotCenterAddress = String.Empty Then
                        'TODO: Put a blank page and show message "For depot service for models not found here, please contact the Sony national service line at 866-766-9272, then select option 2."
                    End If

                    oBillTo.Company = Trim(txtCompany.Text)
                    oBillTo.AttnName = Trim(txtAttnName.Text)
                    oBillTo.Address1 = Trim(txtAddr1.Text)
                    oBillTo.Address2 = Trim(txtAddr2.Text)
                    oBillTo.Address3 = "" 'Trim(txtAddr3.Text)
                    oBillTo.City = Trim(txtCity.Text)
                    oBillTo.Phone = Trim(txtPhone.Text)
                    oBillTo.PhoneExtn = Trim(txtPhExtn.Text)
                    If bShow Then
                        oBillTo.State = Trim(ddlBillToState.SelectedItem.Value)
                    End If
                    oBillTo.ZipCode = Trim(txtZip.Text)
                    oBillTo.Fax = Trim(txtFax.Text)
                    oServiceInfo.BillToAddr = oBillTo

                    oServiceInfo.IsProductShipOnHold = Convert.ToBoolean(rdoShipOption.SelectedIndex)

                    oShipTo.Company = Trim(txtShpCompany.Text)
                    oShipTo.AttnName = Trim(txtShpAttnName.Text)
                    oShipTo.Address1 = Trim(txtShpAddr1.Text)
                    oShipTo.Address2 = Trim(txtShpAddr2.Text)
                    oShipTo.Address3 = "" 'Trim(txtShpAddr3.Text)
                    oShipTo.City = Trim(txtShpCity.Text)
                    oShipTo.Phone = Trim(txtShpPhone.Text)
                    oShipTo.PhoneExtn = Trim(txtShpExtension.Text)
                    oShipTo.State = Trim(ddlShpState.SelectedItem.Value)
                    oShipTo.ZipCode = Trim(txtShpZip.Text)
                    oShipTo.Fax = Trim(txtShpFax.Text)
                    oServiceInfo.ShipToAddr = oShipTo

                    oServiceInfo.PONumber = Trim(txtPONumber.Text)
                    oServiceInfo.isApproval = chkApproval.Checked
                    oServiceInfo.isTaxExempt = chkTaxexempt.Checked
                    oServiceInfo.ShipStateAbbr = ddlShpState.SelectedValue.ToString()
                    oServiceInfo.PaymentType = rdPayment.SelectedValue
                    oServiceInfo.ContactPersonName = txtContactName.Text.ToString().Trim()
                    oServiceInfo.ContactEmailAddress = Trim(txtEmailAddr.Text)

                    Session("snServiceInfo") = oServiceInfo

                    'Response.Redirect("sony-service-problem-desc.aspx") '?model=" + sModelName)
                    Response.Redirect("sony-service-problem-desc.aspx?model=" + sModelName)
                End If
            Catch ex As Exception
                ErrorValidation.Text = Utilities.WrapExceptionforUI(ex)
                ErrorValidation.Visible = True
                objPCILogger.IndicationSuccessFailure = GetGlobalResourceObject("Resource", "mem_csc_pgerror_msg_3")  '6524
                objPCILogger.Message = GetGlobalResourceObject("Resource", "repair_snysvc_nachldrerror_msg_3") & ex.Message.ToString() '6524
            Finally
                objPCILogger.PushLogToMSMQ() '6524
            End Try

        End Sub

        Private Sub FindDepotCenterAddress(ByVal pState As String, ByRef oServiceInfo As ServiceItemInfo)
            Dim cm As New CatalogManager
            Dim sAddr As String

            If pState.Length > 0 Then
                Try
                    sAddr = cm.GetDepotServiceCenter(pState, sModelName)
                    If sAddr <> String.Empty Then
                        sAddr = sAddr.Replace("!", "<br/>")
                        oServiceInfo.DepotCenterAddress = sAddr.Split(("#").ToCharArray)(0)
                        oServiceInfo.DepotCenterAddressID = Convert.ToInt32(sAddr.Split(("#").ToCharArray)(1))
                        oServiceInfo.DepotEmail = Convert.ToString(sAddr.Split(("#").ToCharArray)(2))
                    Else
                        'oServiceInfo.DepotCenterAddress = "For depot service for models not found here, please contact the Sony national service line at 866-766-9272, then select option 2."
                        oServiceInfo.DepotCenterAddress = String.Empty
                    End If
                Catch ex As Exception
                    oServiceInfo.DepotCenterAddress = ex.Message
                    oServiceInfo.ShipStateAbbr = ddlShpState.SelectedItem.Text
                    Response.Redirect("sony-service-centernotfound.aspx", True)
                End Try
            Else
                oServiceInfo.DepotCenterAddress = "NA"
            End If
        End Sub

        Private Sub PopulateStatesDDL(ByVal objDDL As Object)
            Dim cm As CatalogManager = New CatalogManager
            Dim states() As StatesDetail
            Dim iState As StatesDetail

            Try
                states = cm.StatesBySalesOrg(HttpContextManager.GlobalData)
                'states = cm.GetStates()
                Dim newListItem1 As New ListItem(GetGlobalResourceObject("Resource", "repair_ddl_sl_msg"), "")
                objDDL.Items.Add(newListItem1)

                For Each iState In states
                    Dim newListItem As New ListItem(iState.StateName, iState.StateAbbr)
                    objDDL.Items.Add(newListItem)
                Next
            Catch ex As Exception
                ErrorValidation.Text = Utilities.WrapExceptionforUI(ex)
                ErrorValidation.Visible = True
            End Try
        End Sub

        Private Function ChangeCase(ByVal pString As String) As String
			Dim str As String = pString.ToLower()
			Return System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(str)
		End Function

        ' ASleight - This is handled in SSL.aspx, which all pages inherit from
        'Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        '	If (HttpContextManager.SelectedLang IsNot Nothing) Then
        '		Dim ci As New CultureInfo(HttpContextManager.SelectedLang)
        '		Page.Culture = ci.ToString()
        '		Page.UICulture = ci.ToString()
        '	Else
        '		HttpContextManager.SelectedLang = "en-US"
        '		Page.Culture = "en-US"
        '		Page.UICulture = "en-US"
        '	End If
        'End Sub
        'Protected Sub noPoRequiredCheckBox_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles noPoRequiredCheckBox.CheckedChanged
        '    If noPoRequiredCheckBox.Checked = True Then
        '        Me.txtPONumber.Text = "NO-PO-REQ"
        '    Else
        '        Me.txtPONumber.Text = ""
        '    End If
        'End Sub
    End Class
End Namespace
