<%@ Reference Page="~/SSL.aspx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="SonyFooter" Src="SonyFooter.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="PersonalMessage" Src="PersonalMessage.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="ServicePlusNav" Src="ServicePlusNav.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="TopSonyHeader" Src="TopSonyHeader.ascx" %>

<%@ Page Language="vb" AutoEventWireup="false" Inherits="ServicePLUSWebApp.ViewSavedCart" EnableViewStateMac="true" CodeFile="ViewSavedCart.aspx.vb" CodeFileBaseClass="ServicePLUSWebApp.SSL" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="<%=PageLanguage %>">
<head>
    <title><%=Resources.Resource.ttl_ViewSavedCartDetails%> </title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link href="includes/ServicesPLUS_style.css" type="text/css" rel="stylesheet">

    <script src="includes/jquery-1.10.2.min.js" type="text/javascript"></script>
    <script src="includes/ServicesPLUS.js" type="text/javascript"></script>
    <script src="includes/svcplus_globalization.js" type="text/javascript"></script>
</head>
<body style="color: #000000; background: #5d7180; margin: 0px;">
    <form id="formid" method="post" runat="server">
        <center>
            <table width="760" border="0" role="presentation">
                <tr>
                    <td width="25" style="background: url('images/sp_left_bkgd.gif');">
                        <img src="images/spacer.gif" width="25" height="25" alt="">
                    </td>
                    <td width="710" bgcolor="#ffffff">
                        <table width="710" border="0" role="presentation">
                            <tr>
                                <td>
                                    <ServicePLUSWebApp:TopSonyHeader ID="TopSonyHeader" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <nav><ServicePLUSWebApp:ServicePlusNav ID="SonyNav" runat="server" /></nav>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="710" border="0" role="presentation">
                                        <tr>
                                            <td width="464" bgcolor="#363d45" height="57" background="images/sp_int_header_top_ServicesPLUS_onepix.gif"
                                                align="right" valign="top">
                                                <br>
                                                <h1 class="headerText">ServicesPLUS &nbsp;&nbsp;</h1>
                                            </td>
                                            <td valign="middle" bgcolor="#363d45">
                                                <ServicePLUSWebApp:PersonalMessage ID="Personalmessage1" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td bgcolor="#f2f5f8">
                                                <h2 class="headerTitle" style="padding-right: 20px; text-align: right;"><%=Resources.Resource.el_ViewSavedCardDetail%></h2>
                                            </td>
                                            <td bgcolor="#99a8b5">&nbsp;</td>
                                        </tr>
                                        <tr height="9">
                                            <td bgcolor="#f2f5f8">
                                                <img height="9" src="images/sp_int_header_btm_left_onepix.gif" width="464" alt="">
                                            </td>
                                            <td bgcolor="#99a8b5">
                                                <img height="9" src="images/sp_int_header_btm_right.gif" width="246" alt="">
                                            </td>
                                        </tr>
                                    </table>
                                    <asp:Label ID="ErrorLabel" runat="server" CssClass="redAsterick"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="710" border="0" role="presentation">
                                        <tr>
                                            <td width="20" height="20">
                                                <img height="20" src="images/spacer.gif" width="20" alt="">
                                            </td>
                                            <td width="670" height="20">
                                                <img height="20" src="images/spacer.gif" width="670" alt="">
                                            </td>
                                            <td width="20" height="20">
                                                <img height="20" src="images/spacer.gif" width="20" alt="">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="20">
                                                <img height="20" src="images/spacer.gif" width="20" alt="">
                                            </td>
                                            <td width="670">

                                                <table cellpadding="2" width="360" border="0" role="presentation">
                                                    <tr>
                                                        <td width="175" height="12">
                                                            <img height="12" src="<%=Resources.Resource.sna_svc_img_21()%>" width="53" alt="Purchase Order Number">
                                                        </td>
                                                        <td width="10" height="12">
                                                            <img height="12" src="images/spacer.gif" width="10" alt="">
                                                        </td>
                                                        <td width="175" height="12">
                                                            <img height="12" src="images/spacer.gif" width="10" alt="">
                                                            <asp:Label ID="POErrorLabel" runat="server" CssClass="redAsterick"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="top" width="175">
                                                            &nbsp;
																<SPS:SPSTextBox ID="TextBoxPO" runat="server"></SPS:SPSTextBox>
                                                        </td>
                                                        <td width="10">&nbsp;</td>
                                                        <td valign="top" width="175">
                                                            <asp:ImageButton ID="AddPO" runat="server" ImageUrl="<%$Resources:Resource,img_AddPONumber%>" AlternateText="Add PO Number"></asp:ImageButton>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td height="12">
                                                            <img height="12" src="<%=Resources.Resource.sna_svc_img_19()%>" width="66" alt="Quote Number">
                                                        </td>
                                                        <td height="12">
                                                            <img height="12" src="images/spacer.gif" width="10">
                                                        </td>
                                                        <td height="12">
                                                            <img height="12" src="<%=Resources.Resource.sna_svc_img_20()%>" width="63" alt="Quote Expires">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="tableHeader" width="175" bgcolor="#f2f5f8">
                                                            &nbsp;
																<asp:Label ID="lblQuote" runat="server"></asp:Label>
                                                        </td>
                                                        <td width="10">&nbsp;</td>
                                                        <td class="tableHeader" width="175" bgcolor="#f2f5f8">
                                                            &nbsp;
																<asp:Label ID="lblExpires" runat="server"></asp:Label>
                                                        </td>
                                                    </tr>



                                                </table>
                                                <table role="presentation">
                                                    <tr>
                                                        <td height="12">
                                                            <img height="12" src="images/spacer.gif" width="10" alt="">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3">
                                                            <asp:Label ID="lblMessageText" runat="server" CssClass="redAsterick"></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <p>
                                                    <span class="bodyCopy"><%=Resources.Resource.el_SaveShoppingCartInfo%></span>
                                                </p>
                                                <asp:Table ID="tblCartDetail" runat="server"></asp:Table>
                                                <table width="400" border="0" role="presentation">
                                                    <tr>
                                                        <td width="93">
                                                            <asp:ImageButton ID="EmailQuote" runat="server" ImageUrl="images/sp_int_emailQuote_btn.gif"
                                                                AlternateText="Email Quote"></asp:ImageButton>
                                                        </td>
                                                        <td>
                                                            <img height="4" src="images/spacer.gif" width="10" alt="">
                                                        </td>
                                                        <td width="92">
                                                            <asp:ImageButton ID="continueShopping" runat="server" ImageUrl="images/sp_int_continueShop_btn.gif"
                                                                AlternateText="Continue Shopping"></asp:ImageButton>
                                                        </td>
                                                        <td>
                                                            <img height="4" src="images/spacer.gif" width="10" alt="">
                                                        </td>
                                                        <td>
                                                            <asp:ImageButton ID="PunchOut" runat="server" Visible="false"
                                                                ImageUrl="images/sp_int_punchout_btn.gif" AlternateText="Submit cart contents to your purchasing system."></asp:ImageButton><asp:ImageButton ID="Checkout" runat="server" ImageUrl="<%$Resources:Resource,img_btnCheckOut%>" AlternateText="Checkout"></asp:ImageButton>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <%--7814 starts--%>
                                                <br />
                                                <table role="presentation">
                                                    <tr>
                                                        <td colspan="5" id="tdLegend" runat="server">
                                                            <span style="font-family: Wingdings; color: red; font-size: 12px;">v</span><span class="bodyCopy">
																	&nbsp;<a href="parts-promotions.aspx"><%=Resources.Resource.el_LimitedWaranty%></a></span>
                                                        </td>
                                                        <td id="Td1" runat="server">&nbsp;</td>
                                                    </tr>
                                                    <%--8091 starts--%>
                                                    <tr>
                                                        <td colspan="5" width="670" height="21" id="tdshowkitinfo" runat="server">
                                                            <span class="tableData">
                                                                <span color="red">*</span>&nbsp;<span color="black"><%=Resources.Resource.el_KitPartsInfo%></span></span>
                                                        </td>
                                                    </tr>
                                                    <%--8091 ends--%>
                                                    <tr>
                                                        <%-- <td width="20"><IMG height="20" src="images/spacer.gif" width="20" alt=""></td>--%>

                                                        <%--<td colspan="5"  width="670" height="21" >
													        <asp:Label ID="lblNote" Runat="server" CssClass="bodyCopy">This subtotal does not include shipping, tax and 
															any applicable recycling fees.</asp:Label></td>--%><%--7814--%>
                                                        <td colspan="5" width="670" height="21">
                                                            <asp:Label ID="lblNote" runat="server" CssClass="bodyCopy"><%=Resources.Resource.el_SubtotalInfo%></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <%--7814--%>
                                                        <%--<td width="20"><IMG height="20" src="images/spacer.gif" width="20" alt=""></td>--%>
                                                        <td colspan="5" id="tdrecyclefee" runat="server">
                                                            <span class="bodyCopy">&nbsp;   <%=Resources.Resource.el_Cart_Recycle1%> <span color="red">*</span>  <%=Resources.Resource.el_Cart_Recycle2%>&nbsp;<a href="http://www.sony.com/recyclemodels" target='_blank'>http://www.sony.com/recyclemodels.</a></span>
                                                        </td>
                                                        <td id="Td2" runat="server">&nbsp;</td>
                                                    </tr>

                                                </table>
                                                <%--7814 ends--%>
                                                <p class="legalCopy">
                                                    <%=Resources.Resource.el_CartShippingInfo1%>  <span runat="server" id="checkouttext">
                                                        <%=Resources.Resource.el_CartShippingInfo2%>  </span>
                                                    <br>
                                                    <a class="legalLink" href="TC2.aspx"><%=Resources.Resource.el_TermsAndCondition%>  </a>| <a class="legalLink" href="LW.aspx">
                                                        <%=Resources.Resource.el_LimitedWaranty%> </a>
                                                </p>
                                            </td>
                                            <td width="20">
                                                <img height="20" src="images/spacer.gif" width="20" alt="">
                                            </td>
                                        </tr>

                                    </table>
                                </td>
                            </tr>
                            <tr>

                                <td>
                                    <footer><ServicePLUSWebApp:SonyFooter ID="SonyFooter1" runat="server" /></footer>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="25" style="background: url('images/sp_right_bkgd.gif')">
                        <img src="images/spacer.gif" width="25" height="20" alt="">
                    </td>
                </tr>
            </table>
        </center>
    </form>
</body>
</html>
