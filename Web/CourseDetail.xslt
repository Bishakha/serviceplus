<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:dateTimeFormatObj="urn:xsltExtension-DateTimeFormat" xmlns:courseFormatObj="urn:xsltExtension-CourseFormat">
  <xsl:template match="/">
    <Table border="0" cellPadding="0">
      <xsl:apply-templates select="//TRAININGCOURSE">
        <xsl:sort select="CTITLE" />
      </xsl:apply-templates>
    </Table>
  </xsl:template>
  <xsl:template match="TRAININGCOURSE">
    <TR>
      <TD>
        <hr color="#99999"></hr>
      </TD>
      <TD>
        <hr color="#99999"></hr>
      </TD>
      <TD>
        <hr color="#99999"></hr>
      </TD>
    </TR>
    <TR>
      <TD valign="TOP">
        <Table border="0" cellPadding="0">

          <TR >
            <TD class="promoCopyBold" valign="TOP" >
              <a name="{CNUMBER}">
                <xsl:value-of select="CTITLE" />
              </a>
            </TD>
          </TR>
          <TR>
            <TD class="promoCopyMid">
              <xsl:value-of select="CDESCRIPTION" disable-output-escaping="yes"/>
            </TD>
          </TR>
          <TR>
            <TD class="promoCopyMid">
              Recommended Prerequisite(s): <xsl:value-of select="courseFormatObj:FormatPreRequisite(string(./PREREQUISITES))" disable-output-escaping="yes"/>
            </TD>
          </TR>
          <xsl:choose>
            <xsl:when test="TYPECODE = 0">
              <TR>
                <TD class="promoCopyMid">
                  Price per Student: <xsl:value-of select="courseFormatObj:FormatPrice(string(./PRICE))" disable-output-escaping="yes"/>
                </TD>
              </TR>
            </xsl:when>
            <xsl:otherwise>
              <TR>
                <TD class="promoCopyMid">
                  Price: <xsl:value-of select="courseFormatObj:FormatPrice(string(./PRICE))" disable-output-escaping="yes"/>
                </TD>
              </TR>
            </xsl:otherwise>
          </xsl:choose>
          <tr>
            <td>
              <a href="#top">
                <img border="0" height="28" src="images/sp_int_back2top_btn.gif" width="78" alt="Back to Top" />
              </a>
            </td>
          </tr>
        </Table>
      </TD>
      <TD width="22"></TD>
      <TD valign="TOP" >
        <Table border="0" cellPadding="0">
          <xsl:apply-templates select="TRAININGLOCATION" />
        </Table>
        <br/><br/>
        <Table border="0" cellPadding="0">
          <xsl:apply-templates select="TRAININGRCLOCATION" />
        </Table>
      </TD>
    </TR>
    <TR></TR>
  </xsl:template>
  <xsl:template match="TRAININGLOCATION">
    <xsl:if test="TYPECODE = 0">
      <xsl:if test="position() = 1 ">
        <TR>
          <TD borderColor="#ffffff" class="LocationCopy">
            <img height="29" src="images/sp_hm_classlocation_cat.gif" width="193" alt="Class Locations and Dates" />
          </TD>
        </TR>
      </xsl:if>
      <xsl:if test="position() != 1 ">
        <TR>
          <TD heigth="2" borderColor="#ffffff">
            <hr color="#b7b7b7" size="2"></hr>
          </TD>
        </TR>
      </xsl:if>
      <TR class="promoCopyMid">
        <TD nowrap="true" borderColor="#ffffff">
          <xsl:value-of disable-output-escaping="yes" select="courseFormatObj:FormatWhiteSpace(3)" />
          <xsl:value-of select="LNAME" />
        </TD>
      </TR>
      <TR class="promoCopyMid">
        <TD nowrap="true" borderColor="#ffffff">
          <xsl:value-of disable-output-escaping="yes" select="courseFormatObj:FormatWhiteSpace(3)" />
          <xsl:value-of select="LCITY" />
          <xsl:text>, </xsl:text>
          <xsl:value-of select="LSTATE" />
          <xsl:text>  </xsl:text>
          <xsl:value-of select="LCOUNTRY" />
        </TD>
      </TR>
    </xsl:if>
    <xsl:if test="TYPECODE != 0">
      <TR>
        <TD borderColor="#ffffff"></TD>
      </TR>
      <TR>
        <TD></TD>
      </TR>
      <TR>
        <TD></TD>
      </TR>
    </xsl:if>
    <xsl:apply-templates select="TRAININGCLASS" />
  </xsl:template>
  <xsl:template match="TRAININGCLASS">
    <TR class="promoCopyMid">
      <TD nowrap="true" borderColor="#ffffff">
        <xsl:value-of disable-output-escaping="yes" select="courseFormatObj:FormatWhiteSpace(5)" />
        <xsl:choose>
          <xsl:when test="TYPECODE != 0">
            <a href="sony-training-class-details.aspx?ClassNumber={CPARTNUMBER}"><img border="0" height="34" src="images/sp_int_order_btn.gif" alt="Order" width="71" /></a>
          </xsl:when>
          <xsl:when test="string-length(./STARTDATE) > 0">
            <a href="sony-training-class-details.aspx?ClassNumber={CPARTNUMBER}">
              <xsl:value-of select="dateTimeFormatObj:Format(string(./STARTDATE))" /> - <xsl:value-of select="dateTimeFormatObj:Format(string(./ENDDATE))" />
            </a>
          </xsl:when>
        </xsl:choose>
      </TD>
    </TR>
  </xsl:template>


  <xsl:template match="TRAININGRCLOCATION">
    <xsl:if test="TYPECODE = 0">
      <xsl:if test="position() = 1 ">
        <TR>
          <TD borderColor="#ffffff" class="LocationCopy">
            <img height="29" src="images/sp_hm_classlocation_cat_1.gif" width="193" alt="Request a Class" />
          </TD>
        </TR>
      </xsl:if>
      <xsl:if test="position() != 1 ">
        <TR>
          <TD heigth="2" borderColor="#ffffff">
            <hr color="#b7b7b7" size="2"></hr>
          </TD>
        </TR>
      </xsl:if>
      <TR class="promoCopyMid">
        <TD nowrap="true" borderColor="#ffffff">
          <xsl:value-of disable-output-escaping="yes" select="courseFormatObj:FormatWhiteSpace(3)" />
          <xsl:value-of select="LNAME" />
        </TD>
      </TR>
      <TR class="promoCopyMid">
        <TD nowrap="true" borderColor="#ffffff">
          <xsl:value-of disable-output-escaping="yes" select="courseFormatObj:FormatWhiteSpace(3)" />
          <xsl:value-of select="LCITY" />
          <xsl:text>, </xsl:text>
          <xsl:value-of select="LSTATE" />
          <xsl:text>  </xsl:text>
          <xsl:value-of select="LCOUNTRY" />
        </TD>
      </TR>
    </xsl:if>
    <xsl:if test="TYPECODE != 0">
      <TR>
        <TD borderColor="#ffffff"></TD>
      </TR>
      <TR>
        <TD></TD>
      </TR>
      <TR>
        <TD></TD>
      </TR>
    </xsl:if>
    <xsl:apply-templates select="TRAININGRCCLASS" />

  </xsl:template>
  <xsl:template match="TRAININGRCCLASS">
    <TR class="promoCopyMid">
      <TD nowrap="true" borderColor="#ffffff">
        <xsl:value-of disable-output-escaping="yes" select="courseFormatObj:FormatWhiteSpace(5)" />
        <xsl:choose>
          <xsl:when test="TYPECODE != 0">
            <xsl:choose>
              <xsl:when test="(TYPECODE =3) or (TYPECODE =4)">

                <!--Added following codition to avoid base url from Training url-->
                <xsl:if test="not(contains(TRAININGURL, 'http://')) and  TRAININGURL!=''">
                  <a href="http://{TRAININGURL}" target="_blank"><img border="0" height="20" src="images/sp_int_register_btn.gif" alt="Register" width="71" /></a>
                </xsl:if>

                <xsl:if test="contains(TRAININGURL, 'http://')">
                  <a href="{TRAININGURL}" target="_blank"><img border="0" height="20" src="images/sp_int_register_btn.gif" alt="Register" width="71" /></a>
                </xsl:if>

              </xsl:when>
              <xsl:when test="TYPECODE !=1">
                <a href="sony-training-catalog-2.aspx?mincatid={MINCATCODE}&#38;CourseNumber={CNUMBER}&#38;atc=true">
                  <img border="0" height="20" src="images/sp_int_add2Cart_btn.gif" alt="Add to Cart" width="30" />Add to Cart
                </a>
              </xsl:when>
              <xsl:otherwise>
                <a href="sony-software-part-{CNUMBER}.aspx">
                  <img border="0" height="34" src="images/sp_int_order_btn.gif" alt="Order" width="71" />
                </a>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:when>
          <xsl:when test="string-length(./STARTDATE) = 0">
            <a href="sony-training-class-details.aspx?CourseNumber={CNUMBER},{LOCATIONCODE}">Request Class</a>
          </xsl:when>
        </xsl:choose>
      </TD>
    </TR>
  </xsl:template>

</xsl:stylesheet>


