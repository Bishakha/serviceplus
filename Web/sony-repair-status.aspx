<%@ Page Language="VB" AutoEventWireup="false" CodeFile="sony-repair-status.aspx.vb"
    Inherits="ServicePLUSWebApp.sony_repair_status" EnableViewStateMac="true" %>

<%@ Register Src="UserControl/SPSPromotion.ascx" TagName="SPSPromotion" TagPrefix="uc1" %>
<%@ Register Src="RepairStatus.ascx" TagName="RepairStatus" TagPrefix="uc2" %>

<%@ Reference Page="~/SSL.aspx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="SonyFooter" Src="SonyFooter.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="TopSonyHeader" Src="TopSonyHeader.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="PersonalMessage" Src="PersonalMessage.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="ServicePlusNav" Src="ServicePlusNav.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="<%=PageLanguage %>">
<head>
    <title><%=Resources.Resource.header_serviceplus()%> - <%=Resources.Resource.repair_stus_hdr_msg()%></title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <meta content="Repair, Maintenance, Services, Sony Models, Sony Professional Models, Sony Broadcast Models, Sony Business Models, Sony Software, Sony Professional Software, Sony Broadcast Software, Sony Business Software"
        name="keywords">
        <link href="includes/ServicesPLUS_style.css" type="text/css" rel="stylesheet">
        <link href="themes/base/jquery.ui.all.css" rel="stylesheet" type="text/css" />
        <link href="includes/jquery.cluetip.css" rel="stylesheet" type="text/css" />
        <script src="includes/jquery-1.4.2.min.js" type="text/javascript"></script>
        <script src="includes/jquery.ui.core.js" type="text/javascript"></script>
        <script src="includes/jquery.ui.widget.js" type="text/javascript"></script>
        <script src="includes/jquery.cluetip.js" type="text/javascript"></script>
        <script src="includes/jquery.ui.mouse.js" type="text/javascript"></script>
        <script src="includes/jquery.ui.draggable.js" type="text/javascript"></script>
        <script src="includes/jquery.ui.position.js" type="text/javascript"></script>
        <script src="includes/jquery.ui.resizable.js" type="text/javascript"></script>
        <script src="includes/jquery.ui.dialog.js" type="text/javascript"></script>
        <script src="includes/jquery-ui-1.8.1.custom.min.js" type="text/javascript"></script>
        <script language="javascript" src="includes/ServicesPLUS.js" type="text/javascript"></script>
        <script type="text/javascript" src="includes/svcplus_globalization.js"></script>

    <script language="javascript" type="text/javascript">

            function SetFocus()
            {
                ModelNumber.focus();
            }
            //Added and Modified by Sneha on 17th oct 2013 for showing popup
    //code written by Umesh Dubey
               function OrderLookUp(cnt,models) {
                 var lstModels = new Array(models.split(','));
                 $("#divMessage").text('');
                 var buttons = $('.ui-dialog-buttonpane').children('button');
                 buttons.remove();
                 $("#divMessage").append(' <br/>'); 
                 $("divMessage").dialog("destroy");
                 $("#divMessage").dialog({ // dialog box
                     title: '<span class="modalpopup-title"><%=Resources.Resource.repair_mdl_msg()%></span>',
                     height: 260,
                     width: 560,
                     modal: true,
                     position: 'top'
                 });               
                 var content = "<span class='modalpopup-content'><%=Resources.Resource.repair_pup_mdlsrch_msg()%>" + "<br/>" + "<%=Resources.Resource.repair_pup_mdlsrch_msg_1()%></span>"
                 content += "<br>" + "<table class='modalpopup-content' style='margin-left:100px;'>"
                 content += '<tr><td width="100px"><u><%=Resources.Resource.repair_pup_mdlsrch_msg_3()%></u></td><td width="100px"><u><%=Resources.Resource.repair_pup_mdlsrch_msg_2()%></u></td></tr>';
                 for (i = 0; i < cnt; i++) {
                     var lstmodel = new Array(lstModels[0][i].split(':'));
                     content += '<tr><td>' + lstmodel[0][0] + '</td><td>' + lstmodel[0][1]+  '</td></tr>';
                 }
                 content += "</table>"                                
                 $("#divMessage").append(content); //message to display in divmessage div
                 $("#divMessage").append('<br/> <br/><a ><img src ="<%=Resources.Resource.img_btnCloseWindow()%>" alt ="Close" onclick="javascript:return ClosePopUp();" id="messagego" /></a>');
                return false;
             }

             function ClosePopUp() {
                $("#divMessage").dialog("close");
                return true;
             }
    </script>

    <link href="includes/ServicesPLUS_style.css" rel="stylesheet" type="text/css" />
    <link href="includes/ServicesPLUS_style.css" rel="stylesheet" type="text/css" />
</head>
<body text="#000000" bgcolor="#5d7180" leftmargin="0" topmargin="0" onload="javascript:SetFocus();"
    onkeydown="javascript:SetTheFocusButton(event, 'btnSearch');">
    <form id="Form1" method="post" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" />
        <div id="divMessage" style ="font:11px;font-family:Arial ;overflow: auto;font-weight:bold;"></div>
        <center>
            <table width="710" border="0" role="presentation">
                <tr>
                    <td background="images/sp_left_bkgd.gif" style="width: 25px">
                        <img height="25" src="images/spacer.gif" width="25" alt=""></td>
                    <td width="689" bgcolor="#ffffff">
                        <table width="100%" border="0" role="presentation">
                            <tr>
                                <td style="width: 714px">
                                    <ServicePLUSWebApp:TopSonyHeader ID="TopSonyHeader" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td height="23" style="width: 714px">
                                    <nav><ServicePLUSWebApp:ServicePlusNav ID="SonyNav" runat="server" /></nav>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 714px">
                                    <table width="710" border="0" role="presentation">
                                        <tr>
                                            <td align="right" background="images/repair_lrg.gif" bgcolor="#363d45" height="82"
                                                style="width: 465px">
                                                <br>
                                                <h1 class="headerText"><%=Resources.Resource.repair_snysvc_srl_msg_4 %></h1>
                                            </td>
                                            <td valign="top" width="238" bgcolor="#363d45">
                                                                <ServicePLUSWebApp:PersonalMessage ID="PersonalMessageDisplay" runat="server" />
                                            </td>
                                        </tr>
                                        <tr valign="top">
                                            <td bgcolor="#f2f5f8" style="width: 465px">
                                                <table width="464" border="0" role="presentation">
                                                    <tr>
                                                        <td width="20">
                                                            <img height="20" src="images/spacer.gif" width="20" alt=""></td>
                                                        <td>
                                                            <h1 class="headerTitle"><%=Resources.Resource.repair_stus_chkdpt_msg()%></h1></td>
                                                    </tr>
                                                    <tr>
                                                        <td width="20" colspan="2">
                                                            <img height="10" src="images/spacer.gif" width="20" alt=""></td>
                                                    </tr>
                                                    <tr>
                                                        <td width="20">
                                                            <img height="20" src="images/spacer.gif" width="20" alt=""></td>
                                                        <td>
                                                            <asp:Label ID="ErrorLabel" runat="server" Text="" CssClass="redAsterick" EnableViewState="false" />
                                                            <br /><span class="finderCopyDark"><%=Resources.Resource.repair_stus_srch_msg()%> <br /><%=Resources.Resource.repair_stus_srch_msg_1()%></span></td>
                                                    </tr>
                                                    <tr>
                                                        <td width="20" colspan="2">
                                                            <img height="10" src="images/spacer.gif" width="20" alt=""></td>
                                                    </tr>
                                                    <tr>
                                                        <td width="20">
                                                            <img height="20" src="images/spacer.gif" width="20" alt=""></td>
                                                        <td>
                                                            <asp:UpdatePanel ID="UpdatePanel2" runat="server" ChildrenAsTriggers=true>
                                                                <ContentTemplate>
                                                                    <table id="Table1" align="left" border="0" cellpadding="1" class="bodycopy" role="presentation">
                                                                        <tr>
                                                                            <td class="finderCopyDark" width="20%">
                                                                                <%=Resources.Resource.repair_stus_MNo_msg()%></td>
                                                                            <td class="finderCopyDark" width="20%">
                                                                                <%=Resources.Resource.repair_stus_SNo_msg()%></td>
                                                                            <td class="tableHeader" width="20%" >
                                                                            </td>
                                                                            <td class="tableHeader" width="40%">
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td >
                                                                                <SPS:SPSTextBox ID="ModelNumber" runat="server" CssClass="bodyCopy" /></td>
                                                                            <td >
                                                                                <SPS:SPSTextBox ID="SerialNumber" runat="server" CssClass="bodyCopy" /></td>
                                                                            <td class="tableHeader">
                                                                                <%=Resources.Resource.repair_stus_or_msg()%>
                                                                            </td>
                                                                            <td class="tableHeader">
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="finderCopyDark" colspan="2">
                                                                                <%=Resources.Resource.repair_stus_noti_msg()%></td>
                                                                            <td class="tableHeader" style="width: 25px">
                                                                            </td>
                                                                            <td class="tableHeader" style="width: 25px">
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="1" style="width: 134px">
                                                                                <SPS:SPSTextBox ID="NotificationNumber" runat="server" CssClass="bodyCopy"
                                                                                    ></SPS:SPSTextBox></td>
                                                                            <td class="tableHeader" colspan="2">
                                                                               <%=Resources.Resource.repair_stus_or_msg()%>
                                                                            </td>
                                                                            <td class="tableHeader" colspan="1">
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="finderCopyDark" colspan="2">
                                                                                <%=Resources.Resource.repair_stus_phno_msg()%></td>
                                                                            <td class="tableHeader" style="width: 25px">
                                                                            </td>
                                                                            <td class="tableHeader" style="width: 25px">
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="2">
                                                                                <SPS:SPSTextBox ID="PhoneNumber" runat="server" CssClass="bodyCopy"
                                                                                    ></SPS:SPSTextBox></td>
                                                                            <td class="tableHeader" style="width: 25px">
                                                                            </td>
                                                                            <td class="tableHeader" style="width: 25px">
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="4" height=15px>
                                                                                <asp:Label ID="lblError" CssClass="redAsterick" Visible=false runat="server" Text="">&nbsp;</asp:Label>
                                                                            </td>
                                                                            <td colspan="1" height="15">
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="1" height=10px>
                                                                                <asp:ImageButton ID="btnSearch" runat="server" AlternateText="Search" ImageUrl="<%$ Resources:Resource,repair_svc_img_1 %>" /></td>
                                                                            <td colspan="3">
                                                                                <asp:UpdateProgress ID="UpdateProgress1" runat="server">
                                                                                    <ProgressTemplate>
                                                                                        <span id="Span4" class="bodycopy"><%=Resources.Resource.mem_csc_plw_msg()%></span>
                                                                                        <img src="images/progbar.gif" width="100" alt="Please wait" />
                                                                                    </ProgressTemplate>
                                                                                </asp:UpdateProgress>
                                                                            </td>
                                                                            <td colspan="1">
                                                                            </td>
                                                                        </tr>
                                                                       
                                                                    </table>
                                                                </ContentTemplate>
                                                                <Triggers>
                                                                    <asp:AsyncPostBackTrigger ControlID="btnNewSearch" EventName="Click" />
                                                                </Triggers>
                                                            </asp:UpdatePanel>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td width="238" bgcolor="#99a8b5">
                                                <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                                    <ContentTemplate>
                                                        <table width="100%" border="0" role="presentation">
                                                            <tr><td>
                                                            <img height="20" src="images/spacer.gif" width="20" alt="">
                                                            </td></tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:ImageButton ID="btnNewSearch" Visible=false runat="server" AlternateText="Search" ImageUrl="<%$ Resources:Resource,img_btnNewSearch %>" OnClick="btnNewSearch_Click" /></td>
                                                            </tr>
                                                        </table>
                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:AsyncPostBackTrigger ControlID="btnSearch" EventName="Click" />
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                            </td>
                                        </tr>
                                        <tr height="9">
                                            <td bgcolor="#f2f5f8" style="width: 465px">
                                                <img height="9" src="images/sp_int_header_btm_left_onepix.gif" width="464" alt=""></td>
                                            <td width="238" bgcolor="#99a8b5">
                                                <img height="9" src="images/sp_int_header_btm_right.gif" width="246" alt=""></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 714px">
                                    <table width="100%" border="0" role="presentation">
                                        <tr>
                                            <td width="2%">
                                                <img height="20" src="images/spacer.gif" width="20" alt=""></td>
                                            <td width="100%" bordercolor="#d5dee9" valign="top">
                                                
                                            </td>
                                            <td width="2%">
                                                <img height="20" src="images/spacer.gif" width="20" alt=""></td>
                                        </tr>
                                        <tr>
                                            <td width="2%">
                                            </td>
                                            <td bordercolor="#d5dee9" valign="top" width="100%">
                                                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                                    <ContentTemplate>
                                                        <asp:PlaceHolder ID="statusControlPlaceHolder" runat="server"></asp:PlaceHolder>
                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:AsyncPostBackTrigger ControlID="btnSearch" EventName="Click" />
                                                        <%-- <asp:AsyncPostBackTrigger ControlID="btnClearSearch" EventName="Click" />--%>
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                            </td>
                                            <td width="2%">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 714px">
                                    &nbsp;</td>
                            </tr>
                            <tr>
                                
                                <td width="100%">
                                    <footer><ServicePLUSWebApp:SonyFooter ID="SonyFooter1" runat="server" /></footer>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="25" background="images/sp_right_bkgd.gif">
                        <img height="20" src="images/spacer.gif" width="25" alt=""></td>
                </tr>
            </table>
        </center>
        &nbsp;
    </form>
</body>
</html>
