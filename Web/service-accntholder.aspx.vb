Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Process
Imports ServicesPlusException
Imports Sony.US.AuditLog

Namespace ServicePLUSWebApp
    Partial Class service_accntholder
        Inherits SSL

        Private cm As New CustomerManager
        Private customer As Customer
        Private current_bill_to As Account
        Private current_ship_to_list() As Account
        Private current_bill_to_list() As Account
        Private a_shipto_list() As SISAccount
        Private b_shipto_list() As SISAccount
        Private sModelName As String
        Public bShow As Boolean = True
        Public mDepotServiceCharge As String

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Dim oServiceInfo As ServiceItemInfo

            Try
                If Session.Item("ServiceModelNumber") IsNot Nothing Then
                    If Not IsValidString(Session.Item("ServiceModelNumber").ToString()) Then Throw New ServicesPLUSUIException("User modified ServiceModelNumber session variable with illegal values.", New ArgumentException())
                    sModelName = Session.Item("ServiceModelNumber").ToString()
                ElseIf Request.QueryString("model") IsNot Nothing Then
                    If Not IsValidString(Request.QueryString("model")) Then Throw New ServicesPLUSUIException("User modified model querystring with illegal values.", New ArgumentException())
                    sModelName = Request.QueryString("model").Trim()
                Else
                    sModelName = String.Empty
                    Response.Redirect("sony-repair.aspx", False)
                    HttpContext.Current.ApplicationInstance.CompleteRequest()
                End If

                ' check for Service Contract/Warranty.
                If Session.Item("ServiceModelNumber") Is Nothing Then
                    Response.Redirect("sony-repair.aspx", False)
                    HttpContext.Current.ApplicationInstance.CompleteRequest()
                End If

                oServiceInfo = CType(Session("snServiceInfo"), ServiceItemInfo)
                If UCase(oServiceInfo.WarrantyType) = "C" Or UCase(oServiceInfo.WarrantyType) = "W" Then
                    bShow = False
                End If

                If Not IsPostBack Then
                    PopulateStatesDDL(ddlShpState)
                    PopulateBillShipDDL()
                End If

                'lblSubHdr.Text = sModelName + GetGlobalResourceObject("Resource", "repair_svcacc_dept_msg")
                lblSubHdr.InnerText = sModelName + Resources.Resource.repair_svcacc_dept_msg

                If HttpContextManager.GlobalData.IsCanada Then
                    mDepotServiceCharge = Sony.US.SIAMUtilities.ConfigurationData.GeneralSettings.RepairCosting.DepotServiceChargeForCanada
                Else
                    mDepotServiceCharge = Sony.US.SIAMUtilities.ConfigurationData.GeneralSettings.RepairCosting.DepotServiceCharge
                End If

                chkApproval.Text = Resources.Resource.repair_svcacc_info_msg + mDepotServiceCharge + Resources.Resource.repair_svcacc_info_msg_1
            Catch ex As Exception
                ErrorValidation.Text = Utilities.WrapExceptionforUI(ex)
                ErrorValidation.Visible = True
            End Try
        End Sub

        Private Function validateForm() As Boolean
            Dim isFormValid As Boolean = True
            Dim rgxEmailAddress As String = "\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"

            LabelLine1Star.Text = ""
            LabelLine2Star.Text = ""
            LabelLine3Star.Text = ""
            LabelLine4star.Text = ""

            If String.IsNullOrWhiteSpace(txtShpCompany.Text) Then
                lblShpCompany.CssClass = "redAsterick"
                isFormValid = False
            Else
                lblShpCompany.CssClass = ""
            End If

            If String.IsNullOrWhiteSpace(txtShpAttnName.Text) Then
                lblShpAttn.CssClass = "redAsterick"
                isFormValid = False
            Else
                lblShpAttn.CssClass = ""
            End If

            If String.IsNullOrWhiteSpace(txtShpAddr1.Text) Then
                lblShpAddr1.CssClass = "redAsterick"
                isFormValid = False
            ElseIf txtShpAddr1.Text.Length > 35 Then
                lblShpAddr1.CssClass = "redAsterick"
                isFormValid = False
                LabelLine1Star.Text = "<span class=""redAsterick"">" + GetGlobalResourceObject("Resource", "repair_svcacc_vald_msg") + "</span>"
                txtShpAddr1.Focus()
            Else
                lblShpAddr1.CssClass = ""
                LabelLine1Star.Text = "*"
            End If

            If txtShpAddr2.Text.Length > 35 Then
                lblShpAddr2.CssClass = "redAsterick"
                isFormValid = False
                LabelLine2Star.Text = "<span class=""redAsterick"">&nbsp;&nbsp;" + GetGlobalResourceObject("Resource", "repair_svcacc_vald_msg_2") + "</span>"
            Else
                lblShpAddr2.CssClass = ""
                LabelLine2Star.Text = ""
            End If

            If txtShpAddr3.Text.Length > 35 Then
                lblShpAddr3.CssClass = "redAsterick"
                isFormValid = False
                LabelLine3Star.Text = "<span class=""redAsterick"">&nbsp;&nbsp;" + GetGlobalResourceObject("Resource", "repair_svcacc_vald_msg_3") + "</span>"
            Else
                lblShpAddr3.CssClass = ""
                LabelLine3Star.Text = ""
            End If

            If String.IsNullOrWhiteSpace(txtShpCity.Text) Then
                lblShpCity.CssClass = "redAsterick"
                isFormValid = False
            ElseIf txtShpCity.Text.Length > 35 Then
                lblShpCity.CssClass = "redAsterick"
                isFormValid = False
                LabelLine4star.Text = "<span class=""redAsterick"">&nbsp;&nbsp;" + GetGlobalResourceObject("Resource", "repair_svcacc_vald_msg_4") + "</span>"
            Else
                lblShpCity.CssClass = ""
                LabelLine4star.Text = "*"
            End If


            If String.IsNullOrWhiteSpace(ddlShpState.SelectedValue.ToString()) Then
                lblShpState.CssClass = "redAsterick"
                isFormValid = False
            Else
                lblShpState.CssClass = ""
            End If

            If String.IsNullOrWhiteSpace(txtShpPhno.Text) Then
                lblShpPhno.CssClass = "redAsterick"
                isFormValid = False
            Else
                lblShpPhno.CssClass = ""
            End If

            If String.IsNullOrWhiteSpace(txtShpZip.Text) Then
                lblShpZip.CssClass = "redAsterick"
                isFormValid = False
            Else
                lblShpZip.CssClass = ""
            End If

            If Not IsValidPhone(txtShpPhno.Text) Then
                lblShpPhno.CssClass = "redAsterick"
                isFormValid = False
                lblErrmsgShpPhone.Visible = True
            Else
                lblShpPhno.CssClass = ""
                lblErrmsgShpPhone.Visible = False
            End If

            lblErrMsgShpFax.Visible = False
            lblShpFax.CssClass = ""
            If Not String.IsNullOrWhiteSpace(txtShpFax.Text) Then
                If Not IsValidPhone(txtShpFax.Text) Then
                    lblShpFax.CssClass = "redAsterick"
                    isFormValid = False
                    lblErrMsgShpFax.Visible = True
                End If
            End If

            lblErrMsgBillFax.Visible = False
            lblBillFax.CssClass = ""
            If Not String.IsNullOrWhiteSpace(txtBillFax.Text) And bShow = True Then
                If Not IsValidPhone(txtBillFax.Text) Then
                    lblBillFax.CssClass = "redAsterick"
                    isFormValid = False
                    lblErrMsgBillFax.Visible = True
                End If
            End If

            If String.IsNullOrWhiteSpace(txtPONumber.Text) And bShow = True Then
                lblPONumber.CssClass = "redAsterick"
                isFormValid = False
            Else
                lblPONumber.CssClass = ""
            End If

            If chkApproval.Checked = False And bShow = True Then
                lblApprvl.CssClass = "redAsterick"
                isFormValid = False
            Else
                lblApprvl.CssClass = ""
            End If

            'If chkPreapproval.Checked = True And Trim(txtPreApprvAmt.Text) = "" And bShow = True Then 'Commented for fixing Bug# 610
            '    lblPreApprvl.CssClass = "redAsterick"
            '    lblPreApprvlMsg.Visible = True
            '    bReturn = False
            'ElseIf chkPreapproval.Checked = True And Not IsNumeric(txtPreApprvAmt.Text.Trim()) And bShow = True Then 'Fix for Bug# 235
            '    lblPreApprvl.CssClass = "redAsterick"
            '    lblPreApprvlMsg.Visible = True
            '    bReturn = False
            'Else
            '    lblPreApprvlMsg.Visible = False
            '    lblPreApprvl.CssClass = ""
            'End If

            If String.IsNullOrWhiteSpace(txtContactName.Text) Then
                lblContactName.CssClass = "redAsterick"
                isFormValid = False
                lblContactName.Visible = True
            Else
                lblContactName.Visible = False
                lblContactName.CssClass = ""
            End If

            If String.IsNullOrWhiteSpace(txtEmailAddr.Text) Then
                lblEmailAddr.CssClass = "redAsterick"
                isFormValid = False
                lblErrMsgEmailAddr.Visible = True
            ElseIf Not Regex.IsMatch(txtEmailAddr.Text, rgxEmailAddress) Then
                lblEmailAddr.CssClass = "redAsterick"
                isFormValid = False
                lblErrMsgEmailAddr.Visible = True
            Else
                lblErrMsgEmailAddr.Visible = False
                lblEmailAddr.CssClass = ""
            End If

            ErrorValidation.Visible = Not isFormValid
            Return isFormValid
        End Function

        Private Sub PopulateBillShipDDL()
            Dim billToText As StringBuilder
            Dim customer As New Customer
            Dim sap_accounts As Boolean = False
            customer = Session.Item("customer")

            Try
                If bShow Then
                    Session("current_bill_to_list") = customer.SAPBillToAccounts

                    If customer.UserType = "A" Then
                        For Each billAccount As Account In customer.SAPBillToAccounts
                            billAccount = cm.PopulateCustomerDetailWithSAPAccount(billAccount)
                            billToText = New StringBuilder
                            If Not String.IsNullOrWhiteSpace(billAccount.AccountNumber) Then billToText.Append(billAccount.AccountNumber + "-")
                            If billAccount.Address IsNot Nothing Then
                                If Not String.IsNullOrWhiteSpace(billAccount.Address.Name) Then billToText.Append(" " + billAccount.Address.Name)
                                If Not String.IsNullOrWhiteSpace(billAccount.Address.Line1) Then billToText.Append(" " + billAccount.Address.Line1)
                                If Not String.IsNullOrWhiteSpace(billAccount.Address.Line2) Then billToText.Append(" " + billAccount.Address.Line2)
                            End If
                            ddlBillTo.Items.Add(New ListItem(billToText.ToString(), billAccount.AccountNumber))
                        Next
                    End If
                End If

                txtBillFax.Text = customer.FaxNumber
                txtShpAddr1.Text = customer.Address.Line1
                txtShpAddr2.Text = customer.Address.Line2
                'txtShpAddr3.Text = customer.Address.Line3
                txtShpCity.Text = customer.Address.City
                txtShpAttnName.Text = customer.FirstName + " " + customer.LastName
                txtShpCompany.Text = customer.CompanyName

                txtShpPhExtn.Text = customer.PhoneExtension
                txtShpPhno.Text = customer.PhoneNumber
                ddlShpState.Text = customer.Address.State
                txtShpZip.Text = customer.Address.PostalCode
                txtShpFax.Text = customer.FaxNumber
                txtEmailAddr.Text = customer.EmailAddress

                txtContactName.Text = customer.FirstName + " " + customer.LastName

                LabelLine1Star.Text = "*"
                LabelLine4star.Text = "*"
            Catch ex As Exception
                ErrorValidation.Visible = Utilities.WrapExceptionforUI(ex)
                ErrorValidation.Visible = True
            End Try
        End Sub

        Private Sub PopulateStatesDDL(ByVal objDDL As Object)
            'populateStateDropdownList(ddlStates)
            'Dim objGlobalData As New GlobalData
            'If Not (Session.Item("GlobalData")) Is Nothing Then
            '    objGlobalData = (Session.Item("GlobalData"))
            'End If
            Dim cm As New CatalogManager
            Dim statesdtls() As StatesDetail
            Dim iState As StatesDetail
            'If (objGlobalData IsNot Nothing) Then
            Try
                statesdtls = cm.StatesBySalesOrg(HttpContextManager.GlobalData)
                Dim newListItem1 As New ListItem()
                newListItem1.Text = GetGlobalResourceObject("Resource", "repair_ddl_sl_msg")
                newListItem1.Value = ""
                objDDL.Items.Add(newListItem1)

                For Each iState In statesdtls
                    Dim newListItem As New ListItem()
                    newListItem.Text = ChangeCase(iState.StateName)
                    newListItem.Value = iState.StateAbbr
                    objDDL.Items.Add(newListItem)
                Next
            Catch ex As Exception
                ErrorValidation.Visible = Utilities.WrapExceptionforUI(ex)
                ErrorValidation.Visible = True
                'Throw New Exception(ex.Message)
            End Try
            'End If
        End Sub
        Private Function ChangeCase(ByVal pString As String) As String
            Dim str As String = pString.ToLower()
            Return System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(str)
        End Function

        Protected Sub imgBtnNext_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgBtnNext.Click
            Dim oServiceInfo As ServiceItemInfo
            Dim oBillTo As New CustomerAddr
            Dim oShipTo As New CustomerAddr
            Dim objPCILogger As New PCILogger()
            Try
                customer = HttpContextManager.Customer

                objPCILogger.EventOriginApplication = "ServicesPLUS"
                objPCILogger.EventOriginApplicationLocation = Me.Page.GetType().Name
                objPCILogger.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
                objPCILogger.OperationalUser = Environment.UserName
                objPCILogger.EventDateTime = DateTime.Now.ToLongTimeString()
                objPCILogger.HTTPRequestObjectValues = objPCILogger.GetRequestContextValue()
                objPCILogger.IndicationSuccessFailure = "Success"
                objPCILogger.EventOriginMethod = "imgBtnNext_Click"
                objPCILogger.Message = "imgBtnNext_Click Success."
                objPCILogger.EventType = EventType.Add
                objPCILogger.CustomerID = customer.CustomerID
                objPCILogger.CustomerID_SequenceNumber = customer.SequenceNumber
                objPCILogger.EmailAddress = customer.EmailAddress
                objPCILogger.SIAM_ID = customer.SIAMIdentity
                objPCILogger.LDAP_ID = customer.LdapID

                If Not validateForm() Then
                    Return
                End If
                oServiceInfo = Session("snServiceInfo")
                oServiceInfo.ModelNumber = sModelName

                FindDepotCenterAddress(ddlShpState.SelectedValue.Trim(), oServiceInfo)
                If oServiceInfo.DepotCenterAddress = String.Empty Then
                    Response.Redirect("sony-service-centernotfound.aspx", True)
                Else
                    If bShow Then
                        current_bill_to_list = Session.Item("current_bill_to_list")
                        current_bill_to = CType(current_bill_to_list.GetValue(ddlBillTo.SelectedIndex()), Account)

                        oBillTo.Company = customer.CompanyName
                        oBillTo.AttnName = current_bill_to.Address.Name
                        oBillTo.Address1 = current_bill_to.Address.Line1
                        oBillTo.Address2 = current_bill_to.Address.Line2
                        oBillTo.Address3 = current_bill_to.Address.Line3
                        oBillTo.Address4 = current_bill_to.Address.Line4
                        oBillTo.City = current_bill_to.Address.City
                        oBillTo.Phone = customer.PhoneNumber
                        oBillTo.PhoneExtn = customer.PhoneExtension
                        oBillTo.State = current_bill_to.Address.State
                        oBillTo.ZipCode = current_bill_to.Address.PostalCode
                        oBillTo.Fax = txtBillFax.Text.Trim()

                        Session.Remove("current_bill_to")
                        Session("current_bill_to") = current_bill_to
                    End If

                    oServiceInfo.IsAccountHolder = True
                    oServiceInfo.BillToAddr = oBillTo

                    oServiceInfo.IsProductShipOnHold = Convert.ToBoolean(rdoShipOption.SelectedIndex)
                    oShipTo.Company = txtShpCompany.Text.Trim.Trim()
                    oShipTo.AttnName = txtShpAttnName.Text.Trim()
                    oShipTo.Address1 = txtShpAddr1.Text.Trim()
                    oShipTo.Address2 = txtShpAddr2.Text.Trim()
                    oShipTo.Address3 = "" 'txtShpAddr3.Text.Trim()
                    oShipTo.City = txtShpCity.Text.Trim()
                    oShipTo.Phone = txtShpPhno.Text.Trim()
                    oShipTo.PhoneExtn = txtShpPhExtn.Text.Trim()
                    oShipTo.State = ddlShpState.SelectedValue.Trim()
                    oShipTo.ZipCode = txtShpZip.Text.Trim()
                    oShipTo.Fax = txtShpFax.Text.Trim()

                    oServiceInfo.ShipToAddr = oShipTo

                    oServiceInfo.PONumber = txtPONumber.Text.Trim()
                    oServiceInfo.isApproval = chkApproval.Checked
                    oServiceInfo.isTaxExempt = chkTaxexempt.Checked
                    oServiceInfo.ShipStateAbbr = ddlShpState.SelectedValue.ToString()

                    oServiceInfo.PaymentType = rdPayment.SelectedValue
                    oServiceInfo.ContactEmailAddress = txtEmailAddr.Text.ToString().Trim()
                    oServiceInfo.ContactPersonName = txtContactName.Text.Trim()

                    Session("snServiceInfo") = oServiceInfo
                    Response.Redirect("sony-service-problem-desc.aspx?model=" + sModelName)
                End If
            Catch ex As Exception
                ErrorValidation.Visible = Utilities.WrapExceptionforUI(ex)
                ErrorValidation.Visible = True
                objPCILogger.IndicationSuccessFailure = "Failure" '6524
                objPCILogger.Message = "imgBtnNext_Click Failed. " & ex.Message.ToString() '6524
            Finally
                objPCILogger.PushLogToMSMQ() '6524
            End Try
        End Sub

        Private Sub FindDepotCenterAddress(ByVal pState As String, ByRef oServiceInfo As ServiceItemInfo)
            Dim cm As New CatalogManager
            Dim sAddr As String

            If pState.Length > 0 Then
                Try
                    sAddr = cm.GetDepotServiceCenter(pState, sModelName)
                    If Not String.IsNullOrWhiteSpace(sAddr) Then
                        Dim addressParts = sAddr.Replace("!", "<br/>").Split("#")
                        oServiceInfo.DepotCenterAddress = addressParts(0)
                        oServiceInfo.DepotCenterAddressID = Convert.ToInt32(addressParts(1))
                        oServiceInfo.DepotEmail = Convert.ToString(addressParts(2))
                    Else
                        'oServiceInfo.DepotCenterAddress = "For depot service for models not found here, please contact the Sony national service line at 866-766-9272, then select option 2."
                        oServiceInfo.DepotCenterAddress = String.Empty
                    End If
                Catch ex As Exception
                    oServiceInfo.DepotCenterAddress = ex.Message
                    oServiceInfo.ShipStateAbbr = ddlShpState.Text
                    Response.Redirect("sony-service-centernotfound.aspx", True)
                End Try
            Else
                oServiceInfo.DepotCenterAddress = "NA"
            End If
        End Sub

        ' ASleight - This is handled in SSL.aspx, which all pages inherit from
        'Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        '	If (HttpContextManager.SelectedLang IsNot Nothing) Then
        '		Dim ci As New CultureInfo(HttpContextManager.SelectedLang)
        '		Page.Culture = ci.ToString()
        '		Page.UICulture = ci.ToString()
        '	Else
        '		HttpContextManager.SelectedLang = "en-US"
        '		Page.Culture = "en-US"
        '		Page.UICulture = "en-US"
        '	End If
        'End Sub
    End Class
End Namespace
