<%@ Reference Page="~/SSL.aspx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="TopSonyHeader" Src="TopSonyHeader.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="SonyFooter" Src="SonyFooter.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="PersonalMessage" Src="PersonalMessage.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="ServicePlusNav" Src="ServicePlusNav.ascx" %>

<%@ Page Language="vb" AutoEventWireup="false" Inherits="ServicePLUSWebApp.training_tc" CodeFile="training-tc.aspx.vb" CodeFileBaseClass="ServicePLUSWebApp.SSL" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="<%=PageLanguage %>">
<head>
    <title>Sony Training Institute - Term & Condition</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

    <link href="includes/ServicesPLUS_style.css" type="text/css" rel="stylesheet">
    <script type="text/javascript" src="includes/ServicesPLUS.js"></script>
    <script type="text/javascript" src="includes/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="includes/svcplus_globalization.js"></script>
</head>
<body style="margin: 0px; background: #5d7180; color: Black;">
    <form id="Form1" method="post" runat="server">
        <center>
            <table width="760" border="0" role="presentation">
                <tr>
                    <td width="25" style="background: url('images/sp_left_bkgd.gif')">
                        <img height="25" src="images/spacer.gif" width="25" alt="">
                    </td>
                    <td width="710" bgcolor="#ffffff">
                        <table width="710" border="0" role="presentation">
                            <tr>
                                <td>
                                    <ServicePLUSWebApp:TopSonyHeader ID="TopSonyHeader" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <nav><ServicePLUSWebApp:ServicePlusNav ID="SonyNav" runat="server" /></nav>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="710" border="0" role="presentation">
                                        <tr>
                                            <td width="464" bgcolor="#363d45" height="57" background="images/sp_int_header_top_ServicesPLUS_onepix.gif"
                                                align="right" valign="top">
                                                <br>
                                                <h1 class="headerText">ServicesPLUS &nbsp;&nbsp;</h1>
                                            </td>
                                            <td valign="top" bgcolor="#363d45">
                                                <ServicePLUSWebApp:PersonalMessage ID="PersonalMessageDisplay" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td bgcolor="#f2f5f8">
                                                <h2 class="headerTitle" style="padding-right: 20px; text-align: right;">Terms and Conditions</h2>
                                            </td>
                                            <td bgcolor="#99a8b5">&nbsp;</td>
                                        </tr>
                                        <tr height="9">
                                            <td bgcolor="#f2f5f8">
                                                <img height="9" src="images/sp_int_header_btm_left_onepix.gif" width="464" alt="">
                                            </td>
                                            <td bgcolor="#99a8b5">
                                                <img height="9" src="images/sp_int_header_btm_right.gif" width="246" alt="">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="600" border="0" class="bodyCopy" role="presentation">
                                        <tr>
                                            <td>
                                                <img height="29" src="images/spacer.gif" width="15" alt="">
                                            </td>
                                            <td align="center"><span class="bodyCopyBold">PARTS</span></td>
                                        </tr>
                                        <tr>
                                            <td style="height: 706px">
                                                <img height="29" src="images/spacer.gif" width="15" alt="">
                                            </td>
                                            <td style="height: 706px">
                                                <p>
                                                    <span class="bodyCopyBold">NOTIFICATION OF CLAIMS: WARRANTY SERVICE:</span>
                                                    If Purchaser discovers that any replacement part purchased 
															through the Parts Operations ("Parts") has proven defective in material or 
															workmanship, then written notice with an explanation of the claim shall be 
															given promptly by Purchaser to Sony but all claims for warranty service must be 
															made within the warranty period. Sony will charge the Purchaser for replacement 
															parts and will credit the Purchaser if Sony determines that the parts are 
															covered by the warranty after the defective unit has been returned. If after 
															investigation Sony determines that the reported problem was not covered by the 
															warranty, we reserve the right to charge the Purchaser for the cost of 
															investigating the problem at its then prevailing time-and-materials rate. No 
															replacement by Purchaser of any part thereof shall extend the warranty period 
															as to the entire Product for which the Part is used. The specific warranty on 
															the replacement part only shall be in effect for a period of ninety (90) days 
															following its shipment, except where indicated above.
                                                </p>
                                                <p>
                                                    <span class="bodyCopyBold">EXCLUSIVE REMEDY: ACCEPTANCE:</span>&nbsp;Purchaser's 
														exclusive remedy and Sony's sole obligation is to supply a new or rebuilt 
														replacement part, or issue a credit towards the purchase thereof, for any 
														defective part covered by this warranty, failing which Sony shall refund to 
														Purchaser the purchase price for such part. Purchaser's failure to make a claim 
														as provided in paragraph 1 above or continued use of the Part shall constitute 
														an unqualified acceptance of such Part and a waiver by Purchaser of claims 
														thereto.&nbsp;
                                                </p>
                                                <p>
                                                    <span class="bodyCopyBold">EXCEPTIONS TO LIMITED WARRANTY:</span>&nbsp;Sony 
														shall have no liability or obligation to Purchaser with respect to any Part 
														requiring service during the warranty period which is subjected to the 
														following: abuse; improper use: negligence, accident, modification, failure of 
														the end-user to follow the operating procedures outlined in the user's manual: 
														failure of the end-user to follow the maintenance procedures in the service 
														manual for the product in which the Part is used where a schedule is specified 
														for regular replacement or maintenance or cleaning of certain parts (based on 
														usage) and the end-user has failed to follow such schedule; attempted repair by 
														non-qualified personnel; operation of unit outside of the published environment 
														and electrical parameters, or if such Parts original identification (e.g., 
														trademark, serial number) markings have been defaced, altered or removed. Sony 
														excludes from warranty coverage Parts sold AS IS and /or WITH ALL FAULTS and 
														excludes used Parts which have not been sold by Sony to the Purchaser. Sony 
														also excludes from warranty coverage Parts located outside of the United States 
														and Puerto Rico, and consumable items such as fuses and batteries.&nbsp;
                                                </p>
                                                <p>
                                                    <span class="bodyCopyBold">DISCLAIMER OF WARRANTY:</span>&nbsp;EXCEPT 
															FOR THE FOREGOING WARRANTIES, SONY HEREBY DISCLAIMS AND EXCLUDES ALL OTHER 
															WARRANTIES, EXPRESS OR IMPLIED INCLUDING, BUT NOT LIMITED TO ANY AND/OR ALL 
															IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND/OR 
															ANY WARRANTY WITH REGARD TO ANY CLAIM OF INFRINGEMENT THAT MAY BE PROVIDED IN 
															SECTION 2-312(3) OF THE UNIFORM COMMERCIAL CODE AND/OR IN ANY OTHER COMPARABLE 
															STATE STATUE. SONY HEREBY DISCLAIMS ANY REPRESENTATIONS OR WARRANTY THAT THE 
															PART IS COMPATIBLE WITH ANY COMBINATION OF NON-SONY PRODUCTS TO WHICH THE 
															PURCHASER MAY CONNECT THE PART.
                                                </p>
                                                <p>
                                                    <span class="bodyCopyBold">LIMITATION OF LIABILITY:</span>&nbsp;THE 
															LIABILITY OF SONY, IF ANY, AND PURCHASER'S SOLE AND EXCLUSIVE REMEDY FOR 
															DAMAGES FOR ANY CLAIM OF ANY KIND WHATSOEVER, REGARDLESS OF THE LEGAL THEORY 
															AND WHETHER ARISING IN TORT OR CONTRACT, SHALL NOT BE GREATER THAN THE ACTUAL 
															PURCHASE PRICE OF THE PART WITH RESPECT TO WHICH SUCH CLAIM IS MADE. IN NO 
															EVENT SHALL SONY BE LIABLE TO PURCHASER FOR ANY SPECIAL, INDIRECT, INCIDENTAL, 
															OR CONSEQUENTIAL DAMAGES OF ANY KIND INCLUDING, BUT NOT LIMITED TO 
															COMPENSATION, REIMBURSEMENT OR DAMAGES ON ACCOUNT OF THE LOSS OF PRESENT OR 
															PROSPECTIVE PROFITS OR FOR ANY OTHER REASON WHATSOEVER.
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <img height="29" src="images/spacer.gif" width="15" alt="">
                                            </td>
                                            <td align="center"><span class="bodyCopyBold">SOFTWARE</span></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <img height="29" src="images/spacer.gif" width="15" alt="">
                                            </td>
                                            <td>
                                                <p class="bodyCopyBold">Software License Agreement:</p>
                                                <p>
                                                    <span class="bodyCopyBold">IMPORTANT - READ CAREFULLY:</span>
                                                    This End-User License Agreement ("License") is a legal agreement between you 
													and Sony Corporation ("SONY"), the manufacturer of your SOFTWARE. The SOFTWARE 
													includes computer software, and any associated media and printed materials. You 
													may use the SOFTWARE only in connection with the use of the personal computer 
													(hereinafter referred to as SYSTEM). By installing, copying or otherwise using 
													the SOFTWARE, you agree to be bound by the terms of this License. If you do not 
													agree to the terms of this License, SONY is unwilling to license the SOFTWARE 
													to you. In such event, you may not use or copy the SOFTWARE, and you should 
													promptly contact SONY for instructions on return of the unused product for a 
													refund of the purchase price of the SOFTWARE.
                                                </p>
                                                <p></p>
                                                <p>
                                                    <span class="bodyCopyBold">SOFTWARE LICENSE:&nbsp;</span>The SOFTWARE is 
													protected by copyright laws and international copyright treaties, as well as 
													other intellectual property laws and treaties. The SOFTWARE is licensed, not 
													sold.
                                                </p>
                                                <p>
                                                    <span class="bodyCopyBold">GRANT OF LICENSE:</span> This License grants you the 
														following rights:<br />
                                                    - Software. You may install and use one (1) copy of the SOFTWARE.<br />
                                                    - Back-up Copy. You may use a back-up copy solely for archival purposes.
                                                </p>
                                                <p>
                                                    <span class="bodyCopyBold">MPEG-4 VISUAL PATENT PORTOFOLIO LICENSE: </span>MPEG-4 
														Encoding products are licensed under the MPEG-4 Visual Patent Portfolio 
														License(i) for the personal and non-commercial use of a consumer for (i) 
														encoding video in compliance with the MPEG-4 visual standard ("MPEG-4 Video") 
														and/or (ii) decoding MPEG-4 video that was encoded by a consumer engaged in a 
														personal and non-commercial activity and/or was obtained from a video provider 
														licensed by MPEG LA to provide MPEG-4 video. No license will be granted or 
														shall be implied for any other use. Additional information including that 
														relating to promotional, internal and commercial uses and licensing may be 
														obtained from MPEG LA, LLC. See http://www.mpegla.com for more information.
                                                </p>
                                                <p>
                                                    <span class="bodyCopyBold">DESCRIPTION OF OTHER RIGHTS LIMITATIONS</span><br />
                                                    - Limitation on Reverse Engineering, Decompilation and Disassembly. You may not 
														modify, reverse engineer, decompile, or disassemble the SOFTWARE in whole or in 
														part.<br />
                                                    - Single SYSTEM. The SOFTWARE is licensed with the SYSTEM as a single 
														integrated product. The SOFTWARE may only be used with the SYSTEM specified by 
														SONY.<br />
                                                    - Rental. You may not rent or lease the SOFTWARE.<br />
                                                    - Software Transfer. You may transfer all of your rights under this License 
														only as part of a sale or transfer of the SYSTEM specified by SONY, provided 
														you retain no copies, transfer all of the SOFTWARE (including all copies, 
														component parts, the media and printed materials, all versions and any upgrades 
														of the SOFTWARE and this License), and the recipient agrees to the terms of 
														this License.<br />
                                                    - Termination. Without prejudice to any other rights, SONY may terminate this 
														License if you fail to comply with the terms and conditions of this License. In 
														such event, you must destroy all copies of the SOFTWARE and all of its 
														component parts.
                                                </p>
                                                <p>
                                                    <span class="bodyCopyBold">COPYRIGHT:</span>
                                                    All title and copyrights in and to the SOFTWARE (including but not 
													limited to any images, photographs, animation, video, audio, music, text and 
													"applets", incorporated into the SOFTWARE), and any copies of the SOFTWARE, are 
													owned by SONY or its suppliers. All rights not specifically granted under this 
													License are reserved by SONY.
                                                </p>
                                                <p>
                                                    <span class="bodyCopyBold">PRODUCT SUPPORT:</span>
                                                    Product support for the SOFTWARE is not provided by SONY.
                                                </p>
                                                <p>
                                                    <span class="bodyCopyBold">LIMITED WARRANTY FOR CD-ROM MEDIA:</span>
                                                    SONY warrants that for a period of ninety (90) days from the date of its 
													delivery to you the CD-ROM media on which the copy of the SOFTWARE is furnished 
													to you will be free from defects in materials and workmanship under normal use. 
													This limited warranty extends only to you as the original licensee. SONY's 
													entire liability and your exclusive remedy will be replacement of the CD-ROM 
													media not meeting SONY's limited warranty and which is returned to SONY with 
													proof of purchase in the form of a bill of sale (which is evidence that the 
													CD-ROM media is within the warranty period). SONY will have no responsibility 
													to replace a disk damaged by accident, abuse or misapplication.
                                                </p>
                                                <p>
                                                    <span class="bodyCopyBold">EXCLUSION OF WARRANTY ON SONY SOFTWARE:</span>
                                                    You expressly acknowledge and agree that use of the SOFTWARE is at your 
												    sole risk. The SOFTWARE is provided "AS IS" and without warranty of any kind 
												    and SONY and its representative (hereinafter collectively referred to as SONY) 
												    EXPRESSLY DISCLAIM ALLWARRANTIES, EXPRESS OR IMPLIED, INCLUDING, BUT NOT 
												    LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY FITNESS FOR A PARTICULAR 
												    PURPOSE. SONY DOES NOT WARRANT THAT THE FUNCTIONS CONTAINED IN THE SOFTWARE 
												    WILL MEET YOUR REQUIREMENTS, OR THAT THE OPERATION OF THE SOFTWARE WILL BE 
												    CORRECTED. FURTHERMORE, SONY DOES NOT WARRANT OR MAKE ANY REPRESENTATIONS 
												    REGARDING THE USE OR THE RESULTS OF THE USE OF THE SOFTWARE IN TERMS OF ITS 
												    CORRECTNESS, ACCURACY, RELIABILITY, OR OTHERWISE. NO ORAL OR WRITTEN 
												    INFORMATION OR ADVICE GIVEN BY SONY SHALL CREATE A WARRANTY OR IN ANY WAY 
												    INCREASE THE SCOPE OF THIS WARRANTY. SHOULD THE SOFTWARE PROVE DEFECTIVE, YOU 
												    (NOT SONY) ASSUME THE ENTIRE COST OF ALL NECESSARY SERVICING, REPAIR OR 
												    CORRECTION.
                                                </p>
                                                <p>
                                                    <span class="bodyCopyBold">LIMITATION OF LIABILITY:</span> SONY SHALL NOT BE 
													LIABLE FOR ANY INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR BREACH OF ANY EXPRESS OR 
													IMPLIED WARRANTY, BREACH OF CONTRACT, NEGLIGENCE, STRICT LIABILITY OR ANY OTHER 
													LEGAL THEORY RELATED TO THIS PRODUCT. SUCH DAMAGES INCLUDE, BUT ARE NOT LIMITED 
													TO, LOSS OF PROFITS, LOSS OF REVENUE, LOSS OF DATA, LOSS OF USE OF THE PRODUCT 
													OR ANY ASSOCIATED EQUIPMENT, DOWN TIME AND PURCHASER'S TIME, EVEN IF SONY HAS 
													BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES. IN ANY CASE, SONY'S ENTIRE 
													LIABILITY UNDER ANY PROVISION OF THIS AGREEMENT SHALL BE LIMITED TO THE AMOUNT 
													ACTUALLY PAID BY YOU ALLOCABLE TO THE SOFTWARE.
                                                </p>
                                                <p>&nbsp;</p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <img height="29" src="images/spacer.gif" width="15" alt="">
                                            </td>
                                            <td align="center">
                                                <span class="bodyCopyBold">TRAINING</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <img height="29" src="images/spacer.gif" width="15" alt="">
                                            </td>
                                            <td>
                                                <p>
                                                    <span class="bodyCopyBold">HOTEL/GROUND TRANSPORTATION:&nbsp;</span>Students 
													are responsible for their own travel and hotel arrangements. With your 
													registration confirmation you'll receive a lodging list. It is strongly 
													recommended that hotel reservations be made at least two weeks prior to your 
													training class. Please note that lodging and meals are not included in the 
													class registration fee.
                                                </p>
                                                <p>
                                                    <span class="bodyCopyBold">TRANSFERS/CANCELLATION POLICY:&nbsp;</span>If it is 
													necessary to cancel your attendance at a course for which you are registered, 
													you must cancel your enrollment, in writing, at least three (3) weeks prior to 
													the start of the course. This will entitle you to a full reimbursement of the 
													tuition fee. A 25% cancellation fee will be charged for registration cancelled 
													within three (3) weeks of the course, or you may transfer the tuition to 
													another course offered within six (6) months and there will be no penalty. This 
													transfer may be made only once to avoid the 25% cancellation fee. "No Shows" or 
													cancellations received on or after the first day of class are billed at 50% of 
													the course's published fee.
                                                </p>
                                                <p>
                                                    **Dates and locations are subject to change without notice. Please contact us to 
													confirm scheduling before making final travel arrangements. Non-refundable 
													airfares are not recommended. Prices are subject to change at anytime. We will 
													notify you if there is a price change upon receipt of your order.
                                                </p>
                                                <p>&nbsp;</p>
                                            </td>
                                        </tr>
                                    </table>
                                    <span class="legalCopy">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="legalLink" href="http://pro.sony.com/bbsccms/services/files/servicesprograms/WARNTY2f.pdf" target="_blank">Limited 
												Warranty</a> </span>
                                    <br>
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <br />
                                    <asp:ImageButton ID="Accept" runat="server" AlternateText="Accept" ImageUrl="images/sp_int_accept_btn.gif" />&nbsp;&nbsp;&nbsp;
									<asp:ImageButton ID="Decline" runat="server" AlternateText="Decline" ImageUrl="images/sp_int_decline_btn.gif" />
                                </td>
                            </tr>
                            <tr>

                                <td>
                                    <footer><ServicePLUSWebApp:SonyFooter ID="SonyFooter1" runat="server" /></footer>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="25" style="background: url('images/sp_right_bkgd.gif')">
                        <img height="20" src="images/spacer.gif" width="25" alt="">
                    </td>
                </tr>
            </table>
        </center>
    </form>
</body>
</html>
