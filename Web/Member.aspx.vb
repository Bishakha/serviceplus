Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Process
Imports Sony.US.ServicesPLUS.Data
Imports Sony.US.siam
Imports System.Xml
Imports System.Text
Imports System.IO
Imports ServicesPlusException
Imports Sony.US.SIAMUtilities
Imports Sony.US.ServicesPLUS
Imports Sony.US.AuditLog
Imports System.Collections.Generic
Imports System.Linq
Imports System.Globalization

Namespace ServicePLUSWebApp


    Partial Class Member
        Inherits SSL

        Public isCanada As Boolean = False
        Public isFrench As Boolean = False
        ''Dim objUtilties As Utilities = New Utilities
        Protected WithEvents Label1 As System.Web.UI.WebControls.Label

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub


        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                isCanada = HttpContextManager.GlobalData.IsCanada
                isFrench = HttpContextManager.GlobalData.IsFrench

                Session.Remove("RegisterReturnPage")
                Session.Remove("RegisterReturnPageQS")
                AddToCartAfterLogin(ErrorLabel)

                ProcessPage()
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Sub fnLogin(ByVal strEmail As String, ByVal strLdapID As String)
            Dim cm As New Sony.US.ServicesPLUS.Process.CustomerManager
            Dim customer As Customer
            Dim sFirstName As String = String.Empty
            Dim sLastName As String = String.Empty
            Dim emailAddress As String = cm.GetActiveCustomerEmailAddress(strLdapID)

            If Not String.IsNullOrEmpty(emailAddress) Then
                If emailAddress.ToUpper().Trim() <> strEmail.ToUpper().Trim() Then
                    Dim ht As New Hashtable
                    ht.Add("emailAddress", strEmail)
                    ht.Add("ldapId", strLdapID)
                    Session("EmailIdNotMatched") = ht
                    Response.Redirect("default.aspx", False)
                    HttpContext.Current.ApplicationInstance.CompleteRequest()
                    Return
                End If
            End If

            customer = cm.GetCustomer(strLdapID, strEmail, sFirstName, sLastName)

            If customer Is Nothing Then
                Dim c As New Customer
                'Session.Add("NewUserData", sFirstName + "*" + sLastName + "*" + sEmailID) ' 6909 v9
                If Session("existingSMcustomer") IsNot Nothing Then
                    c = Session("existingSMcustomer")
                Else
                    c.EmailAddress = strEmail
                    c.FirstName = sFirstName
                    c.LastName = sLastName
                    c.UserName = strEmail
                    Dim admin As New SecurityAdministrator
                    Dim data As String = String.Empty
                    Try
                        data = admin.SearchUser(strEmail)
                    Catch ex As Exception
                        Session.Add("HOMEPAGE", ex)
                        Dim homePage As String = String.Empty

                        If HttpContextManager.GlobalData.SalesOrganization = ConfigurationData.GeneralSettings.GlobalData.SalesOrganization.CA Then
                            homePage = ConfigurationData.Environment.IdentityURLs.LogoutENCA
                        Else
                            homePage = ConfigurationData.Environment.IdentityURLs.LogoutUS
                        End If

                        Response.Redirect(homePage, False)
                        Return
                    End Try
                    Dim dataarr As String() = data.Split("*"c)
                    c.CompanyName = dataarr(2)
                End If
                c.LdapID = strLdapID
                Session.Add("customer", c)
                Response.Redirect("shared/Register-Existing-User.aspx", False)
                HttpContext.Current.ApplicationInstance.CompleteRequest()
            Else
                Session.Add("StatusCode", customer.StatusCode)
                If (customer.StatusCode = 3) Then
                    Session.Add("customer", customer)
                    Response.Redirect("shared/Register-Existing-User.aspx", False)
                    HttpContext.Current.ApplicationInstance.CompleteRequest()
                ElseIf (customer.StatusCode = 1) Then
                    If customer.LdapID.Trim() <> strLdapID.Trim() Then
                        cm.UpdateLdapid(strLdapID.Trim(), customer.CustomerID)
                    End If

                    'If customer.UserType = "AccountHolder" Then'6668
                    If customer.UserType = "A" Or customer.UserType = "P" Then '6668
                        Dim objCoreAccount As Account = Nothing
                        Dim iIndex As Integer = 0
                        For Each objAccount As Account In customer.SAPBillToAccounts
                            objCoreAccount = cm.PopulateCustomerDetailWithSAPAccount(objAccount, HttpContextManager.GlobalData)
                            If Not objCoreAccount Is Nothing Then
                                customer.SAPBillToAccounts(iIndex).SoldToAccount = objCoreAccount.SoldToAccount
                            End If

                            iIndex = iIndex + 1
                        Next
                    End If
                    If Not String.IsNullOrEmpty(Request.QueryString("SessionID")) Then
                        Dim pOutDataMgr As PunchOutDataManager = New PunchOutDataManager()
                        customer.IsPunchOutUser = pOutDataMgr.IsPunchOutSession(Request.QueryString("SessionID"))
                        If customer.IsPunchOutUser Then
                            Session("PunchOutSesison") = Request.QueryString("SessionID")
                        End If
                    End If
                    Session.Add("customer", customer)
                End If
            End If

        End Sub

        ''' <summary>
        ''' Compares the current site domain to the user's country as stored in the database.
        ''' If they don't match, it will initiate a Redirect.
        ''' </summary>
        ''' <returns>False if the user is being redirected due to site mismatch.</returns>
        Private Function IsCorrectCountrySite() As Boolean
            Dim catMan As New CatalogManager
            Dim customer As Customer
            Dim siteCountryCode As String = String.Empty
            Dim newURL As String = String.Empty
            Dim isCanada As Boolean = False
            Dim retVal As Boolean = True

            If (HttpContextManager.Customer IsNot Nothing) And (ConfigurationData.GeneralSettings.Environment <> "Development") Then
                customer = HttpContextManager.Customer
                ' Sets siteCountryCode to 'US' or 'CA'
                'catMan.CountryBySalesOrg(HttpContextManager.GlobalData, siteCountryCode)
                siteCountryCode = IIf(HttpContextManager.DomainURL.ToLower().Contains("sony.ca"), "CA", "US")
                ' If the user's country doesn't match this site's country, put them in the right place.
                If (Not String.IsNullOrEmpty(customer.CountryCode)) AndAlso (customer.CountryCode <> siteCountryCode) Then
                    retVal = False
                    Utilities.LogMessages($"User is visiting the wrong country site. LDAP: {customer.LdapID}, User Country: {customer.CountryCode}, Site Country: {siteCountryCode}")
                    If customer.CountryCode = "CA" Then
                        newURL = ConfigurationData.Environment.URL.CA
                    Else
                        newURL = ConfigurationData.Environment.URL.US
                    End If
                    newURL = newURL + "/member.aspx?UserData=" + customer.SIAMIdentity
                    Response.Redirect(newURL, False)
                    HttpContext.Current.ApplicationInstance.CompleteRequest()
                End If
            End If
            Return retVal
        End Function


        Private Sub ProcessPage()
            Try
                Dim cm As New CustomerManager
                Dim siamID As String = Nothing
                Dim customer As Customer = Nothing
                Dim blGetData As Boolean = False

                'Login based on query string 
                If (Request.QueryString("UserData") IsNot Nothing) And (ConfigurationData.GeneralSettings.Environment <> "Development") Then
                    siamID = Request.QueryString("UserData")
                    blGetData = True
                    ' If SIAM ID doesn't match the stored Customer, we need to clear storage so we don't log back into the "old" account.
                    If (HttpContextManager.Customer IsNot Nothing) AndAlso (siamID <> HttpContextManager.Customer.SIAMIdentity) Then
                        HttpContextManager.Customer = Nothing
                    End If
                End If

                If (HttpContextManager.Customer IsNot Nothing) Then
                    customer = HttpContextManager.Customer
                Else
                    If (String.IsNullOrEmpty(siamID)) And (Session.Item("siamID") IsNot Nothing) Then
                        siamID = Session.Item("siamID")
                    End If

                    If (Not String.IsNullOrEmpty(siamID)) Then
                        customer = cm.GetCustomer(siamID)
                        'get active status for customer now instead of getting every time later
                        IsUserActive(customer)
                        HttpContextManager.Customer = customer
                    End If
                End If

                If customer Is Nothing Then Response.Redirect("default.aspx", True)

                'Get Account details from LDAP 
                If blGetData Then
                    If customer.UserType = "A" Or customer.UserType = "P" Then
                        Dim objCoreAccount As Account = Nothing
                        Dim iIndex As Integer = 0
                        Try
                            Dim strInvalidAcct As String = String.Empty
                            Dim strCustAcct As String = customer.CustomerID
                            For Each objAccount As Account In customer.SAPBillToAccounts
                                objCoreAccount = cm.PopulateCustomerDetailWithSAPAccount(objAccount, HttpContextManager.GlobalData)
                                If objCoreAccount IsNot Nothing Then
                                    If objCoreAccount.SoldToAccount Is Nothing Or objCoreAccount.AccountNumber Is Nothing Then
                                        If strInvalidAcct.Length > 0 Then
                                            strInvalidAcct &= "," & objAccount.AccountNumber
                                        Else
                                            strInvalidAcct = objAccount.AccountNumber
                                        End If
                                    Else
                                        customer.SAPBillToAccounts(iIndex).SoldToAccount = objCoreAccount.SoldToAccount
                                        strCustAcct += "�" + objCoreAccount.SoldToAccount
                                    End If
                                End If
                                iIndex = iIndex + 1
                            Next
                            'Session.Add("CustomerAccnt", strCustAcct)
                            'Remove the invalid account 
                            If strInvalidAcct.Length > 0 Then
                                For Each strData As String In strInvalidAcct.Split(",")
                                    'Dim i As Integer = 0
                                    For Each objAccount As Account In customer.SAPBillToAccounts
                                        'i = i + 1
                                        If (objAccount.AccountNumber = strData) Then
                                            customer.RemoveAccount(objAccount)
                                            Exit For
                                        End If
                                    Next
                                Next
                            End If
                        Catch ex As Exception

                        End Try
                    End If

                    HttpContextManager.Customer = customer
                End If
                ' If the user is visiting the wrong country site, this method will redirect them.
                If Not IsCorrectCountrySite() Then
                    Return
                End If

                LabelMemberName.Text = customer.FirstName + " " + customer.LastName
                'check for free download software
                If Session("SelectedFreeSoftwareBeforeLoggedIn") IsNot Nothing Then
                    Response.Redirect("sony-foc-software-tc.aspx?ln=-1", True)
                ElseIf Request.QueryString("CourseLineno") IsNot Nothing Then
                    RedirectCourse(Convert.ToInt16(Request.QueryString("CourseLineno")))
                End If
            Catch ex As Exception
                Utilities.WrapExceptionforUI(ex)
            End Try

        End Sub

        Private Sub checkPasswordExpirationDate(ByRef thisCustomer As Customer)
            If Not thisCustomer Is Nothing Then
                Try
                    Dim thisSA As New SecurityAdministrator
                    Dim thisDate As Date = thisSA.GetUser(thisCustomer.SIAMIdentity).TheUser.PasswordExpirationDate
                    Dim daysUntilExpiration As Int32

                    If IsDate(thisDate) Then
                        daysUntilExpiration = Date.op_Subtraction(thisDate, Date.Now).Days
                        If daysUntilExpiration < 6 Then
                            '-- prompt the user to change password -
                            RegisterClientScriptBlock("ChangePassword", "<script language=""javascript"">launchWin4('ChangePasswordPopUp.aspx','400','400');</script>")

                            '-- show user change password message -
                            If daysUntilExpiration = 0 Then
                                PasswordExpirationLabel.Text = "Your password will expire today. To change your password log out and change it from the Login screen."
                            Else
                                PasswordExpirationLabel.Text = "Your password will expire in " + daysUntilExpiration.ToString() + " days. To change your password log out and change it from the Login screen."
                            End If
                        End If
                    End If
                Catch ex As Exception
                    '-- need to do something with this.
                End Try
            End If

        End Sub

        Private Sub RedirectCourse(ByVal courseNumber As Integer)
            Dim customer As Customer
            If HttpContextManager.Customer Is Nothing Then Return

            customer = HttpContextManager.Customer

            Dim courseSelected As CourseSchedule = CType(Session.Item("courseschedule"), ArrayList)(courseNumber)

            Dim cr As CourseReservation = New CourseReservation
            cr.CourseSchedule = courseSelected
            Session("trainingreservation") = cr

            Dim nonAccount = True

            ' ASleight - These two loops aren't well-designed. Figure out the intended logic and improve.
            For Each sap As Account In customer.SAPBillToAccounts
                'If sap.Validated Then
                If customer.UserType = "A" Then
                    nonAccount = False
                End If
            Next

            For Each sis As SISAccount In customer.SISLegacyBillToAccounts
                'If sis.Validated Then
                If customer.UserType = "A" Then
                    nonAccount = False
                End If
            Next

            If nonAccount And IsUserActive(customer) Then
                Response.Redirect("training-payment-naccnt.aspx", True)
            Else
                Response.Redirect("training-payment-accnt.aspx", True)
            End If
        End Sub

        Private Sub setSession()
            Dim sFeature As String = "scrollbars=1,resizable=0,width=744,height=754,left=0,top=0"
            Dim sUserIdentity As String = String.Empty
            Dim sEmailID As String = String.Empty
            Dim sFirstName As String = String.Empty
            Dim sLastName As String = String.Empty
            Dim sCompanyName As String = String.Empty
            'Dim hInfo As String = String.Empty
            Dim objPCILoggerForCustomer As New PCILogger()
            Dim cm As New Sony.US.ServicesPLUS.Process.CustomerManager
            Dim customer As Customer

            Try
                '-- lets clear the session variables --
                Session.Remove("carts")
                Session.Remove("order")
                HttpContextManager.Customer = Nothing

                objPCILoggerForCustomer.EventOriginApplication = "ServicesPLUS"
                objPCILoggerForCustomer.EventOriginApplicationLocation = Me.Page.GetType().Name
                objPCILoggerForCustomer.EventOriginMethod = "setSession()"
                objPCILoggerForCustomer.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
                objPCILoggerForCustomer.OperationalUser = Environment.UserName
                objPCILoggerForCustomer.EventType = EventType.Login
                objPCILoggerForCustomer.EventDateTime = DateTime.Now.ToLongTimeString()
                objPCILoggerForCustomer.HTTPRequestObjectValues = objPCILoggerForCustomer.GetRequestContextValue()
                objPCILoggerForCustomer.IndicationSuccessFailure = "Success"
                objPCILoggerForCustomer.Message = "Login Success"

                '2016-04-29 ASleight Shibboleth returns ID headers with different names than SiteMinder
                If (HttpContext.Current.Request.ServerVariables("HTTP_IDP_UID") IsNot Nothing) Then
                    sUserIdentity = HttpContext.Current.Request.ServerVariables("HTTP_IDP_UID")
                    sEmailID = HttpContext.Current.Request.ServerVariables("HTTP_IDP_MAIL")
                    sFirstName = HttpContext.Current.Request.ServerVariables("HTTP_IDP_GIVENNAME")
                    sLastName = HttpContext.Current.Request.ServerVariables("HTTP_IDP_SN")
                End If
                'Utilities.fnLogException(New Exception("USERID :" + sUserIdentity))

                Dim emailAddress As String = cm.GetActiveCustomerEmailAddress(sUserIdentity)

                If Not String.IsNullOrEmpty(emailAddress) Then
                    If emailAddress.ToUpper().Trim() <> sEmailID.ToUpper().Trim() Then
                        Dim ht As New Hashtable
                        ht.Add("emailAddress", sEmailID)
                        ht.Add("ldapId", sUserIdentity)
                        Session("EmailIdNotMatched") = ht
                        Response.Redirect("default.aspx", False)
                        HttpContext.Current.ApplicationInstance.CompleteRequest()
                        Return
                    End If
                End If

                customer = cm.GetCustomer(sUserIdentity, sEmailID, sFirstName, sLastName)

                If (customer IsNot Nothing) Then
                    HttpContextManager.Customer = customer
                    objPCILoggerForCustomer.CustomerID = customer.CustomerID
                    objPCILoggerForCustomer.CustomerID_SequenceNumber = customer.SequenceNumber
                    objPCILoggerForCustomer.EmailAddress = customer.EmailAddress
                    objPCILoggerForCustomer.SIAM_ID = customer.SIAMIdentity
                    objPCILoggerForCustomer.LDAP_ID = customer.LdapID
                    Session.Add("StatusCode", customer.StatusCode)
                    If (customer.StatusCode = 3) Then
                        Response.Redirect("shared/Register-Existing-User.aspx", False)
                        HttpContext.Current.ApplicationInstance.CompleteRequest()
                    ElseIf (customer.StatusCode = 1) Then
                        If customer.LdapID.Trim() <> sUserIdentity.Trim() Then
                            cm.UpdateLdapid(sUserIdentity.Trim(), customer.CustomerID)
                        End If

                        If customer.UserType = "A" Or customer.UserType = "P" Then '6668
                            Dim objCoreAccount As Account = Nothing
                            Dim iIndex As Integer = 0
                            For Each objAccount As Account In customer.SAPBillToAccounts
                                objCoreAccount = cm.PopulateCustomerDetailWithSAPAccount(objAccount, HttpContextManager.GlobalData)
                                If Not objCoreAccount Is Nothing Then
                                    customer.SAPBillToAccounts(iIndex).SoldToAccount = objCoreAccount.SoldToAccount
                                End If
                                iIndex = iIndex + 1
                            Next
                        End If
                        If Not String.IsNullOrEmpty(Request.QueryString("SessionID")) Then
                            Dim pOutDataMgr As PunchOutDataManager = New PunchOutDataManager()
                            customer.IsPunchOutUser = pOutDataMgr.IsPunchOutSession(Request.QueryString("SessionID"))
                            If customer.IsPunchOutUser Then
                                Session("PunchOutSesison") = Request.QueryString("SessionID")
                            End If
                        End If
                    End If
                End If

            Catch ex As Exception
                Dim objEx As New ServicesPlusBusinessException(" :User Name:" + sUserIdentity + " Inner InnerException: " + ex.InnerException.ToString() + "  Message: " + ex.Message _
                 + "     " + "<a href=""#"" onclick=""javascript:window.open('" + ConfigurationData.GeneralSettings.URL.contact_us_popup_link + "',null,'" + sFeature + "');"">" + "contact us" + "</a>" + " for assistance and mention incident number .")
                objEx.setcustomMessage = True
                objPCILoggerForCustomer.IndicationSuccessFailure = "Failure"
                objPCILoggerForCustomer.Message = "Login Failure. " & ex.Message.ToString()
                Throw objEx
            Finally
                objPCILoggerForCustomer.PushLogToMSMQ()
            End Try
        End Sub
    End Class

End Namespace
