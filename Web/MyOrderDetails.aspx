<%@ Reference Page="~/SSL.aspx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="ServicePlusNav" Src="ServicePlusNav.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="PersonalMessage" Src="PersonalMessage.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="TopSonyHeader" Src="TopSonyHeader.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="SonyFooter" Src="SonyFooter.ascx" %>

<%@ Page Language="vb" AutoEventWireup="false" Inherits="ServicePLUSWebApp.MyOrderDetails" EnableViewStateMac="true" CodeFile="MyOrderDetails.aspx.vb" CodeFileBaseClass="ServicePLUSWebApp.SSL" %>

<html lang="<%=PageLanguage %>">
<head>
    <title><%=Resources.Resource.ttl_MyOrderDetails%></title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link href="includes/ServicesPLUS_style.css" type="text/css" rel="stylesheet">
    <script type="text/javascript" src="includes/ServicesPLUS.js"></script>
    <script type="text/javascript" src="includes/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="includes/svcplus_globalization.js"></script>
    <script type="text/javascript">
			<!--
    function cancelOrderItem(cancelCommand, partOrderLineNumber) {
        var bolCancel;
        bolCancel = window.confirm( '<%= Resources.Resource.el_CancelItem%>');
        var q = document.getElementById('<%=hCancelEvent.clientID %>')
        q.value = bolCancel
        var vLineItem = document.getElementById('<%=hLineItem.clientID %>')
        vLineItem.value = partOrderLineNumber.toString()

        if (bolCancel) {
            __doPostBack(cancelCommand.toString(), partOrderLineNumber.toString());
        }
        else {
            alert( '<%= Resources.Resource.el_NotCancledBackordered%>');
            }
        }

        function __doPostBack(eventTarget, eventArgument) {
            var theform;
            if (window.navigator.appName.toLowerCase().indexOf("netscape") > -1) {
                theform = document.forms["Form1"];
            }
            else {
                theform = document.Form1;
            }
            theform.__EVENTTARGET.value = eventTarget.split("$").join(":");
            theform.__EVENTARGUMENT.value = eventArgument;
            theform.submit();
        }
			// -->

    </script>
</head>
<body style="color: #000000; background: #5d7180; margin: 0px;">
    <form id="Form1" name="form1" action="MyOrderDetails.aspx" method="post" runat="server">
        <input id="__EVENTTARGET" type="hidden" name="__EVENTTARGET">
        <input id="__EVENTARGUMENT" type="hidden" name="__EVENTARGUMENT">
        <asp:HiddenField ID="hCancelEvent" runat="server" Value="False" />
        <asp:HiddenField ID="hOrderId" runat="server" Value="" />
        <asp:HiddenField ID="hLineItem" runat="server" Value="" />
        <center>
            <table width="760" border="0" role="presentation">
                <tr>
                    <td width="25" style="background: url('images/sp_left_bkgd.gif');">
                        <img src="images/spacer.gif" width="25" height="25" alt="">
                    </td>
                    <td width="710" bgcolor="#ffffff">
                        <table width="710" border="0" role="presentation">
                            <tr>
                                <td>
                                    <ServicePLUSWebApp:TopSonyHeader ID="TopSonyHeader" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <nav><ServicePLUSWebApp:ServicePlusNav ID="SonyNav" runat="server" /></nav>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="710" border="0" role="presentation">
                                        <tr>
                                            <td width="464" bgcolor="#363d45" background="images/sp_int_header_top_ServicesPLUS_onepix.gif"
                                                height="57" align="right" valign="top">
                                                <br />
                                                <h1 class="headerText">ServicesPLUS&nbsp;&nbsp;</h1>
                                            </td>
                                            <td valign="top" bgcolor="#363d45">
                                                <table width="246" border="0" role="presentation">
                                                    <tr>
                                                        <td colspan="3">
                                                            <img height="24" src="images/sp_int_header_top_right.gif" width="246" alt="">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="5">&nbsp;</td>

                                                        <td width="236">
                                                            <span class="memberName">
                                                                <ServicePLUSWebApp:PersonalMessage ID="PersonalMessageDisplay" runat="server" />
                                                            </span>
                                                        </td>
                                                        <td width="5">&nbsp;</td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td bgcolor="#f2f5f8">
                                                <h2 class="headerTitle" style="padding-right: 20px; text-align: right;"><%=Resources.Resource.el_ViewOrderDetail%></h2>
                                            </td>
                                            <td bgcolor="#99a8b5">&nbsp;</td>
                                        </tr>
                                        <tr height="9">
                                            <td bgcolor="#f2f5f8">
                                                <img height="9" src="images/sp_int_header_btm_left_onepix.gif" width="464" alt="">
                                            </td>
                                            <td bgcolor="#99a8b5">
                                                <img height="9" src="images/sp_int_header_btm_right.gif" width="246" alt="">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="710" border="0" role="presentation">
                                        <tr>
                                            <td width="20" height="20">
                                                <img height="20" src="images/spacer.gif" width="20" alt="">
                                            </td>
                                            <td width="670" height="20">
                                                <asp:Label ID="errorMessageLabel" runat="server" ForeColor="Red" CssClass="tableData" EnableViewState="False" />
                                            </td>
                                            <td width="20" height="20">
                                                <img height="20" src="images/spacer.gif" width="20" alt="">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="20">
                                                <img height="20" src="images/spacer.gif" width="20" alt="">
                                            </td>
                                            <td width="670">
                                                <img height="5" src="images/spacer.gif" width="670" alt=""><br />
                                                <asp:Table ID="OrderNumberTable" runat="server" BorderWidth="0"
                                                    Width="670" />
                                                <img height="5" src="images/spacer.gif" width="670" alt=""><br />
                                                <asp:Table ID="TrainingOrderDetailsTable" Visible="false" runat="server" CellSpacing="0"
                                                    BorderWidth="0" />
                                                <div runat="server" id="orderdiv">
                                                    <asp:Table ID="OrderDetailsTable" Visible="false" runat="server" CellSpacing="0"
                                                        BorderWidth="0" />
                                                    <img height="30" src="images/spacer.gif" width="670" alt=""><br />
                                                    <asp:Table ID="OrderInvoiceTable" runat="server" CellPadding="0"
                                                        BorderWidth="0" Width="670" Visible="false" />
                                                </div>
                                            </td>
                                            <td width="20">
                                                <img height="20" src="images/spacer.gif" width="20" alt="">
                                            </td>
                                        </tr>
                                    </table>
                                    <p>
                                        <br />
                                        <img height="30" src="images/spacer.gif" width="670" alt=""><br />
                                        <span class="legalCopy">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; *
											<asp:Label ID="labelSpecialTax" runat="server" Text="<%$ Resources:Resource, el_RecyclingFee1%>" />
                                            <asp:HyperLink ID="specialTaxHyperLink" runat="server" Font-Underline="True" Font-Names="Arial"><%=Resources.Resource.el_RecyclingFee2%> </asp:HyperLink>
                                        </span>
                                    </p>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <footer><ServicePLUSWebApp:SonyFooter ID="SonyFooter1" runat="server" /></footer>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="25" style="background: url('images/sp_right_bkgd.gif')">
                        <img src="images/spacer.gif" width="25" height="20" alt="">
                    </td>
                </tr>
            </table>
        </center>
    </form>
</body>
</html>
