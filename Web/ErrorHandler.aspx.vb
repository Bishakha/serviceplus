Imports Sony.US.SIAMUtilities
Namespace ServicePLUSWebApp

    Partial Class ErrorHandler
        Inherits SSL

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub


        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            ' ASleight - Adding exception handler so we get logs from errors that aren't handled in normal ways.
            If (Session("Exception") IsNot Nothing) Then
                Dim ex As Exception = CType(Session("Exception"), Exception)
                MessageLabel.Text = ServicesPlusException.Utilities.WrapExceptionforUI(ex)
            Else
                ' Old method, based on query strings, which does no logging because it abandoned the exception.
                If Request.QueryString("ApplicationError") IsNot Nothing Then
                    MessageLabel.Text = "Unexpected Error has occurred."
                ElseIf Request.QueryString("SoftwareError") IsNot Nothing Then
                    MessageLabel.Text = "Due to technical issues we are unable to add this product to your shopping cart."
                Else
                    MessageLabel.Text = "Unexpected Error has occurred."
                End If
                MessageLabel.Text &= " If you need to place an immediate order please contact ServicesPLUS support at 1-800-538-7550."
            End If
            Session.Remove("SelectedItemsBeforeLoggedIn")
        End Sub

    End Class

End Namespace
