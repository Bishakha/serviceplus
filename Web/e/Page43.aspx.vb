Imports Sony.US.siam, Sony.US.ServicesPLUS.Controls
Imports Sony.US.SIAMUtilities
Imports System.Drawing
Imports ServicesPlusException

Namespace SIAMAdmin

    Partial Class Page43
        Inherits UtilityClass
        'Dim objUtilties As Utilities = New Utilities

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub


        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            MyBase.IsProtectedPage(True, True)
            InitializeComponent()
            ApplyDataGridStyle()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                If Not IsPostBack Then
                    Me.btnGet.Visible = False
                    Me.lbLogMessage.Visible = False
                    Me.gridLogMessages.Visible = False
                    Me.Label1.Visible = False
                    Me.Label2.Visible = False
                    Me.Label3.Visible = False
                    Me.Label4.Visible = False
                    Me.Label5.Visible = False
                    Me.Label6.Visible = False
                    Me.ddLabel.Visible = False
                    Me.txtFilter.Visible = False
                    Me.txtBeginTime.Visible = False
                    Me.txtEndTime.Visible = False
                End If
            Catch ex As Exception
                lbErrorText.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Sub btnLogin_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLogin.Click
            Try
                Dim psw As String = Encryption.Encrypt(TextBox1.Text)

                If psw = "g8ZTGqfWG5epUpTT1Lq+/A==" Then
                    Me.btnGet.Visible = True
                    Me.lbLogMessage.Visible = True
                    Me.gridLogMessages.Visible = True
                    Me.Label1.Visible = True
                    Me.Label2.Visible = True
                    Me.Label3.Visible = True
                    Me.Label4.Visible = True
                    Me.Label5.Visible = True
                    Me.Label6.Visible = True
                    Me.ddLabel.Visible = True
                    Me.txtFilter.Visible = True
                    Me.txtBeginTime.Visible = True
                    Me.txtEndTime.Visible = True
                End If
            Catch ex As Exception
                lbErrorText.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Sub btnGet_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGet.Click
            Try
                Dim theLogMessages As ArrayList = getLogMessages(False)
                Me.gridLogMessages.CurrentPageIndex = 0
                Me.gridLogMessages.DataSource = theLogMessages
                gridLogMessages.DataBind()
                If theLogMessages.Count <= 0 Then
                    '-- no log to display --
                    Me.lbErrorText.Text = "No log messages at this time."
                End If
            Catch ex As Exception
                lbErrorText.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub
        Private Function getLogMessages(Optional ByVal useCache As Boolean = True) As ArrayList
            Dim theLogMessages As ArrayList
            Try
                Dim logMessagesObj As Object = Session("LogMessages")
                If useCache And Not logMessagesObj Is Nothing Then
                    theLogMessages = CType(logMessagesObj, ArrayList)
                Else
                    Dim logger As SonyLogger1 = New SonyLogger1
                    theLogMessages = logger.ReadTrace(Me.ddLabel.SelectedItem.ToString(), Me.txtFilter.Text, Me.txtBeginTime.Text, Me.txtEndTime.Text)
                    Session("LogMessages") = theLogMessages
                End If
            Catch ex As Exception
                Me.lbErrorText.Text = Utilities.WrapExceptionforUI(ex)
            End Try
            Return theLogMessages
        End Function
        Sub gridLogMessages_Paging(ByVal sender As Object, ByVal e As DataGridPageChangedEventArgs)
            Try
                gridLogMessages.CurrentPageIndex = e.NewPageIndex
                Dim theLogMessages As ArrayList = getLogMessages() 'always from cached
                Me.gridLogMessages.DataSource = theLogMessages
                gridLogMessages.DataBind()
                gridLogMessages.SelectedIndex = -1 'unselect
            Catch ex As Exception
                lbErrorText.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Public Sub gridLogMessages_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
            Try
                Dim selectedMsg As SonyLogMessage = getLogMessage()
                Me.lbLogMessage.Text = selectedMsg.Body
            Catch ex As Exception
                lbErrorText.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Function getLogMessage() As SonyLogMessage
            Dim theLogMessages As ArrayList = Me.getLogMessages() 'always try to get from cached first

            Return CType(theLogMessages(Me.gridLogMessages.SelectedIndex + Me.gridLogMessages.CurrentPageIndex * Me.gridLogMessages.PageSize), SonyLogMessage)
        End Function

        Private Sub ApplyDataGridStyle()
            gridLogMessages.BackColor = Color.Gainsboro
            gridLogMessages.ForeColor = Color.Blue
            gridLogMessages.AllowPaging = True
            gridLogMessages.PageSize = 15
            gridLogMessages.PagerStyle.Mode = PagerMode.NumericPages
        End Sub
    End Class

End Namespace
