Imports System.Web.UI
Imports Sony.US.ServicesPLUS.Process
Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Data
Imports System.Web.UI.WebControls
Imports System.Data
Imports System.Drawing
Imports Sony.US.SIAMUtilities
Imports ServicesPlusException

Namespace SIAMAdmin

    Partial Class SearchFunctionality
        Inherits UtilityClass
        'Dim objUtilties As Utilities = New Utilities

        Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click

            Dim oUsrSecManager As New UserRoleSecurityManager
            Dim col_functionality() As Functionality

            Dim iStatus As Integer = 0

         
            Try
               
                If txtFunctionalityName.Text.Length > 0 Or txtURLMapped.Text.Length > 0 Then
                    If chkIsActive.Checked Then
                        col_functionality = oUsrSecManager.GetFunctionalitiesObj(txtFunctionalityName.Text, txtURLMapped.Text, "0*Active")
                    Else
                        col_functionality = oUsrSecManager.GetFunctionalitiesObj(txtFunctionalityName.Text, txtURLMapped.Text, "0*InActive")

                    End If

                Else
                    If chkIsActive.Checked Then
                        col_functionality = oUsrSecManager.GetFunctionalitiesObj(txtFunctionalityName.Text, txtURLMapped.Text, "1")
                    Else
                        col_functionality = oUsrSecManager.GetFunctionalitiesObj(txtFunctionalityName.Text, txtURLMapped.Text, "3")

                    End If
                End If

              

                If col_functionality.Length > 0 Then
                    ApplyDataGridStyle()
                    dgSearchFunctionality.DataSource = col_functionality
                    dgSearchFunctionality.DataBind()
                    dgSearchFunctionality.Visible = True
                Else
                    dgSearchFunctionality.Visible = False
                    ErrorLabel.Text = "No Result(s) found."
                End If




            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try

        End Sub

        Private Sub ApplyDataGridStyle()

            'dgSearchRole.ApplyStyle(GetStyle(500))
            dgSearchFunctionality.PageSize = 30
            dgSearchFunctionality.AutoGenerateColumns = False
            dgSearchFunctionality.AllowPaging = False
            dgSearchFunctionality.GridLines = GridLines.Horizontal
            dgSearchFunctionality.AllowSorting = True
            dgSearchFunctionality.CellPadding = 3
            dgSearchFunctionality.PagerStyle.Mode = PagerMode.NumericPages
            dgSearchFunctionality.HeaderStyle.Font.Size = FontUnit.Point(8)
            dgSearchFunctionality.HeaderStyle.ForeColor = Color.GhostWhite
            dgSearchFunctionality.HeaderStyle.BackColor = Color.FromKnownColor(KnownColor.ControlDarkDark)
            dgSearchFunctionality.ItemStyle.Font.Size = FontUnit.Point(8)
            dgSearchFunctionality.AlternatingItemStyle.Font.Size = FontUnit.Point(8)

        End Sub

        Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click

            txtFunctionalityName.Text = ""
            txtURLMapped.Text = ""
            chkIsActive.Checked = False
            dgSearchFunctionality.Visible = False

        End Sub

        Protected Sub Page_Init1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
            MyBase.IsProtectedPage(True, True)
        End Sub
    End Class

End Namespace
