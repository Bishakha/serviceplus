Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Process
Imports Sony.US.ServicesPLUS.Data
Imports System.Drawing
Imports ServicesPlusException


Namespace SIAMAdmin


    Partial Class training_class_search
        Inherits UtilityClass
        'Dim objUtilties As Utilities = New Utilities

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Const majorCategoryDropdownMessage As String = "All categories."
        Private Const modelNumberDropdownMessage As String = "All models."
        Private Const locationDropdownMessage As String = "All locations."
        Private Const titleDropdownMessage As String = "All titles."
        Private Const monthDropdownMessage As String = "All months."
        Private Const majorCategoryDropdownUnavailableMessage As String = "No categories are available as this time."
        Private Const locationDropdownUnavailableMessage As String = "No locations are available as this time."
        'search parameter array length
        Private Const conSearchParmsArrayLength As Int16 = 10
        Private Const conMaxResultsPerPage As Int16 = 50
        Private IsSearch As Boolean = True

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            MyBase.IsProtectedPage(True, True)
            InitializeComponent()
            '-- apply the datagrid style --
            ApplyDataGridStyle(classDataGrid)
            classDataGrid.SelectedItemStyle.BackColor = Color.Lime
        End Sub

#End Region


        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                If Not IsPostBack Then
                    PopulateModelNumberDropDown()
                    PopulateMajorCategoryDropDown()              '-- populate the ddl
                    PopulateLocationDropDown()
                    PopulateTitleDropDown()
                    PopulateMonthDropDown()
                    PopulateYearDropDown()
                End If
            Catch ex As Exception
                errorMessageLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Sub btnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearch.Click
            Try
                Dim courseResults() As CourseSchedule
                '-- get search results collection
                courseResults = GetCourseResultCollection(GetSearchParms())
                classDataGrid.CurrentPageIndex = 0
                classDataGrid.DataSource = courseResults
                Session.Item("searchedClass") = courseResults
                classDataGrid.DataBind()
                If courseResults.Length = 0 Then
                    errorMessageLabel.Text = "No result found for search criteria."
                End If
            Catch ex As Exception
                errorMessageLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Function GetSearchParms() As String()
            Dim returnSearchParms(conSearchParmsArrayLength) As String
            Dim city As String = ""
            Dim state As String = ""
            Dim modelNumber As String = ""
            Dim courseNumber As String = ""
            Dim categoryName As String = ""
            Dim title As String = ""
            Dim location As String = ""
            Dim month, year As Integer

            Try
                ' Pull all of the values from the inputs.
                modelNumber = ddlModelNumber.SelectedValue.Trim()
                courseNumber = txtCourseNumber.Text.ToString().Trim()
                categoryName = ddMajorCategory.SelectedValue.ToString().Trim()
                title = ddTitle.SelectedValue.Trim()
                location = ddLocation.SelectedValue.ToString().Trim()
                month = ddMonth.SelectedIndex
                year = ddYear.SelectedIndex

                ' Clean up any "default" values that were selected.
                If modelNumber = modelNumberDropdownMessage Then modelNumber = ""
                If categoryName = majorCategoryDropdownMessage Then categoryName = ""
                If title = titleDropdownMessage Then title = ""
                If location <> locationDropdownMessage Then
                    Dim split As String() = location.Split(",")
                    city = split(0)
                    state = split(1)
                End If
                If month = 0 Then month = -1
                If year = 0 Then
                    year = -1
                Else
                    year = Convert.ToInt16(ddYear.SelectedItem.Text)
                End If

                '-- just a check to make sure we are not passing in to much data. --
                If modelNumber.Length > 30 Then modelNumber.Substring(0, 30)
                If categoryName.Length > 100 Then categoryName.Substring(0, 100)

                returnSearchParms.SetValue(modelNumber, 0)
                returnSearchParms.SetValue(courseNumber, 1)
                returnSearchParms.SetValue(categoryName, 2)
                returnSearchParms.SetValue(title, 3)
                returnSearchParms.SetValue(city, 4)
                returnSearchParms.SetValue(state, 5)
                returnSearchParms.SetValue(month.ToString(), 6)
                returnSearchParms.SetValue(year.ToString(), 7)
                Select Case rdoStatus.SelectedIndex
                    Case 0
                        returnSearchParms.SetValue("1", 8)
                        returnSearchParms.SetValue("0", 9)
                    Case 1
                        returnSearchParms.SetValue("2", 8)
                        returnSearchParms.SetValue("1", 9)
                    Case 2
                        returnSearchParms.SetValue("1", 8)
                        returnSearchParms.SetValue("1", 9)
                End Select
            Catch ex As Exception
                errorMessageLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
            Return returnSearchParms
        End Function

        Private Sub PopulateMajorCategoryDropDown()
            Try
                Dim courseDataManager As New CourseDataManager
                Dim categories() As String = courseDataManager.GetMajorCategoryDescritions(majorCategoryDropdownMessage)
                ddMajorCategory.DataSource = categories
                ddMajorCategory.DataBind()
            Catch ex As Exception
                errorMessageLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Sub PopulateModelNumberDropDown()
            Try
                Dim courseDataManager As New CourseDataManager
                Dim modelnumbers() As String = courseDataManager.GetModelNumber(modelNumberDropdownMessage)
                ddlModelNumber.DataSource = modelnumbers
                ddlModelNumber.DataBind()
            Catch ex As Exception
                errorMessageLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Sub PopulateTitleDropDown()
            Try
                Dim courseDataManager As New CourseDataManager
                Dim titles() As String = courseDataManager.GetCourseTitles(titleDropdownMessage)
                ddTitle.DataSource = titles
                ddTitle.DataBind()
            Catch ex As Exception
                errorMessageLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Sub PopulateLocationDropDown()
            Try
                Dim courseDataManager As New CourseDataManager
                Dim location() As CourseLocation = courseDataManager.GetLocations()
                ddLocation.Items.Add(New ListItem(locationDropdownMessage, locationDropdownMessage))
                For Each cl As CourseLocation In location
                    ddLocation.Items.Add(New ListItem(cl.City + ", " + cl.State, cl.City + "," + cl.State))
                Next
            Catch ex As Exception
                errorMessageLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Sub PopulateMonthDropDown()
            'Dim courseManager As New CourseManager
            Dim monthes() As String = {"All months.", "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"}

            ddMonth.DataSource = monthes
            ddMonth.DataBind()
        End Sub

        Private Sub PopulateYearDropDown()
            Dim intcount As Int16
            ddYear.Items.Add("All Year")
            For intcount = DateTime.Today.Year - 3 To DateTime.Today.Year + 1
                ddYear.Items.Add(intcount.ToString())
            Next
        End Sub

        Private Function GetCourseResultCollection(ByVal searchParms As String()) As CourseSchedule()
            Dim resultCollection As CourseSchedule() = Nothing

            Try
                Dim courseManager As New CourseManager
                resultCollection = courseManager.CourseSearchTrainingClass(searchParms(2), searchParms(0), searchParms(1), searchParms(3), searchParms(4), searchParms(5), "", searchParms(6).ToString(), searchParms(7).ToString(), True, chkFutureItem.Checked, searchParms(8).ToString(), searchParms(9).ToString(), chkShowReservationOnly.Checked)
            Catch ex As Exception
                errorMessageLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
            Return resultCollection
        End Function

        Private Sub classDataGrid_ItemCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles classDataGrid.ItemCreated
            Try
                If e.Item.ItemType <> ListItemType.Header Then
                    e.Item.ApplyStyle(GetStyle(e.Item.ItemType))
                End If
            Catch ex As Exception
                errorMessageLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Sub classDataGrid_ItemDataBound(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles classDataGrid.ItemDataBound
            Try
                If (e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem) Then
                    Dim courseResults As CourseSchedule = e.Item.DataItem
                    If courseResults.Course.Type.Code <> "Classroom Training" Then
                        e.Item.Cells(3).Text = String.Empty
                        e.Item.Cells(5).Text = String.Empty
                        e.Item.Cells(6).Text = String.Empty
                    End If
                    Dim hyperLink As HyperLink = CType(e.Item.Cells(8).FindControl("HyperlinkRegister"), HyperLink)
                    hyperLink.NavigateUrl = "training-class-detail.aspx?LineNo=" + (e.Item.ItemIndex() + classDataGrid.CurrentPageIndex * classDataGrid.PageSize).ToString

                    Dim strParam As String = "0,1"
                    Select Case rdoStatus.SelectedIndex
                        Case 0
                            strParam = "0,1"
                        Case 1
                            strParam = "2,1"
                        Case 2
                            strParam = "1,1"
                    End Select
                    hyperLink = CType(e.Item.Cells(4).FindControl("hlkCourseNumber"), HyperLink)
                    hyperLink.Attributes.Add("onclick", "javascript:ShowReport('" + (e.Item.ItemIndex() + classDataGrid.CurrentPageIndex * classDataGrid.PageSize).ToString + "'," + strParam + ",1" + ");")
                    hyperLink.NavigateUrl = "#"
                    hyperLink.ForeColor = Color.White
                    hyperLink = CType(e.Item.Cells(7).FindControl("hlkPartNumber"), HyperLink)
                    hyperLink.Attributes.Add("onclick", "javascript:ShowReport('" + (e.Item.ItemIndex() + classDataGrid.CurrentPageIndex * classDataGrid.PageSize).ToString + "'," + strParam + ",2" + ");")
                    hyperLink.NavigateUrl = "#"
                    hyperLink.ForeColor = Color.Yellow



                    If e.Item.Cells(10).Text = "2" Then
                        e.Item.BackColor = Color.BurlyWood
                        e.Item.ForeColor = Color.Black
                    Else
                        e.Item.BackColor = Color.DarkCyan
                        e.Item.ForeColor = Color.White
                    End If
                End If
            Catch ex As Exception
                errorMessageLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Sub classDataGrid_Paging(ByVal sender As Object, ByVal e As DataGridPageChangedEventArgs)
            Try
                classDataGrid.CurrentPageIndex = e.NewPageIndex
                classDataGrid.DataSource = Session.Item("searchedClass")
                classDataGrid.DataBind()
            Catch ex As Exception
                errorMessageLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Sub ApplyDataGridStyle(ByVal DataGrid1 As System.Web.UI.WebControls.DataGrid)
            'DataGrid1.ApplyStyle(GetStyle(500))
            DataGrid1.PageSize = 8
            DataGrid1.AutoGenerateColumns = False
            DataGrid1.AllowPaging = True
            DataGrid1.GridLines = GridLines.Horizontal
            DataGrid1.AllowSorting = True
            DataGrid1.CellPadding = 3
            DataGrid1.PagerStyle.Mode = PagerMode.NumericPages
            DataGrid1.HeaderStyle.Font.Size = FontUnit.Point(8)
            DataGrid1.HeaderStyle.ForeColor = Color.GhostWhite
            DataGrid1.HeaderStyle.BackColor = Color.FromKnownColor(KnownColor.ControlDarkDark)
            DataGrid1.ItemStyle.Font.Size = FontUnit.Point(8)
            DataGrid1.AlternatingItemStyle.Font.Size = FontUnit.Point(8)
        End Sub
    End Class

End Namespace
