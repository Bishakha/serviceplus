Imports System.Data
Imports System.IO
Imports System.Xml
Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Data
Imports Sony.US.ServicesPLUS.Process
Imports System.Globalization
Imports Sony.US.ServicesPLUS.Controls
Imports Sony.US.SIAMUtilities
Imports ServicesPlusException

'<-------------------------------------------------------------------------------------------->
'<Created>    : Rujuta on 18th Dec 2009 for BUG : 2127
'<Description> : Created to check Log file content depending on Date & File selected . 
'<-------------------------------------------------------------------------------------------->

Namespace SIAMAdmin
    Partial Class LogFileViewer
        Inherits UtilityClass
        'Dim objUtilties As Utilities = New Utilities

#Region " Web Form Designer Generated Code "

        Private LogDirectory As String = String.Empty
        Private di As IO.DirectoryInfo
        Private aryFi As IO.FileInfo()
        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub



        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            MyBase.IsProtectedPage(True, True)
            InitializeComponent()
            If Not (ConfigurationData.Environment.PCILogger.ApplicationLogsLocation Is Nothing) Then
                LogDirectory = ConfigurationData.Environment.PCILogger.ApplicationLogsLocation
            Else
                ErrorLabel.Text = "Unable to find Directory location"
                ddlFileCreatedDate.Enabled = False
                ddlFileCreatedDate.Items.Clear()
                ddlFileNames.Items.Clear()
            End If
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                ErrorLabel.Visible = False
                di = New IO.DirectoryInfo(LogDirectory)
                If Not (di.Exists) Then
                    ErrorLabel.Text = "Unable to find Directory location"
                    ddlFileCreatedDate.Enabled = False
                    ddlFileCreatedDate.Items.Clear()
                    ddlFileNames.Items.Clear()
                    Exit Sub
                End If

                If Not IsPostBack Then
                    PopulateFileCreationDateDropDown()
                End If




            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                ErrorLabel.Visible = True
            End Try
        End Sub

        Private Sub PopulateFileCreationDateDropDown()
            '<-------------------------------------------------------------------------------------------->
            '<Description> : Populate Dropdownlist with all unique dates present under log directory  . 
            '<-------------------------------------------------------------------------------------------->
            Try
                aryFi = di.GetFiles("*.xml")
                If aryFi Is Nothing Then Exit Sub
                Dim arrDates As New ArrayList()

                ddlFileCreatedDate.Items.Clear()
                For Each fi As IO.FileInfo In aryFi
                    arrDates.Add(fi.LastWriteTime.Date)
                Next
                arrDates.Sort()
                arrDates.Reverse()

                If arrDates.Count > 0 Then
                    For Each dateValue As Date In arrDates
                        If Not (ddlFileCreatedDate.Items.IndexOf(New ListItem(dateValue)) >= 0) Then
                            ddlFileCreatedDate.Items.Add(New ListItem(dateValue))
                        End If
                    Next
                    ddlFileCreatedDate.DataBind()
                    ddlFileCreatedDate.Items.Insert(0, "Select Date")
                    ddlFileCreatedDate.SelectedIndex = 0
                End If




            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                ErrorLabel.Visible = True
            End Try
        End Sub

        Private Sub PopulateFiles()
            '<-------------------------------------------------------------------------------------------->
            '<Description> : Populate Listbox with all Files present for user selected date from dropdown list.
            '<-------------------------------------------------------------------------------------------->
            Try
                aryFi = di.GetFiles("*.xml")
                If aryFi Is Nothing Then Exit Sub

                Dim dtFiles As New DataTable
                dtFiles.Columns.Add("NameDate", GetType(String))
                dtFiles.Columns.Add("Date", GetType(Date))
                dtFiles.Columns.Add("Name", GetType(String))
                Dim row As DataRow = Nothing

                ddlFileNames.Enabled = True
                ddlFileNames.Items.Clear()

                For Each fi As IO.FileInfo In aryFi
                    If (fi.LastWriteTime.Date = CType(ddlFileCreatedDate.SelectedItem.Text, Date)) Then
                        row = dtFiles.NewRow()
                        row("NameDate") = fi.Name & "   " & fi.LastWriteTime.ToString("hh:mm:ss tt")
                        row("Date") = fi.LastWriteTime
                        row("Name") = fi.Name
                        dtFiles.Rows.Add(row)
                    End If
                Next

                If dtFiles.Rows.Count > 0 Then
                    Dim dvFiles As DataView = New DataView(dtFiles)
                    dvFiles.Sort = "Date DESC"
                    ddlFileNames.DataSource = dvFiles
                    ddlFileNames.DataTextField = "NameDate"
                    ddlFileNames.DataValueField = "Name"
                    ddlFileNames.DataBind()
                    ddlFileNames.Items.Insert(0, "Select File")
                    ddlFileNames.SelectedIndex = 0
                End If





            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                ErrorLabel.Visible = True
            End Try
        End Sub

        Function IndentXMLString(ByVal Xml As String) As String
            '<-------------------------------------------------------------------------------------------->
            '<Description> : Display textbox with formatted Xml Value
            '<-------------------------------------------------------------------------------------------->
            Dim ms As MemoryStream = New MemoryStream()
            Dim xtw As XmlTextWriter = New XmlTextWriter(ms, Encoding.Unicode)
            Dim doc As XmlDocument = New XmlDocument()

            Try
                doc.LoadXml(Xml)
                xtw.Formatting = Formatting.Indented
                doc.WriteContentTo(xtw)
                xtw.Flush()
                ms.Seek(0, SeekOrigin.Begin)
                Dim sr As StreamReader = New StreamReader(ms)
                Return sr.ReadToEnd()
            Catch ex As Exception
                Return String.Empty
            End Try
        End Function

        Protected Sub ddlFileCreatedDate_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlFileCreatedDate.SelectedIndexChanged
            '<-------------------------------------------------------------------------------------------->
            '<Description> : Populate DropDown when user select different date from dropdownlistbox.
            '<-------------------------------------------------------------------------------------------->
            Try

                If (ddlFileCreatedDate.SelectedIndex <> 0) Then
                    PopulateFiles()
                Else
                    ddlFileNames.Items.Clear()
                    ddlFileNames.Enabled = False
                End If
                txtFileContent.Text = ""




            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                ErrorLabel.Visible = True
            End Try
        End Sub

        Protected Sub ddlFileNames_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlFileNames.SelectedIndexChanged
            '<-------------------------------------------------------------------------------------------->
            '<Description> : Display File content when user select File from dropdownlistbox.
            '<-------------------------------------------------------------------------------------------->
            Try
                Dim fileName As String = "ServicesPLUS-Error.xml"
                If (ddlFileNames.SelectedIndex <> 0) Then
                    txtFileContent.Text = ""
                    If (File.Exists(LogDirectory & "\" & ddlFileNames.SelectedItem.Value)) Then
                        If (ddlFileNames.SelectedItem.Value <> fileName) Then
                            Dim s As String = File.ReadAllText(LogDirectory & "\" & ddlFileNames.SelectedItem.Value)
                            txtFileContent.Text = IndentXMLString("<ServicesPLUSErrorMessageLogView>" + s + "</ServicesPLUSErrorMessageLogView>")
                        Else
                            ErrorLabel.Text = ddlFileNames.SelectedItem.Value + "  file is being used by another process. Please try some other file."
                            ErrorLabel.Visible = True
                        End If

                    End If
                Else
                    txtFileContent.Text = ""
                End If

            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                ErrorLabel.Visible = True
            End Try
        End Sub
    End Class

End Namespace
