Imports System.Drawing
Imports ServicesPlusException
Imports Sony.US.ServicesPLUS.Controls
Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Process
Imports Sony.US.siam
Imports Sony.US.SIAMUtilities

Namespace SIAMAdmin
    Partial Class FindOrders
        Inherits UtilityClass

        Private siamIdentity As String
        Private LDapID As String
        Dim domianurl As String = Nothing
        Dim domianur As Boolean = False
        ' Dim gdobj As GlobalData = Nothing
        Dim culinfodtls As String = ""
        Dim objGlobalData As GlobalData = Nothing

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Protected WithEvents Payer As SPSTextBox
        Protected WithEvents CheckBox1 As System.Web.UI.WebControls.CheckBox
        Protected WithEvents Textbox1 As SPSTextBox
        Protected WithEvents Checkbox2 As System.Web.UI.WebControls.CheckBox
        Protected WithEvents Textbox2 As SPSTextBox
        Protected WithEvents Checkbox3 As System.Web.UI.WebControls.CheckBox
        Protected WithEvents Textbox3 As SPSTextBox
        Protected WithEvents Checkbox4 As System.Web.UI.WebControls.CheckBox
        Protected WithEvents Textbox4 As SPSTextBox
        Protected WithEvents Checkbox5 As System.Web.UI.WebControls.CheckBox
        Protected WithEvents Textbox5 As SPSTextBox
        Protected WithEvents Checkbox6 As System.Web.UI.WebControls.CheckBox
        Protected WithEvents CheckBox7 As System.Web.UI.WebControls.CheckBox
        Protected WithEvents Checkbox8 As System.Web.UI.WebControls.CheckBox
        Protected WithEvents LinkISSA As System.Web.UI.WebControls.Button
        Protected WithEvents AddAccnt As System.Web.UI.WebControls.Button
        Protected WithEvents Button1 As System.Web.UI.WebControls.Button


        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            Page.ID = Request.Url.Segments.GetValue(1)

            MyBase.IsProtectedPage(True, True)
            InitializeComponent()
            '-- apply the datagrid style --
            ApplyDataGridStyle()
        End Sub

#End Region

#Region "Events"
        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            'If Not Page.IsPostBack Then 'by sathish
            Try
                InitializeComponent()
                If objGlobalData Is Nothing Then
                    objGlobalData = New GlobalData()
                    If sps_dropdown.SelectedValue.Equals("US") Then
                        objGlobalData.SalesOrganization = ConfigurationData.GeneralSettings.GlobalData.SalesOrganization.US
                    Else
                        objGlobalData.SalesOrganization = ConfigurationData.GeneralSettings.GlobalData.SalesOrganization.CA
                    End If
                    If (objGlobalData IsNot Nothing) Then
                        HttpContext.Current.Session.Add("AGlobalData", objGlobalData)
                    End If
                End If
            Catch ex As Exception

            End Try
            controlMethod(sender, e)
            ' End If 'by sathish
        End Sub

        Private Sub FindOrder_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles FindOrder.Click
            Dim om As New OrderManager
            Dim debugMessage As String = $"FindOrders.aspx - FindOrder.Click START at {DateTime.Now.ToShortTimeString}{Environment.NewLine}"
            Try
                'Dim callingControl As String = String.Empty
                hdnOrderNumber.Value = OrderNumber.Text.Trim()
                debugMessage &= $"- OrderNumber.Text = {OrderNumber.Text}, "
                'If Not sender.Id Is Nothing Then callingControl = sender.Id
                'If callingControl = FindOrder.ID Then
                Dim OrderNo As String = om.FindConflictedOrder(OrderNumber.Text.Trim())
                If Not String.IsNullOrWhiteSpace(OrderNo) Then
                    debugMessage &= $"Conflicted Order No = {OrderNo}.{Environment.NewLine}"
                    hdnOrderNumber.Value = OrderNo
                    ScriptManager.RegisterStartupScript(Me, Me.GetType(), "showopoup", "javascript:OrderPopup();", True)
                End If

                debugMessage &= $"- Calling PopulateOrder, hdnOrderNumber.Value = {hdnOrderNumber.Value}{Environment.NewLine}"
                PopulateOrder(hdnOrderNumber.Value)
                'End If

            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            Finally
                Utilities.LogDebug(debugMessage)
            End Try
        End Sub

        Private Sub DataGrid1_ItemCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DataGrid1.ItemCreated, DataGrid1.ItemCreated
            If e.Item.ItemType <> ListItemType.Header Then
                e.Item.ApplyStyle(GetStyle(e.Item.ItemType))
            End If
        End Sub

        Sub DataGrid1_Paging(ByVal sender As Object, ByVal e As DataGridPageChangedEventArgs)
            Try
                DataGrid1.CurrentPageIndex = e.NewPageIndex
                LDapID = Request.QueryString("LdapId") '7086
                Dim siamid As String '7086
                Dim cm As New CustomerManager '7086
                siamid = cm.GetSiamIdByLdapId(LDapID) '7086
                'wasCustomerPassed(Request.QueryString("Identity"))'7086
                wasCustomerPassed(siamid) '7086
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

#End Region

#Region "Methods"
        Private Sub controlMethod(ByVal sender As System.Object, ByVal e As System.EventArgs)
            Try
                '206-04-22 ASleight - Referencing MyBase doesn't allow access to variables; using the qualified Class name is suggested
                'Dim user As User = Session(MyBase.CustomerSessionVariable)
                Dim user As User = Session(UtilityClass.CustomerSessionVariable)
                Dim callingControl As String = String.Empty
                If Not sender.Id Is Nothing Then callingControl = sender.Id
                Select Case callingControl
                    Case Page.ID.ToString()
                        If Not Request.QueryString("LdapId") Is Nothing Then '7086
                            LDapID = Request.QueryString("LdapId") '7086
                            Dim siamid As String '7086
                            Dim cm As New CustomerManager '7086
                            siamid = cm.GetSiamIdByLdapId(LDapID) '7086
                            'wasCustomerPassed(Request.QueryString("Identity"))'7086
                            wasCustomerPassed(siamid) '7086
                            writeScriptsToPage()
                        End If '7086
                    Case FindOrder.ID.ToString()
                        PopulateOrder(hdnOrderNumber.Value)
                End Select
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Sub wasCustomerPassed(ByVal thisIdentity As String)
            Try
                If Not thisIdentity = String.Empty Then
                    PopulateDataGridWithOrders(getOrders(thisIdentity))
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Sub PopulateOrder(ByRef orderNumber As String)
            Try
                If Not String.IsNullOrWhiteSpace(orderNumber) Then
                    PopulateDataGridWithOrders(getOrder(orderNumber))
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

#Region "       Get Order/Orders        "

        Private Function getOrder(ByRef orderNumber As String) As Order()
            Dim returnValue As Order() = Nothing
            Dim om As New OrderManager
            Dim thisOrder As Order
            Dim newlist As New ArrayList
            Dim debugMessage As String = $"FindOrders.aspx - getOrder START at {DateTime.Now.ToShortTimeString}.{Environment.NewLine}"

            Try
                If Not String.IsNullOrWhiteSpace(orderNumber) Then
                    debugMessage &= $"- Calling OrderManager.GetOrder with Order #: {orderNumber}.{Environment.NewLine}"
                    thisOrder = om.GetOrder(orderNumber, False, True, objGlobalData)

                    If thisOrder IsNot Nothing Then
                        debugMessage &= $"- Calling OrderManager.GetOrderInvoices.{Environment.NewLine}"
                        om.GetOrderInvoices(thisOrder) 'get invoice info

                        If thisOrder IsNot Nothing Then
                            debugMessage &= $"- Order Seq #: {thisOrder.SequenceNumber} fetched. Adding to array.{Environment.NewLine}"
                            newlist.Add(thisOrder)
                            returnValue = newlist.ToArray(thisOrder.GetType())
                        Else
                            debugMessage &= $"- Inner Else statement hit. Order not found.{Environment.NewLine}"
                            ErrorLabel.Text = "Order could not be found."
                        End If
                    Else
                        debugMessage &= $"- Outer Else statement hit. Order not found.{Environment.NewLine}"
                        ErrorLabel.Text = "Order could not be found."
                    End If
                End If
            Catch ex As Exception
                Utilities.LogDebug(debugMessage)
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try

            debugMessage &= $"- Returning list. Array length is: {If(returnValue?.Count.ToString(), "null")}."
            Utilities.LogDebug(debugMessage)
            Return returnValue
        End Function


        Private Function getOrders(ByVal siamIdentity As String) As Order()
            Dim theOrders As Order() = Nothing
            Try
                Dim ordersObj As Object = Cache("CustomerOrder_" + siamIdentity)
                If ordersObj Is Nothing Then
                    If Not String.IsNullOrWhiteSpace(siamIdentity) Then
                        Dim thisCM As New CustomerManager
                        Dim thisCustomer As Customer = thisCM.GetCustomer(siamIdentity)

                        If thisCustomer IsNot Nothing Then
                            Dim thisOM As New OrderManager
                            theOrders = thisOM.GetOrders(thisCustomer, "Ordernumber", "desc", objGlobalData, True)
                            Cache.Insert("CustomerOrder_" + siamIdentity, theOrders, Nothing, DateTime.Now.AddMinutes(2), TimeSpan.Zero) 'cache for 2 minutes
                        End If
                    End If
                Else
                    theOrders = CType(ordersObj, Order())
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
            Return theOrders
        End Function

#End Region

        Private Sub PopulateDataGridWithOrders(ByRef orders As Order())
            Try
                If orders IsNot Nothing Then
                    DataGrid1.Visible = True
                    DataGrid1.DataSource = orders
                    If orders.Length > DataGrid1.PageSize Then DataGrid1.AllowPaging = True
                    DataGrid1.DataBind()
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

#Region "       Helper Methods      "


        Private Sub ApplyDataGridStyle()
            DataGrid1.ApplyStyle(GetStyle(530))
            DataGrid1.PageSize = 10
            DataGrid1.AutoGenerateColumns = False
            DataGrid1.AllowPaging = False
            DataGrid1.GridLines = GridLines.Horizontal
            DataGrid1.AllowSorting = True
            DataGrid1.CellPadding = 3
            DataGrid1.PagerStyle.Mode = PagerMode.NumericPages
            DataGrid1.HeaderStyle.Height = Unit.Pixel(30)
            DataGrid1.PagerStyle.Height = Unit.Pixel(10)
            DataGrid1.HeaderStyle.Font.Size = FontUnit.Point(8)
            DataGrid1.HeaderStyle.ForeColor = Color.GhostWhite
            DataGrid1.HeaderStyle.BackColor = Color.FromKnownColor(KnownColor.ControlDarkDark)
            DataGrid1.ItemStyle.Font.Size = FontUnit.Point(8)
            DataGrid1.ItemStyle.ForeColor = Color.GhostWhite
            DataGrid1.AlternatingItemStyle.Font.Size = FontUnit.Point(8)
            DataGrid1.AlternatingItemStyle.ForeColor = Color.GhostWhite
        End Sub

        '-- helper scripts --
        Private Sub writeScriptsToPage()
            '-- this script will make sure that this page is only accessed via the iform tag in default.aspx -         
            RegisterClientScriptBlock("redirect", "<script language=""javascript"">if ( parent.frames.length === 0 ){window.location.replace(""" + MyBase.EntryPointPage + """)}</script>")

            '-- this will clear the status bar on the browser 
            RegisterStartupScript("ClearStatus", "<script language=""javascript"">window.parent.status = ""Done."";</script>")

            '-- this script is used to confirm that a user wants to remove an application -
            RegisterStartupScript("RemoveApplication", "<script language=""javascript"">function DeleteOp(itemId){var ok = window.confirm(""Are you sure ?"");if (ok === true){document.location.href=""" + Page.Request.Url.Segments.GetValue(1).ToString() + "?Remove="" + itemId;}}</script>")
        End Sub
#End Region

#End Region

        Protected Sub sps_dropdown_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles sps_dropdown.SelectedIndexChanged
            Try
                If objGlobalData Is Nothing Then
                    objGlobalData = New GlobalData()
                    If sps_dropdown.SelectedValue.Equals("US") Then
                        objGlobalData.SalesOrganization = ConfigurationData.GeneralSettings.GlobalData.SalesOrganization.US
                    Else
                        objGlobalData.SalesOrganization = ConfigurationData.GeneralSettings.GlobalData.SalesOrganization.CA
                    End If

                    If (objGlobalData IsNot Nothing) Then
                        HttpContext.Current.Session.Add("AGlobalData", objGlobalData)
                    End If
                End If
            Catch ex As Exception
            End Try
        End Sub
    End Class

End Namespace
