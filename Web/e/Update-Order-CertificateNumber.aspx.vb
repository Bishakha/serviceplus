Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Data
Imports Sony.US.ServicesPLUS.Process
Imports System.Globalization
Imports Sony.US.ServicesPLUS.Controls
Imports System.Data
Imports ServicesPlusException

Namespace SIAMAdmin
    Partial Class UpdateOrderCertificateNumber
        Inherits UtilityClass
        'Dim objUtilties As Utilities = New Utilities

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Protected WithEvents txtCourseNumber As SPSTextBox


        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            MyBase.IsProtectedPage(True, True)
            InitializeComponent()
        End Sub

#End Region

        Private m_CustomerEmailAddress As String = String.Empty
        Private m_CustomerFirstName As String = String.Empty
        Private m_CustomerLastName As String = String.Empty

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                If Not Request.QueryString("onum") Is Nothing Then
                    txtOrderNumber.Text = Request.QueryString("onum")
                    PopulateGrid()
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Sub PopulateGrid()
            Dim objOrderManager As OrderManager
            Try
                objOrderManager = New OrderManager()
                Dim dtCertificateData As DataSet = objOrderManager.GetCertificateOrderLinesForOrder(txtOrderNumber.Text)
                If dtCertificateData.Tables.Count > 0 Then
                    If dtCertificateData.Tables(0).Rows.Count > 0 Then
                        m_CustomerEmailAddress = dtCertificateData.Tables(0).Rows(0)("EMAILADDRESS").ToString()
                        m_CustomerFirstName = dtCertificateData.Tables(0).Rows(0)("FirstName").ToString()
                        m_CustomerLastName = dtCertificateData.Tables(0).Rows(0)("LastName").ToString()
                        Dim strCustomerData() As String = {m_CustomerEmailAddress, m_CustomerFirstName, m_CustomerLastName}
                        ViewState.Add("OrderCustomerData", strCustomerData)
                    End If
                End If
                dgCertificate.DataSource = dtCertificateData.Tables(0)
                dgCertificate.DataBind()
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            Finally
                objOrderManager = Nothing
            End Try
        End Sub

        Protected Sub dgCertificate_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgCertificate.ItemDataBound
            Try
                If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
                    Dim iQuantity = Convert.ToInt16(e.Item.Cells(1).Text.ToString())
                    Dim Cell2Label As Label
                    Cell2Label = e.Item.FindControl("lblActivationCode")
                    Dim strExistingActivationCodeCount As String

                    strExistingActivationCodeCount = IIf(Cell2Label.Text = String.Empty, 0, (Cell2Label.Text.ToString().Split(",")).Length)

                    If Cell2Label.Text = String.Empty Or iQuantity > strExistingActivationCodeCount Then
                        Dim m_ListCodeSpan As HtmlControl
                        Dim m_ImageButton As ImageButton
                        m_ListCodeSpan = e.Item.FindControl("listCodeSpan")
                        m_ImageButton = e.Item.FindControl("imgPicCode")
                        m_ListCodeSpan.Visible = False
                        m_ImageButton.Visible = False
                        Cell2Label.Text = "<SPAN class=""redAsterick"">" + "Missing " + (iQuantity - strExistingActivationCodeCount).ToString() + " activation code.</Span></br>" + Cell2Label.Text
                    Else
                        Dim m_ActivationCodeTextBox As SPSTextBox
                        Dim m_ListCodeSpan As HtmlControl
                        Dim m_ImageButton As ImageButton
                        m_ActivationCodeTextBox = e.Item.FindControl("txtActivationCode")
                        m_ListCodeSpan = e.Item.FindControl("listCodeSpan")
                        m_ImageButton = e.Item.FindControl("imgPicCode")
                        m_ListCodeSpan.Visible = False
                        m_ImageButton.Visible = False
                        m_ActivationCodeTextBox.Visible = False
                        e.Item.Cells(3).Controls.Add(New LiteralControl("All code are assigned."))
                        e.Item.Cells(4).Controls(0).Visible = False
                    End If
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub


        Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
            Try
                PopulateGrid()
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Protected Sub dgCertificate_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgCertificate.ItemCommand
            Try
                Dim m_ActivationCodeTextBox As SPSTextBox

                m_ActivationCodeTextBox = e.Item.FindControl("txtActivationCode")
                If m_ActivationCodeTextBox.Text.IndexOf(",") = 0 Then
                    m_ActivationCodeTextBox.Text = m_ActivationCodeTextBox.Text.Substring(1, m_ActivationCodeTextBox.Text.Length - 1)
                End If
                If m_ActivationCodeTextBox.Text.LastIndexOf(",") = m_ActivationCodeTextBox.Text.Length - 1 Then
                    m_ActivationCodeTextBox.Text = m_ActivationCodeTextBox.Text.Substring(0, m_ActivationCodeTextBox.Text.Length - 2)
                End If

                m_ActivationCodeTextBox.Text = m_ActivationCodeTextBox.Text.ToUpper().Replace(" ", "").Replace("~", ",").Replace(",,", ",")
                While m_ActivationCodeTextBox.Text.IndexOf(",,") >= 0
                    m_ActivationCodeTextBox.Text = m_ActivationCodeTextBox.Text.Replace(",,", ",")
                End While



                If m_ActivationCodeTextBox.Text = String.Empty Then
                    ErrorLabel.Text = "Please enter activation code in the text box."
                    Exit Sub
                Else
                    Dim m_Code As String()
                    Dim Cell2Label As Label
                    Dim strExistingCode As String = String.Empty
                    Cell2Label = e.Item.FindControl("lblActivationCode")
                    If Cell2Label.Text.IndexOf("</br>") >= 0 And Cell2Label.Text.IndexOf("</br>") + 5 < Cell2Label.Text.Length Then
                        strExistingCode = Cell2Label.Text.Substring(Cell2Label.Text.IndexOf("</br>") + 5, Cell2Label.Text.Length - (Cell2Label.Text.IndexOf("</br>") + 5))
                    End If

                    If strExistingCode <> String.Empty Then
                        m_Code = strExistingCode.ToUpper().Split(",")
                        If m_Code.Length > 0 Then
                            For Each strCode As String In m_Code
                                If m_ActivationCodeTextBox.Text.IndexOf(strCode) >= 0 Then
                                    ErrorLabel.Text = "Activation code " + strCode + " already exist with the order line item."
                                    Exit Sub
                                End If
                            Next
                        End If
                    End If

                    m_Code = m_ActivationCodeTextBox.Text.Split(",")

                    Dim iQuantity = Convert.ToInt16(e.Item.Cells(1).Text.ToString())
                    If strExistingCode <> String.Empty Then
                        If m_Code.Length > iQuantity - (strExistingCode.Split(",")).Length Then
                            ErrorLabel.Text = "Entered activation code is more than required."
                            Exit Sub
                        End If
                    End If


                    Array.Sort(m_Code)


                    Dim m_ActivationCode As String = String.Empty
                    For Each strCode As String In m_Code
                        If strCode = m_ActivationCode Then
                            ErrorLabel.Text = "Duplicate activation code " + strCode + " in text box."
                            Exit Sub
                        End If
                        m_ActivationCode = strCode
                    Next

                    Try
                        Dim strSavingCertificate As String = String.Empty
                        m_ActivationCode = m_ActivationCodeTextBox.Text
                        If Not strExistingCode Is String.Empty Then
                            strSavingCertificate = strExistingCode + "," + m_ActivationCode
                        Else
                            strSavingCertificate = m_ActivationCode
                        End If

                        While strSavingCertificate.IndexOf(",,") >= 0
                            strSavingCertificate = strSavingCertificate.Replace(",,", ",")
                        End While


                        If strSavingCertificate <> String.Empty Then
                            Dim objCourseManager As CourseManager = New CourseManager()
                            strSavingCertificate = objCourseManager.UpdateActivationCodeForOrderLine(e.Item.Cells(5).Text, e.Item.Cells(6).Text, strSavingCertificate)
                            'TODO: Send Activation Code Update Mail.
                            'm_CustomerEmailAddress
                            Try
                                Dim objOrderManager As OrderManager = New OrderManager()
                                Dim strMailBody As StringBuilder = New StringBuilder()
                                Dim strCustomerData() As String = ViewState.Item("OrderCustomerData")
                                strMailBody.Append("Dear " + strCustomerData(1) + " " + strCustomerData(2) + vbCrLf + vbCrLf)
                                strMailBody.Append("Training certification code for your order " + txtOrderNumber.Text + " has been updated." + vbCrLf + vbCrLf)
                                strMailBody.Append("Please login to ServicesPLUS and check your order status in ""My Orders.""" + vbCrLf + vbCrLf)
                                strMailBody.Append("Sincerely," + vbCrLf)
                                strMailBody.Append("The ServicesPLUS(sm) Team" + vbCrLf)
                                strMailBody.Append("Copyright 2009 Sony Electronics Inc. All Rights Reserved." + vbCrLf)
                                objOrderManager.SendCertificateConfirmationEmail(txtOrderNumber.Text, strCustomerData(0), strMailBody.ToString())




                            Catch ex As Exception
                                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                            End Try
                        End If
                        dgCertificate.EditItemIndex = -1
                        PopulateGrid()




                    Catch ex As Exception
                        ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                    End Try
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Sub SendUpdateCodeMail()

        End Sub
    End Class

End Namespace
