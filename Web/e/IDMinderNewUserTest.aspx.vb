Imports System.Web.UI
Imports Sony.US.ServicesPLUS.Process
Imports Sony.US.ServicesPLUS.Core
Imports System.Xml
Imports System.Data
Imports Sony.US.siam, Sony.US.ServicesPLUS.Controls
Imports System.Drawing
Imports Sony.US.ServicesPLUS.Data
Imports Sony.US.SIAMUtilities
Imports Microsoft.Win32
Imports WinLDAP
Imports ServicesPlusException
Imports Sony.US.AuditLog

Namespace SIAMAdmin

    Partial Class B2BBindTest

        Inherits UtilityClass
        'Dim objUtilties As Utilities = New Utilities
        Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
            'MyBase.IsProtectedPage(True, True)
            'If Request.UrlReferrer Is Nothing Then Response.Redirect("default.aspx")
        End Sub

        Protected Sub Bind_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Bind.Click
            Dim objPCILogger As New PCILogger() '6524
            Try

                '6524 start
                objPCILogger.EventOriginApplication = "ServicesPLUS"
                objPCILogger.EventOriginApplicationLocation = Me.Page.GetType().Name
                objPCILogger.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
                objPCILogger.OperationalUser = Environment.UserName
                objPCILogger.EventDateTime = DateTime.Now.ToLongTimeString()
                objPCILogger.HTTPRequestObjectValues = objPCILogger.GetRequestContextValue()
                objPCILogger.IndicationSuccessFailure = "Success"
                objPCILogger.EventOriginMethod = "Bind_Click"
                objPCILogger.Message = "Bind_Click Success."
                objPCILogger.EventType = EventType.Others
                '6524 end

                EnableFields()
                Dim securityAdmin As SecurityAdministrator = New SecurityAdministrator()
                Dim objUser As New User()
                SetUserObject(objUser)
                Dim strResult As String = securityAdmin.IDMinderNewUserTest(objUser, txtURL.Text.Trim(), txtadminID.Text.Trim(), txtUserName.Text.Trim(), txtPassword.Text.Trim())
                'objSecAdmin.AddUser(objUser
                txtResult.Text = strResult
                DisableFields()
                'SendingDataCopy()
            Catch ex As Exception
                txtResult.Text = Utilities.WrapExceptionforUI(ex)
                objPCILogger.IndicationSuccessFailure = "Failure" '6524
                objPCILogger.Message = "Bind_Click Failed. " & ex.Message.ToString() '6524
            Finally
                objPCILogger.PushLogToMSMQ() '6524
            End Try

        End Sub

        Private Sub SendingDataCopy()
            Dim strBldr As StringBuilder = New StringBuilder()
            strBldr.Append("<B>User Requesting Object:</B>" + "</br>")
            strBldr.Append("requestedInfo.ADDRESS_LINE1 = ""Test Address Line1""" + "</br>")
            strBldr.Append("requestedInfo.ADDRESS_LINE2 = ""Test Address Line2""" + "</br>")
            strBldr.Append("requestedInfo.ADDRESS_LINE3 = """"" + "</br>")
            strBldr.Append("requestedInfo.CITY = ""Test City""" + "</br>")
            strBldr.Append("requestedInfo.COMPANY_NAME = ""Test Company""" + "</br>")
            strBldr.Append("requestedInfo.CONTACT_NUMBER = ""000-000-0000""" + "</br>")
            strBldr.Append("requestedInfo.COUNTRY = ""USA""" + "</br>")
            strBldr.Append("requestedInfo.DEPARTMENT = ""Technology""" + "</br>")
            'strBldr.Append("requestedInfo.EMAIL = ""Test@Test.com""" + "</br>")
            strBldr.Append("requestedInfo.FAX = ""111-111-1111""" + "</br>")
            strBldr.Append("requestedInfo.FIRST_NAME = ""Test First Name""" + "</br>")
            strBldr.Append("requestedInfo.LAST_NAME = ""Test Last name""" + "</br>")
            strBldr.Append("requestedInfo.PASSWORD = ""Password2""" + "</br>")
            strBldr.Append("requestedInfo.PHONE = ""000-000-0000""" + "</br>")
            strBldr.Append("requestedInfo.POSTAL_CODE = ""11111""" + "</br>")
            'strBldr.Append("Dim objThirdId As User._thirdIDstructure = New User._thirdIDstructure()" + "</br>")
            'strBldr.Append("requestedInfo.SECRETPIN_ANSWER = ""PIN1""" + "</br>")
            'strBldr.Append("requestedInfo.SECRETPIN_ANSWER2 = ""PIN2""" + "</br>")
            'strBldr.Append("requestedInfo.SECRETPIN_ANSWER3 = ""PIN3""" + "</br>")
            'strBldr.Append("requestedInfo.STATE = ""Test State""" + "</br>")
            'strBldr.Append("requestedInfo.USERID = ""TestUser""" + "</br>")
            strBldr.Append("requestedInfo.isUserValid = true" + "</br>")
            strBldr.Append("requestedInfo.userValid = true" + "</br>")


            strBldr.Append("</br>" + "</br>")
            strBldr.Append("<B>Account Info:</B>" + "</br>")
            strBldr.Append("acctInfo.accountType = " + txtAccountType.Text.Trim() + "</br>")
            strBldr.Append("acctInfo.duns = " + txtDuns.Text.Trim() + "</br>")
            strBldr.Append("acctInfo.account = " + txtAccount.Text.Trim() + "</br>")
            strBldr.Append("acctInfo.sonyContact = " + txtSonyContact.Text.Trim() + "</br>")

            strBldr.Append("</br>" + "</br>")
            strBldr.Append("<B>RequestingApplication= </B>" + txtadminID.Text.Trim() + "</br>")
            strBldr.Append("<B>RequestingUserID= </B>" + txtUserName.Text.Trim() + "</br>")
            strBldr.Append("<B>RequestingPassword= </B>" + txtPassword.Text.Trim() + "</br>")

            txtSendingData.InnerHtml = "<span class=""bodycopy"">" + strBldr.ToString() + "</span>"

        End Sub

        Private Sub SetUserObject(ByRef LocalUser As User)
            LocalUser.AddressLine1 = "Test Address Line1"
            LocalUser.AddressLine2 = "Test Address Line2"
            LocalUser.City = "Test City"
            LocalUser.CompanyName = "Test Company"
            LocalUser.Phone = "000-000-0000"
            LocalUser.EmailAddress = SPStxtEmail.Text
            LocalUser.Fax = "111-111-1111"
            LocalUser.FirstName = "Test First Name"
            LocalUser.LastName = "Test Last name"
            LocalUser.Password = "Password2"
            LocalUser.Zip = "11111"
            'Dim objThirdId As User._thirdIDstructure = New User._thirdIDstructure()
            ' objThirdId.UserSecurityPINs(0) = "PIN1"
            ' objThirdId.UserSecurityPINs(1) = "PIN2"
            ' objThirdId.UserSecurityPINs(2) = "PIN3"
            'LocalUser.ThirdID = objThirdId
            ' LocalUser.State = "Test State"
            'LocalUser.UserId = "TestUser"
            ' LocalUser.UserType = "AccountHolder"
        End Sub

        'Protected Sub rdoEnvironment_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdoEnvironment.SelectedIndexChanged
        '    txtResult.Text = ""
        '    Select Case rdoEnvironment.SelectedIndex
        '        Case 0
        '            GetEnvironmentArgs()
        '        Case 1
        '            EnableFields()
        '        Case Else
        '            Return
        '    End Select
        'End Sub

        'Private Function GetAccountInfo() As AccountInfoVO
        '    Dim acctInfo As AccountInfoVO = New AccountInfoVO()
        '    Select Case rdoEnvironment.SelectedIndex
        '        Case 0
        '            acctInfo.accountType = 10001
        '            acctInfo.duns = ""
        '            acctInfo.account = "1234567"
        '            acctInfo.sonyContact = "R. Upton"
        '        Case 1
        '            acctInfo.accountType = Convert.ToInt64(txtAccountType.Text.Trim())
        '            acctInfo.duns = ""
        '            acctInfo.account = txtAccount.Text.Trim()
        '            acctInfo.sonyContact = txtSonyContact.Text.Trim()
        '    End Select
        '    Return acctInfo
        'End Function
        Private Sub GetEnvironmentArgs()
            Try
                ConfigurationData.IDMinderNewUserTest.Load(ConfigurationData.GeneralSettings.Environment)
                txtURL.Text = ConfigurationData.IDMinderNewUserTest.Url
                txtadminID.Text = ConfigurationData.IDMinderNewUserTest.Adminid
                txtUserName.Text = ConfigurationData.IDMinderNewUserTest.UserName
                txtPassword.Text = Encryption.DeCrypt(ConfigurationData.IDMinderNewUserTest.Password, Encryption.PWDKey)
                txtNumberOfTries.Text = 1
                txtSendingData.InnerHtml = ""
                txtSendingData.InnerText = ""
                txtResult.Text = ""

                txtAccountType.Text = "10001"
                txtDuns.Text = ""
                txtAccount.Text = "1234567890"
                txtSonyContact.Text = "R. Upton"

                DisableFields()
                SendingDataCopy()
            Catch ex As Exception
                txtResult.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Sub DisableFields()
            txtURL.Enabled = False
            txtadminID.Enabled = False
            txtUserName.Enabled = False
            txtPassword.Enabled = False
            txtNumberOfTries.Enabled = False
            txtResult.Enabled = False

            txtAccountType.Enabled = False
            txtDuns.Enabled = False
            txtAccount.Enabled = False
            txtSonyContact.Enabled = False

        End Sub
        Private Sub EnableFields()
            txtURL.Enabled = True
            txtadminID.Enabled = True
            txtUserName.Enabled = True
            txtPassword.Enabled = True
            txtNumberOfTries.Enabled = True

            txtAccountType.Enabled = True
            txtDuns.Enabled = True
            txtAccount.Enabled = True
            txtSonyContact.Enabled = True
        End Sub


        Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            If Not IsPostBack Then
                'ddlEnviroment.Items.Add("Development")
                'ddlEnviroment.Items.Add("QA")
                'ddlEnviroment.Items.Add("QA2008")
                'ddlEnviroment.Items.Add("Staging")
                'ddlEnviroment.Items.Add("Production")
                'GetEnvironmentArgs()
            End If
            Try
                ' lbl_envname.Text = ConfigurationData.GeneralSettings.Environment
                GetEnvironmentArgs()
                EnableFields()
                DisableFields()
            Catch ex As Exception
                SingletonLogHelper.Instance.LogException(ex, "Page load", "Page load")
                Dim sUnique As String = Utilities.GetUniqueKey()
                Dim oException As ServicesPLUSBaseException = New ServicesPLUSBaseException()
                oException.ErrorDescription = ex.Message
                oException.MInnerException = ex.InnerException
                Utilities.LogException(oException, "Application error", sUnique)
            Finally
                SingletonLogHelper.Instance.LogMember("Page load", "Page load")
            End Try
            'rdoEnvironment.Items(0).Text = ConfigurationData.GeneralSettings.Environment
        End Sub

        'Protected Sub ddlEnviroment_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlEnviroment.SelectedIndexChanged
        '    'Try
        '    '    EnableFields()
        '    '    GetEnvironmentArgs()
        '    '    DisableFields()

        '    'Catch ex As Exception
        '    '    SingletonLogHelper.Instance.LogException(ex, "IDMinderNewUserTest.ddlEnviroment_SelectedIndexChanged()", "ddlEnviroment_SelectedIndexChanged()")
        '    '    Dim sUnique As String = Utilities.GetUniqueKey()
        '    '    Dim oException As ServicesPLUSBaseException = New ServicesPLUSBaseException()
        '    '    oException.ErrorDescription = ex.Message
        '    '    oException.MInnerException = ex.InnerException
        '    '    Utilities.LogException(oException, "Application error", sUnique)
        '    'Finally
        '    '    SingletonLogHelper.Instance.LogMember("IDMinderNewUserTest.ddlEnviroment_SelectedIndexChanged", "ddlEnviroment_SelectedIndexChanged")

        '    'End Try
        'End Sub
       
    End Class
End Namespace
