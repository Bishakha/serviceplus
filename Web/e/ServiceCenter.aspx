<%@ Reference Page="~/e/Utility.aspx" %>

<%@ Page Language="vb" AutoEventWireup="false" Inherits="SIAMAdmin.ServiceCenter"
    CodeFile="ServiceCenter.aspx.vb" CodeFileBaseClass="SIAMAdmin.UtilityClass" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>Service Center</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <%--<link href="includes/style.css" type="text/css" rel="stylesheet">--%>
    <link href="includes/ServicesPLUS_style.css" rel="stylesheet" type="text/css" />

    <script language="javascript">
        function ConfirmDelete() {
            window.event.returnValue = confirm("This activity will delete selected mapping...");
        }
        window.parent.status = "Manage Service Center is open.";
    </script>

    <style type="text/css">
        Body
        {
            filter: chroma(color=#FFFFFF);
            scrollbar-face-color: #c9d5e6;
            scrollbar-shadow-color: #632984;
            scrollbar-highlight-color: #632984;
            scrollbar-3dlight-color: #130919;
            scrollbar-darkshadow-color: #130919;
            scrollbar-track-color: #130919;
            scrollbar-arrow-color: black;
        }
        </style>
</head>
<body>
    <form id="Form1" method="post" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <table width="100%" align="center" border="0">
        <tr>
            <td align="center" height="20">
                <asp:Label ID="lblHeader" CssClass="headerTitle" runat="server" Text="Find ServicesPLUS User">Maintain Service Center</asp:Label>
            </td>
        </tr>
        <tr>
            <td height="15">
            </td>
        </tr>
        <tr>
            <td>
                <table border="0" width="100%" align="center">
                    
                    <tr  height=20>
                        <td align="left">
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
                                    <asp:Label ID="ErrorLabel" runat="server" CssClass="redAsterick" EnableViewState="False"></asp:Label>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="btnAddNew" EventName="Click" />
                                    <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />
                                    <asp:AsyncPostBackTrigger ControlID="btnCancel" EventName="Click" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td align="Left">
                        <asp:UpdatePanel ID="FieldUpdatePanel" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <div id="div1" style="border-right: #c9d5e6 thin solid; width: 90%; border-top: #c9d5e6 thin solid;
                                        border-left: #c9d5e6 thin solid; border-bottom: #c9d5e6 thin solid">
                                        <table width="100%" cellspacing="0">
                                            <tr bgcolor="#c9d5e6" height="20px">
                                                <td width="25%"">
                                                    <asp:Label ID="NameLabel" CssClass="bodyCopyBold" runat="server">Primary Service Center</asp:Label>
                                                </td>
                                                <td width="75%"">
                                                    <SPS:SPSDropDownList ID="drpPrimaryServiceCenter" CssClass="bodyCopy" runat="server"
                                                        Width="98%" AutoPostBack="True">
                                                    </SPS:SPSDropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="25%"" colspan="2">
                                                    &nbsp;<asp:Label ID="lblExistingInfo" runat="server" CssClass="redAsterick" 
                                                        EnableViewState="False"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr bgcolor="#c9d5e6">
                                                <td width="25%"" " class="style1">
                                                    <asp:Label ID="SIAMUserID" runat="server" CssClass="bodyCopyBold">Secondary Service Center</asp:Label>
                                                </td>
                                                <td width="75%"">
                                                    <SPS:SPSDropDownList ID="drpSecondaryServiceCenter" CssClass="bodyCopy" runat="server"
                                                        Width="98%">
                                                    </SPS:SPSDropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="25%"">
                                                    &nbsp;
                                                </td>
                                                <td width="75%"">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr bgcolor="#c9d5e6">
                                                <td width="100%" colspan="2">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" align="left" width="100%">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" align="right" width="25%">
                                                    <asp:Button ID="btnAddNew" runat="server" CssClass="bodyCopyBold" 
                                                        Text="Add New" Width="70px">
                                                    </asp:Button>
                                                    &nbsp;&nbsp;&nbsp;
                                                    <asp:Button ID="btnSave" runat="server" CssClass="bodyCopyBold" Text="Save" 
                                                        Width="70px"></asp:Button>
                                                    &nbsp;&nbsp;&nbsp;
                                                    <asp:Button ID="btnCancel" runat="server" CssClass="bodyCopyBold" Text="Cancel" 
                                                        Width="70px">
                                                    </asp:Button>
                                                    &nbsp;&nbsp;
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="dgServiceCenter" EventName="ItemCommand" />
                                </Triggers>
                            </asp:UpdatePanel>
                            
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td align="Center">
                            <asp:UpdatePanel ID="GridUpdatePanel" runat="server" ChildrenAsTriggers="true">
                                <ContentTemplate>
                                    <asp:DataGrid ID="dgServiceCenter" runat="server" Width="100%" AutoGenerateColumns="False"
                                        AllowPaging="True" PageSize="8">
                                        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                        <EditItemStyle BackColor="#999999" />
                                        <SelectedItemStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                        <PagerStyle Mode="NumericPages" BackColor="#284775" CssClass="BodyCopy" ForeColor="White"
                                            HorizontalAlign="Center" />
                                        <AlternatingItemStyle BackColor="White" ForeColor="#284775" />
                                        <ItemStyle BackColor="#F7F6F3" ForeColor="#333333" />
                                        <HeaderStyle BackColor="#284775" ForeColor="White" CssClass="TableHeader" />
                                        <Columns>
                                            <asp:BoundColumn DataField="PRIMARYADDRESS" HeaderText="Primary Service Center">
                                                <ItemStyle Font-Bold="False" CssClass="tableData" Font-Italic="False" Font-Overline="False"
                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Left" VerticalAlign="Middle" />
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="SECONDERYADDRESS" HeaderText="Secondary Cervice Center">
                                                <ItemStyle Font-Bold="False" CssClass="tableData" Font-Italic="False" Font-Overline="False"
                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Left" VerticalAlign="Middle" />
                                            </asp:BoundColumn>
                                            <asp:TemplateColumn>
                                                <ItemTemplate>
                                                    <asp:LinkButton runat="server" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "PRIMARYSERVICECENTERID")%>'
                                                        CommandName="Edit" CssClass="bodyCopySM" Text="Edit" ID="lnkBtnEdit">
                                                    </asp:LinkButton>
                                                    <itemstyle cssclass="bodycopy" horizontalalign="Center" verticalalign="Middle" />
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn>
                                                <ItemTemplate>
                                                    <asp:LinkButton runat="server" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "PRIMARYSERVICECENTERID")%>'
                                                        CommandName="Delete" CssClass="bodyCopySM" Text="Delete" ID="lnkBtnDelete">
                                                    </asp:LinkButton>
                                                    <itemstyle font-bold="False" font-italic="False" font-overline="False" font-strikeout="False"
                                                        font-underline="False" horizontalalign="Center" verticalalign="Middle" />
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:BoundColumn DataField="PRIMARYSERVICECENTERID" Visible="False"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="SECONDARYSERVICECENTERID" Visible="False"></asp:BoundColumn>
                                        </Columns>
                                    </asp:DataGrid>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="btnAddNew" EventName="Click" />
                                    <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />
                                    <asp:AsyncPostBackTrigger ControlID="btnCancel" EventName="Click" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <input type="hidden" id="hdnLastOpr" runat="server" />
    </form>
</body>
</html>
