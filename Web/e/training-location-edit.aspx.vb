Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Data
Imports Sony.US.ServicesPLUS.Process
Imports System.Text.RegularExpressions
Imports System.Drawing
Imports ServicesPlusException
Imports Sony.US.AuditLog

Namespace SIAMAdmin

    Partial Class training_location_edit
        Inherits UtilityClass
        'Dim objUtilties As Utilities = New Utilities

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub


        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            MyBase.IsProtectedPage(True, True)
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Dim locationSelected As CourseLocation
            Dim itemNumber As Integer = -1

            If Not IsPostBack Then
                Try
                    LoadDDLS()
                    PopulateCountryDropDown()
                    If (Request.QueryString("LineNo") IsNot Nothing) And (Session.Item("training-location-list") IsNot Nothing) Then
                        itemNumber = Convert.ToInt16(Request.QueryString("LineNo"))
                        locationSelected = CType(Session.Item("training-location-list"), Array)(itemNumber)
                        txtAccomodation.Text = locationSelected.Accomodations
                        txtAddress1.Text = locationSelected.Address1
                        txtAddress2.Text = locationSelected.Address2
                        txtAddress3.Text = locationSelected.Address3
                        txtDirection.Text = locationSelected.Direction
                        txtExtension.Text = locationSelected.Extension
                        txtFax.Text = locationSelected.Fax
                        txtName.Text = locationSelected.Name
                        txtNotes.Text = locationSelected.Notes
                        txtPhone.Text = locationSelected.Phone
                        txtZip.Text = locationSelected.Zip
                        txtCity.Text = locationSelected.City
                        ddlState.SelectedValue = locationSelected.State
                        ddlCountry.SelectedValue = locationSelected.Country.Country
                    End If
                Catch ex As Exception
                    ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                End Try
            End If
        End Sub

        Private Sub LoadDDLS()
            ddlState.BackColor = Color.White
            ddlState.Items.Add(New ListItem("Select One", ""))
            ddlState.Items.Add(New ListItem("AL", "AL"))
            ddlState.Items.Add(New ListItem("AK", "AK"))
            ddlState.Items.Add(New ListItem("AR", "AR"))
            ddlState.Items.Add(New ListItem("AZ", "AZ"))
            ddlState.Items.Add(New ListItem("CA", "CA"))
            ddlState.Items.Add(New ListItem("CO", "CO"))
            ddlState.Items.Add(New ListItem("CT", "CT"))
            ddlState.Items.Add(New ListItem("DC", "DC"))
            ddlState.Items.Add(New ListItem("DE", "DE"))
            ddlState.Items.Add(New ListItem("FL", "FL"))
            ddlState.Items.Add(New ListItem("GA", "GA"))
            ddlState.Items.Add(New ListItem("HI", "HI"))
            ddlState.Items.Add(New ListItem("IA", "IA"))
            ddlState.Items.Add(New ListItem("ID", "ID"))
            ddlState.Items.Add(New ListItem("IL", "IL"))
            ddlState.Items.Add(New ListItem("IN", "IN"))
            ddlState.Items.Add(New ListItem("KS", "KS"))
            ddlState.Items.Add(New ListItem("KY", "KY"))
            ddlState.Items.Add(New ListItem("LA", "LA"))
            ddlState.Items.Add(New ListItem("MA", "MA"))
            ddlState.Items.Add(New ListItem("MD", "MD"))
            ddlState.Items.Add(New ListItem("ME", "ME"))
            ddlState.Items.Add(New ListItem("MI", "MI"))
            ddlState.Items.Add(New ListItem("MN", "MN"))
            ddlState.Items.Add(New ListItem("MO", "MO"))
            ddlState.Items.Add(New ListItem("MS", "MS"))
            ddlState.Items.Add(New ListItem("MT", "MT"))
            ddlState.Items.Add(New ListItem("NC", "NC"))
            ddlState.Items.Add(New ListItem("ND", "ND"))
            ddlState.Items.Add(New ListItem("NE", "NE"))
            ddlState.Items.Add(New ListItem("NH", "NH"))
            ddlState.Items.Add(New ListItem("NJ", "NJ"))
            ddlState.Items.Add(New ListItem("NM", "NM"))
            ddlState.Items.Add(New ListItem("NV", "NV"))
            ddlState.Items.Add(New ListItem("NY", "NY"))
            ddlState.Items.Add(New ListItem("OH", "OH"))
            ddlState.Items.Add(New ListItem("OK", "OK"))
            ddlState.Items.Add(New ListItem("OR", "OR"))
            ddlState.Items.Add(New ListItem("PA", "PA"))
            ddlState.Items.Add(New ListItem("RI", "RI"))
            ddlState.Items.Add(New ListItem("SC", "SC"))
            ddlState.Items.Add(New ListItem("SD", "SD"))
            ddlState.Items.Add(New ListItem("TN", "TN"))
            ddlState.Items.Add(New ListItem("TX", "TX"))
            ddlState.Items.Add(New ListItem("UT", "UT"))
            ddlState.Items.Add(New ListItem("VA", "VA"))
            ddlState.Items.Add(New ListItem("VT", "VT"))
            ddlState.Items.Add(New ListItem("WA", "WA"))
            ddlState.Items.Add(New ListItem("WI", "WI"))
            ddlState.Items.Add(New ListItem("WV", "WV"))
            ddlState.Items.Add(New ListItem("WY", "WY"))
            ddlState.Items.Add(New ListItem("ON", "ON"))
        End Sub

        Private Sub PopulateCountryDropDown()
            Try
                Dim courseDataManager As New CourseDataManager
                Dim countriess() As CourseCountry = courseDataManager.GetCountry()
                For Each cl As CourseCountry In countriess
                    ddlCountry.Items.Add(New ListItem(cl.TerritoryName, cl.Country))
                Next
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Sub btnUpdate_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles btnUpdate.Click
            Dim objPCILogger As New PCILogger() '6524
            Dim cm As New CourseManager
            Dim bUpdate As Boolean = False
            Dim locationSelected As CourseLocation

            If Not ValidateForm() Then
                ErrorLabel.Text = "Error entering location info:" + ErrorLabel.Text
                Return
            End If

            Try
                objPCILogger.EventOriginApplication = "ServicesPLUS"
                objPCILogger.EventOriginApplicationLocation = Page.GetType().Name
                objPCILogger.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
                objPCILogger.OperationalUser = Environment.UserName
                objPCILogger.EventDateTime = DateTime.Now.ToLongTimeString()
                objPCILogger.HTTPRequestObjectValues = objPCILogger.GetRequestContextValue()
                objPCILogger.IndicationSuccessFailure = "Success"
                objPCILogger.EventOriginMethod = "btnUpdate_Click"

                If (Not Request.QueryString("LineNo") Is Nothing) Then 'Update
                    bUpdate = True
                    Dim itemNumber As Integer = -1
                    itemNumber = Convert.ToInt16(Request.QueryString("LineNo"))
                    locationSelected = CType(Session.Item("training-location-list"), Array)(itemNumber)
                Else
                    locationSelected = New CourseLocation
                End If

                locationSelected.Accomodations = txtAccomodation.Text
                locationSelected.Address1 = txtAddress1.Text
                locationSelected.Address2 = txtAddress2.Text
                locationSelected.Address3 = txtAddress3.Text
                locationSelected.Direction = txtDirection.Text
                locationSelected.Extension = txtExtension.Text
                locationSelected.Fax = txtFax.Text
                locationSelected.Name = txtName.Text
                locationSelected.Notes = txtNotes.Text
                locationSelected.Phone = txtPhone.Text
                locationSelected.City = txtCity.Text
                locationSelected.Zip = txtZip.Text
                locationSelected.State = ddlState.SelectedValue
                locationSelected.Country.Country = ddlCountry.SelectedValue

                If bUpdate Then
                    objPCILogger.Message = "UpdateCourseLocation Success."
                    objPCILogger.EventType = EventType.Update
                    cm.UpdateCourseLocation(locationSelected)
                Else
                    objPCILogger.Message = "AddCourseLocation Success."
                    objPCILogger.EventType = EventType.Add
                    cm.AddCourseLocation(locationSelected)
                End If

            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                objPCILogger.IndicationSuccessFailure = "Failure" '6524
                objPCILogger.Message = "Add/Update CourseLocation Failed. " & ex.Message.ToString() '6524
            Finally
                objPCILogger.PushLogToMSMQ() '6524
            End Try
        End Sub

        Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
            Response.Redirect("training-location-list.aspx")
        End Sub

        Private Function ValidateForm() As Boolean
            Dim bValid = True

            ErrorLabel.Text = ""

            labelName.CssClass = "Body"
            labelAddress1.CssClass = "Body"
            labelAddress2.CssClass = "Body"
            labelAddress3.CssClass = "Body"
            labelCity.CssClass = "Body"
            labelCountry.CssClass = "Body"
            labelPhone.CssClass = "Body"
            labelZip.CssClass = "Body"
            labelState.CssClass = "Body"

            LabelNameStar.Text = ""
            LabelAddress1Star.Text = ""
            LabelAddress2Star.Text = ""
            LabelAddress3Star.Text = ""
            LabelCityStar.Text = ""
            LabelCountryStar.Text = ""
            LabelPhoneStar.Text = ""
            LabelZipStar.Text = ""
            LabelStateStar.Text = ""

            If txtName.Text = "" Then
                labelName.CssClass = "redAsterick"
                LabelNameStar.Text = "<span class=""redAsterick"">*</span>"
                bValid = False
            End If
            If txtAddress1.Text = "" Then
                labelAddress1.CssClass = "redAsterick"
                LabelAddress1Star.Text = "<span class=""redAsterick"">*</span>"
                bValid = False
            End If
            If txtAddress1.Text.Length > 35 Then
                labelAddress1.CssClass = "redAsterick"
                LabelAddress1Star.Text = "<span class=""redAsterick"">Street Address must be 35 characters or less in length.</span>"
                txtAddress1.Focus()
                bValid = False
            End If

            If txtAddress2.Text.Length > 35 Then
                labelAddress2.CssClass = "redAsterick"
                LabelAddress2Star.Text = "<span class=""redAsterick"">Address 2nd Line must be 35 characters or less in length.</span>"
                txtAddress2.Focus()
                bValid = False
            End If

            If txtAddress3.Text.Length > 35 Then
                labelAddress3.CssClass = "redAsterick"
                LabelAddress3Star.Text = "<span class=""redAsterick"">Address 3rd Line must be 35 characters or less in length.</span>"
                txtAddress3.Focus()
                bValid = False
            End If

            If txtCity.Text = "" Then
                labelCity.CssClass = "redAsterick"
                LabelCityStar.Text = "<span class=""redAsterick"">*</span>"
                bValid = False
            ElseIf txtCity.Text.Length > 35 Then
                labelCity.CssClass = "redAsterick"
                LabelCityStar.Text = "<span class=""redAsterick"">City must be 35 characters or less in length.</span>"
                txtCity.Focus()
                bValid = False
            End If
            If ddlState.SelectedIndex = 0 Then
                bValid = False
                labelState.CssClass = "redAsterick"
                LabelStateStar.Text = "<span class=""redAsterick"">*</span>"
            End If
            Dim sZip As String = txtZip.Text.Trim()
            Dim objZipCodePattern As New Regex("\d{5}(-\d{4})?")
            If Not objZipCodePattern.IsMatch(sZip) Then
                labelZip.CssClass = "redAsterick"
                LabelZipStar.Text = "<span class=""redAsterick"">*</span>"
                bValid = False
            End If
            Dim sPhoneNumber As String = txtPhone.Text.Trim()
            If sPhoneNumber = "" Then
                ErrorLabel.Visible = True
                labelPhone.CssClass = "redAsterick"
                LabelPhoneStar.Text = "<span class=""redAsterick"">*</span>"
                bValid = False
            Else
                Dim objHPhoneNumber As New Regex("(\d{3}[-]{1}\d{3}[-]{1}\d{4})")
                If Not objHPhoneNumber.IsMatch(sPhoneNumber) Then
                    bValid = False
                    ErrorLabel.Text = "Invalid Phone number."
                End If
            End If
            Dim sExtension As String = txtExtension.Text.Trim.ToString()
            If sExtension.Length > 0 Then
                Dim objExt As New Regex("^[0-9]*$")
                If Not objExt.IsMatch(sExtension) Or sExtension.Length > 4 Then
                    ErrorLabel.Text = "Extension must be numeric and 4 or fewer digits."
                    bValid = False
                End If
            End If
            Dim sFax As String = txtFax.Text.Trim.ToString()
            If sFax.Length > 0 Then
                Dim objHPhoneNumber As New Regex("(\d{3}[-]{1}\d{3}[-]{1}\d{4})")
                If Not objHPhoneNumber.IsMatch(sFax) Then
                    bValid = False
                    ErrorLabel.Text = "Invalid Fax number."
                End If
            End If
            Return bValid
        End Function
    End Class

End Namespace
