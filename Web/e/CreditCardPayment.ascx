<%@ Control Language="vb" AutoEventWireup="false" Inherits="SIAMAdmin.CreditCardPayment"
    CodeFile="CreditCardPayment.ascx.vb" %>
<link href="includes/style.css" type="text/css" rel="stylesheet">
<table id="Table1" width="100%" border="0">
    <tr>
        <td style="height: 16px" colspan="2">
            <asp:Label ID="Label19" runat="server" CssClass="headerBig"> Credit Card Information:</asp:Label>
        </td>
    </tr>
    <tr>
        <td align="Left" colspan="1" rowspan="1" width="40%">
            <asp:Label ID="LabelName" runat="server" CssClass="bodycopybold">Name as it appears on credit card:</asp:Label>
        </td>
        <td width="85%">
            <SPS:SPSTextBox ID="txtName" runat="server" CssClass="bodycopy" Width="136px"></SPS:SPSTextBox>
            <asp:Label ID="LabelNameStar" CssClass="redAsterick" runat="server"><span class="redAsterick">*</span></asp:Label>
        </td>
    </tr>
    <tr>
        <td align="Left" colspan="1" width="40%">
            <asp:Label ID="LabelName0" runat="server" CssClass="bodycopybold">Bill to Company Name:</asp:Label>
        </td>
        <td width="85%">
            <SPS:SPSTextBox ID="txtCompanyName" runat="server" CssClass="bodycopy" Width="136px"></SPS:SPSTextBox>
        </td>
    </tr>
    <tr>
        <td align="Left" width="40%">
            <asp:Label ID="LabelLine1" runat="server" CssClass="bodycopybold">Street Address:</asp:Label>
        </td>
        <td width="85%">
            <SPS:SPSTextBox ID="txtStreet1" runat="server" CssClass="bodycopy" Width="136px"></SPS:SPSTextBox>
            <asp:Label ID="LabelLine1Star" CssClass="redAsterick" runat="server">
																					<span class="redAsterick">*</span>
            </asp:Label>
        </td>
    </tr>
    <tr>
        <td align="Left" width="40%">
            <asp:Label ID="LabelLine2" runat="server" CssClass="bodycopybold">Address 2nd Line:</asp:Label>
        </td>
        <td width="85%">
            <SPS:SPSTextBox ID="txtStreet2" runat="server" CssClass="bodycopy" Width="144px"></SPS:SPSTextBox>
             <asp:Label ID="LabelLine2Star" CssClass="redAsterick" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td align="Left" width="40%">
            <asp:Label ID="LabelLine3" runat="server" CssClass="bodycopybold">Address 3rd Line:</asp:Label>
        </td>
        <td width="85%">
            <SPS:SPSTextBox ID="txtStreet3" runat="server" CssClass="bodycopy" Width="144px"></SPS:SPSTextBox>
             <asp:Label ID="LabelLine3Star" CssClass="redAsterick" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td align="Left" width="40%">
            <asp:Label ID="LabelCity" CssClass="bodycopybold" runat="server">Bill to City:</asp:Label>
        </td>
        <td width="85%">
            <SPS:SPSTextBox ID="TextBoxCity" CssClass="bodycopy" runat="server" Width="160px"></SPS:SPSTextBox>
            <asp:Label ID="LabelCityStar" CssClass="redAsterick" runat="server">
																					<span class="redAsterick">*</span>
            </asp:Label>
        </td>
    </tr>
    <tr>
        <td align="Left" width="40%">
            <asp:Label ID="LabelState" runat="server" CssClass="bodycopybold">Bill to State:</asp:Label>
        </td>
        <td width="85%">
            <asp:DropDownList ID="DDLState" CssClass="bodycopy" runat="server" BackColor="White">
            </asp:DropDownList>
            <asp:Label ID="LabelStateStar" CssClass="redAsterick" runat="server">
																					<span class="redAsterick">*</span>
            </asp:Label>
        </td>
    </tr>
    <tr>
        <td align="Left" width="40%">
            <asp:Label ID="LabelZip" runat="server" CssClass="bodycopybold">Bill to Postal Code:</asp:Label>
        </td>
        <td width="85%">
            <SPS:SPSTextBox ID="txtZip" runat="server" CssClass="bodycopy" Width="96px"></SPS:SPSTextBox>
            <asp:Label ID="LabelZipStar" CssClass="redAsterick" runat="server">
																					<span class="redAsterick">*</span>
            </asp:Label>
        </td>
    </tr>
    <tr>
        <td align="Left" width="40%">
            <asp:Label ID="Label1" runat="server" CssClass="bodycopybold">Type:</asp:Label>
        </td>
        <td width="85%">
            <asp:DropDownList ID="ddlType" runat="server" CssClass="bodycopy" Width="96px">
            </asp:DropDownList>
            <asp:Label ID="LabelCardStar" CssClass="redAsterick" runat="server">
																					<span class="redAsterick">*</span>
            </asp:Label>
        </td>
    </tr>
    <tr>
        <td align="Left" colspan="1" rowspan="1" width="40%">
            <asp:Label ID="LabelCCNumber" runat="server" CssClass="bodycopybold">CC Number:</asp:Label>
        </td>
        <td width="85%">
            <SPS:SPSTextBox ID="txtCCNumber" runat="server" CssClass="bodycopy"></SPS:SPSTextBox>
            <asp:Label ID="LabelCCNumberStar" CssClass="redAsterick" runat="server" Text="*"></asp:Label>
        </td>
    </tr>
    <tr>
        <td align="Left" colspan="1" rowspan="1" width="40%">
        </td>
        <td style="height: 23px" width="85%">
            <asp:Label ID="lblValidCCNumber" CssClass="redAsterick" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td align="Left" width="40%">
            <asp:Label ID="Label3" runat="server" CssClass="bodycopybold">Expiration Date:</asp:Label>
        </td>
        <td width="85%">
            <SPS:SPSTextBox ID="txtExpireationDate" runat="server" CssClass="bodycopy" Width="144px"></SPS:SPSTextBox><asp:Label
                ID="Label5" runat="server" CssClass="bodycopy">(MM/YYYY)</asp:Label>
            <asp:Label ID="LabelExpStar" CssClass="redAsterick" runat="server"><span class="redAsterick">*</span></asp:Label>
        </td>
    </tr>
    <%--<TR>
		<TD style="WIDTH: 133px; HEIGHT: 25px" align="right"><asp:label id="Label4" runat="server" CssClass="bodycopy">Security Code:</asp:label></TD>
		<TD style="HEIGHT: 25px"><SPS:SPSTextBox id="txtCCSecurityCode" runat="server" CssClass="bodycopy"></SPS:SPSTextBox></TD>
	</TR>--%>
</table>
