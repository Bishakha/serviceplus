<%@ Control Language="vb" AutoEventWireup="false" Inherits="SIAMAdmin.SiamAdmin_Header"
    CodeFile="SiamAdmin_Header.ascx.vb" %>
<table width="100%" border="0"  bgcolor="#ffffff">
    <tr>
        <td>
            <table width="100%" border="0" cellspacing="0">
                <tr>
                    
                    <td bgcolor="white" width=10%>
                        &nbsp;&nbsp;<img src="images/siam001.gif" width="91" height="52">
                    </td>
                    
                    <td bgcolor="white" align="left" width=70%>
                        <asp:Label ID="lblEnv" runat="server" Width="100%" Height="32px" Font-Bold="True"
                            Font-Names="Arial,Helvetica,sans-serif" Visible="False"></asp:Label><br />
                        <span class="redAsterick">Please be advised that some of the information shown in this application is considered to be Secret and should only be revealed to authorized persons.</span>
                    </td>
                    <td bgcolor="white" width=20% align="right" valign="Middle" rowspan="1">
                        <asp:Label ID="lblCSSIAMLogo" runat="server" Text="ServicesPLUS Admin" Font-Bold="True"
                            Font-Names="Arial,Helvetica,sans-serif" Font-Size="10pt" ForeColor="Black" 
                            ></asp:Label>&nbsp;&nbsp;
                        <asp:Label ID="lblBuildNo" runat="server" Font-Bold="True" Font-Names="Arial,Helvetica,sans-serif"
                            Font-Size="10pt" ForeColor="Black"></asp:Label>&nbsp;&nbsp;<br />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    
    <tr height="1px">
       
        <td height="1px" valign="top" align="right" bgcolor="#363d44">
           
        </td>
    </tr>
    
    <tr height="27">
        <td width="100%" bgcolor="#c9d5e6" height="20" align="right">
            <table border="0" width="100%" align="center">
                
                <tr width="100%">
                    <td width="40%" align="left">
                        &nbsp;&nbsp;<asp:Label ID="lblUser" runat="server" CssClass="BodyHead"></asp:Label>
                    </td>
                    
                    <td width="60%" align="right">
                        <img height="1" src="images/spacer.gif"><asp:Label ID="lblDateTime" CssClass="BodyHead"
                            runat="server"></asp:Label>&nbsp;&nbsp;<br />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr height="1px">
       
        <td height="1px" valign="top" align="right" bgcolor="#363d44">
           
        </td>
    </tr>
    
</table>
