
function LoadContentFrame(page)
{
	//window.status = "Please wait while the page is loading...";
	window.frames[0].location = page;	
	window.status="";
}

function logout()
{
	document.location.href="default.aspx?logout=true";
}

function popupLineItems(url, newWidth, newHeight, isInvoice) 
{
   var newUrl
   
   if (isInvoice)
    newUrl = url + "&Invoice=True"
   else
    newUrl = url + "&Invoice=False"
   window.open(newUrl,"","toolbar=0,location=0,directories=0,status=0,menubar=0,scrollbars=no,resize=0,width=" + newWidth + ",height=" + newHeight);
}

function FIND(item) {
	if( window.mmIsOpera ) return(document.getElementById(item));
	if (document.all) return(document.all[item]);
	if (document.getElementById) return(document.getElementById(item));
	return(false);
}

function confirmDelete(question) 
{
	debugger
	return window.confirm(question);
}


function SetTheFocusButton(e, thisControlsName)
{
	var charCode = (navigator.appName === "Netscape") ? e.which : e.keyCode;				
	if (charCode === 13) { document.getElementById(thisControlsName.toString()).focus(); }
}	