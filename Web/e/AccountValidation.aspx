<%@ Reference Page="~/e/Utility.aspx" %>
<%@ Register TagPrefix="SIAMCustomerService" TagName="CustomerSubNav" Src="CustomerDataSubNavigation.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" smartnavigation=true Inherits="SIAMAdmin.AccountValidation" CodeFile="AccountValidation.aspx.vb" CodeFileBaseClass="SIAMAdmin.UtilityClass" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>AccountValidation</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="includes/style.css" type="text/css" rel="stylesheet">
        
	</HEAD>
	<body >
		<form id="Form1" method="post" runat="server">
			<table width=98% border="0" align="left">
				<tr>
					<td class="PageHead" align="center" width="100%" height="30" runat=server id=headerText></td>
				</tr>
				<tr>
					<td align=center>&nbsp;&nbsp;
					</td>
				</tr>
				<tr runat=server id = headerNav>
					<td align="center">
						<table width="100%" border="0">
							<tr>
								<td><SIAMCUSTOMERSERVICE:CUSTOMERSUBNAV id="CustomerSubNav" runat="server"></SIAMCUSTOMERSERVICE:CUSTOMERSUBNAV></td>
							</tr>
							<tr>
								<td height="10"></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td vAlign="top">
						<table width="100%" align="left" border="0">
							<tr>
								<td align="center" colSpan="4" style="width: 828px"><asp:label id="ErrorLabel" runat="server" ForeColor="Red" CssClass="Body" EnableViewState="False"></asp:label></td>
							</tr>
							<tr>
								<td bgColor="#000000" colSpan="4" style="width: 828px"></td>
							</tr>
							<tr bgColor="threeddarkshadow" height="20">
								<td align="center" colSpan="4" style="width: 828px"><span class="BodyHead" runat=server ><span color="white" runat=server ></span></span>
								</td>
							</tr>
							<tr>
								<td bgColor="#000000" colSpan="4" style="width: 828px"></td>
							</tr>
							<tr>
								<td bgColor="slategray" colSpan="4" height="17" style="width: 828px">
									<table border="0">
										<tr>
											<td width="50"></td>
											<td><span class="Body"><span color="#ffffff">Customer Name:&nbsp;</span></span></td>
											<td><b><asp:label id="CustomerLabel" runat="server" ForeColor="White" CssClass="Body"></asp:label></b></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td bgColor="#000000" colSpan="4" style="width: 828px"></td>
							</tr>
							<tr>
								<td bgColor="lightslategray" colSpan="4" height="17" style="width: 828px">
									<table border="0">
										<tr>
											<td width="50"></td>
											<td align="right"><span class="Body"><span color="#ffffff">Address1:&nbsp;</span></span></td>
											<td><b><asp:label id="Address1Label" runat="server" ForeColor="White" CssClass="Body"></asp:label></b></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td bgColor="#000000" colSpan="4" style="width: 828px"></td>
							</tr>
							<tr>
								<td bgColor="slategray" colSpan="4" height="17" style="width: 828px">
									<table border="0">
										<tr>
											<td width="50"></td>
											<td align="right"><span class="Body"><span color="#ffffff">Address2:&nbsp;</span></span></td>
											<td><b><asp:label id="Address2Label" runat="server" ForeColor="White" CssClass="body"></asp:label></b></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td bgColor="#000000" colSpan="4" style="width: 828px"></td>
							</tr>
								<tr>
								<td bgColor="slategray" colSpan="4" height="17" style="width: 828px">
									<table border="0">
										<tr>
											<td width="50"></td>
											<td align="right"><span class="Body"><span color="#ffffff">Address3:&nbsp;</span></span></td>
											<td><b><asp:label id="Address3Label" runat="server" ForeColor="White" CssClass="body"></asp:label></b></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td bgColor="#000000" colSpan="4" style="width: 828px"></td>
							</tr>
							<tr>
								<td bgColor="lightslategray" colSpan="4" height="17" style="width: 828px">
									<table border="0">
										<tr>
											<td width="50"></td>
											<td align="right"><span class="Body"><span color="#ffffff">City:&nbsp;</span></span></td>
											<td><b><asp:label id="CityLabel" runat="server" ForeColor="White" CssClass="body"></asp:label></b></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td bgColor="#000000" colSpan="4" style="width: 828px"></td>
							</tr>
							<tr>
								<td bgColor="slategray" colSpan="4" height="17" style="width: 828px">
									<table border="0">
										<tr>
											<td width="50"></td>
											<td align="right"><span class="Body"><span color="#ffffff">State:&nbsp;</span></span></td>
											<td><b><asp:label id="StateLabel" runat="server" ForeColor="White" CssClass="body"></asp:label></b></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td bgColor="#000000" colSpan="4" style="width: 828px"></td>
							</tr>
							<tr>
								<td bgColor="lightslategray" colSpan="4" height="17" style="width: 828px">
									<table border="0">
										<tr>
											<td width="50"></td>
											<td align="right"><span class="Body"><span color="#ffffff">Zip:&nbsp;</span></span></td>
											<td><b><asp:label id="ZipLabel" runat="server" ForeColor="White" CssClass="body"></asp:label></b></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td bgColor="#000000" colSpan="4" style="width: 828px"></td>
							</tr>
							<tr>
								<td bgColor="slategray" colSpan="4" height="17" style="width: 828px">
									<table border="0">
										<tr>
											<td width="50"></td>
											<td align="right"><span class="Body"><span color="#ffffff">Phone:&nbsp;</span></span></td>
											<td><b><asp:label id="PhoneLabel" runat="server" ForeColor="White" CssClass="body"></asp:label></b></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td bgColor="#000000" colSpan="4" style="width: 828px"></td>
							</tr>
							<tr>
								<td bgColor="lightslategray" colSpan="4" height="17" style="width: 828px">
									<table border="0">
										<tr>
											<td width="50"></td>
											<td align="right"><span class="Body"><span color="#ffffff">Email Address:&nbsp;</span></span></td>
											<td><b><asp:label id="EmailAddressLabel" runat="server" ForeColor="White" CssClass="body"></asp:label></b></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td bgColor="#000000" colSpan="4" style="width: 828px"></td>
							</tr>
							<tr>
								<td colSpan="4" style="height: 2px; width: 828px;">&nbsp;</td>
							</tr>
							<tr runat=server id = trSearchCompName>
								<td align="center" colSpan="4" style="width: 828px" >
						            <div id="divLDAP" runat="server" style="BORDER-RIGHT: gray thin solid; BORDER-TOP: gray thin solid; BORDER-LEFT: gray thin solid; BORDER-BOTTOM: gray thin solid">
									    <table border="0" width="99%">
										    <tr height="10">
											    <td width=30%><span class="BodyHead"> Search Company Name</span></td>
											    <td width=60% ><SPS:SPSTextBox id="tbCompanyName" runat="server" CssClass="Body" Width="100%"></SPS:SPSTextBox></td>
											    <td width=10%><asp:Button ID="btnCustomerSearch" runat="server" CssClass="Body" Text="Show Accounts" /></td>
										    </tr>
									    </table>
									</div>
								</td>
							</tr>
							<tr>
								<td colSpan="4" style="height: 2px; width: 828px;">&nbsp;</td>
							</tr>
							<tr runat=server id = trAccDetail>
								<td align="center" colSpan="4" style="width: 828px; height: 131px;" >
						            <div id="div1" runat="server" style="BORDER-RIGHT: gray thin solid; BORDER-TOP: gray thin solid; BORDER-LEFT: gray thin solid; BORDER-BOTTOM: gray thin solid">
									    <table border="0" width="99%" align="center">
                                                <tr height="10">
                                                <td align="left" colSpan="4" style="height: 10px; width: 821px;"><span class="BodyHead"> Account numbers (</span><span class="Body">Payer Number</span><span class="BodyHead">) supplied at registration</span>
                                                </td>
                                                </tr>
                                                <tr>
                                                <td colSpan="4" style="height: 3px; width: 821px;"></td>
                                                </tr>
                                                <tr>
                                                    <td align="left" colSpan="4" style="width:100%;">
                                                        <table id="AccountNumberTable" runat="server" class="Body" >
                                                            <tr width=100%>
                                                                <td><SPS:SPSTextBox id="AccountNumberTextBox1" runat="server" CssClass="Body" Width="70px"></SPS:SPSTextBox></td>
                                                                <td><SPS:SPSTextBox id="AccountNumberTextBox2" runat="server" CssClass="Body" Width="70px"></SPS:SPSTextBox></td>
                                                                <td><SPS:SPSTextBox id="AccountNumberTextBox3" runat="server" CssClass="Body" Width="70px"></SPS:SPSTextBox></td>
                                                                <td><SPS:SPSTextBox id="AccountNumberTextBox4" runat="server" CssClass="Body" Width="70px"></SPS:SPSTextBox></td>
                                                                <td><SPS:SPSTextBox id="AccountNumberTextBox5" runat="server" CssClass="Body" Width="70px"></SPS:SPSTextBox></td>
                                                                <td><SPS:SPSTextBox id="AccountNumberTextBox6" runat="server" CssClass="Body" Width="70px"></SPS:SPSTextBox></td>
                                                            </tr>
                                                            <tr width=100%>
                                                                <td><SPS:SPSTextBox id="AccountNumberTextBox7" runat="server" CssClass="Body" Width="70px"></SPS:SPSTextBox></td>
                                                                <td><SPS:SPSTextBox id="AccountNumberTextBox8" runat="server" CssClass="Body" Width="70px"></SPS:SPSTextBox></td>
                                                                <td><SPS:SPSTextBox id="AccountNumberTextBox9" runat="server" CssClass="Body" Width="70px"></SPS:SPSTextBox></td>
                                                                <td><SPS:SPSTextBox id="AccountNumberTextBox10" runat="server" CssClass="Body" Width="70px"></SPS:SPSTextBox></td>
                                                                <td><SPS:SPSTextBox id="AccountNumberTextBox11" runat="server" CssClass="Body" Width="70px"></SPS:SPSTextBox></td>
                                                                <td><SPS:SPSTextBox id="AccountNumberTextBox12" runat="server" CssClass="Body" Width="70px"></SPS:SPSTextBox></td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                <td colSpan="4" style="height: 12px; width: 821px;"><asp:label id="NoValidAccountNumbersLabel" runat="server" ForeColor="Red" CssClass="body" EnableViewState="False"></asp:label></td>
                                                </tr> 
                                                <tr>
								                        <td align="right" colSpan="4" width=100% ><asp:button id="btnAccountSearch" CssClass="Body" Runat="server" Text="Search"></asp:button>
                                                            <asp:button id="btnSave" runat="server" CssClass="Body" Text="Validate" ></asp:button></td>
							                    </tr>
									    </table>
									</div>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				
				<tr>
					<td align="left" style="width: 100%;" ><span class="BodyHead"><asp:label id="CustomerNameResults" runat="server" Visible="False">Detail Account Information</asp:label></span></td>
				</tr>
                <tr><td>&nbsp;</td></tr>
                <tr runat=server id = trDg>
                    <td align="left" style="width: 100%" valign="top">
                        <asp:label id="NoCustomersLabel" runat="server" ForeColor="Red" CssClass="body" EnableViewState="False"></asp:label>
                        <asp:datagrid id="dgSearchResult" CssClass="Body"  Runat="server" Visible="False" PageSize="5"
							AllowPaging="True" AutoGenerateColumns="False" AllowSorting="True" Width="100%">
							<Columns>
								<asp:TemplateColumn HeaderText="Payer Account">
									<ItemTemplate>
										
                                        <asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.AccountNumber") %>' ID="Label8">
										</asp:Label>&nbsp;
                                        <asp:ImageButton ID="imgExpand" CommandName="Expand" runat="server" ImageUrl="images/plus.gif"/>
										
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Company">
									<ItemTemplate>
										<asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.BillTo.Name") %>' ID="Label9">
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Address">
									<ItemTemplate>
										<asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.BillTo.Address1") + " " + DataBinder.Eval(Container, "DataItem.BillTo.Address2") %>' ID="Label10">
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="State">
									<ItemTemplate>
										<asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.BillTo.State") %>' ID="Label13">
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Zip">
									<ItemTemplate>
										<asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.BillTo.Zip").ToString() %>' ID="Label14">
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Add">
									<ItemTemplate>
										<asp:CheckBox runat="server" text='' ID="cb1"></asp:CheckBox>
								        </tr><tr width=100% ><td width=100% align=center colspan=6>
								            <table width=100% align=center runat=server id=innertable visible=false>
								            <tr height=2px>
								                <td colspan=3></td>
								            </tr>
								            <tr valign=middle>
								                <td colspan=1 width=2px>&nbsp;</td>
								                <td colspan=1 width=100%>
								                        <asp:datagrid id="dgInnerSAPNAME" Visible=false Width=100% CssClass="Body"  Runat="server" AutoGenerateColumns="false">
        								            
								                        <Columns>
								                        <asp:TemplateColumn HeaderText="Ship To Account">
									                            <ItemTemplate>
                                                                    <asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.AccountNumber") %>' ID="Label8">
										                            </asp:Label>&nbsp;
                            										
									                            </ItemTemplate>
								                                </asp:TemplateColumn>
								                                <asp:TemplateColumn HeaderText="Company">
									                                <ItemTemplate>
										                                <asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Address.Name") %>' ID="Label9">
										                                </asp:Label>
									                                </ItemTemplate>
								                                </asp:TemplateColumn>
								                                <asp:TemplateColumn HeaderText="Address">
									                                <ItemTemplate>
										                                <asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Address.Address1") + ", " + DataBinder.Eval(Container, "DataItem.Address.Address2") %>' ID="Label10">
										                                </asp:Label>
									                                </ItemTemplate>
								                                </asp:TemplateColumn>
								                                <asp:TemplateColumn HeaderText="State">
									                                <ItemTemplate>
										                                <asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Address.State") %>' ID="Label13">
										                                </asp:Label>
									                                </ItemTemplate>
								                                </asp:TemplateColumn>
								                                <asp:TemplateColumn HeaderText="Zip">
									                                <ItemTemplate>
										                                <asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Address.Zip").ToString() %>' ID="Label14">
										                                </asp:Label>
									                                </ItemTemplate>
								                                </asp:TemplateColumn>
								                     </Columns>
								                    </asp:datagrid>

								                </td>
								                <td colspan=1 width=2px>&nbsp;</td>
								            </tr>
								            <tr >
								                <td colspan=3 width=2px>&nbsp;</td>
								            </tr>
								            </table>
                                            <asp:Label runat="server" ID="lblNoSAPShipto" Visible=false></asp:Label>
								        </td></tr>
									</ItemTemplate>
								</asp:TemplateColumn>                                
							</Columns>
						</asp:datagrid></td>
                </tr>
				
				<tr>
					<td align="right" >&nbsp;</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
