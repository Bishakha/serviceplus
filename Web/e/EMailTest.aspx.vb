Imports System.Data
Imports System.Timers
Imports Oracle.ManagedDataAccess.Client
Imports Oracle.ManagedDataAccess.Types
Imports ServicesPlusException
Imports Sony.US.ServicesPLUS.Data
Imports Sony.US.SIAMUtilities

Namespace SIAMAdmin

    Partial Class EMailTest
        Inherits UtilityClass
        'Dim objUtilties As Utilities = New Utilities
        Protected Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
            IsProtectedPage(True, True)
            'If ConfigurationData.GeneralSettings.Environment.ToUpper() = "PRODUCTION" Then
            '    Response.Redirect("dafault.aspx")
            'End If
            If Request.UrlReferrer Is Nothing Then Response.Redirect("default.aspx")

        End Sub

        Protected Sub btnSend_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSend.Click
            Try
                Dim blnError As Boolean = False
                errNumberOfMail.Visible = False
                errEmailPerHour.Visible = False

                If IsNumeric(txtNumberOfEmail.Text.Trim().ToString()) = False Then
                    blnError = True
                ElseIf Convert.ToInt16(txtNumberOfEmail.Text.Trim().ToString()) <= 0 Then
                    blnError = True
                End If

                If blnError Then
                    errNumberOfMail.Text = "Number of emails should be numeric."
                    blnError = False
                    errNumberOfMail.Visible = True
                End If

                If IsNumeric(txtMailPerHour.Text.Trim().ToString()) = False Then
                    blnError = True
                ElseIf Convert.ToInt16(txtMailPerHour.Text.Trim().ToString()) <= 0 Then
                    blnError = True
                End If

                If blnError Then
                    errEmailPerHour.Text = "Emails per hour should be numeric."
                    blnError = False
                    errEmailPerHour.Visible = True
                End If

                If Not errEmailPerHour.Visible And Not errNumberOfMail.Visible Then
                    Dim dblNumberofMail As Double = Convert.ToInt16(txtNumberOfEmail.Text.Trim().ToString())
                    Dim dblNumberofMailPerHr As Double = Convert.ToInt16(txtMailPerHour.Text.Trim().ToString())
                    lblTestdurationResult.Text = ((dblNumberofMail * 60 / dblNumberofMailPerHr)).ToString() + "  Minutes" + "<br/>" + "<span class=""redasterick"">Process will be running in backend. Use email log to see the status.</span>"
                    Dim mailObj As New ServicesPLUSMailObject With {
                        .From = txtFrom.Text.Trim(),
                        .To = txtTo.Text.Trim(),
                        .SMTP = txtSMTP.Text.Trim(),
                        .Subject = "ServicesPLUS Admin Test Mail",
                        .BCC = "",
                        .Body = "Test Mail",
                        .IsHTML = False,
                        .Mail_Priority = Net.Mail.MailPriority.Normal,
                        .Port = 25
                    }

                    Dim objTestMailer As New TestMailer()

                    Dim newThread As New Threading.Thread(AddressOf objTestMailer.StartMailling)
                    Dim objParam() As Object = {Convert.ToInt16(txtNumberOfEmail.Text.Trim().ToString()), Convert.ToInt16(txtMailPerHour.Text.Trim().ToString()), mailObj}
                    'objTestMailer.StartMailling(objParam)
                    newThread.Start(objParam)
                    btnSend.Enabled = False
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Protected Sub Page_Load1(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
            Try
                If Not IsPostBack Then GetEnvironmentArgs()
                rdoEnvironment.Items(0).Text = ConfigurationData.GeneralSettings.Environment
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Protected Sub rdoEnvironment_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles rdoEnvironment.SelectedIndexChanged
            Try
                lblTestdurationResult.Text = ""
                Select Case rdoEnvironment.SelectedIndex
                    Case 0
                        GetEnvironmentArgs()
                    Case 1
                        EnableFields()
                    Case Else
                        Return
                End Select
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Sub GetEnvironmentArgs()
            Try
                txtSMTP.Text = ConfigurationData.Environment.SMTP.EmailServer
                txtFrom.Text = ConfigurationData.GeneralSettings.Email.Admin.SIAMAdminMailId
                txtTo.Text = ConfigurationData.GeneralSettings.Email.Admin.SIAMAdminMailId
                txtNumberOfEmail.Text = "30"
                txtMailPerHour.Text = "30"
                lblTestdurationResult.Text = "1"
                DisableFields()
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Sub DisableFields()
            txtFrom.Enabled = False
            txtSMTP.Enabled = False
            txtTo.Enabled = False
            txtNumberOfEmail.Enabled = False
            txtMailPerHour.Enabled = False
        End Sub

        Private Sub EnableFields()
            txtFrom.Enabled = True
            txtSMTP.Enabled = True
            txtTo.Enabled = True
            txtNumberOfEmail.Enabled = True
            txtMailPerHour.Enabled = True
            txtFrom.ReadOnly = False
            txtSMTP.ReadOnly = False
            txtTo.ReadOnly = False
            txtNumberOfEmail.ReadOnly = False
            txtMailPerHour.ReadOnly = False
        End Sub
    End Class

    Public Class TestMailer
        Dim objTimer As Timer = New Timer()
        Dim intNumberofMail As Int16 = 0
        Dim intNumberofMailPerHr As Int16 = 0
        Dim intCounter As Int16 = 0

        Dim logger As New MailErrorLogger
        Dim log As MailErrorProcess
        Dim mailObj As ServicesPLUSMailObject = New ServicesPLUSMailObject()

        Public Sub StartMailling(ByVal objMailParm As Object)
            Dim objParam() As Object = objMailParm
            mailObj = objParam(2)
            intNumberofMail = objParam(0)
            intNumberofMailPerHr = objParam(1)
            Dim dblInterval As Double = (60 / intNumberofMailPerHr) * 60 * 1000
            AddHandler objTimer.Elapsed, AddressOf Me._timer_Elapsed

            log = logger.CreateLog("ServicesPLUS Admin e-Mail Test Tool")
            log.TotalRecordCount = intNumberofMail
            objTimer.AutoReset = True
            objTimer.Enabled = True
            objTimer.Interval = dblInterval
            objTimer.Start()
        End Sub
        Public Sub _timer_Elapsed(ByVal sender As Object, ByVal e As ElapsedEventArgs)
            If intCounter >= intNumberofMail Then
                objTimer.Stop()
                objTimer.Dispose()
                logger.FinalizeLog(log)
                Threading.Thread.CurrentThread.Abort()
            Else
                intCounter = intCounter + 1
            End If
            Try
                'Send Mail
                Sony.US.SIAMUtilities.ServicesPLUSMail.SendNetSMTPMail(mailObj)
                log.SuccessRecordCount = log.SuccessRecordCount + 1

            Catch ex As Exception
                Try
                    log.ErrorRecordCount = log.ErrorRecordCount + 1
                    logger.AddError(log, "TextMail" + intCounter.ToString(), ex.Message)
                Catch exInner As Exception
                    'Do nothing
                End Try
            End Try
        End Sub

    End Class

    Public Class MailErrorLogger

        Private m_data_manager As DataManager

        Public Sub New()
            m_data_manager = New DataManager
        End Sub

        Public Function CreateLog(ByVal process_name As String) As MailErrorProcess
            Dim log As New MailErrorProcess(process_name, Me)

            Dim cmd = New OracleCommand
            cmd.CommandText = "ADDMAILTESTLOG"

            cmd.Parameters.Add("xProcessName", OracleDbType.Varchar2, 50)
            cmd.Parameters("xProcessName").Value = process_name
            cmd.Parameters.Add("xStartDate", OracleDbType.Date)
            cmd.Parameters("xStartDate").Value = log.ProcessStartTime

            Dim outparam As New OracleParameter("retVal", OracleDbType.Double)
            outparam.Direction = ParameterDirection.Output
            cmd.Parameters.Add(outparam)

            Dim txn_context As TransactionContext
            Try
                txn_context = m_data_manager.BeginTransaction()
            Catch ex As Exception
                Throw ex
            End Try

            Try
                m_data_manager.ExecuteCMD(cmd, txn_context)

                If Not CType(cmd.Parameters("retVal")?.Value, INullable).IsNull Then
                    log.BatchLogID = cmd.Parameters("retVal").Value
                End If

                m_data_manager.CommitTransaction(txn_context)
            Catch ex As Exception

                m_data_manager.RollbackTransaction(txn_context)
            End Try

            Return log

        End Function

        Public Sub AddError(ByVal log As MailErrorProcess, ByVal record_identifier As String, ByVal error_message As String)
            Console.WriteLine("AddError " + record_identifier + ":" + error_message)
            Dim txn_context As TransactionContext = Nothing
            Dim cmd = New OracleCommand("ADDMAILLOGERROR")

            cmd.Parameters.Add("xSISBatchProcessID", OracleDbType.Int32)
            cmd.Parameters("xSISBatchProcessID").Value = log.BatchLogID
            cmd.Parameters.Add("xRecordID", OracleDbType.Varchar2, 20)
            cmd.Parameters("xRecordID").Value = record_identifier
            cmd.Parameters.Add("xErrorMessage", OracleDbType.Varchar2, 4000)
            cmd.Parameters("xErrorMessage").Value = error_message

            Try
                txn_context = m_data_manager.BeginTransaction()
                m_data_manager.ExecuteCMD(cmd, txn_context)
                m_data_manager.CommitTransaction(txn_context)
            Catch ex As Exception
                Console.WriteLine("Error AddError " + ex.Message)
                m_data_manager.RollbackTransaction(txn_context)
            End Try
        End Sub

        Public Sub FinalizeLog(ByRef batch_process As MailErrorProcess)
            Dim txn_context As TransactionContext = Nothing
            Dim cmd As New OracleCommand("UPDATEMAILERRORLOG")

            cmd.Parameters.Add("xBatchProcessID", OracleDbType.Int32)
            cmd.Parameters("xBatchProcessID").Value = batch_process.BatchLogID
            cmd.Parameters.Add("xEndDate", OracleDbType.Date)
            cmd.Parameters("xEndDate").Value = DateTime.Now
            cmd.Parameters.Add("xTotalRecordCount", OracleDbType.Int32)
            cmd.Parameters("xTotalRecordCount").Value = batch_process.TotalRecordCount
            cmd.Parameters.Add("xSuccessRecordCount", OracleDbType.Int32)
            cmd.Parameters("xSuccessRecordCount").Value = batch_process.SuccessRecordCount
            cmd.Parameters.Add("xSkippedRecordCount", OracleDbType.Int32)
            cmd.Parameters("xSkippedRecordCount").Value = batch_process.SkippedRecordCount
            cmd.Parameters.Add("xErrorRecordCount", OracleDbType.Int32)
            cmd.Parameters("xErrorRecordCount").Value = batch_process.ErrorRecordCount

            Try
                txn_context = m_data_manager.BeginTransaction()
                m_data_manager.ExecuteCMD(cmd, txn_context)
                m_data_manager.CommitTransaction(txn_context)
            Catch ex As Exception
                m_data_manager.RollbackTransaction(txn_context)
            End Try
        End Sub
    End Class

    Public Class MailErrorProcess
        Public Enum BatchStatus
            InProcess
            Complete
        End Enum


        Public Sub New(ByVal process_name As String, ByVal parent As MailErrorLogger)
            ProcessName = process_name
            ProcessStartTime = DateTime.Now
            m_status_code = BatchStatus.InProcess
            TotalRecordCount = 0
            SuccessRecordCount = 0
            SkippedRecordCount = 0
            ErrorRecordCount = 0
            Me.Parent = parent
        End Sub

        Private m_status_code As BatchStatus
        Public ReadOnly Property Parent() As MailErrorLogger
        Public Property BatchLogID() As String
        Public Property ProcessName() As String
        Public Property ProcessStartTime() As DateTime
        Public Property ProcessEndTime() As DateTime
        Public Property TotalRecordCount() As Integer
        Public Property SuccessRecordCount() As Integer
        Public Property SkippedRecordCount() As Integer
        Public Property ErrorRecordCount() As Integer
    End Class

End Namespace
