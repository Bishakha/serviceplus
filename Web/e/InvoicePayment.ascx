<%@ Control Language="vb" AutoEventWireup="false" Inherits="SIAMAdmin.InvoicePayment" CodeFile="InvoicePayment.ascx.vb" %>
<LINK href="includes/style.css" type="text/css" rel="stylesheet">
<TABLE id="Table1" style="WIDTH: 100%; HEIGHT: 230px" cellPadding="0"
	width="320" border="0">
	<tr>
	    <td>
	        <asp:Label ID="ErrorLabel" runat="server" Text="" CssClass="redAsterick" EnableViewState="false" />
	    </td>
	</tr>
	<TR>
		<TD style="HEIGHT: 16px" colSpan="2"><asp:label id="Label19" runat="server" Font-Size="Smaller" CssClass="Body" Font-Bold="True"> Invoice Payment Information:</asp:label></TD>
	</TR>
	<TR>
		<TD align="right" colSpan="1" rowSpan="1" width="40%"><asp:label id="Label1" runat="server" CssClass="Body">Name as it appears on credit card:</asp:label></TD>
		<TD style="HEIGHT: 23px"><SPS:SPSTextBox id="txtAtn" runat="server" CssClass="Body" Width="136px"></SPS:SPSTextBox>
			<asp:label id="Label2" CssClass="redAsterick" runat="server">
				<span class="redAsterick">*</span></asp:label></TD>
	</TR>
	<TR>
		<TD align="right" colSpan="1" rowSpan="1" width="40%"><asp:label id="LabelName" runat="server" CssClass="Body">Company Name:</asp:label></TD>
		<TD style="HEIGHT: 23px"><SPS:SPSTextBox id="txtCompanyName" runat="server" CssClass="Body" Width="136px"></SPS:SPSTextBox>
			<asp:label id="LabelNameStar" CssClass="redAsterick" runat="server">
				<span class="redAsterick">*</span></asp:label></TD>
	</TR>
	<TR>
		<TD align="right" width="40%"><asp:label id="LabelLine1" runat="server" CssClass="Body">Street Address:</asp:label></TD>
		<TD style="HEIGHT: 23px"><SPS:SPSTextBox id="txtStreet1" runat="server" CssClass="Body" Width="136px"></SPS:SPSTextBox>
			<asp:label id="LabelLine1Star" CssClass="redAsterick" runat="server">
				<span class="redAsterick">*</span>
			</asp:label></TD>
	</TR>
	<TR>
		<TD align="right" width="40%"><asp:label id="LabelLine2" runat="server" CssClass="Body">Address 2nd Line:</asp:label></TD>
		<TD style="HEIGHT: 24px"><SPS:SPSTextBox id="txtStreet2" runat="server" CssClass="Body" Width="136px"></SPS:SPSTextBox>
		<asp:label id="LabelLine2Star" CssClass="redAsterick" runat="server">
				
			</asp:label>
		</TD>
	</TR>
	<TR>
		<TD align="right" width="40%"><asp:label id="LabelLine3" runat="server" CssClass="Body">Address 3rd Line:</asp:label></TD>
		<TD style="HEIGHT: 24px"><SPS:SPSTextBox id="txtStreet3" runat="server" CssClass="Body" Width="136px"></SPS:SPSTextBox>
		<asp:label id="LabelLine3Star" CssClass="redAsterick" runat="server">
				
			</asp:label>
		</TD>
	</TR>
	
	<TR>
		<TD align="right" width="40%">
			<asp:label id="LabelCity" CssClass="Body" runat="server">Bill to City:</asp:label></TD>
		<TD style="HEIGHT: 24px">
			<SPS:SPSTextBox id="TextBoxCity" CssClass="Body" runat="server" Width="160px"></SPS:SPSTextBox>
			<asp:label id="LabelCityStar" CssClass="redAsterick" runat="server">
				<span class="redAsterick">*</span>
			</asp:label></TD>
	</TR>
	<TR>
		<TD align="right" width="40%"><asp:label id="LabelState" runat="server" CssClass="Body">Bill to State:</asp:label></TD>
		<TD style="HEIGHT: 24px">
			<asp:dropdownlist id="DDLState" CssClass="Body" runat="server"></asp:dropdownlist>
			<asp:label id="LabelStateStar" CssClass="redAsterick" runat="server">
				<span class="redAsterick">*</span>
			</asp:label></TD>
	</TR>
	<TR>
		<TD align="right" width="40%"><asp:label id="LabelZip" runat="server" CssClass="Body">Bill to Postal Code:</asp:label></TD>
		<TD style="HEIGHT: 25px"><SPS:SPSTextBox id="txtZip" runat="server" CssClass="Body" Width="96px"></SPS:SPSTextBox>
			<asp:label id="LabelZipStar" CssClass="redAsterick" runat="server">
				<span class="redAsterick">*</span>
			</asp:label></TD>
	</TR>
	<TR>
		<TD style="HEIGHT: 71px" colSpan="2" vAlign="top"><asp:label id="labelInvoice" runat="server" CssClass="Body" Width="297px" Height="64px"></asp:label></TD>
	</TR>
</TABLE>
