<%@ Reference Page="~/e/Utility.aspx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SIAMAdmin.FindOrders" CodeFile="FindOrders.aspx.vb" CodeFileBaseClass="SIAMAdmin.UtilityClass" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>FindOrders</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="includes/style.css" type="text/css" rel="stylesheet">
		<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
		
		<link href="../includes/ServicesPLUS_style.css" type="text/css" rel="stylesheet"/>
		<script language="javascript" src="../includes/ServicesPLUS.js" type="text/javascript"></script>
        <link href="../themes/base/jquery.ui.all.css" rel="stylesheet" type="text/css" />
        <link href="../includes/jquery.cluetip.css" rel="stylesheet" type="text/css" />
        <script src="../includes/jquery-1.4.2.min.js" type="text/javascript"></script>
        <script src="../includes/jquery.ui.core.js" type="text/javascript"></script>
        <script src="../includes/jquery.ui.widget.js" type="text/javascript"></script>
        <script src="../includes/jquery.cluetip.js" type="text/javascript"></script>
        <script src="../includes/jquery.ui.mouse.js" type="text/javascript"></script>
        <script src="../includes/jquery.ui.draggable.js" type="text/javascript"></script>
        <script src="../includes/jquery.ui.position.js" type="text/javascript"></script>
        <script src="../includes/jquery.ui.resizable.js" type="text/javascript"></script>
        <script src="../includes/jquery.ui.dialog.js" type="text/javascript"></script>
        <script src="../includes/jquery-ui-1.8.1.custom.min.js" type="text/javascript"></script>

        <script type="text/javascript">

            function OrderPopup() {
                $("#DataGrid1").hide();
                $("#ErrorLabel").hide();
                var Neworderno = $("#hdnOrderNumber").val();
                var txtorderno = $("#OrderNumber").val();
                $("#divMessage").text('');
                var buttons = $('.ui-dialog-buttonpane').children('button');
                buttons.remove();
                $("#divMessage").append(' <br/>');
                $("divMessage").dialog("destroy");
                $("#divMessage").dialog({ // dialog box
                    title: '<span class="modalpopup-title">Order Renamed</span>',
                    height: 220,
                    width: 560,
                    modal: true,
                    position: 'top',
                    close: function() {
                        redirectTo();
                    }
                });
                var content = "<span class='modalpopup-content'>Your Search criteria for Order matches order which have been renamed.<br/>";
                content += "Your search results will include any records which match a new order number listed below.</span>";
                content += "<br/>" + "<table class='modalpopup-content' style='margin-left:100px;'>";
                content += '<tr><td width="150px"><u>Old Order Number</u></td><td width="200px"><u>New Order Number</u></td></tr>';
                content += '<tr><td>' + txtorderno + '</td><td>' + Neworderno + '</td></tr>';
                content += "</table>";
                
                $("#divMessage").append(content); //message to display in divmessage div
                $("#divMessage").append('<br/> <br/><a ><img src ="images/sp_int_closeWindow_btn.gif" alt ="Close" onclick="javascript:return ClosePopUp();" id="messagego" /></a>');
                return false;
            }

            function ClosePopUp() {
                $("#divMessage").dialog("close");
            }

            function redirectTo() {
                $("#DataGrid1").show();
                $("#ErrorLabel").show();
                return true;
            }
            
        </script>
        
	</HEAD>
	<body>
	 <div id="divMessage" style ="font:11px;font-family:Arial ;overflow: auto;"></div>
		<form id="Form1" method="post" runat="server">
        <asp:HiddenField ID="hdnOrderNumber"  runat="server" />
			<table width="520" align="left" border="0">
				<tr>
					<td align="center" height="10"><asp:label id="ErrorLabel" runat="server" EnableViewState="False" ForeColor="Red" CssClass="Body"></asp:label></td>
				</tr>
				<tr>
					<td colSpan="5" height="5"></td>
				</tr>
				<tr>
					<td align="center"><span class="BodyHead">ServicesPLUS Find Order</span></td>
				</tr>
				<tr>
					<td colSpan="5" height="5"></td>
				</tr>
				<tr>
					<td class="Body" align="center" colSpan="5">&nbsp;Please enter a order number and 
						click the "Find Order" button.</td>
				</tr>
				<tr>
					<td colSpan="5" height="2"></td>
				</tr>
				<tr>
					<td align="center">
						<table border="0">
						<tr>
								<td style="HEIGHT: 17px"><span class="Body">Region&nbsp;</span></td>
								<td style="HEIGHT: 17px"><SPS:SPSDropDownList id="sps_dropdown" runat="server" CssClass="Body">
								<asp:ListItem Text="USA" Value="US" Selected="True"></asp:ListItem>
								<asp:ListItem Text="Canada" Value="CA"></asp:ListItem>
								</SPS:SPSDropDownList></td>
							</tr>
							<tr>
								<td style="HEIGHT: 17px"><span class="Body">Order Number&nbsp;</span></td>
								<td style="HEIGHT: 17px"><SPS:SPSTextBox id="OrderNumber" runat="server" CssClass="Body"></SPS:SPSTextBox></td>
							</tr>
							
							<tr>
								<td colSpan="2" height="3">
                                    
                                </td>
							</tr>
							<tr>
								<td align="right" colSpan="2"><asp:button id="FindOrder" runat="server" CssClass="Body" Text="Find Order"></asp:button></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td height="10">&nbsp;</td>
				</tr>
				<tr>
					<td height="5"><asp:datagrid id="DataGrid1" runat="server" 
                            OnPageIndexChanged="DataGrid1_Paging" AutoGenerateColumns="False" Width=100%>
							<Columns>
								<asp:TemplateColumn HeaderText="OrderNumber">
									<ItemTemplate>
										<asp:HyperLink runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.OrderNumber") %>' NavigateUrl='<%#"OrderDetails.aspx?on="+DataBinder.Eval(Container, "DataItem.OrderNumber").ToString()%>' ID="Hyperlink1">
										</asp:HyperLink>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Transaction Date">
									<ItemTemplate>
										<asp:HyperLink runat="server" Text='<%# Format(DataBinder.Eval(Container, "DataItem.OrderDate"), "MM/dd/yyyy") %>' NavigateUrl='<%#"OrderDetails.aspx?on="+DataBinder.Eval(Container, "DataItem.OrderNumber").ToString()%>' ID="Hyperlink2">
										</asp:HyperLink>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="P.O. Number">
									<ItemTemplate>
										<asp:HyperLink runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.POReference") %>' NavigateUrl='<%#"OrderDetails.aspx?on="+DataBinder.Eval(Container, "DataItem.OrderNumber").ToString()%>' ID="Hyperlink3">
										</asp:HyperLink>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Payer Number" Visible="False">
									<ItemTemplate>
										<asp:HyperLink runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.SAPBillToAccountNumber") %>' NavigateUrl='<%#"OrderDetails.aspx?on="+DataBinder.Eval(Container, "DataItem.OrderNumber").ToString()%>' ID="Hyperlink4">
										</asp:HyperLink>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Billing Address" Visible="False">
									<ItemTemplate>
										<asp:HyperLink runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.BillTo.Line1") %>' NavigateUrl='<%#"OrderDetails.aspx?on="+DataBinder.Eval(Container, "DataItem.OrderNumber").ToString()%>' ID="Hyperlink5">
										</asp:HyperLink>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Order Total">
									<ItemTemplate>
										<asp:HyperLink runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.AllocatedGrandTotal") %>' NavigateUrl='<%#"OrderDetails.aspx?on="+DataBinder.Eval(Container, "DataItem.OrderNumber").ToString()%>' ID="Hyperlink6">
										</asp:HyperLink>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Invoices" Visible="False">
									<ItemTemplate>
										<asp:HyperLink runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Invoices.Length") %>' NavigateUrl='<%#"OrderDetails.aspx?on="+DataBinder.Eval(Container, "DataItem.OrderNumber").ToString()%>' ID="Hyperlink7">
										</asp:HyperLink>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="First Name" Visible="False">
									<ItemTemplate>
										<asp:HyperLink id="firstNameLink" runat="server" ForeColor="Black" CssClass="Body" Text='<%# DataBinder.Eval(Container, "DataItem.FirstName") %>' NavigateUrl='<%#"CustomerInfo.aspx?LdapId="+DataBinder.Eval(Container, "DataItem.Customer.LdapID").ToString()%>'>
										</asp:HyperLink><%--7086--%>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Last Name" Visible="False">
									<ItemTemplate>
										<asp:HyperLink id="lastNameLink" runat="server" ForeColor="Black" CssClass="Body" Text='<%# DataBinder.Eval(Container, "DataItem.LastName") %>' NavigateUrl='<%#"CustomerInfo.aspx?LdapId="+DataBinder.Eval(Container, "DataItem.Customer.LdapID").ToString()%>'>
										</asp:HyperLink>
									</ItemTemplate>
								</asp:TemplateColumn>
							</Columns>
						</asp:datagrid></td>
				</tr>
				<tr>
					<td></td>
				</tr>
				<tr>
					<td colSpan="5">&nbsp;</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
