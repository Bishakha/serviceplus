﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="SaleOrder.ascx.vb" Inherits="e_SaleOrder" %>
<table>
    <tr>
        <td colspan="2">
            <div>
                <fieldset style="text-align: right;">
                <table>
                <tr align="left">
                 <td>
                                                    <SPS:SPSLabel ID="SPSLabel1" CssClass="BodyHead" runat="server">Order Simulation(INPUT 3)?</SPS:SPSLabel>
                                                </td>
                                                <td>
                                                    <SPS:SPSTextBox ID="txtOrderSimulation" runat="server">X</SPS:SPSTextBox>
                                                </td>
                </tr>
                <tr align="left">
                 <td>
                                                    <SPS:SPSLabel ID="SPSLabel5" CssClass="BodyHead" runat="server">Payer Account number</SPS:SPSLabel>
                                                </td>
                                                <td>
                                                    <SPS:SPSTextBox ID="txtPayerAccount" runat="server">0001078406 </SPS:SPSTextBox>
                                                </td>
                </tr>
                </table>
                    <legend><b>OrderDetails</b></legend>
                    <table>
                        <tr>
                            <td colspan="2">
                                <div>
                                    <fieldset style="text-align: right;">
                                        <legend><b>ShipInformation</b></legend>
                                        <table>
                                            <tr>
                                                <td>
                                                    <SPS:SPSLabel ID="SPSshipinfoShipTo" CssClass="BodyHead" runat="server">ShipTo</SPS:SPSLabel>
                                                </td>
                                                <td>
                                                    <SPS:SPSTextBox ID="txtshipinfoShipTo" runat="server">0001078406</SPS:SPSTextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <SPS:SPSLabel ID="SPSshipinfoShipToName1" CssClass="BodyHead" runat="server">ShipToName1</SPS:SPSLabel>
                                                </td>
                                                <td>
                                                    <SPS:SPSTextBox ID="txtshipinfoShipToName1" runat="server">Rick</SPS:SPSTextBox>
                                                </td>
                                            </tr>
                                          <%--  <tr>
                                                <td>
                                                    <SPS:SPSLabel ID="SPSshipinfoShipToName2" CssClass="BodyHead" runat="server">ShipToName2</SPS:SPSLabel>
                                                </td>
                                                <td>
                                                    <SPS:SPSTextBox ID="txtshipinfoShipToName2" runat="server">ShipToName2</SPS:SPSTextBox>
                                                </td>
                                            </tr>--%>
                                            <tr>
                                                <td>
                                                    <SPS:SPSLabel ID="SPSshipinfoShipToName3" CssClass="BodyHead" runat="server">ShipToName3</SPS:SPSLabel>
                                                </td>
                                                <td>
                                                    <SPS:SPSTextBox ID="txtshipinfoShipToName3" runat="server">Test First and Last Name</SPS:SPSTextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <SPS:SPSLabel ID="SPSshipinfoStreet" CssClass="BodyHead" runat="server">Street</SPS:SPSLabel>
                                                </td>
                                                <td>
                                                    <SPS:SPSTextBox ID="txtshipinfoStreet" runat="server">1730 North First Street</SPS:SPSTextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <SPS:SPSLabel ID="SPSshipinfoStreet2" CssClass="BodyHead" runat="server">Street2</SPS:SPSLabel>
                                                </td>
                                                <td>
                                                    <SPS:SPSTextBox ID="txtshipinfoStreet2" runat="server">Test Address 2</SPS:SPSTextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <SPS:SPSLabel ID="SPSshipinofDistrict" CssClass="BodyHead" runat="server">District</SPS:SPSLabel>
                                                </td>
                                                <td>
                                                    <SPS:SPSTextBox ID="txtshipinfoDistrict" runat="server">Test Address 3</SPS:SPSTextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <SPS:SPSLabel ID="SPSShipinofCity" CssClass="BodyHead" runat="server">City</SPS:SPSLabel>
                                                </td>
                                                <td>
                                                    <SPS:SPSTextBox ID="txtshipinfoCity" runat="server">San Jose</SPS:SPSTextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <SPS:SPSLabel ID="SPSshipinfoState" CssClass="BodyHead" runat="server">State</SPS:SPSLabel>
                                                </td>
                                                <td>
                                                    <SPS:SPSTextBox ID="txtshipinfoState" runat="server">CA</SPS:SPSTextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <SPS:SPSLabel ID="SPSshipinfoZipCode" CssClass="BodyHead" runat="server">ZipCode</SPS:SPSLabel>
                                                </td>
                                                <td>
                                                    <SPS:SPSTextBox ID="txtshipinfoZipCode" runat="server">95112</SPS:SPSTextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <SPS:SPSLabel ID="SPSShipinfoCountry" CssClass="BodyHead" runat="server">Country</SPS:SPSLabel>
                                                </td>
                                                <td>
                                                    <SPS:SPSTextBox ID="txtshipinfoCountry" runat="server">US</SPS:SPSTextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <SPS:SPSLabel ID="SPSshipinfoFirstTelephoneNumber" CssClass="BodyHead" runat="server">FirstTelephoneNumber</SPS:SPSLabel>
                                                </td>
                                                <td>
                                                    <SPS:SPSTextBox ID="txtshipinfoFirstTelephoneNumber" runat="server">408-352-4269</SPS:SPSTextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <SPS:SPSLabel ID="SPSBusExt" CssClass="BodyHead" runat="server">BusExt</SPS:SPSLabel>
                                                </td>
                                                <td>
                                                    <SPS:SPSTextBox ID="txtBusExt" runat="server">1111</SPS:SPSTextBox>
                                                </td>
                                            </tr>
                                           <%-- <tr>
                                                <td>
                                                    <SPS:SPSLabel ID="SPSshipinfoEmail" CssClass="BodyHead" runat="server">Email</SPS:SPSLabel>
                                                </td>
                                                <td>
                                                    <SPS:SPSTextBox ID="txtshipinofEmail" runat="server">Email</SPS:SPSTextBox>
                                                </td>
                                            </tr>--%>
                                            <tr>
                                                <td>
                                                    <SPS:SPSLabel ID="SPSshipinfoFax" CssClass="BodyHead" runat="server">Fax</SPS:SPSLabel>
                                                </td>
                                                <td>
                                                    <SPS:SPSTextBox ID="txtshipinfoFax" runat="server">408-352-4444</SPS:SPSTextBox>
                                                </td>
                                            </tr>
                                        </table>
                                    </fieldset>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <div>
                                    <fieldset style="text-align: right;">
                                        <legend><b>BillInformation</b></legend>
                                        <table>
                                            <tr>
                                                <td>
                                                    <SPS:SPSLabel ID="SPSSoldTo" CssClass="BodyHead" runat="server">SoldTo</SPS:SPSLabel>
                                                </td>
                                                <td>
                                                    <SPS:SPSTextBox ID="txtSoldTo" runat="server">0001078406</SPS:SPSTextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <SPS:SPSLabel ID="SPSBillToName1" CssClass="BodyHead" runat="server">BillToName1</SPS:SPSLabel>
                                                </td>
                                                <td>
                                                    <SPS:SPSTextBox ID="txtBillToName1" runat="server">Rick Upton</SPS:SPSTextBox>
                                                </td>
                                            </tr>
                                            <%--<tr>
                                                <td>
                                                    <SPS:SPSLabel ID="SPSBillToName2" CssClass="BodyHead" runat="server">BillToName2</SPS:SPSLabel>
                                                </td>
                                                <td>
                                                    <SPS:SPSTextBox ID="txtBillToName2" runat="server">BillToName2</SPS:SPSTextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <SPS:SPSLabel ID="SPSBillToName3" CssClass="BodyHead" runat="server">BillToName3</SPS:SPSLabel>
                                                </td>
                                                <td>
                                                    <SPS:SPSTextBox ID="txtBillToName3" runat="server">BillToName3</SPS:SPSTextBox>
                                                </td>
                                            </tr>--%>
                                            <tr>
                                                <td>
                                                    <SPS:SPSLabel ID="SPSStreet" CssClass="BodyHead" runat="server">Street</SPS:SPSLabel>
                                                </td>
                                                <td>
                                                    <SPS:SPSTextBox ID="txtStreet" runat="server">1730 North First Street</SPS:SPSTextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <SPS:SPSLabel ID="SPSStreet2" CssClass="BodyHead" runat="server">Street2</SPS:SPSLabel>
                                                </td>
                                                <td>
                                                    <SPS:SPSTextBox ID="txtStreet2" runat="server">Test Address 2</SPS:SPSTextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <SPS:SPSLabel ID="SPSDistrict" CssClass="BodyHead" runat="server">District</SPS:SPSLabel>
                                                </td>
                                                <td>
                                                    <SPS:SPSTextBox ID="txtDistrict" runat="server">Test Address 3</SPS:SPSTextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <SPS:SPSLabel ID="SPSCity" CssClass="BodyHead" runat="server">City</SPS:SPSLabel>
                                                </td>
                                                <td>
                                                    <SPS:SPSTextBox ID="txtCity" runat="server">San Jose</SPS:SPSTextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <SPS:SPSLabel ID="SPSState" CssClass="BodyHead" runat="server">State</SPS:SPSLabel>
                                                </td>
                                                <td>
                                                    <SPS:SPSTextBox ID="txtState" runat="server">CA</SPS:SPSTextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <SPS:SPSLabel ID="SPSZipCode" CssClass="BodyHead" runat="server">ZipCode</SPS:SPSLabel>
                                                </td>
                                                <td>
                                                    <SPS:SPSTextBox ID="txtZipCode" runat="server">95112</SPS:SPSTextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <SPS:SPSLabel ID="SPSCountry" CssClass="BodyHead" runat="server">Country</SPS:SPSLabel>
                                                </td>
                                                <td>
                                                    <SPS:SPSTextBox ID="txtCountry" runat="server">US</SPS:SPSTextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <SPS:SPSLabel ID="SPSEmail" CssClass="BodyHead" runat="server">Email</SPS:SPSLabel>
                                                </td>
                                                <td>
                                                    <SPS:SPSTextBox ID="txtEmail" runat="server">Rick.Upton@am.sony.com</SPS:SPSTextBox>
                                                </td>
                                            </tr>
                                        </table>
                                    </fieldset>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <div>
                                    <fieldset style="text-align: right;">
                                        <legend><b>Line Details</b></legend>
                                        <table>
                                            <%--<tr>
                                                <td>
                                                    <SPS:SPSLabel ID="SPSCarrier" CssClass="BodyHead" runat="server">Carrier</SPS:SPSLabel>
                                                </td>
                                                <td>
                                                    <SPS:SPSTextBox ID="txtCarrier" runat="server">Carrier</SPS:SPSTextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <SPS:SPSLabel ID="SPSVendorNumber" CssClass="BodyHead" runat="server">VendorNumber</SPS:SPSLabel>
                                                </td>
                                                <td>
                                                    <SPS:SPSTextBox ID="txtVendorNumber" runat="server">VendorNumber</SPS:SPSTextBox>
                                                </td>
                                            </tr>--%>
                                             <tr>
                                                <td>
                                                    <SPS:SPSLabel ID="SPSCarrierCode" CssClass="BodyHead" runat="server">Carrier Code</SPS:SPSLabel>
                                                </td>
                                                <td>
                                                    <SPS:SPSTextBox ID="txtCarrierCode1" runat="server">F1</SPS:SPSTextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <SPS:SPSLabel ID="SPSPartNumber" CssClass="BodyHead" runat="server">PartNumber</SPS:SPSLabel>
                                                </td>
                                                <td>
                                                    <SPS:SPSTextBox ID="txtPartNumber1" runat="server">180249134</SPS:SPSTextBox>
                                                </td>
                                            </tr>
                                            
                                            <tr>
                                                <td>
                                                    <SPS:SPSLabel ID="SPSOrderQuantity" CssClass="BodyHead" runat="server">OrderQuantity</SPS:SPSLabel>
                                                </td>
                                                <td>
                                                    <SPS:SPSTextBox ID="txtOrderQuantity1" runat="server">2</SPS:SPSTextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <SPS:SPSLabel ID="SPSLabel2" CssClass="BodyHead" runat="server">Carrier Code</SPS:SPSLabel>
                                                </td>
                                                <td>
                                                    <SPS:SPSTextBox ID="txtCarrierCode2" runat="server">F1</SPS:SPSTextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <SPS:SPSLabel ID="SPSLabel3" CssClass="BodyHead" runat="server">PartNumber</SPS:SPSLabel>
                                                </td>
                                                <td>
                                                    <SPS:SPSTextBox ID="txtPartNumber2" runat="server">180249134</SPS:SPSTextBox>
                                                </td>
                                            </tr>
                                            
                                            <tr>
                                                <td>
                                                    <SPS:SPSLabel ID="SPSLabel4" CssClass="BodyHead" runat="server">OrderQuantity</SPS:SPSLabel>
                                                </td>
                                                <td>
                                                    <SPS:SPSTextBox ID="txtOrderQuantity2" runat="server">2</SPS:SPSTextBox>
                                                </td>
                                            </tr>
                                            <%--<tr>
                                                <td>
                                                    <SPS:SPSLabel ID="SPSModel" CssClass="BodyHead" runat="server">Model</SPS:SPSLabel>
                                                </td>
                                                <td>
                                                    <SPS:SPSTextBox ID="txtModel" runat="server">Model</SPS:SPSTextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <SPS:SPSLabel ID="SPSSerial" CssClass="BodyHead" runat="server">Serial</SPS:SPSLabel>
                                                </td>
                                                <td>
                                                    <SPS:SPSTextBox ID="txtSerial" runat="server">Serial</SPS:SPSTextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <SPS:SPSLabel ID="SPSProductCode" CssClass="BodyHead" runat="server">ProductCode</SPS:SPSLabel>
                                                </td>
                                                <td>
                                                    <SPS:SPSTextBox ID="txtProductCode" runat="server">ProductCode</SPS:SPSTextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <SPS:SPSLabel ID="SPSServiceTag" CssClass="BodyHead" runat="server">ServiceTag</SPS:SPSLabel>
                                                </td>
                                                <td>
                                                    <SPS:SPSTextBox ID="txtServiceTag" runat="server">ServiceTag</SPS:SPSTextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <SPS:SPSLabel ID="SPSItemText" CssClass="BodyHead" runat="server">ItemText</SPS:SPSLabel>
                                                </td>
                                                <td>
                                                    <SPS:SPSTextBox ID="txtItemText" runat="server">ItemText</SPS:SPSTextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <SPS:SPSLabel ID="SPSItemComments" CssClass="BodyHead" runat="server">ItemComments</SPS:SPSLabel>
                                                </td>
                                                <td>
                                                    <SPS:SPSTextBox ID="txtItemComments" runat="server">ItemComments</SPS:SPSTextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <SPS:SPSLabel ID="SPSBillingOnly" CssClass="BodyHead" runat="server">BillingOnly</SPS:SPSLabel>
                                                </td>
                                                <td>
                                                    <SPS:SPSTextBox ID="txtBillingOnly" runat="server">BillingOnly</SPS:SPSTextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <SPS:SPSLabel ID="SPSLineDetail1" CssClass="BodyHead" runat="server">LineDetail1</SPS:SPSLabel>
                                                </td>
                                                <td>
                                                    <SPS:SPSTextBox ID="txtLineDetail1" runat="server">LineDetail1</SPS:SPSTextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <SPS:SPSLabel ID="SPSLineDetail2" CssClass="BodyHead" runat="server">LineDetail1</SPS:SPSLabel>
                                                </td>
                                                <td>
                                                    <SPS:SPSTextBox ID="txtLineDetail2" runat="server">txtLineDetail2</SPS:SPSTextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <SPS:SPSLabel ID="SPSLineDetail3" CssClass="BodyHead" runat="server">LineDetail3</SPS:SPSLabel>
                                                </td>
                                                <td>
                                                    <SPS:SPSTextBox ID="txtLineDetail3" runat="server">txtLineDetail3</SPS:SPSTextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <SPS:SPSLabel ID="SPSLineDetail4" CssClass="BodyHead" runat="server">LineDetail4</SPS:SPSLabel>
                                                </td>
                                                <td>
                                                    <SPS:SPSTextBox ID="txtLineDetail4" runat="server">txtLineDetail4</SPS:SPSTextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <SPS:SPSLabel ID="SPSLineDetail5" CssClass="BodyHead" runat="server">LineDetail5</SPS:SPSLabel>
                                                </td>
                                                <td>
                                                    <SPS:SPSTextBox ID="txtLineDetail5" runat="server">txtLineDetail5</SPS:SPSTextBox>
                                                </td>
                                            </tr>--%>
                                        </table>
                                    </fieldset>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <SPS:SPSLabel ID="SPSCustomerPONumber" CssClass="BodyHead" runat="server">CustomerPONumber</SPS:SPSLabel>
                            </td>
                            <td>
                                <SPS:SPSTextBox ID="txtCustomerPONumber" runat="server"></SPS:SPSTextBox>
                            </td>
                        </tr>
                        <%--<tr>
                            <td>
                                <SPS:SPSLabel ID="SPSCustomerPODate" CssClass="BodyHead" runat="server">CustomerPODate</SPS:SPSLabel>
                            </td>
                            <td>
                                <SPS:SPSTextBox ID="txtCustomerPODate" runat="server">CustomerPODate</SPS:SPSTextBox>
                            </td>
                        </tr>--%>
                        <tr>
                            <td>
                                <SPS:SPSLabel ID="SPSCustomerPOType" CssClass="BodyHead" runat="server">CustomerPOType</SPS:SPSLabel>
                            </td>
                            <td>
                                <SPS:SPSTextBox ID="txtCustomerPOType" runat="server">ZSVC</SPS:SPSTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <SPS:SPSLabel ID="SPSYourReference" CssClass="BodyHead" runat="server">YourReference</SPS:SPSLabel>
                            </td>
                            <td>
                                <SPS:SPSTextBox ID="txtYourReference" runat="server"></SPS:SPSTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <SPS:SPSLabel ID="SPSOrderEntryPerson" CssClass="BodyHead" runat="server">OrderEntryPerson</SPS:SPSLabel>
                            </td>
                            <td>
                                <SPS:SPSTextBox ID="txtOrderEntryPerson" runat="server">Rick Upton</SPS:SPSTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <SPS:SPSLabel ID="SPSGroupCode" CssClass="BodyHead" runat="server">GroupCode</SPS:SPSLabel>
                            </td>
                            <td>
                                <SPS:SPSTextBox ID="txtGroupCode" runat="server">70</SPS:SPSTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <SPS:SPSLabel ID="SPSReasonCode" CssClass="BodyHead" runat="server">ReasonCode</SPS:SPSLabel>
                            </td>
                            <td>
                                <SPS:SPSTextBox ID="txtReasonCode" runat="server">Y</SPS:SPSTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <SPS:SPSLabel ID="SPSCardType" CssClass="BodyHead" runat="server">CardType</SPS:SPSLabel>
                            </td>
                            <td>
                                <SPS:SPSTextBox ID="txtCardType" runat="server"></SPS:SPSTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <SPS:SPSLabel ID="SPSCardName" CssClass="BodyHead" runat="server">CardName</SPS:SPSLabel>
                            </td>
                            <td>
                                <SPS:SPSTextBox ID="txtCardName" runat="server"></SPS:SPSTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <SPS:SPSLabel ID="SPSCardNumber" CssClass="BodyHead" runat="server">CardNumber</SPS:SPSLabel>
                            </td>
                            <td>
                                <SPS:SPSTextBox ID="txtCardNumber" runat="server"></SPS:SPSTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <SPS:SPSLabel ID="SPSCardVerificationValue" CssClass="BodyHead" runat="server">CardVerificationValue</SPS:SPSLabel>
                            </td>
                            <td>
                                <SPS:SPSTextBox ID="txtCardVerificationValue" runat="server"></SPS:SPSTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <SPS:SPSLabel ID="SPSValidTo" CssClass="BodyHead" runat="server">ValidTo</SPS:SPSLabel>
                            </td>
                            <td>
                                <SPS:SPSTextBox ID="txtValidTo" runat="server"></SPS:SPSTextBox>
                            </td>
                        </tr>
                        <%--<tr>
                            <td>
                                <SPS:SPSLabel ID="SPSCreatedByCS3" CssClass="BodyHead" runat="server">CreatedByCS3</SPS:SPSLabel>
                            </td>
                            <td>
                                <SPS:SPSTextBox ID="txtCreatedByCS3" runat="server">CreatedByCS3</SPS:SPSTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <SPS:SPSLabel ID="SPSHeaderComments" CssClass="BodyHead" runat="server">Header Comments</SPS:SPSLabel>
                            </td>
                            <td>
                                <SPS:SPSTextBox ID="txtHeaderComments" runat="server"></SPS:SPSTextBox>
                            </td>
                        </tr>--%>
                        <tr>
                            <td>
                                <SPS:SPSLabel ID="SPSTaxExempt" CssClass="BodyHead" runat="server">TaxExempt</SPS:SPSLabel>
                            </td>
                            <td>
                                <SPS:SPSTextBox ID="txtTaxExempt" runat="server">2</SPS:SPSTextBox>
                            </td>
                        </tr>
                       <%-- <tr>
                            <td>
                                <SPS:SPSLabel ID="SPSShippingCharge" CssClass="BodyHead" runat="server">ShippingCharge</SPS:SPSLabel>
                            </td>
                            <td>
                                <SPS:SPSTextBox ID="txtShippingCharge" runat="server">ShippingCharge</SPS:SPSTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <SPS:SPSLabel ID="SPSHandlingCharge" CssClass="BodyHead" runat="server">HandlingCharge</SPS:SPSLabel>
                            </td>
                            <td>
                                <SPS:SPSTextBox ID="txtHandlingCharge" runat="server">HandlingCharge</SPS:SPSTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <SPS:SPSLabel ID="SPSInput1" CssClass="BodyHead" runat="server">Input1</SPS:SPSLabel>
                            </td>
                            <td>
                                <SPS:SPSTextBox ID="txtInput1" runat="server">Input1</SPS:SPSTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <SPS:SPSLabel ID="SPSInput2" CssClass="BodyHead" runat="server">Input2</SPS:SPSLabel>
                            </td>
                            <td>
                                <SPS:SPSTextBox ID="txtInput2" runat="server">txt</SPS:SPSTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <SPS:SPSLabel ID="SPSInput3" CssClass="BodyHead" runat="server">Input3</SPS:SPSLabel>
                            </td>
                            <td>
                                <SPS:SPSTextBox ID="txtInput3" runat="server">txtInput3</SPS:SPSTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <SPS:SPSLabel ID="SPSInput4" CssClass="BodyHead" runat="server">Input4</SPS:SPSLabel>
                            </td>
                            <td>
                                <SPS:SPSTextBox ID="txtInput4" runat="server">Input4</SPS:SPSTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <SPS:SPSLabel ID="SPSInput5" CssClass="BodyHead" runat="server">Input5</SPS:SPSLabel>
                            </td>
                            <td>
                                <SPS:SPSTextBox ID="txtInput5" runat="server">Input5</SPS:SPSTextBox>
                            </td>
                        </tr>--%>
                    </table>
                </fieldset>
            </div>
        </td>
    </tr>
    <tr>
        <td>
            <SPS:SPSLabel ID="lblDistributionChannel" CssClass="BodyHead" runat="server">Distribution Channel</SPS:SPSLabel>
        </td>
        <td>
            <SPS:SPSTextBox ID="txtDistributionChannel" runat="server"></SPS:SPSTextBox>
        </td>
    </tr>
    <tr>
        <td>
            <SPS:SPSLabel ID="SPSDivision" CssClass="BodyHead" runat="server">Division</SPS:SPSLabel>
        </td>
        <td>
            <SPS:SPSTextBox ID="txtDivision" runat="server"></SPS:SPSTextBox>
        </td>
    </tr>
  <%--  <tr>
        <td>
            <SPS:SPSLabel ID="SPSSalesDocumentType" CssClass="BodyHead" runat="server">SalesDocumentType</SPS:SPSLabel>
        </td>
        <td>
            <SPS:SPSTextBox ID="txtSalesDocumentType" runat="server">1073116</SPS:SPSTextBox>
        </td>
    </tr>--%>
    <tr>
        <td>
            <SPS:SPSLabel ID="SPSSalesOrganization" CssClass="BodyHead" runat="server">SalesOrganization</SPS:SPSLabel>
        </td>
        <td>
           <%-- <SPS:SPSTextBox ID="txtSalesOrganization" runat="server">1000</SPS:SPSTextBox>--%>
           <SPS:SPSTextBox ID="txtSalesOrganization" runat="server"></SPS:SPSTextBox>
        </td>
    </tr>
    <%-- Sasikumar WP SPLUS_WP014--%>
    <tr>
        <td>
            <SPS:SPSLabel ID="SPSLanguage" CssClass="BodyHead" runat="server">Language</SPS:SPSLabel>
        </td>
        <td>
            <SPS:SPSTextBox ID="txtLanguage" runat="server"></SPS:SPSTextBox>
        </td>
    </tr>
    <tr>
        <td>
            <SPS:SPSLabel ID="SPSSource" CssClass="BodyHead" runat="server">Source</SPS:SPSLabel>
        </td>
        <td>
            <SPS:SPSTextBox ID="txtSource" runat="server">SvcPlus</SPS:SPSTextBox>
        </td>
    </tr>
</table>
