<%@ Reference Page="~/e/Utility.aspx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SIAMAdmin.training_type_edit" CodeFile="training-type-edit.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
  <HEAD>
    <title>training_type_edit</title>
<meta content="Microsoft Visual Studio .NET 7.1" name=GENERATOR>
<meta content="Visual Basic .NET 7.1" name=CODE_LANGUAGE>
<meta content=JavaScript name=vs_defaultClientScript>
<meta content=http://schemas.microsoft.com/intellisense/ie5 name=vs_targetSchema><LINK href="includes/style.css" type=text/css rel=stylesheet >
  </HEAD>
<body>
<form id=Form1 method=post runat="server">
<TABLE id=Table3 
style="Z-INDEX: 101; LEFT: 8px; WIDTH: 554px; POSITION: absolute; TOP: 8px; HEIGHT: 193px" 
cellSpacing=0 cellPadding=0 align=center border=0>
  <TR>
    <TD class=PageHead style="HEIGHT: 16px" align=center colSpan=1 height=16 
    rowSpan=1>Training Course Type</TD></TR>
  <TR>
    <TD class=PageHead style="HEIGHT: 3px" align=center><asp:label id=ErrorLabel runat="server" Font-Size="Smaller" EnableViewState="False" ForeColor="Red" CssClass="Body"></asp:label></TD></TR>
  <TR>
    <TD style="HEIGHT: 12px"><asp:label id=Label23 runat="server" Font-Size="Smaller" CssClass="Body" Font-Bold="True"> Course Type Information:</asp:label></TD></TR>
  <TR>
    <TD style="HEIGHT: 107px" vAlign=top align=left>
      <TABLE id=Table2 style="WIDTH: 560px; HEIGHT: 69px" cellSpacing=0 
      cellPadding=0 width=560 border=0>
        <TR>
          <TD style="WIDTH: 84px; HEIGHT: 8px; align: " vAlign=top align=right 
          right?><asp:label id=labelCode runat="server" CssClass="Body"> Code:</asp:label></TD>
          <TD style="HEIGHT: 8px" vAlign=top><SPS:SPSTextBox id=txtCode runat="server" CssClass="Body"></SPS:SPSTextBox>
<asp:label id=LabelCodeStar runat="server" CssClass="redAsterick"><span class="redAsterick">*</span></asp:label></TD></TR>
        <TR>
          <TD style="WIDTH: 84px; HEIGHT: 5px" vAlign=top align=right 
          ><asp:label id=labelDescription runat="server" CssClass="Body">Description:</asp:label></TD>
          <TD style="HEIGHT: 5px" vAlign=top><SPS:SPSTextBox id=txtDescription runat="server" CssClass="Body" Height="26px" Width="464px"></SPS:SPSTextBox>
<asp:label id=LabelDescriptionStar runat="server" CssClass="redAsterick"><span class="redAsterick">*</span></asp:label></TD></TR></TABLE></TD></TR>
  <TR>
    <TD style="HEIGHT: 19px" align=center>
      <TABLE id=Table1 style="WIDTH: 504px; HEIGHT: 24px" cellSpacing=0 
      cellPadding=0 width=504 border=0>
        <TR>
          <TD align=center><asp:button id=btnUpdate runat="server" Text="Update"></asp:button></TD>
          <TD align=center><asp:button id=btnCancel runat="server" Text="Back"></asp:button></TD></TR></TABLE></TD></TR></TABLE></form>

  </body>
</HTML>
