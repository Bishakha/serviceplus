<%@ Reference Page="~/e/Utility.aspx" %>
<%@ Page Language="vb" SmartNavigation="true" AutoEventWireup="false" Inherits="SIAMAdmin.training_model_edit" CodeFile="training-model-edit.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
  <HEAD>
    <title>training_model_edit</title>
<meta content="Microsoft Visual Studio .NET 7.1" name=GENERATOR>
<meta content="Visual Basic .NET 7.1" name=CODE_LANGUAGE>
<meta content=JavaScript name=vs_defaultClientScript>
<meta content=http://schemas.microsoft.com/intellisense/ie5 name=vs_targetSchema><LINK href="includes/style.css" type=text/css rel=stylesheet >
  </HEAD>
<body>
<form id=Form1 method=post runat="server">
<TABLE id=Table3 
style="Z-INDEX: 101; LEFT: 8px; WIDTH: 551px; POSITION: absolute; TOP: 8px; HEIGHT: 193px" 
cellSpacing=0 cellPadding=0 align=center border=0>
  <TR>
    <TD class=PageHead style="HEIGHT: 19px" align=center colSpan=1 height=19 
    rowSpan=1>Training Course Model</TD></TR>
  <TR>
    <TD class=PageHead style="HEIGHT: 3px" align=left><asp:label id=ErrorLabel runat="server" EnableViewState="False" ForeColor="Red" CssClass="Body"></asp:label></TD></TR>
  <%--<TR>
    <TD style="HEIGHT: 12px"><asp:label id=Label23 runat="server" Font-Size="Smaller" CssClass="Body" Font-Bold="True"> Course Model:</asp:label></TD></TR>--%>
  <TR>
    <TD style="HEIGHT: 43px" vAlign=top align=left>
      <TABLE id=Table2 style="WIDTH: 352px; HEIGHT: 59px" cellSpacing=0 
      cellPadding=0 width=552 border=0 align=center>
        <TR>
          <TD vAlign=Middle align=right><asp:label id=labelModel runat="server" CssClass="Body"><b>Model:&nbsp;&nbsp;</b></asp:label></TD>
          <TD vAlign=Middle><SPS:SPSTextBox id=txtModel runat="server" CssClass="Body" Height="20px"></SPS:SPSTextBox><asp:label id=lblModelNumber runat="server" CssClass="Body" Visible=false></asp:label>
            <asp:label id=LabelCodeStar runat="server" CssClass="redAsterick"><span class="redAsterick">*</span></asp:label></TD>
         </TR>
          <TR>
              <TD  vAlign=Middle align=right>
                <asp:label id=labelCourseNumber runat="server" CssClass="Body"><b>Course Number:&nbsp;&nbsp;</b></asp:label>
              </TD>
              <TD  vAlign=Middle>
                <asp:DropDownList runat=server ID=drpCourseNumber CssClass="Body"  AutoPostBack=false ></asp:DropDownList>
                <asp:label id=lblSelectedCourse runat="server" CssClass="Body" Visible=false></asp:label>
                <asp:label id=LabelCourseNumberStar runat="server" CssClass="redAsterick"><span class="redAsterick">*</span></asp:label>
              </TD>
          </TR>
          
          </TABLE>
      </TD>
     </TR>
  <TR>
    <TD style="HEIGHT: 19px" align=center>
      <TABLE id=Table1 style="WIDTH: 200px; HEIGHT: 24px" cellSpacing=0 
      cellPadding=0  border=0 align=center>
        <TR>
          <TD align=center><asp:button id=btnUpdate runat="server" Text="Update"></asp:button>&nbsp;&nbsp;<asp:button id=btnCancel runat="server" Text="Back"></asp:button></TD>
          </TR></TABLE></TD></TR></TABLE></form>

  </body>
</HTML>
