Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Data
Imports Sony.US.ServicesPLUS.Process
Imports ServicesPlusException


Namespace SIAMAdmin


    Partial Class training_class_detail
        Inherits UtilityClass
        'Dim objUtilties As Utilities = New Utilities

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            MyBase.IsProtectedPage(True, True)
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            If Not IsPostBack Then
                Dim cm As New CourseManager
                Dim cdm As New CourseDataManager
                Dim courseSelected As CourseSchedule = Nothing
                Dim courseType As New CourseType
                Dim itemNumber As Integer = -1

                Try
                    If (Not Request.QueryString("LineNo") Is Nothing) And (Not Session.Item("searchedClass") Is Nothing) Then
                        itemNumber = Convert.ToInt16(Request.QueryString("LineNo"))
                        courseSelected = CType(Session.Item("searchedClass"), Array)(itemNumber)
                        'Viewstate.Add("cour"
                    ElseIf Not Request.QueryString("ClassNumber") Is Nothing Then
                        Try
                            cdm.GetCourseClassByNumber(Request.QueryString("ClassNumber"), courseSelected)
                        Catch ex As NotSupportedException 'don't know why GetCourseClassByNumber(Request.QueryString("ClassNumber"), courseSelected) throw this exception
                            'just ignore it
                        End Try
                        Dim coursesSelected(1) As CourseSchedule
                        coursesSelected(0) = courseSelected
                        Session.Item("courseschedule") = coursesSelected
                        itemNumber = 0
                    End If
                Catch ex As Exception
                    ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                End Try

                courseType.PartNumber = courseSelected.PartNumber()
                cm.GetCourseType(courseType)

                If courseSelected IsNot Nothing Then
                    Try
                        'LabelNumber.Text = courseSelected.Course.Number
                        LabelTitle.Text = courseSelected.Course.Title
                        labelDescription.Text = courseSelected.Course.Description
                        LabelPrerequisite.Text = courseSelected.Course.PreRequisite
                        LabelStartDate.Text = FormatDate(courseSelected.StartDate)
                        LabelEndDate.Text = FormatDate(courseSelected.EndDate)
                        lblTrainingTime.Text = courseSelected.Time
                        Dim tr As TableRow
                        Dim td As TableCell

                        If Not (courseType.Code <> 0) Then
                            tr = New TableRow
                            td = New TableCell
                            td.Height = New Unit("5px")
                            tr.Cells.Add(td)
                            Me.locationTable.Rows.Add(tr)

                            tr = New TableRow
                            td = New TableCell
                            td.VerticalAlign = VerticalAlign.Top
                            td.CssClass = "Body"
                            td.Height = New Unit("14px")
                            td.Controls.Add(New LiteralControl("&nbsp;" + courseSelected.Location.Name))
                            tr.Cells.Add(td)
                            Me.locationTable.Rows.Add(tr)

                            tr = New TableRow
                            td = New TableCell
                            td.VerticalAlign = VerticalAlign.Top
                            td.CssClass = "Body"
                            td.Height = New Unit("14px")
                            td.Controls.Add(New LiteralControl("&nbsp;" + courseSelected.Location.Address1))
                            tr.Cells.Add(td)
                            Me.locationTable.Rows.Add(tr)

                            If courseSelected.Location.Address2 <> "" Then
                                tr = New TableRow
                                td = New TableCell
                                td.VerticalAlign = VerticalAlign.Top
                                td.Height = New Unit("14px")
                                td.CssClass = "Body"
                                td.Controls.Add(New LiteralControl("&nbsp;" + courseSelected.Location.Address2))
                                tr.Cells.Add(td)
                                Me.locationTable.Rows.Add(tr)
                            End If

                            tr = New TableRow
                            td = New TableCell
                            td.VerticalAlign = VerticalAlign.Top
                            td.CssClass = "Body"
                            td.Height = New Unit("14px")
                            td.Controls.Add(New LiteralControl("&nbsp;" + courseSelected.Location.City + ", " + courseSelected.Location.State + " " + courseSelected.Location.Zip))
                            tr.Cells.Add(td)
                            Me.locationTable.Rows.Add(tr)

                            tr = New TableRow
                            td = New TableCell
                            td.VerticalAlign = VerticalAlign.Top
                            td.CssClass = "Body"
                            td.Height = New Unit("14px")
                            td.Controls.Add(New LiteralControl("&nbsp;" + courseSelected.Location.Country.TerritoryName))
                            tr.Cells.Add(td)
                            Me.locationTable.Rows.Add(tr)
                            Session.Add("IsNonClassRoom", 0)
                        Else
                            Session.Add("IsNonClassRoom", 1)
                        End If
                        LabelListPrice.Text = String.Format("{0:c}", courseSelected.Course.ListPrice).ToString()

                        If courseType.Code <> 0 Then
                            Label4.Visible = False
                            Label5.Visible = False
                            Label6.Visible = False
                            Label7.Text = "Price:"
                            LabelStartDate.Visible = False
                            LabelEndDate.Visible = False
                            lblTrainingTime.Visible = False
                        End If




                    Catch ex As Exception
                        ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                    End Try
                End If
            End If
        End Sub

        Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
            Response.Redirect("training-class-search.aspx")

            'If CType(Request.QueryString("IsSearch"), Boolean) = True Then
            '    Session.Remove("IsSearch")
            'Else
            '    Response.Redirect("training-class-search.aspx?Identity=" + Session.Item("currUserSiamID"))
            'End If
        End Sub
    End Class

End Namespace
