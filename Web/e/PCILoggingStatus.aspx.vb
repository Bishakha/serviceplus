Imports System.Web.UI
Imports Sony.US.ServicesPLUS.Process
Imports Sony.US.ServicesPLUS.Core
Imports System.Xml
Imports System.Data
Imports Sony.US.siam, Sony.US.ServicesPLUS.Controls
Imports System.Drawing
Imports Sony.US.ServicesPLUS.Data
Imports Sony.US.SIAMUtilities
Imports Microsoft.Win32
Imports WinLDAP
Imports Sony.US.AuditLog

Namespace SIAMAdmin

    Partial Class PCILoggingStatus
        Inherits UtilityClass

        Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
            MyBase.IsProtectedPage(True, True)
            If Request.UrlReferrer Is Nothing Then Response.Redirect("default.aspx")

        End Sub

        Protected Sub StatusChange(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnStatus.Click
            ''Praveen kumar N.B added on 14-12-2009
            'Event logging code block added for Role/Function
            Dim objPCILogger As New PCILogger()
            objPCILogger.EventOriginApplicationLocation = Me.Page.GetType().Name
            objPCILogger.EventOriginApplication = "ServicesPLUS Admin"
            objPCILogger.EventOriginMethod = "StatusChange"
            objPCILogger.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
            objPCILogger.OperationalUser = Environment.UserName
            objPCILogger.EventType = EventType.LoggerStatusChange
            If Not Session.Item("siamuser") Is Nothing Then
                'objPCILogger.UserName = DirectCast(DirectCast(Session.Item("siamuser"), System.Object), Sony.US.siam.User).UserId
                objPCILogger.EmailAddress = DirectCast(DirectCast(Session.Item("siamuser"), System.Object), Sony.US.siam.User).EmailAddress '6524
                objPCILogger.CustomerID = DirectCast(DirectCast(Session.Item("siamuser"), System.Object), Sony.US.siam.User).CustomerID
                objPCILogger.CustomerID_SequenceNumber = DirectCast(DirectCast(Session.Item("siamuser"), System.Object), Sony.US.siam.User).SequenceNumber
                objPCILogger.SIAM_ID = DirectCast(DirectCast(Session.Item("siamuser"), System.Object), Sony.US.siam.User).Identity
                'objPCILogger.B2B_ID = DirectCast(DirectCast(Session.Item("siamuser"), System.Object), Sony.US.siam.User).AlternateUserId'6524
                objPCILogger.LDAP_ID = DirectCast(DirectCast(Session.Item("siamuser"), System.Object), Sony.US.siam.User).AlternateUserId '6524
            End If


            objPCILogger.EventDateTime = DateTime.Now.ToLongTimeString()
            objPCILogger.HTTPRequestObjectValues = GetRequestContextValue()
            objPCILogger.IndicationSuccessFailure = "Success"

            If ConfigurationData.GeneralSettings.LoggingStatus = "ON" Then
                objPCILogger.Message = "Logger Status is set to OFF"
                objPCILogger.PushLogToMSMQ()
                ConfigurationData.GeneralSettings.LoggingStatus = "OFF"
                lblStatus.Text = "Logger Status is OFF"

                btnStatus.Text = "Set Logger ON"

            Else
               
                ConfigurationData.GeneralSettings.LoggingStatus = "ON"
                lblStatus.Text = "Logger Status is ON"
                btnStatus.Text = "Set Logger OFF"
                objPCILogger.Message = "Logger Status is set to ON"
                objPCILogger.PushLogToMSMQ()
            End If



        End Sub

        Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            If Not IsPostBack Then
                If ConfigurationData.GeneralSettings.LoggingStatus = "ON" Then
                    lblStatus.Text = "Logger Status is ON"
                    btnStatus.Text = "Set Logger OFF"
                Else
                    lblStatus.Text = "Logger Status is OFF"
                    btnStatus.Text = "Set Logger ON"
                End If
            End If
        End Sub
    End Class
End Namespace
