<%@ Reference Page="~/e/Utility.aspx" %>
<%@ Register TagPrefix="uc1" TagName="TrainingDataSubNavigation" Src="TrainingDataSubNavigation.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SIAMAdmin.training_type_list" CodeFile="training-type-list.aspx.vb" CodeFileBaseClass="SIAMAdmin.UtilityClass" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>training_type_list</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="includes/style.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server" >
			<TABLE id="Table1" style="Z-INDEX: 101; LEFT: 300px; POSITION: absolute; TOP: 8px; " cellSpacing="0"
				width="520" align="left" border="0" >
				<TR>
					<TD class="PageHead" align="center" height="30">
						Manage&nbsp;Training&nbsp;Type</TD>
				</TR>
				<TR>
					<TD align="center" height="10">
						<asp:label id="ErrorLabel" runat="server" EnableViewState="False" ForeColor="Red" CssClass="Body"></asp:label></TD>
				</TR>
<%--				<TR>
					<TD align="left" height="10">
						<uc1:TrainingDataSubNavigation id="TrainingDataSubNavigation" runat="server"></uc1:TrainingDataSubNavigation></TD>
				</TR>--%>
				<TR>
					<TD align="center" height="10"></TD>
				</TR>
				<TR>
					<TD align="center"><span class="BodyHead">
							<asp:label id="Label1" runat="server"> Current Type List</asp:label></span></TD>
				</TR>
				<TR>
					<TD height="10">&nbsp;</TD>
				</TR>
				<TR>
					<TD height="5">
						<asp:datagrid id="typeDataGrid" runat="server" OnPageIndexChanged="typeDataGrid_Paging">
							<Columns>
								<asp:TemplateColumn HeaderText="Code">
									<ItemTemplate>
										<asp:HyperLink runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Code") %>' NavigateUrl='' ID="lnkType">
										</asp:HyperLink>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Description">
									<ItemTemplate>
										<asp:Label runat="server" Text='<%# Format(DataBinder.Eval(Container, "DataItem.Description")) %>' ID="labelDescription">
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
							</Columns>
						</asp:datagrid></TD>
				</TR>
				<TR>
					<TD style="HEIGHT: 19px" align="center">
						<asp:button id="btnAdd" runat="server" Text="Add"></asp:button></TD>
				</TR>
				<TR>
					<TD colSpan="5">&nbsp;</TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
