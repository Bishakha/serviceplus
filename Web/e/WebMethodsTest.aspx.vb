Imports System.Net
Imports System.Net.Security
Imports System.Security.Cryptography.X509Certificates
Imports ServicesPlusException
Imports Sony.US.AuditLog
Imports Sony.US.ServicesPLUS.Controls
Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Process
Imports Sony.US.ServicesPLUS.Process.WebMethods
Imports Sony.US.SIAMUtilities

Namespace SIAMAdmin
    Partial Class WebMethodsTest
        Inherits UtilityClass


#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Protected WithEvents txtCourseNumber As SPSTextBox


        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            MyBase.IsProtectedPage(True, True)
            InitializeComponent()
        End Sub

#End Region

        Private Const pcAccountInquiry As String = "Account Inquiry (Customer Lookup)"
        Private Const pcCancelOrder As String = "Cancel Order"
        Private Const pcPartInquiry As String = "Part Inquiry"
        Private Const pcOrderInquiry As String = "Order Inquiry (PO Inquiry)"
        Private Const pcSalesOrder As String = "Order Simulation or Injection (SalesOrder)"

        Private Const pcSeparator As String = "-------------------------------------------------------"

        Private StartTime As Date
        Private EndTime As Date
        Private TimeSpan As TimeSpan
        Dim saphelper As New SAPHelper
        Dim strUname As String
        Dim strPwd As String
        'Dim objUtilties As Utilities = New Utilities

#Region "Web Service Call Events"

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try

                If Not IsPostBack Then
                    '9070 Start 
                    'ddlEnviroment.Items.Add("Development")
                    'ddlEnviroment.Items.Add("QA")
                    'ddlEnviroment.Items.Add("QA2008")
                    'ddlEnviroment.Items.Add("Staging")
                    'ddlEnviroment.Items.Add("Production")
                    '9070 end
                    cmbInterface.Items.Add("Select Interface")

                    Dim strMethods() As String = {pcAccountInquiry, pcCancelOrder, pcPartInquiry, pcOrderInquiry, pcSalesOrder}
                    For Each strMethod As String In strMethods
                        cmbInterface.Items.Add(strMethod)
                    Next

                    ' ddlEnviroment.Items.FindByValue(ConfigurationData.GeneralSettings.Environment).Selected = True '9070
                    If ConfigurationData.GeneralSettings.Environment.ToUpper() = "PRODUCTION" Then
                        ' ddlEnviroment.Enabled = False
                    End If
                Else
                    lblError.Text = ""
                End If

            Catch ex As Exception
                Utilities.WrapExceptionforUI(ex)
                lblError.Text = ex.Message
            End Try
            'Commeneted he below line for Bug 9070
            'lbl_env.Text = ConfigurationData.GeneralSettings.Environment.ToUpper()
            Try
                If cmbInterface.Text = pcPartInquiry Then
                    ConfigurationData.WebmethodTest.Load(ConfigurationData.GeneralSettings.Environment)
                    CType(PartInquiry1.FindControl("txtSAPAccountNumber"), TextBox).Text = ConfigurationData.WebmethodTest.DefaultAccount
                    ''Commeneted as per bug 6322
                    CType(PartInquiry1.FindControl("txtLocation"), TextBox).Text = ConfigurationData.WebmethodTest.PartLocation
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub
        Private Sub btnSendData_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSendData.Click
            Try
                If Me.cmbInterface.SelectedIndex = 0 Then
                    lblError.Text = "Please select a Method to test."
                    Me.cmbInterface.Focus()
                    Exit Sub
                End If

                Select Case Me.cmbInterface.Text
                    Case pcCancelOrder
                        CancelOrder()
                    Case pcAccountInquiry
                        CustomerLookUp()
                    Case pcPartInquiry
                        PartInquiry()
                    Case pcOrderInquiry
                        OrderInquiry()
                    Case pcSalesOrder
                        SalesOrder()
                End Select
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub


        Private Sub cmbInterface_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbInterface.SelectedIndexChanged
            Try
                SaleOrder1.Visible = (cmbInterface.Text = pcSalesOrder)
                CustomerLookUp1.Visible = (cmbInterface.Text = pcAccountInquiry)
                CancelOrder1.Visible = (cmbInterface.Text = pcCancelOrder)
                PartInquiry1.Visible = (cmbInterface.Text = pcPartInquiry)
                OrderInquiry1.Visible = (cmbInterface.Text = pcOrderInquiry)

                lblDataSent.Text = ""
                txtDataReturned.Text = ""
                lblDataReturned.Text = ""

                If cmbInterface.Text = pcPartInquiry Then
                    ' ASleight - Note: The following overwrites these textbox values. Should be fixed, because it makes it impossible
                    '            to do a Part Inquiry with any OTHER SAP account number.
                    ConfigurationData.WebmethodTest.Load(ConfigurationData.GeneralSettings.Environment)
                    CType(PartInquiry1.FindControl("txtSAPAccountNumber"), TextBox).Text = ConfigurationData.WebmethodTest.DefaultAccount
                    CType(PartInquiry1.FindControl("txtLocation"), TextBox).Text = ConfigurationData.WebmethodTest.PartLocation
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub


#End Region
#Region "Web Service Call Logic"
        Private Sub CustomerLookUp()
            Dim bld As New System.Text.StringBuilder()
            Dim objPCILogger As New PCILogger()
            Try
                objPCILogger.EventOriginApplication = "ServicesPLUS"
                objPCILogger.EventOriginApplicationLocation = Me.Page.GetType().Name
                objPCILogger.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
                objPCILogger.OperationalUser = Environment.UserName
                objPCILogger.EventDateTime = DateTime.Now.ToLongTimeString()
                objPCILogger.HTTPRequestObjectValues = objPCILogger.GetRequestContextValue()
                objPCILogger.IndicationSuccessFailure = "Success"
                objPCILogger.EventOriginMethod = "CustomerLookUp"
                objPCILogger.Message = "CustomerLookUp Success."
                objPCILogger.EventType = EventType.View

                ConfigurationData.WebmethodTest.Load(ConfigurationData.GeneralSettings.Environment)
                Dim objCustomerLookup As New CustomerLookup.CustomerLookupWSDL()
                Dim Cert As X509Certificate
                ConfigurationData.WebmethodTest.Load(ConfigurationData.GeneralSettings.Environment)


                Cert = X509Certificate.CreateFromCertFile(ConfigurationData.WebmethodTest.CertificatePath)
                objCustomerLookup.Url = ConfigurationData.Environment.SAPWebMethods.CustomerLookUp
                Cert = X509Certificate.CreateFromCertFile(ConfigurationData.Environment.SAPWebMethods.CertificatePath)
                'ServicePointManager.CertificatePolicy = New TrustAllCertificatePolicy()
                ServicePointManager.Expect100Continue = True
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12
                ServicePointManager.ServerCertificateValidationCallback = New RemoteCertificateValidationCallback(AddressOf ValidateRemoteCertificate)
                objCustomerLookup.ClientCertificates.Add(Cert)
                objCustomerLookup.Credentials = getCredential()

                Dim objGlobalData As New GlobalData
                objGlobalData.SalesOrganization = CType(CustomerLookUp1.FindControl("txtSalesOrganization"), TextBox).Text
                objGlobalData.DistributionChannel = CType(CustomerLookUp1.FindControl("txtDistributionChannel"), TextBox).Text
                objGlobalData.Division = CType(CustomerLookUp1.FindControl("txtDivision"), TextBox).Text

                ''Method to calll
                Dim objddlMethod = CType(CustomerLookUp1.FindControl("cmMethod"), DropDownList).Text
                DisplayDataSent(bld)
                If objddlMethod = "getPayerDetails" Then
                    Try
                        Dim objInput As New CustomerLookup.cusPayerAccountInput
                        Dim objOutput As CustomerLookup.cusPayerAccountOutput()
                        Dim AccountNumbers() As String = CType(CustomerLookUp1.FindControl("txtSAPPayerAccountNumbers"), TextBox).Text.Split(vbCrLf)

                        objInput.CompanyName = CType(CustomerLookUp1.FindControl("txtCompanyName"), TextBox).Text
                        objInput.SAPPayerAccountNumber = AccountNumbers
                        objInput.SalesOrganization = objGlobalData.SalesOrganization
                        objInput.DistributionChannel = objGlobalData.DistributionChannel
                        objInput.Division = objGlobalData.Division

                        objOutput = objCustomerLookup.getPayerDetails(objInput)

                        For Each objAcc As CustomerLookup.cusPayerAccountOutput In objOutput
                            bld.Append("BillTo" & vbNewLine)
                            bld.Append(vbNewLine)
                            bld.Append("SAPPayerAccountNumber= " & objAcc.SAPPayerAccountNumber & vbNewLine)
                            bld.Append("SapSoldToAccountNum= " & objAcc.SapSoldToAccountNum & vbNewLine)
                            bld.Append("CompanyName= " & objAcc.CompanyName & vbNewLine)
                            bld.Append("Detail1= " & objAcc.Detail1 & vbNewLine)
                            bld.Append("SapSoldToAccountNum= " & objAcc.Detail1 & vbNewLine)
                            bld.Append("Detail1= " & objAcc.Detail1 & vbNewLine)
                            bld.Append("Detail2= " & objAcc.Detail2 & vbNewLine)
                            bld.Append("Detail3= " & objAcc.Detail3 & vbNewLine)
                            bld.Append("Detail4= " & objAcc.Detail4 & vbNewLine)
                            bld.Append("Detail5= " & objAcc.Detail5 & vbNewLine)
                            bld.Append("ErrorMessage= " & objAcc.ErrorMessage & vbNewLine)
                            If (objAcc.Payer IsNot Nothing) Then
                                bld.Append("Address1= " & objAcc.Payer.Address1 & vbNewLine)
                                bld.Append("Address2= " & objAcc.Payer.Address2 & vbNewLine)
                                bld.Append("Address3= " & objAcc.Payer.Address3 & vbNewLine)
                                bld.Append("City= " & objAcc.Payer.City & vbNewLine)
                                bld.Append("Attention= " & objAcc.Payer.Attention & vbNewLine)
                                bld.Append("Name= " & objAcc.Payer.Name & vbNewLine)
                                bld.Append("Phone= " & objAcc.Payer.Phone & vbNewLine)
                                bld.Append("Phone Extension= " & objAcc.Payer.PhoneExtn & vbNewLine)
                                bld.Append("State= " & objAcc.Payer.State & vbNewLine)
                                bld.Append("Zip= " & objAcc.Payer.Zip & vbNewLine)
                                bld.Append("Country= " & objAcc.Payer.Country & vbNewLine)
                                bld.Append("PayerDetail1= " & objAcc.Payer.PayerDetail1 & vbNewLine)
                                bld.Append("PayerDetail2= " & objAcc.Payer.PayerDetail2 & vbNewLine)
                                bld.Append("PayerDetail3= " & objAcc.Payer.PayerDetail3 & vbNewLine)
                                bld.Append("PayerDetail4= " & objAcc.Payer.PayerDetail4 & vbNewLine)
                                bld.Append("PayerDetail5= " & objAcc.Payer.PayerDetail5 & vbNewLine)
                            End If
                            bld.Append(pcSeparator & vbNewLine)
                        Next
                    Catch ex As Exception

                    End Try
                ElseIf objddlMethod = "getShippingInformation" Then
                    Dim objShipdetails As New CustomerLookup.cusShipToDetails()
                    Dim sAccountNumber = CType(CustomerLookUp1.FindControl("txtAccountNumber"), TextBox).Text
                    'Sasikumar WP SPLUS_WP014
                    'objShipdetails = objCustomerLookup.getShippingInformation(sAccountNumber, "", "", "")
                    objShipdetails = objCustomerLookup.getShippingInformation(sAccountNumber, "", "", "", objGlobalData.SalesOrganization, objGlobalData.DistributionChannel, objGlobalData.Division)
                    bld.Append("SAPShipToAccountNumber= " & objShipdetails.SAPPayerAccountNumber & vbNewLine)
                    bld.Append("Detail1= " & objShipdetails.Detail1 & vbNewLine)
                    bld.Append("Detail2= " & objShipdetails.Detail2 & vbNewLine)
                    bld.Append("Detail3= " & objShipdetails.Detail3 & vbNewLine)
                    bld.Append("Detail4= " & objShipdetails.Detail4 & vbNewLine)
                    bld.Append("Detail5= " & objShipdetails.Detail5 & vbNewLine)
                    bld.Append("errorMessage= " & objShipdetails.errorMessage & vbNewLine)
                    For Each objArrayOfShipToItem As CustomerLookup.ShipTo In objShipdetails.ShipTo
                        bld.Append(vbNewLine)
                        bld.Append("Address1= " & objArrayOfShipToItem.Address1 & vbNewLine)
                        bld.Append("Address2= " & objArrayOfShipToItem.Address2 & vbNewLine)
                        bld.Append("Address3= " & objArrayOfShipToItem.Address3 & vbNewLine)
                        bld.Append("City= " & objArrayOfShipToItem.City & vbNewLine)
                        bld.Append("Attention= " & objArrayOfShipToItem.Attention & vbNewLine)
                        bld.Append("Name= " & objArrayOfShipToItem.Name & vbNewLine)
                        bld.Append("Phone= " & objArrayOfShipToItem.Phone & vbNewLine)
                        bld.Append("Phone Extension= " & objArrayOfShipToItem.PhoneExtn & vbNewLine)
                        bld.Append("State= " & objArrayOfShipToItem.State & vbNewLine)
                        bld.Append("Zip= " & objArrayOfShipToItem.Zip & vbNewLine)
                        bld.Append("Country= " & objArrayOfShipToItem.Country & vbNewLine)
                        bld.Append("ShipToDetail1= " & objArrayOfShipToItem.ShipToDetail1 & vbNewLine)
                        bld.Append("ShipToDetail2= " & objArrayOfShipToItem.ShipToDetail2 & vbNewLine)
                        bld.Append("ShipToDetail3= " & objArrayOfShipToItem.ShipToDetail3 & vbNewLine)
                        bld.Append("ShipToDetail4= " & objArrayOfShipToItem.ShipToDetail4 & vbNewLine)
                        bld.Append("ShipToDetail5= " & objArrayOfShipToItem.ShipToDetail5 & vbNewLine)
                        bld.Append(vbNewLine)
                    Next
                End If
                DisplayElapsedTime()
            Catch ex As Exception
                Utilities.WrapExceptionforUI(ex)
                bld.Append("Exception:" & ex.Message.ToString())
                objPCILogger.IndicationSuccessFailure = "Failure" '6524
                objPCILogger.Message = "CustomerLookUp Failed. " & ex.Message.ToString() '6524
            Finally
                objPCILogger.PushLogToMSMQ() '6524
            End Try
            Me.txtDataReturned.Text = bld.ToString()
        End Sub

        Private Sub CancelOrder()
            Dim bld As New System.Text.StringBuilder()
            Dim objPCILogger As New PCILogger() '6524
            Try
                Dim OrderNumber As String = CType(CancelOrder1.FindControl("txtOrderNumber"), TextBox).Text
                Dim ReasonforRejection As String = CType(CancelOrder1.FindControl("txtReasonforRejection"), TextBox).Text

                Dim LineNumbers() As String = CType(CancelOrder1.FindControl("txtLineNumbers"), TextBox).Text.Split(vbCrLf)
                Dim objCancelorder As New CancelOrder.cancelOrder()
                Dim objCancelOrderRequest As New CancelOrder.cancelOrderRequest()
                Dim objCancelOrderResponse As New CancelOrder.cancelOrderResponse
                Dim objDetails As New CancelOrder.updateOrderDetails()
                Dim objLine As New CancelOrder.lineNumbers
                ConfigurationData.WebmethodTest.Load(ConfigurationData.GeneralSettings.Environment)
                Dim Cert As X509Certificate

                '6524 start
                objPCILogger.EventOriginApplication = "ServicesPLUS"
                objPCILogger.EventOriginApplicationLocation = Me.Page.GetType().Name
                objPCILogger.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
                objPCILogger.OperationalUser = Environment.UserName
                objPCILogger.EventDateTime = DateTime.Now.ToLongTimeString()
                objPCILogger.HTTPRequestObjectValues = objPCILogger.GetRequestContextValue()
                objPCILogger.IndicationSuccessFailure = "Success"
                objPCILogger.EventOriginMethod = "CancelOrder"
                objPCILogger.Message = "CancelOrder Success."
                objPCILogger.EventType = EventType.Others
                '6524 end

                ConfigurationData.WebmethodTest.Load(ConfigurationData.GeneralSettings.Environment)
                objCancelorder.Credentials = getCredential()
                objCancelorder.Url = ConfigurationData.WebmethodTest.CancelOrder
                Cert = X509Certificate.CreateFromCertFile(ConfigurationData.WebmethodTest.CertificatePath)
                'ServicePointManager.CertificatePolicy = New TrustAllCertificatePolicy()
                ServicePointManager.Expect100Continue = True
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12
                ServicePointManager.ServerCertificateValidationCallback = New RemoteCertificateValidationCallback(AddressOf ValidateRemoteCertificate)
                objCancelorder.ClientCertificates.Add(Cert)

                objCancelOrderRequest.orderNumber = OrderNumber
                objCancelOrderRequest.reasonForRejection = ReasonforRejection

                objLine.lineNumber = LineNumbers
                objCancelOrderRequest.lineNumbers = objLine

                Dim objNumber As String = String.Empty
                For Each objNumber In LineNumbers
                    bld.Append("LineNumber=" & objNumber & vbNewLine)
                Next
                bld.Append("OrderNumber=" & OrderNumber & vbNewLine)
                bld.Append("ReasonforRejection=" & ReasonforRejection & vbNewLine)

                DisplayDataSent(bld)

                objCancelOrderResponse = objCancelorder.CallcancelOrder(objCancelOrderRequest)

                If Not objCancelOrderResponse Is Nothing Then
                    For Each objCancel As CancelOrder.cancelOrderResult In objCancelOrderResponse.cancelOrderResult
                        bld.Append("ErrorNumber:" & objCancel.errorNumber & vbNewLine)
                        bld.Append("ErrorMessage:" & objCancel.errorMessage & vbNewLine)
                        bld.Append("OrderNumber:" & objCancel.orderNumber & vbNewLine)
                        If Not objCancel.updateOrderDetails Is Nothing Then
                            For Each objDetails In objCancel.updateOrderDetails
                                bld.Append("lineNumber:" & objDetails.lineNumber & vbNewLine)
                                bld.Append("lineStatus:" & objDetails.lineStatus & vbNewLine)
                                bld.Append("partNumber:" & objDetails.partNumber & vbNewLine)
                                bld.Append("updatePrice:" + objDetails.updatedPrice & vbNewLine)
                                bld.Append("lineStatusMessage:" & objDetails.lineStatusMessage & vbNewLine)
                                bld.Append("partQuantity:" & objDetails.partQuantity & vbNewLine)
                            Next
                        End If
                    Next
                End If



                ' bld.Append("ErrorNumber: " & objCancelOrderResponse.cancelOrderResult.errorNumber & vbNewLine & "ErrorMessage: " & objCancelOrderResponse.cancelOrderResult.errorMessage & vbNewLine)


                DisplayElapsedTime()
            Catch ex As Exception
                Utilities.WrapExceptionforUI(ex)
                bld.Append("Exception:" & ex.Message.ToString())
                objPCILogger.IndicationSuccessFailure = "Failure" '6524
                objPCILogger.Message = "CancelOrder Failed. " & ex.Message.ToString() '6524
            Finally
                objPCILogger.PushLogToMSMQ() '6524
            End Try
            Me.txtDataReturned.Text = bld.ToString()
        End Sub

        Private Sub OrderInquiry()
            Dim bld As New System.Text.StringBuilder()
            Dim objPCILogger As New PCILogger() '6524
            Try
                Dim objpoRequest As New PoInquiry.poRequest()
                Dim objpoResponse As New PoInquiry.poResponse()
                Dim objpoInquiry As New PoInquiry.poInquiry()
                Dim objOrderDetail As New PoInquiry.OrderDetail()
                Dim objInvoices As New PoInquiry.Invoices()

                Dim Cert As X509Certificate

                '6524 start
                objPCILogger.EventOriginApplication = "ServicesPLUS"
                objPCILogger.EventOriginApplicationLocation = Me.Page.GetType().Name
                objPCILogger.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
                objPCILogger.OperationalUser = Environment.UserName
                objPCILogger.EventDateTime = DateTime.Now.ToLongTimeString()
                objPCILogger.HTTPRequestObjectValues = objPCILogger.GetRequestContextValue()
                objPCILogger.IndicationSuccessFailure = "Success"
                objPCILogger.EventOriginMethod = "OrderInquiry"
                objPCILogger.Message = "OrderInquiry Success."
                objPCILogger.EventType = EventType.View
                '6524 end

                ConfigurationData.WebmethodTest.Load(ConfigurationData.GeneralSettings.Environment)
                objpoInquiry.Credentials = getCredential()
                objpoInquiry.Url = ConfigurationData.WebmethodTest.POInquiry
                Cert = X509Certificate.CreateFromCertFile(ConfigurationData.WebmethodTest.CertificatePath)
                'ServicePointManager.CertificatePolicy = New TrustAllCertificatePolicy()
                ServicePointManager.Expect100Continue = True
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12
                ServicePointManager.ServerCertificateValidationCallback = New RemoteCertificateValidationCallback(AddressOf ValidateRemoteCertificate)
                objpoInquiry.ClientCertificates.Add(Cert)

                objpoRequest.P_SENDER = CType(OrderInquiry1.FindControl("txtPOSender"), TextBox).Text
                objpoRequest.P_SO_NUMBER = CType(OrderInquiry1.FindControl("txtOrderNumber"), TextBox).Text
                objpoRequest.Language = CType(OrderInquiry1.FindControl("txtLanguage"), TextBox).Text 'Sasikumar WP SPLUS_WP014


                If CType(OrderInquiry1.FindControl("chkDetail"), CheckBox).Checked Then
                    objpoRequest.P_DETAIL_FLAG = "Y"
                Else
                    objpoRequest.P_DETAIL_FLAG = "N"
                End If

                If CType(OrderInquiry1.FindControl("chkHeader"), CheckBox).Checked Then
                    objpoRequest.P_HEADER_FLAG = "Y"
                Else
                    objpoRequest.P_HEADER_FLAG = "N"
                End If

                If CType(OrderInquiry1.FindControl("chkInvoice"), CheckBox).Checked Then
                    objpoRequest.P_INVOICE_FLAG = "Y"
                Else
                    objpoRequest.P_INVOICE_FLAG = "N"
                End If

                bld.Append("P_SO_NUMBER=" & CType(OrderInquiry1.FindControl("txtOrderNumber"), TextBox).Text & vbNewLine)
                bld.Append("P_SENDER=" & CType(OrderInquiry1.FindControl("txtPOSender"), TextBox).Text & vbNewLine)
                bld.Append("P_LANGUAGE=" & CType(OrderInquiry1.FindControl("txtLanguage"), TextBox).Text & vbNewLine) 'Sasikumar WP SPLUS_WP014
                bld.Append("P_DETAIL_FLAG=" & objpoRequest.P_DETAIL_FLAG & vbNewLine)
                bld.Append("P_HEADER_FLAG=" & objpoRequest.P_HEADER_FLAG & vbNewLine)
                bld.Append("P_INVOICE_FLAG=" & objpoRequest.P_INVOICE_FLAG & vbNewLine)

                DisplayDataSent(bld)

                objpoResponse = objpoInquiry.processPoInquiry(objpoRequest)

                If objpoResponse.ErrorMessage = "" Then
                    bld.Append("USERID=" & objpoResponse.USERID & vbNewLine)
                    bld.Append("SOURCE=" & objpoResponse.SOURCE & vbNewLine)
                    bld.Append("ACCOUNTNUMBER=" & objpoResponse.ACCOUNTNUMBER & vbNewLine)
                    bld.Append("CUSTOMERPO=" & objpoResponse.CUSTOMERPO & vbNewLine)
                    bld.Append("EVENTID=" & objpoResponse.EVENTID & vbNewLine)
                    bld.Append("SHIPPINGINFO= " & objpoResponse.SHIPPINGINFO & vbNewLine)
                    bld.Append("SHIPPINGCHARGE=" & objpoResponse.SHIPPINGCHARGE & vbNewLine)
                    bld.Append("HANDLINGCHARGE=" & objpoResponse.HANDLINGCHARGE & vbNewLine)

                    bld.Append("AMOUNT=" & objpoResponse.AMOUNT & vbNewLine)

                    bld.Append("TOTALORDERAMOUNT=" & objpoResponse.TOTALORDERAMOUNT & vbNewLine)

                    bld.Append("TOTALRECYCLINGFEES=" & objpoResponse.TOTALRECYCLINGFEES & vbNewLine)
                    bld.Append("SHIPTOATTN=" & objpoResponse.OUTPUT3 & vbNewLine)
                    bld.Append("SHIPTONAME=" & objpoResponse.SHIPTONAME & vbNewLine)
                    bld.Append("SHIPTOADDRESS1=" & objpoResponse.SHIPTOADDRESS1 & vbNewLine)
                    bld.Append("SHIPTOADDRESS2=" & objpoResponse.SHIPTOADDRESS2 & vbNewLine)
                    bld.Append("SHIPTOADDRESS3=" & objpoResponse.SHIPTOADDRESS3 & vbNewLine)
                    bld.Append("SHIPTOADDRESS4=" & objpoResponse.SHIPTOADDRESS4 & vbNewLine)
                    bld.Append("SHIPTOSTATE=" & objpoResponse.SHIPTOSTATE & vbNewLine)
                    bld.Append("SHIPTOZIP=" & objpoResponse.SHIPTOZIP & vbNewLine)
                    bld.Append("SHIPTOCOUNTRY=" & objpoResponse.SHIPTOCOUNTRY & vbNewLine)
                    bld.Append("SHIPTOPHONENUMBER=" & objpoResponse.SHIPTOPHONENUMBER & vbNewLine)
                    bld.Append("SHIPTOOFFICENUMBER=" & objpoResponse.SHIPTOOFFICENUMBER & vbNewLine)
                    bld.Append("BILLTOATTN=" & objpoResponse.BILLTO_ATTENTION & vbNewLine)
                    bld.Append("BILLTONAME=" & objpoResponse.BILLTONAME & vbNewLine)
                    bld.Append("BILLTOADDRESS1=" & objpoResponse.BILLTOADDRESS1 & vbNewLine)
                    bld.Append("BILLTOADDRESS2=" & objpoResponse.BILLTOADDRESS2 & vbNewLine)
                    bld.Append("BILLTOADDRESS3=" & objpoResponse.BILLTOADDRESS3 & vbNewLine)
                    bld.Append("BILLTOADDRESS4=" & objpoResponse.BILLTOADDRESS4 & vbNewLine)
                    bld.Append("BILLTOSTATE=" & objpoResponse.BILLTOSTATE & vbNewLine)
                    bld.Append("BILLTOZIP=" & objpoResponse.BILLTOZIP & vbNewLine)
                    bld.Append("BILLTOCOUNTRY=" & objpoResponse.BILLTOCOUNTRY & vbNewLine)
                    bld.Append("CHARGECARD=" & objpoResponse.CHARGECARD & vbNewLine)
                    bld.Append("CHARGEEXP=" & objpoResponse.CHARGEEXP & vbNewLine)
                    bld.Append("STATUS=" & objpoResponse.STATUS & vbNewLine)
                    bld.Append("STATUSDATE=" & objpoResponse.STATUSDATE & vbNewLine)
                    bld.Append("RECEIVEDDATE=" & objpoResponse.RECEIVEDDATE & vbNewLine)
                    bld.Append("PONUMBER=" & objpoResponse.PONUMBER & vbNewLine)
                    bld.Append(pcSeparator & vbNewLine)

                    For Each objArrayOfOrderDetailItem In objpoResponse.OrderDetail()
                        bld.Append("LINENUMBER= " & objArrayOfOrderDetailItem.LINENUMBER & vbNewLine)
                        bld.Append("LINESTATUS= " & objArrayOfOrderDetailItem.LINESTATUS & vbNewLine)
                        bld.Append("LINESTATUSDATE= " & objArrayOfOrderDetailItem.LINESTATUSDATE & vbNewLine)
                        bld.Append("PARTNUMBER= " & objArrayOfOrderDetailItem.PARTNUMBER & vbNewLine)
                        bld.Append("PARTQUANTITY= " & objArrayOfOrderDetailItem.PARTQUANTITY & vbNewLine)
                        bld.Append("CUSTOMERPRICE= " & objArrayOfOrderDetailItem.CUSTOMERPRICE & vbNewLine)
                        bld.Append("RECYCLINGFEE= " & objArrayOfOrderDetailItem.RECYCLINGFEE & vbNewLine)
                        bld.Append("PARTDESCRIPTION= " & objArrayOfOrderDetailItem.PARTDESCRIPTION & vbNewLine)
                        bld.Append("INVOICENUMBER= " & objArrayOfOrderDetailItem.INVOICENUMBER & vbNewLine)
                        bld.Append("BACKORDEREDQUANTITY= " & objArrayOfOrderDetailItem.BACKORDEREDQUANTITY & vbNewLine)
                        bld.Append("CUSTOMERPO= " & objArrayOfOrderDetailItem.OUTPUT1B & vbNewLine)
                        bld.Append("OUTPUT2B= " & objArrayOfOrderDetailItem.OUTPUT2B & vbNewLine)
                        bld.Append("OUTPUT3B= " & objArrayOfOrderDetailItem.OUTPUT3B & vbNewLine)
                        bld.Append("OUTPUT4B= " & objArrayOfOrderDetailItem.OUTPUT4B & vbNewLine)
                        bld.Append("LISTPRICE= " & objArrayOfOrderDetailItem.OUTPUT5B & vbNewLine)
                        bld.Append(vbNewLine)
                    Next

                    If Not objpoResponse.Invoices() Is Nothing Then
                        bld.Append(pcSeparator & vbNewLine)
                        For Each objArrayOfInvoicesItem In objpoResponse.Invoices()
                            If Not objArrayOfInvoicesItem Is Nothing Then
                                bld.Append("DOC_NUMBER= " & objArrayOfInvoicesItem.DOC_NUMBER & vbNewLine)
                                bld.Append("DATED= " & objArrayOfInvoicesItem.DATED & vbNewLine)
                                bld.Append("CARRIERCODE= " & objArrayOfInvoicesItem.CARRIERCODE & vbNewLine)
                                bld.Append("TRACKINGNUMBER = " & objArrayOfInvoicesItem.TRACKINGNUMBER & vbNewLine)
                                bld.Append("GRANDTOTAL= " & objArrayOfInvoicesItem.GRANDTOTAL & vbNewLine)
                                bld.Append("PARTSUBTOTAL= " & objArrayOfInvoicesItem.PARTSUBTOTAL & vbNewLine)
                                bld.Append("SHIPPINGAMOUNT= " & objArrayOfInvoicesItem.SHIPPINGAMOUNT & vbNewLine)
                                bld.Append("TAXAMOUNT= " & objArrayOfInvoicesItem.TAXAMOUNT & vbNewLine)
                                bld.Append("RECYCLINGFEE= " & objArrayOfInvoicesItem.RECYCLINGFEE & vbNewLine)
                                bld.Append("OUTPUT1A= " & objArrayOfInvoicesItem.OUTPUT1A & vbNewLine)
                                bld.Append("OUTPUT2A= " & objArrayOfInvoicesItem.OUTPUT2A & vbNewLine)
                                bld.Append("OUTPUT3A= " & objArrayOfInvoicesItem.OUTPUT3A & vbNewLine)
                                bld.Append("OUTPUT4A= " & objArrayOfInvoicesItem.OUTPUT4A & vbNewLine)
                                bld.Append("OUTPUT5A= " & objArrayOfInvoicesItem.OUTPUT5A & vbNewLine)
                                bld.Append(vbNewLine)
                            End If
                        Next
                    End If

                    bld.Append(pcSeparator & vbNewLine)
                    bld.Append("CREDITSWITCH=" & objpoResponse.CREDITSWITCH & vbNewLine)
                    bld.Append("CREDITREASON=" & objpoResponse.CREDITREASON & vbNewLine)
                    bld.Append("LEGACYACCOUNTNUMBER=" & objpoResponse.LEGACYACCOUNTNUMBER & vbNewLine)
                    bld.Append("INTERNALORDER=" & objpoResponse.INTERNALORDER & vbNewLine)
                    bld.Append("CUSTOMERCLASS=" & objpoResponse.CUSTOMERCLASS & vbNewLine)
                    bld.Append("GROUPNUMBER=" & objpoResponse.GROUPNUMBER & vbNewLine)
                    bld.Append("CARRIERCODE=" & objpoResponse.CARRIERCODE & vbNewLine)
                    bld.Append("POREFERENCE=" & objpoResponse.POREFERENCE & vbNewLine)
                    bld.Append("ErrorNumber=" & objpoResponse.ErrorNumber & vbNewLine)
                    bld.Append("ErrorMessage=" & objpoResponse.ErrorMessage & vbNewLine)
                    bld.Append("OUTPUT1=" & objpoResponse.OUTPUT1 & vbNewLine)
                    bld.Append("OUTPUT2=" & objpoResponse.OUTPUT2 & vbNewLine)
                    bld.Append("OUTPUT3=" & objpoResponse.OUTPUT3 & vbNewLine)
                    bld.Append("OUTPUT4=" & objpoResponse.OUTPUT4 & vbNewLine)
                    bld.Append("OUTPUT5=" & objpoResponse.OUTPUT5 & vbNewLine)
                    bld.Append("TOTALSHIPPING=" & objpoResponse.TOTALSHIPPING & vbNewLine)
                    bld.Append("BILLTO_PHONE=" & objpoResponse.BILLTO_PHONE & vbNewLine)
                    bld.Append("TOTALORDERAMOUNT=" & objpoResponse.TOTALORDERAMOUNT & vbNewLine)
                    bld.Append("TOTALTAXAMOUNT=" & objpoResponse.TOTALTAXAMOUNT & vbNewLine)
                    bld.Append("PARTSUBTOTAL=" & objpoResponse.PARTSUBTOTAL & vbNewLine)
                    bld.Append("TOTALRECYCLINGFEES=" & objpoResponse.TOTALRECYCLINGFEES & vbNewLine)
                Else
                    bld.Append("ErrorNumber: " & objpoResponse.ErrorNumber & vbNewLine & "ErrorMessage: " & objpoResponse.ErrorMessage & vbNewLine)
                End If



                DisplayElapsedTime()
            Catch ex As Exception
                Utilities.WrapExceptionforUI(ex)
                bld.Append("Exception= " & ex.Message)
                objPCILogger.IndicationSuccessFailure = "Failure" '6524
                objPCILogger.Message = "OrderInquiry Failed. " & ex.Message.ToString() '6524
            Finally
                objPCILogger.PushLogToMSMQ() '6524
            End Try
            Me.txtDataReturned.Text = bld.ToString()
        End Sub

        Private Sub PartInquiry()
            Dim bld As New System.Text.StringBuilder()
            Dim objResult As String = String.Empty
            Dim objRequest As String = String.Empty
            Dim objPCILogger As New PCILogger()

            Try
                ConfigurationData.WebmethodTest.Load(ConfigurationData.GeneralSettings.Environment)
                Dim objpartInquiry = New PartInquiry.partInquiry()
                Dim objPartInquiryRequest = New PartInquiry.PartInquiryRequest()
                Dim objPartInquiryResponse = New PartInquiry.PartInquiryResponse()

                Dim PartNumbers() As String = CType(PartInquiry1.FindControl("txtPartNumbers"), TextBox).Text.Split(vbCrLf)
                ' ASleight - This appears to allow multiple parts, but the textbox isn't multiline. Should be fixed one way or the other. Doubt "vbCrLf" is correct, though.

                Dim Cert As X509Certificate
                objPCILogger.EventOriginApplication = "ServicesPLUS"
                objPCILogger.EventOriginApplicationLocation = Me.Page.GetType().Name
                objPCILogger.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
                objPCILogger.OperationalUser = Environment.UserName
                objPCILogger.EventDateTime = DateTime.Now.ToLongTimeString()
                objPCILogger.HTTPRequestObjectValues = objPCILogger.GetRequestContextValue()
                objPCILogger.IndicationSuccessFailure = "Success"
                objPCILogger.EventOriginMethod = "PartInquiry"
                objPCILogger.Message = "PartInquiry Success."
                objPCILogger.EventType = EventType.View

                ConfigurationData.WebmethodTest.Load(ConfigurationData.GeneralSettings.Environment)
                objpartInquiry.Credentials = getCredential()
                objpartInquiry.Url = ConfigurationData.WebmethodTest.PartInquiry
                Cert = X509Certificate.CreateFromCertFile(ConfigurationData.WebmethodTest.CertificatePath)
                'ServicePointManager.CertificatePolicy = New TrustAllCertificatePolicy()
                ServicePointManager.Expect100Continue = True
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12
                ServicePointManager.ServerCertificateValidationCallback = New RemoteCertificateValidationCallback(AddressOf ValidateRemoteCertificate)
                objpartInquiry.ClientCertificates.Add(Cert)

                objPartInquiryRequest.SapAcNumber = CType(PartInquiry1.FindControl("txtSAPAccountNumber"), TextBox).Text.Trim()
                objPartInquiryRequest.PriceInfo = CType(PartInquiry1.FindControl("txtPriceInfo"), TextBox).Text.Trim()
                objPartInquiryRequest.InventoryInfo = CType(PartInquiry1.FindControl("txtInventoryInfo"), TextBox).Text.Trim()
                objPartInquiryRequest.Location = CType(PartInquiry1.FindControl("txtLocation"), TextBox).Text.Trim()
                'Sasikumar WP SPLUS_WP014
                objPartInquiryRequest.SalesOrganization = CType(PartInquiry1.FindControl("txtSalesOrganization"), TextBox).Text.Trim()
                objPartInquiryRequest.DistributionChannel = CType(PartInquiry1.FindControl("txtDistributionChannel"), TextBox).Text.Trim()
                objPartInquiryRequest.Division = CType(PartInquiry1.FindControl("txtDivision"), TextBox).Text.Trim()
                objPartInquiryRequest.Language = CType(PartInquiry1.FindControl("txtLanguage"), TextBox).Text.Trim()

                Dim objPartNumber As New PartInquiry.PartNumber()
                Dim arrPartNumber As PartInquiry.PartNumber()
                arrPartNumber = New PartInquiry.PartNumber(PartNumbers.Count - 1) {}
                Dim iCount As Integer = 0
                Dim iLenth As Integer = PartNumbers.Count

                Dim objPart1 As String = String.Empty
                For Each objPart1 In PartNumbers
                    ''bld.Append("PartNumber=" & objPart & vbNewLine)
                    If iCount <= iLenth Then
                        Dim objlPartNumber As New PartInquiry.PartNumber
                        objlPartNumber.PartNumber1 = objPart1.Trim()

                        arrPartNumber(iCount) = objlPartNumber
                        iCount = iCount + 1
                    End If

                Next
                objPartInquiryRequest.PartNumber = arrPartNumber

                'Part Numbers to be included

                Dim objPart As String = String.Empty
                For Each objPart In PartNumbers
                    bld.Append("PartNumber=" & objPart & vbNewLine)
                Next

                bld.Append("SapAcNumber=" & CType(PartInquiry1.FindControl("txtSAPAccountNumber"), TextBox).Text & vbNewLine)
                bld.Append("PriceInfo=" & CType(PartInquiry1.FindControl("txtPriceInfo"), TextBox).Text & vbNewLine)
                bld.Append("InventoryInfo=" & CType(PartInquiry1.FindControl("txtInventoryInfo"), TextBox).Text & vbNewLine)
                bld.Append("Location=" & CType(PartInquiry1.FindControl("txtLocation"), TextBox).Text & vbNewLine)
                'Sasikumar WP SPLUS_WP014
                bld.Append("DistributionChannel=" & CType(PartInquiry1.FindControl("txtDistributionChannel"), TextBox).Text & vbNewLine)
                bld.Append("Division=" & CType(PartInquiry1.FindControl("txtDivision"), TextBox).Text & vbNewLine)
                bld.Append("SalesOrganization=" & CType(PartInquiry1.FindControl("txtSalesOrganization"), TextBox).Text & vbNewLine)
                bld.Append("Language=" & CType(PartInquiry1.FindControl("txtLanguage"), TextBox).Text & vbNewLine)

                DisplayDataSent(bld)
                'Dim objSAPHelper As New SAPHelper()
                'objPartInquiryResponse = objSAP.PartInquiry1(ConfigurationData.WebmethodTest.DefaultAccount, Nothing, PartNumbers, "X", "X", "")
                'objRequest = objSAPHelper.SerialiseObject(objPartInquiryRequest)
                objPartInquiryResponse = objpartInquiry.ProcessPartInquiryResponse(objPartInquiryRequest)


                'objResult = objSAPHelper.SerialiseObject(objPartInquiryResponse)
                For Each objArrayOfPartInquiryResponseItem In objPartInquiryResponse.PartInquiryResponse1
                    bld.Append(vbNewLine)
                    bld.Append("NumberAvailable= " & objArrayOfPartInquiryResponseItem.NumberAvailable & vbNewLine)
                    bld.Append("NewPartNumber= " & objArrayOfPartInquiryResponseItem.NewPartNumber & vbNewLine)
                    bld.Append("ReplacementPartNumber= " & objArrayOfPartInquiryResponseItem.ReplacementPartNumber & vbNewLine)
                    bld.Append("InternalPrice = " & objArrayOfPartInquiryResponseItem.InternalPrice & vbNewLine)
                    bld.Append("EffectiveDate= " & objArrayOfPartInquiryResponseItem.EffectiveDate & vbNewLine)
                    bld.Append("LastDeliveryDate= " & objArrayOfPartInquiryResponseItem.LastDeliveryDate & vbNewLine)
                    bld.Append("_x0032_ndLastDeliveryDate= " & objArrayOfPartInquiryResponseItem.Item2ndLastDeliveryDate & vbNewLine)
                    bld.Append("DeliveryQty= " & objArrayOfPartInquiryResponseItem.DeliveryQty & vbNewLine)
                    bld.Append("_x0032_ndLastShippingCode= " & objArrayOfPartInquiryResponseItem.Item2ndLastShippingCode & vbNewLine)
                    bld.Append("DeliveryQty2= " & objArrayOfPartInquiryResponseItem.DeliveryQty2 & vbNewLine)
                    bld.Append("LastSaleDate= " & objArrayOfPartInquiryResponseItem.LastSaleDate & vbNewLine)
                    bld.Append("LastReceivedDate= " & objArrayOfPartInquiryResponseItem.LastReceivedDate & vbNewLine)
                    bld.Append("POPendingQty= " & objArrayOfPartInquiryResponseItem.POPendingQty & vbNewLine)
                    bld.Append("IntransitQty= " & objArrayOfPartInquiryResponseItem.IntransitQty & vbNewLine)
                    bld.Append("ConsumptionTotal= " & objArrayOfPartInquiryResponseItem.ConsumptionTotal & vbNewLine)
                    bld.Append("PartDescription= " & objArrayOfPartInquiryResponseItem.PartDescription & vbNewLine)
                    bld.Append("ErrorMessage= " & objArrayOfPartInquiryResponseItem.ErrorMessage & vbNewLine)
                    bld.Append("ErrorNumber= " & objArrayOfPartInquiryResponseItem.ErrorNumber & vbNewLine)
                    bld.Append("Replacement= " & objArrayOfPartInquiryResponseItem.Replacement & vbNewLine)
                    bld.Append("RecyclingFlag= " & objArrayOfPartInquiryResponseItem.RecyclingFlag & vbNewLine)
                    bld.Append("ProgramCode= " & objArrayOfPartInquiryResponseItem.ProgramCode & vbNewLine)
                    bld.Append("DiscountCode= " & objArrayOfPartInquiryResponseItem.DiscountCode & vbNewLine)
                    bld.Append("DiscountPercentage= " & objArrayOfPartInquiryResponseItem.DiscountPercentage & vbNewLine)
                    bld.Append("ListPrice= " & objArrayOfPartInquiryResponseItem.ListPrice & vbNewLine)
                    bld.Append("YourPrice= " & objArrayOfPartInquiryResponseItem.YourPrice & vbNewLine)
                    bld.Append("CoreCharge= " & objArrayOfPartInquiryResponseItem.CoreCharge & vbNewLine)
                    bld.Append("ItemShippedFrom= " & objArrayOfPartInquiryResponseItem.ItemShippedFrom & vbNewLine)
                    ''Commented for bug # 5993
                    ''bld.Append("BillingOnly= " & objArrayOfPartInquiryResponseItem.BillingOnly & vbNewLine)
                    bld.Append("OUTPUT1= " & objArrayOfPartInquiryResponseItem.OUTPUT1 & vbNewLine)
                    bld.Append("OUTPUT2= " & objArrayOfPartInquiryResponseItem.OUTPUT2 & vbNewLine)
                    bld.Append("OUTPUT3= " & objArrayOfPartInquiryResponseItem.OUTPUT3 & vbNewLine)
                    bld.Append("OUTPUT4= " & objArrayOfPartInquiryResponseItem.OUTPUT4 & vbNewLine)
                    bld.Append("OUTPUT5= " & objArrayOfPartInquiryResponseItem.OUTPUT5 & vbNewLine)
                    If Not objArrayOfPartInquiryResponseItem.Kitinfo Is Nothing Then
                        For Each objArrayOfKitinfoItem In objArrayOfPartInquiryResponseItem.Kitinfo
                            bld.Append("Kit Details")
                            bld.Append("ItemNumber= " & objArrayOfKitinfoItem.ItemNumber & vbNewLine)
                            bld.Append("StatusFlag= " & objArrayOfKitinfoItem.StatusFlag & vbNewLine)
                            bld.Append("StatusMessage= " & objArrayOfKitinfoItem.StatusMessage & vbNewLine)
                            bld.Append("ItemQuantity= " & objArrayOfKitinfoItem.ItemQuantity & vbNewLine)
                            bld.Append("Description= " & objArrayOfKitinfoItem.Description & vbNewLine)
                            bld.Append("Availability= " & objArrayOfKitinfoItem.Availability & vbNewLine)
                            bld.Append("ItemListPrice= " & objArrayOfKitinfoItem.ItemListPrice & vbNewLine)
                            bld.Append("ItemYourPrice= " & objArrayOfKitinfoItem.ItemYourPrice & vbNewLine)
                            bld.Append("Category= " & objArrayOfKitinfoItem.Category & vbNewLine)
                            bld.Append("OUTPUT1A= " & objArrayOfKitinfoItem.OUTPUT1A & vbNewLine)
                            bld.Append("OUTPUT2A= " & objArrayOfKitinfoItem.OUTPUT2A & vbNewLine)
                            bld.Append("OUTPUT3A= " & objArrayOfKitinfoItem.OUTPUT3A & vbNewLine)
                            bld.Append("OUTPUT4A= " & objArrayOfKitinfoItem.OUTPUT4A & vbNewLine)
                            bld.Append("OUTPUT5A= " & objArrayOfKitinfoItem.OUTPUT5A & vbNewLine)
                        Next
                    End If
                    bld.Append(vbNewLine)
                    ' bld.Append(pcSeparator)
                Next
                DisplayElapsedTime()
            Catch ex As Exception
                bld.Append("Exception= " & ex.Message)
                objPCILogger.IndicationSuccessFailure = "Failure" '6524
                objPCILogger.Message = "PartInquiry Failed. " & ex.Message.ToString() '6524
            Finally
                objPCILogger.PushLogToMSMQ() '6524
            End Try
            Me.txtDataReturned.Text = bld.ToString()
        End Sub


        Private Sub SalesOrder()
            Dim bld As New System.Text.StringBuilder()
            Dim objPCILogger As New PCILogger() '6524
            Try

                '6524 start
                objPCILogger.EventOriginApplication = "ServicesPLUS"
                objPCILogger.EventOriginApplicationLocation = Me.Page.GetType().Name
                objPCILogger.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
                objPCILogger.OperationalUser = Environment.UserName
                objPCILogger.EventDateTime = DateTime.Now.ToLongTimeString()
                objPCILogger.HTTPRequestObjectValues = objPCILogger.GetRequestContextValue()
                objPCILogger.IndicationSuccessFailure = "Success"
                objPCILogger.EventOriginMethod = "SalesOrder"
                objPCILogger.Message = "SalesOrder Success."
                objPCILogger.EventType = EventType.View
                '6524 end

                Dim objSales As New OrderInjection.createSalesOrderInjection()
                Dim salesOderRequest As New OrderInjection.SalesOrderRequest()
                Dim salesOderResponse As New OrderInjection.SalesOrderResponse()
                Dim objOrderDetails As New OrderInjection.OrderDetails()
                Dim objshipinformation As New OrderInjection.ShipInformation()
                Dim objBillInformation As New OrderInjection.BillInformation()
                Dim objsalesRequest1 As New OrderInjection.SalesOrderRequest1()

                Dim objlineDetails As New OrderInjection.LineDetails()
                Dim objLineDetails1 As New OrderInjection.LineDetails()
                Dim arrLinedetails As OrderInjection.LineDetails()
                arrLinedetails = New OrderInjection.LineDetails(5) {}

                'Billing Information
                bld.Append("BillToName1 =" & CType(SaleOrder1.FindControl("txtBillToName1"), TextBox).Text & vbNewLine)
                'bld.Append("BillToName2 =" & CType(SaleOrder1.FindControl("txtBillToName2"), TextBox).Text & vbNewLine)
                'bld.Append("BillToName3 =" & CType(SaleOrder1.FindControl("txtBillToName3"), TextBox).Text & vbNewLine)
                bld.Append("City =" & CType(SaleOrder1.FindControl("txtCity"), TextBox).Text & vbNewLine)
                bld.Append("Country =" & CType(SaleOrder1.FindControl("txtCountry"), TextBox).Text & vbNewLine)
                bld.Append("District =" & CType(SaleOrder1.FindControl("txtDistrict"), TextBox).Text & vbNewLine)
                bld.Append("Email =" & CType(SaleOrder1.FindControl("txtEmail"), TextBox).Text & vbNewLine)
                bld.Append("SoldTo =" & CType(SaleOrder1.FindControl("txtSoldTo"), TextBox).Text & vbNewLine)
                bld.Append("State=" & CType(SaleOrder1.FindControl("txtState"), TextBox).Text & vbNewLine)
                bld.Append("Street =" & CType(SaleOrder1.FindControl("txtStreet"), TextBox).Text & vbNewLine)
                bld.Append("Street2 =" & CType(SaleOrder1.FindControl("txtStreet2"), TextBox).Text & vbNewLine)
                bld.Append("ZipCode=" & CType(SaleOrder1.FindControl("txtZipCode"), TextBox).Text & vbNewLine)

                'Ship Information
                bld.Append("BusExt =" & CType(SaleOrder1.FindControl("txtBusExt"), TextBox).Text & vbNewLine)
                bld.Append("City =" & CType(SaleOrder1.FindControl("txtshipinfoCity"), TextBox).Text & vbNewLine)
                bld.Append("Country =" & CType(SaleOrder1.FindControl("txtshipinfoCountry"), TextBox).Text & vbNewLine)
                bld.Append("District =" & CType(SaleOrder1.FindControl("txtshipinfoDistrict"), TextBox).Text & vbNewLine)
                'bld.Append("Email =" & CType(SaleOrder1.FindControl("txtshipinofEmail"), TextBox).Text & vbNewLine)
                bld.Append("Fax =" & CType(SaleOrder1.FindControl("txtshipinfoFax"), TextBox).Text & vbNewLine)
                bld.Append("FirstTelephoneNumber =" & CType(SaleOrder1.FindControl("txtshipinfoFirstTelephoneNumber"), TextBox).Text & vbNewLine)
                bld.Append("ShipTo =" & CType(SaleOrder1.FindControl("txtshipinfoShipTo"), TextBox).Text & vbNewLine)
                bld.Append("ShipToName1 =" & CType(SaleOrder1.FindControl("txtshipinfoShipToName1"), TextBox).Text & vbNewLine)
                'bld.Append("ShipToName2 =" & CType(SaleOrder1.FindControl("txtshipinfoShipToName2"), TextBox).Text & vbNewLine)
                bld.Append("ShipToName3 =" & CType(SaleOrder1.FindControl("txtshipinfoShipToName3"), TextBox).Text & vbNewLine)
                bld.Append("State =" & CType(SaleOrder1.FindControl("txtshipinfoState"), TextBox).Text & vbNewLine)
                bld.Append("Street =" & CType(SaleOrder1.FindControl("txtshipinfoStreet"), TextBox).Text & vbNewLine)
                bld.Append("Street2 =" & CType(SaleOrder1.FindControl("txtshipinfoStreet2"), TextBox).Text & vbNewLine)
                bld.Append("ZipCode =" & CType(SaleOrder1.FindControl("txtshipinfoZipCode"), TextBox).Text & vbNewLine)

                'Order Details
                bld.Append("CardName =" & CType(SaleOrder1.FindControl("txtCardName"), TextBox).Text & vbNewLine)
                bld.Append("CardNumber =" & CType(SaleOrder1.FindControl("txtCardNumber"), TextBox).Text & vbNewLine)
                bld.Append("CardType =" & CType(SaleOrder1.FindControl("txtCardType"), TextBox).Text & vbNewLine)
                bld.Append("CardVerificationValue =" & CType(SaleOrder1.FindControl("txtCardVerificationValue"), TextBox).Text & vbNewLine)
                'bld.Append("CreatedByCS3 =" & CType(SaleOrder1.FindControl("txtCreatedByCS3"), TextBox).Text & vbNewLine)
                'bld.Append("CustomerPODate =" & CType(SaleOrder1.FindControl("txtCustomerPODate"), TextBox).Text & vbNewLine)
                bld.Append("CustomerPONumber =" & CType(SaleOrder1.FindControl("txtCustomerPONumber"), TextBox).Text & vbNewLine)
                bld.Append("CustomerPOType =" & CType(SaleOrder1.FindControl("txtCustomerPOType"), TextBox).Text & vbNewLine)
                bld.Append("GroupCode =" & CType(SaleOrder1.FindControl("txtGroupCode"), TextBox).Text & vbNewLine)
                'bld.Append("HandlingCharge =" & CType(SaleOrder1.FindControl("txtHandlingCharge"), TextBox).Text & vbNewLine)
                'bld.Append("HeaderComments =" & CType(SaleOrder1.FindControl("txtHeaderComments"), TextBox).Text & vbNewLine)
                'bld.Append("Input1 =" & CType(SaleOrder1.FindControl("txtInput1"), TextBox).Text & vbNewLine)
                'bld.Append("Input2 =" & CType(SaleOrder1.FindControl("txtInput2"), TextBox).Text & vbNewLine)
                'bld.Append("Input3 =" & CType(SaleOrder1.FindControl("txtInput3"), TextBox).Text & vbNewLine)
                'bld.Append("Input4 =" & CType(SaleOrder1.FindControl("txtInput4"), TextBox).Text & vbNewLine)
                'bld.Append("Input5 =" & CType(SaleOrder1.FindControl("txtInput5"), TextBox).Text & vbNewLine)
                bld.Append("OrderEntryPerson =" & CType(SaleOrder1.FindControl("txtOrderEntryPerson"), TextBox).Text & vbNewLine)
                bld.Append("ReasonCode =" & CType(SaleOrder1.FindControl("txtReasonCode"), TextBox).Text & vbNewLine)
                'bld.Append("ShippingCharge =" & CType(SaleOrder1.FindControl("txtShippingCharge"), TextBox).Text & vbNewLine)
                bld.Append("TaxExempt =" & CType(SaleOrder1.FindControl("txtTaxExempt"), TextBox).Text & vbNewLine)
                bld.Append("ValidTo =" & CType(SaleOrder1.FindControl("txtValidTo"), TextBox).Text & vbNewLine)
                bld.Append("YourReference =" & CType(SaleOrder1.FindControl("txtYourReference"), TextBox).Text & vbNewLine)

                'Line Details
                bld.Append("Carrier =" & CType(SaleOrder1.FindControl("txtCarrierCode1"), TextBox).Text & vbNewLine)
                bld.Append("OrderQuantity =" & CType(SaleOrder1.FindControl("txtOrderQuantity1"), TextBox).Text & vbNewLine)
                bld.Append("PartNumber =" & CType(SaleOrder1.FindControl("txtPartNumber1"), TextBox).Text & vbNewLine)
                bld.Append("Carrier =" & CType(SaleOrder1.FindControl("txtCarrierCode2"), TextBox).Text & vbNewLine)
                bld.Append("OrderQuantity =" & CType(SaleOrder1.FindControl("txtOrderQuantity2"), TextBox).Text & vbNewLine)
                bld.Append("PartNumber =" & CType(SaleOrder1.FindControl("txtPartNumber2"), TextBox).Text & vbNewLine)

                ''Key Field
                bld.Append("DistributionChannel=" & CType(SaleOrder1.FindControl("txtDistributionChannel"), TextBox).Text & vbNewLine)
                bld.Append("Division=" & CType(SaleOrder1.FindControl("txtDivision"), TextBox).Text & vbNewLine)
                'bld.Append("SalesDocumentType=" & CType(SaleOrder1.FindControl("txtSalesDocumentType"), TextBox).Text & vbNewLine)
                bld.Append("SalesOrganization=" & CType(SaleOrder1.FindControl("txtSalesOrganization"), TextBox).Text & vbNewLine)
                'bld.Append("Source=" & CType(SaleOrder1.FindControl("txtSource"), TextBox).Text & vbNewLine)

                'Sasikumar WP SPLUS_WP014
                bld.Append("Language=" & CType(PartInquiry1.FindControl("txtLanguage"), TextBox).Text & vbNewLine)


                DisplayDataSent(bld)
                Dim Cert As X509Certificate
                ConfigurationData.WebmethodTest.Load(ConfigurationData.GeneralSettings.Environment)
                objSales.Credentials = getCredential()
                objSales.Url = ConfigurationData.WebmethodTest.SalesOrder
                Cert = X509Certificate.CreateFromCertFile(ConfigurationData.WebmethodTest.CertificatePath)
                'ServicePointManager.CertificatePolicy = New TrustAllCertificatePolicy()
                ServicePointManager.Expect100Continue = True
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12
                ServicePointManager.ServerCertificateValidationCallback = New RemoteCertificateValidationCallback(AddressOf ValidateRemoteCertificate)
                objSales.ClientCertificates.Add(Cert)

                'objsalesRequest1.DistributionChannel = CType(SaleOrder1.FindControl("txtDistributionChannel"), TextBox).Text
                'objsalesRequest1.Division = CType(SaleOrder1.FindControl("txtDivision"), TextBox).Text
                ''objsalesRequest1.SalesDocumentType = CType(SaleOrder1.FindControl("txtSalesDocumentType"), TextBox).Text
                'objsalesRequest1.SalesOrganization = CType(SaleOrder1.FindControl("txtSalesOrganization"), TextBox).Text
                'objsalesRequest1.Source = CType(SaleOrder1.FindControl("txtSource"), TextBox).Text
                ''objsalesRequest1.SalesOrganization = "1000"
                ''objsalesRequest1.DistributionChannel = "50"
                ''objsalesRequest1.Division = "50"
                ''objsalesRequest1.Source = "SvcPlus"
                ''Billing Information
                'objBillInformation.BillToName1 = CType(SaleOrder1.FindControl("txtBillToName1"), TextBox).Text
                ''objBillInformation.BillToName2 = CType(SaleOrder1.FindControl("txtBillToName2"), TextBox).Text
                ''objBillInformation.BillToName3 = CType(SaleOrder1.FindControl("txtBillToName3"), TextBox).Text
                'objBillInformation.City = CType(SaleOrder1.FindControl("txtCity"), TextBox).Text
                'objBillInformation.Country = CType(SaleOrder1.FindControl("txtCountry"), TextBox).Text
                'objBillInformation.District = CType(SaleOrder1.FindControl("txtDistrict"), TextBox).Text
                'objBillInformation.Email = CType(SaleOrder1.FindControl("txtEmail"), TextBox).Text

                ' ''Sold to Account Number
                'objBillInformation.SoldTo = CType(SaleOrder1.FindControl("txtSoldTo"), TextBox).Text
                'objBillInformation.State = CType(SaleOrder1.FindControl("txtState"), TextBox).Text
                'objBillInformation.Street = CType(SaleOrder1.FindControl("txtStreet"), TextBox).Text
                'objBillInformation.Street2 = CType(SaleOrder1.FindControl("txtStreet2"), TextBox).Text
                'objBillInformation.ZipCode = CType(SaleOrder1.FindControl("txtZipCode"), TextBox).Text

                'objOrderDetails.BillInformation = objBillInformation
                ''Order Details
                objOrderDetails.CardName = CType(SaleOrder1.FindControl("txtCardName"), TextBox).Text
                objOrderDetails.CardNumber = CType(SaleOrder1.FindControl("txtCardNumber"), TextBox).Text
                objOrderDetails.CardType = CType(SaleOrder1.FindControl("txtCardType"), TextBox).Text
                objOrderDetails.CardVerificationValue = CType(SaleOrder1.FindControl("txtCardVerificationValue"), TextBox).Text
                ''objOrderDetails.CreatedByCS3 = CType(SaleOrder1.FindControl("txtCreatedByCS3"), TextBox).Text
                ''objOrderDetails.CustomerPODate = CType(SaleOrder1.FindControl("txtCustomerPODate"), TextBox).Text
                'objOrderDetails.CustomerPONumber = CType(SaleOrder1.FindControl("txtCustomerPONumber"), TextBox).Text
                'objOrderDetails.CustomerPOType = CType(SaleOrder1.FindControl("txtCustomerPOType"), TextBox).Text
                ''objOrderDetails.CustomerPOType = "ZPAS"
                'objOrderDetails.GroupCode = CType(SaleOrder1.FindControl("txtGroupCode"), TextBox).Text
                ''objOrderDetails.HandlingCharge = CType(SaleOrder1.FindControl("txtHandlingCharge"), TextBox).Text
                ''bjOrderDetails.HeaderComments = CType(SaleOrder1.FindControl("txtHeaderComments"), TextBox).Text
                ''objOrderDetails.Input1 = CType(SaleOrder1.FindControl("txtInput1"), TextBox).Text
                ''objOrderDetails.Input2 = CType(SaleOrder1.FindControl("txtInput2"), TextBox).Text
                ''objOrderDetails.Input3 = CType(SaleOrder1.FindControl("txtInput3"), TextBox).Text
                ''objOrderDetails.Input4 = CType(SaleOrder1.FindControl("txtInput4"), TextBox).Text
                ''objOrderDetails.Input5 = CType(SaleOrder1.FindControl("txtInput5"), TextBox).Text
                ' ''Account Payer Account
                'objOrderDetails.Input4 = CType(SaleOrder1.FindControl("txtGroupCode"), TextBox).Text

                'objOrderDetails.OrderEntryPerson = CType(SaleOrder1.FindControl("txtOrderEntryPerson"), TextBox).Text
                'objOrderDetails.ReasonCode = CType(SaleOrder1.FindControl("txtReasonCode"), TextBox).Text

                ''objOrderDetails.ShippingCharge = CType(SaleOrder1.FindControl("txtShippingCharge"), TextBox).Text
                'objOrderDetails.TaxExempt = CType(SaleOrder1.FindControl("txtTaxExempt"), TextBox).Text
                'objOrderDetails.ValidTo = CType(SaleOrder1.FindControl("txtValidTo"), TextBox).Text
                'objOrderDetails.YourReference = CType(SaleOrder1.FindControl("txtYourReference"), TextBox).Text

                ' ''objlineDetails
                ''objlineDetails.BillingOnly = CType(SaleOrder1.FindControl("txtBillingOnly"), TextBox).Text
                'objlineDetails.Carrier = CType(SaleOrder1.FindControl("txtCarrierCode1"), TextBox).Text
                ''objlineDetails.ItemComments = "2" ''CType(SaleOrder1.FindControl("txtItemComments"), TextBox).Text
                ''objlineDetails.ItemText = "2" ''CType(SaleOrder1.FindControl("txtItemText"), TextBox).Text
                ''objlineDetails.LineDetail1 = "2" '' CType(SaleOrder1.FindControl("txtLineDetail1"), TextBox).Text
                ''objlineDetails.LineDetail2 = "2" '' CType(SaleOrder1.FindControl("txtLineDetail2"), TextBox).Text
                ''objlineDetails.LineDetail3 = "2" 'CType(SaleOrder1.FindControl("txtLineDetail3"), TextBox).Text
                ''objlineDetails.LineDetail4 = "2" ' CType(SaleOrder1.FindControl("txtLineDetail4"), TextBox).Text
                ''objlineDetails.LineDetail5 = "2" ' CType(SaleOrder1.FindControl("txtLineDetail5"), TextBox).Text
                ''objlineDetails.Model = "2" 'CType(SaleOrder1.FindControl("txtModel"), TextBox).Text
                'objlineDetails.OrderQuantity = CType(SaleOrder1.FindControl("txtOrderQuantity1"), TextBox).Text
                'objlineDetails.PartNumber = CType(SaleOrder1.FindControl("txtPartNumber1"), TextBox).Text
                ''objlineDetails.ProductCode = "3" 'CType(SaleOrder1.FindControl("txtProductCode"), TextBox).Text
                ''objlineDetails.Serial = "3" 'CType(SaleOrder1.FindControl("txtSerial"), TextBox).Text
                ''objlineDetails.ServiceTag = "2" 'CType(SaleOrder1.FindControl("txtServiceTag"), TextBox).Text
                ''objlineDetails.VendorNumber = "2" ' CType(SaleOrder1.FindControl("txtVendorNumber"), TextBox).Text
                'arrLinedetails(0) = objlineDetails

                'objLineDetails1.Carrier = CType(SaleOrder1.FindControl("txtCarrierCode2"), TextBox).Text
                'objLineDetails1.OrderQuantity = CType(SaleOrder1.FindControl("txtOrderQuantity2"), TextBox).Text
                'objLineDetails1.PartNumber = CType(SaleOrder1.FindControl("txtPartNumber2"), TextBox).Text
                'arrLinedetails(1) = objLineDetails1

                'objOrderDetails.LineDetails = arrLinedetails



                ''Shiping Information
                'objshipinformation.BusExt = CType(SaleOrder1.FindControl("txtBusExt"), TextBox).Text
                'objshipinformation.City = CType(SaleOrder1.FindControl("txtshipinfoCity"), TextBox).Text
                'objshipinformation.Country = CType(SaleOrder1.FindControl("txtshipinfoCountry"), TextBox).Text
                'objshipinformation.District = CType(SaleOrder1.FindControl("txtshipinfoDistrict"), TextBox).Text
                ''objshipinformation.Email = CType(SaleOrder1.FindControl("txtshipinofEmail"), TextBox).Text
                'objshipinformation.Fax = CType(SaleOrder1.FindControl("txtshipinfoFax"), TextBox).Text
                'objshipinformation.FirstTelephoneNumber = CType(SaleOrder1.FindControl("txtshipinfoFirstTelephoneNumber"), TextBox).Text
                'objshipinformation.ShipTo = CType(SaleOrder1.FindControl("txtshipinfoShipTo"), TextBox).Text
                'objshipinformation.ShipToName1 = CType(SaleOrder1.FindControl("txtshipinfoShipToName1"), TextBox).Text
                ''objshipinformation.ShipToName2 = CType(SaleOrder1.FindControl("txtshipinfoShipToName2"), TextBox).Text
                'objshipinformation.ShipToName3 = CType(SaleOrder1.FindControl("txtshipinfoShipToName3"), TextBox).Text
                'objshipinformation.State = CType(SaleOrder1.FindControl("txtshipinfoState"), TextBox).Text
                'objshipinformation.Street = CType(SaleOrder1.FindControl("txtshipinfoStreet"), TextBox).Text
                'objshipinformation.Street2 = CType(SaleOrder1.FindControl("txtshipinfoStreet2"), TextBox).Text
                'objshipinformation.ZipCode = CType(SaleOrder1.FindControl("txtshipinfoZipCode"), TextBox).Text
                'objOrderDetails.ShipInformation = objshipinformation
                'objOrderDetails.Input3 = CType(SaleOrder1.FindControl("txtOrderSimulation"), TextBox).Text
                objsalesRequest1.Source = "SvcPlus"
                objshipinformation.ShipTo = CType(SaleOrder1.FindControl("txtSoldTo"), TextBox).Text
                objshipinformation.ShipToName1 = CType(SaleOrder1.FindControl("txtshipinfoShipToName1"), TextBox).Text
                objshipinformation.ShipToName3 = CType(SaleOrder1.FindControl("txtshipinfoShipToName3"), TextBox).Text
                objshipinformation.Street = CType(SaleOrder1.FindControl("txtshipinfoStreet"), TextBox).Text
                objshipinformation.Street2 = CType(SaleOrder1.FindControl("txtStreet2"), TextBox).Text
                objshipinformation.District = CType(SaleOrder1.FindControl("txtshipinfoDistrict"), TextBox).Text
                objshipinformation.City = CType(SaleOrder1.FindControl("txtshipinfoCity"), TextBox).Text
                objshipinformation.State = CType(SaleOrder1.FindControl("txtshipinfoState"), TextBox).Text
                objshipinformation.Country = CType(SaleOrder1.FindControl("txtshipinfoCountry"), TextBox).Text
                objshipinformation.ZipCode = CType(SaleOrder1.FindControl("txtshipinfoZipCode"), TextBox).Text
                objshipinformation.FirstTelephoneNumber = CType(SaleOrder1.FindControl("txtshipinfoFirstTelephoneNumber"), TextBox).Text '"408-352-4269"
                objshipinformation.BusExt = CType(SaleOrder1.FindControl("txtBusExt"), TextBox).Text 'String.Empty
                objshipinformation.Fax = CType(SaleOrder1.FindControl("txtshipinfoFax"), TextBox).Text 'String.Empty
                objOrderDetails.ShipInformation = objshipinformation

                objBillInformation.SoldTo = CType(SaleOrder1.FindControl("txtSoldTo"), TextBox).Text
                objOrderDetails.BillInformation = objBillInformation

                objOrderDetails.CustomerPONumber = "NO-PO-REQ"
                objOrderDetails.CustomerPOType = "ZSVC"
                objOrderDetails.YourReference = "WJqgVcJIIgaC"
                objOrderDetails.OrderEntryPerson = "0444300300008016"
                objOrderDetails.ReasonCode = "Y"
                objOrderDetails.TaxExempt = CType(SaleOrder1.FindControl("txtTaxExempt"), TextBox).Text 'String.Empty
                objOrderDetails.Input1 = "S1"
                objOrderDetails.Input3 = "X"
                objOrderDetails.Input4 = CType(SaleOrder1.FindControl("txtSoldTo"), TextBox).Text

                objlineDetails.PartNumber = CType(SaleOrder1.FindControl("txtPartNumber1"), TextBox).Text
                objlineDetails.OrderQuantity = CType(SaleOrder1.FindControl("txtOrderQuantity1"), TextBox).Text '"1"
                arrLinedetails(0) = objlineDetails

                objOrderDetails.LineDetails = arrLinedetails

                'Sasikumar WP SPLUS_WP014
                objsalesRequest1.SalesOrganization = CType(PartInquiry1.FindControl("txtSalesOrganization"), TextBox).Text.Trim()
                objsalesRequest1.DistributionChannel = CType(PartInquiry1.FindControl("txtDistributionChannel"), TextBox).Text.Trim()
                objsalesRequest1.Division = CType(PartInquiry1.FindControl("txtDivision"), TextBox).Text.Trim()
                objsalesRequest1.Language = CType(PartInquiry1.FindControl("txtLanguage"), TextBox).Text.Trim()


                objsalesRequest1.OrderDetails = objOrderDetails
                Dim objSap As New SAPHelper

                salesOderRequest.SalesOrderRequest1 = objsalesRequest1
                Dim temStr As String = objSap.SerialiseObject(salesOderRequest)
                salesOderResponse = objSales.receiveSalesOrderNotification(salesOderRequest)

                bld.Append("MessageText= " & salesOderResponse.SalesOrderResponse1.MessageText & vbNewLine)
                bld.Append("Messagetype= " & salesOderResponse.SalesOrderResponse1.Messagetype & vbNewLine)

                If salesOderResponse.SalesOrderResponse1.Messagetype = "S" And Not salesOderResponse.SalesOrderResponse1.OrderConfirmation Is Nothing Then

                    bld.Append(pcSeparator)
                    bld.Append(vbNewLine)
                    Dim objLinconformation = New OrderInjection.LineConfirmation()
                    bld.Append("OrderStatus=" & salesOderResponse.SalesOrderResponse1.OrderConfirmation.OrderStatus & vbNewLine)
                    ''bld.Append("OrderStatus=" & salesOderResponse.SalesOrderResponse1.OrderConfirmation.LineConfirmation )
                    bld.Append("OutPut1= " & salesOderResponse.SalesOrderResponse1.OrderConfirmation.OutPut1 & vbNewLine)
                    bld.Append("OutPut2= " & salesOderResponse.SalesOrderResponse1.OrderConfirmation.OutPut2 & vbNewLine)
                    bld.Append("OutPut3= " & salesOderResponse.SalesOrderResponse1.OrderConfirmation.OutPut3 & vbNewLine)
                    bld.Append("OutPut4= " & salesOderResponse.SalesOrderResponse1.OrderConfirmation.OutPut4 & vbNewLine)
                    bld.Append("OutPut5= " & salesOderResponse.SalesOrderResponse1.OrderConfirmation.OutPut5 & vbNewLine)
                    bld.Append("PaymentCardAuthorizationNumber= " & salesOderResponse.SalesOrderResponse1.OrderConfirmation.PaymentCardAuthorizationNumber & vbNewLine)
                    bld.Append("PaymentCardResultText= " & salesOderResponse.SalesOrderResponse1.OrderConfirmation.PaymentCardResultText & vbNewLine)
                    bld.Append("SalesDocument= " & salesOderResponse.SalesOrderResponse1.OrderConfirmation.SalesDocument & vbNewLine)
                    bld.Append("TotalBackedOrderQty= " & salesOderResponse.SalesOrderResponse1.OrderConfirmation.TotalBackedOrderQty & vbNewLine)
                    bld.Append("TotalConfirmedQty= " & salesOderResponse.SalesOrderResponse1.OrderConfirmation.TotalConfirmedQty & vbNewLine)
                    bld.Append("TotalNetValue= " & salesOderResponse.SalesOrderResponse1.OrderConfirmation.TotalNetValue & vbNewLine)
                    bld.Append("TotalOrderAmount= " & salesOderResponse.SalesOrderResponse1.OrderConfirmation.TotalOrderAmount & vbNewLine)
                    bld.Append("TotalOrderedQty= " & salesOderResponse.SalesOrderResponse1.OrderConfirmation.TotalOrderedQty & vbNewLine)
                    bld.Append("TotalPartSubTotal= " & salesOderResponse.SalesOrderResponse1.OrderConfirmation.TotalPartSubTotal & vbNewLine)
                    bld.Append("TotalRecyclingFee= " & salesOderResponse.SalesOrderResponse1.OrderConfirmation.TotalRecyclingFee & vbNewLine)
                    bld.Append("TotalShippingHandling= " & salesOderResponse.SalesOrderResponse1.OrderConfirmation.TotalShippingHandling & vbNewLine)
                    bld.Append("TotalTaxAmount= " & salesOderResponse.SalesOrderResponse1.OrderConfirmation.TotalTaxAmount & vbNewLine)

                    If Not salesOderResponse.SalesOrderResponse1.OrderConfirmation.LineConfirmation Is Nothing Then
                        For Each objLinconformation In salesOderResponse.SalesOrderResponse1.OrderConfirmation.LineConfirmation
                            bld.Append("-----------------Lineconfirmation-----------------" & vbNewLine)
                            bld.Append("BackOrderQty= " & objLinconformation.BackOrderQty & vbNewLine)
                            bld.Append("ConfirmedQty= " & objLinconformation.ConfirmedQty & vbNewLine)
                            bld.Append("LineConfirmation1= " & objLinconformation.LineConfirmation1 & vbNewLine)
                            bld.Append("LineConfirmation2= " & objLinconformation.LineConfirmation2 & vbNewLine)
                            bld.Append("LineConfirmation3= " & objLinconformation.LineConfirmation3 & vbNewLine)
                            bld.Append("LineConfirmation4= " & objLinconformation.LineConfirmation4 & vbNewLine)
                            bld.Append("LineConfirmation5= " & objLinconformation.LineConfirmation5 & vbNewLine)
                            bld.Append("LineNumber= " & objLinconformation.LineNumber & vbNewLine)
                            bld.Append("LineStatusItem= " & objLinconformation.LineStatusItem & vbNewLine)
                            bld.Append("OrderQty= " & objLinconformation.OrderQty & vbNewLine)
                            bld.Append("PartOrdered= " & objLinconformation.PartOrdered & vbNewLine)
                            bld.Append("PartPrice= " & objLinconformation.PartPrice & vbNewLine)
                            bld.Append("PartShipped= " & objLinconformation.PartShipped & vbNewLine)
                            bld.Append("RecyclingFee= " & objLinconformation.RecyclingFee & vbNewLine)
                            bld.Append("ShortTextItem= " & objLinconformation.ShortTextItem & vbNewLine)
                            bld.Append(vbNewLine)
                        Next
                    End If

                End If
                DisplayElapsedTime()
            Catch ex As Exception
                bld.Append("Exception= " & ex.Message)
                objPCILogger.IndicationSuccessFailure = "Failure" '6524
                objPCILogger.Message = "SalesOrder Failed. " & ex.Message.ToString() '6524
            Finally
                objPCILogger.PushLogToMSMQ() '6524
            End Try
            Me.txtDataReturned.Text = bld.ToString()
        End Sub

        Public Function getCredential() As System.Net.ICredentials
            strPwd = Encryption.DeCrypt(ConfigurationData.WebmethodTest.Password, Encryption.PWDKey)
            strUname = ConfigurationData.WebmethodTest.UserName
            Dim objNetwork As System.Net.ICredentials
            System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 ' SecurityProtocolType.Ssl3
            objNetwork = New NetworkCredential(strUname, strPwd)
            Return objNetwork
        End Function

        Private Sub DisplayDataSent(ByVal bld As System.Text.StringBuilder)
            Me.lblDataReturned.Text = "Data Returned"
            Me.txtDataReturned.Text = ""
            lblDataSent.Text = bld.ToString()
            bld.Remove(0, Len(bld.ToString()))
            StartTime = DateAndTime.Now
        End Sub

        Private Sub DisplayElapsedTime()
            EndTime = DateAndTime.Now
            TimeSpan = EndTime.Subtract(StartTime)
            Me.lblDataReturned.Text = "Data Returned in " & TimeSpan.TotalSeconds.ToString() & " seconds"
        End Sub

        'Private Function ValidateFields() As Boolean
        '    Select Case Me.cmbInterface.Text
        '        Case pcAccountInquiry
        '            If Me.txtSAPPayerAccountNumber.Text = "" Then
        '                lblError.Text = "Please enter SAP Payer Account Number."
        '                Me.txtSAPPayerAccountNumber.Focus()
        '                Exit Function
        '            End If
        '            If txtLegacyAccountNumber.Text = "" Then
        '                lblError.Text = "Please enter Legacy Account Number."
        '                Me.txtLegacyAccountNumber.Focus()
        '                Exit Function
        '            End If
        '            If txtCompanyName.Text = "" Then
        '                lblError.Text = "Please enter Company Name."
        '                Me.txtCompanyName.Focus()
        '                Exit Function
        '            End If
        '        Case pcCancelOrder
        '            If Me.txtOrderNumber.Text = "" Then
        '                lblError.Text = "Please enter Order Number."
        '                Me.txtOrderNumber.Focus()
        '                Exit Function
        '            End If
        '            If Me.txtReasonforRejection.Text = "" Then
        '                lblError.Text = "Please enter Reason for Rejection."
        '                Me.txtReasonforRejection.Focus()
        '                Exit Function
        '            End If
        '        Case pcOrderInquiry
        '            If Me.txtOrderNumber.Text = "" Then
        '                lblError.Text = "Please enter Order Number."
        '                Me.txtOrderNumber.Focus()
        '                Exit Function
        '            End If
        '            If Me.txtPOSender.Text = "" Then
        '                lblError.Text = "Please enter Sender."
        '                Me.txtPOSender.Focus()
        '                Exit Function
        '            End If
        '        Case pcPartInquiry
        '            If Me.txtSAPPayerAccountNumber.Text = "" Then
        '                lblError.Text = "Please enter SAP Payer Account Number."
        '                Me.txtSAPPayerAccountNumber.Focus()
        '                Exit Function
        '            End If
        '            If Me.txtPartNumbers.Text = "" Then
        '                lblError.Text = "Please enter Part Number."
        '                Me.txtPartNumbers.Focus()
        '                Exit Function
        '            End If
        '            If Me.txtLocation.Text = "" Then
        '                lblError.Text = "Please enter Location."
        '                Me.txtLocation.Focus()
        '                Exit Function
        '            End If
        '            If Me.txtPriceInfo.Text = "" Then
        '                lblError.Text = "Please enter Price Info."
        '                Me.txtPriceInfo.Focus()
        '                Exit Function
        '            End If
        '            If Me.txtInventoryInfo.Text = "" Then
        '                lblError.Text = "Please enter Inventory Info."
        '                Me.txtInventoryInfo.Focus()
        '                Exit Function
        '            End If
        '        Case Else
        '            lblError.Text = "The Web Method '" & Me.cmbInterface.Text & "' is not ready to test."
        '            Me.cmbInterface.Focus()
        '            Exit Function
        '    End Select

        '    Return True
        'End Function


#End Region

        Protected Sub lnkShowHideDataSent_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkShowHideDataSent.Click
            If tdDataSent.Visible Then
                lnkShowHideDataSent.Text = "Show Data Sent"
            Else
                lnkShowHideDataSent.Text = "Hide Data Sent"
            End If
            tdDataSent.Visible = Not tdDataSent.Visible
        End Sub

        'Protected Sub ddlEnviroment_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlEnviroment.Load

        'End Sub

        'Protected Sub ddlEnviroment_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlEnviroment.SelectedIndexChanged
        '    Try
        '        If cmbInterface.Text = "Part Inquiry" Then
        '            ConfigurationData.WebmethodTest.Load(ddlEnviroment.Text)
        '            CType(PartInquiry1.FindControl("txtSAPAccountNumber"), TextBox).Text = ConfigurationData.WebmethodTest.DefaultAccount
        '            ''Commeneted as per bug 6322
        '            CType(PartInquiry1.FindControl("txtLocation"), TextBox).Text = ConfigurationData.WebmethodTest.PartLocation
        '        End If
        '    Catch ex As Exception
        '        ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
        '    End Try
        'End Sub

        Private Shared Function ValidateRemoteCertificate(ByVal sender As Object, ByVal certificate As X509Certificate, ByVal chain As X509Chain, ByVal policyErrors As SslPolicyErrors) As Boolean
            Return True
        End Function
    End Class
    Public Class TrustAllCertificatePolicy
        Implements System.Net.ICertificatePolicy

        Public Function CheckValidationResult(ByVal sp As ServicePoint, ByVal cert As X509Certificate, ByVal req As WebRequest, ByVal problem As Integer) As Boolean Implements System.Net.ICertificatePolicy.CheckValidationResult
            Return True
        End Function
    End Class

End Namespace
