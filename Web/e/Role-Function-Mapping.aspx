<%@ Reference Page="~/e/Utility.aspx" %>
<%@ Page Language="vb" AutoEventWireup="false" smartnavigation=true Inherits="SIAMAdmin.Role_Function_Mapping" CodeFile="Role-Function-Mapping.aspx.vb" CodeFileBaseClass="SIAMAdmin.UtilityClass" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Manage Role Functionality Mapping</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="includes/style.css" type="text/css" rel="stylesheet">
		<script language=javascript>
		function add(obj1,obj2)
            {
                var flag=0;
                if(obj1.options.length === 0)
                    ;
                else
                {
                    var k=0;
                    var len_obj2 = obj2.length;
                    while(k<obj1.options.length)
                    {
                        if(obj1.options[k].selected)
                        {
                            var arr = new Option();
                            arr.value=obj1.options[k].value;
                            arr.text=obj1.options[k].text;
                            var repeat_name;
                            for(i=0;i<obj2.options.length;i++)
                                if(obj2[i].value === arr.value)
                                    {
                                        repeat_name=true;
                                        break;
                                    }
                            if (repeat_name === false)
                                obj2.options[obj2.options.length]=arr;
                            }
                        k++;
                        }
                 }
            }



            function remove(obj1,obj2)
            {
            var str=document.form1;
            var flag=0;

            
            while(flag<str.elements.length)
            {
            if(str.elements[flag].name==hiddenvar)
            {
            hidden_name=str.elements[flag];
            break;
            }
            flag++;
            }

            if(obj2.options.length === 0)
            ;
            else
            {
            while(obj2.selectedIndex != -1)
            {
            for(var i=(obj2.options.length -1);i >= 0;i--)
            if(obj2.options[i].selected === true)
            {
            obj2.options[i]= null;
            }
            }
            }


        }
		
		</script>
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<table width="100%" align="center" border="0">
				<tr>
					<td class="PageHead" align="center"  height="30">
					    <asp:label id="lblHeader" runat="server">Role Functionality Mapping</asp:label>
					</td>
				</tr>
				<tr>
					<td height="5">&nbsp;
						<asp:label id="ErrorLabel" runat="server" CssClass="body" ForeColor="Red" EnableViewState="False"></asp:label>
					</td>
				</tr>
				<tr>
					<td align="center">
						<table width="500" border="0">
							<tr>
								<td>
									<table width="100%" align="center" bgColor="#5d7180" border="0">
										<tr height="1">
											<td bgColor="#000000" colSpan="9"></td>
										</tr>
										<tr>
											<td align="center">
												<table border="0">
													<tr height="25">
														<%--<td class=Nav3>New</td>--%>
														<td class="Nav3" align="center" width="5">&nbsp;</td><%--|&nbsp;</td>
														<td><A class=Nav3 href="#" >Search</A></td>--%>
																								
													</tr>
												</table>
											</td>
										</tr>
										<tr height="1">
											<td bgColor="#000000" colSpan="9"></td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
                <tr>
					<td>
						<table border=0 width =100% align="center">
							<%--<tr>
								<td align="center">&nbsp;</td>
							</tr>--%>
							 <tr>
								<td align="left"> &nbsp; &nbsp; &nbsp; 
							   <asp:label id="lblMessage" runat="server" CssClass="body" ForeColor="Red" EnableViewState="False"></asp:label>
							    </td>
							</tr>
							<%--<tr>
								<td align="center">&nbsp;</td>
							</tr>--%>
							<tr>
							    <td align="center" >
							        <table width=100% border="0">
						            <tr>
								        <td >
									    <table align=center width=100% border="0">
                                            <%--<tr>
                                                <td align="right" width="30%">
											        <asp:label id="lblMappingIDText" CssClass="Body" Runat="server">Mapping ID:</asp:label>
											    </td>
                                                <td colspan="1">
											        <asp:label id="lblMappingID" CssClass="Body" Runat="server"></asp:label>
                                                </td>
                                            </tr>--%>
                                            <tr><td colspan=2></td></tr>
                                            <tr><td colspan=2></td></tr>
										    <tr>
											    <td align="right" width=30%>
											        <asp:label id="lblRole" CssClass="Body" Runat="server">Role:</asp:label></td>
											    <td colspan="1">
                                                    <asp:DropDownList ID="ddlRole" CssClass="Body" runat="server" Width=90%>
                                                    </asp:DropDownList></td>
										    </tr>
                                            <tr><td colspan=2></td></tr>
                                            <tr><td colspan=2></td></tr>
										    
										    <tr>
											    <td align="right" valign=top  width=30%><br/><asp:label id="lblFunctionality" runat="server" CssClass="Body">Functionality:</asp:label></td>
                                                <td>
                                                    <table width=90% border="0" height="160">
                                                        <tr valign=top>
                                                            <td width=48%><asp:label id="Label1" runat="server" CssClass="Body">Available Functionality:</asp:label><br />
                                                                <asp:ListBox ID="lstAvailable" CssClass="Body" Width=98% Height=98% SelectionMode="Multiple" runat="server"></asp:ListBox>
                                                            </td>
											                <td valign=middle width=4%>
                                                                &nbsp;<asp:Button ID="btnSelect" runat="server" CssClass="Body" Text=">>" />&nbsp;
                                                                <br /><br />&nbsp;<asp:Button ID="btnDeSelect" runat="server" CssClass="Body" Text="<<" />&nbsp;
                                                            </td>
                                                            <td  width=48%><asp:label id="Label2" runat="server" CssClass="Body">Selected Functionality:</asp:label><br />
                                                                <asp:ListBox ID="lstSelected" CssClass="Body" Width=98% Height=98% runat="server" SelectionMode="Multiple"></asp:ListBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                 </tr>
                                            <tr><td colspan=2>&nbsp;</td></tr>
               
 									    </table>
								    </td>
							        </tr>
							        </table>
							    </td>
							</tr>
							<tr>
								
								<td align="center" >
									<table border="0" width=100%>
											<tr> 
											    <td align=center style="height: 21px">
											        <asp:button id="btnSave" runat="server" CssClass="Body" Text="Save"></asp:button>
									            <asp:button id="btnCancel" runat="server" CssClass="Body" Text="Cancel"></asp:button></td>
										</tr>
									</table>
								</td>
								
							</tr>
							<tr>
								<td align="left" style="height: 12px" ><asp:label id="lblFoundResut" runat="server" CssClass="body" Visible="False"></asp:label></td>
							</tr>
							<tr>
								<td align=center>
							        <asp:datagrid id="dgRoleFunctionalityMapping" runat="server">
									    <Columns>
										    <asp:BoundColumn DataField="username" SortExpression="username" HeaderText="User ID"></asp:BoundColumn>
										    <asp:TemplateColumn SortExpression="Name" HeaderText="Name">
											    <ItemTemplate>
												    <asp:HyperLink runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Name") %>' NavigateUrl='<%# thisPage + "?Identity="+DataBinder.Eval(Container.DataItem, "Identity").ToString()%>' ID="Hyperlink1">
												    </asp:HyperLink><%--7086--%>
											    </ItemTemplate>
										    </asp:TemplateColumn>
										    <asp:BoundColumn DataField="EmailAddress" SortExpression="EmailAddress" HeaderText="Email Address"></asp:BoundColumn>
										    <asp:BoundColumn DataField="Company" SortExpression="Company" HeaderText="Company"></asp:BoundColumn>
										    <asp:TemplateColumn>
											    <ItemTemplate>
												    <asp:HyperLink runat="server" Text="ServicesPlus" NavigateUrl='<%# thisPage + "?Identity="+DataBinder.Eval(Container.DataItem, "Identity").ToString()%>' ID="Hyperlink2">
												    </asp:HyperLink>
											    </ItemTemplate>
										    </asp:TemplateColumn>
										    <asp:TemplateColumn>
											    <ItemTemplate>
												    <asp:LinkButton runat="server" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "Identity")%>' CommandName="Delete" Text="Delete" ID="lnkBtnDelete">
												    </asp:LinkButton>
											    </ItemTemplate>
										    </asp:TemplateColumn>
									    </Columns>
								    </asp:datagrid>
								</td>
							</tr>
							
						</table>
					</td>
				</tr>
			</table>
			<input type=hidden id="hdnSelected" runat=server />
		</form>
	</body>
</HTML>
