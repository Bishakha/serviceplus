Imports Sony.US.siam, Sony.US.ServicesPLUS.Controls
Imports ServicesPlusException
Imports Sony.US.ServicesPLUS.Process '7086


Namespace SIAMAdmin

Partial Class TestAuthentication
        Inherits UtilityClass
        Private LDapID As String '7086
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

        'Dim objUtilties As Utilities = New Utilities
    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
            MyBase.IsProtectedPage(True, True)
            InitializeComponent()
    End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                'Dim strIdentity = Request.QueryString("identity")'7086
                '7086 Starts
                Dim cm As New CustomerManager '7086
                LDapID = Request.QueryString("LdapId")
                Dim strIdentity = cm.GetSiamIdByLdapId(LDapID)
                '7086 Ends
                Dim fur As FetchUserResponse = New Sony.US.siam.SecurityAdministrator().GetUser(strIdentity)
                'If fur.TheUser.UserType = "AccountHolder" Then'6668
                If fur.TheUser.UserType = "A" Or fur.TheUser.UserType = "P" Then '6668
                    hrefSAPAccount.Visible = True
                    'hrefSAPAccount.HRef = "ViewSAPAccounts.aspx?Identity=" + strIdentity'7086
                    hrefSAPAccount.HRef = "ViewSAPAccounts.aspx?LdapId=" + LDapID '7086
                Else
                    hrefSAPAccount.Visible = False
                End If
                If Not fur.SiamResponseMessage = Messages.GET_USER_COMPLETED.ToString() Then
                    Session.Remove("LastError")
                    Session.Add("LastError", fur.SiamResponseMessage)
                    Response.Redirect("ErrorPage.aspx")
                Else
                    Dim c As New Credential
                    c.UserName = fur.TheUser.UserId
                    c.Password = fur.TheUser.Password

                    Dim ar As AuthenticationResult = New Sony.US.siam.SecurityManager().Authenticate(c)
                    Label1.Text = ar.SiamResponseMessage
                End If
            Catch ex As Exception
                Label1.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Public ReadOnly Property getIdentity() As String
            Get
                Return Request.QueryString("Identity")
            End Get
        End Property
        '7086 Starts
        Public ReadOnly Property getLDapID_prop() As String
            Get
                Return Request.QueryString("LdapId")
            End Get
        End Property
        '7086 Ends
    End Class
    
End Namespace
