Imports ServicesPlusException
Imports Sony.US.AuditLog
Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.siam
Imports Sony.US.SIAMUtilities

Namespace SIAMAdmin
    Partial Class _default
        Inherits UtilityClass

#Region " Web Form Designer Generated Code "
        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            IsProtectedPage(False)
            InitializeComponent()
        End Sub
#End Region

#Region "Private variable"
        Private idpUserID As String = String.Empty
        Private idpEmailAddress As String = String.Empty
        Private emailBody As String = String.Empty
        Private emailSubject As String = "ServicesPlus Admin Login Failed - "
        'Private strDefaultFrameLocation As String = "Default.aspx"

        Protected WithEvents linkResources As HtmlAnchor
        Protected WithEvents lblUser As Label
        Protected WithEvents lblDateTime As Label
        Protected WithEvents ControlPlaceHolder As PlaceHolder
#End Region

#Region "   Events  "
        Private Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
            ' IDP (via Kerberos) will provide the User ID in the HTTP header after a successful login.
            If Not IsPostBack Then
                If (ConfigurationData.GeneralSettings.AdminLocalAccess.AuthenticateAdminLocalFlag.ToString() = "Y") Then
                    ' This Config flag is a bypass to allow all logins. Be careful.
                    idpUserID = ConfigurationData.GeneralSettings.AdminLocalAccess.AuthenticateAdminLocalGlobalID
                    processLoggin()
                Else
                    '2016-04-29 ASleight - Modified login variable names for Shibboleth, the SiteMinder replacement.
                    If (HttpContext.Current.Request.ServerVariables("HTTP_IDP_UID") IsNot Nothing) Then
                        idpUserID = HttpContext.Current.Request.ServerVariables("HTTP_IDP_UID")
                        processLoggin()
                    Else
                        If ConfigurationData.GeneralSettings.Environment <> "Development" Then
                            Page.ClientScript.RegisterStartupScript(Me.GetType(), "LoadFrame", "<script language=""javascript"">window.location='" + Application("SIAMLogoutURL").ToString() + "';</script>")
                        Else
                            idpUserID = "50120C2174"
                            processLoggin()
                        End If
                    End If
                End If
            End If
        End Sub
#End Region

#Region "   Methods "
        Private Sub processLoggin()
            Dim objPCILogger As New PCILogger()
            Dim appSupportEmail = ConfigurationData.GeneralSettings.Email.Admin.SIAMAdminMailId ' ServicesPlus@am.sony.com
            Try
                Dim objFetchUserResponse As FetchUserResponse = New SecurityAdministrator().GetUserWithEmail(idpUserID)  ' 2019-06-21 "GetUserWithEmail" was altered to remove an "Email" parameter it would search by. This makes it a confusing function name.
                objPCILogger.EventOriginApplication = "ServicesPLUS Admin"
                objPCILogger.EventOriginApplicationLocation = Page.GetType().Name
                objPCILogger.EventOriginMethod = "processLoggin"
                objPCILogger.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
                objPCILogger.OperationalUser = Environment.UserName
                objPCILogger.EventDateTime = DateTime.Now.ToLongTimeString()
                objPCILogger.EventType = EventType.Login
                objPCILogger.HTTPRequestObjectValues = GetRequestContextValue()
                objPCILogger.CustomerID = idpUserID

                If objFetchUserResponse.TheUser IsNot Nothing Then
                    idpEmailAddress = objFetchUserResponse.TheUser.EmailAddress  ' 2019-03-26 ASleight - Fix to set email address and prevent SendMail errors.
                Else
                    emailBody = $"Your Global ID {idpUserID} is not set up in the ServicesPLUS Admin application. Please send an email to {appSupportEmail} to request access to this application."
                    emailSubject &= "Not a ServicesPLUS Admin User"
                    objPCILogger.IndicationSuccessFailure = "Failure"
                    'SendMail() ' 2019-03-26 ASleight - User not found, so we don't know their email address.
                    Session("NotLoggedInMessage") = $"Global ID {idpUserID} was not found to be a user of of the ServicesPLUS Administration Application. Please contact Application Support at <a href='mailto:{appSupportEmail}' class='Body'>{appSupportEmail}</a> for assistance."
                    Response.Redirect("NotLoggedIn.aspx", True)
                    Exit Sub
                End If


                If ((objFetchUserResponse.TheUser.UserStatusCode = 2) And objFetchUserResponse.TheUser.UserType = "I") Then ' Code = 2 = Disabled user
                    Session("NotLoggedInMessage") = "Global ID " & idpUserID & " user access to the ServicesPLUS Admin application has been set as inactive. Please contact the <a href='mailto:ApplicationSupportCenter@am.sony.com' class='Body'>Application Support Center</a> for assistance."
                    Response.Redirect("NotLoggedIn.aspx", True)
                    Exit Sub
                End If
                Session.Add(CurrentUserSessionVariables, objFetchUserResponse.TheUser)
                If SiamSideNav_Menu1.loadMenu(objFetchUserResponse.TheUser.Functionalities) = False Then
                    emailBody = $"Your Global ID {idpUserID} does not have a role assigned within the ServicesPLUS Admin application. Please send an email to {appSupportEmail} to request a role."
                    emailSubject &= "Role not available"
                    objPCILogger.IndicationSuccessFailure = "Failure"
                    SendMail()
                    Session("NotLoggedInMessage") = "Global ID " & idpUserID & " has no role assigned within the ServicesPLUS Admin application. Please contact the <a href='mailto:ApplicationSupportCenter@am.sony.com' class='Body'>Application Support Center</a> for assistance."
                    Response.Redirect("NotLoggedIn.aspx?e=" + Encryption.Encrypt(idpUserID + idpEmailAddress), True)
                    Exit Sub
                Else
                    Dim customer As New Customer With {
                        .CustomerID = objFetchUserResponse.TheUser.CustomerID,
                        .FirstName = objFetchUserResponse.TheUser.FirstName,
                        .LastName = objFetchUserResponse.TheUser.LastName,
                        .EmailAddress = objFetchUserResponse.TheUser.EmailAddress,
                        .SIAMIdentity = objFetchUserResponse.TheUser.Identity
                    }
                    Session("siamuser") = objFetchUserResponse.TheUser
                    Session("SiamID") = customer.SIAMIdentity
                    Session("UserStatus") = objFetchUserResponse.TheUser.UserType
                    ' 2019-06-21 ASleight Fixing sloppy code that does excessive casting.
                    objPCILogger.EmailAddress = customer.EmailAddress
                    objPCILogger.CustomerID = customer.CustomerID
                    objPCILogger.CustomerID_SequenceNumber = objFetchUserResponse.TheUser.SequenceNumber
                    objPCILogger.SIAM_ID = customer.SIAMIdentity
                    objPCILogger.LDAP_ID = objFetchUserResponse.TheUser.AlternateUserId
                    objPCILogger.Message = $"Login Successful for SIAM Identity {customer.SIAMIdentity}"

                    CType(Page.FindControl("SiamAdmin_Header").FindControl("lblUser"), Label).Text = $"Welcome: {objFetchUserResponse.TheUser.FirstName} {objFetchUserResponse.TheUser.LastName}"

                    If ConfigurationData.GeneralSettings.UnderMaintenance = "Y" Then
                        Page.ClientScript.RegisterStartupScript(Me.GetType(), "LoadFrame", "<script language=""javascript"">LoadContentFrame('UnderMaintenanceMessage.htm');</script>")
                    Else
                        Page.ClientScript.RegisterStartupScript(Me.GetType(), "LoadFrame", "<script language=""javascript"">LoadContentFrame('" + SM_DefaultPage + "');</script>")
                    End If
                    objPCILogger.IndicationSuccessFailure = "Success"
                End If
            Catch thrdex As Threading.ThreadAbortException
                'Do Nothing
            Catch ex As Exception
                objPCILogger.Message = ex.Message.ToString()
                objPCILogger.IndicationSuccessFailure = "Failure"
                objPCILogger.PushLogToMSMQ()
                Session("NotLoggedInMessage") = Utilities.WrapExceptionforUI(ex)
                Response.Redirect("NotLoggedIn.aspx", False)
            End Try
        End Sub

        Private Sub SendMail()
            'Dim config As SIAMConfig = CType(CachingServices.Cache("SiamConfiguration"), SIAMConfig)
            Dim sFromMailID As String = ConfigurationData.GeneralSettings.Email.Admin.SIAMAdminMailId
            Dim oMailer As New Mailer()
            oMailer.SendMail(emailBody, idpEmailAddress, sFromMailID, emailSubject, True, ConfigurationData.Environment.SMTP.EmailServer)
        End Sub

#End Region

        Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
            If Session("NewLogIn") IsNot Nothing Then
                hdnSecrecyBanner.Value = 1
            Else
                hdnSecrecyBanner.Value = 0
                Session("NewLogIn") = 0
            End If

        End Sub
    End Class

End Namespace
