<%@ Reference Page="~/e/Utility.aspx" %>
<%@ Page Language="vb" AutoEventWireup="false" SmartNavigation="True" Inherits="SIAMAdmin.training_class_edit" CodeFile="training-class-edit.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>training_class_edit</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="includes/style.css" type="text/css" rel="stylesheet">
		<script language="javascript" src="includes/SiamAdmin.js" type="text/javascript"></script>
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table3" align="center" border="0" width=100%>
				<TR>
					<TD class="PageHead" align="center" colSpan="1" height="25" rowSpan="1">Training Class<br /></TD>
				</TR>
				<%--<TR>
					<TD>&nbsp;</TD>
				</TR>--%>
				<TR>
					<TD class="PageHead" style="HEIGHT: 1px" align="left"><asp:label id="ErrorLabel" runat="server" EnableViewState="False" ForeColor="Red" CssClass="Body"></asp:label><br /><br /></TD>
				</TR>
				<%--<TR>
					<TD>&nbsp;</TD>
				</TR>
				<TR>
					<TD style="HEIGHT: 12px"><asp:label id="Label23" runat="server" Font-Size="Smaller" CssClass="Body" Font-Bold="True"> Class Information:</asp:label></TD>
				</TR>--%>
				<TR>
					<TD style="HEIGHT: 167px" vAlign="top" align="left">
						<TABLE id="Table2" width=100% cellPadding="0"
							 border="0">
							<TR>
								<TD style="WIDTH: 65px;" align="right" colSpan="1" rowSpan="1"><asp:label id="labelClassStatus" runat="server" CssClass="Body"> Class Status:</asp:label></TD>
								<TD><asp:RadioButtonList ID="rdoStatus" Enabled="false" AutoPostBack=true  CssClass="Body" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem Value="1" Selected="True">Active</asp:ListItem>
                                            <asp:ListItem Value="2">Hidden</asp:ListItem>
                                            <asp:ListItem Value="3">Deleted</asp:ListItem>
                                        </asp:RadioButtonList></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 65px; HEIGHT: 7px" align="right" colSpan="1" rowSpan="1"><asp:label id="labelCourseNumber" runat="server" CssClass="Body"> Course#:</asp:label></TD>
								<TD style="HEIGHT: 7px"><asp:dropdownlist id="ddlTrainingCourse" runat="server" CssClass="Body"></asp:dropdownlist>
									<asp:label id="LabelCourseNumberStar" runat="server" CssClass="redAsterick">
										<span class="redAsterick">*</span></asp:label></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 65px; HEIGHT: 26px" align="right"><asp:label id="labelStartDate" runat="server" CssClass="Body">Start Date:</asp:label></TD>
								<TD style="HEIGHT: 26px"><SPS:SPSTextBox id="txtStartDate" runat="server" CssClass="Body" Width="128px"></SPS:SPSTextBox><span class="Body">(MM/DD/YYYY)</span></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 65px; HEIGHT: 24px" align="right"><asp:label id="labelEndDate" runat="server" CssClass="Body">End Date:</asp:label></TD>
								<TD style="HEIGHT: 26px"><SPS:SPSTextBox id="txtEndDate" runat="server" CssClass="Body" Width="128px"></SPS:SPSTextBox><span class="Body">(MM/DD/YYYY)</span></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 65px; HEIGHT: 19px" align="right"><asp:label id="Label7" runat="server" CssClass="Body">Location :</asp:label></TD>
								<TD style="HEIGHT: 19px"><asp:dropdownlist id="ddlLocation" runat="server" CssClass="Body"></asp:dropdownlist>
									<asp:label id="LabelLocationStar" runat="server" CssClass="redAsterick">
										<span class="redAsterick">*</span></asp:label></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 65px; HEIGHT: 20px" align="right"><asp:label id="labelCapacity" runat="server" CssClass="Body"> Capacity:</asp:label></TD>
								<TD style="HEIGHT: 20px"><SPS:SPSTextBox id="txtCapacity" runat="server" CssClass="Body" Width="72px"></SPS:SPSTextBox>
									<asp:label id="LabelCapacityStar" runat="server" CssClass="redAsterick">
										<span class="redAsterick">*</span></asp:label></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 65px; HEIGHT: 20px" align="right"><asp:label id="labelTime" runat="server" CssClass="Body"> Time:</asp:label></TD>
								<TD style="HEIGHT: 20px"><SPS:SPSTextBox id="txtTime" runat="server" CssClass="Body" Width="88px"></SPS:SPSTextBox></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 65px; HEIGHT: 21px" align="right"><asp:label id="labelPartNumber" runat="server" CssClass="Body">Part #:</asp:label></TD>
								<TD style="HEIGHT: 21px"><SPS:SPSTextBox id="txtPartNumber" runat="server" CssClass="Body" Enabled="False"></SPS:SPSTextBox></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 65px; HEIGHT: 93px" vAlign="top" align="right"><asp:label id="Label26" runat="server" CssClass="Body">Notes:</asp:label></TD>
								<TD style="HEIGHT: 93px"><SPS:SPSTextBox id="txtNotes" runat="server" CssClass="Body" Width="400px" TextMode="MultiLine"
										Height="90px"></SPS:SPSTextBox></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<TR>
					<TD style="HEIGHT: 19px" align="left">
						<TABLE id="Table1" style="WIDTH: 464px; HEIGHT: 27px" width="464"
							border="0">
							<TR>
								<TD align="center"><asp:button id="btnUpdate" runat="server" Text="Update"></asp:button></TD>
								<TD align="center"><asp:button id="btnCancel" runat="server" Text="Close"></asp:button></TD>
								<TD align="center">
									<asp:Button id="btnDelete" runat="server" Text="Hide" Visible="false"></asp:Button>
									<asp:Button id="btnActivate" runat="server" Text="Activate" Visible="false"></asp:Button>
								</TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
			</TABLE>
			&nbsp;
		</form>
	</body>
</HTML>
