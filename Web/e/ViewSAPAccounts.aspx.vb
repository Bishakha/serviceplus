Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Process
Imports Sony.US.ServicesPLUS.Controls
Imports System.Drawing
Imports ServicesPlusException
Imports Sony.US.AuditLog
Imports Sony.US.SIAMUtilities

Namespace SIAMAdmin



    Partial Class ViewSAPAccounts
        Inherits UtilityClass
        'Dim objUtilties As Utilities = New Utilities
        'Changes done towards Bug#1212




#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub


        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            MyBase.IsProtectedPage(True, True)
            Page.ID = Request.Url.Segments.GetValue(1)
            InitializeComponent()
            '-- apply the datagrid style --
            ApplyDataGridStyle()
        End Sub

#End Region



#Region "   Events  "

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            'Put user code to initialize the page here
            If Not Page.IsPostBack Then
                controlMethed(sender, e)
            End If

        End Sub

        Private Sub DataGrid1_ItemCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DataGrid1.ItemCreated
            If e.Item.ItemType <> ListItemType.Header Then
                e.Item.ApplyStyle(GetStyle(e.Item.ItemType))
            End If
        End Sub

        'Private Sub AddNewSAPAccounts_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AddNewSAPAccounts.Click
        '    controlMethed(sender, e)
        'End Sub

#End Region



#Region "   Method  "


#Region "       Control Method      "

        Private Sub controlMethed(ByVal sender As System.Object, ByVal e As System.EventArgs)
            Dim custMan As New CustomerManager
            Dim SiamIdentity As String = String.Empty
            Dim LDapID As String = String.Empty
            Dim callingControl As String = String.Empty
            Dim reloadCustomer As Boolean = False

            Try
                If Request.QueryString("LdapId") IsNot Nothing Then
                    LDapID = Request.QueryString("LdapId")
                    SiamIdentity = custMan.GetSiamIdByLdapId(LDapID)
                Else
                    Return
                End If

                Dim thisCustomer As Customer = custMan.GetCustomer(SiamIdentity)

                If thisCustomer IsNot Nothing Then
                    If sender.ID IsNot Nothing Then callingControl = sender.ID.ToString()

                    Select Case callingControl
                        'Case AddNewSAPAccounts.ID.ToString()
                        '    addNewSAPAccountNumbers(thisCustomer)
                        Case DeleteSAPAccounts.ID.ToString()
                            reloadCustomer = RemoveSAPAccounts(thisCustomer)
                    End Select

                    If reloadCustomer Then 'Some SAP accounts deleted, need to reload the customer info
                        thisCustomer = custMan.GetCustomer(SiamIdentity)
                    End If
                    populateDataGrid(thisCustomer)
                End If

                WriteScriptsToPage()

            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

#End Region


#Region "       Populate the datagrid       "


        Private Sub populateDataGrid(ByRef thisCustomer As Customer)
            Try
                Dim showNoAccountMessage As Boolean = False
                If Not thisCustomer.SAPBillToAccounts Is Nothing And thisCustomer.SAPBillToAccounts.Length > 0 Then

                    Dim theAccounts As Account() = getSAPAccounts(thisCustomer)
                    Dim i As Integer = 0
                    For Each a In theAccounts
                        i = i + 1
                        If Not a.Validated Then
                            Dim account As Account = thisCustomer.SAPBillToAccounts(i - 1)
                            'Changes made by Sneha on 21/07/2014 for Bug:9015"Update ServicesPLUS to support 10 character KUNNR"
                            'a.AccountNumber = "000" + account.AccountNumber
                            a.AccountNumber = account.AccountNumber
                        End If
                    Next
                    If Not theAccounts Is Nothing Then
                        DataGrid1.DataSource = theAccounts
                        DataGrid1.AllowPaging = False
                        If theAccounts.Length > DataGrid1.PageSize Then DataGrid1.AllowPaging = True
                        DataGrid1.DataBind()
                        DataGrid1.Visible = True
                    Else
                        showNoAccountMessage = True     '-- show message - no account
                        DataGrid1.Visible = False ' no need to display the grid if no data is available - Deepa May 8, 2006
                    End If
                Else
                    showNoAccountMessage = True         '-- show message - no account
                    DataGrid1.Visible = False ' no need to display the grid if no data is available - Deepa May 8, 2006
                End If

                If showNoAccountMessage Then
                    If Not ErrorLabel.Text.Contains("Operation timed out.") Then

                        ErrorLabel.Text = "No SAP accounts."

                    End If

                End If


                ' clear off the previous text box contents
                For Each thisControl As Control In Me.FindControl("Form1").Controls
                    If TypeOf thisControl Is SPSTextBox Then
                        Dim thisAccountNumber As SPSTextBox = CType(thisControl, SPSTextBox)
                        thisAccountNumber.Text = ""
                    End If
                Next




            Catch ex As Exception
                'ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)'7382
                '7382 starts
                If ex.Message.Contains("Object reference not set to an instance of an object.") Then
                    Dim strUniqueKey As String = Utilities.GetUniqueKey
                    ErrorLabel.Text = Utilities.WrapExceptionforUINumber(ex, strUniqueKey)
                    Dim er As String
                    er = ErrorLabel.Text.Replace(ErrorLabel.Text, "Operation to retrieve account information failed. Please try again or contact the Application Support Center for assistance and mention incident number")
                    ErrorLabel.Text = er + " " + strUniqueKey + "."
                Else
                    ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                End If
                '7382 ends
            End Try
        End Sub

#End Region


#Region "       Get SAP Accounts        "


        Private Function getSAPAccounts(ByRef thisCustomer As Customer) As Account()
            Dim returnValue As Account() = Nothing
            Dim objPCILogger As New PCILogger()
            Try
                objPCILogger.EventOriginApplication = "ServicesPLUS"
                objPCILogger.EventOriginApplicationLocation = Me.Page.GetType().Name
                objPCILogger.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
                objPCILogger.OperationalUser = Environment.UserName
                objPCILogger.EventDateTime = DateTime.Now.ToLongTimeString()
                objPCILogger.HTTPRequestObjectValues = objPCILogger.GetRequestContextValue()
                objPCILogger.IndicationSuccessFailure = "Success"
                objPCILogger.EventOriginMethod = "getSAPAccounts"
                objPCILogger.Message = "getSAPAccounts Success."
                objPCILogger.EventType = EventType.View

                If thisCustomer IsNot Nothing Then
                    Dim thisCustMng As New CustomerManager
                    Dim thisArrayList As New ArrayList
                    Dim thisAccount As Account = Nothing
                    Dim objGlobalData As New GlobalData With {
                        .SalesOrganization = ConfigurationData.GeneralSettings.GlobalData.SalesOrganization.US,
                        .DistributionChannel = ConfigurationData.GeneralSettings.GlobalData.DistributionChannel.Value,
                        .Division = ConfigurationData.GeneralSettings.GlobalData.Division.Value,
                        .Language = ConfigurationData.GeneralSettings.GlobalData.LanguagewM.US
                    }
                    If thisCustomer.CountryCode = ConfigurationData.GeneralSettings.GlobalData.SalesOrganization.CA.Substring(0, 2) Then
                        objGlobalData.SalesOrganization = ConfigurationData.GeneralSettings.GlobalData.SalesOrganization.CA
                        objGlobalData.Language = ConfigurationData.GeneralSettings.GlobalData.LanguagewM.CA
                    End If

                    '-- so SAP account do NOT get populated with address information - not sure why - dwd
                    For Each thisAccount In thisCustomer.SAPBillToAccounts
                        thisCustMng.PopulateSAPAccount(thisAccount, objGlobalData)

                        If thisCustomer.UserType = "A" Then
                            If Not IsDuplicateAccountNumber(thisAccount.AccountNumber, thisArrayList) Then
                                thisArrayList.Add(thisAccount)
                            End If
                        End If
                    Next

                    If thisArrayList.Count > 0 Then returnValue = thisArrayList.ToArray(thisAccount.GetType)
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                objPCILogger.IndicationSuccessFailure = "Failure"
                objPCILogger.Message = "getSAPAccounts Failed. " & ex.Message.ToString()
                Throw  ' ASleight - This seems like poor practice. We process the error, then throw again?
            Finally
                objPCILogger.PushLogToMSMQ()
            End Try

            Return returnValue
        End Function

#End Region


#Region "   Add SAP Account Numbers     "

        Private Sub addNewSAPAccountNumbers(ByRef thisCustomer As Customer)
            Try
                'Global Data 
                Dim objGlobalData As New GlobalData()
                If thisCustomer.CountryCode = ConfigurationData.GeneralSettings.GlobalData.SalesOrganization.US.Substring(0, 2) Then
                    objGlobalData.SalesOrganization = ConfigurationData.GeneralSettings.GlobalData.SalesOrganization.US
                    objGlobalData.DistributionChannel = ConfigurationData.GeneralSettings.GlobalData.DistributionChannel.Value
                    objGlobalData.Division = ConfigurationData.GeneralSettings.GlobalData.Division.Value
                    objGlobalData.Language = ConfigurationData.GeneralSettings.GlobalData.LanguagewM.US
                Else
                    objGlobalData.SalesOrganization = ConfigurationData.GeneralSettings.GlobalData.SalesOrganization.CA
                    objGlobalData.DistributionChannel = ConfigurationData.GeneralSettings.GlobalData.DistributionChannel.Value
                    objGlobalData.Division = ConfigurationData.GeneralSettings.GlobalData.Division.Value
                    objGlobalData.Language = ConfigurationData.GeneralSettings.GlobalData.LanguagewM.CA
                End If

                Dim updateCustomer As Boolean = False

                For Each thisControl As Control In Me.FindControl("Form1").Controls
                    Dim thisTB As New SPSTextBox

                    If TypeOf thisControl Is SPSTextBox Then
                        Dim thisAccountNumber As String = CType(thisControl, SPSTextBox).Text.ToString()

                        If isValidSAPAccountFormat(thisAccountNumber) Then
                            Dim thisCustMng As New CustomerManager

                            Dim isValid As Boolean = thisCustMng.ValidateCustomerSAPAccount(thisCustomer, thisCustomer.AddSAPBillToAccount(thisAccountNumber), objGlobalData)
                            '***************Changes done for Bug# 1212 Ends here***************

                            If isValid = True Then
                                Console.WriteLine("Is a vaild account number {0}", thisAccountNumber)
                            Else
                                Console.WriteLine("Is NOT a vaild account number {0}", thisAccountNumber)
                            End If
                            'thisCustomer.AddSAPBillToAccount(thisTB.Text.ToString())
                            If Not updateCustomer Then updateCustomer = True
                        End If
                    End If
                Next

                If updateCustomer Then

                End If





            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Function isValidSAPAccountFormat(ByVal thisSAPAccount As String) As Boolean
            Dim returnValue As Boolean = False
            Try
                If thisSAPAccount.Trim().Length = 7 And IsNumeric(thisSAPAccount.Trim()) Then
                    returnValue = True
                End If


                returnValue = False


                returnValue = False
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                returnValue = False
            End Try
            Return returnValue
        End Function

#End Region

#Region "       Helper Methods      "

        '-- style for the datagrid -- this should really except a datagrid as a parameter
        Private Sub ApplyDataGridStyle()
            'DataGrid1.ApplyStyle(GetStyle(530))
            'DataGrid1.PageSize = 15
            'DataGrid1.AutoGenerateColumns = False
            'DataGrid1.AllowPaging = False
            'DataGrid1.GridLines = GridLines.Horizontal
            'DataGrid1.AllowSorting = True
            'DataGrid1.CellPadding = 3
            'DataGrid1.PagerStyle.Mode = PagerMode.NumericPages
            'DataGrid1.ItemStyle.Font.Size = FontUnit.Point(8)
            'DataGrid1.AlternatingItemStyle.Font.Size = FontUnit.Point(8)

            'DataGrid1.ApplyStyle(GetStyle(500))
            DataGrid1.PageSize = 8
            DataGrid1.AutoGenerateColumns = False
            DataGrid1.AllowPaging = True
            DataGrid1.GridLines = GridLines.Horizontal
            DataGrid1.AllowSorting = True
            DataGrid1.CellPadding = 3
            DataGrid1.PagerStyle.Mode = PagerMode.NumericPages
            DataGrid1.HeaderStyle.Font.Size = FontUnit.Point(8)
            DataGrid1.HeaderStyle.ForeColor = Color.GhostWhite
            DataGrid1.HeaderStyle.BackColor = Color.FromKnownColor(KnownColor.ControlDarkDark)
            DataGrid1.ItemStyle.Font.Size = FontUnit.Point(8)
            DataGrid1.AlternatingItemStyle.Font.Size = FontUnit.Point(8)
        End Sub

        '-- helper scripts --
        Private Sub WriteScriptsToPage()
            '-- this script will make sure that this page is only accessed via the iform tag in default.aspx -         
            RegisterClientScriptBlock("redirect", "<script language=""javascript"">if ( parent.frames.length === 0 ){window.location.replace(""" + MyBase.EntryPointPage + """)}</script>")

            '-- this will clear the status bar on the browser 
            RegisterStartupScript("ClearStatus", "<script language=""javascript"">window.parent.status = ""Done."";</script>")

            '-- this script is used to confirm that a user wants to remove an application -
            RegisterStartupScript("RemoveApplication", "<script language=""javascript"">function DeleteOp(itemId){var ok = window.confirm(""Are you sure ?"");if (ok === true){document.location.href=""" + Page.Request.Url.Segments.GetValue(1).ToString() + "?Remove="" + itemId;}}</script>")
        End Sub

        '-- makes sure the passed number does not exist in the passed arraylist --
        Private Function IsDuplicateAccountNumber(ByVal thisAccountNumber As String, ByRef thisArrayList As ArrayList) As Boolean
            Dim returnValue As Boolean = False

            Try
                If Not String.IsNullOrWhiteSpace(thisAccountNumber) And thisArrayList IsNot Nothing Then
                    For Each thisAccount As Account In thisArrayList
                        If thisAccount.AccountNumber = thisAccountNumber Then Return True
                    Next
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
            Return returnValue
        End Function
#End Region

#End Region

        Private Function RemoveSAPAccounts(ByRef thisCustomer As Customer) As Boolean
            Dim pciLog As New PCILogger()
            Dim gridItem As DataGridItem
            Dim checkBox As CheckBox
            Dim label As Label
            Dim cm As New CustomerManager
            Dim bRemoved = False
            Dim strAccount As String = String.Empty
            Dim iAccountCount As Int32 = 0
            Dim strAccountsToDelete(DataGrid1.Items.Count - 1) As String

            Try
                pciLog.EventOriginApplication = "ServicesPLUS"
                pciLog.EventOriginApplicationLocation = Me.Page.GetType().Name
                pciLog.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
                pciLog.OperationalUser = Environment.UserName
                pciLog.EventDateTime = DateTime.Now.ToLongTimeString()
                pciLog.HTTPRequestObjectValues = pciLog.GetRequestContextValue()
                pciLog.IndicationSuccessFailure = "Success"
                pciLog.EventOriginMethod = "removeSAPAccounts"
                pciLog.Message = "removeSAPAccounts Success."
                pciLog.EventType = EventType.Delete

                For Each gridItem In DataGrid1.Items
                    checkBox = CType(gridItem.Cells(6).Controls(1), CheckBox)
                    If checkBox.Checked Then
                        label = CType(gridItem.Cells(1).Controls(1), Label)
                        strAccountsToDelete(iAccountCount) = label.Text
                        'cm.DeleteSAPAccount(thisCustomer.CustomerID, label.Text)
                        iAccountCount += 1
                    End If
                Next

                If DataGrid1.Items.Count = iAccountCount Then
                    ErrorLabel.Text = "You can not delete all accounts"
                Else
                    For Each strAccount In strAccountsToDelete
                        If Not String.IsNullOrWhiteSpace(strAccount) Then
                            Utilities.LogDebug($"ViewSAPAccounts.removeSAPAccounts - Trying to delete Acct: {strAccount} for Customer ID: {thisCustomer.CustomerID}")
                            cm.DeleteSAPAccount(thisCustomer.CustomerID, strAccount)
                            bRemoved = True
                        End If
                    Next
                End If

            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                pciLog.IndicationSuccessFailure = "Failure" '6524
                pciLog.Message = "removeSAPAccounts Failed. " & ex.Message.ToString() '6524
            Finally
                pciLog.PushLogToMSMQ() '6524
            End Try
            Return bRemoved
        End Function

        Private Sub DeleteSAPAccounts_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DeleteSAPAccounts.Click
            controlMethed(sender, e)
        End Sub

        Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
            'If Not Request.QueryString("Identity") Is Nothing Then '7086
            '    btnModifyAccount.Attributes.Add("onclick", "javascript:window.location = 'AccountValidation.aspx?Identity=" + Request.QueryString("Identity").ToString + "&Mode=VSAPAC'; return false;") '7086
            If Request.QueryString("LdapId") IsNot Nothing Then '7086
                btnModifyAccount.Attributes.Add("onclick", "javascript:window.location = 'AccountValidation.aspx?LdapId=" + Request.QueryString("LdapId").ToString + "&Mode=VSAPAC'; return false;") '7086
            Else
                btnModifyAccount.Visible = False
            End If
        End Sub

        Public ReadOnly Property getIdentity() As String
            Get
                Return Request.QueryString("Identity")
            End Get
        End Property
        Public ReadOnly Property getLDapID_propr() As String
            Get
                Return Request.QueryString("LdapId")
            End Get
        End Property

        Protected Sub btnReloadAccount_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReloadAccount.Click
            controlMethed(sender, e)
        End Sub
    End Class

End Namespace
