Imports System.Data
Imports System.Drawing
Imports System.Xml
Imports ServicesPlusException
Imports Sony.US.AuditLog
Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Data
Imports Sony.US.ServicesPLUS.Process
Imports Sony.US.siam
Imports Sony.US.SIAMUtilities

Namespace SIAMAdmin

    Partial Class modifyUser
        Inherits UtilityClass

        Dim m_siamid As String = ""

#Region "  EVENTS  "
        Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
            MyBase.IsProtectedPage(True, True)
        End Sub

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                If Not IsPostBack Then
                    'Upper case - Zip code 
                    txtZip.Attributes.Add("onblur", "this.value = this.value.toUpperCase()")

                    m_siamid = Request.QueryString("siamid")
                    If m_siamid.Trim.Length <= 0 Then
                        Response.Redirect("SearchUser.aspx")
                    Else
                        PopulateCountryDDL()
                        PopulateStatesDDL()
                        PopulateRole()
                        Session.Add("snSIAMID", m_siamid)
                        LoadPageControls(m_siamid)
                    End If
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Protected Sub cmdSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdSave.Click
            Dim secAdmin As New SecurityAdministrator
            Dim custmgr As New CustomerManager()
            Dim updateResponse As UpdateUserResponse
            Dim userToEdit As User
            Dim adminUser As User
            Dim ctDate As Date = Date.Now.AddMonths(3)

            ' update the user
            If cmdSave.Text = " New " Then
                ClearFields()
                Exit Sub
            End If

            Dim pciLog As New PCILogger With {
                .EventOriginApplicationLocation = Page.GetType().Name,
                .EventOriginApplication = "ServicesPLUS Admin",
                .EventOriginMethod = "cmdSave_Click",
                .EventOriginServerMachineName = HttpContext.Current.Server.MachineName,
                .OperationalUser = Environment.UserName,
                .EventType = EventType.Update,
                .EventDateTime = DateTime.Now.ToLongTimeString(),
                .HTTPRequestObjectValues = GetRequestContextValue()
            }

            If CheckUserInputs() = True Then
                Try
                    If Session.Item("siamuser") IsNot Nothing Then
                        adminUser = DirectCast(Session.Item("siamuser"), User)
                        pciLog.EmailAddress = adminUser.EmailAddress
                        pciLog.CustomerID = adminUser.CustomerID
                        pciLog.CustomerID_SequenceNumber = adminUser.SequenceNumber
                        pciLog.SIAM_ID = adminUser.Identity
                        pciLog.LDAP_ID = adminUser.AlternateUserId
                    End If

                    userToEdit = New User With {
                        .AddressLine1 = txtAddr1.Text.Trim(),
                        .AddressLine2 = txtAddr2.Text.Trim(),
                        .AddressLine3 = txtAddr3.Text.Trim(),
                        .City = txtCity.Text.Trim(),
                        .CompanyName = txtCompName.Text.Trim(),
                        .Country = ddlCountry.SelectedValue,
                        .CustomerID = Session("scustomerid").ToString().Trim(),
                        .EmailAddress = txtEmail.Text.Trim(),
                        .EmailOK = False,
                        .Fax = txtFax.Text.Trim(),
                        .FirstName = txtFirstName.Text.Trim(),
                        .Identity = Session("snSIAMID").ToString().Trim(),
                        .LastName = txtLastName.Text.Trim(),
                        .Phone = txtPhone.Text.Trim(),
                        .PhoneExtension = txtExt.Text.Trim(),
                        .SequenceNumber = Convert.ToInt32(Session("Sequencenumber")),
                        .State = ddlStates.SelectedValue,
                        .UserId = txtUsrName.Text.Trim(),
                        .UserStatusCode = If(ddlUsrStatus.Text = "Active", 1, 3),
                        .UserType = "I",
                        .Zip = txtZip.Text.Trim()
                    }

                    ' call siam here to find out if the user id is a duplicate
                    If (secAdmin.IsDuplicateUser(userToEdit)) Then
                        SaveUserRoles(userToEdit.Identity)
                        Try
                            updateResponse = secAdmin.UpdateUser(userToEdit)
                            ErrorLabel.Text = "Updated successfully"
                            cmdSave.Text = " New "
                            pciLog.IndicationSuccessFailure = "Success"
                            pciLog.Message = $"User: {userToEdit.FirstName} {userToEdit.LastName} with UserId {userToEdit.UserId} details updated successfully"
                        Catch ex As Exception
                            ErrorLabel.Visible = True
                            pciLog.IndicationSuccessFailure = "Failure"
                            If (ex.Message = "PasswordMatch") Then
                                ErrorLabel.Text = "The new password can not be the same as one of the last three passwords used. Please choose another password."
                                pciLog.Message = "Error:Password is same as one of the last three passwords used"
                            Else
                                pciLog.Message = ex.Message
                            End If
                            Exit Sub
                        End Try
                    Else
                        ErrorLabel.Text = "Registration has encountered an error. You have attempted to register with a duplicate UserId, or a duplicate First Name, Last Name, and Email Address. Please check your information."
                        pciLog.IndicationSuccessFailure = "Failure"
                        pciLog.Message = "Error:Attempted to register with a duplicate UserId, or a duplicate First Name, Last Name, and Email Address "
                    End If

                Catch ex As Exception
                    ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                    pciLog.IndicationSuccessFailure = "Failure"
                    pciLog.Message = ex.Message.ToString()
                Finally
                    pciLog.PushLogToMSMQ()
                    Session.Remove("SequenceNumber")
                    Session.Remove("scustomerid") '6668
                End Try
            End If
        End Sub

        Protected Sub ddlCountry_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCountry.SelectedIndexChanged
            Try
                PopulateStatesDDL()
                ConfigZipCode()
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub
#End Region

#Region "  PAGE SETUP METHODS  "
        Private Sub LoadPageControls(ByVal pSIAMID As String)
            Try
                Dim listSiamIdentity As New ArrayList()
                Dim userMan As New UserRoleSecurityManager
                Dim oUser As Sony.US.siam.User
                Dim ds As New DataSet
                Dim row As DataRow
                Dim userStatus As String = ""
                Dim userRole As String = ""
                Dim userState As String = ""

                ds = userMan.GetUserDetailDocs(pSIAMID)

                If ds.Tables(0)?.Rows?.Count > 0 Then
                    row = ds.Tables(0).Rows(0)

                    oUser = New User With {
                        .AddressLine1 = row("ADDRESSLINE1").ToString(),
                        .AddressLine2 = row("ADDRESSLINE2").ToString(),
                        .AddressLine3 = row("ADDRESSLINE3").ToString(),
                        .City = row("CITY").ToString(),
                        .CompanyName = row("COMPANYNAME").ToString(),
                        .Country = row("COUNTRYCODE").ToString(),
                        .CustomerID = row("CUSTOMERID").ToString(),
                        .EmailAddress = row("EMAILADDRESS").ToString(),
                        .Fax = row("FAX").ToString(),
                        .FirstName = row("FIRSTNAME").ToString(),
                        .LastName = row("LASTNAME").ToString(),
                        .Phone = row("PHONE").ToString(),
                        .SequenceNumber = row("SEQUENCENUMBER").ToString(),
                        .State = row("STATE").ToString(),
                        .UserId = row("USERNAME").ToString(),
                        .Zip = row("ZIP").ToString()
                    }
                    userStatus = IIf((row("STATUSCODE").ToString() = "1"), "Active", "Inactive")
                    userRole = row("ROLENAME").ToString()
                    userState = row("STATE").ToString()
                    listSiamIdentity.Add(row("USERID").ToString())  ' Store SiamId, since this User type doesn't have it.

                    Session.Add("scustomerid", oUser.CustomerID)
                    Session.Add("Sequencenumber", oUser.SequenceNumber)

                    txtAddr1.Text = oUser.AddressLine1
                    txtAddr2.Text = oUser.AddressLine2
                    txtAddr3.Text = oUser.AddressLine3
                    txtCity.Text = oUser.City
                    txtCompName.Text = oUser.CompanyName
                    txtEmail.Text = oUser.EmailAddress.ToString() '.Substring(0, prefixlength)
                    txtFax.Text = oUser.Fax
                    txtFirstName.Text = oUser.FirstName
                    txtLastName.Text = oUser.LastName
                    txtPhone.Text = oUser.Phone
                    txtZip.Text = oUser.Zip
                    txtUsrName.Text = oUser.UserId

                    For Each sidentity As String In listSiamIdentity
                        txtExt.Text = PopulatePhoneExt(sidentity)
                    Next

                    PopulateUserType("I")
                    ddlUsrStatus.SelectedIndex = ddlUsrStatus.Items.IndexOf(ddlUsrStatus.Items.FindByValue(userStatus))
                    LoadUserRoles(pSIAMID)
                    ddlCountry.SelectedValue = oUser.Country

                    If ddlCountry.SelectedValue = ConfigurationData.GeneralSettings.GlobalData.SalesOrganization.US.Substring(0, 2) Then
                        lblZip.Text = "Zip Code"
                        txtZip.MaxLength = Convert.ToInt32(ConfigurationData.GeneralSettings.GlobalData.ZipCode.US)
                    Else
                        lblZip.Text = "Postal Code"
                        txtZip.MaxLength = Convert.ToInt32(ConfigurationData.GeneralSettings.GlobalData.ZipCode.CA)
                    End If

                    PopulateStatesDDL()
                    ddlStates.SelectedValue = oUser.State
                    If txtExt.Text.Trim() = "-" Then    ' ASleight - Uh... what?! Also, I've tried to trip this logic and it doesn't happen.
                        txtExt.Text = String.Empty
                        ErrorLabel.Text = "User does not exist in ServicesPLUS. Please click Save to add this user in ServicesPLUS application."
                    End If
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Sub PopulateCountryDDL()
            Try
                Dim data_store As New Sony.US.ServicesPLUS.Data.SISDataManager
                Dim countrydetail As DataSet = data_store.GetCountryDetails()

                If countrydetail.Tables(0)?.Rows?.Count > 0 Then
                    For Each row As DataRow In countrydetail.Tables(0).Rows
                        ddlCountry.Items.Add(New ListItem(row("COUNTRYNAME"), row("COUNTRYCODE")))
                    Next
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Sub PopulateStatesDDL()
            Dim cm As New CatalogManager
            Dim data_store As New Sony.US.ServicesPLUS.Data.SISDataManager
            Dim statedetail As DataSet
            Dim selectOne As New ListItem("Select One", "")

            Try
                statedetail = data_store.GetStateDetailsByCountry(ddlCountry.SelectedItem.Text)

                ddlStates.Items.Clear()
                ddlStates.Items.Add(selectOne)

                If statedetail.Tables(0)?.Rows?.Count > 0 Then
                    For Each row As DataRow In statedetail.Tables(0).Rows
                        ddlStates.Items.Add(New ListItem(row("STATE"), row("ABBREVIATION")))
                    Next
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Sub PopulateRole()
            Dim oUsrSecMgr As New UserRoleSecurityManager
            Dim userRoleList() As Sony.US.ServicesPLUS.Core.UserRole = oUsrSecMgr.GetUserRoles("%", "1")

            For Each role As UserRole In userRoleList
                lstRole.Items.Add(New ListItem(ChangeCase(role.RoleName), role.RoleID))
            Next
        End Sub

        Private Function PopulatePhoneExt(ByVal siamIdentity As String) As String
            Dim cm As New CustomerManager
            Dim returnValue As String
            returnValue = cm.GetCustomerPhoneExtension(siamIdentity)
            If returnValue.Trim() <> String.Empty Then
                If (returnValue <> String.Empty And returnValue.IndexOf("~") >= 0) Then
                    Session.Add("modifyUserSequence", returnValue.Split("~")(1))
                    Session.Add("modifyCustomerID", returnValue.Split("~")(2))
                    returnValue = returnValue.Split("~")(0)
                End If
            Else
                returnValue = "-"
                Session.Add("modifyCustomerID", "NOTHING")
            End If
            Return returnValue
        End Function

        Private Sub PopulateUserType(ByVal userType As String)
            Dim selectOne As New ListItem("Select One", "")

            ' ASleight - What's the point of the below "if"?
            If userType = "A" Or userType = "P" Then
                ddlUsrStatus.Items.Add(selectOne)
                ddlUsrStatus.Items.Add(ConfigurationData.GeneralSettings.StatusCodes.Active) '6668
                ddlUsrStatus.Items.Add(ConfigurationData.GeneralSettings.StatusCodes.Inactive) '6668
            Else
                ddlUsrStatus.Items.Add(selectOne)
                ddlUsrStatus.Items.Add(ConfigurationData.GeneralSettings.StatusCodes.Active) '6668
                ddlUsrStatus.Items.Add(ConfigurationData.GeneralSettings.StatusCodes.Inactive) '6668
            End If
        End Sub

        Private Sub ClearFields()
            txtExt.Text = String.Empty
            cmdSave.Text = "  Save  "
            cmdSave.Text = "  Save  "
            cmdSave.ToolTip = "Click to save user" 'Added for E785
            txtFirstName.Text = ""
            txtLastName.Text = ""
            txtCompName.Text = ""
            txtAddr1.Text = ""
            txtAddr2.Text = ""
            txtAddr3.Text = ""
            txtFax.Text = ""
            txtCity.Text = ""
            ddlStates.SelectedIndex = 0
            txtZip.Text = ""
            txtPhone.Text = ""
            txtEmail.Text = ""
            txtUsrName.Text = ""
            ddlUsrStatus.SelectedIndex = 0
            lstRole.SelectedIndex = -1
            txtFirstName.ReadOnly = False
            txtLastName.ReadOnly = False
            txtUsrName.ReadOnly = False
            txtFirstName.Focus()
        End Sub

        Private Sub ConfigZipCode()
            If ddlCountry.SelectedValue = ConfigurationData.GeneralSettings.GlobalData.SalesOrganization.US.Substring(0, 2) Then
                lblZip.Text = "Zip Code"
                txtZip.MaxLength = Convert.ToInt32(ConfigurationData.GeneralSettings.GlobalData.ZipCode.US)
                txtZip.Text = ""
            Else
                lblZip.Text = "Postal Code"
                txtZip.MaxLength = Convert.ToInt32(ConfigurationData.GeneralSettings.GlobalData.ZipCode.CA)
                txtZip.Text = ""
            End If
        End Sub
#End Region

#Region "  HELPER AND VALIDATION METHODS  "
        Private Function ChangeCase(ByVal pString As String) As String
            Dim str As String = pString.ToLower()
            Return System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(str)
        End Function

        Private Function CheckUserInputs() As Boolean
            Dim isInputValid As Boolean = True
            Dim emailPattern As String = "^[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$"
            Dim rgxZipCode As Regex
            Dim isCanada As Boolean = False
            Dim rolesSelected As Integer = 0

            ErrorLabel.Text = ""
            LabelLine1Star.Text = ""
            LabelLine2Star.Text = ""
            LabelLine3Star.Text = ""
            LabelLine4Star.Text = ""

            lblCompName.ForeColor = Color.Black
            If String.IsNullOrWhiteSpace(txtCompName.Text) Then
                lblCompName.ForeColor = Color.Red
                ErrorLabel.Text = "Please enter valid Company Name"
                isInputValid = False
            End If

            lblAddr1.ForeColor = Color.Black
            If String.IsNullOrWhiteSpace(txtAddr1.Text) Then
                lblAddr1.ForeColor = Color.Red
                ErrorLabel.Text = "Please enter valid Address"
                txtAddr1.Focus()
                isInputValid = False
            ElseIf txtAddr1.Text.Length > 35 Then
                lblAddr1.ForeColor = Color.Red
                LabelLine1Star.Text = "Street Address must be 35 characters or less in length"
                txtAddr1.Focus()
                isInputValid = False
            End If

            lblAddr2.ForeColor = Color.Black
            If txtAddr2.Text.Length > 35 Then
                lblAddr2.ForeColor = Color.Red
                LabelLine2Star.Text = "Address 2nd Line must be 35 characters or less in length"
                txtAddr2.Focus()
                isInputValid = False
            End If

            lblAddr3.ForeColor = Color.Black
            If txtAddr3.Text.Length > 35 Then
                lblAddr3.ForeColor = Color.Red
                LabelLine3Star.Text = "Address 3rd Line must be 35 characters or less in length"
                txtAddr3.Focus()
                isInputValid = False
            End If

            lblCity.ForeColor = Color.Black
            If String.IsNullOrWhiteSpace(txtCity.Text) Then
                lblCity.ForeColor = Color.Red
                ErrorLabel.Text = "Please enter valid City."
                isInputValid = False
            ElseIf txtCity.Text.Length > 35 Then
                lblCity.ForeColor = Color.Red
                LabelLine4Star.Text = "City must be 35 characters or less in length"
                isInputValid = False
            End If

            lblState.ForeColor = Color.Black
            If ddlStates.SelectedIndex < 1 Then
                lblState.ForeColor = Color.Red
                ErrorLabel.Text = "Please choose State"
                isInputValid = False
            End If

            lblZip.ForeColor = Color.Black
            If String.IsNullOrWhiteSpace(txtZip.Text) Then
                lblZip.ForeColor = Color.Red
                ErrorLabel.Text = "Please enter valid Postal/Zip Code"
                isInputValid = False
            Else
                isCanada = (ddlCountry.SelectedValue = ConfigurationData.GeneralSettings.GlobalData.SalesOrganization.CA.Substring(0, 2))
                If isCanada Then
                    rgxZipCode = New Regex("^[ABCEGHJKLMNPRSTVXY]{1}\d{1}[A-Z]{1} *\d{1}[A-Z]{1}\d{1}$")
                Else
                    rgxZipCode = New Regex("\d{5}(-\d{4})?")
                End If

                If Not rgxZipCode.IsMatch(txtZip.Text.Trim()) Then
                    If isCanada Then
                        ErrorLabel.Text = "Invalid Postal Code format. Use: A1A 1A1"
                    Else
                        ErrorLabel.Text = "Invalid Zip Code format. Use: 12345 or 12345-6789"
                    End If
                    isInputValid = False
                End If

                If Not isCanada Then
                    If txtZip.Text.Length < Convert.ToInt32(ConfigurationData.GeneralSettings.GlobalData.ZipCode.US) Then
                        lblZip.ForeColor = Color.Red
                        ErrorLabel.Text = "Invalid Zip Code format. Use: 12345 or 12345-6789"
                        isInputValid = False
                    End If
                ElseIf txtZip.Text.Length < Convert.ToInt32(ConfigurationData.GeneralSettings.GlobalData.ZipCode.CA) Then
                    lblZip.ForeColor = Color.Red
                    ErrorLabel.Text = "Invalid Postal Code format. Use: A1A 1A1"
                    isInputValid = False
                End If
            End If

            lblPhone.ForeColor = Color.Black
            If String.IsNullOrWhiteSpace(txtPhone.Text) Then
                lblPhone.ForeColor = Color.Red
                ErrorLabel.Text = "Please enter valid Phone number"
                isInputValid = False
            End If

            If Not String.IsNullOrWhiteSpace(txtExt.Text) Then
                If Not IsNumeric(txtExt.Text.Trim()) Then
                    ErrorLabel.Text = "Extension field can have only numeric value"
                    isInputValid = False
                End If
            End If

            lblEmail.ForeColor = Color.Black
            If (String.IsNullOrWhiteSpace(txtEmail.Text)) Or (Not Regex.IsMatch(txtEmail.Text, emailPattern)) Then
                lblEmail.ForeColor = Color.Red
                ErrorLabel.Text = "Please enter valid email address"
                isInputValid = False
            End If

            lblRole.ForeColor = Color.Black
            If lstRole.Items?.Count = 0 Then
                lblRole.ForeColor = Color.Red
                ErrorLabel.Text = "Please choose Role"
                isInputValid = False
            End If

            ' 2018-06-14 ASleight - Replaced with LINQ query below for speed.
            'For Each listItem As ListItem In lstRole.Items
            '    If listItem.Selected Then
            '        rolesSelected += 1
            '    End If
            'Next
            rolesSelected = (From item As ListItem In lstRole.Items Where item.Selected = True Select item).Count

            If rolesSelected = 0 Then
                lblRole.ForeColor = Color.Red
                ErrorLabel.Text = "Please choose Role"
                isInputValid = False
            End If

            lblStatus.ForeColor = Color.Black
            If ddlUsrStatus.SelectedIndex < 1 Then
                lblStatus.ForeColor = Color.Red
                ErrorLabel.Text = "Please choose user status"
                isInputValid = False
            End If

            Return isInputValid
        End Function

        Private Sub SaveUserRoles(ByVal pSiamID As String)
            Dim oUsrRoleSecMgr As New UserRoleSecurityManager
            Dim sRoleString As String = ""
            Dim index As Integer

            For index = 0 To lstRole.Items.Count - 1
                If lstRole.Items(index).Selected = True Then
                    sRoleString = sRoleString + lstRole.Items(index).Value.ToString() + "-"
                End If
            Next
            If oUsrRoleSecMgr.UpdateUserRoles(pSiamID, sRoleString) = 0 Then
                Throw New Exception()
            End If

        End Sub

        Private Sub LoadUserRoles(ByVal pSIAMID As String)
            Dim oUsrRoleSecMgr As New UserRoleSecurityManager
            Dim docUserFunct As XmlDocument

            docUserFunct = oUsrRoleSecMgr.GetUserRolesDocs(pSIAMID)
            Dim nodes As XmlNodeList = docUserFunct.DocumentElement.ChildNodes

            For Each node As XmlNode In nodes
                SelectRole(node("ROLEID").InnerText)
            Next
        End Sub

        Private Sub SelectRole(ByVal psRoleID As String)
            Dim index As Integer

            For index = 0 To lstRole.Items.Count - 1
                If lstRole.Items(index).Value = psRoleID.Trim() Then
                    lstRole.Items(index).Selected = True
                End If
            Next
        End Sub
#End Region
    End Class

End Namespace
