<%@ Reference Page="~/e/Utility.aspx" %>

<%@ Page Language="VB" SmartNavigation="true" AutoEventWireup="false" CodeFile="modifyUser.aspx.vb" Inherits="SIAMAdmin.modifyUser" CodeFileBaseClass="SIAMAdmin.UtilityClass" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
<head id="Head1" runat="server">
    <title>Modify User Profile</title>
    <link href="includes/style.css" type="text/css" rel="stylesheet">
    <style type="text/css">
        .style1 {
            height: 22px;
        }
    </style>
</head>
<body>
    <form id="frmUser" runat="server">
        <table width="75%" border="0" align="center" cellspacing="2">
            <tr class="PageHead">
                <td colspan="2" style="text-align: center; width: 601px;">
                    Modify User Profile
                </td>
            </tr>
            <tr>
                <td align="center" colspan="2" style="width: 601px; height: 25px">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="2" bgcolor="#5d7180" align="center" style="height: 25px; width: 601px;">
                    <table border="0">
                        <tr style="height: 25px;">
                            <td class="Nav3"><a class="Nav3" href="appsUser.aspx">New</a></td>
                            <td class="Nav3" align="center" width="5">&nbsp;|&nbsp;</td>
                            <td><a class="Nav3" href="SearchUser.aspx">Search</a></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="text-align: center; width: 601px; height: 19px;">
                    <asp:Label ID="ErrorLabel" runat="server" CssClass="body" ForeColor="Red" EnableViewState="False" />&nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: right">
                    <asp:Label ID="Label1" runat="server" CssClass="body" Text="First Name:" />
                </td>
                <td>
                    <span size="2" face="MS Reference Sans Serif">
                        <SPS:SPSTextBox ID="txtFirstName" runat="server" CssClass="body" ReadOnly="True" />
                        <span style="font-size: 6pt; color: #ff3333">*</span></span>
                </td>
            </tr>
            <tr>
                <td style="text-align: right">
                    <asp:Label ID="Label2" runat="server" CssClass="body" Text="Last Name:" />
                </td>
                <td>
                    <span size="2" face="MS Reference Sans Serif">
                        <SPS:SPSTextBox ID="txtLastName" runat="server" CssClass="body" ReadOnly="True" />
                        <span style="font-size: 6pt; color: #ff3333">*</span></span>
                </td>
            </tr>
            <tr>
                <td style="text-align: right">
                    <asp:Label ID="lblCompName" runat="server" CssClass="body" Text="Company Name:" />
                </td>
                <td>
                    <span size="2" face="MS Reference Sans Serif">
                        <SPS:SPSTextBox ID="txtCompName" runat="server" CssClass="body" />
                        <span style="font-size: 6pt; color: #ff3333">*</span></span>
                </td>
            </tr>
            <tr>
                <td style="text-align: right">
                    <asp:Label ID="lblAddr1" runat="server" CssClass="body" Text="Street Address:" />
                </td>
                <td>
                    <span size="2" face="MS Reference Sans Serif">
                    <SPS:SPSTextBox ID="txtAddr1" runat="server" CssClass="body" MaxLength="50" Width="182px" />
                    <asp:Label ID="LabelLine1Star" CssClass="redAsterick" runat="server">
                    <span style="font-size: 6pt; color: #ff3333">*</span></asp:Label></span>
                </td>
            </tr>
            <tr>
                <td style="text-align: right">
                    <asp:Label ID="lblAddr2" runat="server" CssClass="body" Text="Address 2nd Line:" />
                </td>
                <td>
                    <SPS:SPSTextBox ID="txtAddr2" runat="server" CssClass="body" MaxLength="50" Width="182px" />
                    <asp:Label ID="LabelLine2Star" CssClass="redAsterick" runat="server" />
                </td>
            </tr>
            <tr>
                <td style="text-align: right">
                    <asp:Label ID="lblAddr3" runat="server" CssClass="body" Text="Address 3rd Line:" />
                </td>
                <td>
                    <SPS:SPSTextBox ID="txtAddr3" runat="server" CssClass="body" MaxLength="50" Width="182px" />
                    <asp:Label ID="LabelLine3Star" CssClass="redAsterick" runat="server" />
                </td>
            </tr>
            <tr>
                <td style="text-align: right">
                    <span style="font-size: 8pt; font-family: MS Sans Serif">
                    <asp:Label ID="lblCity" runat="server" CssClass="body" Text="City :" /></span>
                </td>
                <td>
                    <SPS:SPSTextBox ID="txtCity" runat="server" CssClass="body" MaxLength="50" />
                    <asp:Label ID="LabelLine4Star" CssClass="redAsterick" runat="server" />
                    <span style="font-size: 6pt; color: #ff3333;">*</span>
                </td>
            </tr>
            <tr>
                <td style="text-align: right;" class="style1">
                    <asp:Label ID="lblCountry" runat="server" CssClass="body" Text="Country:" />
                </td>
                <td class="style1">
                    <asp:DropDownList ID="ddlCountry" runat="server" CssClass="body" AutoPostBack="True" />
                    <span style="font-size: 6pt; color: #ff3333;">*</span>
                </td>
            </tr>
            <tr>
                <td style="text-align: right; height: 22px;">
                    <asp:Label ID="lblState" runat="server" CssClass="body" Text="State:" />
                </td>
                <td style="height: 22px">
                    <asp:DropDownList ID="ddlStates" runat="server" CssClass="body" Width="128px" />
                    <span style="font-size: 6pt; color: #ff3333;">*</span>
                </td>
            </tr>

            <tr>
                <td style="text-align: right">
                    <asp:Label ID="lblZip" runat="server" CssClass="body" Text="ZIP:" />
                </td>
                <td>
                    <SPS:SPSTextBox ID="txtZip" runat="server" CssClass="body" MaxLength="6" />
                    <span style="font-size: 6pt; color: #ff3333;">*</span>
                </td>
            </tr>
            <tr>
                <td style="text-align: right">
                    <asp:Label ID="lblPhone" runat="server" CssClass="body" Text="Phone:" />
                </td>
                <td>
                    <SPS:SPSTextBox ID="txtPhone" runat="server" CssClass="body" MaxLength="20" />
                    <span style="font-size: 6pt; color: #ff3333;">*</span>
                    <SPS:SPSTextBox ID="txtExt" Width="40" MaxLength="5" runat="server" CssClass="body" />
                </td>
            </tr>
            <tr>
                <td style="text-align: right">
                    <asp:Label ID="Label9" runat="server" CssClass="body" Text="FAX:" />
                </td>
                <td>
                    <SPS:SPSTextBox ID="txtFax" runat="server" CssClass="body" MaxLength="20" />
                </td>
            </tr>
            <tr>
                <td style="text-align: right">
                    <asp:Label ID="lblEmail" runat="server" CssClass="body" Text="E mail:" />
                </td>
                <td>
                    <SPS:SPSTextBox ID="txtEmail" runat="server" CssClass="body" MaxLength="100" />
                    <%--  <asp:Label ID="lblEmailSuffix" runat="server" CssClass="body" Text="@am.sony.com" />--%>
                    <span style="font-size: 6pt; color: #ff3333;">*</span>
                </td>
            </tr>
            <tr>
                <td style="text-align: right">
                    <asp:Label ID="Label11" runat="server" CssClass="body" Text="User Name:" />
                </td>
                <td>
                    <SPS:SPSTextBox ID="txtUsrName" runat="server" CssClass="body" MaxLength="20" ReadOnly="True" />
                    <span style="font-size: 6pt; color: #ff3333;">*</span>
                </td>
            </tr>
            <tr>
                <td style="text-align: right">
                    <asp:Label ID="lblRole" runat="server" CssClass="body" Text="Role:" />
                </td>
                <td>

                    <asp:ListBox ID="lstRole" runat="server" CssClass="body" SelectionMode="Multiple" Width="165px" />
                    <span style="font-size: 6pt; color: #ff3333;">*</span>
                </td>
            </tr>
            <tr>
                <td style="text-align: right">
                    <asp:Label ID="lblType" runat="server" CssClass="body" Text="Type:" />
                </td>
                <td>
                    <SPS:SPSTextBox ID="txtUserType" runat="server" CssClass="body" MaxLength="20" ReadOnly="True">Internal</SPS:SPSTextBox>
                </td>
            </tr>
            <tr>
                <td style="text-align: right">
                    <asp:Label ID="lblStatus" runat="server" CssClass="body" Text="Status:" />
                </td>
                <td>
                    <asp:DropDownList ID="ddlUsrStatus" runat="server" CssClass="body" Width="128px" />
                    <span style="font-size: 6pt; color: #ff3333;">*</span>
                </td>
            </tr>
            <tr>
                <td style="width: 299px; height: 19px;" colspan="2">&nbsp;</td>
            </tr>
            <tr>
                <%--<td colspan="2"><div align="center"> --%>
                <td></td>
                <td>
                    <asp:Button ID="cmdSave" runat="server" CssClass="body" Text="  Save  " ToolTip="Click to save user" />
                    <%--<asp:Button ID="cmdCancel" runat="server" CssClass="body" Text=" Cancel " ToolTip="Click to Cancel the entry" />&nbsp;--%>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
