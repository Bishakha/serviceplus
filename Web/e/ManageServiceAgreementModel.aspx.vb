﻿Imports Sony.US.SIAMUtilities
Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Process
Imports ServicesPlusException
Imports Sony.US.AuditLog
Imports System.Data

Namespace SIAMAdmin
    Partial Class ManageServiceAgreementModel
        Inherits UtilityClass
        'Dim objUtilties As Utilities = New Utilities

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            MyBase.IsProtectedPage(True, True)
            Page.ID = Request.Url.Segments.GetValue(1)
            InitializeComponent()

        End Sub


        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                'ErrorLabel.Visible = False
                ErrorLabel.Text = String.Empty
                If Not IsPostBack Then


                    'Populate DataGrid
                    InitializeControls(False)
                    fnBindCountry()
                    fnBindLanguage()
                    PopulateDataGrid()
                    btnCancel.Enabled = False
                    btnSave.Enabled = False
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Protected Sub dgServiceAgreementModel_ItemCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgServiceAgreementModel.ItemCreated
            If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
                Dim lnkBtn As LinkButton
                lnkBtn = e.Item.FindControl("lnkBtnDelete")
                lnkBtn.Attributes.Add("onclick", "javascript:return ConfirmDelete();")
            End If
        End Sub

        Private Sub dgServiceAgreementModel_PageIndexChanged(ByVal sender As System.Object, ByVal e As DataGridPageChangedEventArgs) Handles dgServiceAgreementModel.PageIndexChanged
            Try
                dgServiceAgreementModel.CurrentPageIndex = e.NewPageIndex
                PopulateDataGrid()
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Protected Sub dgServiceAgreementModel_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgServiceAgreementModel.ItemCommand
            Try
                If e.CommandName = "Edit" Then
                    dgServiceAgreementModel.SelectedIndex = e.Item.ItemIndex
                    InitializeControls(True)
                    txtProductCCode.Text = e.Item.Cells(0).Text
                    txtProductCCode.ReadOnly = False
                    txtProductGroup.Text = e.Item.Cells(1).Text
                    txtProductCode.Text = e.Item.Cells(2).Text
                    txtModelNumber.Text = e.Item.Cells(3).Text
                    lblSAModelID.Text = e.CommandArgument.ToString()

                    btnSave.Enabled = True
                    btnSave.Text = "Update"
                    btnAddNew.Enabled = False
                    btnCancel.Enabled = True

                    Dim strCountry As String
                    strCountry = e.Item.Cells(4).Text
                    'If e.Item.Cells(4).Text = ConfigurationData.GeneralSettings.GlobalData.SalesOrganization.US Then
                    '    ddlcountry.SelectedIndex = 0
                    'Else
                    '    ddlcountry.SelectedIndex = 1
                    'End If
                    ddlcountry.Items.FindByText(strCountry).Selected = True

                    fnBindLanguage()

                    ddllanguage.SelectedIndex = -1
                    strCountry = e.Item.Cells(5).Text
                    ddllanguage.Items.FindByText(strCountry).Selected = True

                ElseIf e.CommandName = "Delete" Then
                    Dim objSIAMOperation As SIAMOperation = New SIAMOperation()
                    objSIAMOperation.DeleteServiceAgreementModel(e.Item.Cells(6).Text.Trim())
                    ErrorLabel.Text = "Deleted successfully."
                    InitializeControls(False)
                    btnSave.Enabled = False
                    btnAddNew.Enabled = True
                    btnCancel.Enabled = False
                    btnSave.Text = "Save"
                    PopulateDataGrid()
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Sub AddNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddNew.Click
            dgServiceAgreementModel.SelectedIndex = -1
            InitializeControls(True)
            btnSave.Enabled = True
            btnCancel.Enabled = True
            btnSave.Text = "Save"
            btnAddNew.Enabled = False
            txtProductCCode.ReadOnly = False
            txtProductCCode.Focus()
        End Sub

        Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
            dgServiceAgreementModel.SelectedIndex = -1
            InitializeControls(False)

            btnSave.Enabled = False
            btnAddNew.Enabled = True
            btnCancel.Enabled = False
            btnSave.Text = "Save"
        End Sub

        Private Sub btnSave_click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
            Dim objPCILogger As New PCILogger() '6524
            If Valid() = False Then
                ErrorLabel.Text = "Please correct following red marked field(s)."
                Exit Sub
            Else
                Dim objSIAMOperation As SIAMOperation = New SIAMOperation()
                Try
                    '6524 start
                    objPCILogger.EventOriginApplication = "ServicesPLUS"
                    objPCILogger.EventOriginApplicationLocation = Me.Page.GetType().Name
                    objPCILogger.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
                    objPCILogger.OperationalUser = Environment.UserName
                    objPCILogger.EventDateTime = DateTime.Now.ToLongTimeString()
                    objPCILogger.HTTPRequestObjectValues = objPCILogger.GetRequestContextValue()
                    objPCILogger.IndicationSuccessFailure = "Success"
                    objPCILogger.EventOriginMethod = "btnSave_click"
                    objPCILogger.Message = "btnSave_click Success."
                    objPCILogger.EventType = EventType.Add
                    '6524 end

                    Dim strLanguage As String
                    strLanguage = ddllanguage.SelectedItem.Value
                    Dim objGlobalData As New GlobalData
                    objGlobalData.SalesOrganization = ddlcountry.SelectedItem.Value
                    objGlobalData.Language = strLanguage

                    Dim intID As Int32
                    If btnSave.Text = "Save" Then
                        If Not objSIAMOperation.checkProductCode(txtProductCCode.Text.Trim()) Then
                            objSIAMOperation.InsertServiceAgreementModel(txtProductCCode.Text.Trim(), txtProductGroup.Text.Trim(), txtProductCode.Text.Trim(), txtModelNumber.Text.Trim(), objGlobalData)
                        Else
                            ErrorLabel.Text = "Product Code already exist."
                            Exit Sub
                        End If

                    ElseIf btnSave.Text = "Update" Then
                        intID = Convert.ToInt32(lblSAModelID.Text.Trim())
                        objSIAMOperation.UpdateServiceAgreementModel(intID, txtProductCCode.Text.Trim(), txtProductGroup.Text.Trim(), txtProductCode.Text.Trim(), txtModelNumber.Text.Trim(), objGlobalData)
                    End If
                    dgServiceAgreementModel.SelectedIndex = -1
                    btnSave.Enabled = False
                    btnAddNew.Enabled = True
                    btnCancel.Enabled = False
                    btnSave.Text = "Save"
                    PopulateDataGrid()
                    InitializeControls(False)




                Catch ex As Exception
                    ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                    objPCILogger.IndicationSuccessFailure = "Failure" '6524
                    objPCILogger.Message = "btnSave_click Failed. " & ex.Message.ToString() '6524
                Finally
                    objPCILogger.PushLogToMSMQ() '6524
                End Try
            End If
        End Sub

        Private Sub PopulateDataGrid()

            Dim objSIAMOperation As SIAMOperation = New SIAMOperation()
            Dim dsCertificateNumbers As DataSet = objSIAMOperation.GETAllServiceAgreementModel()
            dgServiceAgreementModel.SelectedIndex = -1
            If dsCertificateNumbers.Tables(0).Rows.Count > 0 Then
                dgServiceAgreementModel.DataSource = dsCertificateNumbers.Tables(0)
            Else
                dgServiceAgreementModel.DataSource = Nothing
            End If
            dgServiceAgreementModel.DataBind()

        End Sub

        Private Sub InitializeControls(ByVal Enable As Boolean)
            txtProductCCode.Text = ""
            txtProductCode.Text = ""
            txtModelNumber.Text = ""
            txtProductGroup.Text = ""
            txtProductCCode.Enabled = Enable
            txtProductCode.Enabled = Enable
            txtModelNumber.Enabled = Enable
            txtProductGroup.Enabled = Enable
            txtProductCCode.ReadOnly = True
            ddlcountry.SelectedIndex = 0
            fnBindLanguage()

        End Sub

        Private Function Valid() As Boolean
            Dim blnValid As Boolean = True

            If txtProductCCode.Text.Trim() = "" Then
                lblProductCategory.CssClass = "redAsterick"
                blnValid = False
            End If
            If txtProductGroup.Text.Trim() = "" Then
                txtProductGroup.CssClass = "redAsterick"
                blnValid = False
            End If
            If txtProductCode.Text.Trim() = "" Then
                lblProductCode.CssClass = "redAsterick"
                blnValid = False
            End If
            If txtModelNumber.Text.Trim() = "" Then
                lblModelNumber.CssClass = "redAsterick"
                blnValid = False
            End If
            Return blnValid
        End Function

        'Country and Language 
        Private Sub fnBindCountry()
            Dim dsCountry As DataSet
            Dim catMng As New CatalogManager

            dsCountry = catMng.GetCountry()

            ddlcountry.DataSource = dsCountry.Tables(0)
            ddlcountry.DataTextField = "COUNTRYNAME"
            ddlcountry.DataValueField = "COUNTRYCODE"
            ddlcountry.DataBind()

            'If dsCountry.Tables(0).Rows.Count > 0 Then
            '    With dsCountry.Tables(0)
            '        For i As Integer = 0 To dsCountry.Tables(0).Rows.Count - 1
            '            ddlcountry.Items.Add(New ListItem(dsCountry.Tables(0).Rows(i)("COUNTRYNAME"), dsCountry.Tables(0).Rows(i)("COUNTRYCODE")))
            '        Next
            '    End With
            'End If

        End Sub

        Private Sub fnBindLanguage()
            Try

                ddllanguage.Items.Clear()

                Dim newListItem1 As New ListItem()
                newListItem1.Text = "English"
                newListItem1.Value = ConfigurationData.GeneralSettings.GlobalData.Language.US
                ddllanguage.Items.Add(newListItem1)

                If ddlcountry.SelectedValue = ConfigurationData.GeneralSettings.GlobalData.SalesOrganization.CA.Substring(0, 2) Then

                    Dim newListItem2 As New ListItem()
                    newListItem2.Text = "French"
                    newListItem2.Value = ConfigurationData.GeneralSettings.GlobalData.Language.CA
                    ddllanguage.Items.Add(newListItem2)
                    ddllanguage.Items.FindByText(newListItem2.ToString()).Selected = True

                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub
        Protected Sub ddlcountry_SelectedIndexChanged1(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlcountry.SelectedIndexChanged

            Try
                fnBindLanguage()
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub
#End Region
    End Class
End Namespace
