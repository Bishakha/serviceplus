<%@ Reference Page="~/e/Utility.aspx"  %>

<%@ Page Language="vb" AutoEventWireup="false" SmartNavigation="True" CodeFile="Update-Temp-Reservation.aspx.vb"
    Inherits="SIAMAdmin.UpdateTempReservation" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>Allocate Class to Temporary Reservation</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <link href="includes/style.css" type="text/css" rel="stylesheet">
    <link href="includes/ServicesPLUS_style.css" rel="stylesheet" type="text/css" />

    <script language="javascript" src="includes/SiamAdmin.js" type="text/javascript"></script>

</head>
<body>
    <form id="Form1" method="post" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <table id="Table3" align="center" border="0" width="100%">
        <tr>
            <td class="PageHead" align="center" colspan="1" height="25" rowspan="1">
                Allocate Class to Temporary Reservation<br />
            </td>
        </tr>
        <tr>
            <td class="PageHead" style="height: 1px" align="left">
                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                    <ContentTemplate>
                        <asp:Label ID="ErrorLabel" runat="server" EnableViewState="False" ForeColor="Red"
                            CssClass="Body"></asp:Label><br /><br />
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnUpdate" EventName="Click" />
                        <asp:AsyncPostBackTrigger ControlID="ddlTrainingCourse" EventName="SelectedIndexChanged" />
                    </Triggers>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td valign="top" align="left" width="45%">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server" ChildrenAsTriggers="true">
                    <ContentTemplate>
                        <table id="Table2" width="100%" border="0">
                            <tr>
                                <td align="right" colspan="1" rowspan="1" width="20%">
                                    <asp:Label ID="labelCourseNumber" runat="server" CssClass="BodyCopy">Course</asp:Label>
                                </td>
                                <td style="height: 7px" width="30%">
                                    <asp:DropDownList ID="ddlTrainingCourse" runat="server"  CssClass="Body"
                                        Width="97%">
                                    </asp:DropDownList>
                                    
                                </td>
                                <td style="height: 7px" align="right" width="10%">
                                    <asp:Label ID="lblActivationCode0" runat="server" CssClass="BodyCopy">Location</asp:Label>
                                </td>
                                <td width="40%">
                                    <asp:DropDownList ID="ddlLocation" runat="server" CssClass="Body"
                                        Width="98%">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" align="right" width="20%">
                                    &nbsp;
                                </td>
                                <td width="30%">
                                    &nbsp;
                                </td>
                                <td width="10%">
                                    &nbsp;
                                </td>
                                <td width="40%" align="right">
                                    &nbsp;</td>
                            </tr>
                            <tr>
                                <td valign="top" align="right" width="20%">
                                    <asp:Label ID="lblClass" runat="server" CssClass="BodyCopy">Available Class</asp:Label>
                                </td>
                                <td align="left" width="30%">
                                    <asp:DropDownList ID="ddlTrainingClass" runat="server"  CssClass="Body"
                                        Width="97%">
                                    </asp:DropDownList>
                                </td>
                                <td align="right" width="10%">
                                    &nbsp;
                                </td>
                                <td align="right" width="40%">
                                    <asp:Button ID="btnSearch" runat="server" Text="Search Class" />
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" align="right" width="10%">
                                    &nbsp;
                                </td>
                                <td align="right" width="40%">
                                    &nbsp;
                                </td>
                                <td align="right" width="10%">
                                    &nbsp;
                                </td>
                                <td align="right" width="40%">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" align="right" width="10%">
                                    &nbsp;
                                </td>
                                <td colspan="3" align="left">
                                    <div style="overflow: auto; width: 100%; height: 200px">
                                        <asp:DataGrid ID="dgReservation" runat="server" PageSize="5000" AutoGenerateColumns="False"
                                            CellPadding="4" ForeColor="#333333" GridLines="None" Width="97%">
                                            <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                            <EditItemStyle BackColor="#999999" />
                                            <SelectedItemStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                            <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                                            <AlternatingItemStyle BackColor="White" ForeColor="#284775" />
                                            <ItemStyle BackColor="#F7F6F3" ForeColor="#333333" />
                                            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" CssClass="Body" ForeColor="White" />
                                            <Columns>
                                                <asp:TemplateColumn HeaderText="Select">
                                                    <ItemTemplate>
                                                        <asp:CheckBox runat="server" ID="chkSelect" Checked="false"></asp:CheckBox>
                                                    </ItemTemplate>
                                                    <HeaderStyle Width="10%" />
                                                </asp:TemplateColumn>
                                                <asp:BoundColumn DataField="ReservationNumber" HeaderText="Reservation Number">
                                                    <ItemStyle Font-Bold="False" CssClass="bodycopy" Font-Italic="False" Font-Overline="False"
                                                        Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Left" VerticalAlign="Middle" />
                                                    <HeaderStyle Width="20%" />
                                                </asp:BoundColumn>
                                                <asp:BoundColumn DataField="FirstName" HeaderText="Student First Name" Visible="True">
                                                    <ItemStyle Font-Bold="False" CssClass="bodycopy" Font-Italic="False" Font-Overline="False"
                                                        Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Left" VerticalAlign="Middle" />
                                                    <HeaderStyle Width="25%" />
                                                </asp:BoundColumn>
                                                <asp:BoundColumn DataField="LastName" HeaderText="Student Last Name">
                                                    <ItemStyle Font-Bold="False" CssClass="bodycopy" Font-Italic="False" Font-Overline="False"
                                                        Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Left" VerticalAlign="Middle" />
                                                    <HeaderStyle Width="25%" />
                                                </asp:BoundColumn>
                                                <asp:BoundColumn DataField="UPDATEDATE" HeaderText="Reservation Date" Visible="True">
                                                    <ItemStyle Font-Bold="False" CssClass="bodycopy" Font-Italic="False" Font-Overline="False"
                                                        Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Left" VerticalAlign="Middle" />
                                                    <HeaderStyle Width="20%" />
                                                </asp:BoundColumn>
                                            </Columns>
                                        </asp:DataGrid>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" align="right" width="10%">
                                    &nbsp;
                                </td>
                                <td colspan="3" align="left">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" align="right" width="10%">
                                    &nbsp;
                                </td>
                                <td colspan="3" align="right">
                                    <asp:Button ID="btnDeleteReservation" runat="server" Text="Delete Reservation"></asp:Button>
                                    &nbsp;
                                    <asp:Button ID="btnUpdate" runat="server" Text="Update Reservation" />
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnSearch" EventName="Click" />
                        <asp:AsyncPostBackTrigger ControlID="btnUpdate" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
