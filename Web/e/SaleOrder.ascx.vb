﻿Imports System.Security.Cryptography
Imports Sony.US.SIAMUtilities

Partial Class e_SaleOrder
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        txtYourReference.Text = GetUniqueKey()
        'Sasikumar WP SPLUS_WP014
        If Not IsPostBack Then
            txtSalesOrganization.Text = ConfigurationData.GeneralSettings.GlobalData.SalesOrganization.US
            txtDistributionChannel.Text = ConfigurationData.GeneralSettings.GlobalData.DistributionChannel.Value
            txtDivision.Text = ConfigurationData.GeneralSettings.GlobalData.Division.Value
            txtLanguage.Text = ConfigurationData.GeneralSettings.GlobalData.LanguagewM.US
        End If
    End Sub

    Private Function GetUniqueKey() As String
        Dim maxSize As Integer = 12

        Dim chars As Char() = New Char(61) {}
        Dim a As String
        a = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890"
        chars = a.ToCharArray()
        Dim size As Integer = maxSize
        Dim data As Byte() = New Byte(0) {}
        Dim crypto As New RNGCryptoServiceProvider()
        crypto.GetNonZeroBytes(data)
        size = maxSize
        data = New Byte(size - 1) {}
        crypto.GetNonZeroBytes(data)
        Dim result As New StringBuilder(size)
        For Each b As Byte In data
            result.Append(chars(b Mod (chars.Length - 1)))
        Next
        Return result.ToString()
    End Function
End Class
