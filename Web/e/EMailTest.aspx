<%@ Page Language="VB" AutoEventWireup="false" SmartNavigation="true" CodeFile="EMailTest.aspx.vb" Inherits="SIAMAdmin.EMailTest" %>
<%@ Reference Page="~/e/Utility.aspx" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>Email Test</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <link href="includes/style.css" type="text/css" rel="stylesheet">

    <script language="javascript" src="includes/SiamAdmin.js" type="text/javascript"></script>

</head>
<body bgcolor="#ffffff" class="Body">
    <form id="EmailTest" method="post" runat="server">
    <table align="center" border="0" width="100%">
        <tr>
            <td class="PageHead" align="center" colspan="1" height="30" rowspan="1">
                E-Mail Test
            </td>
        </tr>
        <tr bgcolor="#c9d5e6">
            <td align="left" height="20">
                &nbsp;<asp:Label ID="ErrorLabel" runat="server"  EnableViewState="False"
                    CssClass="redAsterick"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="center" height="50">
                &nbsp;
                <div id="div1" style="border-right: #c9d5e6 thin solid; width: 60%; border-top: #c9d5e6 thin solid;
                    border-left: #c9d5e6 thin solid; border-bottom: #c9d5e6 thin solid">
                    <table width="100%" border="0" cellspacing="2">
                        <tr>
                            <td align="left" width="30%">
                                <asp:Label ID="lblHost5" runat="server" CssClass="BodyHead" Text="Environment"></asp:Label>
                            </td>
                            <td width="80%">
                                <asp:RadioButtonList ID="rdoEnvironment" runat="server" AutoPostBack="True" 
                                    CssClass="bodyCopy" RepeatDirection="Horizontal">
                                    <asp:ListItem Selected="True">Environment</asp:ListItem>
                                    <asp:ListItem>Custom</asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" width="30%">
                                <asp:Label ID="lblSMTP" runat="server" CssClass="BodyHead" Text="SMTP"></asp:Label><br />
                            </td>
                            <td width="70%">
                                <SPS:SPSTextBox ID="txtSMTP" runat="server" CssClass="body" Width="100%" ReadOnly="True"></SPS:SPSTextBox>
                            </td>
                        </tr>
                        <tr bgcolor="#c9d5e6">
                            <td align="left">
                                <asp:Label ID="lblFrom" runat="server" CssClass="BodyHead" Text="From"></asp:Label>
                            </td>
                            <td align="left" valign="middle">
                                <SPS:SPSTextBox ID="txtFrom" runat="server" CssClass="body" Width="100%" MaxLength="4"
                                    ReadOnly="True"></SPS:SPSTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <asp:Label ID="lblTo" runat="server" CssClass="BodyHead" Text="To"></asp:Label>
                            </td>
                            <td align="left" valign="middle">
                                <SPS:SPSTextBox ID="txtTo" runat="server" CssClass="body" Width="100%" ReadOnly="True"></SPS:SPSTextBox>
                            </td>
                        </tr>
                        <tr bgcolor="#c9d5e6">
                            <td align="left">
                                <asp:Label ID="lblNumberOfEmail" runat="server" CssClass="BodyHead" Text="Number of Emails"></asp:Label>
                            </td>
                            <td align="left" valign="middle">
                                <SPS:SPSTextBox ID="txtNumberOfEmail" runat="server" CssClass="body" Width="60px"
                                    MaxLength="2" ReadOnly="True">30</SPS:SPSTextBox>
                                <asp:Label ID="errNumberOfMail" runat="server"  EnableViewState="False"
                    CssClass="redAsterick" Visible="False"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <asp:Label ID="lblEMailPerHour" runat="server" CssClass="BodyHead" Text="Email Per Hour"></asp:Label>
                            </td>
                            <td align="left" valign="middle">
                                <SPS:SPSTextBox ID="txtMailPerHour" runat="server" CssClass="body" Width="60px" ReadOnly="True">30</SPS:SPSTextBox>
                                <asp:Label ID="errEmailPerHour" runat="server"  EnableViewState="False"
                    CssClass="redAsterick" Visible="False"></asp:Label>
                            </td>
                        </tr>
                        <tr bgcolor="#c9d5e6">
                            <td align="left" valign="top">
                                <asp:Label ID="lblTestDuration" runat="server" CssClass="BodyHead" Text="Test Duration"></asp:Label>
                            </td>
                            <td align="left" valign="middle">
                                <asp:Label ID="lblTestdurationResult" runat="server" CssClass="BodyHead"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td style="height: 21px">
                                &nbsp;
                            </td>
                            <td style="height: 21px">
                                <asp:Button ID="btnSend" runat="server" Text="Start" CssClass="BodyHead" Width="100px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
