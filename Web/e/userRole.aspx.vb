Imports ServicesPlusException
Imports Sony.US.AuditLog
Imports Sony.US.ServicesPLUS.Data

Namespace SIAMAdmin

    Partial Class e_userRole
        Inherits UtilityClass

        Protected Sub cmdSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdSave.Click
            If cmdSave.Text = "New" Then
                txtRoleName.Text = ""
                chkActive.Checked = False
                cmdSave.Text = " Save "
                Exit Sub
            End If
            Dim objPCILogger As New PCILogger()
            Try
                'prasad added on 14-12-2009
                'Event logging code added for Add Role functionality. BugId #2100
                objPCILogger.EventOriginApplicationLocation = Me.Page.GetType().Name
                objPCILogger.EventOriginApplication = "ServicesPLUS Admin"
                objPCILogger.EventOriginMethod = "cmdSave_Click"
                objPCILogger.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
                objPCILogger.OperationalUser = Environment.UserName
                objPCILogger.EventType = EventType.Add
                If Not Session.Item("siamuser") Is Nothing Then
                    Dim adminUser = DirectCast(Session.Item("siamuser"), Sony.US.siam.User)
                    'objPCILogger.UserName = DirectCast(DirectCast(Session.Item("siamuser"), System.Object), Sony.US.siam.User).UserId
                    objPCILogger.EmailAddress = adminUser.EmailAddress '6524
                    objPCILogger.CustomerID = adminUser.CustomerID
                    objPCILogger.CustomerID_SequenceNumber = adminUser.SequenceNumber
                    objPCILogger.SIAM_ID = adminUser.Identity
                    objPCILogger.LDAP_ID = adminUser.AlternateUserId '6524
                End If
                objPCILogger.EventDateTime = DateTime.Now.ToLongTimeString()
                objPCILogger.HTTPRequestObjectValues = GetRequestContextValue()

                If ValidateUsrInput() = True Then
                    Dim oUsrRoleMgr As New UserRoleSecurityManager
                    Dim sRoleName As String = txtRoleName.Text.Trim()
                    Dim iStatus As Integer

                    If chkActive.Checked = True Then
                        iStatus = 0
                    Else
                        iStatus = 1
                    End If
                    Dim roleId As Integer
                    roleId = oUsrRoleMgr.AddUserRole(sRoleName, iStatus)
                    If roleId <> vbNull Then
                        ErrorLabel.Text = "Role added successfully"
                        cmdSave.Text = "New"
                        objPCILogger.IndicationSuccessFailure = "Success"
                        objPCILogger.Message = "Role Name " & sRoleName & " with Role ID " & roleId & " is added successfully."
                    Else
                        ErrorLabel.Text = "Error on adding new Role"
                        objPCILogger.IndicationSuccessFailure = "Failure"
                        objPCILogger.Message = "Error on adding new Role Name " & sRoleName
                    End If

                Else
                    objPCILogger.IndicationSuccessFailure = "Failure"
                    objPCILogger.Message = "Role Name is empty"
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                objPCILogger.IndicationSuccessFailure = "Failure"
                objPCILogger.Message = ex.Message.ToString()
            Finally
                objPCILogger.PushLogToMSMQ()
            End Try

        End Sub

        Protected Sub Page_Init1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
            MyBase.IsProtectedPage(True, True)
        End Sub

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        End Sub

        Private Function ValidateUsrInput() As Boolean
            If String.IsNullOrWhiteSpace(txtRoleName.Text) Then
                ErrorLabel.Text = "Please enter Role Name"
                Return False
            Else
                ErrorLabel.Text = ""
                Return True
            End If
        End Function
    End Class
End Namespace