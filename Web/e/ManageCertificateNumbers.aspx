﻿<%@ Reference Page="~/e/Utility.aspx" %>
<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ManageCertificateNumbers.aspx.vb" Inherits="SIAMAdmin.ManageCertificateNumbers" CodeFileBaseClass="SIAMAdmin.UtilityClass" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head runat="server">
    <title>Manage Certificate Numbers</title>
     <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
     <link href="includes/ServicesPLUS_style.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" language="javascript">
        function ConfirmDelete() {
            window.event.returnValue = confirm("This activity will delete selected mapping...");
        }
        window.parent.status = "Maintain Shipping Method is open.";
    </script>

</head>
<body>
    <form id="Form11" method="post" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <table width="100%" align="center" border="0">
        <tr>
            <td align="center" height="20">
                <asp:Label ID="lblHeader" CssClass="headerTitle" runat="server" >Maintain Service Agreement Registration Certificate Numbers</asp:Label>
            </td>
        </tr>
        <tr>
            <td height="15">
            </td>
        </tr>
        <tr>
            <td>
                <table border="0" width="100%" align="center">
                    <tr height="20">
                        <td align="left">
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
                                    <asp:Label ID="ErrorLabel" runat="server" CssClass="redAsterick" EnableViewState="False"></asp:Label>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="btnAddNew" EventName="Click" />
                                    <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />
                                    <asp:AsyncPostBackTrigger ControlID="btnCancel" EventName="Click" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:UpdatePanel ID="FieldUpdatePanel" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <div id="div1" style="border-right: #c9d5e6 thin solid; width: 100%; border-top: #c9d5e6 thin solid;
                                        border-left: #c9d5e6 thin solid; border-bottom: #c9d5e6 thin solid">
                                        <table width="100%" cellspacing="0">
                                            <tr bgcolor="#c9d5e6" height="20px">
                                                <td width="10%">
                                                    <asp:Label ID="lblProductCode"  CssClass="bodyCopyBold" runat="server">Product Code</asp:Label>
                                                </td>
                                                <td width="10%">
                                                    <SPS:SPSTextBox ID="txtProductCode" CssClass="bodyCopy" Width="80%" runat="server"
                                                        Text="" MaxLength="13"></SPS:SPSTextBox>
                                                    <span class="redAsterick">*</span>
                                                </td>
                                                <td width="12%">
                                                    <asp:Label ID="lblStarCertNumber" runat="server" CssClass="bodyCopyBold">Starting Certificate Number</asp:Label>
                                                </td>
                                                <td width="10%">
                                                    <SPS:SPSTextBox ID="txtStarCertNumber" runat="server" Width="80%" CssClass="bodyCopy"
                                                        Text="" MaxLength="5"></SPS:SPSTextBox>
                                                    <span class="redAsterick">*</span>
                                                </td>
                                                 <td width="12%">
                                                    <asp:Label ID="lblEndCertNumber" runat="server" CssClass="bodyCopyBold">Ending Certificate Number</asp:Label>
                                                </td>
                                                <td width="10%">
                                                    <SPS:SPSTextBox ID="txtEndCertNumber" runat="server" Width="80%" CssClass="bodyCopy"
                                                        Text="" MaxLength="5"></SPS:SPSTextBox>
                                                    <span class="redAsterick">*</span>
                                                </td>
                                            </tr>
                                            
                                            <tr height="20px">
                                                <td colspan="4">
                                                </td>
                                            </tr>
                                            <tr>
                                               <td colspan="6" align="right" width="25%">
                                                   <asp:Label ID="lblShippingMethodID" runat="server" Visible="false"></asp:Label>
                                                   <asp:Button ID="btnAddNew" runat="server" CssClass="bodyCopyBoldBtnNew" 
                                                       Text="Add New" Width="70px" />
                                                   &nbsp;&nbsp;&nbsp;
                                                   <asp:Button ID="btnSave" runat="server" CssClass="bodyCopyBoldBtnNew" 
                                                       Text="Save" Width="70px" />
                                                   &nbsp;&nbsp;&nbsp;
                                                   <asp:Button ID="btnCancel" runat="server" CssClass="bodyCopyBoldBtnNew" 
                                                       Text="Cancel" Width="70px" />
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="dgShippingMethod" EventName="ItemCommand" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:UpdatePanel ID="GridUpdatePanel" runat="server" ChildrenAsTriggers="true">
                                <ContentTemplate>
                                    <asp:DataGrid ID="dgShippingMethod" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                        PageSize="8" Width="100%">
                                        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                        <EditItemStyle BackColor="#999999" />
                                        <SelectedItemStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                        <PagerStyle BackColor="#284775" CssClass=bodyCopy ForeColor="White" HorizontalAlign="Center"
                                            Mode="NumericPages" />
                                        <AlternatingItemStyle BackColor="White" ForeColor="#284775" CssClass="bodyCopy" />
                                        <ItemStyle BackColor="#F7F6F3" ForeColor="#333333" CssClass="bodyCopy" />
                                        <HeaderStyle BackColor="#284775" CssClass="tableHeader" ForeColor="White" />
                                        <Columns>
                                            <asp:BoundColumn DataField="ProductCode" HeaderText="Product Code">
                                                <HeaderStyle Width="12%" />
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="STARTINGCERTNUM" HeaderText="STARTING CERTIFICATE NUMBER ">
                                               <HeaderStyle Width="35%" HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="ENDINGCERTNUM" HeaderText="ENDING CERTIFICATE NUMBER ">
                                                <HeaderStyle Width="35%" HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:BoundColumn>                                           
                                            <asp:TemplateColumn>
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkBtnEdit" runat="server" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "ProductCode")%>'
                                                        CommandName="Edit" CssClass="bodyCopySM" Text="Edit">
                                                    </asp:LinkButton>
                                                    <itemstyle cssclass="bodycopy" horizontalalign="Center" verticalalign="Middle" />
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn>
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkBtnDelete" runat="server" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "ProductCode")%>'
                                                        CommandName="Delete" CssClass="bodyCopySM" Text="Delete">
                                                    </asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:BoundColumn DataField="ProductCode" Visible="False"></asp:BoundColumn>
                                            
                                        </Columns>
                                    </asp:DataGrid>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="btnAddNew" EventName="Click" />
                                    <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />
                                    <asp:AsyncPostBackTrigger ControlID="btnCancel" EventName="Click" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <input type="hidden" id="hdnLastOpr" runat="server" />
    </form>
</body>
</html>
