Imports System.Collections.Specialized
Imports Sony.US.siam, Sony.US.ServicesPLUS.Controls
Imports System.Drawing
Imports System.Data
Imports Sony.US.SIAMUtilities
Imports Sony.US.AuditLog
Imports ServicesPlusException

Namespace SIAMAdmin
    Public Class UtilityClass
        Inherits System.Web.UI.Page

#Region "   Enums       "
        Public Enum SortDirection
            ASC = 0
            DESC = 1
        End Enum

        Public Enum enumRoles
            CustomerService = 0
            SiamAdministrator = 1
            AccountValidators = 2
            TrainingAdmin = 3
        End Enum
#End Region

#Region "   Page Members    "
        Private thisStyleDefinition As StyleDefinition
        Public Const RedirectPage As String = "NotLoggedIn.aspx"
        Public Const EntryPointPage As String = "default.aspx"
        Public Const SM_DefaultPage As String = "users.aspx"
        Public Const CustomerSessionVariable As String = "TheCurrentUser"
        Public Const LogoutSessionVariable As String = "logout"
        Public Const LoggedInSessionVariable As String = "loggedin"
        Public Const CurrentUserSessionVariables As String = "TheCurrentUser"
        Protected Const NullDateTime = "01/01/0001"
        'Dim objUtilties As Utilities = New Utilities
#End Region

#Region " Web Form Designer Generated Code "
        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

#Region "   Events  "

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                If Not Session("unmnt") Is Nothing Then
                    If Session("unmnt").ToString() <> ConfigurationData.GeneralSettings.UnderMaintenance Then
                        Session.Remove("unmnt")
                        Session.Add("unmnt", ConfigurationData.GeneralSettings.UnderMaintenance)
                        Page.ClientScript.RegisterStartupScript(Me.GetType(), "LoadFrame", "<script language=""javascript"">window.parent.location = 'default.aspx';</script>")
                        Exit Sub
                    End If
                Else
                    If ConfigurationData.GeneralSettings.UnderMaintenance = "Y" Then
                        Session.Add("unmnt", ConfigurationData.GeneralSettings.UnderMaintenance)
                        Page.ClientScript.RegisterStartupScript(Me.GetType(), "LoadFrame", "<script language=""javascript"">window.parent.location = 'default.aspx';</script>")
                        Exit Sub
                    End If
                End If

                If Not Request.Cookies("Siamrequestkey") Is Nothing Then
                    Dim strRequestKey As String
                    strRequestKey = Request.Cookies("Siamrequestkey").Value.ToString()
                    If Not Session.Item("SIAMRequestKey") Is Nothing Then
                        Request.Cookies.Remove("Siamrequestkey")
                        If strRequestKey <> Session.Item("SIAMRequestKey").ToString() Then
                            SetUniqueRequestKey()
                            'Response.Redirect("default.aspx")
                            Page.ClientScript.RegisterStartupScript(Me.GetType(), "LoadFrame", "<script language=""javascript"">window.parent.location = 'default.aspx';</script>")
                        End If
                    Else
                        Request.Cookies.Remove("Siamrequestkey")
                        SetUniqueRequestKey()
                        'Response.Redirect("default.aspx")
                        Page.ClientScript.RegisterStartupScript(Me.GetType(), "LoadFrame", "<script language=""javascript"">window.parent.location = 'default.aspx';</script>")
                    End If
                End If
                SetUniqueRequestKey()
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub
        Private Sub SetUniqueRequestKey()
            Dim strRequestKey As String
            Dim objCookie As HttpCookie
            strRequestKey = System.Guid.NewGuid().ToString()
            objCookie = New HttpCookie("Siamrequestkey", strRequestKey)
            Response.Cookies.Add(objCookie)
            Session.Add("SIAMRequestKey", strRequestKey)

            If ConfigurationData.GeneralSettings.NOCACHPAGE.IndexOf(Me.GetType().Name.ToUpper) < 0 Then
                Response.Buffer = True
                Response.ExpiresAbsolute = Now().Subtract(New TimeSpan(1, 0, 0, 0))
                Response.Expires = 0
                Response.CacheControl = "no-cache"
            End If
        End Sub
#End Region

#Region "   Methods     "
        Public Function GetServerVariableValue(ByVal Key As String) As String
			'Dim loop1, loop2 As Integer
			'Dim arr1(), arr2() As String
			'Dim coll As NameValueCollection
			'Dim value As String = String.Empty
			'' Load ServerVariable collection into NameValueCollection object.
			'coll = Request.ServerVariables
			'' Get names of all keys into a string array.
			'arr1 = coll.AllKeys
			'For loop1 = 0 To arr1.GetUpperBound(0)
			'    If Key = arr1(loop1) Then
			'        arr2 = coll.GetValues(loop1) ' Get all values under this key.
			'        For loop2 = 0 To arr2.GetUpperBound(0)
			'            value = Server.HtmlEncode(arr2(loop2))
			'            Return value
			'            Exit Function
			'        Next loop2
			'    End If
			'Next loop1
			'Return value
			If (Request.ServerVariables(Key) IsNot Nothing) Then
				Return Request.ServerVariables(Key)
			Else
				Return String.Empty
			End If
        End Function

        Public Sub IsProtectedPage(ByVal isProtected As Boolean)
            If Request.UrlReferrer Is Nothing Then
                If (Me.GetType().BaseType.Name <> "_default") Then
                    Response.Redirect("default.aspx")
                End If
            Else
                If isProtected Then
                    If Session.Item(CustomerSessionVariable) Is Nothing Then
                        Page.ClientScript.RegisterStartupScript(Me.GetType(), "LoadParentPage", "<script language=""javascript"">window.parent.location='redirect.aspx';</script>")
                        'Response.Redirect(RedirectPage)
                    End If
                End If
            End If
        End Sub

        Public Sub IsProtectedPage(ByVal isProtected As Boolean, ByVal IsInIForm As Boolean)
            'If (Request.UrlReferrer Is Nothing And (Request.AppRelativeCurrentExecutionFilePath <> "~/e/PrintException.aspx" And Request.AppRelativeCurrentExecutionFilePath <> "~/e/training-participant-report.aspx")) = True Then
            '    If (Me.GetType().BaseType.Name <> "_default") Then
            '        Response.Redirect("default.aspx")
            '    End If
            'Else
            If isProtected Then
                If Session.Item(CustomerSessionVariable) Is Nothing Or Not TypeOf Session.Item(CustomerSessionVariable) Is User Then
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "LoadParentPage", "<script language=""javascript"">window.parent.location='redirect.aspx';</script>")
                End If
            End If
            'End If
        End Sub

        Public Function RecoverQueryString() As String
            Dim queryString As String = ""
            Dim nIndex As Integer
            nIndex = Me.Request.RawUrl().IndexOf("?")
            If nIndex >= 0 Then
                queryString = Me.Request.RawUrl().Remove(0, nIndex) 'including &
            End If
            'Dim coll As NameValueCollection = Me.Request.QueryString()
            'For nIndex = 0 To coll.Count
            'If nIndex > 0 Then
            'queryString = queryString + "&"
            'End If
            'queryString = queryString + coll.Keys(nIndex) + "=" + coll.Item(nIndex)
            'Next

            Return queryString
        End Function

        Protected Function FormatDate(ByRef dateTime As DateTime) As String
            If dateTime = NullDateTime Then
                Return "TBD"
            Else
                Return dateTime.ToString("MM/dd/yyyy")
            End If
        End Function

        Public Function GetStyle(ByVal ItemType As ListItemType) As Style
            If IsNothing(thisStyleDefinition) Then thisStyleDefinition = buildStyles()

            Return thisStyleDefinition.GetStyle(ItemType)
        End Function

        Public Function GetStyle(ByVal Width As Integer) As Style
            Dim thisStyle As New Style

            thisStyle.BorderStyle = BorderStyle.Solid
            thisStyle.BorderWidth = Unit.Pixel(2)
            thisStyle.BackColor = Color.Gainsboro
            thisStyle.ForeColor = Color.GhostWhite
            If Width = 0 Then
                thisStyle.Width = Unit.Percentage(100)
            Else
                thisStyle.Width = Unit.Pixel(Width)
            End If

            thisStyle.BorderColor = Color.Black

            Return thisStyle
        End Function

        Private Function buildStyles() As StyleDefinition
            Dim thisStyleDef As New StyleDefinition
            Dim thisStyle As Style

            '-- item style
            thisStyle = New Style
            thisStyle.BackColor = Color.SlateGray
            thisStyle.ForeColor = Color.GhostWhite
            thisStyle.CssClass = "BodyClass"

            thisStyleDef.AddStyle(thisStyle, ListItemType.Item)


            '-- alternate style
            thisStyle = New Style
            thisStyle.BackColor = Color.LightSlateGray
            thisStyle.ForeColor = Color.GhostWhite
            thisStyle.CssClass = "BodyClass"

            thisStyleDef.AddStyle(thisStyle, ListItemType.AlternatingItem)


            '-- pager style
            thisStyle = New Style
            thisStyle.BackColor = Color.DarkGray
            thisStyle.ForeColor = Color.GhostWhite
            thisStyle.Font.Size = FontUnit.Point(10)
            thisStyle.Font.Name = "Arial"
            thisStyle.Height = Unit.Pixel(10)

            thisStyleDef.AddStyle(thisStyle, ListItemType.Pager)


            '-- header style
            thisStyle = New Style
            thisStyle.BackColor = Color.FromKnownColor(KnownColor.ControlDarkDark)
            thisStyle.ForeColor = Color.GhostWhite
            thisStyle.CssClass = "BodyClass"
            thisStyle.Height = Unit.Pixel(25)
            thisStyle.Font.Size = FontUnit.Point(14)
            thisStyle.Font.Name = "Arial"


            Dim thisTW As System.IO.TextWriter
            Dim thisWriter As New HtmlTextWriter(thisTW)
            thisWriter.AddAttribute("Mode", "NumericPages")
            thisStyle.AddAttributesToRender(thisWriter)
            thisStyleDef.AddStyle(thisStyle, ListItemType.Header)

            '-- Separator style
            thisStyle = New Style
            thisStyle.BackColor = Color.GhostWhite
            thisStyle.ForeColor = Color.GhostWhite
            thisStyle.CssClass = "BodyClass"
            thisStyle.Height = Unit.Pixel(10)

            thisStyleDef.AddStyle(thisStyle, ListItemType.Separator)

            Return thisStyleDef
        End Function

        Private Function getUsersRole(ByRef thisUser As User) As enumRoles
            Dim returnValue As enumRoles = enumRoles.AccountValidators

            Try
                For Each thisRole As Role In thisUser.Roles
                    Select Case thisRole.Name
                        Case enumRoles.SiamAdministrator.ToString() ' siam admin role                        
                            returnValue = enumRoles.SiamAdministrator

                        Case enumRoles.CustomerService.ToString()   ' customer service role                        
                            If Not returnValue = enumRoles.SiamAdministrator Then returnValue = enumRoles.CustomerService

                        Case enumRoles.AccountValidators.ToString() ' account validator role

                    End Select
                Next
            Catch ex As Exception
                Throw New Exception("Error getting users role. " + ex.Message.ToString())
            End Try

            Return returnValue
        End Function

        Public Sub populateStateDropdownList(ByRef thisDDL As DropDownList)
            If thisDDL Is Nothing Then Exit Sub

            If (thisDDL.Items.FindByText("Select State") Is Nothing) Then
                thisDDL.Items.Add(New ListItem("Select State", ""))
            End If
            thisDDL.Items.Add(New ListItem("Alabama", "AL"))
            thisDDL.Items.Add(New ListItem("Alaska", "AK"))
            thisDDL.Items.Add(New ListItem("Arizona", "AZ"))
            thisDDL.Items.Add(New ListItem("Arkansas", "AR"))
            thisDDL.Items.Add(New ListItem("California", "CA"))
            thisDDL.Items.Add(New ListItem("Colorado", "CO"))
            thisDDL.Items.Add(New ListItem("Connecticut", "CT"))
            thisDDL.Items.Add(New ListItem("Delaware", "DE"))
            thisDDL.Items.Add(New ListItem("Florida", "FL"))
            thisDDL.Items.Add(New ListItem("Georgia", "GA"))
            thisDDL.Items.Add(New ListItem("Guam", "GU"))
            thisDDL.Items.Add(New ListItem("Hawaii", "HI"))
            thisDDL.Items.Add(New ListItem("Idaho", "ID"))
            thisDDL.Items.Add(New ListItem("Illinois", "IL"))
            thisDDL.Items.Add(New ListItem("Indiana", "IN"))
            thisDDL.Items.Add(New ListItem("Iowa", "IA"))
            thisDDL.Items.Add(New ListItem("Kansas", "KS"))
            thisDDL.Items.Add(New ListItem("Kentucky", "KY"))
            thisDDL.Items.Add(New ListItem("Louisiana", "LA"))
            thisDDL.Items.Add(New ListItem("Maine", "ME"))
            thisDDL.Items.Add(New ListItem("Maryland", "MD"))
            thisDDL.Items.Add(New ListItem("Massachusetts", "MA"))
            thisDDL.Items.Add(New ListItem("Michigan", "MI"))
            thisDDL.Items.Add(New ListItem("Minnesota", "MN"))
            thisDDL.Items.Add(New ListItem("Mississippi", "MS"))
            thisDDL.Items.Add(New ListItem("Missouri", "MO"))
            thisDDL.Items.Add(New ListItem("Montana", "MT"))
            thisDDL.Items.Add(New ListItem("Nebraska", "NE"))
            thisDDL.Items.Add(New ListItem("Nevada", "NV"))
            thisDDL.Items.Add(New ListItem("New Hampshire", "NH"))
            thisDDL.Items.Add(New ListItem("New Jersey", "NJ"))
            thisDDL.Items.Add(New ListItem("New Mexico", "NM"))
            thisDDL.Items.Add(New ListItem("New York", "NY"))
            thisDDL.Items.Add(New ListItem("North Carolina", "NC"))
            thisDDL.Items.Add(New ListItem("North Dakota", "ND"))
            thisDDL.Items.Add(New ListItem("Ohio", "OH"))
            thisDDL.Items.Add(New ListItem("Oklahoma", "OK"))
            thisDDL.Items.Add(New ListItem("Oregon", "OR"))
            thisDDL.Items.Add(New ListItem("Pennsylvania", "PA"))
            thisDDL.Items.Add(New ListItem("Puerto Rico", "PR"))
            thisDDL.Items.Add(New ListItem("Rhode Island", "RI"))
            thisDDL.Items.Add(New ListItem("South Carolina", "SC"))
            thisDDL.Items.Add(New ListItem("South Dakota", "SD"))
            thisDDL.Items.Add(New ListItem("Tennessee", "TN"))
            thisDDL.Items.Add(New ListItem("Texas", "TX"))
            thisDDL.Items.Add(New ListItem("Utah", "UT"))
            thisDDL.Items.Add(New ListItem("Vermont", "VT"))
            thisDDL.Items.Add(New ListItem("Virgin Islands", "VI"))
            thisDDL.Items.Add(New ListItem("Virginia", "VA"))
            thisDDL.Items.Add(New ListItem("Washington", "WA"))
            thisDDL.Items.Add(New ListItem("Washington D.C.", "DC"))
            thisDDL.Items.Add(New ListItem("West Virginia", "WV"))
            thisDDL.Items.Add(New ListItem("Wisconsin", "WI"))
            thisDDL.Items.Add(New ListItem("Wyoming", "WY"))
        End Sub

        Public Sub populateProvinceDropdownList(ByRef thisDDL As DropDownList)
            If thisDDL Is Nothing Then Exit Sub

            If (thisDDL.Items.FindByText("Select State") Is Nothing) Then
                thisDDL.Items.Add(New ListItem("Select State", ""))
            End If
            thisDDL.Items.Add(New ListItem("Alberta", "AB"))
            thisDDL.Items.Add(New ListItem("British Columbia", "BC"))
            thisDDL.Items.Add(New ListItem("Manitoba", "MB"))
            thisDDL.Items.Add(New ListItem("New Brunswick", "NB"))
            thisDDL.Items.Add(New ListItem("Newfoundland", "NL"))
            thisDDL.Items.Add(New ListItem("Northwest Territories", "NT"))
            thisDDL.Items.Add(New ListItem("Nova Scotia", "NS"))
            thisDDL.Items.Add(New ListItem("Nunavut", "NU"))
            thisDDL.Items.Add(New ListItem("Ontario", "ON"))
            thisDDL.Items.Add(New ListItem("Prince Edward Island", "PE"))
            thisDDL.Items.Add(New ListItem("Quebec", "QC"))
            thisDDL.Items.Add(New ListItem("Saskatchewan", "SK"))
            thisDDL.Items.Add(New ListItem("Yukon", "YT"))

        End Sub

        'This Function is used to log the details in the ServicesPLUS Admin
        Public Function GetRequestContextValue() As Object
            Dim objHTTPRequestObject As HTTPRequestObject = New HTTPRequestObject()

            objHTTPRequestObject.HTTP_X_Forward_For = GetServerVariableValue("HTTP_X_FORWARDED_FOR")
            objHTTPRequestObject.RemoteADDR = GetServerVariableValue("REMOTE_ADDR")
            objHTTPRequestObject.HTTPReferer = GetServerVariableValue("HTTP_REFERER")
            objHTTPRequestObject.HTTPURL = GetServerVariableValue("URL")

            If objHTTPRequestObject.HTTPURL = "" Then
                objHTTPRequestObject.HTTPURL = GetServerVariableValue("HTTP_URL")
            End If

            objHTTPRequestObject.HTTPUserAgent = GetServerVariableValue("HTTP_USER_AGENT")
            Return objHTTPRequestObject

        End Function
#End Region

#Region "   Style Definition Class  "
        Public Class StyleDefinition

#Region "   Construtors     "

            Public Sub New()
                thisArrayListOfStyle = New ArrayList
            End Sub

#End Region


#Region "   Data Members    "

            Private thisReturnStyle As Style
            Private thisArrayListOfStyle As ArrayList

#End Region


#Region "   Functions   "


            Public Function GetStyle(ByVal thisType As ListItemType)
                thisReturnStyle = Nothing
                For Each thisExistingStyle As StyleDefinitionTemplate In thisArrayListOfStyle
                    If thisExistingStyle.ListItemType = thisType Then
                        '-- found style object so return it to caller
                        thisReturnStyle = thisExistingStyle.Style
                    End If
                Next

                Return thisReturnStyle
            End Function

            Public Sub AddStyle(ByVal thisStyleObject As Style, ByVal thisType As ListItemType)
                '-- make sure we have a style object
                If Not IsNothing(thisStyleObject) Then
                    Dim bolStyleExisted As Boolean = False

                    For Each thisExistingStyle As StyleDefinitionTemplate In thisArrayListOfStyle
                        If thisExistingStyle.ListItemType = thisType Then
                            '-- style already exit for type so replace the existing style object with the passed style object
                            thisExistingStyle = New StyleDefinitionTemplate(thisStyleObject, thisType)
                            bolStyleExisted = True
                        End If
                    Next

                    If Not bolStyleExisted Then thisArrayListOfStyle.Add(New StyleDefinitionTemplate(thisStyleObject, thisType))
                End If
            End Sub

#End Region


#Region "       Style definition template sub class  "
            '-- sub class of style definition
            Private Class StyleDefinitionTemplate
                Private thisListItem As ListItemType
                Private thisStyle As Style

                Public Sub New(ByVal styleObject As Style, ByVal listItem As ListItemType)
                    thisStyle = styleObject
                    thisListItem = listItem
                End Sub


                Public Property Style() As Style
                    Get
                        Return thisStyle
                    End Get
                    Set(ByVal Value As Style)
                        thisStyle = Value
                    End Set
                End Property

                Public ReadOnly Property ListItemType() As ListItemType
                    Get
                        Return thisListItem
                    End Get
                End Property

            End Class
#End Region

        End Class
#End Region

#Region "XSLT Date Time Class"
        Public Class XsltDateTime
            Protected m_strFormat As String = "MM/dd/yyyy"
            Public Sub New()
            End Sub
            Public Sub New(ByRef strFormat As String)
                m_strFormat = strFormat
            End Sub
            Public Function Format(ByRef strDateTime As String) As String
                Dim dateTime As DateTime = Convert.ToDateTime(strDateTime)
                Return dateTime.ToString(m_strFormat)
            End Function
        End Class
#End Region


    End Class
End Namespace


