Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Data
Imports Sony.US.ServicesPLUS.Process

Imports System.Text


Namespace SIAMAdmin

Partial Class AccountPayment
    Inherits System.Web.UI.UserControl
    Implements IAccountPayment

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private cm As New CustomerManager
    Protected m_order As Sony.US.ServicesPLUS.Core.Order
    Private current_bill_to_list() As Account
    Private current_bill_to As Account

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region


        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            If Not IsPostBack Then


                Dim first_one As Boolean = True
                Dim sap_accounts As Boolean = False

                '-- populate ddl with SAP accounts
                Try
                    'populate with SAP accounts if present
                    For Each a As Account In m_order.Customer.SAPBillToAccounts
                        'If a.Validated Then '6865
                        If m_order.Customer.UserType = "A" Then '6865
                            'sap_accounts = True
                            'Throw New NotImplementedException("Still SIS is in use")
                            'get all address details per sap bill_to - bs 03/01/2004
                            'sis.SAPPayorInquiry(a, True)
                            Dim thisSB As New StringBuilder
                            ''changed for bug 6219
                            a = cm.PopulateCustomerDetailWithSAPAccount(a)
                            If Not a.AccountNumber Is Nothing Then thisSB.Append(a.AccountNumber + "-")
                            If Not a.Address Is Nothing Then
                                If Not a.Address.Name Is Nothing Then thisSB.Append(" " + a.Address.Name)
                                If Not a.Address.Line1 Is Nothing Then thisSB.Append(" " + a.Address.Line1)
                                If Not a.Address.Line2 Is Nothing Then thisSB.Append(" " + a.Address.Line2)
                                If Not a.Address.Line3 Is Nothing Then thisSB.Append(" " + a.Address.Line3)
                                If Not a.Address.Line4 Is Nothing Then thisSB.Append(" " + a.Address.Line4)
                            End If

                            Me.ddlAccntNumber.Items.Add(New ListItem(thisSB.ToString(), a.AccountNumber))
                        End If
                    Next
                    Session.Add("current_bill_to_list", m_order.Customer.SAPBillToAccounts)
                    'For Each objLoopAccount As Account In Customer.SAPBillToAccounts
                    '    objLoopAccount = cm.PopulateCustomerDetailWithSAPAccount(objLoopAccount.AccountNumber)
                    '    If Not objLoopAccount.AccountNumber Is Nothing Then thisSB.Append(objLoopAccount.AccountNumber.ToString() + "-")
                    '    If Not objLoopAccount.Address Is Nothing Then
                    '        If Not objLoopAccount.Address.Name Is Nothing Then thisSB.Append(" " + objLoopAccount.Address.Name.ToString())
                    '        If Not objLoopAccount.Address.Line1 Is Nothing Then thisSB.Append(" " + objLoopAccount.Address.Line1.ToString())
                    '        If Not objLoopAccount.Address.Line2 Is Nothing Then thisSB.Append(" " + objLoopAccount.Address.Line2.ToString())
                    '    End If
                    '    DropDownList2.Items.Add(New ListItem(thisSB.ToString(), objLoopAccount.AccountNumber))
                    'Next


                Catch ex As Exception
                    Throw New Exception("Error building SAP accounts. " + ex.Message.ToString())
                End Try

                '-- populate ddl with SIS accounts
                'Try
                '    If ddlAccntNumber.Items.Count = 0 Then
                '        For Each b As SISAccount In m_order.Customer.SISLegacyBillToAccounts
                '            cm.PopulateLegacyAccount(b) 'Changes done by Kannapiran towards Bug# 1212

                '            Dim thisSB As New StringBuilder
                '            If Not b.AccountNumber Is Nothing Then thisSB.Append(b.AccountNumber.ToString() + "-")
                '            If Not b.Address Is Nothing Then
                '                If Not b.Address.Name Is Nothing Then thisSB.Append(" " + b.Address.Name.ToString())
                '                If Not b.Address.Line1 Is Nothing Then thisSB.Append(" " + b.Address.Line1.ToString())
                '                If Not b.Address.Line2 Is Nothing Then thisSB.Append(" " + b.Address.Line2.ToString())
                '                If Not b.Address.Line3 Is Nothing Then thisSB.Append(" " + b.Address.Line3.ToString())
                '                If Not b.Address.Line4 Is Nothing Then thisSB.Append(" " + b.Address.Line4.ToString())
                '            End If

                '            Me.ddlAccntNumber.Items.Add(New ListItem(thisSB.ToString(), b.AccountNumber))
                '        Next
                '        Session.Add("current_bill_to_list", m_order.Customer.SISLegacyBillToAccounts)
                '    Else
                '        'if some weren't validated then would have an issue with this
                '        'customer service should make sure all valid or all not valid
                '        Session.Add("current_bill_to_list", m_order.Customer.SAPBillToAccounts)
                '    End If
                'Catch ex As Exception
                '    Throw New Exception("Error building SIS accounts. " + ex.Message.ToString())
                'End Try

                Me.ddlAccntNumber.SelectedValue = m_order.SAPBillToAccountNumber
                ddlAccntNumber_SelectedIndexChanged(Nothing, Nothing) 'fill in the bill to info
            End If
        End Sub

    Public Property Order() As Sony.US.ServicesPLUS.Core.Order Implements IAccountPayment.Order
        Get
            Return m_order
        End Get
        Set(ByVal Value As Sony.US.ServicesPLUS.Core.Order)
            m_order = Value
        End Set
    End Property

    Private Sub ddlAccntNumber_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlAccntNumber.SelectedIndexChanged
        'set current_bill_to 
            If (ddlAccntNumber.SelectedIndex >= 0) Then

                current_bill_to_list = Session.Item("current_bill_to_list")
                current_bill_to = CType(current_bill_to_list.GetValue(ddlAccntNumber.SelectedIndex()), Account)
                Session.Add("current_trainingbill_to", current_bill_to)
                Me.labelName().Text = current_bill_to.Address.Name
                Me.labelStreet1.Text = current_bill_to.Address.Line1
                Me.labelStreet2.Text = current_bill_to.Address.Line2
                Me.labelStreet3.Text = current_bill_to.Address.Line3
                Me.lblAddress4.Text = current_bill_to.Address.Line4
                Me.labelState.Text = current_bill_to.Address.State
                Me.labelZip.Text = current_bill_to.Address.PostalCode

            End If
    End Sub

    Function Save(Optional ByRef customer As Customer = Nothing) As Boolean Implements IAccountPayment.Save
        If Not ValidateForm() Then
            Return False
        End If

        current_bill_to = Session.Item("current_trainingbill_to")
        m_order.SAPBillToAccountNumber = current_bill_to.AccountNumber
        m_order.BillTo = current_bill_to.Address
        Return True
    End Function
    Private Function ValidateForm() As Boolean
        Return True
    End Function

    Private Function ProcessAddressCity(ByRef addressLine As String) As String
        Dim nIndex = addressLine.IndexOf(",")
        If (nIndex >= 0) Then
            Return addressLine.Substring(0, nIndex)
        Else
            Return ""
        End If
    End Function
End Class

End Namespace
