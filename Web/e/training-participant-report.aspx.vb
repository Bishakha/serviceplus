Imports System.Data
Imports CrystalDecisions.Shared
Imports ServicesPlusException
Imports Sony.US.AuditLog
Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Data

Namespace SIAMAdmin
    Partial Class training_participant_report
        Inherits UtilityClass
        'Dim objUtilties As Utilities = New Utilities

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private rd As CrystalDecisions.CrystalReports.Engine.ReportDocument
        Private intReportType As Int16 = 0

#End Region

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            MyBase.IsProtectedPage(True, True)
            InitializeComponent()
        End Sub


        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            If (Request.QueryString("type") IsNot Nothing) Then
                intReportType = Convert.ToInt16(Request.QueryString("type"))
            End If

            Try
                BindReport()
            Catch thrdEx As Threading.AbandonedMutexException
                ' do nothing
            Catch ex As Exception
                errorMessageLabel.Text = Utilities.WrapExceptionforUI(ex)
                errorMessageLabel.Visible = True
            End Try
            'End If
        End Sub

        Private Sub btnExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExport.Click
            Dim objPCILogger As New PCILogger() '6524
            Try
                objPCILogger.EventOriginApplication = "ServicesPLUS"
                objPCILogger.EventOriginApplicationLocation = Me.Page.GetType().Name
                objPCILogger.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
                objPCILogger.OperationalUser = Environment.UserName
                objPCILogger.EventDateTime = DateTime.Now.ToLongTimeString()
                objPCILogger.HTTPRequestObjectValues = objPCILogger.GetRequestContextValue()
                objPCILogger.IndicationSuccessFailure = "Success"
                objPCILogger.EventOriginMethod = "btnExport_Click"
                objPCILogger.Message = "Export Success."
                objPCILogger.EventType = EventType.Others

                BindReport()
                'Dim crExportOptions As ExportOptions
                Dim Fname As String = Server.MapPath("ReportTemp") & "/" & Session.SessionID.ToString & ".xls"
                Dim crDiskFileDestinationOptions As New DiskFileDestinationOptions With {
                    .DiskFileName = Fname
                }
                rd.ExportOptions.DestinationOptions = crDiskFileDestinationOptions
                rd.ExportOptions.ExportDestinationType = ExportDestinationType.DiskFile
                rd.ExportOptions.ExportFormatType = ExportFormatType.Excel
                rd.Export()
                ' The following code writes the xsl file 
                ' to the Client�s browser.
                Response.ClearContent()
                Response.ClearHeaders()
                Response.ContentType = "application/x-msexcel"
                Response.WriteFile(Fname)
                Response.Flush()
                Response.Close()

                ' delete the exported file from disk
                'System.IO.File.Delete(Fname)
            Catch ex As Exception
                errorMessageLabel.Text = Utilities.WrapExceptionforUI(ex)
                errorMessageLabel.Visible = True
                objPCILogger.IndicationSuccessFailure = "Failure" '6524
                objPCILogger.Message = "Export Failed. " & ex.Message.ToString() '6524
            Finally
                objPCILogger.PushLogToMSMQ() '6524
            End Try
        End Sub

        Private Sub BindReport()
            If (Request.QueryString("LineNo") IsNot Nothing) And (Session.Item("searchedClass") IsNot Nothing) Then
                Dim lineNumber As Integer = Convert.ToInt16(Request.QueryString("LineNo"))
                Dim courseSelected As CourseSchedule = CType(Session.Item("searchedClass"), Array)(lineNumber)
                Dim cdm = New CourseDataManager
                Dim ds As DataSet = Nothing

                rd = New CrystalDecisions.CrystalReports.Engine.ReportDocument()
                If intReportType = 1 Then 'Course
                    ds = cdm.GetCourseParticipants(courseSelected.Course.Number, "", courseSelected.Location.Code, Convert.ToInt16(Request.QueryString("hide")), Convert.ToInt16(Request.QueryString("delstatus"))) 'return all status
                    rd.Load(Server.MapPath("Reports\rptCourseParticipantReport.rpt"))
                    courseParticipantCRV.DisplayGroupTree = True
                ElseIf intReportType = 2 Then 'PartNumber
                    ds = cdm.GetClassParticipants(courseSelected.PartNumber, "", courseSelected.Location.Code, Convert.ToInt16(Request.QueryString("hide")), Convert.ToInt16(Request.QueryString("delstatus"))) 'return all status
                    rd.Load(Server.MapPath("Reports\rptCourseParticipantReport.rpt"))
                    'rd.Load(Server.MapPath("Reports\rptClassParticipantReport.rpt"))
                End If
                If ds?.Tables(0)?.Rows?.Count > 0 Then
                    rd.SetDataSource(ds.Tables(0))
                    courseParticipantCRV.ReportSource = rd
                    courseParticipantCRV.DataBind()
                Else
                    errorMessageLabel.Visible = True
                    errorMessageLabel.Text = "No Record Found"
                End If
            End If
        End Sub

    End Class

End Namespace
