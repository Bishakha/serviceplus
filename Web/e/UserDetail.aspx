<%@ Reference Page="~/e/Utility.aspx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SIAMAdmin.UserDetail" CodeFile="UserDetail.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>UserDetail</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<link rel="stylesheet" type="text/css" href="includes/style.css">
		<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<table height="90%" width="771" border="0">
				<tr height="52">
					<td vAlign="top" width="189" bgColor="white" height="52"><img height="52" src="images/siam001.gif" width="189"></td>
					<td vAlign="top" width="19" bgColor="white" height="52"><img height="52" src="images/spacer.gif" width="19"></td>
					<td vAlign="top" width="127" bgColor="white" height="52"><img height="52" src="images/spacer.gif" width="127"></td>
					<td vAlign="top" width="237" bgColor="white" height="52"><img height="52" src="images/siam002.gif" width="237"></td>
					<td vAlign="top" width="176" bgColor="white" height="52"><img height="52" src="images/siam003.gif" width="176"></td>
					<td vAlign="top" background="images/siam_vert_bkg001.gif" bgColor="#99a8b5" rowSpan="6"><img height="52" src="images/spacer.gif" width="23"></td>
				</tr>
				<tr height="9">
					<td vAlign="top" background="images/siam_horz_bkg001.gif" bgColor="white" colSpan="5"
						height="9"><img height="9" src="images/spacer.gif" width="23"></td>
				</tr>
				<tr height="21">
					<td vAlign="top" background="images/siam_horz_bkg002.gif" bgColor="#c9d5e6" colSpan="5"
						height="21"><img height="21" src="images/spacer.gif" width="23"></td>
				</tr>
				<tr height="42">
					<td vAlign="top" width="189" bgColor="#363d44" height="42"><img height="42" src="images/spacer.gif" width="189" border="0"></td>
					<td vAlign="top" bgColor="#363d44" colSpan="2" height="42"><img height="42" src="images/spacer.gif" width="146" border="0"></td>
					<td vAlign="top" width="237" bgColor="#363d44" height="42"><img height="42" src="images/spacer.gif" width="237"></td>
					<td vAlign="top" width="176" bgColor="#363d44" height="42"><img height="42" src="images/siam006.gif" width="176"></td>
				</tr>
				<tr height="27">
					<td vAlign="top" width="189" bgColor="#5d7180" height="27"><img height="27" src="images/siam007.gif" width="189"></td>
					<td width="19" bgColor="white" height="27"><img height="27" src="images/spacer.gif" width="19"></td>
					<td bgColor="white" colSpan="3" height="27"></td>
				</tr>
				<tr>
					<td vAlign="top" align="center" width="189" bgColor="#5d7180"></td>
					<td width="19" bgColor="white"></td>
					<td vAlign="top" bgColor="white" colSpan="3">
						<table width="500" align="center" border="0">
							<tr>
								<td colSpan="2" height="50">&nbsp;</td>
							</tr>
							<tr>
								<td colSpan="2" align="center"><span class="BodyHead">SIAM ADD/EDIT CS USER</span>
								</td>
							</tr>
							<tr>
								<td colSpan="2">&nbsp;</td>
							</tr>
							<tr>
								<td width="150"><span class="Body">User Type:</span></td>
								<td width="200"><span class="Body"><SPS:SPSTextBox id="UserType" runat="server" CssClass="Body"></SPS:SPSTextBox></span></td>
							</tr>
							<tr>
								<td width="150"><span class="Body">Active:</span></td>
								<td width="200"><span class="Body"><SPS:SPSTextBox id="Active" runat="server" CssClass="Body"></SPS:SPSTextBox></span></td>
							</tr>
							<tr>
								<td width="150"><span class="Body">Am Domain UN:</span></td>
								<td width="200"><span class="Body"><SPS:SPSTextBox id="Domain" runat="server" CssClass="Body"></SPS:SPSTextBox></span></td>
							</tr>
							<tr>
								<td width="150"><span class="Body">*First Name:</span></td>
								<td width="200"><span class="Body"><SPS:SPSTextBox id="FName" runat="server" CssClass="Body"></SPS:SPSTextBox></span></td>
							</tr>
							<tr>
								<td width="150"><span class="Body">*Last Name:</span></td>
								<td width="200"><span class="Body"><SPS:SPSTextBox id="LName" runat="server" CssClass="Body"></SPS:SPSTextBox></span></td>
							</tr>
							<tr>
								<td width="150"><span class="Body">*E-mail Address:</span></td>
								<td width="200"><span class="Body"><SPS:SPSTextBox id="EmailAddress" runat="server" CssClass="Body"></SPS:SPSTextBox></span></td>
							</tr>
							<tr>
								<td width="150"><span class="Body">*Address 1:</span></td>
								<td width="200"><span class="Body"><SPS:SPSTextBox id="Address1" runat="server" CssClass="Body"></SPS:SPSTextBox></span></td>
							</tr>
							<tr>
								<td width="150"><span class="Body">Address 2:</span></td>
								<td width="200"><span class="Body"><SPS:SPSTextBox id="Address2" runat="server" CssClass="Body"></SPS:SPSTextBox></span></td>
							</tr>
							<tr>
								<td width="150"><span class="Body">*City:</span></td>
								<td width="200"><span class="Body"><SPS:SPSTextBox id="City" runat="server" CssClass="Body"></SPS:SPSTextBox></span></td>
							</tr>
							<tr>
								<td width="150"><span class="Body">*State:</span></td>
								<td width="200"><asp:DropDownList id="State" runat="server" CssClass="Body"></asp:DropDownList></span></td>
							</tr>
							<tr>
								<td width="150"><span class="Body">*Zip:</span></td>
								<td width="200"><span class="Body"><SPS:SPSTextBox id="Textbox3" runat="server" CssClass="Body"></SPS:SPSTextBox></span></td>
							</tr>
							<tr>
								<td width="150"><span class="Body">*Phone:</span></td>
								<td width="200"><span class="Body"><SPS:SPSTextBox id="Phone" runat="server" CssClass="Body"></SPS:SPSTextBox></span></td>
							</tr>
							<tr>
								<td width="150"><span class="Body">Fax:</span></td>
								<td width="200"><span class="Body"><SPS:SPSTextBox id="Fax" runat="server" CssClass="Body"></SPS:SPSTextBox></span></td>
							</tr>
							<tr>
								<td colspan="2">
									<table border="0" width="200">
										<tr>
											<td colspan="6">&nbsp;</td>
										</tr>
										<tr>
											<td><span class="Body">Acct#:</span></td>
											<td><span class="Body"><SPS:SPSTextBox id="Account" runat="server" CssClass="Body"></SPS:SPSTextBox></span></td>
											<td><span class="Body">Acct#:</span></td>
											<td><span class="Body"><SPS:SPSTextBox id="Account2" runat="server" CssClass="Body"></SPS:SPSTextBox></span></td>
											<td><span class="Body">Acct#:</span></td>
											<td><span class="Body"><SPS:SPSTextBox id="Account3" runat="server" CssClass="Body"></SPS:SPSTextBox></span></td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<table height="10%" width="771" border="0">
				<tr>
					<td vAlign="top" width="189" bgColor="#5d7180"><img height="1" src="images/spacer.gif" width="189"></td>
					<td width="19" bgColor="white"><img height="1" src="images/spacer.gif" width="19"></td>
					<td vAlign="top" bgColor="white"><img height="1" src="images/spacer.gif" width="540"></td>
					<td vAlign="top" background="images/siam_vert_bkg001.gif" bgColor="#99a8b5"><img height="23" src="images/spacer.gif" width="23"></td>
				</tr>
				<tr>
					<td vAlign="top" align="center" width="189" bgColor="#5d7180"></td>
					<td width="19" bgColor="white"></td>
					<td vAlign="top" bgColor="white"><span class="Bottom">Copyright and</span> <A class="Bottom" href="123.htm">
							Links</A></td>
					<td valign="top" bgcolor="#99a8b5" background="images/siam_vert_bkg001.gif"><img height="23" width="23" src="images/spacer.gif"></td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
