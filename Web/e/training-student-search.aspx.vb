Imports Sony.US.ServicesPLUS.Data
Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Process
Imports System.Drawing
Imports ServicesPlusException

Namespace SIAMAdmin

    Partial Class training_student_search
        Inherits UtilityClass
        'Dim objUtilties As Utilities = New Utilities

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub


        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            MyBase.IsProtectedPage(True, True)
            Page.ID = Request.Url.Segments.GetValue(1)
            InitializeComponent()
            '-- apply the datagrid style --
            ApplyDataGridStyle()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            If Not Page.IsPostBack Then controlMethod(sender, e)
        End Sub

        Private Sub controlMethod(ByVal sender As System.Object, ByVal e As System.EventArgs)
            Try
                Dim caller As String = String.Empty

                If Not sender.ID Is Nothing Then caller = sender.ID.ToString()

                Select Case caller
                    Case Page.ID.ToString()

                    Case SearchButton.ID.ToString()
                        doSearchForStudents()
                    Case ShowAllStudentsButton.ID.ToString()
                        doShowAllStudents()
                End Select

                Me.writeScriptsToPage()
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                ErrorLabel.Visible = True
            End Try
        End Sub

        Private Sub doSearchForStudents()
            Dim name As String = "%" + NameTextBox.Text.ToLower() + "%"
            Dim email = "%" + EmailTextBox.Text.ToLower() + "%"
            Dim company = "%" + CompanyTextBox.Text.ToLower() + "%"

            Try
                Dim cdm As CourseDataManager = New CourseDataManager
                Dim cps As CourseParticipant() = cdm.SearchParticipants(name, email, company)
                Me.studentDataGrid.DataSource = cps
                Session.Add("searchedStudents", cps)
                Me.studentDataGrid.DataBind()
                Session("searchedparticipants") = cps


                ErrorLabel.Visible = True


                ErrorLabel.Visible = True
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                ErrorLabel.Visible = True
            End Try
        End Sub
        Private Sub doShowAllStudents()
            Try
                Dim cdm As CourseDataManager = New CourseDataManager
                Dim cps As CourseParticipant() = cdm.GetCourseParticipants()
                Me.studentDataGrid.DataSource = cps
                Session.Add("searchedStudents", cps)
                Me.studentDataGrid.DataBind()
                Session("searchedparticipants") = cps


                ErrorLabel.Visible = True


                ErrorLabel.Visible = True
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                ErrorLabel.Visible = True
            End Try
        End Sub
        Private Sub studentDataGrid_ItemCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles studentDataGrid.ItemCreated
            Try
                If e.Item.ItemType <> ListItemType.Header Then
                    e.Item.ApplyStyle(GetStyle(e.Item.ItemType))
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                ErrorLabel.Visible = True
            End Try
        End Sub
        Private Sub studentDataGrid_ItemDataBound(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles studentDataGrid.ItemDataBound
            Try
                If (e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem) Then
                    Dim labelStatus As Label = CType(e.Item.Cells(3).FindControl("LabelStatus"), Label)
                    Dim cp As CourseParticipant = CType(e.Item.DataItem, CourseParticipant)
                    labelStatus.Text = cp.StatusDisplay
                    'Dim hyperLink As HyperLink = CType(e.Item.Cells(1).FindControl("HyperlinkFirstName"), HyperLink)
                    'hyperLink.NavigateUrl = "training-student.aspx?SearchedLineNo=" + e.Item.ItemIndex().ToString
                    'hyperLink = CType(e.Item.Cells(1).FindControl("HyperlinkLastName"), HyperLink)
                    'hyperLink.NavigateUrl = "training-student.aspx?SearchedLineNo=" + e.Item.ItemIndex().ToString
                    Dim hyperLink As HyperLink = CType(e.Item.Cells(1).FindControl("HyperlinkReservationNo"), HyperLink)
                    hyperLink.NavigateUrl = "training-student.aspx?SearchedLineNo=" + e.Item.ItemIndex().ToString
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                ErrorLabel.Visible = True
            End Try
        End Sub

        Private Sub ApplyDataGridStyle()
            'studentDataGrid.ApplyStyle(GetStyle(500))
            'studentDataGrid.PageSize = 10
            'studentDataGrid.AutoGenerateColumns = False
            'studentDataGrid.AllowPaging = False
            'studentDataGrid.GridLines = GridLines.Horizontal
            'studentDataGrid.AllowSorting = True
            'studentDataGrid.CellPadding = 3
            'studentDataGrid.PagerStyle.Mode = PagerMode.NumericPages
            'studentDataGrid.ItemStyle.Font.Size = FontUnit.Point(8)
            'studentDataGrid.AlternatingItemStyle.Font.Size = FontUnit.Point(8)
            'studentDataGrid.HeaderStyle.ForeColor = Color.GhostWhite
            'studentDataGrid.HeaderStyle.BackColor = Color.FromKnownColor(KnownColor.ControlDarkDark)
            ''studentDataGrid.ItemStyle.ForeColor = Color.GhostWhite
            'studentDataGrid.ItemStyle.BackColor = Color.Gainsboro
            ''studentDataGrid.AlternatingItemStyle.ForeColor = Color.GhostWhite
            'studentDataGrid.AlternatingItemStyle.BackColor() = Color.Gainsboro


            studentDataGrid.ApplyStyle(GetStyle(500))
            studentDataGrid.PageSize = 8
            studentDataGrid.AutoGenerateColumns = False
            studentDataGrid.AllowPaging = True
            studentDataGrid.GridLines = GridLines.Horizontal
            studentDataGrid.AllowSorting = True
            studentDataGrid.CellPadding = 3
            studentDataGrid.PagerStyle.Mode = PagerMode.NumericPages
            studentDataGrid.HeaderStyle.Font.Size = FontUnit.Point(8)
            studentDataGrid.HeaderStyle.ForeColor = Color.GhostWhite
            studentDataGrid.HeaderStyle.BackColor = Color.FromKnownColor(KnownColor.ControlDarkDark)
            studentDataGrid.ItemStyle.Font.Size = FontUnit.Point(8)
            studentDataGrid.AlternatingItemStyle.Font.Size = FontUnit.Point(8)

        End Sub
        Sub studentDataGrid_Paging(ByVal sender As Object, ByVal e As DataGridPageChangedEventArgs)
            Try
                studentDataGrid.CurrentPageIndex = e.NewPageIndex
                studentDataGrid.DataSource = Session.Item("searchedStudents")
                studentDataGrid.DataBind()
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                ErrorLabel.Visible = True
            End Try
        End Sub

        Private Sub writeScriptsToPage()
            '-- this script will make sure that this page is only accessed via the iform tag in default.aspx -         
            RegisterClientScriptBlock("redirect", "<script language=""javascript"">if ( parent.frames.length === 0 ){window.location.replace(""" + MyBase.EntryPointPage + """)}</script>")

            '-- this will clear the status bar on the browser 
            RegisterStartupScript("ClearStatus", "<script language=""javascript"">window.parent.status = ""Done."";</script>")

            '-- this script is used to confirm that a user wants to remove an application -
            RegisterStartupScript("RemoveApplication", "<script language=""javascript"">function DeleteOp(itemId){var ok = window.confirm(""Are you sure ?"");if (ok === true){document.location.href=""" + Page.Request.Url.Segments.GetValue(1).ToString() + "?Remove="" + itemId;}}</script>")
        End Sub

        Private Sub ShowAllStudentsButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ShowAllStudentsButton.Click
            controlMethod(sender, e)
        End Sub

        Private Sub SearchButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SearchButton.Click
            controlMethod(sender, e)
        End Sub
    End Class

End Namespace
