﻿Imports System.Web.UI
Imports Sony.US.ServicesPLUS.Process
Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Data
Imports Sony.US.siam, Sony.US.ServicesPLUS.Controls
Imports System.Web.UI.WebControls
Imports System.Data
Imports System.Drawing
Imports Sony.US.AuditLog
Imports ServicesPlusException

Namespace SIAMAdmin
    Partial Class SearchPromotionalCode
        Inherits UtilityClass
        Dim srchPattern As String = String.Empty
        'Dim objUtilties As Utilities = New Utilities
        Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
            MyBase.IsProtectedPage(True, True)
            ApplyDataGridStyle()
        End Sub
        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                If Not IsPostBack Then
                    LoadIndustry()
                    LoadModelPrefix()
                    LoadReseller()
                    ddlModel.Items.Insert(0, New ListItem("Select a model prefix above before proceeding"))
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub
        Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
            Try
                If ValidateData() Then
                    Dim ds As DataSet
                    Dim objPRManager As OnlineProductRegistrationManager = New OnlineProductRegistrationManager()
                    If srchPattern = "" Then
                        srchPattern = " "
                    Else
                        srchPattern = srchPattern.Remove(0, 4)
                        srchPattern = " where " + srchPattern

                    End If
                    Session.Item("searchPattern") = srchPattern
                    ds = objPRManager.GetPromotionalCodeNumbers(srchPattern)
                    If ds.Tables.Count <> 0 Then
                        Dim dv As New DataView(ds.Tables(0))
                        dv.Sort = ("PROMO_CODE_NUMBER")
                        ds.Tables.Clear()
                        ds.Tables.Add(dv.ToTable)
                        Session.Item("PrmotionalCodeNumbers") = ds
                        dgSearchPromo.DataSource = ds
                        dgSearchPromo.DataBind()
                    Else
                        dgSearchPromo.Columns.Clear()
                        dgSearchPromo.DataBind()
                        ErrorLabel.Text = "No Records found"
                    End If

                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub
        Private Sub ApplyDataGridStyle()

            dgSearchPromo.ApplyStyle(GetStyle(500))
            dgSearchPromo.PageSize = 8
            dgSearchPromo.AutoGenerateColumns = False
            dgSearchPromo.AllowPaging = True
            dgSearchPromo.GridLines = GridLines.Horizontal
            dgSearchPromo.AllowSorting = True
            dgSearchPromo.CellPadding = 3
            dgSearchPromo.PagerStyle.Mode = PagerMode.NumericPages
            dgSearchPromo.HeaderStyle.Font.Size = FontUnit.Point(8)
            dgSearchPromo.HeaderStyle.ForeColor = Color.GhostWhite
            dgSearchPromo.HeaderStyle.BackColor = Color.FromKnownColor(KnownColor.ControlDarkDark)
            dgSearchPromo.ItemStyle.Font.Size = FontUnit.Point(8)
            dgSearchPromo.AlternatingItemStyle.Font.Size = FontUnit.Point(8)

        End Sub

        Protected Sub dgSearchPromo_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgSearchPromo.ItemDataBound
            Try
                Dim lnkbtn As LinkButton
                If e.Item.ItemType <> ListItemType.Header And e.Item.ItemType <> ListItemType.Footer Then
                    e.Item.Cells(1).Text = Convert.ToDateTime(String.Format("{0:d}", e.Item.Cells(1).Text))
                    e.Item.Cells(2).Text = Convert.ToDateTime(String.Format("{0:d}", e.Item.Cells(2).Text))
                    e.Item.Cells(3).Text = Convert.ToDateTime(String.Format("{0:d}", e.Item.Cells(2).Text))
                    If (e.Item.Cells(4).Text = "n/a") Then
                        e.Item.Cells(4).Text = ""
                    End If
                    If (e.Item.Cells(5).Text = "n/a") Then
                        e.Item.Cells(5).Text = ""
                    End If
                    If (e.Item.Cells(6).Text = "n/a") Then
                        e.Item.Cells(6).Text = ""
                    End If
                    lnkbtn = e.Item.FindControl("lnkBtnDelete")
                    If Not lnkbtn Is Nothing Then
                        lnkbtn.Attributes.Add("onclick", "javascript:ConfirmDelete();")
                    End If
                End If

                'If (e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem) Then
                '    lnkbtn = e.Item.FindControl("lnkBtnDelete")
                '    If Not lnkbtn Is Nothing Then
                '        lnkbtn.Attributes.Add("onclick", "javascript:ConfirmDelete();")
                '    End If
                'End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Protected Sub dgSearchPromo_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dgSearchPromo.PageIndexChanged
            Try
                dgSearchPromo.CurrentPageIndex = e.NewPageIndex
                If Not Session.Item("PrmotionalCodeNumbers") Is Nothing Then
                    dgSearchPromo.DataSource = Session.Item("PrmotionalCodeNumbers")
                    dgSearchPromo.DataBind()
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Protected Sub ddlModelPrefix_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlModelPrefix.SelectedIndexChanged
            Try
                'Load Model
                If ddlModelPrefix.SelectedIndex > 0 Then
                    LoadModel()
                Else
                    ddlModelPrefix.Items.Clear()
                    ddlModelPrefix.Items.Insert(0, New ListItem("Select a model prefix above before proceeding"))
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub
        Private Sub LoadModel()
            Dim objPRManager As OnlineProductRegistrationManager = New OnlineProductRegistrationManager()
            Dim oDS As DataSet = objPRManager.GetPRModels(ddlModelPrefix.SelectedValue.ToString())
            ddlModel.Items.Clear()
            'ddlModelPrefix.AutoPostBack = False

            If Not oDS Is Nothing Then
                'Dim lstItem As ListItem
                'For Each lstFuncItem In oDS.Tables(0).Rows
                '    If (Not lstFuncItem Is Nothing) Then
                '        If lstFuncItem.AvailableStatus = 0 Then
                '            lstItem = New ListItem(lstFuncItem.Name, lstFuncItem.FunctionalityId.ToString() + "~")
                '            lstAvailable.Items.Add(lstItem)
                '        Else
                '            lstItem = New ListItem(lstFuncItem.Name, lstFuncItem.FunctionalityId)
                '            lstSelected.Items.Add(lstItem)
                '        End If
                '    End If
                'Next lstFuncItem



                ddlModel.DataTextField = "MODELNUMBER"
                ddlModel.DataValueField = "MODELNUMBER"
                ddlModel.DataSource = oDS.Tables(0)
                ddlModel.DataBind()
            Else
                Throw New ApplicationException("Exception occurred")
            End If
            ddlModel.Items.Insert(0, New ListItem("Select Model"))

        End Sub
        Private Sub LoadReseller()
            Dim objPRManager As OnlineProductRegistrationManager = New OnlineProductRegistrationManager()
            Dim oDS = objPRManager.GetReseller()
            If Not oDS Is Nothing Then
                ddlResellerName.DataTextField = "RESELLERNAME"
                ddlResellerName.DataValueField = "RESELLERCODE"
                ddlResellerName.DataSource = oDS.Tables(0).DefaultView()
                ddlResellerName.DataBind()
            Else
                Throw New ApplicationException("Exception occurred")
            End If
            ddlResellerName.Items.Insert(0, New ListItem("Select Reseller"))
        End Sub
        Private Sub LoadModelPrefix()
            Dim objPRManager As OnlineProductRegistrationManager = New OnlineProductRegistrationManager()
            Dim oDS = objPRManager.GetModelPrefix()
            ddlModelPrefix.Items.Clear()
            If Not oDS Is Nothing Then
                ddlModelPrefix.DataTextField = "MODELPREFIX"
                ddlModelPrefix.DataValueField = "MODELPREFIX"
                ddlModelPrefix.DataSource = oDS.Tables(0).DefaultView()
                ddlModelPrefix.DataBind()

            Else
                Throw New ApplicationException("Exception occurred")
            End If
            ddlModelPrefix.Items.Insert(0, New ListItem("Click..."))
        End Sub
        Private Sub LoadIndustry()
            Dim objPRManager As OnlineProductRegistrationManager = New OnlineProductRegistrationManager()
            Dim oDS = objPRManager.GetIndustry()
            PUSH.Items.Clear()
            If Not oDS Is Nothing Then
                PUSH.DataTextField = "INDUSTRY"
                PUSH.DataValueField = "INDUSTRYCODE"
                PUSH.DataSource = oDS.Tables(0).DefaultView()
                PUSH.DataBind()

            Else
                Throw New ApplicationException("Exception occurred")
            End If
            PUSH.Items.Insert(0, New ListItem("Select INDUSTRY"))
        End Sub
        Private Function ValidateData() As Boolean
            If (Len(txtpromocodenumber.Text.Trim()) = 10) Then
                If Not ((txtpromocodenumber.Text.Trim().ToUpper.StartsWith("IN") Or txtpromocodenumber.Text.Trim().ToUpper.StartsWith("MO") Or txtpromocodenumber.Text.Trim().ToUpper.StartsWith("RE") Or txtpromocodenumber.Text.Trim().ToUpper.StartsWith("CB"))) Then
                    ErrorLabel.Text = "The Promo Code Number you entered is not valid."
                    Return False
                Else
                    srchPattern = srchPattern + " and a.PROMO_CODE_NUMBER like '%" + txtpromocodenumber.Text.Trim().ToUpper() + "%'"
                End If
            Else
                If (Len(txtpromocodenumber.Text.Trim()) >= 2) Then
                    srchPattern = srchPattern + " and a.PROMO_CODE_NUMBER like '%" + txtpromocodenumber.Text.Trim().ToUpper() + "%'"
                Else
                    If txtpromocodenumber.Text.Trim() <> "" Then
                        ErrorLabel.Text = "Please enter atleast 2 characters of Promo code number."
                        Return False
                    End If

                End If
            End If
            'Promo start date check
            If (txtPromoStartdate.Text.Trim() <> String.Empty) Then

                If IsDate(txtPromoStartdate.Text.ToString()) Then
                    Dim thisPattern As String = "^(\d{2}\/\d{2}\/\d{4}$)"
                    If Not System.Text.RegularExpressions.Regex.IsMatch(txtPromoStartdate.Text.ToString(), thisPattern) Then
                        ErrorLabel.Text = "Please enter Promo Start Date in MM/DD/YYYY format."
                        ErrorLabel.Visible = True
                        Return False
                    Else
                        srchPattern = srchPattern + " and a.PROMO_START_DATE = to_date('" + txtPromoStartdate.Text.Trim() + "', 'mm-dd-yyyy')"
                    End If
                Else
                    ErrorLabel.Text = "Please enter Promo Start Date in MM/DD/YYYY format."
                    ErrorLabel.Visible = True
                    Return False
                End If
            End If
            'Promo start date check
            If (txtpromoenddate.Text.Trim() <> String.Empty) Then

                If IsDate(txtpromoenddate.Text.ToString()) Then
                    Dim thisPattern As String = "^(\d{2}\/\d{2}\/\d{4}$)"
                    If Not System.Text.RegularExpressions.Regex.IsMatch(txtpromoenddate.Text.ToString(), thisPattern) Then
                        ErrorLabel.Text = "Please enter Promo End Date in MM/DD/YYYY format."
                        ErrorLabel.Visible = True
                        Return False
                    Else
                        srchPattern = srchPattern + " and a.PROMO_END_DATE = to_date('" + txtpromoenddate.Text.Trim() + "', 'mm-dd-yyyy')"
                    End If
                Else
                    ErrorLabel.Text = "Please enter Promo End Date in MM/DD/YYYY format."
                    ErrorLabel.Visible = True
                    Return False
                End If
            End If
            'Promo start date check
            If (txtproregenddate.Text.Trim() <> String.Empty) Then

                If IsDate(txtproregenddate.Text.ToString()) Then
                    Dim thisPattern As String = "^(\d{2}\/\d{2}\/\d{4}$)"
                    If Not System.Text.RegularExpressions.Regex.IsMatch(txtproregenddate.Text.ToString(), thisPattern) Then
                        ErrorLabel.Text = "Please enter Promo Registration End Date in MM/DD/YYYY format."
                        ErrorLabel.Visible = True
                        Return False
                    Else
                        srchPattern = srchPattern + " and a.PROD_REG_END_DATE =to_date('" + txtproregenddate.Text.Trim() + "', 'mm-dd-yyyy')"
                    End If
                Else
                    ErrorLabel.Text = "Please enter Promo Registration End Date in MM/DD/YYYY format."
                    ErrorLabel.Visible = True
                    Return False
                End If
            End If
            If PUSH.SelectedIndex > 0 Then
                srchPattern = srchPattern + " and INDUSTRY = '" + PUSH.SelectedItem.Text.Trim() + "'"
            End If
            If ddlModel.SelectedIndex > 0 Then
                srchPattern = srchPattern + " and MODELNUMBER = '" + ddlModel.SelectedItem.Text.Trim() + "'"
            End If
            If ddlResellerName.SelectedIndex > 0 Then
                srchPattern = srchPattern + " and RESELLERNAME = '" + ddlResellerName.SelectedItem.Text.Trim() + "'"
            End If


            Return True

        End Function

        Protected Sub dgSearchPromo_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgSearchPromo.ItemCommand
            Try
                If e.CommandName = "Delete" Then
                    Dim ds As DataSet
                    Dim objPRManager As OnlineProductRegistrationManager = New OnlineProductRegistrationManager()
                    Dim strPromotioncode As String = String.Empty
                    strPromotioncode = e.CommandArgument.ToString()
                    Try
                        objPRManager.BeginTransaction()
                        Select Case strPromotioncode.Trim.ToUpper().Substring(0, 2)
                            Case "IN"
                                objPRManager.DeletePROIndustry(strPromotioncode.Trim.ToUpper())

                            Case "MO"
                                objPRManager.DeletePROMODEL(strPromotioncode.Trim.ToUpper())

                            Case "RE"
                                objPRManager.DeletePROReseller(strPromotioncode.Trim.ToUpper())

                            Case "CB"
                                objPRManager.DeletePROIndustry(strPromotioncode.Trim.ToUpper())
                                objPRManager.DeletePROMODEL(strPromotioncode.Trim.ToUpper())
                                objPRManager.DeletePROReseller(strPromotioncode.Trim.ToUpper())
                        End Select
                        objPRManager.DeletePROMOTIONCODE(strPromotioncode.Trim.ToUpper())
                        objPRManager.CommitTransaction()
                        srchPattern = Session.Item("searchPattern").ToString()
                        ds = objPRManager.GetPromotionalCodeNumbers(srchPattern)
                        If ds.Tables.Count <> 0 Then
                            Dim dv As New DataView(ds.Tables(0))
                            dv.Sort = ("PROMO_CODE_NUMBER")
                            ds.Tables.Clear()
                            ds.Tables.Add(dv.ToTable)
                            Session.Item("PrmotionalCodeNumbers") = ds
                            dgSearchPromo.DataSource = ds
                            dgSearchPromo.DataBind()
                        Else
                            dgSearchPromo.Columns.Clear()
                            dgSearchPromo.DataBind()
                            ErrorLabel.Text = "No Records found"
                        End If


                        objPRManager.RollbackTransaction()


                        objPRManager.RollbackTransaction()
                    Catch ex As Exception
                        ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                        objPRManager.RollbackTransaction()
                    End Try


                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
            Try
                txtpromocodenumber.Text = ""
                txtPromoStartdate.Text = ""
                txtpromoenddate.Text = ""
                txtproregenddate.Text = ""
                LoadIndustry()
                LoadModelPrefix()
                LoadReseller()
                ddlModel.Items.Clear()
                ddlModel.Items.Insert(0, New ListItem("Select a model prefix above before proceeding"))
                dgSearchPromo.Columns.Clear()
                dgSearchPromo.DataBind()
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub
    End Class
End Namespace
