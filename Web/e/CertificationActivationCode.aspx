<%@ Reference Page="~/e/Utility.aspx"  %>

<%@ Page Language="vb" AutoEventWireup="false" SmartNavigation="True" CodeFile="CertificationActivationCode.aspx.vb"  Inherits="SIAMAdmin.CertificationActivationCode"
     %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>Certification Code</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <link href="includes/style.css" type="text/css" rel="stylesheet">
    <link href="includes/ServicesPLUS_style.css" rel="stylesheet" type="text/css" />
    <script language="javascript" src="includes/SiamAdmin.js" type="text/javascript"></script>

</head>
<body >
    <form id="Form1" method="post" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <table id="Table3" align="center" border="0" width="100%">
        <tr>
            <td class="PageHead" align="center" colspan="1" height="25" rowspan="1">
                Maintain Certification Activation Code<br />
            </td>
        </tr>
       
        <tr>
            <td valign="top" align="center">
               <div id="div1" style="border-right: #c9d5e6 thin solid; width: 70%; border-top: #c9d5e6 thin solid;
                                        border-left: #c9d5e6 thin solid; border-bottom: #c9d5e6 thin solid">
                <table id="Table2" width="100%" border="0">
                    <tr>
                        <td colspan=2>
                                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                            <ContentTemplate>
                                <asp:Label ID="ErrorLabel" runat="server" EnableViewState="False" ForeColor="Red"
                                    CssClass="Body"></asp:Label>&nbsp;
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="btnUpdate"  EventName="Click"/>
                                <asp:AsyncPostBackTrigger ControlID="ddlTrainingCourse"  EventName="SelectedIndexChanged"/>
                            </Triggers>
                        </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr  bgcolor="#c9d5e6">
                        <td align="Left" colspan="1"  width="15%">
                            <asp:Label ID="labelCourseNumber" runat="server" CssClass="BodyCopy">Certification Course</asp:Label>
                        </td>
                        <td style="height: 7px">
                            <asp:DropDownList ID="ddlTrainingCourse" runat="server" AutoPostBack=true  CssClass="Body" Width="98%">
                            </asp:DropDownList>
                            <asp:Label ID="LabelCourseNumberStar" runat="server" CssClass="redAsterick">
										<span class="redAsterick">*</span></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" align="left" width="15%">
                            <asp:Label ID="lblActivationCode" runat="server" CssClass="BodyCopy">Activation Code</asp:Label>
                        </td>
                        <td>
                            <SPS:SPSTextBox ID="txtActivationCode" runat="server" CssClass="Body" Width="98%" TextMode="MultiLine"
                                Height="90px"></SPS:SPSTextBox>
                        </td>
                    </tr>
                    <tr  bgcolor="#c9d5e6">
                        <td valign="top" align="right" width="15%">
                            &nbsp;
                        </td>
                        <td align="left">
                            <asp:UpdateProgress ID="UpdateProgress1" AssociatedUpdatePanelID="UpdatePanel1" runat="server">
                            <ProgressTemplate>
                                <span id="Span4" class="bodycopy">Please wait...</span>
                                      <img src="images/progbar.gif" width="100" />
                            </ProgressTemplate>
                            </asp:UpdateProgress>
                        </td>
                    </tr>
                    <tr>
                       
                        <td align="right" colspan=2>
                            <asp:Button ID="btnUpdate" runat="server" Text="Update"></asp:Button>&nbsp;&nbsp;&nbsp;
                        </td>
                    </tr>
                    
                </table>
                </div>
               
            </td>
        </tr>
        <tr>
            <td valign="top" align="center">
                <table id="Table1" width="70%" border="0">
                    <tr >
                        <td align="left" colspan=2>
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server" ChildrenAsTriggers=true>
                                <ContentTemplate>
                                    <div style="overflow: auto; width: 100%; height: 200px">
                                        <asp:DataGrid ID="dgCertificate" runat="server" PageSize="5000" AutoGenerateColumns="False" CellPadding="4"
                                            ForeColor="#333333" GridLines="None" Width="97%">
                                            <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                            <EditItemStyle BackColor="#999999" />
                                            <SelectedItemStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                            <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                                            <AlternatingItemStyle BackColor="White" ForeColor="#284775" />
                                            <ItemStyle BackColor="#F7F6F3" ForeColor="#333333" />
                                            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" CssClass="Body" ForeColor="White" />
                                            <Columns>
                                                <asp:BoundColumn DataField="CERTIFICATIONACTIVATIONCODE" HeaderText="Activation Code">
                                                    <ItemStyle Font-Bold="False" CssClass="bodycopy" Font-Italic="False" Font-Overline="False"
                                                        Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Left" VerticalAlign="Middle" />
                                                    <HeaderStyle Width="40%" />
                                                </asp:BoundColumn>
                                                <asp:BoundColumn DataField="PURCHASED" HeaderText="Purchased" Visible="True">
                                                    <ItemStyle Font-Bold="False" CssClass="bodycopy" Font-Italic="False" Font-Overline="False"
                                                        Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Left" VerticalAlign="Middle" />
                                                    <HeaderStyle Width="20%" />
                                                </asp:BoundColumn>
                                                <asp:BoundColumn DataField="UPDATEDATE" HeaderText="Update Date" Visible="True">
                                                    <ItemStyle Font-Bold="False" CssClass="bodycopy" Font-Italic="False" Font-Overline="False"
                                                        Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Left" VerticalAlign="Middle" />
                                                    <HeaderStyle Width="40%" />
                                                </asp:BoundColumn>
                                            </Columns>
                                        </asp:DataGrid>
                                    </div>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="btnUpdate"  EventName="Click"/>
                                    <asp:AsyncPostBackTrigger ControlID="ddlTrainingCourse"  EventName="SelectedIndexChanged"/>
                                </Triggers>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
               
            </td>
        </tr>
        
    </table>
    </form>
</body>
</html>
