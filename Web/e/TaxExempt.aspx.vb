﻿Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Process
Imports ServicesPlusException
Imports ServicePLUSWebApp

Namespace SIAMAdmin
    Partial Class TaxExempt
        Inherits UtilityClass

#Region "   PAGE AND BUTTON EVENTS   "
        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            MyBase.IsProtectedPage(True, True)
            'InitializeComponent()
        End Sub

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                lblErrorMessage.Text = ""
                lblUserMessage.Text = ""
                If Not IsPostBack Then
                    'Utilities.LogMessages("TAX: Page_Load - START")
                    GetTaxData()
                    BindData()
                    'Utilities.LogMessages("TAX: Page_Load - Data fetched and bound")

                    If HttpContextManager.SelectedCustomer IsNot Nothing Then
                        Dim customer = HttpContextManager.SelectedCustomer
                        'Utilities.LogMessages("TAX: Page_Load - Fetching Customer data")
                        ' Set up the "Customer Details" labels with data
                        'Utilities.LogMessages("TAX: Page_Load - Setting up Customer details labels")
                        lblCustomerName.Text = customer.Name
                        lblCustomerCompany.Text = customer.CompanyName
                        lblCustomerEmail.Text = customer.EmailAddress
                        lblCustomerLdap.Text = customer.LdapID
                        lblCustomerSiam.Text = customer.SIAMIdentity
                        lblCustomerState.Text = customer.Address.State
                    End If
                End If
            Catch ex As Exception
                lblErrorMessage.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        'Private Sub grdTaxExempt_RowCommand(ByVal sender As System.Object, ByVal e As GridViewCommandEventArgs) Handles grdTaxExempt.RowCommand
        Protected Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As EventArgs)
            Dim tm As New TaxExemptionManager
            Dim newTax = New TaxExemption

            Try
                'Utilities.LogMessages("TAX: Add Row - START")
                lblErrorMessage.Text = ""
                lblUserMessage.Text = ""

                'Utilities.LogMessages("TAX: Add Row - Populating TaxExemption object")
                newTax.DocumentStatus = "V"   ' Field is obsolete, but required. We don't show it to the end user.
                newTax.SiamID = lblCustomerSiam.Text
                newTax.Name = CType(grdTaxExempt.FooterRow.FindControl("txtNewName"), TextBox).Text
                newTax.City = CType(grdTaxExempt.FooterRow.FindControl("txtNewCity"), TextBox).Text
                newTax.State = CType(grdTaxExempt.FooterRow.FindControl("ddlNewState"), DropDownList).SelectedValue
                newTax.DocumentState = CType(grdTaxExempt.FooterRow.FindControl("ddlNewDocumentState"), DropDownList).SelectedValue
                'Utilities.LogMessages("TAX: Add Row - ddlNewDocumentState fetched. Value: " & newTax.DocumentState)
                newTax.DocumentDate = DateTime.Parse((CType((grdTaxExempt.FooterRow.FindControl("txtNewDocumentDate")), HtmlInputText)).Value)
                'Utilities.LogMessages("TAX: Add Row - txtNewDocumentDate fetched. Value: " & newTax.DocumentDate)
                newTax.ExpirationDate = DateTime.Parse((CType((grdTaxExempt.FooterRow.FindControl("txtNewExpirationDate")), HtmlInputText)).Value)
                'Utilities.LogMessages("TAX: Add Row - txtNewExpirationDate fetched. Value: " & newTax.ExpirationDate)
                newTax.DocumentIDNumber = CType(grdTaxExempt.FooterRow.FindControl("txtNewDocumentIDNumber"), TextBox).Text
                'Utilities.LogMessages("TAX: Add Row - txtNewDocumentIDNumber fetched")

                'Utilities.LogMessages("TAX: Add Row - Validating Exemption data")
                If (IsExemptionValid(newTax)) Then
                    If tm.AddTaxExemption(newTax) Then   ' Row was added to the database successfully
                        lblUserMessage.Text = "New Tax Exemption added to database."
                    End If   ' No need to handle Else; the Validation generates error messages of its own.
                End If

                'Utilities.LogMessages("TAX: Add Row - Rebinding data")
                GetTaxData()
                BindData()
                'Utilities.LogMessages("TAX: Add Row - Finish")
            Catch ex As Exception
                lblErrorMessage.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub
#End Region

#Region "   GRIDVIEW EVENTS   "
        Protected Sub grdTaxExempt_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdTaxExempt.RowCreated
            Dim ddlState As DropDownList
            Dim ddlDocumentState As DropDownList

            Try
                If (e.Row.RowType = DataControlRowType.DataRow And e.Row.RowIndex = grdTaxExempt.EditIndex) Then
                    ' Populate dropdowns with all States (US) and Provinces (CA)
                    'Utilities.LogMessages("TAX: grdTaxExempt_RowEditing - Dropdown preparation")
                    ddlState = CType(e.Row.FindControl("ddlState"), DropDownList)
                    If ddlState IsNot Nothing Then
                        populateStateDropdownList(ddlState)
                        populateProvinceDropdownList(ddlState)
                    Else
                        Utilities.LogMessages("TAX: grdTaxExempt_RowCreated - ddlState not found")
                    End If

                    ddlDocumentState = CType(e.Row.FindControl("ddlDocumentState"), DropDownList)
                    If ddlState IsNot Nothing Then
                        populateStateDropdownList(ddlDocumentState)
                        populateProvinceDropdownList(ddlDocumentState)
                    Else
                        Utilities.LogMessages("TAX: grdTaxExempt_RowCreated - ddlDocumentState not found")
                    End If
                ElseIf e.Row.RowType = DataControlRowType.Footer Then
                    'Utilities.LogMessages("TAX: grdTaxExempt_RowCreated - START")
                    ' Fill the State dropdowns
                    ddlState = CType(e.Row.FindControl("ddlNewState"), DropDownList)
                    If (ddlState IsNot Nothing) AndAlso (ddlState.Items.Count = 0) Then
                        populateStateDropdownList(ddlState)
                        populateProvinceDropdownList(ddlState)
                    Else
                        Utilities.LogMessages("TAX: grdTaxExempt_RowCreated - ddlNewState not found")
                    End If
                    ddlDocumentState = CType(e.Row.FindControl("ddlNewDocumentState"), DropDownList)
                    If (ddlDocumentState IsNot Nothing) AndAlso (ddlDocumentState.Items.Count = 0) Then
                        populateStateDropdownList(ddlDocumentState)
                        populateProvinceDropdownList(ddlDocumentState)
                    Else
                        Utilities.LogMessages("TAX: grdTaxExempt_RowCreated - ddlNewDocumentState not found")
                    End If
                    'Utilities.LogMessages("TAX: grdTaxExempt_RowCreated - Dropdowns filled")

                    ' Set up default data for the "New" row in the grid footer
                    If HttpContextManager.SelectedCustomer IsNot Nothing Then
                        'Utilities.LogMessages("TAX: grdTaxExempt_RowCreated - Fetching Customer data")
                        Dim customer = HttpContextManager.SelectedCustomer
                        CType(e.Row.FindControl("txtNewName"), TextBox).Text = customer.CompanyName
                        CType(e.Row.FindControl("txtNewCity"), TextBox).Text = customer.Address.City
                        CType(e.Row.FindControl("ddlNewState"), DropDownList).SelectedValue = customer.Address.State
                        'Utilities.LogMessages("TAX: grdTaxExempt_RowCreated - Row data pre-filled")
                    End If
                End If
            Catch ex As Exception
                lblErrorMessage.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        ''' <summary>
        ''' This event occurs after the user clicks "Edit" but before the GridView has switched modes.
        ''' </summary>
        ''' <remarks></remarks>
        Protected Sub grdTaxExempt_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles grdTaxExempt.RowEditing
            Try
                'Utilities.LogMessages("TAX: grdTaxExempt_RowEditing - START")
                grdTaxExempt.ShowFooter = False
                grdTaxExempt.EditIndex = e.NewEditIndex
                'Utilities.LogMessages("TAX: grdTaxExempt_RowEditing - Rebinding Gridview")
                BindData()
            Catch ex As Exception
                lblErrorMessage.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Protected Sub grdTaxExempt_RowCancelingEdit(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCancelEditEventArgs) Handles grdTaxExempt.RowCancelingEdit
            Try
                'Utilities.LogMessages("TAX: Cancel editing row")
                grdTaxExempt.ShowFooter = True
                grdTaxExempt.EditIndex = -1
                GetTaxData()
                BindData()
            Catch ex As Exception
                lblErrorMessage.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Protected Sub grdTaxExempt_RowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles grdTaxExempt.RowUpdating
            Try
                'Utilities.LogMessages("TAX: Update Row - START")
                Dim tm As New TaxExemptionManager()
                Dim taxList = CType(Session("TaxExemptData"), ArrayList)
                Dim row = grdTaxExempt.Rows(e.RowIndex)
                Dim taxItem As TaxExemption = taxList(row.DataItemIndex)

                'Update the values.
                'Utilities.LogMessages("TAX: Update Row - INFO - DataItemIndex: " & row.DataItemIndex)
                ' taxItem.RowID should already be set, and should not change
                ' taxItem.SiamID should already be set, and should not change
                taxItem.Name = (CType((row.FindControl("txtName")), TextBox)).Text
                taxItem.City = (CType((row.FindControl("txtCity")), TextBox)).Text
                taxItem.State = (CType((row.FindControl("ddlState")), DropDownList)).SelectedValue
                taxItem.DocumentState = (CType((row.FindControl("ddlDocumentState")), DropDownList)).SelectedValue
                taxItem.DocumentDate = DateTime.Parse((CType((row.FindControl("txtDocumentDate")), HtmlInputText)).Value)
                'Utilities.LogMessages("TAX: Update Row - INFO - DocumentDate:" & taxItem.DocumentDate)
                taxItem.ExpirationDate = DateTime.Parse((CType((row.FindControl("txtExpirationDate")), HtmlInputText)).Value)
                taxItem.DocumentIDNumber = (CType((row.FindControl("txtDocumentIDNumber")), TextBox)).Text
                taxItem.UpdateDate = DateTime.Now
                If (IsExemptionValid(taxItem)) Then
                    'Utilities.LogMessages("TAX: Update Row - Update database start")
                    tm.UpdateTaxExemption(taxItem)
                    'Utilities.LogMessages("TAX: Update Row - Update database complete")
                    'Reset the edit index.
                    grdTaxExempt.ShowFooter = True
                    grdTaxExempt.EditIndex = -1
                    GetTaxData()
                    BindData()
                End If
                'Utilities.LogMessages("TAX: Update Row - Finish")
            Catch ex As Exception
                lblErrorMessage.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Protected Sub grdTaxExempt_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles grdTaxExempt.RowDeleting
            Try
                'Utilities.LogMessages("TAX: Delete Row - START")
                Dim tm As New TaxExemptionManager()
                Dim taxList = CType(Session("TaxExemptData"), ArrayList)
                'Utilities.LogMessages("TAX: Delete Row - Parsing Row ID")
                Dim row = grdTaxExempt.Rows(e.RowIndex)
                Dim taxItem = CType(taxList(row.DataItemIndex), TaxExemption)

                'Utilities.LogMessages("TAX: Delete Row - INFO - DataItemIndex: " & row.DataItemIndex & ", RowID: " & taxItem.RowID)
                tm.DeleteTaxExemption(taxItem.RowID)
                'Utilities.LogMessages("TAX: Delete Row - Refreshing Data")
                GetTaxData()
                BindData()
                'Utilities.LogMessages("TAX: Delete Row - Finish")
            Catch ex As Exception
                lblErrorMessage.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub
#End Region

        ''' <summary>
        ''' Fetches all Tax Exemptions for the current SIAM ID, and places it in Session Variable "TaxExemptData" as an ArrayList.
        ''' </summary>
        ''' <remarks>You must call BindData() after if you wish to update the GridView.</remarks>
        Private Sub GetTaxData()
            Try
                Dim tm As New TaxExemptionManager
                Dim siam = String.Empty

                If (Not String.IsNullOrEmpty(ucCustomerMenu.SiamID())) Then
                    siam = ucCustomerMenu.SiamID()
                    Session("TaxExemptData") = tm.GetTaxExemptions(siam)
                Else
                    lblErrorMessage.Text = "An error occurred while trying to read the Customer data. Please <a href='users.aspx'>Click Here</a> to try again."
                    Utilities.LogMessages("TAX: GetTaxData - Navigation Siam ID is Null")
                End If
            Catch ex As Exception
                lblErrorMessage.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        ''' <summary>
        ''' Binds the GridView to the Session Variable "TaxExemptData", which is an ArrayList of TaxExemptions
        ''' </summary>
        Private Sub BindData()
            Dim data As ArrayList = Nothing
            Dim isDataEmpty As Boolean = False

            Try
                If (Session("TaxExemptData") IsNot Nothing) Then
                    data = CType(Session("TaxExemptData"), ArrayList)
                End If
                If (data Is Nothing) OrElse (data.Count = 0) Then
                    data.Add(New TaxExemption)
                    isDataEmpty = True
                End If
                grdTaxExempt.DataSource = data
                grdTaxExempt.DataBind()
                If isDataEmpty Then grdTaxExempt.Rows(0).Visible = False
            Catch ex As Exception
                lblErrorMessage.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub


        ''' <summary>
        ''' Checks a provided TaxExemption to ensure all fields are filled in.
        ''' </summary>
        Private Function IsExemptionValid(ByVal item As TaxExemption) As Boolean
            Dim isValid As Boolean = True

            lblErrorMessage.Text = ""
            Try
                If String.IsNullOrEmpty(item.SiamID) Then
                    isValid = False
                    lblErrorMessage.Text &= "There was an error fetching the SIAM ID from memory. Please refresh the page and try again." & vbCrLf
                End If

                If String.IsNullOrEmpty(item.Name) Then
                    isValid = False
                    lblErrorMessage.Text &= "Company Name is required." & vbCrLf
                End If

                If String.IsNullOrEmpty(item.City) Then
                    isValid = False
                    lblErrorMessage.Text &= "City is required." & vbCrLf
                End If

                If String.IsNullOrEmpty(item.State) Then
                    isValid = False
                    lblErrorMessage.Text &= "Company State is required." & vbCrLf
                End If

                If String.IsNullOrEmpty(item.DocumentState) Then
                    isValid = False
                    lblErrorMessage.Text &= "Exempt State is required." & vbCrLf
                End If

                If String.IsNullOrEmpty(item.DocumentDate) Then
                    isValid = False
                    lblErrorMessage.Text &= "Exemption Begins date is required." & vbCrLf
                End If

                If String.IsNullOrEmpty(item.ExpirationDate) Then
                    isValid = False
                    lblErrorMessage.Text &= "Exemption Ends date is required." & vbCrLf
                End If

                If String.IsNullOrEmpty(item.DocumentIDNumber) Then
                    isValid = False
                    lblErrorMessage.Text &= "Deloitte Document ID is required." & vbCrLf
                End If

                Return isValid
            Catch ex As Exception
                lblErrorMessage.Text = Utilities.WrapExceptionforUI(ex)
                Return False
            End Try
        End Function
    End Class
End Namespace
