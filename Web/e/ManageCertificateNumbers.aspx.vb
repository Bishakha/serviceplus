﻿Imports Sony.US.ServicesPLUS.Process
Imports ServicesPlusException
Imports Sony.US.AuditLog
Imports System.Data

Namespace SIAMAdmin
    Partial Class ManageCertificateNumbers
        Inherits UtilityClass
        'Dim objUtilties As Utilities = New Utilities

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            MyBase.IsProtectedPage(True, True)
            Page.ID = Request.Url.Segments.GetValue(1)
            InitializeComponent()

        End Sub

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                'ErrorLabel.Visible = False
                ErrorLabel.Text = String.Empty
                If Not IsPostBack Then

                    'Populate DataGrid
                    InitializeControls(False)
                    PopulateDataGrid()
                    btnCancel.Enabled = False
                    btnSave.Enabled = False
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub
        Private Sub dgShippingMethod_PageIndexChanged(ByVal sender As System.Object, ByVal e As DataGridPageChangedEventArgs) Handles dgShippingMethod.PageIndexChanged
            Try
                dgShippingMethod.CurrentPageIndex = e.NewPageIndex
                PopulateDataGrid()
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub
        Protected Sub dgShippingMethod_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgShippingMethod.ItemCommand
            Try
                If e.CommandName = "Edit" Then
                    dgShippingMethod.SelectedIndex = e.Item.ItemIndex
                    InitializeControls(True)
                    txtProductCode.Text = e.Item.Cells(0).Text
                    txtProductCode.ReadOnly = True
                    txtStarCertNumber.Text = e.Item.Cells(1).Text
                    txtEndCertNumber.Text = e.Item.Cells(2).Text
                    btnSave.Enabled = True
                    btnSave.Text = "Update"
                    btnAddNew.Enabled = False
                    btnCancel.Enabled = True
                ElseIf e.CommandName = "Delete" Then
                    Dim objSIAMOperation As SIAMOperation = New SIAMOperation()
                    objSIAMOperation.DeleteCertificateNumbers(e.Item.Cells(0).Text.Trim())
                    ErrorLabel.Text = "Deleted successfully."
                    InitializeControls(False)
                    btnSave.Enabled = False
                    btnAddNew.Enabled = True
                    btnCancel.Enabled = False
                    btnSave.Text = "Save"
                    PopulateDataGrid()
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub
        Private Sub AddNewUSer_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddNew.Click
            dgShippingMethod.SelectedIndex = -1
            InitializeControls(True)
            btnSave.Enabled = True
            btnCancel.Enabled = True
            btnSave.Text = "Save"
            btnAddNew.Enabled = False
            txtProductCode.ReadOnly = False
            txtProductCode.Focus()
        End Sub
        Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
            dgShippingMethod.SelectedIndex = -1
            InitializeControls(False)

            btnSave.Enabled = False
            btnAddNew.Enabled = True
            btnCancel.Enabled = False
            btnSave.Text = "Save"
        End Sub
        Private Sub btnSave_click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
            Dim objPCILogger As New PCILogger() '6524
            If Valid() = False Then
                ErrorLabel.Text = "Please correct following red marked field(s)."
                Exit Sub
            Else
                If Convert.ToInt32(txtStarCertNumber.Text.Trim()) > Convert.ToInt32(txtEndCertNumber.Text.Trim()) Then
                    ErrorLabel.Text = "Starting certificate number should be less than Ending certificate number"
                    Exit Sub
                End If
                Dim objSIAMOperation As SIAMOperation = New SIAMOperation()
                Try
                    '6524 start
                    objPCILogger.EventOriginApplication = "ServicesPLUS"
                    objPCILogger.EventOriginApplicationLocation = Me.Page.GetType().Name
                    objPCILogger.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
                    objPCILogger.OperationalUser = Environment.UserName
                    objPCILogger.EventDateTime = DateTime.Now.ToLongTimeString()
                    objPCILogger.HTTPRequestObjectValues = objPCILogger.GetRequestContextValue()
                    objPCILogger.IndicationSuccessFailure = "Success"
                    objPCILogger.EventOriginMethod = "btnSave_click"
                    objPCILogger.Message = "btnSave_click Success."
                    objPCILogger.EventType = EventType.Add
                    '6524 end

                    Dim intStarCertNumber As Int32
                    Dim intEndCertNumber As Int32
                    If txtStarCertNumber.Text.Trim() <> String.Empty Then
                        intStarCertNumber = Convert.ToInt32(txtStarCertNumber.Text.Trim())
                    End If
                    If txtEndCertNumber.Text.Trim() <> String.Empty Then
                        intEndCertNumber = Convert.ToInt32(txtEndCertNumber.Text.Trim())
                    End If
                    If btnSave.Text = "Save" Then
                        If Not objSIAMOperation.checkProductCode(txtProductCode.Text.Trim()) Then
                            objSIAMOperation.InsertCertificateNumbers(txtProductCode.Text.Trim(), intStarCertNumber, intEndCertNumber)
                        Else
                            ErrorLabel.Text = "Product Code already exist."
                            Exit Sub
                        End If

                    ElseIf btnSave.Text = "Update" Then
                        objSIAMOperation.UpdateCertificateNumbers(txtProductCode.Text.Trim(), intStarCertNumber, intEndCertNumber)
                    End If
                    dgShippingMethod.SelectedIndex = -1
                    btnSave.Enabled = False
                    btnAddNew.Enabled = True
                    btnCancel.Enabled = False
                    btnSave.Text = "Save"
                    PopulateDataGrid()
                    InitializeControls(False)




                Catch ex As Exception
                    ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                    objPCILogger.IndicationSuccessFailure = "Failure" '6524
                    objPCILogger.Message = "btnSave_click Failed. " & ex.Message.ToString() '6524
                Finally
                    objPCILogger.PushLogToMSMQ() '6524
                End Try
            End If
        End Sub

        Private Function Valid() As Boolean
            Dim blnValid As Boolean = True

            If txtProductCode.Text.Trim() = "" Then
                lblProductCode.CssClass = "redAsterick"
                blnValid = False
            End If
            If txtStarCertNumber.Text.Trim() = "" Or IsNumeric(txtStarCertNumber.Text.Trim()) = False Then
                lblStarCertNumber.CssClass = "redAsterick"
                blnValid = False
            End If
            If txtEndCertNumber.Text.Trim() = "" Or IsNumeric(txtEndCertNumber.Text.Trim()) = False Then
                lblEndCertNumber.CssClass = "redAsterick"
                blnValid = False
            End If
            Return blnValid
        End Function
        Private Sub PopulateDataGrid()

            Dim objSIAMOperation As SIAMOperation = New SIAMOperation()
            Dim dsCertificateNumbers As DataSet = objSIAMOperation.GETCertificateNumbers()
            dgShippingMethod.SelectedIndex = -1
            dgShippingMethod.DataSource = dsCertificateNumbers.Tables(0)
            dgShippingMethod.DataBind()
        End Sub
        Private Sub InitializeControls(ByVal Enable As Boolean)
            txtProductCode.Text = ""
            txtStarCertNumber.Text = ""
            txtEndCertNumber.Text = ""

            txtProductCode.Enabled = Enable
            txtStarCertNumber.Enabled = Enable
            txtEndCertNumber.Enabled = Enable
            txtProductCode.ReadOnly = True


        End Sub

#End Region
    End Class
End Namespace
