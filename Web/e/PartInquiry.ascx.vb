﻿Imports Sony.US.SIAMUtilities

Partial Class e_PartInquiry
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        'Sasikumar WP SPLUS_WP014
        If Not IsPostBack Then
            txtSalesOrganization.Text = ConfigurationData.GeneralSettings.GlobalData.SalesOrganization.US
            txtDistributionChannel.Text = ConfigurationData.GeneralSettings.GlobalData.DistributionChannel.Value
            txtDivision.Text = ConfigurationData.GeneralSettings.GlobalData.Division.Value
            txtLanguage.Text = ConfigurationData.GeneralSettings.GlobalData.LanguagewM.US
        End If

    End Sub
End Class
