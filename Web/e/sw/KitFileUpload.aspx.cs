﻿using System;
using Sony.US.SIAMUtilities;
using System.Web.UI.HtmlControls;

/// <summary>
/// This page is no longer used. All Admin file uploads are handled by FileUpload.aspx
/// </summary>
public partial class e_sw_KitFileUpload : System.Web.UI.Page {
    protected void Page_Load(object sender, EventArgs e) {

    }

    protected void btnAddImage_Click(object sender, EventArgs e) {
        string strFileName;
        HtmlInputFile htmlFile = (HtmlInputFile)ImageFileUpload;
        if (htmlFile.PostedFile.ContentLength > 0) {
            string sFormat = String.Format("{0:#.##}", (float)htmlFile.PostedFile.ContentLength / 2048);
            ViewState["ImageName"] = htmlFile.PostedFile.FileName.Substring(htmlFile.PostedFile.FileName.LastIndexOf("\\") + 1);
            strFileName = ViewState["ImageName"].ToString();

            if ((strFileName.EndsWith(".pdf") == true) || (strFileName.EndsWith(".txt") == true) || (strFileName.EndsWith(".zip") == true)) {
                //htmlFile.PostedFile.SaveAs(Server.MapPath(strFileName));
                htmlFile.PostedFile.SaveAs(ConfigurationData.Environment.SoftwarePlusConfiguration.SoftwarePLUSReleaseNotes + "\\" + strFileName);
                lblError.Visible = false;
                System.Threading.Thread.Sleep(2000);
                KitlabelFileName.Text = strFileName.ToString();
                Label1.Visible = true;

                Label1.Text = "Upload successfull!";
                Response.Redirect("KitInformation.aspx?KitlabelFileName=" + KitlabelFileName.Text);

            } else {
                Label1.Visible = false;
                lblError.Visible = true;
                lblError.Text = "Please Check your file";
            }

        } else {
            lblError.Visible = true;
            lblError.Text = "Please Select any file to upload";

        }


    }

}
