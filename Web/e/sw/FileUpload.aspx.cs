﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;
using ServicesPlusException;
using Sony.US.SIAMUtilities;

public partial class e_sw_FileUpload : Page {
    // LOCAL VARIABLES
    private List<string> Allowed_Extensions = new List<string> { ".txt", ".pdf", ".zip" };
    private bool IsDownloadFile = false;
    private bool IsReleaseNote = false;
    private bool IsUserGuide = false;
    private string FileType = "";
    private string KitNo = "";     // For Release Notes, in order to name the file properly.

    protected void Page_Load(object sender, EventArgs e) {
        try {
            FileType = Request.QueryString["type"];
            IsDownloadFile = (FileType == "DownloadFile");
            IsReleaseNote = (FileType == "ReleaseNote");
            IsUserGuide = (FileType == "UserGuide");   // Was "ReleaseLog"
            KitNo = Request.QueryString["KitNo"] ?? "";
            //Utilities.LogDebug($"FileUpload.PageLoad - IsDownload: {IsDownloadFile}, IsReleaseNote: {IsReleaseNote}, IsUserGuide: {IsUserGuide}, KitNo: {KitNo}");
        } catch (Exception ex) {
            lblError.Text = "There was an error loading this page. The error has been logged, please try again or contact <a href=\"mailto:ServicesPlus@am.sony.com\">ServicesPlus@am.sony.com</a> for assistance.";
            lblError.Text += "<br/><br/>Error Message: " + ex.Message;
            Utilities.WrapExceptionforUI(ex);
        }
    }

    /* ASleight - This section contains the AjaxFileUpload, which is nicer, but has issues with our /e/ path.
                    If you can solve the issue, please do and use this code!
    protected void fileUpload_UploadStart(object sender, AjaxControlToolkit.AjaxFileUploadStartEventArgs e) {
        lblError.Text = "";     // Clear any prior error messages, instead of leaving a message during the upload.
    }

    protected void fileUpload_UploadComplete(object sender, AjaxControlToolkit.AjaxFileUploadEventArgs e) {
        string debugMessage = $"FileUpload.btnUpload.Click - BEGIN at {DateTime.Now.ToShortTimeString()}{Environment.NewLine}";
        try {
            string fileName = e.FileName;
            string extension = Path.GetExtension(fileName).ToLower();   // Get the extension as ".zip", for example.

            // First, check for any issues with the file.
            if (e.FileSize == 0) {
                lblError.Text = "Your file was not uploaded properly, or a file was not selected.";
                return;
            }
            if (!Allowed_Extensions.Contains(extension)) {  // With the FileUpload control only allowing certain extensions, this is slightly redundant, but worth the extra security.
                lblError.Text = "Your file could not be processed. Please upload a .TXT, .PDF or .ZIP file.";
                return;
            }
            debugMessage += $"- FileName: {fileName}, Extension: {extension}{Environment.NewLine}";

            // Save the file to the server
            if (IsDownloadFile) {
                debugMessage += $"- Saving Download File to: {ConfigurationData.Environment.SoftwarePlusConfiguration.SoftwarePLUSDownloadFilepath}\\{fileName}{Environment.NewLine}";
                fileUpload.SaveAs($"{ConfigurationData.Environment.SoftwarePlusConfiguration.SoftwarePLUSDownloadFilepath}\\{fileName}");
            } else if (IsReleaseNote) {
                var fileSplit = fileName.Split('.');
                var fileSuffix = fileSplit[fileSplit.GetUpperBound(0)];     // Gets the final element of the first array dimension
                fileName = $"Release-Notes-{KitNo}.{fileSuffix}";
                debugMessage += $"- Saving Release Note to: {ConfigurationData.Environment.SoftwarePlusConfiguration.SoftwarePLUSReleaseNotes}\\{fileName}{Environment.NewLine}";
                fileUpload.SaveAs($"{ConfigurationData.Environment.SoftwarePlusConfiguration.SoftwarePLUSReleaseNotes}\\{fileName}");
            } else if (IsUserGuide) {
                debugMessage += $"- Saving User Guide to: {ConfigurationData.Environment.SoftwarePlusConfiguration.SoftwarePLUSUserManualsPath}\\{fileName}{Environment.NewLine}";
                fileUpload.SaveAs($"{ConfigurationData.Environment.SoftwarePlusConfiguration.SoftwarePLUSUserManualsPath}\\{fileName}");
            }
            // Call JavaScript to put the new file name in the page that called this one.
            ScriptManager.RegisterStartupScript(this, GetType(), "RefreshParentPage", $"RefreshParentPage('{fileName}','{FileType}');", true);
        } catch (Exception ex) {
            lblError.Text = "There was an error trying to upload your file. The error has been logged, please try again or contact <a href=\"mailto:ServicesPlus@am.sony.com\">ServicesPlus@am.sony.com</a> for assistance.";
            lblError.Text += "<br/><br/>Error Message: " + ex.Message;
            Utilities.WrapExceptionforUI(ex);
        } finally {
            Utilities.LogDebug(debugMessage);
        }
    }*/

    protected void btnUpload_Click(object sender, EventArgs e) {
        string debugMessage = $"FileUpload.btnUpload.Click - BEGIN at {DateTime.Now.ToShortTimeString()}{Environment.NewLine}";

        try {
            string fileName = fileUpload.PostedFile?.FileName;
            string extension = Path.GetExtension(fileName).ToLower();   // Get the extension as ".zip", for example.

            lblError.Text = "";
            if (!fileUpload.HasFile || fileUpload.PostedFile?.ContentLength < 1) {
                lblError.Text = "Please select a file before clicking Upload, or ensure the file is not zero bytes.";
                return;
            }

            debugMessage += $"- FileName: {fileName}, Extension: {extension}{Environment.NewLine}";
            if (Allowed_Extensions.Contains(extension)) {
                if (IsDownloadFile) {
                    debugMessage += $"- Saving Download File to: {ConfigurationData.Environment.SoftwarePlusConfiguration.SoftwarePLUSDownloadFilepath}\\{fileName}{Environment.NewLine}";
                    fileUpload.PostedFile.SaveAs($"{ConfigurationData.Environment.SoftwarePlusConfiguration.SoftwarePLUSDownloadFilepath}\\{fileName}");
                    ClientScript.RegisterStartupScript(Page.GetType(), "RefreshParentPage", $"RefreshParentPage('{fileName}','DownloadFile');", true);
                } else if (IsReleaseNote) {
                    var fileSplit = fileName.Split('.');
                    var fileSuffix = fileSplit[fileSplit.GetUpperBound(0)];
                    fileName = $"Release-Notes-{KitNo}.{fileSuffix}";
                    debugMessage += $"- Saving Release Note to: {ConfigurationData.Environment.SoftwarePlusConfiguration.SoftwarePLUSReleaseNotes}\\{fileName}{Environment.NewLine}";
                    fileUpload.PostedFile.SaveAs($"{ConfigurationData.Environment.SoftwarePlusConfiguration.SoftwarePLUSReleaseNotes}\\{fileName}");
                    ClientScript.RegisterStartupScript(Page.GetType(), "RefreshParentPage", $"RefreshParentPage('{fileName}','ReleaseNote');", true);
                } else if (IsUserGuide) {
                    debugMessage += $"- Saving User Guide to: {ConfigurationData.Environment.SoftwarePlusConfiguration.SoftwarePLUSUserManualsPath}\\{fileName}{Environment.NewLine}";
                    fileUpload.PostedFile.SaveAs($"{ConfigurationData.Environment.SoftwarePlusConfiguration.SoftwarePLUSUserManualsPath}\\{fileName}");
                    ClientScript.RegisterStartupScript(Page.GetType(), "RefreshParentPage", $"RefreshParentPage('{fileName}','UserGuide');", true);
                } else {
                    debugMessage += $"- ERROR deciding which type of file is being uploaded.";
                    lblError.Text = "Error deciding how to use the uploaded file. Please close this window and refresh the SoftwarePlus page.";
                }
            } else {    // Invalid file extension
                debugMessage += "- Invalid file extension: " + extension;
                lblError.Text = "Your file could not be processed. Please upload a .TXT, .PDF or .ZIP file.";
            }

            // Clear the control's data to prepare for the next upload, if any.
            //fileUpload = new FileUpload();
        } catch (Exception ex) {
            lblError.Text = "There was an error trying to upload your file. The error has been logged, please try again or contact <a href=\"mailto:ServicesPlus@am.sony.com\">ServicesPlus@am.sony.com</a> for assistance.";
            lblError.Text += "<br/><br/>Error Message: " + ex.Message;
            Utilities.WrapExceptionforUI(ex);
        } finally {
            Utilities.LogDebug(debugMessage);
        }
    }

    /*  OLD FILE UPLOAD CODE
    protected void btnAddImage_Click(object sender, EventArgs e) {
        try {
            string strFileName;
            HtmlInputFile htmlFile = (HtmlInputFile)ImageFileUpload;
            if (htmlFile.PostedFile.ContentLength > 0) {
                string sFormat = String.Format("{0:#.##}", (float)htmlFile.PostedFile.ContentLength / 2048);
                ViewState["ImageName"] = htmlFile.PostedFile.FileName.Substring(htmlFile.PostedFile.FileName.LastIndexOf("\\") + 1);
                strFileName = ViewState["ImageName"].ToString();

                if (strFileName.EndsWith(".pdf") || strFileName.EndsWith(".txt") || strFileName.EndsWith(".zip")) {
                    //htmlFile.PostedFile.SaveAs(Server.MapPath(strFileName));
                    if (DownloadFilep) {
                        htmlFile.PostedFile.SaveAs(ConfigurationData.Environment.SoftwarePlusConfiguration.SoftwarePLUSDownloadFilepath + "\\" + strFileName);
                    }
                    if (ReleaseNotes) {
                        string docext = Path.GetExtension(strFileName).ToLower();
                        htmlFile.PostedFile.SaveAs(ConfigurationData.Environment.SoftwarePlusConfiguration.SoftwarePLUSReleaseNotes + "\\" + "Release-Notes-" + KitNo + docext);
                    }
                    lblError.Visible = false;
                    System.Threading.Thread.Sleep(2000);
                    //labelFileName.Text = strFileName.ToString();
                    Label1.Visible = true;
                    Label1.Text = "Upload successfull!";

                    //Response.Redirect("KitInformation.aspx?Action=" + ActionName.ToString() + "");
                    if (DownloadFilep)
                        ScriptManager.RegisterStartupScript(this, GetType(), "GetRefershParentPage", "GetRefershParentPage('" + strFileName + "','');", true);
                    if (ReleaseNotes)
                        ScriptManager.RegisterStartupScript(this, GetType(), "GetRefershParentPage", "GetRefershParentPage('','" + strFileName + "');", true);
                } else {
                    Label1.Visible = false;
                    lblError.Visible = true;
                    lblError.Text = "Please check your file. Upload file types must be one of: PDF, TXT or ZIP";
                }
            } else {
                lblError.Visible = true;
                lblError.Text = "Please select a file to upload";
            }
        } catch (Exception ex) {
            lblError.Visible = true;
            lblError.Text = "There was an error trying to upload your file. The error has been logged, please try again or contact <a href=\"mailto:ServicesPlus@am.sony.com\">ServicesPlus@am.sony.com</a> for assistance.";
        }
    }
    */
}
