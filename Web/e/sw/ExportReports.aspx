<%@ Page Title="" Language="C#" MasterPageFile="~/e/sw/SWPlusMaster.master" AutoEventWireup="true" CodeFile="ExportReports.aspx.cs" Inherits="sw_ExportReports" Theme="SkinFile" %>

<%@ Register Assembly="CrystalDecisions.Web" Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="Controls" Namespace="Sony.US.ServicesPLUS.Controls" TagPrefix="SPS" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%--    <asp:UpdatePanel runat="server" ID="UpdateCRReport">
        <ContentTemplate>--%>
    <br />
    <asp:Label ID="lblerror" runat="server" Font-Bold="True" ForeColor="Red" />
    <div>
        <asp:Panel ID="pnlTestReport" runat="server" BorderStyle="Groove" Width="600px">
            <table width="100%">
                <tr>
                    <td>
                        <asp:ListBox ID="lstReportName" runat="server" Height="180px"
                            OnSelectedIndexChanged="lstReportName_SelectedIndexChanged" AutoPostBack="true">
                            <asp:ListItem Text="Internet Models" Value="InternetModels" />
                            <asp:ListItem Text="Internet Kit Items" Value="InternetKitItems" />
                        </asp:ListBox>
                    </td>
                    <td>
                        <asp:Panel ID="pnlInternet" runat="server">
                            <table>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblReleaseType" runat="server" Text="Release Type :" />
                                        <asp:RequiredFieldValidator ID="reqvalreltype" runat="server"
                                            ControlToValidate="ddlReleaseType" Display="Dynamic"
                                            ErrorMessage="Select Release Type" SetFocusOnError="True" Text="*" />
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlReleaseType" runat="server" Width="135px" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblDistribution" runat="server" Text="Distribution :" />
                                        <asp:RequiredFieldValidator ID="reqvaldist" runat="server"
                                            ControlToValidate="ddlDistribution" Display="Dynamic"
                                            ErrorMessage="Select Distribution" SetFocusOnError="True" Text="*" />
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlDistribution" runat="server" Width="135px" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblVisibility" runat="server" Text="Visibility :" />
                                        <asp:RequiredFieldValidator ID="reqvalvisib" runat="server"
                                            ControlToValidate="ddlVisibility" Display="Dynamic"
                                            ErrorMessage="Select Visibility" SetFocusOnError="True" Text="*" />
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlVisibility" runat="server" Width="135px" />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </td>
                    <td align="right">
                        <asp:ImageButton ID="btnSubmit" runat="server"
                            ImageUrl="~/Shared/Images/sw_submit.jpg"
                            ImageAlign="AbsMiddle" OnClick="btnSubmit_Click" />
                    </td>
                </tr>
            </table>
        </asp:Panel>
    </div>
    <br />
    <%--            <asp:Panel ID="panelUpdateProgress" runat="server">
                <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdateCRReport" DisplayAfter="0">
                    <ProgressTemplate>
                        <div style="position: relative; top: 30%; text-align: center;">
                        <img src="../../Shared/Images/progbar.gif"  style="vertical-align: middle" alt="Processing" />
                        Processing ...
                        </div>
                    </ProgressTemplate>
                </asp:UpdateProgress>
            </asp:Panel>--%>
    <asp:ValidationSummary ID="ValidationSummary1"
        runat="server" ShowMessageBox="True" ShowSummary="False" />
    <asp:Label ID="lblReportStatus" runat="server" Font-Bold="True" ForeColor="Red" />
    <div id="divReport">
        <asp:Panel ID="pnlCRReport" runat="server">
            <CR:CrystalReportViewer ID="CrystalReportViewer1" runat="server" AutoDataBind="true" />  <%--Obsolete: DisplayGroupTree="False"--%>
        </asp:Panel>
    </div>
    <%--        </ContentTemplate>
    </asp:UpdatePanel>--%>
</asp:Content>

