﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="FileUpload.aspx.cs" Inherits="e_sw_FileUpload" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head runat="server">
    <title>File Upload</title>
    <link href="../includes/ServicesPLUS_style.css" type="text/css" rel="stylesheet" />

    <script language="javascript" type="text/javascript">
        function RefreshParentPage(fileName, fileType) {
            var controlName = ""

            if (fileType === "DownloadFile") {
                controlName = "ContentPlaceHolder1_hdnDownloadFile";
            } else if (fileType === "ReleaseNote") {
                controlName = "ContentPlaceHolder1_hdnReleaseNote";
            } else if (fileType === "UserGuide") {
                controlName = "ContentPlaceHolder1_hdnUserGuide";
            } else {
                window.alert("Unknown fileType parameter: " + fileType);
            }
            window.alert("File Type: " + fileType + ", Old Control Value: " + window.opener.document.getElementById(controlName).value);
            window.opener.document.getElementById(controlName).value = fileName;
            window.alert("New Control Value: " + window.opener.document.getElementById(controlName).value);

            window.opener.__doPostBack();
            window.opener = self;
            window.close();
        }

        function HandleUploadError(sender, e) {
            var errorLabel = document.getElementById('lblError');
            var message = document.createTextNode("Upload Error Message: " + e.message);
            errorLabel.appendChild(message);
        }
    </script>
</head>

<body>
    <div class="box_scale">
        <form id="form1" runat="server">
            <asp:ScriptManager ID="ScriptManager1" runat="server" />
            <table style="background: white; border: 4px solid black; width: 400px; padding: 10px;">
                <tr>
                    <td colspan="2" style="text-align: center;">
                        <h2>File Upload</h2>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:FileUpload ID="fileUpload" AllowMultiple="false" runat="server" Width="300px" />
                    </td>
                    <td>
                        <asp:Button ID="btnUpload" OnClick="btnUpload_Click" runat="server" Text="Upload File" />
                    </td>
                    <%-- This is a better File Upload visually, but it has problems with our /e/ path and causes errors.
                        <td colspan="2">
                        <ajaxToolkit:AjaxFileUpload ID="fileUpload" MaximumNumberOfFiles="1" AllowedFileTypes="pdf,txt,zip" AutoStartUpload="true" Width="600"
                            OnUploadComplete="fileUpload_UploadComplete" OnUploadStart="fileUpload_UploadStart" runat="server" />
                    </td>--%>
                </tr>
                <tr>
                    <td colspan="2" style="text-align: center;">
                        <asp:Label ID="lblError" CssClass="redAsterick" runat="server" />
                    </td>
                </tr>
            </table>
        </form>
    </div>
</body>
</html>
