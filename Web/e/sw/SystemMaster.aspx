<%@ Page Language="C#" MasterPageFile="~/e/sw/SWPlusMaster.master" AutoEventWireup="true"CodeFile="SystemMaster.aspx.cs" Inherits="sw_SystemMaster" Title="Lookup Tables" Theme="SkinFile" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="Controls" Namespace="Sony.US.ServicesPLUS.Controls" TagPrefix="SPS" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
    <meta name="vs_defaultClientScript" content="JavaScript">
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">

    <script type="text/javascript">
        function ConfirmationBox(strPRODUCT_CATEGORY) {

            var result = confirm('Are you sure you want to delete ' + strPRODUCT_CATEGORY + ' Details?');
            if (result) {

                return true;
            }
            else {
                return false;
            }
        }
        function ConfirmationBoxcountry(strCOUNTRY) {

            var result = confirm('Are you sure you want to delete ' + strCOUNTRY + ' Details?');
            if (result) {

                return true;
            }
            else {
                return false;
            }
        }
        function ConfirmationBoxmodelprodcat(strMODEL_ID) {

            var result = confirm('Are you sure you want to delete ' + strMODEL_ID + ' Details?');
            if (result) {

                return true;
            }
            else {
                return false;
            }
        }
        function ConfirmationBoxpostitle(strposition_title) {

            var result = confirm('Are you sure you want to delete ' + strposition_title + ' Details?');
            if (result) {

                return true;
            }
            else {
                return false;
            }
        }
        function ConfirmationBoxregstate(strregSTATE) {

            var result = confirm('Are you sure you want to delete ' + strregSTATE + ' Details?');
            if (result) {

                return true;
            }
            else {
                return false;
            }
        }
        function ConfirmationBoxstate(strstSTATE) {

            var result = confirm('Are you sure you want to delete ' + strstSTATE + ' Details?');
            if (result) {

                return true;
            }
            else {
                return false;
            }
        }
    </script>

   <%-- <asp:UpdatePanel ID="Updatepnlsysmaster" runat="server">
        <ContentTemplate>--%>
            <table border="0" style="width: 100%">
             
                <tr>
                    <td colspan="2">
                        <asp:Label ID="lblerror" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label>
                    </td>
                </tr>
                <tr><td><table>
                <tr>
                    <td style="width: 100px">
                        <asp:Label ID="lbllookup" runat="server" Text="Lookup Table:"></asp:Label>
                    </td>
                    <td align="left" width="250">
                        <asp:DropDownList ID="ddlllokup" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlllokup_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                </tr>
                
                <tr>
                    <%--<td style="width: 100px">
                        <asp:Panel ID="panelUpdateProgress" runat="server">
                            <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="Updatepnlsysmaster"
                                DisplayAfter="0">
                                <ProgressTemplate>
                                    <div style="position: relative; top: 30%; text-align: center;">
                                        <img src="../../Shared/Images/progbar.gif" style="vertical-align: middle" alt="Processing" />
                                        Processing ...
                                    </div>
                                </ProgressTemplate>
                            </asp:UpdateProgress>
                        </asp:Panel>
                    </td>--%>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td style="width: 100px">
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                 </table></td></tr>
                <tr>
                    <td colspan="2">
                        <asp:MultiView ID="MultiView1" runat="server">
                            <asp:View ID="vwcategories" runat="server">
                                <asp:Panel ID="pnlgvcategories" runat="server" Height="450px" 
                                    ScrollBars="Vertical" Width="300px">
                                  <asp:GridView ID="gvcategories" runat="server" DataKeyNames="PRODUCT_CATEGORY"
                                    AllowSorting="True" AutoGenerateColumns="False" CellPadding="4" ShowFooter="true"
                                    ForeColor="#333333" GridLines="None" OnPageIndexChanging="gvcategories_PageIndexChanging"
                                    OnRowCancelingEdit="gvcategories_RowCancelingEdit" OnRowDeleting="gvcategories_RowDeleting"
                                    OnRowEditing="gvcategories_RowEditing" OnRowUpdating="gvcategories_RowUpdating"
                                    OnRowCommand="gvcategories_RowCommand" 
                                        OnRowDataBound="gvcategories_RowDataBound" PageSize="50" 
                                        onprerender="gvcategories_PreRender">
                                    <RowStyle BackColor="#EFF3FB" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="Product Category">
                                            <EditItemTemplate>
                                                <SPS:SPSTextBox ID="txteditcategory" runat="server" Text='<%# Eval("PRODUCT_CATEGORY") %>'
                                                    MaxLength="40"></SPS:SPSTextBox><asp:FilteredTextBoxExtender ID="txteditcategory_FilteredTextBoxExtender"
                                                        runat="server" Enabled="True" FilterType="Custom, Numbers, UppercaseLetters, LowercaseLetters"
                                                        ValidChars="/.&, " TargetControlID="txteditcategory">
                                                    </asp:FilteredTextBoxExtender>
                                                <asp:RequiredFieldValidator ID="reqeditcat" runat="server" 
                                                    ControlToValidate="txteditcategory" Display="Dynamic" 
                                                    ErrorMessage="Enter Product Category" SetFocusOnError="True" 
                                                    ValidationGroup="validationeditcat">*</asp:RequiredFieldValidator>
                                            </EditItemTemplate>
                                            <FooterTemplate>
                                                <SPS:SPSTextBox ID="txtnewcategory" runat="server" MaxLength="40"></SPS:SPSTextBox><asp:FilteredTextBoxExtender
                                                    ID="txtnewcategory_FilteredTextBoxExtender" runat="server" Enabled="True" FilterType="Custom, Numbers, UppercaseLetters, LowercaseLetters"
                                                    ValidChars=".&, " TargetControlID="txtnewcategory">
                                                </asp:FilteredTextBoxExtender>
                                                <asp:RequiredFieldValidator ID="reqnewcat" runat="server" 
                                                    ErrorMessage="Enter Product Category" ControlToValidate="txtnewcategory" 
                                                    Display="Dynamic" SetFocusOnError="True" ValidationGroup="validationnewcat">*</asp:RequiredFieldValidator></FooterTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="Label2" runat="server" Text='<%# Eval("PRODUCT_CATEGORY") %>'></asp:Label></ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <EditItemTemplate>
                                                <asp:ImageButton ID="imgbtnUpdate" CommandName="Update" runat="server" ImageUrl="~/Shared/images/update.jpg"
                                                    ToolTip="Update" Height="15px" Width="15px" 
                                                    ValidationGroup="validationeditcat" /><asp:ImageButton ID="imgbtnCancel"
                                                        runat="server" CommandName="Cancel" 
                                                    ImageUrl="~/Shared/Images/Cancel.jpg" ToolTip="Cancel"
                                                        Height="15px" Width="15px" CausesValidation="False" /></EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:ImageButton ID="imgbtnEdit" CommandName="Edit" runat="server" ImageUrl="~/Shared/Images/Edit.jpg"
                                                    ToolTip="Edit" Height="15px" Width="15px" CausesValidation="False" />
                                                <asp:ImageButton ID="imgbtnDelete" CommandName="Delete"
                                                        Text="Edit" runat="server" ImageUrl="~/Shared/Images/delete.jpg" ToolTip="Delete"
                                                        Height="15px" Width="15px" CausesValidation="False" /></ItemTemplate>
                                            <FooterTemplate>
                                                <asp:ImageButton ID="imgbtnAdd" runat="server" ImageUrl="~/Shared/Images/AddNewitem.jpg"
                                                    CommandName="AddNew" Width="30px" Height="30px" ToolTip="Add new Product Category"
                                                    ValidationGroup="validationnewcat" /></FooterTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                    <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                    <EditRowStyle BackColor="#2461BF" />
                                    <AlternatingRowStyle BackColor="White" />
                                </asp:GridView>
                                </asp:Panel>
                            </asp:View>
                            <asp:View ID="vwcountries" runat="server">
                            <asp:Panel ID="pnlcountries" runat="server" Height="450px" 
                                    ScrollBars="Vertical" Width="300px">
                                <asp:GridView ID="gvcountries" runat="server" DataKeyNames="COUNTRY"
                                    AllowSorting="True" AutoGenerateColumns="False" CellPadding="4" ShowFooter="true"
                                    ForeColor="#333333" GridLines="None" OnPageIndexChanging="gvcountries_PageIndexChanging"
                                    OnRowCancelingEdit="gvcountries_RowCancelingEdit" OnRowDeleting="gvcountries_RowDeleting"
                                    OnRowEditing="gvcountries_RowEditing" OnRowUpdating="gvcountries_RowUpdating"
                                    OnRowCommand="gvcountries_RowCommand" 
                                    OnRowDataBound="gvcountries_RowDataBound" 
                                    onprerender="gvcountries_PreRender">
                                    <RowStyle BackColor="#EFF3FB" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="Country">
                                            <EditItemTemplate>
                                                <SPS:SPSTextBox ID="txteditcountry" runat="server" Text='<%# Eval("COUNTRY") %>'
                                                    MaxLength="20"></SPS:SPSTextBox><asp:FilteredTextBoxExtender ID="txteditcountry_FilteredTextBoxExtender"
                                                        runat="server" Enabled="True" FilterType="Custom, Numbers, UppercaseLetters, LowercaseLetters"
                                                        ValidChars="& " TargetControlID="txteditcountry">
                                                    </asp:FilteredTextBoxExtender>
                                                <asp:RequiredFieldValidator ID="reqeditcountry" runat="server" 
                                                    ControlToValidate="txteditcountry" Display="Dynamic" 
                                                    ErrorMessage="Enter Country" SetFocusOnError="True" 
                                                    ValidationGroup="validationeditcat">*</asp:RequiredFieldValidator>
                                            </EditItemTemplate>
                                            <FooterTemplate>
                                                <SPS:SPSTextBox ID="txtnewcountry" runat="server" MaxLength="20"></SPS:SPSTextBox><asp:FilteredTextBoxExtender
                                                    ID="txtnewcountry_FilteredTextBoxExtender" runat="server" Enabled="True" FilterType="Custom, Numbers, UppercaseLetters, LowercaseLetters"
                                                    ValidChars="& " TargetControlID="txtnewcountry">
                                                </asp:FilteredTextBoxExtender>
                                                <asp:RequiredFieldValidator ID="reqnewcountry" runat="server" 
                                                    ControlToValidate="txtnewcountry" Display="Dynamic" 
                                                    ErrorMessage="Enter Country" SetFocusOnError="True" 
                                                    ValidationGroup="validationnewcat">*</asp:RequiredFieldValidator>
                                            </FooterTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="Label2" runat="server" Text='<%# Eval("COUNTRY") %>'></asp:Label></ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <EditItemTemplate>
                                                <asp:ImageButton ID="imgbtnUpdate" CommandName="Update" runat="server" ImageUrl="~/Shared/images/update.jpg"
                                                    ToolTip="Update" Height="15px" Width="15px" 
                                                    ValidationGroup="validationeditcat" /><asp:ImageButton ID="imgbtnCancel"
                                                        runat="server" CommandName="Cancel" 
                                                    ImageUrl="~/Shared/Images/Cancel.jpg" ToolTip="Cancel"
                                                        Height="15px" Width="15px" CausesValidation="False" /></EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:ImageButton ID="imgbtnEdit" CommandName="Edit" runat="server" ImageUrl="~/Shared/Images/Edit.jpg"
                                                    ToolTip="Edit" Height="15px" Width="15px" CausesValidation="False" />
                                                <asp:ImageButton ID="imgbtnDelete" CommandName="Delete"
                                                        Text="Edit" runat="server" ImageUrl="~/Shared/Images/delete.jpg" ToolTip="Delete"
                                                        Height="15px" Width="15px" CausesValidation="False" /></ItemTemplate>
                                            <FooterTemplate>
                                                <asp:ImageButton ID="imgbtnAdd" runat="server" ImageUrl="~/Shared/Images/AddNewitem.jpg"
                                                    CommandName="AddNew" Width="30px" Height="30px" ToolTip="Add new Country" 
                                                    ValidationGroup="validationnewcat" /></FooterTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                    <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                    <EditRowStyle BackColor="#2461BF" />
                                    <AlternatingRowStyle BackColor="White" />
                                </asp:GridView>
                                </asp:Panel>
                            </asp:View>
                            <asp:View ID="vwmodelprodcat" runat="server">
                            <asp:Panel ID="pnlmodelprodcat" runat="server" Height="450px" 
                                    ScrollBars="Vertical" Width="450px">
                                <asp:GridView ID="gvmodelprodcat" runat="server" DataKeyNames="MODEL_ID,PRODUCT_CATEGORY"
                                    AllowSorting="True" AutoGenerateColumns="False" CellPadding="4" ShowFooter="true"
                                    ForeColor="#333333" GridLines="None" OnPageIndexChanging="gvmodelprodcat_PageIndexChanging"
                                    OnRowCancelingEdit="gvmodelprodcat_RowCancelingEdit" OnRowDeleting="gvmodelprodcat_RowDeleting"
                                    OnRowEditing="gvmodelprodcat_RowEditing" OnRowUpdating="gvmodelprodcat_RowUpdating"
                                    OnRowCommand="gvmodelprodcat_RowCommand" 
                                    OnRowDataBound="gvmodelprodcat_RowDataBound" 
                                    onprerender="gvmodelprodcat_PreRender">
                                    <RowStyle BackColor="#EFF3FB" />
                                    <Columns>
                                        
                                        <asp:TemplateField HeaderText="Model ID">
                                            <EditItemTemplate>
                                                <SPS:SPSTextBox ID="txteditmodelid" runat="server" Text='<%# Eval("MODEL_ID") %>'
                                                    MaxLength="15"></SPS:SPSTextBox><asp:FilteredTextBoxExtender ID="txteditmodelid_FilteredTextBoxExtender"
                                                        runat="server" Enabled="True" FilterType="Custom, Numbers, UppercaseLetters, LowercaseLetters"
                                                        ValidChars="/.&, " TargetControlID="txteditmodelid">
                                                    </asp:FilteredTextBoxExtender>
                                                <asp:RequiredFieldValidator ID="reqeditmodelid" runat="server" 
                                                    ControlToValidate="txteditmodelid" Display="Dynamic" 
                                                    ErrorMessage="Enter Model ID" SetFocusOnError="True" 
                                                    ValidationGroup="validationeditcat"></asp:RequiredFieldValidator>
                                            </EditItemTemplate>
                                            <FooterTemplate>
                                                <SPS:SPSTextBox ID="txtnewmodelid" runat="server" MaxLength="15"></SPS:SPSTextBox><asp:FilteredTextBoxExtender
                                                    ID="txtnewmodelid_FilteredTextBoxExtender" runat="server" Enabled="True" FilterType="Custom, Numbers, UppercaseLetters, LowercaseLetters"
                                                    ValidChars="/.&, " TargetControlID="txtnewmodelid">
                                                </asp:FilteredTextBoxExtender>
                                                <asp:RequiredFieldValidator ID="reqnewmodelid" runat="server" 
                                                    ControlToValidate="txtnewmodelid" Display="Dynamic" 
                                                    ErrorMessage="Enter Model ID" SetFocusOnError="True" 
                                                    ValidationGroup="validationnewcat">*</asp:RequiredFieldValidator>
                                            </FooterTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblmodelid" runat="server" Text='<%# Eval("MODEL_ID") %>'></asp:Label></ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Product Category">
                                            <EditItemTemplate>
                                                <SPS:SPSTextBox ID="txteditprodcat" runat="server" Text='<%# Eval("PRODUCT_CATEGORY") %>'
                                                    MaxLength="40"></SPS:SPSTextBox><asp:FilteredTextBoxExtender ID="txteditprodcat_FilteredTextBoxExtender"
                                                        runat="server" Enabled="True" FilterType="Custom, Numbers, UppercaseLetters, LowercaseLetters"
                                                        ValidChars=".&, " TargetControlID="txteditprodcat">
                                                    </asp:FilteredTextBoxExtender>
                                            </EditItemTemplate>
                                            <FooterTemplate>
                                                <SPS:SPSTextBox ID="txtnewprodcat" runat="server" MaxLength="40"></SPS:SPSTextBox><asp:FilteredTextBoxExtender
                                                    ID="txtnewprodcat_FilteredTextBoxExtender" runat="server" Enabled="True" FilterType="Custom, Numbers, UppercaseLetters, LowercaseLetters"
                                                    ValidChars=".&, " TargetControlID="txtnewprodcat">
                                                </asp:FilteredTextBoxExtender>
                                            </FooterTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="Label2" runat="server" Text='<%# Eval("PRODUCT_CATEGORY") %>'></asp:Label></ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <EditItemTemplate>
                                                <asp:ImageButton ID="imgbtnUpdate" CommandName="Update" runat="server" ImageUrl="~/Shared/images/update.jpg"
                                                    ToolTip="Update" Height="15px" Width="15px" 
                                                    ValidationGroup="validationeditcat" /><asp:ImageButton ID="imgbtnCancel"
                                                        runat="server" CommandName="Cancel" 
                                                    ImageUrl="~/Shared/Images/Cancel.jpg" ToolTip="Cancel"
                                                        Height="15px" Width="15px" CausesValidation="False" /></EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:ImageButton ID="imgbtnEdit" CommandName="Edit" runat="server" ImageUrl="~/Shared/Images/Edit.jpg"
                                                    ToolTip="Edit" Height="15px" Width="15px" CausesValidation="False" />
                                                <asp:ImageButton ID="imgbtnDelete" CommandName="Delete"
                                                        Text="Edit" runat="server" ImageUrl="~/Shared/Images/delete.jpg" ToolTip="Delete"
                                                        Height="15px" Width="15px" CausesValidation="False" /></ItemTemplate>
                                            <FooterTemplate>
                                                <asp:ImageButton ID="imgbtnAdd" runat="server" ImageUrl="~/Shared/Images/AddNewitem.jpg"
                                                    CommandName="AddNew" Width="30px" Height="30px" ToolTip="Add new Model and Product Category"
                                                    ValidationGroup="validationnewcat" /></FooterTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                    <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                    <EditRowStyle BackColor="#2461BF" />
                                    <AlternatingRowStyle BackColor="White" />
                                </asp:GridView>
                                </asp:Panel>
                            </asp:View>
                            <asp:View ID="vwstates" runat="server">
                            <asp:Panel ID="pnlstates" runat="server" Height="450px" 
                                    ScrollBars="Vertical" Width="450">
                                <asp:GridView ID="gvstates" runat="server" DataKeyNames="STATE,STATE_NAME"
                                    AllowSorting="True" AutoGenerateColumns="False" CellPadding="4" ShowFooter="true"
                                    ForeColor="#333333" GridLines="None" OnPageIndexChanging="gvstates_PageIndexChanging"
                                    OnRowCancelingEdit="gvstates_RowCancelingEdit" OnRowDeleting="gvstates_RowDeleting"
                                    OnRowEditing="gvstates_RowEditing" OnRowUpdating="gvstates_RowUpdating" OnRowCommand="gvstates_RowCommand"
                                    OnRowDataBound="gvstates_RowDataBound" onprerender="gvstates_PreRender">
                                    <RowStyle BackColor="#EFF3FB" />
                                    <Columns>
                                        
                                        <asp:TemplateField HeaderText="State">
                                            <EditItemTemplate>
                                                <SPS:SPSTextBox ID="txteditststate" runat="server" Text='<%# Eval("STATE") %>' MaxLength="2"></SPS:SPSTextBox><asp:FilteredTextBoxExtender
                                                    ID="txteditststate_FilteredTextBoxExtender" runat="server" Enabled="True" FilterType="Custom, Numbers, UppercaseLetters, LowercaseLetters"
                                                    ValidChars="& " TargetControlID="txteditststate">
                                                </asp:FilteredTextBoxExtender>
                                                <asp:RequiredFieldValidator ID="reqeditstate" runat="server" 
                                                    ControlToValidate="txteditststate" Display="Dynamic" ErrorMessage="Enter State" 
                                                    SetFocusOnError="True" ValidationGroup="validationeditcat">*</asp:RequiredFieldValidator>
                                            </EditItemTemplate>
                                            <FooterTemplate>
                                                <SPS:SPSTextBox ID="txtnewststate" runat="server" MaxLength="2"></SPS:SPSTextBox><asp:FilteredTextBoxExtender
                                                    ID="txtnewststate_FilteredTextBoxExtender" runat="server" Enabled="True" FilterType="Custom, Numbers, UppercaseLetters, LowercaseLetters"
                                                    ValidChars="& " TargetControlID="txtnewststate">
                                                </asp:FilteredTextBoxExtender>
                                                <asp:RequiredFieldValidator ID="reqnewstate" runat="server" 
                                                    ControlToValidate="txtnewststate" Display="Dynamic" ErrorMessage="Enter State" 
                                                    SetFocusOnError="True" ValidationGroup="validationnewcat">*</asp:RequiredFieldValidator>
                                            </FooterTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblststate" runat="server" Text='<%# Eval("STATE") %>'></asp:Label></ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="State Name">
                                            <EditItemTemplate>
                                                <SPS:SPSTextBox ID="txteditstatename" runat="server" Text='<%# Eval("STATE_NAME") %>'
                                                    MaxLength="25"></SPS:SPSTextBox><asp:FilteredTextBoxExtender ID="txteditstatename_FilteredTextBoxExtender"
                                                        runat="server" Enabled="True" FilterType="Custom, Numbers, UppercaseLetters, LowercaseLetters"
                                                        ValidChars="& " TargetControlID="txteditstatename">
                                                    </asp:FilteredTextBoxExtender>
                                                <asp:RequiredFieldValidator ID="reqeditstatename" runat="server" 
                                                    ControlToValidate="txteditstatename" Display="Dynamic" 
                                                    ErrorMessage="Enter State Name" SetFocusOnError="True" 
                                                    ValidationGroup="validationeditcat">*</asp:RequiredFieldValidator>
                                            </EditItemTemplate>
                                            <FooterTemplate>
                                                <SPS:SPSTextBox ID="txtnewstatename" runat="server" MaxLength="25"></SPS:SPSTextBox><asp:FilteredTextBoxExtender
                                                    ID="txtnewstatename_FilteredTextBoxExtender" runat="server" Enabled="True" FilterType="Custom, Numbers, UppercaseLetters, LowercaseLetters"
                                                    ValidChars="& " TargetControlID="txtnewstatename">
                                                </asp:FilteredTextBoxExtender>
                                                <asp:RequiredFieldValidator ID="reqnewstatename" runat="server" 
                                                    ControlToValidate="txtnewstatename" Display="Dynamic" 
                                                    ErrorMessage="Enter State Name" SetFocusOnError="True" 
                                                    ValidationGroup="validationnewcat">*</asp:RequiredFieldValidator>
                                            </FooterTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblstatename" runat="server" Text='<%# Eval("STATE_NAME") %>'></asp:Label></ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <EditItemTemplate>
                                                <asp:ImageButton ID="imgbtnUpdate" CommandName="Update" runat="server" ImageUrl="~/Shared/images/update.jpg"
                                                    ToolTip="Update" Height="15px" Width="15px" 
                                                    ValidationGroup="validationeditcat" /><asp:ImageButton ID="imgbtnCancel"
                                                        runat="server" CommandName="Cancel" 
                                                    ImageUrl="~/Shared/Images/Cancel.jpg" ToolTip="Cancel"
                                                        Height="15px" Width="15px" CausesValidation="False" /></EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:ImageButton ID="imgbtnEdit" CommandName="Edit" runat="server" ImageUrl="~/Shared/Images/Edit.jpg"
                                                    ToolTip="Edit" Height="15px" Width="15px" CausesValidation="False" />
                                                <asp:ImageButton ID="imgbtnDelete" CommandName="Delete"
                                                        Text="Edit" runat="server" ImageUrl="~/Shared/Images/delete.jpg" ToolTip="Delete"
                                                        Height="15px" Width="15px" CausesValidation="False" /></ItemTemplate>
                                            <FooterTemplate>
                                                <asp:ImageButton ID="imgbtnAdd" runat="server" ImageUrl="~/Shared/Images/AddNewitem.jpg"
                                                    CommandName="AddNew" Width="30px" Height="30px" ToolTip="Add new State and State Name"
                                                    ValidationGroup="validationnewcat" /></FooterTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                    <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                    <EditRowStyle BackColor="#2461BF" />
                                    <AlternatingRowStyle BackColor="White" />
                                </asp:GridView>
                                </asp:Panel>
                            </asp:View>
                        </asp:MultiView>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table border="0" style="width: 100%">
                            <tr>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" 
                                        ShowMessageBox="True" ShowSummary="False" ValidationGroup="validationnewcat" />
                                </td>
                                <td>
                                    <asp:ValidationSummary ID="ValidationSummary2" runat="server" 
                                        ShowMessageBox="True" ShowSummary="False" ValidationGroup="validationeditcat" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
       <%-- </ContentTemplate>
    </asp:UpdatePanel>--%>
</asp:Content>
