﻿using System;
using System.Web.UI;

public partial class sw_SWPlusMaster : MasterPage {
    private string userId = string.Empty;
    //SoftwarePlusConfigManager configMgr;
    //UsersDataManager usermgr;

    protected void Page_Load(object sender, EventArgs e) {
        if (!IsPostBack) {
            string sysrole;
            lbldttime.Text = DateTime.Now.ToLongDateString();
            if (Session["UserName"] != null) {
                if (Session["Role"].ToString() == "SoftwareAdmin") {
                    sysrole = "Administrator";
                } else if (Session["Role"].ToString() == "SoftwareEngineer") {
                    sysrole = "Engineer";
                } else {
                    sysrole = "Guest";
                }

                lblusername.Text = "Welcome " + Session["UserName"].ToString() + ", " + sysrole;
            }
        }
    }

    protected void Page_PreInit(object sender, EventArgs e) {
        // This is necessary because Safari and Chrome browsers don't display the Menu control correctly.
        // All webpages displaying an ASP.NET menu control must inherit this class.
        if (Request.ServerVariables["http_user_agent"].IndexOf("Safari", StringComparison.CurrentCultureIgnoreCase) != -1)
            Page.ClientTarget = "uplevel";
    }
}
