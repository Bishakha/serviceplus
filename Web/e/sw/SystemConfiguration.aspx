<%@ Page Language="C#" MasterPageFile="~/e/sw/SWPlusMaster.master" AutoEventWireup="true" CodeFile="SystemConfiguration.aspx.cs"
 Inherits="sw_SystemConfiguration" Title="System Configuration" Theme="SkinFile" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="Controls" Namespace="Sony.US.ServicesPLUS.Controls" TagPrefix="SPS" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <%--<asp:UpdatePanel ID="Updatepnlsysconfig" runat="server">
<ContentTemplate>--%>
<table border="0" style="width: 100%">
<tr>
<td style="width: 4px">
</td>
<td>
<table border="0" style="width: 100%">
<tr><td><asp:Label ID="lblerror" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label></td></tr>
<tr>
<td>
<fieldset>
<legend class="legendlabel">Configuration Data</legend>
 <table border="0" style="width: 100%">
 <tr><td style="width: 150px">
 
     <asp:Label ID="lblcompanyname" runat="server" Text="Company Name:"></asp:Label>
 
 </td>
     <td>
         <SPS:SPSTextBox ID="txtcompanyname" runat="server" Enabled="False" 
             Width="200px">Sony Electronics Inc.</SPS:SPSTextBox>
     </td>
     <td>
         <asp:Label ID="lbladd1" runat="server" Text="Address1:"></asp:Label>
     </td>
     <td>
         <SPS:SPSTextBox ID="txtadd1" runat="server" MaxLength="40" Width="200px"></SPS:SPSTextBox>
     </td>
     </tr>
     <tr>
         <td style="width: 150px">
             <asp:Label ID="lbladd2" runat="server" Text="Address2:"></asp:Label>
         </td>
         <td>
             <SPS:SPSTextBox ID="txtadd2" runat="server" MaxLength="40" Width="200px"></SPS:SPSTextBox>
         </td>
         <td>
             <asp:Label ID="lblcity" runat="server" Text="City:"></asp:Label>
         </td>
         <td>
             <SPS:SPSTextBox ID="txtcity" runat="server" MaxLength="25" Width="200px"></SPS:SPSTextBox>
         </td>
     </tr>
     <tr>
         <td style="width: 150px">
             <asp:Label ID="Label4" runat="server" Text="State:"></asp:Label>
         </td>
         <td>
            <asp:ComboBox ID="ddlstate" runat="server"  DropDownStyle="DropDownList" AutoCompleteMode="SuggestAppend" MaxLength="0">
            </asp:ComboBox>
         </td>
         <td>
             <asp:Label ID="lblpin" runat="server" Text="Zip:"></asp:Label>
         </td>
         <td>
             <SPS:SPSTextBox ID="txtpin" runat="server" MaxLength="10" Width="200px"></SPS:SPSTextBox>
         </td>
     </tr>
     <tr>
         <td style="width: 150px">
             <asp:Label ID="lblcountry" runat="server" Text="Country:"></asp:Label>
         </td>
         <td>
            <asp:ComboBox ID="ddlcountry" runat="server"  DropDownStyle="DropDownList" AutoCompleteMode="SuggestAppend" MaxLength="0">
            </asp:ComboBox>
         </td>
         <td>
             <asp:Label ID="lbloffphone" runat="server" Text="Office Phone:"></asp:Label>
         </td>
         <td>
             <SPS:SPSTextBox ID="txtoffphone" runat="server" MaxLength="10" Width="200px"></SPS:SPSTextBox>
         </td>
     </tr>
     <tr>
         <td style="width: 150px">
             <asp:Label ID="lblfaxnum" runat="server" Text="Fax Number:"></asp:Label>
         </td>
         <td>
             <SPS:SPSTextBox ID="txtfaxnum" runat="server" MaxLength="10" Width="200px"></SPS:SPSTextBox>
         </td>
         <td>
             <asp:Label ID="lblinternetadd" runat="server" Text="Internet Address:"></asp:Label>
         </td>
         <td>
             <SPS:SPSTextBox ID="txtinternetadd" runat="server" MaxLength="50" Width="200px"></SPS:SPSTextBox>
         </td>
     </tr>
     <tr>
         <td colspan="4">
             <asp:Label ID="lblcust" runat="server" CssClass="legendlabel" 
                 Text="Customer Upgrade Report"></asp:Label>
         </td>
     </tr>
     <tr>
         <td style="width: 150px">
             <asp:Label ID="lblrptfaxnum" runat="server" Text="Report Fax Number:"></asp:Label>
         </td>
         <td>
             <SPS:SPSTextBox ID="txtrptfaxnum" runat="server" MaxLength="10" Width="200px"></SPS:SPSTextBox>
         </td>
         <td>
             <asp:Label ID="lblrepphnum" runat="server" Text="Report Phone Number:"></asp:Label>
         </td>
         <td>
             <SPS:SPSTextBox ID="txtrepphnum" runat="server" MaxLength="10" Width="200px"></SPS:SPSTextBox>
         </td>
     </tr>
     <tr>
         <td style="width: 150px">
             <asp:Label ID="lblrepopt" runat="server" Text="Option:"></asp:Label>
         </td>
         <td>
             <SPS:SPSTextBox ID="txtrepoption" runat="server" MaxLength="5" Width="200px"></SPS:SPSTextBox>
         </td>
         <td>
             <asp:HiddenField ID="hideseccheck" runat="server" />
         </td>
         <td>
             &nbsp;</td>
     </tr>
 </table>
 </fieldset>
</td>
</tr>
<tr><td>
<asp:Panel ID="pnlsftpdetails" runat="server" Visible="False">
<fieldset>
<legend class="legendlabel">SFTP Details</legend>
 <table border="0" style="width: 100%">
 <tr><td style="width: 150px">
 
     <asp:Label ID="lblftpuserid" runat="server" Text="SFTP User ID:"></asp:Label>
 
     <asp:RequiredFieldValidator ID="reqvaluserid" runat="server" 
         ControlToValidate="txtftpuserid" Display="Dynamic" 
         ErrorMessage="Enter SFTP User ID" SetFocusOnError="True">*</asp:RequiredFieldValidator>
 
 </td>
     <td>
         <SPS:SPSTextBox ID="txtftpuserid" runat="server" MaxLength="21" Width="150px"></SPS:SPSTextBox>
     </td>
     <td>
         <asp:Label ID="lblftppwd" runat="server" Text="SFTP Password:"></asp:Label>
         <asp:RequiredFieldValidator ID="reqvalpwd" runat="server" 
             ControlToValidate="txtftppwd" Display="Dynamic" 
             ErrorMessage="Enter SFTP Password" SetFocusOnError="True">*</asp:RequiredFieldValidator>
     </td>
     <td>
         <SPS:SPSTextBox ID="txtftppwd" runat="server" MaxLength="21" Width="150px" 
             TextMode="Password"></SPS:SPSTextBox>
     </td>
     </tr>
     <tr>
         <td style="width: 150px">
             <asp:Label ID="lblftpserver" runat="server" Text="SFTP Server:"></asp:Label>
             <asp:RequiredFieldValidator ID="reqvalserver" runat="server" 
                 ControlToValidate="txtftpserver" Display="Dynamic" 
                 ErrorMessage="Enter SFTP Server" SetFocusOnError="True">*</asp:RequiredFieldValidator>
         </td>
         <td>
             <SPS:SPSTextBox ID="txtftpserver" runat="server" MaxLength="50" Width="150px"></SPS:SPSTextBox>
         </td>
         <td>
             &nbsp;</td>
         <td>
             &nbsp;</td>
     </tr>
     <tr>
         <td colspan="4">
             <asp:Label ID="lblftpdownfile" runat="server" 
                 Text="SFTP Location(Download File):"></asp:Label>
             <asp:RequiredFieldValidator ID="reqvaldown" runat="server" 
                 ControlToValidate="txtftpdownfile" Display="Dynamic" 
                 ErrorMessage="Enter SFTP Location(Download File)" SetFocusOnError="True">*</asp:RequiredFieldValidator>
         </td>
         <td>
             <asp:CheckBox ID="chkftpdownfile" runat="server" 
                 Text="Delete the local file after a successfull transfer" TextAlign="Left" 
                 Visible="False" />
         </td>
     </tr>
     <tr>
         <td colspan="4">
             <SPS:SPSTextBox ID="txtftpdownfile" runat="server" Width="450px" 
                 MaxLength="255"></SPS:SPSTextBox>
         </td>
     </tr>
     <tr>
         <td colspan="4">
             <asp:Label ID="lblftprelnotefile" runat="server" 
                 Text="SFTP Location(Release Note File):"></asp:Label>
             <asp:RequiredFieldValidator ID="reqvalrelnote" runat="server" 
                 ControlToValidate="txtftprelnotefile" Display="Dynamic" 
                 ErrorMessage="Enter SFTP Location(Release Note File)" SetFocusOnError="True">*</asp:RequiredFieldValidator>
         </td>
         <td>
             <asp:CheckBox ID="chkftprelnotefile" runat="server" 
                 Text="Delete the local file after a successfull transfer" TextAlign="Left" 
                 Visible="False" />
         </td>
     </tr>
     <tr>
         <td colspan="4">
             <SPS:SPSTextBox ID="txtftprelnotefile" runat="server" Width="450px" 
                 MaxLength="255"></SPS:SPSTextBox>
         </td>
     </tr>
     <tr>
         <td colspan="4">
             <asp:Label ID="lblftpuserfile" runat="server" 
                 Text="SFTP Location(User/Installation Manual File):"></asp:Label>
             <asp:RequiredFieldValidator ID="reqvaluser" runat="server" 
                 ControlToValidate="txtftpuserfile" Display="Dynamic" 
                 ErrorMessage="Enter SFTP Location(User.Installation Manual File)" 
                 SetFocusOnError="True">*</asp:RequiredFieldValidator>
         </td>
         <td>
             <asp:CheckBox ID="chkftpuserfile" runat="server" 
                 Text="Delete the local file after a successfull transfer" TextAlign="Left" 
                 Visible="False" />
         </td>
     </tr>
     <tr>
         <td colspan="4">
             <SPS:SPSTextBox ID="txtftpuserfile" runat="server" Width="450px" 
                 MaxLength="255"></SPS:SPSTextBox>
         </td>
     </tr>
      <tr>
         <td colspan="4" style="width: 300px">
             &nbsp;</td>
     </tr>
 </table>
 </fieldset></asp:Panel>
</td></tr>
</table>
</td>
<td style="width: 4px">
</td>
</tr>
</table>
  <table id="tblbutton" align="center">
            <tr>
            <td>
                <asp:Label ID="lblStatus" runat="server" ForeColor="Red" Font-Bold="True" 
                    Font-Size="Medium"></asp:Label>
            
            </td>
            <td>
                <asp:ImageButton ID="btntestftp" runat="server" 
                    ImageUrl="~/Shared/images/sw_testftpconnection.jpg" 
                    onclick="btntestftp_Click" Visible="False" />
            </td>
            <td>
                <asp:ImageButton ID="btnsave" runat="server" 
                    ImageUrl="~/Shared/images/sw_Update.JPG" onclick="btnsave_Click" 
                    CausesValidation="False" />
            </td>
              <td>
                  <asp:ImageButton ID="btncancel" runat="server" 
                      ImageUrl="~/Shared/images/sw_Cancel.jpg" onclick="btncancel_Click" 
                      CausesValidation="False" />
            </td>
           <%-- <td>
                                                            <asp:Panel ID="panelUpdateProgress" runat="server">
                                                                <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="Updatepnlsysconfig"
                                                                    DisplayAfter="0">
                                                                    <ProgressTemplate>
                                                                        <div style="position: relative; top: 30%; text-align: center;">
                                                                            <img src="../../Shared/Images/progbar.gif" style="vertical-align: middle" alt="Processing" />
                                                                            Processing ...
                                                                        </div>
                                                                    </ProgressTemplate>
                                                                </asp:UpdateProgress>
                                                            </asp:Panel>
                                                        </td>--%>
            </tr>
            <tr><td>
                <asp:ValidationSummary ID="valsummary" runat="server" ShowMessageBox="True" 
                    ShowSummary="False" />
            </td></tr>
            </table>
<%--</ContentTemplate>
</asp:UpdatePanel>--%>
</asp:Content>

