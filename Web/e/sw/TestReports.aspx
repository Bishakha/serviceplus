<%@ Page Title="" Language="C#" MasterPageFile="~/e/sw/SWPlusMaster.master" AutoEventWireup="true" CodeFile="TestReports.aspx.cs" Inherits="sw_TestReports" Theme="SkinFile" %>

<%@ Register Assembly="CrystalDecisions.Web" Namespace="CrystalDecisions.Web" TagPrefix="CR" %>  <%--, Version=10.5.3700.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"--%>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ Register Assembly="Controls" Namespace="Sony.US.ServicesPLUS.Controls" TagPrefix="SPS" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <%--    <asp:UpdatePanel runat="server" ID="UpdateCRReport">
        <ContentTemplate>--%>
            <br />
            <asp:Label ID="lblerror" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label>
            <div>
                <asp:Panel ID="pnlTestReport" runat="server" BorderStyle="Groove" Width="600px">
                    <table width="100%">
                        <tr>
                            <td>
                                <asp:ListBox ID="lstReportName" runat="server" Height="180px" 
                                    onselectedindexchanged="lstReportName_SelectedIndexChanged" AutoPostBack="true">
                                    <asp:ListItem Text ="Current Version" Value="CurrentVersion"  />
                                    <asp:ListItem Text ="Engineer Model" Value="EngineerModel" />
                                    <asp:ListItem Text ="EPROM Kits" Value="EPROMKits" />
                                    <asp:ListItem Text ="Kit Availability" Value="KitAvailability" />
                                    <asp:ListItem Text ="Models Not Assigned" Value="ModelsNotAssigned" />
                                    <asp:ListItem Text ="Product Manager Models" Value="ProductManagerModels" />
                                    <asp:ListItem Text ="Software Registration" Value="SoftwareRegistration" Enabled="false" />
                                </asp:ListBox>
                            </td>
                            <td>
                                <table>
                                    <asp:Panel ID="pnlchkItems" runat="server">
                                    
                                    <tr>
                                        <td>
                                             <asp:Label ID="lblReleaseType" runat="server" Text="Release Type :"></asp:Label>
                                             <asp:RequiredFieldValidator ID="reqvalreltype" runat="server" 
                                                 ControlToValidate="ddlReleaseType" Display="Dynamic" 
                                                 ErrorMessage="Select Release Type" SetFocusOnError="True" Text="*"></asp:RequiredFieldValidator>
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlReleaseType" runat="server" Width="135px"></asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                             <asp:Label ID="lblDistribution" runat="server" Text="Distribution :"></asp:Label>
                                             <asp:RequiredFieldValidator ID="reqvaldist" runat="server" 
                                                 ControlToValidate="ddlDistribution" Display="Dynamic" 
                                                 ErrorMessage="Select Distribution" SetFocusOnError="True" Text="*"></asp:RequiredFieldValidator>
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlDistribution" runat="server" Width="135px"></asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                             <asp:Label ID="lblVisibility" runat="server" Text="Visibility :"></asp:Label>
                                             <asp:RequiredFieldValidator ID="reqvalvisib" runat="server" 
                                                 ControlToValidate="ddlVisibility" Display="Dynamic" 
                                                 ErrorMessage="Select Visibility" SetFocusOnError="True" Text="*"></asp:RequiredFieldValidator>
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlVisibility" runat="server" Width="135px"></asp:DropDownList>
                                        </td>
                                    </tr>
                                      </asp:Panel>
                                    <asp:Panel ID="pnlReleaseDates" runat="server">
                                    <tr>
                                        <td>
                                             <asp:Label ID="lblStartReleaseDate" runat="server" Text="Start Release Date :"></asp:Label>
                                              <asp:RegularExpressionValidator ID="rgeddate" runat="server" ControlToValidate="txtStartReleaseDate"
                                                                        Display="Dynamic" ErrorMessage="Enter Start Release Date in mm/dd/yyyy format" ToolTip="Type in mm/dd/yyyy format"
                                                                        ValidationExpression="^([1-9]|0[1-9]|1[012])[- /.]([1-9]|0[1-9]|[12][0-9]|3[01])[- /.][0-9]{4}$"
                                                                        SetFocusOnError="True">*</asp:RegularExpressionValidator>
                                        </td>
                                        <td>
                                            <SPS:SPSTextBox ID="txtStartReleaseDate" runat="server" ></SPS:SPSTextBox>
                                            <asp:FilteredTextBoxExtender ID="txtStartReleaseDate_FilteredTextBoxExtender" runat="server"
                                                                        Enabled="True" FilterType="Custom, Numbers" TargetControlID="txtStartReleaseDate"
                                                                        ValidChars="/">
                                                                    </asp:FilteredTextBoxExtender>
                                                                    <asp:CalendarExtender ID="txtStartReleaseDate_CalendarExtender" runat="server" CssClass="cal_Theme1"
                                                                        Enabled="True" TargetControlID="txtStartReleaseDate" PopupButtonID="imgcalStartReleaseDate">
                                                                    </asp:CalendarExtender>
                                                                    <asp:Image ID="imgcalStartReleaseDate" runat="server" ImageUrl="~/Shared/Images/Calendar.png" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                             <asp:Label ID="lblEndReleaseDate" runat="server" Text="End Release Date :"></asp:Label>
                                              <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtEndReleaseDate"
                                                                        Display="Dynamic" ErrorMessage="Enter End Release Date in mm/dd/yyyy format" ToolTip="Type in mm/dd/yyyy format"
                                                                        ValidationExpression="^([1-9]|0[1-9]|1[012])[- /.]([1-9]|0[1-9]|[12][0-9]|3[01])[- /.][0-9]{4}$"
                                                                        SetFocusOnError="True">*</asp:RegularExpressionValidator>
                                        </td>
                                        <td>
                                             <SPS:SPSTextBox ID="txtEndReleaseDate" runat="server"></SPS:SPSTextBox>
                                             <asp:FilteredTextBoxExtender ID="txtEndReleaseDate_FilteredTextBoxExtender" runat="server"
                                                                        Enabled="True" FilterType="Custom, Numbers" TargetControlID="txtEndReleaseDate"
                                                                        ValidChars="/">
                                                                    </asp:FilteredTextBoxExtender>
                                                                    <asp:CalendarExtender ID="txtEndReleaseDate_CalendarExtender" runat="server" CssClass="cal_Theme1"
                                                                        Enabled="True" TargetControlID="txtEndReleaseDate" PopupButtonID="imgcalEndReleaseDate">
                                                                    </asp:CalendarExtender>
                                                                    <asp:Image ID="imgcalEndReleaseDate" runat="server" ImageUrl="~/Shared/Images/Calendar.png" />
                                        </td>
                                    </tr>
                                    </asp:Panel>
                                    <asp:Panel ID="pnlEngineerName" runat="server">
                                    <tr>
                                        <td>
                                             <asp:Label ID="lblEngineerName" runat="server" Text="Engineer Name :"></asp:Label>
                                             <asp:RequiredFieldValidator ID="reqvalengname" runat="server" 
                                                 ControlToValidate="ddlEngineerName" Display="Dynamic" 
                                                 ErrorMessage="Select Engineer Name" SetFocusOnError="True" Text="*"></asp:RequiredFieldValidator>
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlEngineerName" runat="server" Width="135px" >
                                             </asp:DropDownList>
                                        </td>
                                    </tr>
                                    </asp:Panel>
                                    <asp:Panel ID="pnlProductManager" runat="server">
                                    <tr>
                                        <td>
                                             <asp:Label ID="lblProductManager" runat="server" Text="Product Manager:"></asp:Label>
                                             <asp:RequiredFieldValidator ID="reqvalprodmgr" runat="server" 
                                                 ControlToValidate="ddlProductManager" Display="Dynamic" 
                                                 ErrorMessage="Select Product Manager" SetFocusOnError="True" Text="*"></asp:RequiredFieldValidator>
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlProductManager" runat="server" Width="135px" ></asp:DropDownList>
                                        </td>
                                    </tr>
                                    </asp:Panel>
                                    <asp:Panel ID="pnlKitPartNo" runat="server">
                                    <tr>
                                        <td>
                                             <asp:Label ID="lblKitPartNo" runat="server" Text="Kit Part Number - Description :"></asp:Label>
                                             <asp:RequiredFieldValidator ID="reqvalkitpartno" runat="server" 
                                                 ControlToValidate="ddlKitPartNo" Display="Dynamic" 
                                                 ErrorMessage="Select Kit Part Number-Description" SetFocusOnError="True" 
                                                 Text="*"></asp:RequiredFieldValidator>
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlKitPartNo" runat="server" Width="135px"></asp:DropDownList>
                                        </td>
                                    </tr>
                                    </asp:Panel>
                                    <asp:Panel ID="pnlVersion" runat="server">
                                    <tr>
                                        <td>
                                             <asp:Label ID="lblVersion" runat="server" Text="Version :"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlVersion" runat="server">
                                                <asp:ListItem Text="<" Value="" />
                                                <asp:ListItem Text="<=" Value="" />
                                                <asp:ListItem Text="<>" Value="" />
                                                <asp:ListItem Text="=" Value="" />
                                                <asp:ListItem Text=">" Value="" />
                                                <asp:ListItem Text="=>" Value="" />
                                                <asp:ListItem Text="Null" Value="" />
                                            </asp:DropDownList>
                                            <SPS:SPSTextBox ID="txtVersion" runat="server" MaxLength="10"></SPS:SPSTextBox>
                                        </td>
                                    </tr>
                                    </asp:Panel>
                              </table>
                            </td>
                             <td align="right">
                                 <asp:ImageButton ID="btnSubmit" runat="server" 
                                           ImageUrl="~/Shared/Images/sw_submit.jpg" 
                                           ImageAlign="AbsMiddle" onclick="btnSubmit_Click" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </div>
            <br />
<%--            <asp:Panel ID="panelUpdateProgress" runat="server">
                <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdateCRReport" DisplayAfter="0">
                    <ProgressTemplate>
                        <div style="position: relative; top: 30%; text-align: center;">
                        <img src="../../Shared/Images/progbar.gif"  style="vertical-align: middle" alt="Processing" />
                        Processing ...
                        </div>
                    </ProgressTemplate>
                </asp:UpdateProgress>
            </asp:Panel>--%>
            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True" ShowSummary="False" />
            <asp:Label ID="lblReportStatus" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label>
            
    <div id="divReport">
                <CR:CrystalReportViewer ID="CrystalReportViewer1" runat="server" AutoDataBind="true" />  <%--Obsolete: DisplayGroupTree="False"--%>
            </div>
<%--        </ContentTemplate>
    </asp:UpdatePanel>--%>
</asp:Content>

