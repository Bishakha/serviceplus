<%@ Page Title="" Language="C#" MasterPageFile="~/e/sw/SWPlusMaster.master" AutoEventWireup="true"
    CodeFile="KitInformation.aspx.cs" Inherits="sw_KitInformation_cm" Theme="SkinFile" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="Controls" Namespace="Sony.US.ServicesPLUS.Controls" TagPrefix="SPS" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript">
        function ConfirmationBox() {
            var result = confirm('Are you sure you want to delete the record ?');
            return result;
        }

        function OpenPopup(param) {
            var popupParameters = "toolbar=no, location=no,status=yes,menubar=no,scrollbars=yes,resizable=no, width=450,height=170,left=200,top=200";

            if (param.indexOf("type=ReleaseNote") > -1) {
                // If it's a "Release Note", it needs the Kit Number appended to the Query String so we can force the file naming.
                if (document.getElementById('ContentPlaceHolder1_txtkitpartno') != null) {
                    //alert(document.getElementById('ContentPlaceHolder1_txtkitpartno').value);
                    if (document.getElementById('ContentPlaceHolder1_txtkitpartno').value != "") {
                        param += "&KitNo=" + document.getElementById('ContentPlaceHolder1_txtkitpartno').value;
                    } else {
                        alert("Please provide the Kit number before uploading the file.");
                        return false;
                    }
                } else if (document.getElementById('ContentPlaceHolder1_ddlkitpartno_ddlkitpartno_TextBox') != null) {
                    //alert(document.getElementById('ContentPlaceHolder1_ddlkitpartno_ddlkitpartno_TextBox').value);
                    if (document.getElementById('ContentPlaceHolder1_ddlkitpartno_ddlkitpartno_TextBox').value != "") {
                        param += "&KitNo=" + document.getElementById('ContentPlaceHolder1_ddlkitpartno_ddlkitpartno_TextBox').value;
                    } else {
                        alert("Please select the Kit number before uploading the file.");
                        return false;
                    }
                } else {
                    alert('Kit Part Number could not be discovered.');
                    return false;
                }
            }
            window.open("FileUpload.aspx?" + param + "", "Upload files", popupParameters);

            return false;
        }
    </script>

    <asp:UpdateProgress ID="prgUpdate" runat="server" AssociatedUpdatePanelID="pnlKits" DisplayAfter="200">
        <ProgressTemplate>
            <div style="position: relative; top: 30%; text-align: center; background-color: #DDDDDD; border: 2px solid grey;">
                Please Wait ...
                <img src="../../Shared/Images/progbar.gif" style="vertical-align: middle" alt="Processing" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>

    <asp:UpdatePanel ID="pnlKits" runat="server">
        <ContentTemplate>
            <asp:HiddenField ID="hdnDownloadFile" runat="server" />
            <asp:HiddenField ID="hdnReleaseNote" runat="server" />
            <table border="0" style="width: 900px">
                <tr>
                    <td style="width: 4px">
                    </td>
                    <td style="width: 900px">
                        <table border="0" style="width: 100%">
                            <tr>
                                <td>
                                    <asp:Label ID="lblerror" runat="server" Font-Bold="True" ForeColor="Red" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table border="0" style="width: 100%">
                                        <tr bgcolor="#f2f5f8">
                                            <td>
                                                <asp:Label ID="lblkitpartno" runat="server" Text="Kit Part Number:" />
                                            </td>
                                            <td>
                                                &nbsp;
                                            </td>
                                            <td>
                                                <SPS:SPSTextBox ID="txtkitpartno" runat="server" MaxLength="9" AutoPostBack="True" />
                                                <%--  <asp:RequiredFieldValidator ID="RequiredFieldValidator1"
                                                        ControlToValidate="txtkitpartno"
                                                        Text="Please enter the KitPartNo"
                                                        runat="server" />--%>
                                                <asp:FilteredTextBoxExtender ID="txtkitpartno_FilteredTextBoxExtender" runat="server"
                                                    Enabled="True" FilterType="Numbers, UppercaseLetters, LowercaseLetters" TargetControlID="txtkitpartno" />
                                                <asp:ComboBox ID="ddlkitpartno" runat="server" AutoPostBack="True" DropDownStyle="DropDownList"
                                                    Visible="False" AutoCompleteMode="SuggestAppend" MaxLength="0" OnSelectedIndexChanged="ddlkitpartno_SelectedIndexChanged" />
                                            </td>
                                            <td>
                                                &nbsp;
                                            </td>
                                            <td>
                                                <asp:Label ID="lblsprceedkit" runat="server" Text="Superceded Kit:" />
                                            </td>
                                            <td>
                                                <asp:ComboBox ID="ddlsprceedkit" runat="server" DropDownStyle="DropDownList" AutoCompleteMode="SuggestAppend"
                                                    MaxLength="0" />
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblRequiredKey" runat="server" Text="Required Key" />
                                            </td>
                                            <td>
                                                &nbsp;
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddlRequiredKey" runat="server" Style="margin-top: 5px;" />
                                            </td>
                                            <td>
                                                &nbsp;
                                            </td>
                                            <td>
                                                <asp:CheckBox ID="chkDownloaFreeCharge" runat="server" Text="Download Free Of Charge" />
                                            </td>
                                            <td>
                                                &nbsp;
                                                <asp:CheckBox ID="chkTracking" runat="server" Text="Tracking" />
                                            </td>
                                        </tr>
                                        <tr bgcolor="#f2f5f8">
                                            <td>
                                                <asp:Label ID="lbldownfile" runat="server" Text="Download File" />
                                            </td>
                                            <td>
                                                &nbsp;
                                            </td>
                                            <td>
                                                <asp:ComboBox ID="ddldownfile" runat="server" DropDownStyle="DropDownList" AutoCompleteMode="SuggestAppend"
                                                    MaxLength="0" />
                                            </td>
                                            <td colspan="3">
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblupldownfile" runat="server" Text="Upload Download File" />
                                                <asp:RequiredFieldValidator ID="reqvalupdownfile" runat="server" ControlToValidate="fileupldownfile"
                                                    Display="Dynamic" ErrorMessage="Select Upload Download File" SetFocusOnError="True"
                                                    ValidationGroup="UploadDownload">*</asp:RequiredFieldValidator>
                                            </td>
                                            <td>
                                                &nbsp;
                                            </td>
                                            <td>
                                                <asp:FileUpload ID="fileupldownfile" runat="server" Visible="false" />
                                                <asp:Label ID="FileNameText" runat="server" Visible="false" />
                                                <asp:Button ID="btnUploadDownloadFile" runat="server" OnClientClick="return OpenPopup('type=DownloadFile');"
                                                    Style="margin-top: 10px; margin-left: -20px;" Text="Upload" ValidationGroup="UploadDownload" />
                                            </td>
                                            <td>
                                                &nbsp;
                                            </td>
                                            <td>
                                                <asp:Label ID="lblmsgupload" runat="server" Font-Bold="True" ForeColor="Green" />
                                            </td>
                                            <td>
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr bgcolor="#f2f5f8">
                                            <td>
                                                <asp:Label ID="lblrelnotefile" runat="server" Text="Release Note File" />
                                            </td>
                                            <td>
                                                &nbsp;
                                            </td>
                                            <td>
                                                <asp:ComboBox ID="ddlrelnotefile" runat="server" DropDownStyle="DropDownList" AutoCompleteMode="SuggestAppend"
                                                    MaxLength="0" />
                                            </td>
                                            <td colspan="3">
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lbluplrelnotefile" runat="server" Text="Upload Release Note File" />
                                                <asp:RequiredFieldValidator ID="reqvaluprelnote" runat="server" ControlToValidate="fileuplrelnotefile"
                                                    Display="Dynamic" ErrorMessage="Select Upload Release Note File" SetFocusOnError="True"
                                                    ValidationGroup="UploadRelease">*</asp:RequiredFieldValidator>
                                            </td>
                                            <td>
                                                &nbsp;
                                            </td>
                                            <td>
                                                <asp:FileUpload ID="fileuplrelnotefile" runat="server" Visible="false" />
                                                &nbsp;&nbsp;
                                                <asp:Label ID="FileNameTextRel" runat="server" Visible="false" />
                                                <asp:Button ID="btnUploadReleaseNote" runat="server" OnClientClick="return OpenPopup('type=ReleaseNote');" Text="Upload"
                                                    Style="margin-top: 10px; margin-bottom: 08px; margin-left: -10px;" ValidationGroup="UploadRelease" />
                                            </td>
                                            <td>
                                                &nbsp;
                                            </td>
                                            <td>
                                                <asp:Label ID="lblmsgrelnotefile" runat="server" Font-Bold="True" ForeColor="Green" />
                                            </td>
                                            <td>
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr bgcolor="#f2f5f8">
                                            <td>
                                                <asp:Label ID="lblkitdesc" runat="server" Text="Kit Description:" />
                                            </td>
                                            <td>
                                                &nbsp;
                                            </td>
                                            <td colspan="3">
                                                <asp:TextBox ID="txtkitdesc" runat="server" Height="50px" TextMode="MultiLine" Width="300px"
                                                    AutoPostBack="True" />
                                            </td>
                                            <td>
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblmfrremarks" runat="server" Text="Manufacturer Remarks:" />
                                            </td>
                                            <td>
                                                &nbsp;
                                            </td>
                                            <td colspan="3">
                                                <SPS:SPSTextBox ID="txtmfrremarks" runat="server" Height="50px" TextMode="MultiLine" Width="300px" />
                                            </td>
                                            <td>
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr bgcolor="#f2f5f8">
                                            <td>
                                                <asp:Label ID="lblnotes" runat="server" Text="Notes:" />
                                            </td>
                                            <td>
                                                &nbsp;
                                            </td>
                                            <td colspan="3">
                                                <SPS:SPSTextBox ID="txtnotes" runat="server" Height="50px" TextMode="MultiLine" Width="300px" />
                                            </td>
                                            <td>
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblcreatedby" runat="server" Text="Created By:" />
                                            </td>
                                            <td>
                                                &nbsp;
                                            </td>
                                            <td>
                                                <SPS:SPSTextBox ID="txtcreatedby" runat="server" ReadOnly="True" BackColor="#CCCCCC" />
                                            </td>
                                            <td>
                                                &nbsp;
                                            </td>
                                            <td>
                                                <asp:Label ID="lbldatecreated" runat="server" Text="Date Created:" />
                                            </td>
                                            <td>
                                                <SPS:SPSTextBox ID="txtdatecreated" runat="server" ReadOnly="True" BackColor="#CCCCCC" />
                                            </td>
                                        </tr>
                                        <tr bgcolor="#f2f5f8">
                                            <td>
                                                <asp:Label ID="lblupdatedby" runat="server" Text="Updated By:" />
                                            </td>
                                            <td>
                                                &nbsp;
                                            </td>
                                            <td>
                                                <SPS:SPSTextBox ID="txtupdatedby" runat="server" ReadOnly="True" BackColor="#CCCCCC" />
                                            </td>
                                            <td>
                                                &nbsp;
                                            </td>
                                            <td>
                                                <asp:Label ID="lbllastupdated" runat="server" Text="Last Updated:" />
                                            </td>
                                            <td>
                                                <SPS:SPSTextBox ID="txtlastupdated" runat="server" ReadOnly="True" BackColor="#CCCCCC" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="6">
                                                &nbsp;
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <table id="tblbutton" align="center">
                <tr>
                    <td>
                        <asp:ImageButton ID="btnnew" runat="server" ImageUrl="~/Shared/Images/sw_Add.JPG" OnClick="btnnew_Click" />
                    </td>
                    <td>
                        <asp:ImageButton ID="btnsave" runat="server" ImageUrl="~/Shared/Images/sw_Save.JPG" OnClick="btnsave_Click" Visible="False" />
                    </td>
                    <td>
                        <asp:ImageButton ID="btnedit" runat="server" ImageUrl="~/Shared/images/sw_Cancel.jpg" OnClick="btnedit_Click" />
                    </td>
                    <td>
                        <asp:ImageButton ID="btnupdate" runat="server" ImageUrl="~/Shared/Images/sw_Update.JPG" OnClick="btnupdate_Click" Visible="False" />
                    </td>
                    <td>
                        <asp:ImageButton ID="btnDelete" runat="server" ImageUrl="~/Shared/Images/sw_Delete.JPG" OnClick="btnDelete_Click" Visible="False" />
                    </td>
                </tr>
            </table>
            <table>
                <tr>
                    <td>
                        <asp:Panel ID="Panel3" runat="server" ScrollBars="Auto" Width="850px">
                            <asp:GridView ID="gvitem" runat="server" AllowPaging="True" DataKeyNames="ITEM_ID,KIT_PART_NUMBER"
                                AllowSorting="True" AutoGenerateColumns="False" CellPadding="4" ShowFooter="True"
                                ForeColor="#333333" GridLines="None" OnPageIndexChanging="gvitem_PageIndexChanging"
                                OnRowCancelingEdit="gvitem_RowCancelingEdit" OnRowDeleting="gvitem_RowDeleting"
                                OnRowEditing="gvitem_RowEditing" OnRowUpdating="gvitem_RowUpdating" OnRowCommand="gvitem_RowCommand"
                                OnRowDataBound="gvitem_RowDataBound" Width="850px" OnPreRender="gvitem_PreRender">
                                <RowStyle BackColor="#EFF3FB" />
                                <Columns>
                                    <asp:TemplateField>
                                        <EditItemTemplate>
                                            <asp:ImageButton ID="imgbtnUpdate" CommandName="Update" runat="server" ImageUrl="~/Shared/images/update.jpg"
                                                ToolTip="Update" Height="15px" Width="15px" ValidationGroup="validationeditcat" /><asp:ImageButton
                                                    ID="imgbtnCancel" runat="server" CommandName="Cancel" ImageUrl="~/Shared/Images/Cancel.jpg"
                                                    ToolTip="Cancel" Height="15px" Width="15px" CausesValidation="False" />
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgbtnEdit" CommandName="Edit" runat="server" ImageUrl="~/Shared/Images/Edit.jpg"
                                                ToolTip="Edit" Height="15px" Width="15px" CausesValidation="False" />
                                            <asp:ImageButton ID="imgbtnDelete" CommandName="Delete" Text="Edit" runat="server"
                                                ImageUrl="~/Shared/Images/delete.jpg" ToolTip="Delete" Height="15px" Width="15px"
                                                CausesValidation="False" />
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:ImageButton ID="imgbtnAdd" runat="server" ImageUrl="~/Shared/Images/AddNewitem.jpg"
                                                CommandName="AddNew" Width="30px" Height="30px" ToolTip="Add new Kit Item" ValidationGroup="validationnewcat" />
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <%-- <asp:TemplateField HeaderText="Item ID" SortExpression="Item ID">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtItemId" runat="server" Text='<%# Eval("ITEM_ID") %>' />
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="txtNewItemId" runat="server" />
                                        </FooterTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lbltxtItemId" runat="server" Text='<%# Bind("ITEM_ID") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>--%>
                                    <asp:TemplateField HeaderText="Item Description" SortExpression="Name">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtItemDescription" runat="server" Text='<%# Eval("ITEM_DESCRIPTION") %>' />
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="txtNewItemDescription" runat="server" />
                                        </FooterTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label2" runat="server" Text='<%# Bind("ITEM_DESCRIPTION") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Item Type">
                                        <EditItemTemplate>
                                            <asp:DropDownList ID="ddlItemType" runat="server" SelectedValue='<%# Eval("ITEM_TYPE") %>'>
                                                <asp:ListItem Text="Documentation" Value="Documentation" />
                                                <asp:ListItem Text="Software/Firmware" Value="Software/Firmware" />
                                            </asp:DropDownList>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lbItemType" runat="server" Text='<%# Eval("ITEM_TYPE") %>' />
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:DropDownList ID="ddlNewItemType" runat="server">
                                                <asp:ListItem Selected="True" Text="Documentation" Value="Documentation" />
                                                <asp:ListItem Text="Software/Firmware" Value="Software/Firmware" />
                                            </asp:DropDownList>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Sub Assembly">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtSubAssembly" runat="server" Text='<%# Bind("SUB_ASSEMBLY") %>' />
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="txtNewSubAssembly" runat="server" />
                                        </FooterTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblSubAssembly" runat="server" Text='<%# Bind("SUB_ASSEMBLY") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Location" SortExpression="Location">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtLocation" runat="server" Text='<%# Eval("LOCATION") %>' />
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="txtNewLocation" runat="server" />
                                        </FooterTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblLocation" runat="server" Text='<%# Bind("LOCATION") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Version" SortExpression="Version">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtVersion" runat="server" Text='<%# Eval("VERSION") %>' />
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="txtNewVersion" runat="server" />
                                        </FooterTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblVersion" runat="server" Text='<%# Bind("VERSION") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Speed" SortExpression="Speed">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtSpeed" runat="server" Text='<%# Eval("SPEED") %>' />
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="txtNewSpeed" runat="server" />
                                        </FooterTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblSpeed" runat="server" Text='<%# Bind("SPEED") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Check Sum" SortExpression="Check Sum">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtCheckSum" runat="server" Text='<%# Eval("CHECK_SUM") %>' />
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="txtNewCheckSum" runat="server" />
                                        </FooterTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblCheckSum" runat="server" Text='<%# Bind("CHECK_SUM") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Manufacturer" SortExpression="Manufacturer">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtManufacturer" runat="server" Text='<%# Eval("MANUFACTURER") %>' />
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="txtNewManufacturer" runat="server" />
                                        </FooterTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblManufacturer" runat="server" Text='<%# Bind("MANUFACTURER") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="FileName" SortExpression="FileName">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtFileName" runat="server" Text='<%# Eval("FILE_NAME") %>' />
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="txtNewFileName" runat="server" />
                                        </FooterTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblFileName" runat="server" Text='<%# Bind("FILE_NAME") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="FileFormat" SortExpression="FileFormat">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtFileFormat" runat="server" Text='<%# Eval("FILE_FORMAT") %>' />
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="txtNewFileFormat" runat="server" />
                                        </FooterTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblFileFormat" runat="server" Text='<%# Bind("FILE_FORMAT") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Packaging">
                                        <EditItemTemplate>
                                            <asp:DropDownList ID="ddlPckaging" runat="server" SelectedValue='<%# Eval("PACKAGING") %>'>
                                                <asp:ListItem Text="Anti-Static Bag" Value="Anti-Static Bag" />
                                                <asp:ListItem Text="Box" Value="Box" />
                                                <asp:ListItem Text="CD Rom" Value="CD Rom" />
                                                <asp:ListItem Text="Disk Sleeve" Value="Disk Sleeve" />
                                                <asp:ListItem Text="Document" Value="Document" />
                                                <asp:ListItem Text="ESD Box" Value="ESD Box" />
                                                <asp:ListItem Text="Eprom Box" Value="Eprom Box" />
                                                <asp:ListItem Text="Floppy Disk" Value="Floppy Disk" />
                                                <asp:ListItem Text="IC Packaging" Value="IC Packaging" />
                                                <asp:ListItem Text="Jewell Case" Value="Jewell Case" />
                                                <asp:ListItem Text="Large Box" Value="Large Box" />
                                                <asp:ListItem Text="Manual" Value="Manual" />
                                                <asp:ListItem Text="Memory Stick" Value="Memory Stick" />
                                                <asp:ListItem Text="Pcmcia" Value="Pcmcia" />
                                                <asp:ListItem Text="Plastic Box" Value="Plastic Box" />
                                                <asp:ListItem Text="Shrink Wrap" Value="Shrink Wrap" />
                                                <asp:ListItem Text="Stapled" Value="Stapled" />
                                                <asp:ListItem Text="Tube" Value="Tube" />
                                            </asp:DropDownList>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblPackaging" runat="server" Text='<%# Eval("PACKAGING") %>' />
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:DropDownList ID="ddlNewPckaging" runat="server">
                                                <asp:ListItem Selected="True" Text="Anti-Static Bag" Value="Anti-Static Bag" />
                                                <asp:ListItem Text="Box" Value="Box" />
                                                <asp:ListItem Text="CD Rom" Value="CD Rom" />
                                                <asp:ListItem Text="Disk Sleeve" Value="Disk Sleeve" />
                                                <asp:ListItem Text="Document" Value="Document" />
                                                <asp:ListItem Text="ESD Box" Value="ESD Box" />
                                                <asp:ListItem Text="Eprom Box" Value="Eprom Box" />
                                                <asp:ListItem Text="Floppy Disk" Value="Floppy Disk" />
                                                <asp:ListItem Text="IC Packaging" Value="IC Packaging" />
                                                <asp:ListItem Text="Jewell Case" Value="Jewell Case" />
                                                <asp:ListItem Text="Large Box" Value="Large Box" />
                                                <asp:ListItem Text="Manual" Value="Manual" />
                                                <asp:ListItem Text="Memory Stick" Value="Memory Stick" />
                                                <asp:ListItem Text="Pcmcia" Value="Pcmcia" />
                                                <asp:ListItem Text="Plastic Box" Value="Plastic Box" />
                                                <asp:ListItem Text="Shrink Wrap" Value="Shrink Wrap" />
                                                <asp:ListItem Text="Stapled" Value="Stapled" />
                                                <asp:ListItem Text="Tube" Value="Tube" />
                                            </asp:DropDownList>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Label Contents">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="ddlLabelContents" TextMode="MultiLine" runat="server" Text='<%# Eval("LABEL_CONTENTS") %>' />
                                            <asp:Button ID="btnCopyRightEdit" CommandName="CopyRightEdit" runat="server" Text="�" />
                                            <asp:Button ID="btnRSymbo" CommandName="RSymbolEdit" runat="server" Text="�" />
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lbContents" runat="server" Text='<%# Eval("LABEL_CONTENTS") %>' />
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="ddlNewLabelContents" runat="server" TextMode="MultiLine" Text='<%# Eval("LABEL_CONTENTS") %>' />
                                            <asp:Button ID="btnNewCopyRight" CommandName="CopyRight" runat="server" Text="�" />
                                            <asp:Button ID="btnNewRSymbo" CommandName="RSymbol" runat="server" Text="�" />
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                <EditRowStyle BackColor="#2461BF" />
                                <AlternatingRowStyle BackColor="White" />
                            </asp:GridView>
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:ValidationSummary ID="valsummaryupload" runat="server" ShowMessageBox="True"
                            ShowSummary="False" ValidationGroup="UploadRelease" />
                        <asp:ValidationSummary ID="valsummaryrelease" runat="server" ShowMessageBox="True"
                            ShowSummary="False" ValidationGroup="UploadDownload" />
                    </td>
                </tr>
            </table>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnUploadDownloadFile" />
            <asp:PostBackTrigger ControlID="btnUploadReleaseNote" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
