﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="KitFileUpload.aspx.cs" Inherits="e_sw_KitFileUpload" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title></title>
       <style type="text/css">
.box_scale {
              width:800px;
             height:500px;
}
</style>
    
</head>
<body>
<div class="box_scale">
    <form id="form1" runat="server"> 

    <div> 

        <table width="80%" align="center" style="background: White; 

            padding: 10px;" border="4"> 

            <tr> 

                <td align="left" style="border: none;"> 

                    <div> 

                        <h4> 

                            Select File To Upload</h4> 

                        <input id="ImageFileUpload" contenteditable="false" runat="server" name="ImageFile" type="file" 

                            style="width: 400px" />&nbsp;&nbsp; 
                            

                        <asp:Button ID="btnAddImage" runat="server" Text="Upload File" style="width:auto;" OnClientClick="return ProgressBar()" 

                            OnClick="btnAddImage_Click"   /> 

                        <br /> 

                        <br /> 

                        <asp:Label ID="lblError" runat="server" ForeColor="Red" Font-Bold="true" Visible="false"></asp:Label>
                        <asp:Label ID="Label1" runat="server" ForeColor="Red" Visible="false"></asp:Label>  
                        <asp:Label ID="KitlabelFileName" runat="server" Visible="false"></asp:Label>  
                        <br /> 

                        <br /> 


                        <div id="divUpload" style="display: none"> 

                            <div style="width: 300pt; text-align: center;"> 

                                Uploading...</div> 

                            <div style="width: 300pt; height: 20px; border: solid 1pt gray"> 

                                <div id="divProgress" runat="server" style="width: 1pt; height: 20px; background-color: Blue; 

                                    display: none"> 

                                </div> 

                            </div> 

                            <div style="width: 300pt; text-align: center;"> 

                                <asp:Label ID="lblPercentage" runat="server" Text="Label"></asp:Label></div> 

                            <br /> 

                            

                        </div> 

                    </div> 

                    <br class="clear" /> 

                </td> 

            </tr> 

            <tr> 

                <td> 
                </td> 

            </tr> 

        </table> 

    </div> 

    </form>
    </div>
</body>
<script language="javascript" type="text/javascript">

   

    var size = 2;

    var id = 0;



    function ProgressBar() {

        if (document.getElementById('<%=ImageFileUpload.ClientID %>').value != "") {

            document.getElementById("divProgress").style.display = "block";

            document.getElementById("divUpload").style.display = "block";

            id = setInterval("progress()", 20);

            return true;

        }

        else {

            alert("Select a file to upload");

            return false;

        }

    }


    function progress() {

        size = size + 1;

        if (size > 299) {

            clearTimeout(id);

        }



        document.getElementById("divProgress").style.width = size + "pt";

        document.getElementById("<%=lblPercentage.ClientID %>").firstChild.data = parseInt(size / 3) + "%";

    } 

    </script>
</html>
