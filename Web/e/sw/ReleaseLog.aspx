<%@ Page Title="" Language="C#" MasterPageFile="~/e/sw/SWPlusMaster.master" AutoEventWireup="true"
    CodeFile="ReleaseLog.aspx.cs" Inherits="sw_ReleaseLog_cm" Theme="SkinFile" EnableEventValidation="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="Controls" Namespace="Sony.US.ServicesPLUS.Controls" TagPrefix="SPS" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript">
    function ConfirmationBox(strKIT_PART_NUMBER) {
        var result = confirm('Are you sure you want to delete ' + strKIT_PART_NUMBER + ' Details?');
        if (result) {
            return true;
        }
        else {
            return false;
        }
    }
    function ConfirmationBoxRelID(strRELEASE_ID) {
        var result = confirm('Are you sure you want to delete ' + strRELEASE_ID + ' Details?');
        if (result) {
            return true;
        }
        else {
            return false;
        }
    }

    function OpenPopup(param) {
        var popupParameters = "toolbar=no, location=no,status=yes,menubar=no,scrollbars=yes,resizable=no, width=450,height=170,left=200,top=200";
        window.open("FileUpload.aspx?" + param + "", "Upload User Guide", popupParameters);
        return false;
    } 
    </script>

    <asp:UpdateProgress ID="prgUpdate" runat="server" AssociatedUpdatePanelID="pnlRelease" DisplayAfter="200">
        <ProgressTemplate>
            <div style="position: relative; top: 30%; text-align: center; background-color: #DDDDDD; border: 2px solid grey; ">
                Please Wait ... <img src="../../Shared/Images/progbar.gif" style="vertical-align: middle" alt="Processing" />                
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    
    <asp:UpdatePanel ID="pnlRelease" runat="server">
        <ContentTemplate>
            <asp:HiddenField ID="hdnUserGuide" runat="server" />
            <table style="width: 100%; border: none;">
                <tr>
                    <td style="width: 4px">
                    </td>
                    <td>
                        <table border="0" style="width: 100%">
                            <tr>
                                <td>
                                    <asp:Label ID="lblMessage" runat="server" Font-Bold="True" ForeColor="Red" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <fieldset align="left">
                                        <legend class="legendlabel">Select a Release:</legend>
                                        <table border="0" style="width: 100%; font-family: Verdana, Tahoma, Helvetica;
                                            top: 0px;">
                                            <tr bgcolor="#f2f5f8">
                                                <td>
                                                    <asp:Literal ID="lblReleaseID" runat="server" Text="Release ID:" />
                                                </td>
                                                <td>
                                                    <asp:ComboBox ID="ddlReleaseID" runat="server" AutoPostBack="True" DropDownStyle="DropDownList"
                                                        EnableViewState="false" AutoCompleteMode="SuggestAppend" MaxLength="0" RenderMode="Block"
                                                        OnSelectedIndexChanged="ddlReleaseID_SelectedIndexChanged">
                                                    </asp:ComboBox>
                                                    <SPS:SPSTextBox ID="txtReleaseID" runat="server" BackColor="#CCCCCC" Enabled="False" />
                                                </td>
                                                <td>
                                                    <asp:Literal ID="lblReleaseDate" runat="server" Text="Released:" />
                                                    <asp:RegularExpressionValidator ID="rgeddate0" runat="server" ControlToValidate="txtReleaseDate"
                                                        Display="Dynamic" ErrorMessage="Enter Date in mm/dd/yyyy format" SetFocusOnError="True"
                                                        ToolTip="Type in mm/dd/yyyy format" ValidationExpression="^([1-9]|0[1-9]|1[012])[- /.]([1-9]|0[1-9]|[12][0-9]|3[01])[- /.][0-9]{4}$">*</asp:RegularExpressionValidator>
                                                </td>
                                                <td>
                                                    <SPS:SPSTextBox ID="txtReleaseDate" runat="server" MaxLength="10" />
                                                    <asp:FilteredTextBoxExtender ID="txtReleaseDate_FilteredTextBoxExtender" runat="server"
                                                        Enabled="True" FilterType="Custom, Numbers" TargetControlID="txtReleaseDate"
                                                        ValidChars="/">
                                                    </asp:FilteredTextBoxExtender>
                                                    <asp:CalendarExtender ID="txtReleaseDate_CalendarExtender" runat="server" Enabled="True"
                                                        TargetControlID="txtReleaseDate" CssClass="cal_Theme1" PopupButtonID="imgCalendar">
                                                    </asp:CalendarExtender>
                                                    <asp:Image ID="imgCalendar" runat="server" ImageUrl="~/Shared/Images/Calendar.png" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Literal ID="lblModelID" runat="server" Text="Model ID:" />
                                                </td>
                                                <td>
                                                    <asp:ComboBox ID="ddlModelID" runat="server" AutoPostBack="True" DropDownStyle="DropDownList"
                                                        EnableViewState="false" AutoCompleteMode="SuggestAppend" MaxLength="0"
                                                        RenderMode="Block" OnSelectedIndexChanged="ddlModelID_SelectedIndexChanged" />
                                                    <SPS:SPSTextBox ID="txtModelID" runat="server" Enabled="False" />
                                                </td>
                                                <td>
                                                    <asp:Literal ID="lblVersion" runat="server" Text="Version:" />
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtVersion" runat="server" MaxLength="10" OnTextChanged="txtVersion_TextChanged" />
                                                    <asp:FilteredTextBoxExtender ID="txtVersion_FilteredTextBoxExtender" runat="server"
                                                        Enabled="True" FilterType="Custom, Numbers, UppercaseLetters, LowercaseLetters"
                                                        TargetControlID="txtVersion" ValidChars=".">
                                                    </asp:FilteredTextBoxExtender>
                                                </td>
                                            </tr>
                                            <tr bgcolor="#f2f5f8">
                                                <td>
                                                    <asp:Literal ID="lblreleasetype" runat="server" Text="Release Type:" />
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlReleaseType" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlReleaseType_SelectedIndexChanged" />
                                                </td>
                                                <td>
                                                    <asp:Literal ID="lbldistribution" runat="server" Text="Distribution:" />
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlDistribution" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlDistribution_SelectedIndexChanged" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Literal ID="lblvisibility0" runat="server" Text="Visibility:" />
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlVisibility" runat="server" />
                                                </td>
                                                <td>
                                                    <asp:Literal ID="lblstatus" runat="server" Text="Status:" />
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlStatus" runat="server" />
                                                </td>
                                            </tr>
                                        </table>
                                    </fieldset>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" Width="900px"
                                        OnActiveTabChanged="TabContainer1_ActiveTabChanged" AutoPostBack="True">
                                        <asp:TabPanel runat="server" HeaderText="Release Log" ID="TabPanel1">
                                            <HeaderTemplate>
                                                Release Log
                                            </HeaderTemplate>
                                            <ContentTemplate>
                                                <table>
                                                    <tr bgcolor="#f2f5f8">
                                                        <td>
                                                            <asp:Literal ID="lblstsernum" runat="server" Text="Starting Serial Number:" />
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                        <td>
                                                            <SPS:SPSTextBox ID="txtStartSerialNumber" runat="server" OnTextChanged="txtStartSerialNumber_TextChanged"
                                                                MaxLength="10" />
                                                            <asp:FilteredTextBoxExtender ID="txtStartSerialNumber_FilteredTextBoxExtender" runat="server"
                                                                Enabled="True" FilterType="Numbers, UppercaseLetters, LowercaseLetters" TargetControlID="txtStartSerialNumber">
                                                            </asp:FilteredTextBoxExtender>
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                        <td>
                                                            <asp:Literal ID="lblendsernum" runat="server" Text="Ending Serial Number:" />
                                                        </td>
                                                        <td>
                                                            <SPS:SPSTextBox ID="txtEndSerialNumber" runat="server" OnTextChanged="txtEndSerialNumber_TextChanged"
                                                                MaxLength="10" />
                                                            <asp:FilteredTextBoxExtender ID="txtEndSerialNumber_FilteredTextBoxExtender" runat="server"
                                                                Enabled="True" FilterType="Numbers, UppercaseLetters, LowercaseLetters" TargetControlID="txtEndSerialNumber">
                                                            </asp:FilteredTextBoxExtender>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Literal ID="lblreleaseqty0" runat="server" Text="Release Quantity:" />
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                        <td>
                                                            <SPS:SPSTextBox ID="txtReleaseQuantity" runat="server" MaxLength="4" Text="0" OnTextChanged="txtReleaseQuantity_TextChanged" />
                                                            <asp:FilteredTextBoxExtender ID="txtReleaseQuantity_FilteredTextBoxExtender" runat="server"
                                                                Enabled="True" FilterType="Numbers" TargetControlID="txtReleaseQuantity">
                                                            </asp:FilteredTextBoxExtender>
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                        <td>
                                                            <asp:Literal ID="lblminonhand" runat="server" Text="Min On Hand:" />
                                                        </td>
                                                        <td>
                                                            <SPS:SPSTextBox ID="txtMinimumOnHand" runat="server" Text="0" MaxLength="4" OnTextChanged="txtMinimumOnHand_TextChanged" />
                                                            <asp:FilteredTextBoxExtender ID="txtMinimumOnHand_FilteredTextBoxExtender" runat="server"
                                                                Enabled="True" FilterType="Numbers" TargetControlID="txtMinimumOnHand">
                                                            </asp:FilteredTextBoxExtender>
                                                        </td>
                                                    </tr>
                                                    <tr bgcolor="#f2f5f8">
                                                        <td>
                                                            <asp:Literal ID="lbltechref1" runat="server" Text="Technical Reference:" />
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                        <td>
                                                            <SPS:SPSTextBox ID="txtTechnicalReference" runat="server" MaxLength="30" OnTextChanged="txtTechnicalReference_TextChanged"
                                                                Width="350px" TextMode="MultiLine" />
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Literal ID="lbluserguide" runat="server" Text="User Guide:" />
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                        <td>
                                                            <asp:ComboBox ID="ddlUserGuide" runat="server" DropDownStyle="DropDownList" AutoCompleteMode="SuggestAppend"
                                                                MaxLength="0" RenderMode="Block" />
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Literal ID="lbluploaduserguide" runat="server" Text="Upload User Guide:" />
                                                            <asp:RequiredFieldValidator ID="reqvaluserguide" runat="server" ControlToValidate="FileUpload1"
                                                                Display="Dynamic" ErrorMessage="Select Upload User Guide file" SetFocusOnError="True"
                                                                ValidationGroup="UploadValidation">*</asp:RequiredFieldValidator>
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                        <td>
                                                            <asp:FileUpload ID="FileUpload1" runat="server" Visible="false" />
                                                            <input id="ImageFile" contenteditable="false" runat="server" name="ImageFile" type="file"
                                                                visible="false" style="width: 300px" />
                                                            <asp:Button ID="btnftpuserguide" runat="server" Text="Upload" ValidationGroup="UploadValidation"
                                                                OnClientClick="return OpenPopup('type=UserGuide');" OnClick="btnftpuserguide_Click" />
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                        <td colspan="2">
                                                            <asp:Label ID="lblmsgupload" runat="server" Font-Bold="True" ForeColor="Green" />
                                                        </td>
                                                    </tr>
                                                    <tr bgcolor="#f2f5f8">
                                                        <td>
                                                            <asp:Literal ID="lblcomments" runat="server" Text="Comments:" />
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                        <td>
                                                            <SPS:SPSTextBox ID="txtComments" runat="server" TextMode="MultiLine" Width="350px"
                                                                MaxLength="4000" />
                                                        </td>
                                                        <td colspan="3">
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Literal ID="lblcompatibility0" runat="server" Text="Compatibility:" />
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                        <td>
                                                            <SPS:SPSTextBox ID="txtCompatibility" runat="server" TextMode="MultiLine" Width="350px"
                                                                MaxLength="4000" />
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                    <tr bgcolor="#f2f5f8">
                                                        <td>
                                                            <asp:Literal ID="lblnotes" runat="server" Text="Notes:" />
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                        <td>
                                                            <SPS:SPSTextBox ID="txtNotes" runat="server" MaxLength="4000" TextMode="MultiLine"
                                                                Width="350px" />
                                                        </td>
                                                        <td colspan="3">
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Literal ID="lblcreatedby" runat="server" Text="Created By:" />
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                        <td>
                                                            <SPS:SPSTextBox ID="txtCreatedBy" runat="server" ReadOnly="True" MaxLength="21" BackColor="#CCCCCC" />
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                        <td>
                                                            <asp:Literal ID="lbldatecreated" runat="server" Text="Date Created:" />
                                                        </td>
                                                        <td>
                                                            <SPS:SPSTextBox ID="txtCreatedDate" runat="server" ReadOnly="True" BackColor="#CCCCCC" />
                                                        </td>
                                                    </tr>
                                                    <tr bgcolor="#f2f5f8">
                                                        <td>
                                                            <asp:Literal ID="lblupdatedby" runat="server" Text="Updated By:" />
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                        <td>
                                                            <SPS:SPSTextBox ID="txtUpdatedBy" runat="server" ReadOnly="True" MaxLength="21" BackColor="#CCCCCC" />
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                        <td>
                                                            <asp:Literal ID="lbllastupdated" runat="server" Text="Last Updated:" />
                                                        </td>
                                                        <td>
                                                            <SPS:SPSTextBox ID="txtUpdatedDate" runat="server" ReadOnly="True" BackColor="#CCCCCC" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="6">
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                </table>
                                                <table id="tblbtn" align="center">
                                                    <tr>
                                                        <td align="right">
                                                            <asp:ImageButton ID="btnNew" runat="server" ImageUrl="~/Shared/Images/sw_Add.JPG"
                                                                OnClick="btnNew_Click" CausesValidation="False" />
                                                        </td>
                                                        <td>
                                                            <asp:ImageButton ID="btnUpdate" ImageUrl="~/Shared/Images/sw_Update.JPG" OnClick="btnUpdate_Click"
                                                                CausesValidation="False" runat="server" />
                                                        </td>
                                                        <td align="left">
                                                            <asp:ImageButton ID="btnSave" runat="server" ImageUrl="~/Shared/Images/sw_Save.JPG"
                                                                OnClick="btnSave_Click" />
                                                        </td>
                                                        <td align="left" style="margin-left: 40px">
                                                            <asp:ImageButton ID="btnDelete" runat="server" ImageUrl="~/Shared/Images/sw_Delete.JPG"
                                                                CausesValidation="False" OnClick="btnDelete_Click" />
                                                        </td>
                                                        <td align="left">
                                                            <asp:ImageButton ID="btnCancel" runat="server" ImageUrl="~/Shared/Images/sw_Cancel.JPG"
                                                                OnClick="btnCancel_Click" CausesValidation="False" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right" colspan="5">
                                                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
                                                                ShowSummary="False" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right" colspan="5">
                                                            <asp:ValidationSummary ID="ValidationSummary2" runat="server" ShowMessageBox="True"
                                                                ShowSummary="False" ValidationGroup="UploadValidation" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ContentTemplate>
                                        </asp:TabPanel>
                                        <asp:TabPanel ID="TabPanel2" runat="server" HeaderText="Related Releases & Kits">
                                            <ContentTemplate>
                                                <asp:Label ID="lblchboxstatus" runat="server" Font-Bold="True" ForeColor="Red" Visible="False" />
                                                <fieldset align="middle">
                                                    <legend class="legendlabel">Attach Kit Information</legend>
                                                    <table border="0" style="width: 100%">
                                                        <tr>
                                                            <td>
                                                                <asp:CheckBox ID="chkAddKit" runat="server" AutoPostBack="True" OnCheckedChanged="chkAddKit_CheckedChanged"
                                                                    Text="Attach Kits" Font-Bold="True" Font-Size="Small" ToolTip="Check Attach Kits to add Kit Information" />
                                                            </td>
                                                            <td>
                                                                <asp:ComboBox ID="ddlAddKit" runat="server" DropDownStyle="DropDownList" AutoCompleteMode="SuggestAppend"
                                                                    MaxLength="0" EnableViewState="false" Visible="false" RenderMode="Block">
                                                                </asp:ComboBox>
                                                            </td>
                                                            <td>
                                                                <asp:ImageButton ID="btnAddKit" runat="server" ImageUrl="~/Shared/images/sw_AttachKit.jpg"
                                                                    OnClick="btnAddKit_Click" Visible="False" />
                                                            </td>
                                                            <td>&nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="4">
                                                                <asp:Panel ID="Panel1" runat="server" ScrollBars="Both" Width="850px">
                                                                    <asp:GridView ID="gvRelatedKits" runat="server" AutoGenerateColumns="False" DataKeyNames="KIT_PART_NUMBER"
                                                                        CellPadding="4" ForeColor="#333333" GridLines="None" Width="800px" OnRowDataBound="gvRelatedKits_RowDataBound"
                                                                        OnRowDeleting="gvRelatedKits_RowDeleting">
                                                                        <AlternatingRowStyle BackColor="White" />
                                                                        <Columns>
                                                                            <asp:TemplateField>
                                                                                <ItemTemplate>
                                                                                    <asp:ImageButton ID="imgbtnDelete" CommandName="Delete" Text="Edit" runat="server"
                                                                                        ImageUrl="~/Shared/Images/delete.jpg" ToolTip="Delete" Height="15px" Width="15px"
                                                                                        CausesValidation="False" /></ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:BoundField DataField="KIT_PART_NUMBER" HeaderText="Kit Part Number">
                                                                                <HeaderStyle Wrap="False" />
                                                                                <ItemStyle HorizontalAlign="Center" Wrap="False" />
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="KIT_DESCRIPTION" HeaderText="Kit Description">
                                                                                <HeaderStyle Wrap="False" />
                                                                                <ItemStyle HorizontalAlign="Center" Wrap="False" />
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="SUPERCEDED_KIT" HeaderText="Superceded Kit">
                                                                                <HeaderStyle Wrap="False" />
                                                                                <ItemStyle HorizontalAlign="Center" Wrap="False" />
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="MANUFACTURER_REMARKS" HeaderText="Manufacturer Remarks">
                                                                                <HeaderStyle Wrap="False" />
                                                                                <ItemStyle HorizontalAlign="Center" Wrap="False" />
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="DOWNLOAD_FILE" HeaderText="Download File">
                                                                                <HeaderStyle Wrap="False" />
                                                                                <ItemStyle HorizontalAlign="Center" Wrap="False" />
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="RELEASE_NOTES_FILE" HeaderText="Release Notes File">
                                                                                <HeaderStyle Wrap="False" />
                                                                                <ItemStyle HorizontalAlign="Center" Wrap="False" />
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="USER_MANUAL_FILE" HeaderText="User Manual File">
                                                                                <HeaderStyle Wrap="False" />
                                                                                <ItemStyle HorizontalAlign="Center" Wrap="False" />
                                                                            </asp:BoundField>
                                                                        </Columns>
                                                                        <EditRowStyle BackColor="#2461BF" />
                                                                        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                                                        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                                                        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                                                        <RowStyle BackColor="#EFF3FB" />
                                                                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                                                    </asp:GridView>
                                                                </asp:Panel>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    </legend>
                                                </fieldset>
                                                <fieldset align="middle">
                                                    <legend class="legendlabel">Add Related Release ID's</legend>
                                                    <table>
                                                        <tr>
                                                            <td style="width: 207px">
                                                                <asp:CheckBox ID="chkAddRelated" runat="server" AutoPostBack="True" Text="Add Related Release ID's"
                                                                    Font-Bold="True" Font-Size="Small" OnCheckedChanged="chkAddRelated_CheckedChanged"
                                                                    ToolTip="Check Add Related Release ID's" />
                                                            </td>
                                                            <td>
                                                                <asp:ComboBox ID="ddlAddRelated" runat="server" AutoCompleteMode="SuggestAppend"
                                                                    RenderMode="Block" DropDownStyle="DropDownList" MaxLength="0" EnableViewState="false"
                                                                    Visible="false">
                                                                </asp:ComboBox>
                                                            </td>
                                                            <td>
                                                                <asp:ImageButton ID="btnAddRelated" runat="server" ImageUrl="~/Shared/images/sw_Attach.jpg"
                                                                    OnClick="btnAddRelated_Click" Visible="False" />
                                                            </td>
                                                            <td>&nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="4">
                                                                <asp:GridView ID="gvRelatedReleases" runat="server" DataKeyNames="RELEASE_ID" AutoGenerateColumns="False"
                                                                    CellPadding="4" ForeColor="#333333" GridLines="None" OnPageIndexChanging="gvRelatedReleases_PageIndexChanging"
                                                                    OnRowDataBound="gvRelatedReleases_RowDataBound" OnRowDeleting="gvRelatedReleases_RowDeleting">
                                                                    <AlternatingRowStyle BackColor="White" />
                                                                    <Columns>
                                                                        <asp:TemplateField>
                                                                            <ItemTemplate>
                                                                                <asp:ImageButton ID="imgbtnDelete" CommandName="Delete" Text="Edit" runat="server"
                                                                                    ImageUrl="~/Shared/Images/delete.jpg" ToolTip="Delete" Height="15px" Width="15px"
                                                                                    CausesValidation="False" />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Related Release_Id">
                                                                            <ItemTemplate>
                                                                                <asp:Literal ID="lblChildControl" runat="server" Text='<%# Eval("RELEASE_ID") %>' />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                    </Columns>
                                                                    <EditRowStyle BackColor="#2461BF" />
                                                                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                                                    <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                                                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                                                    <RowStyle BackColor="#EFF3FB" />
                                                                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                                                </asp:GridView>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    </legend>
                                                </fieldset>
                                                <fieldset align="middle">
                                                    <legend class="legendlabel">Kit Item Information</legend>
                                                    <table border="0" style="width: 100%">
                                                        <tr>
                                                            <td>
                                                                <asp:Panel ID="Panel3" runat="server" ScrollBars="Both" Width="850px">
                                                                    <asp:GridView ID="gvRelatedKitDetails" runat="server" AutoGenerateColumns="False"
                                                                        CellPadding="4" ForeColor="#333333" GridLines="None" Width="800px" AllowPaging="True">
                                                                        <AlternatingRowStyle BackColor="White" />
                                                                        <Columns>
                                                                            <asp:BoundField DataField="ITEM_DESCRIPTION" HeaderText="Item Description">
                                                                                <HeaderStyle Wrap="False" />
                                                                                <ItemStyle Wrap="False" />
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="ITEM_TYPE" HeaderText="Item Type">
                                                                                <HeaderStyle Wrap="False" />
                                                                                <ItemStyle Wrap="False" />
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="SUB_ASSEMBLY" HeaderText="Sub Assembly">
                                                                                <HeaderStyle Wrap="False" />
                                                                                <ItemStyle Wrap="False" />
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="LOCATION" HeaderText="Location">
                                                                                <HeaderStyle Wrap="False" />
                                                                                <ItemStyle Wrap="False" />
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="VERSION" HeaderText="Version">
                                                                                <HeaderStyle Wrap="False" />
                                                                                <ItemStyle Wrap="False" />
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="SPEED" HeaderText="Speed">
                                                                                <HeaderStyle Wrap="False" />
                                                                                <ItemStyle Wrap="False" />
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="CHECK_SUM" HeaderText="Check Sum">
                                                                                <HeaderStyle Wrap="False" />
                                                                                <ItemStyle Wrap="False" />
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="MANUFACTURER" HeaderText="Manufacturer">
                                                                                <HeaderStyle Wrap="False" />
                                                                                <ItemStyle Wrap="False" />
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="FILE_FORMAT" HeaderText="File Format">
                                                                                <HeaderStyle Wrap="False" />
                                                                                <ItemStyle Wrap="False" />
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="FILE_NAME" HeaderText="File Name">
                                                                                <HeaderStyle Wrap="False" />
                                                                                <ItemStyle Wrap="False" />
                                                                            </asp:BoundField>
                                                                        </Columns>
                                                                        <EditRowStyle BackColor="#2461BF" />
                                                                        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                                                        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                                                        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                                                        <RowStyle BackColor="#EFF3FB" />
                                                                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                                                    </asp:GridView>
                                                                </asp:Panel>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </fieldset>
                                                <table id="Table1" align="center">
                                                    <tr>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                        <td>
                                                            <asp:ImageButton ID="btnSaveRelated" runat="server" ImageUrl="~/Shared/Images/sw_Save.JPG"
                                                                OnClick="btnSave_Click" />
                                                        </td>
                                                        <td>
                                                            <asp:ImageButton ID="btnUpdateRelated" runat="server" ImageUrl="~/Shared/Images/sw_Update.JPG"
                                                                OnClick="btnUpdate_Click" />
                                                        </td>
                                                        <td>
                                                            <asp:ImageButton ID="btnDeleteRelated" runat="server" ImageUrl="~/Shared/images/sw_Delete.JPG"
                                                                OnClick="btnDelete_Click" />
                                                        </td>
                                                        <td>
                                                            <asp:ImageButton ID="btnCancelRelated" runat="server" ImageUrl="~/Shared/images/sw_Cancel.JPG"
                                                                OnClick="btnCancel_Click" />
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ContentTemplate>
                                        </asp:TabPanel>
                                    </asp:TabContainer>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td style="width: 4px">
                    </td>
                </tr>
            </table>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="TabContainer1$TabPanel1$btnftpuserguide" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
