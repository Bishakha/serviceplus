﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using SoftwarePlus.BusinessAccess;
using System.Collections;
using System.Data;
using System.Drawing;
using ServicesPlusException;

public partial class sw_ReleaseLog_cm : System.Web.UI.Page {
    #region Properties
    Model model = new Model();
    Release release = new Release();
    SoftwarePlusConfigManager softwareConfigMgr = new SoftwarePlusConfigManager();
    Utilities logUtility = new Utilities();
    const string SELECT_ONE = "--Select One--";
    #endregion

    protected void Page_Init(object sender, EventArgs e) {
        if (Session["UserName"] == null)
            Response.Redirect("Default.aspx?post=ReleaseLog", false);
        // Restore the control data
        if (Session["PageMode"] != null)
            SetPageMode(Session["PageMode"].ToString());
        if (Session["ReleaseList"] != null) {
            FillReleaseDropdown();
            FillRelatedReleaseDropdown();
        }
        if (Session["KitList"] != null)
            FillAttachKitDropdown();
        if (Session["ModelList"] != null)
            FillModelDropdown();
    }

    protected void Page_Load(object sender, EventArgs e) {
        try {
            if (Session["UserName"] == null)
                Response.Redirect("Default.aspx?post=ReleaseLog", false);
            if (!Page.IsPostBack) {
                Session.Remove("ReleaseList");   // Force the Release list to be reloaded.
                InitControls();
                if (Request.QueryString["Identity"] != null) {
                    string releaseId = Request.QueryString["Identity"].ToString();
                    ddlReleaseID.SelectedValue = releaseId;
                }
            } else {
                AddUploadedFiledtoDropdown();   //It will bind the lasted file name to Dropdown if there is any upload done.
                ddlAddKit.Visible = chkAddKit.Checked;
                btnAddKit.Visible = chkAddKit.Checked;
                ddlAddRelated.Visible = chkAddRelated.Checked;
                btnAddRelated.Visible = chkAddRelated.Checked;
            }
        } catch (Exception ex) {
            SetMessage(Utilities.WrapExceptionforUI(ex), true);
        }
    }

    #region Control Events
    protected void TabContainer1_ActiveTabChanged(object sender, EventArgs e) {
        try {
            if (TabContainer1.ActiveTabIndex == 0) {
                if (Session["SaveChildModel"] != null) {
                    ReloadReleaseData();
                    Session.Remove("SaveChildModel");
                }
            } else {
                ddlAddKit.Visible = chkAddKit.Checked;
                ddlAddRelated.Visible = chkAddRelated.Checked;
            }
            //GetSelectedReleaseData();
        } catch (Exception ex) {
            SetMessage(Utilities.WrapExceptionforUI(ex), true);
        }
    }

    protected void ddlReleaseID_SelectedIndexChanged(object sender, EventArgs e) {
        try {
            if ((Session["SelectedRelease"] != null) && (Session["SelectedRelease"].ToString() == ddlReleaseID.SelectedValue))
                return;
            else if (ddlReleaseID.SelectedIndex < 1) {
                ClearAllFields();
                return;
            }
            SetMessage("");
            GetSelectedReleaseData();
            Session["SelectedRelease"] = ddlReleaseID.SelectedValue;
        } catch (Exception ex) {
            SetMessage(Utilities.WrapExceptionforUI(ex), true);
        }
    }

    protected void ddlModelID_SelectedIndexChanged(object sender, EventArgs e) {
        try {
            if ((Session["SelectedModel"] != null) && (Session["SelectedModel"].ToString() == ddlModelID.SelectedValue))
                return;
            else if (ddlModelID.SelectedIndex < 1) {
                ClearAllFields();
                return;
            }
            SetMessage("");
            DataSet dsreleaseLogData = release.GetReleaseLogData(ddlModelID.Text);
            FillVersionTextboxFromModel(dsreleaseLogData);
            txtReleaseID.Text = ddlModelID.Text + " V" + txtVersion.Text;
            Session["SelectedModel"] = ddlModelID.SelectedValue;
        } catch (Exception ex) {
            SetMessage(Utilities.WrapExceptionforUI(ex), true);
        }
    }

    protected void ddlReleaseType_SelectedIndexChanged(object sender, EventArgs e) {
        try {
            SetMessage("");
            if (ddlReleaseType.SelectedValue == "Latest" || ddlReleaseType.SelectedValue == "Internal Only" || ddlReleaseType.SelectedValue == "Tokuren" || ddlReleaseType.SelectedValue == "Beta" || ddlReleaseType.SelectedValue == "Special" || ddlReleaseType.SelectedValue == "TBD") {
                ddlDistribution.SelectedValue = "Available";
            } else if (ddlReleaseType.SelectedValue == "Previous") {
                ddlDistribution.SelectedValue = "Discontinued";
            }
        } catch (Exception ex) {
            SetMessage(Utilities.WrapExceptionforUI(ex), true);
        }
    }

    protected void ddlDistribution_SelectedIndexChanged(object sender, EventArgs e) {
        try {
            SetMessage("");
            if ((ddlDistribution.SelectedValue == "Discontinued") || (ddlReleaseType.SelectedValue == "Internal Only")) {
                if (ddlVisibility.Items.Contains(new ListItem("Public")))
                    ddlVisibility.Items.Remove("Public");
                ddlVisibility.SelectedValue = "Employees";
            } else if ((ddlDistribution.SelectedValue == "Available") || (ddlDistribution.SelectedValue == "Contact Service")) {
                if (!ddlVisibility.Items.Contains(new ListItem("Public")))
                    ddlVisibility.Items.Add("Public");
                ddlVisibility.SelectedValue = "Public";
            }
            #region Old Commented Code (Replaced by the above)
            /*if ((ddlReleaseType.SelectedValue == "Latest" && ddlDistribution.SelectedValue == "Available") || (ddlReleaseType.SelectedValue == "Latest" && ddlDistribution.SelectedValue == "Contact Service")) {
                if (ddlVisibility.Text == "Public") {
                    ddlVisibility.Text = "Public";
                    ddlVisibility.Enabled = true;
                } else {
                    if (!ddlVisibility.Items.Contains(new ListItem("Public"))) {
                        ddlVisibility.Items.Add("Public");
                    }
                    ddlVisibility.SelectedValue = "Public";
                    ddlVisibility.Enabled = true;
                }
            }
            if (ddlReleaseType.SelectedValue == "Latest" && ddlDistribution.SelectedValue == "Discontinued") {
                ddlVisibility.Text = "Employees";
                ddlVisibility.Enabled = true;
                ddlVisibility.Items.Remove("Public");
            }
            if ((ddlReleaseType.SelectedValue == "Previous" && ddlDistribution.SelectedValue == "Available") || (ddlReleaseType.SelectedValue == "Previous" && ddlDistribution.SelectedValue == "Contact Service")) {
                if (ddlVisibility.Text == "Public") {
                    ddlVisibility.Text = "Public";
                    ddlVisibility.Enabled = true;
                } else {
                    if (!ddlVisibility.Items.Contains(new ListItem("Public"))) {
                        ddlVisibility.Items.Add("Public");
                    }
                    ddlVisibility.SelectedValue = "Public";
                    ddlVisibility.Enabled = true;

                }
            }
            if (ddlReleaseType.SelectedValue == "Previous" && ddlDistribution.SelectedValue == "Discontinued") {
                ddlVisibility.Text = "Employees";
                ddlVisibility.Enabled = true;
                ddlVisibility.Items.Remove("Public");
            }
            if ((ddlReleaseType.SelectedValue == "Internal Only" && ddlDistribution.SelectedValue == "Available") || (ddlReleaseType.SelectedValue == "Internal Only" && ddlDistribution.SelectedValue == "Discontinued") || (ddlReleaseType.SelectedValue == "Internal Only" && ddlDistribution.SelectedValue == "Contact Service")) {
                ddlVisibility.Text = "Employees";
                ddlVisibility.Enabled = true;
                ddlVisibility.Items.Remove("Public");
            }
            if ((ddlReleaseType.SelectedValue == "Tokuren" || ddlReleaseType.SelectedValue == "Beta" || ddlReleaseType.SelectedValue == "Special" || ddlReleaseType.SelectedValue == "TBD") && (ddlDistribution.SelectedValue == "Available" || ddlDistribution.SelectedValue == "Contact Service")) {
                if (ddlVisibility.Text == "Public") {
                    ddlVisibility.Text = "Public";
                    ddlVisibility.Enabled = true;
                } else {
                    if (!ddlVisibility.Items.Contains(new ListItem("Public"))) {
                        ddlVisibility.Items.Add("Public");
                    }
                    ddlVisibility.SelectedValue = "Public";
                    ddlVisibility.Enabled = true;

                }
            }
            if ((ddlReleaseType.SelectedValue == "Tokuren" || ddlReleaseType.SelectedValue == "Beta" || ddlReleaseType.SelectedValue == "Special" || ddlReleaseType.SelectedValue == "TBD") && (ddlDistribution.SelectedValue == "Available" || ddlDistribution.SelectedValue == "Discontinued")) {
                ddlVisibility.Text = "Employees";
                ddlVisibility.Enabled = true;
                ddlVisibility.Items.Remove("Public");

            }*/
            #endregion
        } catch (Exception ex) {
            SetMessage(Utilities.WrapExceptionforUI(ex), true);
        }
    }

    protected void chkAddKit_CheckedChanged(object sender, EventArgs e) {
        try {
            if (Session["PageMode"].ToString() == "New") {
                SetMessage("You must SAVE your new Release before you can attach related kits or releases.", true);
                chkAddKit.Checked = false;
                return;
            }
            SetMessage("");
            ddlAddKit.Visible = chkAddKit.Checked;
            btnAddKit.Visible = chkAddKit.Checked;
            if ((chkAddKit.Checked) && (ddlAddKit.Items.Count == 0))
                FillAttachKitDropdown();
        } catch (Exception ex) {
            SetMessage(Utilities.WrapExceptionforUI(ex), true);
        }
    }

    protected void chkAddRelated_CheckedChanged(object sender, EventArgs e) {
        try {
            if (Session["PageMode"].ToString() == "New") {
                SetMessage("You must SAVE your new Release before you can attach related kits or releases.", true);
                chkAddRelated.Checked = false;
                return;
            }
            SetMessage("");
            ddlAddRelated.Visible = chkAddRelated.Checked;
            btnAddRelated.Visible = chkAddRelated.Checked;
            if ((chkAddRelated.Checked) && (ddlAddRelated.Items.Count == 0))
                FillRelatedReleaseDropdown();
        } catch (Exception ex) {
            SetMessage(Utilities.WrapExceptionforUI(ex), true);
        }
    }

    protected void btnNew_Click(object sender, ImageClickEventArgs e) {
        try {
            SetMessage("");
            SetPageMode("New");
            SetButtonVisibility("New");
            //DefaultDataFortheControls();
            ClearAllFields();
            txtCreatedBy.Text = Session["userId"].ToString();
            txtCreatedDate.Text = DateTime.Now.ToShortDateString();
            txtReleaseDate.Text = DateTime.Now.AddDays(1).ToShortDateString();
            txtMinimumOnHand.Text = "0";
            txtReleaseQuantity.Text = "0";
            hdnUserGuide.Value = string.Empty;
        } catch (Exception ex) {
            SetMessage(Utilities.WrapExceptionforUI(ex), true);
        }
    }

    protected void btnSave_Click(object sender, ImageClickEventArgs e) {
        try {
            SetMessage("");
            if (AddReleaseLog()) {
                SetPageMode("Edit");
                SetButtonVisibility("Edit");
                SetMessage("New Release saved successfully.");
            }
        } catch (Exception ex) {
            SetMessage(Utilities.WrapExceptionforUI(ex), true);
        }
    }

    protected void btnUpdate_Click(object sender, ImageClickEventArgs e) {
        try {
            SetMessage("");
            UpdateRelease();
            hdnUserGuide.Value = string.Empty;
            chkAddKit.Checked = false;
            chkAddRelated.Checked = false;
            SetPageMode("Edit");
            SetButtonVisibility("Edit");
            //SetMessage("Release updated successfully.");
        } catch (Exception ex) {
            SetMessage(Utilities.WrapExceptionforUI(ex), true);
        }
    }

    protected void btnDelete_Click(object sender, ImageClickEventArgs e) {
        string modelID, version;

        try {
            if ((gvRelatedKits.Rows.Count > 0) || (gvRelatedReleases.Rows.Count > 0)) {
                SetMessage("Cannot delete a Release until you remove all Related Kits and Releases.", true);
                TabContainer1.ActiveTabIndex = 1;
                return;
            }
            SetMessage("");
            ConvertRelease(ddlReleaseID.SelectedValue, out modelID, out version);
            release.DeleteReleaseLog(modelID, version);
            ReloadReleaseData();
            hdnUserGuide.Value = string.Empty;
            SetPageMode("Edit");
            SetButtonVisibility("Edit");
            SetMessage("Release deleted successfully.");
            ddlReleaseID.SelectedIndex = 0;
            ClearAllFields();
        } catch (Exception ex) {
            SetMessage(Utilities.WrapExceptionforUI(ex), true);
        }
    }

    protected void btnCancel_Click(object sender, ImageClickEventArgs e) {
        try {
            SetMessage("");
            if (Session["PageMode"].ToString() == "New") {
                // Cancel the New item and select nothing in Release dropdown
                ddlReleaseID.SelectedIndex = 0;
                SetPageMode("Edit");
                SetButtonVisibility("Edit");
                SetMessage("New Release cancelled.");
            } else {
                // Cancel the Update to the current Release and reset the data from the database.
                GetSelectedReleaseData();
                SetMessage("Release update cancelled. Data has been reloaded from the database.");
            }
        } catch (Exception ex) {
            SetMessage(Utilities.WrapExceptionforUI(ex), true);
        }
    }

    protected void btnAddKit_Click(object sender, ImageClickEventArgs e) {
        try {
            if (ddlAddKit.SelectedIndex == 0) {
                SetMessage("Please select a Kit.", true);
                return;
            }
            string modelID, version;
            string[] data = ddlAddKit.SelectedValue.Split('-');
            SetMessage("", false);
            ConvertRelease(ddlReleaseID.SelectedValue, out modelID, out version);
            release.AddKitReleaseLog(modelID, version, data[0]);
            FillRelatedKitGridview();
            SetMessage("Kit attached successfully.");
        } catch (Exception ex) {
            SetMessage(Utilities.WrapExceptionforUI(ex), true);
        }
    }

    protected void btnAddRelated_Click(object sender, ImageClickEventArgs e) {
        try {
            string modelID, version;
            SetMessage("");
            ConvertRelease(ddlReleaseID.SelectedValue, out modelID, out version);
            release.AddRelatedReleaseLog(modelID, version, ddlAddRelated.SelectedValue);
            FillRelatedReleaseGridview();
            SetMessage("Related Release added successfully.");
        } catch (Exception ex) {
            if (ex.Message.Contains("unique constraint (SERVICESPLUS.PDAT_RELEASES_RELATED) violated")) {
                SetMessage("Duplicate records are not allowed.", true);
            } else if (ex.Message.Contains("integrity constraint (SERVICESPLUS.FDATRELEASESRELATEDDATRELEASES) violated")) {
                SetMessage("Integrity constraint violated.", true);
            } else {
                SetMessage(Utilities.WrapExceptionforUI(ex), true);
            }
        }
    }

    protected void btnftpuserguide_Click(object sender, EventArgs e) {
        try {
            Response.Redirect("FileUpload.aspx?type=UserGuide");
        } catch (Exception ex) {
            if (ex.Message.Contains("Auth fail")) {
                SetMessage("SFTP Credentials are invalid to upload the file.", true);
            } else {
                SetMessage(Utilities.WrapExceptionforUI(ex), true);
            }
        }
    }

    protected void txtMinimumOnHand_TextChanged(object sender, EventArgs e) {
        Session.Add("isupdated", "true");
        Session.Add("txtMinimumOnHand", "Min On Hand was updated to  " + txtMinimumOnHand.Text + ";");
    }

    protected void txtReleaseQuantity_TextChanged(object sender, EventArgs e) {
        Session.Add("isupdated", "true");
        Session.Add("txtReleaseQuantity", "Release quantity was updated to  " + txtReleaseQuantity.Text + ";");
    }

    protected void txtStartSerialNumber_TextChanged(object sender, EventArgs e) {
        Session.Add("isupdated", "true");
        Session.Add("txtStartSerialNumber", "Starting Serial Number was updated to  " + txtStartSerialNumber.Text + ";");
    }

    protected void txtEndSerialNumber_TextChanged(object sender, EventArgs e) {
        Session.Add("isupdated", "true");
        Session.Add("txtEndSerialNumber", "Ending Serial Number was updated to  " + txtEndSerialNumber.Text + ";");
    }

    protected void txtTechnicalReference_TextChanged(object sender, EventArgs e) {
        Session.Add("isupdated", "true");
        Session.Add("txtTechnicalReference", "Technical Reference was updated to " + txtTechnicalReference.Text + ";");
    }

    protected void txtVersion_TextChanged(object sender, EventArgs e) {
        Session["ModelVersion"] = txtVersion.Text;
        if ((txtReleaseID.Visible) && (ddlModelID.SelectedIndex > 0))
            txtReleaseID.Text = ddlModelID.Text + " V" + txtVersion.Text;
    }

    protected void gvRelatedKits_RowDataBound(object sender, GridViewRowEventArgs e) {
        try {
            if (e.Row.RowType == DataControlRowType.DataRow) {
                // Getting primary key from current row
                string kitPartNumber = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "KIT_PART_NUMBER"));
                ImageButton lnkbtndelete = (ImageButton)e.Row.FindControl("imgbtnDelete");
                // Raising javascript confirmationbox whenver user clicks on link button
                if (lnkbtndelete != null) {
                    //lnkbtndelete.Attributes.Add("onclick", "javascript:return ConfirmationBox('" + kitPartNumber + "')");
                    lnkbtndelete.Attributes["onclick"] = "if(!confirm('Do you want to delete Kit # " + kitPartNumber + "?')){ return false; };";
                }
                // Only SoftwareAdmin is allowed to Delete items.
                if (Session["Role"].ToString() != "SoftwareAdmin")
                    lnkbtndelete.Visible = false;
            }
        } catch (Exception ex) {
            SetMessage(Utilities.WrapExceptionforUI(ex), true);
        }
    }

    protected void gvRelatedKits_RowDeleting(object sender, GridViewDeleteEventArgs e) {
        try {
            //SetMessage("");
            string kitID, modelID, version;
            kitID = gvRelatedKits.DataKeys[e.RowIndex].Values["KIT_PART_NUMBER"].ToString();
            ConvertRelease(ddlReleaseID.SelectedValue, out modelID, out version);
            SetMessage(String.Format("Deleting association with Model: {0}, Version: {1}, child Kit:{2}.", modelID, version, kitID));
            release.DeleteKitReleaseLog(modelID, version, kitID);
            FillRelatedKitGridview();
        } catch (Exception ex) {
            SetMessage(Utilities.WrapExceptionforUI(ex), true);
        }
    }

    protected void gvRelatedReleases_RowDataBound(object sender, GridViewRowEventArgs e) {
        try {
            if (e.Row.RowType == DataControlRowType.DataRow) {
                // Getting primary key from current row
                string strRELEASE_ID = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "RELEASE_ID"));
                ImageButton lnkbtndelete = (ImageButton)e.Row.FindControl("imgbtnDelete");
                // Raising javascript confirmationbox whenver user clicks on link button
                if (lnkbtndelete != null) {
                    //lnkbtnresult.Attributes.Add("onclick", "javascript:return ConfirmationBoxRelID('" + strRELEASE_ID + "')");
                    lnkbtndelete.Attributes["onclick"] = "if(!confirm('Do you want to delete Related Release # " + strRELEASE_ID + "?')){ return false; };";
                }
                // Only SoftwareAdmin is allowed to Delete items.
                if (Session["Role"].ToString() != "SoftwareAdmin")
                    lnkbtndelete.Visible = false;
            }
        } catch (Exception ex) {
            SetMessage(Utilities.WrapExceptionforUI(ex), true);
        }
    }

    protected void gvRelatedReleases_PageIndexChanging(object sender, GridViewPageEventArgs e) {
        try {
            SetMessage("");
            if (gvRelatedReleases.EditIndex != -1) {
                // Use the Cancel property to cancel the paging operation.
                e.Cancel = true;
                // Display an error message.
                int newPageNumber = e.NewPageIndex + 1;
            } else {
                DataSet ds = release.GetChildReleaseID(ddlModelID.SelectedValue, txtVersion.Text);
                gvRelatedReleases.PageIndex = e.NewPageIndex;
                gvRelatedReleases.DataSource = ds;
                gvRelatedReleases.DataBind();
            }
        } catch (Exception ex) {
            SetMessage(Utilities.WrapExceptionforUI(ex), true);
        }
    }

    protected void gvRelatedReleases_RowDeleting(object sender, GridViewDeleteEventArgs e) {
        try {
            //SetMessage("");
            string releaseID, modelID, version;
            releaseID = gvRelatedReleases.DataKeys[e.RowIndex].Values["RELEASE_ID"].ToString();
            ConvertRelease(ddlReleaseID.SelectedValue, out modelID, out version);
            SetMessage(String.Format("Deleting association with Model: {0}, Version: {1}, child Release:{2}. ", modelID, version, releaseID));
            release.DeleteRelatedReleaseLogID(modelID, version, releaseID);
            FillRelatedReleaseGridview();
        } catch (Exception ex) {
            if (ex.Message.Contains("unique constraint (SERVICESPLUS.PDAT_RELEASES_RELATED) violated")) {
                SetMessage("Duplicate records are not allowed.", true);
                //} else if (ex.Message.Contains("integrity constraint (SERVICESPLUS.FDATRELEASESRELATEDDATRELEASES) violated")) {

            } else {
                SetMessage(Utilities.WrapExceptionforUI(ex), true);
            }
        }
    }
    #endregion

    #region Helper Methods
    /// <summary>
    /// Turns an input Release into two outputs: Model and Version.
    /// </summary>
    private void ConvertRelease(string release, out string modelID, out string version) {
        string[] strtemp = release.Split(' ');
        modelID = strtemp[0].Trim();
        version = strtemp[1].TrimStart('V').Trim();
    }

    private void GetSelectedReleaseData() {
        if ((string.IsNullOrEmpty(ddlReleaseID.SelectedValue)) || (ddlReleaseID.SelectedIndex == 0)) {
            ClearAllFields();
            return;
        }
        string modelID, version;
        ConvertRelease(ddlReleaseID.SelectedValue, out modelID, out version);
        DataSet dsreleaseInfoData = release.GetReleaseIdVersionsData(modelID, version);
        BindReleaseDataToControls(dsreleaseInfoData);
        //if (TabContainer1.ActiveTabIndex == 1) {
        FillRelatedReleaseGridview();
        FillRelatedKitGridview();
        FillRelatedKitDetailGridview();
        //gvRelatedKitDetails.Visible = true;
        //}
    }

    /// <summary>
    /// Does the first-time loading of all the page data, and sets the page in the default mode.
    /// </summary>
    private void InitControls() {
        SetMessage("");
        SetPageMode("Edit");
        SetButtonVisibility("Edit");
        FillReleaseDropdown();
        FillModelDropdown();
        FillReleaseTypeDropdown();
        FillVisibilityDropdown();
        FillDistributionDropdown();
        FillReleaseStatusDropdown();
        FillUserGuideDropdown();
        GetSelectedReleaseData();
    }

    /// <summary>
    /// Toggle Visible and Enabled for various controls depending on the current mode and the active tab.
    /// </summary>
    /// <param name="mode">New, Edit or Delete. Leave blank to reset page in current mode.</param>
    private void SetPageMode(string mode) {
        //int activeTab = TabContainer1.ActiveTabIndex;
        if (string.IsNullOrEmpty(mode))
            mode = Session["PageMode"].ToString();
        else
            Session["PageMode"] = mode;

        switch (mode) {
            case "New":
                ddlReleaseID.Visible = false;
                txtReleaseID.Visible = true;
                ddlModelID.Visible = true;
                txtModelID.Visible = false;
                txtVersion.Enabled = true;
                // Tab 1
                gvRelatedKits.Visible = false;
                gvRelatedKitDetails.Visible = false;
                gvRelatedReleases.Visible = false;
                chkAddKit.Checked = false;
                chkAddRelated.Checked = false;
                break;
            case "Edit":
            default:
                ddlReleaseID.Visible = true;
                txtReleaseID.Visible = false;
                ddlModelID.Visible = false;
                txtModelID.Visible = true;
                txtVersion.Enabled = false;
                // Tab 1
                gvRelatedKits.Visible = true;
                gvRelatedKitDetails.Visible = true;
                gvRelatedReleases.Visible = true;
                break;
        }
    }

    /// <summary>
    /// Toggles the visibility of the lower buttons, used for modifying Release data.
    /// </summary>
    /// <param name="button">Values: Edit, New, Save</param>
    private void SetButtonVisibility(string button) {
        string userRole = Session["Role"].ToString();

        if (userRole == "SoftwareReadOnly") {
            btnNew.Visible = false;
            btnSave.Visible = false;
            btnUpdate.Visible = false;
            btnDelete.Visible = false;
            btnCancel.Visible = false;

            btnSaveRelated.Visible = false;
            btnUpdateRelated.Visible = false;
            btnDeleteRelated.Visible = false;
            btnCancelRelated.Visible = false;

            btnftpuserguide.Visible = false;
            chkAddKit.Visible = false;
            btnAddKit.Visible = false;
            chkAddRelated.Visible = false;
            btnAddRelated.Visible = false;
        } else if (userRole == "SoftwareAdmin" || userRole == "SoftwareEngineer") {
            if (button == "Edit") {
                btnNew.Visible = true;
                btnSave.Visible = false;
                btnUpdate.Visible = true;
                btnDelete.Visible = true;
                btnCancel.Visible = false;

                btnSaveRelated.Visible = false;
                btnUpdateRelated.Visible = true;
                btnDeleteRelated.Visible = true;
                btnCancelRelated.Visible = false;
            } else if (button == "New") {
                btnNew.Visible = false;
                btnSave.Visible = true;
                btnUpdate.Visible = false;
                btnDelete.Visible = false;
                btnCancel.Visible = true;

                btnSaveRelated.Visible = true;
                btnUpdateRelated.Visible = false;
                btnDeleteRelated.Visible = false;
                btnCancelRelated.Visible = true;
            } else if (button == "Save") {
                btnNew.Visible = true;
                btnSave.Visible = true;
                btnUpdate.Visible = false;
                btnDelete.Visible = false;
                btnCancel.Visible = true;

                btnSaveRelated.Visible = true;
                btnUpdateRelated.Visible = false;
                btnDeleteRelated.Visible = false;
                btnCancelRelated.Visible = true;
            }
        }
    }

    private void AddUploadedFiledtoDropdown() {
        if (!string.IsNullOrEmpty(hdnUserGuide.Value)) {
            string strReleaseLogFile = hdnUserGuide.Value;
            if (ddlUserGuide.Items.FindByValue(strReleaseLogFile) != null) {
                ddlUserGuide.SelectedValue = strReleaseLogFile;
            } else {
                ddlUserGuide.Items.Add(strReleaseLogFile);
                ddlUserGuide.SelectedValue = strReleaseLogFile;
            }
            hdnUserGuide.Value = null;
        }
    }

    private void ClearAllFields() {
        txtReleaseID.Text = "";
        txtModelID.Text = "";
        txtVersion.Text = "1.0";
        txtStartSerialNumber.Text = "";
        txtEndSerialNumber.Text = "";
        txtReleaseQuantity.Text = "";
        txtMinimumOnHand.Text = "";
        txtTechnicalReference.Text = "";
        txtComments.Text = "";
        txtCompatibility.Text = "";
        txtNotes.Text = "";
        txtCreatedBy.Text = "";
        txtCreatedDate.Text = "";
        txtUpdatedBy.Text = "";
        txtUpdatedDate.Text = "";

        ddlStatus.SelectedIndex = 0;
        ddlModelID.SelectedIndex = 0;
        ddlDistribution.SelectedIndex = 0;
        ddlUserGuide.SelectedIndex = 0;
        ddlReleaseType.SelectedIndex = 0;
        ddlVisibility.SelectedIndex = 0;

        chkAddKit.Checked = false;
        ddlAddKit.Visible = false;
        chkAddRelated.Checked = false;
        ddlAddRelated.Visible = false;
    }

    private void BindReleaseDataToControls(DataSet ds) {
        if (ds != null) {
            //DataTable dt = ds.Tables[0];
            //foreach (DataRow row in dt.Rows) {
            DataRow row = ds.Tables[0].Rows[0];
            ddlModelID.SelectedValue = row["MODEL_ID"].ToString();
            txtModelID.Text = row["MODEL_ID"].ToString();
            txtVersion.Text = row["VERSION"].ToString();
            txtReleaseDate.Text = Convert.ToDateTime(row["RELEASE_DATE"].ToString()).ToShortDateString();
            txtReleaseQuantity.Text = row["RELEASE_QUANTITY"].ToString();
            txtMinimumOnHand.Text = row["MINIMUM_ON_HAND"].ToString();
            txtStartSerialNumber.Text = row["START_NUMBER"].ToString();
            txtEndSerialNumber.Text = row["END_NUMBER"].ToString();
            txtTechnicalReference.Text = row["TECHNICAL_REFERENCE"].ToString();
            txtComments.Text = row["COMMENTS"].ToString();
            txtCompatibility.Text = row["COMPATIBILITY"].ToString();
            txtNotes.Text = row["NOTES"].ToString();
            Session["ExistingNotes"] = row["NOTES"].ToString();
            txtCreatedBy.Text = row["CREATED_BY"].ToString();
            txtCreatedDate.Text = row["DATE_CREATED"].ToString();
            txtUpdatedBy.Text = row["UPDATED_BY"].ToString();
            txtUpdatedDate.Text = row["LAST_UPDATED"].ToString();
            //ddlStatus.SelectedValue = row["RELEASE_STATUS"].ToString();
            ddlStatus.SelectedValue = (string.IsNullOrEmpty(row["RELEASE_STATUS"].ToString())) ? SELECT_ONE : row["RELEASE_STATUS"].ToString();
            ddlReleaseType.SelectedValue = (string.IsNullOrEmpty(row["RELEASE_TYPE"].ToString())) ? SELECT_ONE : row["RELEASE_TYPE"].ToString();
            ddlVisibility.SelectedValue = (string.IsNullOrEmpty(row["VISIBILITY_TYPE"].ToString())) ? SELECT_ONE : row["VISIBILITY_TYPE"].ToString();
            ddlDistribution.SelectedValue = (string.IsNullOrEmpty(row["DISTRIBUTION_TYPE"].ToString())) ? SELECT_ONE : row["DISTRIBUTION_TYPE"].ToString();
            ddlUserGuide.SelectedValue = row["USER_MANUAL_FILE"].ToString();
            //}
        }
    }

    /// <summary>
    /// Validates the form data depending on the command type. Also checks for existing Release data.
    /// </summary>
    /// <param name="commandType">New or Edit</param>
    /// <returns>True if data is valid, false if invalid or trying to create a Release that already exists.</returns>
    private bool ValidateData(string commandType) {
        string modelID = (ddlModelID.Visible ? ddlModelID.Text : txtModelID.Text);

        // If "Min. on Hand" or "Release Quantity" are blank, substitute for a zero without marking the page Invalid.
        if (!int.TryParse(txtReleaseQuantity.Text.Trim(), out int testInt))
            txtReleaseQuantity.Text = "0";
        if (!int.TryParse(txtMinimumOnHand.Text.Trim(), out testInt))
            txtMinimumOnHand.Text = "0";

        if (string.IsNullOrEmpty(modelID) || modelID == SELECT_ONE) {
            SetMessage("Please select the Model ID.", true);
        } else if (string.IsNullOrEmpty(txtVersion.Text.Trim())) {
            SetMessage("Please enter a Version number.", true);
        } else if ((string.IsNullOrEmpty(ddlReleaseType.SelectedValue)) || (ddlReleaseType.SelectedValue == SELECT_ONE)) {
            SetMessage("Please select the Release Type.", true);
        } else if ((string.IsNullOrEmpty(ddlVisibility.SelectedValue)) || (ddlVisibility.SelectedValue == SELECT_ONE)) {
            SetMessage("Please select the Visibility.", true);
        } else if ((string.IsNullOrEmpty(ddlDistribution.SelectedValue)) || (ddlDistribution.SelectedValue == SELECT_ONE)) {
            SetMessage("Please select the Distribution.", true);
        } else if ((txtReleaseDate.Text.Length == 0) || (!DateTime.TryParse(txtReleaseDate.Text, out DateTime testDate))) {
            SetMessage("Please enter a valid Released Date.", true);
        } else {   // Everything is valid, but let's check the Release exists or doesn't exist, depending on the action.
            if (release.ValidateReleaseLogData(modelID, txtVersion.Text) > 0) {
                if (commandType == "New") {   // If the method returns a Release, we cannot add the same one
                    SetMessage("Cannot create new Release. A Release already exists for Model: " + ddlModelID.Text + " and Version: " + txtVersion.Text + ".", true);
                    return false;
                }
            } else {
                if (commandType == "Edit") {   // We cannot Edit if we can't find the original Release
                    SetMessage("Cannot update Release. No Release found for Model: " + txtModelID.Text + " and Version: " + txtVersion.Text + ".", true);
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    private void ReloadReleaseData() {
        SetPageMode("Edit");
        SetButtonVisibility("Edit");

        ddlReleaseID.Items.Clear();
        Session.Remove("ReleaseList");
        FillReleaseDropdown();
        ddlReleaseID.SelectedIndex = 0;
        //GetSelectedReleaseData();
    }

    /// <summary>
    /// Save the new item to the database, then reload the Release Dropdown data and select the newly-added item.
    /// </summary>
    /// <returns>Returns "true" on success, "false" on failure or invalid data.</returns>
    private bool AddReleaseLog() {
        if (ValidateData("New")) {
            // Save the new item to the database, then reload the Release Dropdown data and select the newly-added item.
            release.AddReleaseLog(ddlModelID.Text, txtVersion.Text, DateTime.Parse(txtReleaseDate.Text), DateTime.Now, ddlReleaseType.Text, ddlVisibility.Text, ddlDistribution.Text, Convert.ToInt32(txtReleaseQuantity.Text), Convert.ToInt32(txtMinimumOnHand.Text), ddlStatus.Text, txtStartSerialNumber.Text, txtEndSerialNumber.Text, txtTechnicalReference.Text, txtComments.Text, txtCompatibility.Text, txtNotes.Text, txtCreatedBy.Text, DateTime.Now, string.Empty, DateTime.Now, ddlUserGuide.Text);
            ReloadReleaseData();
            ddlReleaseID.SelectedValue = ddlModelID.Text + " V" + txtVersion.Text;
            GetSelectedReleaseData();
            SetMessage("Release created successfully.");
            return true;
        }
        return false;   // Data is not valid
    }

    private void UpdateRelease() {
        if (ValidateData("Edit")) {
            string modelID, version, notes;
            DateTime releaseDate = DateTime.Parse(txtReleaseDate.Text);

            if ((Session["isupdate"] != null) && (Session["isupdated"].ToString() == "true")) {
                notes = Session["txtMinimumOnHand"].ToString() + Session["txtReleaseQuantity"].ToString() + Session["txtStartSerialNumber"].ToString() + Session["txtEndSerialNumber"].ToString() + Session["txtTechnicalReference"].ToString() + "updated by " + Session["userId"].ToString() + " on " + System.DateTime.Now.ToString();
            } else {
                notes = txtNotes.Text;
            }
            ConvertRelease(ddlReleaseID.SelectedValue, out modelID, out version);
            //SetMessage(String.Format("Updating Release with the following data: modelID {0}, version {1}, releaseDate {2}, ddlReleaseType {3}, ddlVisibility {4}, ddlDistribution {5}, txtReleaseQuantity {6}, txtMinimumOnHand {7}, ddlStatus {8}, txtStartSerialNumber {9}, txtEndSerialNumber {10}, txtTechnicalReference {11}, txtComments {12}, txtCompatibility {13}, notes {14}"
            //, modelID, version, releaseDate, ddlReleaseType.Text, ddlVisibility.Text, ddlDistribution.Text, txtReleaseQuantity.Text, txtMinimumOnHand.Text, ddlStatus.Text, txtStartSerialNumber.Text, txtEndSerialNumber.Text, txtTechnicalReference.Text, txtComments.Text, txtCompatibility.Text, notes));
            release.UpdateReleaseLog(modelID, version, releaseDate, DateTime.Now, ddlReleaseType.Text, ddlVisibility.Text, ddlDistribution.Text, Convert.ToInt32(txtReleaseQuantity.Text), Convert.ToInt32(txtMinimumOnHand.Text), ddlStatus.Text, txtStartSerialNumber.Text, txtEndSerialNumber.Text, txtTechnicalReference.Text, txtComments.Text, txtCompatibility.Text, notes, string.Empty, DateTime.Now, Session["userId"].ToString(), DateTime.Now, ddlUserGuide.Text);
            //UpdateVersionInfo();
            //FillUserGuideDropdown();
            GetSelectedReleaseData();
            //SetMessage("Release updated successfully.");
        }
    }

    private bool IsNotesUpdated() {
        if (!Session["ExistingNotes"].ToString().Contains(txtNotes.Text)) {
            return false;
        } else {
            return true;
        }
    }

    /// <summary> Sets a non-error user message using the colour green. </summary>
    private void SetMessage(string message) { SetMessage(message, false); }

    /// <summary> Sets a user message with the colour Red when isError is True, or Green if false. </summary>
    private void SetMessage(string message, bool isError) {
        lblMessage.Text = message;
        if (isError)
            lblMessage.ForeColor = Color.Red;
        else
            lblMessage.ForeColor = Color.Green;
    }
    #endregion

    #region Data Methods
    private void FillReleaseDropdown() {
        if (ddlReleaseID.Items.Count > 0)
            return;
        ArrayList releaseList = new ArrayList();
        if (Session["ReleaseList"] != null)
            releaseList = (ArrayList)Session["ReleaseList"];
        else {
            DataSet dsRelease = release.GetReleaseIdVersions();
            if ((dsRelease != null) && (dsRelease.Tables[0].Rows.Count > 0)) {
                releaseList.Add(SELECT_ONE);
                foreach (DataRow row in dsRelease.Tables[0].Rows) {
                    releaseList.Add(row["MODEL_ID"].ToString() + " V" + row["VERSION"].ToString());
                }
                Session["ReleaseList"] = releaseList;
            }
        }
        ddlReleaseID.DataSource = releaseList;
        ddlReleaseID.DataBind();
    }

    private void FillRelatedReleaseDropdown() {
        if (ddlAddRelated.Items.Count > 0)
            return;
        ArrayList releaseList = new ArrayList();
        if (Session["ReleaseList"] != null)
            releaseList = (ArrayList)Session["ReleaseList"];
        else {
            DataSet dsRelease = release.GetReleaseIdVersions();
            if ((dsRelease != null) && (dsRelease.Tables[0].Rows.Count > 0)) {
                releaseList.Add(SELECT_ONE);
                foreach (DataRow row in dsRelease.Tables[0].Rows) {
                    releaseList.Add(row["MODEL_ID"].ToString() + " V" + row["VERSION"].ToString());
                }
                Session["ReleaseList"] = releaseList;
            }
        }
        ddlAddRelated.DataSource = releaseList;
        ddlAddRelated.DataBind();
    }

    private void FillModelDropdown() {
        if (ddlModelID.Items.Count > 0)
            return;
        ArrayList modelList = new ArrayList();
        if (Session["ModelList"] != null)
            modelList = (ArrayList)Session["ModelList"];
        else {
            DataSet dsmodelIds = model.GetModelIds();
            if ((dsmodelIds != null) && (dsmodelIds.Tables[0].Rows.Count > 0)) {
                modelList.Add(SELECT_ONE);
                foreach (DataRow row in dsmodelIds.Tables[0].Rows) {
                    modelList.Add(row["MODEL_ID"].ToString());
                }
                Session["ModelList"] = modelList;
            }
        }
        ddlModelID.DataSource = modelList;
        ddlModelID.DataBind();
    }

    private void FillReleaseStatusDropdown() {
        if (ddlStatus.Items.Count > 0)
            return;
        ArrayList list = new ArrayList();
        list.Add(SELECT_ONE);
        list.AddRange(softwareConfigMgr.ReleaseStatus());
        ddlStatus.DataSource = list;
        ddlStatus.DataBind();
    }

    private void FillReleaseTypeDropdown() {
        if (ddlReleaseType.Items.Count > 0)
            return;
        ArrayList list = new ArrayList();
        list.Add(SELECT_ONE);
        list.AddRange(softwareConfigMgr.ReleaseType());
        ddlReleaseType.DataSource = list;
        ddlReleaseType.DataBind();
    }

    private void FillVisibilityDropdown() {
        if (ddlVisibility.Items.Count > 0)
            return;
        ArrayList list = new ArrayList();
        list.Add(SELECT_ONE);
        list.AddRange(softwareConfigMgr.ReleaseVisibility());
        ddlVisibility.DataSource = list;
        ddlVisibility.DataBind();
    }

    private void FillDistributionDropdown() {
        if (ddlDistribution.Items.Count > 0)
            return;
        ArrayList list = new ArrayList();
        list.Add(SELECT_ONE);
        list.AddRange(softwareConfigMgr.ReleaseDistribution());
        ddlDistribution.DataSource = list;
        ddlDistribution.DataBind();
    }

    private void FillUserGuideDropdown() {
        ArrayList list = new ArrayList();
        //list.Add(SELECT_ONE);
        list.Add("");
        DataSet ds = release.GetFTPReleaseLogData();
        if (ds != null) {
            foreach (DataRow row in ds.Tables[0].Rows) {
                if (!String.IsNullOrEmpty(row["USER_MANUAL_FILE"].ToString())) {
                    list.Add(row["USER_MANUAL_FILE"].ToString());
                }
            }
        }
        ddlUserGuide.DataSource = list;
        ddlUserGuide.DataBind();
    }

    private void FillRelatedKitGridview() {
        string modelID, version;
        ConvertRelease(ddlReleaseID.SelectedValue, out modelID, out version);
        DataSet ds = release.GetKitInfoForReleaseLog(modelID, version);
        gvRelatedKits.Visible = (ds.Tables[0].Rows.Count > 0);
        gvRelatedKits.DataSource = ds;
        gvRelatedKits.DataBind();
    }

    private void FillRelatedReleaseGridview() {
        string modelID, version;
        ConvertRelease(ddlReleaseID.SelectedValue, out modelID, out version);
        DataSet ds = release.GetChildReleaseID(modelID, version);
        gvRelatedReleases.Visible = (ds.Tables[0].Rows.Count > 0);
        gvRelatedReleases.DataSource = ds;
        gvRelatedReleases.DataBind();
    }

    private void FillRelatedKitDetailGridview() {
        string modelID, version;
        ConvertRelease(ddlReleaseID.SelectedValue, out modelID, out version);
        DataSet ds = release.GetKitItemInfoByReleaseLog(modelID, version);
        gvRelatedKitDetails.Visible = (ds.Tables[0].Rows.Count > 0);
        gvRelatedKitDetails.DataSource = ds;
        gvRelatedKitDetails.DataBind();
    }

    private void FillVersionTextboxFromModel(DataSet ds) {
        if ((ds != null) && (ds.Tables[0].Rows.Count > 0)) {
            // Get the most recent Version number
            DataRow row = ds.Tables[0].Rows[ds.Tables[0].Rows.Count - 1];
            txtVersion.Text = row["VERSION"].ToString();
        } else {
            // This Model has no Releases yet, so assume Version 1.0
            txtVersion.Text = "1.0";
        }
    }

    private void FillAttachKitDropdown() {
        if (ddlAddKit.Items.Count > 0)
            return;
        ArrayList kitList = new ArrayList();
        if (Session["KitList"] != null)
            kitList = (ArrayList)Session["KitList"];
        else {
            KitInfo kit = new KitInfo();
            DataSet dsKitPartNumbers = kit.GetKitInfoForAttachment();
            if ((dsKitPartNumbers != null) && (dsKitPartNumbers.Tables[0].Rows.Count > 0)) {
                kitList.Add(SELECT_ONE);
                foreach (DataRow row in dsKitPartNumbers.Tables[0].Rows) {
                    kitList.Add(row["KIT_PART_NUMBER"].ToString() + "-" + row["KIT_DESCRIPTION"].ToString());
                }
                Session["KitList"] = kitList;
            }
        }
        ddlAddKit.DataSource = kitList;
        ddlAddKit.DataBind();
    }
    #endregion
}

