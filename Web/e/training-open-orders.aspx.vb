Imports Sony.US.ServicesPLUS.Data
Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Process
Imports System.Drawing
Imports System.Data
Imports ServicesPlusException


Namespace SIAMAdmin

    Partial Class training_open_orders
        Inherits UtilityClass

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub


        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            MyBase.IsProtectedPage(True, True)
            InitializeComponent()
            ApplyDataGridStyle()
        End Sub

#End Region

        Private eDGHeader As DataGridItem
        'Dim objUtilties As Utilities = New Utilities

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            If Not IsPostBack Then
                Try
                    writeScriptsToPage()
                    Dim cdm As New CourseDataManager
                    Dim dsReturn As DataSet = New DataSet()
                    Dim crs As CourseReservation() = cdm.GetOpenTrainingReservation(True, True, "StartDate", 1, dsReturn)
                    ViewState.Add("sortKey", "StartDate")
                    ViewState("sortOrder") = "1"
                    ViewState("sortColumn") = "1"
                    ViewState("ActiveOrder") = "1"
                    Session.Add("training-open-orders", dsReturn)
                    Me.orderDataGrid.DataSource = crs
                    Me.orderDataGrid.DataBind()
                    AddSortImage()
                    lblActiveOrder.Text = "Open orders with active class. "
                    lnkAllOrder.Text = "Get all open orders"


                    ErrorLabel.Visible = True


                    ErrorLabel.Visible = True
                Catch ex As Exception
                    ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                    ErrorLabel.Visible = True
                End Try
            End If
        End Sub

        Sub orderDataGrid_Paging(ByVal sender As Object, ByVal e As DataGridPageChangedEventArgs)
            Try
                orderDataGrid.CurrentPageIndex = e.NewPageIndex
                Dim cdm As New CourseDataManager
                Dim dsReturn As DataSet = CType(Session("training-open-orders"), DataSet)
                Me.orderDataGrid.DataSource = cdm.SortCourseReservation(dsReturn, True, ViewState.Item("sortKey").ToString(), Convert.ToInt16(ViewState.Item("sortOrder").ToString()))
                Me.orderDataGrid.DataBind()
                AddSortImage()
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                ErrorLabel.Visible = True
            End Try
        End Sub

        Private Sub orderDataGrid_ItemCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles orderDataGrid.ItemCreated
            Try
                If e.Item.ItemType <> ListItemType.Header Then
                    e.Item.ApplyStyle(GetStyle(e.Item.ItemType))
                ElseIf e.Item.ItemType = ListItemType.Header Then
                    eDGHeader = e.Item
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                ErrorLabel.Visible = True
            End Try
        End Sub
        '-- helper scripts --

        Private Sub writeScriptsToPage()
            '-- this script will make sure that this page is only accessed via the iform tag in default.aspx -         
            RegisterClientScriptBlock("redirect", "<script language=""javascript"">if ( parent.frames.length === 0 ){window.location.replace(""" + MyBase.EntryPointPage + """)}</script>")

            '-- this will clear the status bar on the browser 
            RegisterStartupScript("ClearStatus", "<script language=""javascript"">window.parent.status = ""Done."";</script>")

            '-- this script is used to confirm that a user wants to remove an application -
            RegisterStartupScript("RemoveApplication", "<script language=""javascript"">function DeleteOp(itemId){var ok = window.confirm(""Are you sure ?"");if (ok === true){document.location.href=""" + Page.Request.Url.Segments.GetValue(1).ToString() + "?Remove="" + itemId;}}</script>")
        End Sub

        Private Sub ApplyDataGridStyle()
            'orderDataGrid.ApplyStyle(GetStyle(520))
            orderDataGrid.AllowPaging = True
            orderDataGrid.PageSize = 10
            orderDataGrid.AutoGenerateColumns = False
            'orderDataGrid.AllowPaging = False
            orderDataGrid.GridLines = GridLines.Horizontal
            orderDataGrid.AllowSorting = True
            orderDataGrid.CellPadding = 3
            orderDataGrid.PagerStyle.Mode = PagerMode.NumericPages
            orderDataGrid.HeaderStyle.Height = Unit.Pixel(30)
            orderDataGrid.PagerStyle.Height = Unit.Pixel(10)
            orderDataGrid.HeaderStyle.Font.Size = FontUnit.Point(10)
            orderDataGrid.HeaderStyle.ForeColor = Color.GhostWhite
            orderDataGrid.HeaderStyle.BackColor = Color.FromKnownColor(KnownColor.ControlDarkDark)
            orderDataGrid.ItemStyle.Font.Size = FontUnit.Point(10)
            orderDataGrid.ItemStyle.ForeColor = Color.GhostWhite
            orderDataGrid.AlternatingItemStyle.Font.Size = FontUnit.Point(10)
            orderDataGrid.AlternatingItemStyle.ForeColor = Color.GhostWhite
        End Sub

        Protected Sub btnSearchOrder_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearchOrder.Click
            Try
                Dim dsReturn As DataSet = CType(Session("training-open-orders"), DataSet)
                If txtOrderNumber.Text.Trim() = String.Empty Then Return
                Dim dvSelectedOrder As DataView = New DataView(dsReturn.Tables(0))
                dvSelectedOrder.RowFilter = "RESERVATIONNUMBER = '" + txtOrderNumber.Text.Trim() + "'"
                If dvSelectedOrder.Count = 0 Then
                    If ViewState("ActiveOrder").ToString() = "1" Then
                        lblOrderSearch.Text = "Order number " + txtOrderNumber.Text.Trim() + " does not exist in active order list. Please click on ""Get All Open Orders"" link and try."
                    Else
                        lblOrderSearch.Text = "Order number " + txtOrderNumber.Text.Trim() + " does not exist."
                    End If
                Else
                    orderDataGrid.CurrentPageIndex = 0
                    Dim cdm As New CourseDataManager
                    Dim dsNewDataset As DataSet = New DataSet()
                    dsNewDataset.Tables.Add(dvSelectedOrder.ToTable())
                    Session("training-open-orders") = dsNewDataset
                    Me.orderDataGrid.DataSource = cdm.SortCourseReservation(dsNewDataset, True, ViewState.Item("sortKey").ToString(), Convert.ToInt16(ViewState.Item("sortOrder").ToString()))
                    Me.orderDataGrid.DataBind()
                    AddSortImage()
                End If
                'Dim crs As CourseReservation() = Session.Item("training-open-orders")
                'Dim bfound As Boolean = False
                'For Each cr As CourseReservation In crs
                '    If cr.ReservationNumber.ToUpper() = txtOrderNumber.Text.Trim().ToUpper() Then
                '        Response.Redirect("training-order.aspx?OrderNumber=" + cr.ReservationNumber)
                '        bfound = True
                '    End If
                'Next   
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                ErrorLabel.Visible = True
            End Try
        End Sub

        Protected Sub lnkAllOrder_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAllOrder.Click
            Try
                lblActiveOrder.Visible = False
                lnkAllOrder.Visible = False
                ViewState("ActiveOrder") = "0"
                Dim cdm As New CourseDataManager
                Dim dsReturn As DataSet = New DataSet()
                Dim crs As CourseReservation() = cdm.GetOpenTrainingReservation(False, True, ViewState.Item("sortKey").ToString(), Convert.ToInt16(ViewState.Item("sortOrder").ToString()), dsReturn)
                Session.Remove("training-open-orders")
                Session.Add("training-open-orders", dsReturn)
                Me.orderDataGrid.DataSource = crs
                Me.orderDataGrid.DataBind()
                AddSortImage()
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                ErrorLabel.Visible = True
            End Try
        End Sub

        Protected Sub orderDataGrid_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles orderDataGrid.SortCommand
            Try
                If e.SortExpression = ViewState.Item("sortKey").ToString() Then
                    If ViewState.Item("sortOrder").ToString() = "1" Then
                        ViewState("sortOrder") = "0"
                    Else
                        ViewState("sortOrder") = "1"
                    End If
                Else
                    ViewState("sortKey") = e.SortExpression
                    ViewState("sortOrder") = "1"
                End If
                If e.SortExpression = "ReservationNumber" Then
                    ViewState("sortColumn") = 0
                ElseIf e.SortExpression = "StartDate" Then
                    ViewState("sortColumn") = 1
                ElseIf e.SortExpression = "UpdateDate" Then
                    ViewState("sortColumn") = 2
                ElseIf e.SortExpression = "Title" Then
                    ViewState("sortColumn") = 3
                ElseIf e.SortExpression = "FirstName" Then
                    ViewState("sortColumn") = 4
                ElseIf e.SortExpression = "LastName" Then
                    ViewState("sortColumn") = 5
                End If
                orderDataGrid.CurrentPageIndex = 0
                Dim cdm As New CourseDataManager
                Dim dsReturn As DataSet = CType(Session("training-open-orders"), DataSet)
                Me.orderDataGrid.DataSource = cdm.SortCourseReservation(dsReturn, True, ViewState.Item("sortKey").ToString(), Convert.ToInt16(ViewState.Item("sortOrder").ToString()))
                Me.orderDataGrid.DataBind()
                AddSortImage()
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                ErrorLabel.Visible = True
            End Try
        End Sub

        Private Sub AddSortImage()
            Dim imgSortImage As System.Web.UI.WebControls.Image = New System.Web.UI.WebControls.Image()
            If ViewState("sortOrder").ToString() = "1" Then
                imgSortImage.ImageUrl = "~/images/arrow_up.gif"
            Else
                imgSortImage.ImageUrl = "~/images/arrow_down.gif"
            End If
            eDGHeader.Cells(Convert.ToInt16(ViewState("sortColumn").ToString())).Controls.Add(imgSortImage)
        End Sub

    End Class

End Namespace
