Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Data
Imports Sony.US.ServicesPLUS.Process
Imports ServicesPlusException
Imports Sony.US.AuditLog

Namespace SIAMAdmin

    Partial Class training_model_edit
        Inherits UtilityClass
        'Dim objUtilties As Utilities = New Utilities

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub


        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            MyBase.IsProtectedPage(True, True)
            InitializeComponent()
        End Sub

#End Region
        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            txtModel.Visible = True
            lblModelNumber.Visible = False
            LabelCodeStar.Visible = True
            If Not IsPostBack Then
                Try
                    Me.drpCourseNumber.Visible = True
                    lblSelectedCourse.Visible = False
                    btnUpdate.Text = "Update"

                    Dim cdm As New CourseDataManager
                    Dim css As Course() = cdm.GetCourse()
                    drpCourseNumber.DataTextField = "Number"
                    drpCourseNumber.DataValueField = "Number"
                    drpCourseNumber.DataSource = css
                    drpCourseNumber.DataBind()

                    If (Not Request.QueryString("LineNo") Is Nothing) And (Not Session.Item("training-model-list") Is Nothing) Then
                        Dim modelSelected As CourseModel
                        Dim itemNumber As Integer = -1
                        itemNumber = Convert.ToInt16(Request.QueryString("LineNo"))
                        modelSelected = CType(Session.Item("training-model-list"), Array)(itemNumber)
                        Me.txtModel.Text = modelSelected.Model
                        If Not drpCourseNumber.Items.FindByText(modelSelected.CourseNumber) Is Nothing Then
                            drpCourseNumber.Items.FindByText(modelSelected.CourseNumber).Selected = True
                        Else
                            'Me.drpCourseNumber.Text = ""
                            ErrorLabel.Text = "Course number does not exist in the course table. Click DELETE to delete this invalid model/course relationship record."
                            Me.drpCourseNumber.Visible = False
                            lblSelectedCourse.Visible = True
                            lblSelectedCourse.Text = modelSelected.CourseNumber
                            btnUpdate.Text = "Delete"
                            txtModel.Visible = False
                            lblModelNumber.Visible = True
                            lblModelNumber.Text = txtModel.Text
                            LabelCodeStar.Visible = False
                            'Me.labelCourseNumber.CssClass = "redAsterick"
                            Me.LabelCourseNumberStar.Text = "<span class=""redAsterick"">*</span>"
                            LabelCourseNumberStar.Visible = False
                            Return
                        End If
                    End If





                Catch ex As Exception
                    ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                End Try
            End If
        End Sub
        Private Sub btnUpdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpdate.Click

            If Not ValidateForm() Then
                ErrorLabel.Text = "Error entering model info:" + ErrorLabel.Text
                Return
            End If
            Dim bUpdate As Boolean = False
            Dim bDelete As Boolean = False
            Dim modelSelected As CourseModel
            Dim oldModel As String = ""
            Dim objPCILogger As New PCILogger() '6524
            Try
                '6524 start
                objPCILogger.EventOriginApplication = "ServicesPLUS"
                objPCILogger.EventOriginApplicationLocation = Me.Page.GetType().Name
                objPCILogger.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
                objPCILogger.OperationalUser = Environment.UserName
                objPCILogger.EventDateTime = DateTime.Now.ToLongTimeString()
                objPCILogger.HTTPRequestObjectValues = objPCILogger.GetRequestContextValue()
                objPCILogger.IndicationSuccessFailure = "Success"
                objPCILogger.EventOriginMethod = "btnUpdate_Click"
                '6524 end

                If (Not Request.QueryString("LineNo") Is Nothing) Then 'Update
                    If btnUpdate.Text = "Delete" Then
                        bDelete = True
                        txtModel.Visible = False
                        lblModelNumber.Visible = True
                        lblModelNumber.Text = txtModel.Text
                        LabelCodeStar.Visible = False
                    Else
                        bUpdate = True
                    End If
                    Dim itemNumber As Integer = -1
                    itemNumber = Convert.ToInt16(Request.QueryString("LineNo"))
                    modelSelected = CType(Session.Item("training-model-list"), Array)(itemNumber)
                    oldModel = modelSelected.Model
                Else
                    modelSelected = New CourseModel
                End If

                modelSelected.Model = Me.txtModel.Text
                If drpCourseNumber.Visible = True Then modelSelected.CourseNumber = Me.drpCourseNumber.Text
                Dim cm As New CourseManager
                If bDelete Then
                    objPCILogger.Message = "DeleteCourseModel Success."
                    objPCILogger.EventType = EventType.Delete
                    modelSelected.CourseNumber = lblSelectedCourse.Text
                    cm.DeleteCourseModel(oldModel, modelSelected)
                    ErrorLabel.Text = "Model Course relation deleted successfully"
                    btnUpdate.Visible = False
                ElseIf bUpdate Then
                    objPCILogger.Message = "UpdateCourseModel Success."
                    objPCILogger.EventType = EventType.Update
                    cm.UpdateCourseModel(oldModel, modelSelected)
                    ErrorLabel.Text = "Updated Successfully"
                Else
                    objPCILogger.Message = "AddCourseModel Success."
                    objPCILogger.EventType = EventType.Add
                    cm.AddCourseModel(modelSelected)
                    ErrorLabel.Text = "Added Successfully"
                End If




            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                objPCILogger.IndicationSuccessFailure = "Failure" '6524
                objPCILogger.Message = "Add/Update/Delete CourseModel Failed. " & ex.Message.ToString() '6524
            Finally
                objPCILogger.PushLogToMSMQ() '6524
            End Try
        End Sub
        Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
            Response.Redirect("training-model-list.aspx")
        End Sub
        Private Function ValidateForm() As Boolean
            Dim bValid = True

            ErrorLabel.Text = ""

            Me.labelModel.CssClass = "Body"
            Me.labelCourseNumber.CssClass = "Body"

            Me.LabelCodeStar.Text = ""
            Me.LabelCourseNumberStar.Text = ""

            If Me.drpCourseNumber.Text = "" Then
                Me.labelCourseNumber.CssClass = "redAsterick"
                Me.LabelCourseNumberStar.Text = "<span class=""redAsterick"">*</span>"
                bValid = False
            End If
            If Me.txtModel.Text = "" Then
                Me.labelModel.CssClass = "redAsterick"
                Me.LabelCodeStar.Text = "<span class=""redAsterick"">*</span>"
                bValid = False
            End If
            Return bValid
        End Function
    End Class

End Namespace
