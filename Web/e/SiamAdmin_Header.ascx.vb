Namespace SIAMAdmin

Partial Class SiamAdmin_Header
    Inherits System.Web.UI.UserControl

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub




    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
            'SiamAdminLogoutImg.Visible = False
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            'Put user code to initialize the page here
            '***************Changes done for Bug# 192 Starts here***************
            '***************Kannapiran S*****************
            Dim sbEnv As StringBuilder = New StringBuilder(String.Empty)
            Dim strConnString As String = Sony.US.SIAMUtilities.ConfigurationData.Environment.DataBase.ConnectionString
            Dim strCurrentEnvironment As String = Sony.US.SIAMUtilities.ConfigurationData.GeneralSettings.Environment.ToString().Trim

            ' strConnString has Data Source=*******;User ID=******;Password=*****;
            ' to get the Data Source portion, following action has done

            Dim strSplit() As String = Split(strConnString, ";")
            Dim strDBPortion() As String = Split(strSplit(0), "=")
            Dim strDBValue As String = strDBPortion(1)
            lblBuildNo.Text = "( Build No.: " + Application("Build").ToString().Trim() + " )"

            If (strDBValue.Trim() <> "WCSPRA") And (strCurrentEnvironment.Trim() <> "Production") Then
                sbEnv.Length = 0
                lblEnv.Visible = True
                sbEnv.Append("<span Font-Size=""14"" Color=""#CA002E""> THIS SITE IS FOR TESTING ONLY.</span>")
                sbEnv.Append("<span class=""bodyCopySM"">( App. Env: " + strCurrentEnvironment.Trim() + vbTab + vbTab)
                'Modified by Sunil on 21-04-10 for Bug 2486
                'sbEnv.Append("DB: " + strDBValue.Substring(3, 1) + " )</span>")
                sbEnv.Append("DB: " + strDBValue.ToString().ToLower() + " )</span>")
                lblEnv.Text = sbEnv.ToString()
            Else
                sbEnv.Length = 0
                lblEnv.Text = String.Empty
                lblEnv.Visible = False
            End If

            '***************Kannapiran S*****************
            '***************Changes done for Bug# 192 Ends Here***************
    End Sub

    Private Sub Page_PreRender(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.PreRender
        'Put user code to initialize the page here
        lblDateTime.Text = System.DateTime.Now.ToLongDateString()
    End Sub



End Class

End Namespace
