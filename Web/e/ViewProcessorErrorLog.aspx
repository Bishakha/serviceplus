<%@ Reference Page="~/e/Utility.aspx" %>

<%@ Page Language="vb" AutoEventWireup="false" Inherits="SIAMAdmin.ViewProcessorErrorLog"
    CodeFile="ViewProcessorErrorLog.aspx.vb" CodeFileBaseClass="SIAMAdmin.UtilityClass" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>Maintain Shipping Method</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <%--<link href="includes/style.css" type="text/css" rel="stylesheet">--%>
    <link href="includes/ServicesPLUS_style.css" rel="stylesheet" type="text/css" />

    <script language="javascript">
        function ConfirmDelete() {
            window.event.returnValue = confirm("This activity will delete selected mapping...");
        }
        window.parent.status = "Maintain Shipping Method is open.";
    </script>

    <style type="text/css">
        Body
        {
            filter: chroma(color=#FFFFFF);
            scrollbar-face-color: #c9d5e6;
            scrollbar-shadow-color: #632984;
            scrollbar-highlight-color: #632984;
            scrollbar-3dlight-color: #130919;
            scrollbar-darkshadow-color: #130919;
            scrollbar-track-color: #130919;
            scrollbar-arrow-color: black;
        }
    </style>
</head>
<body>
    <form id="Form1" method="post" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <table width="100%" align="center" border="0">
        <tr>
            <td align="center" height="20">
                <asp:Label ID="lblHeader" CssClass="headerTitle" runat="server" Text="Batch Processor/ EMail Test Tool Error Log"></asp:Label>
            </td>
        </tr>
        <tr>
            <td height="15">
            </td>
        </tr>
        <tr>
            <td>
                <table border="0" width="100%" align="center">
                    <tr height="20">
                        <td align="left">
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
                                    <asp:Label ID="ErrorLabel" runat="server" CssClass="redAsterick" EnableViewState="False"></asp:Label>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="btnSearch" EventName="Click" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:UpdatePanel ID="FieldUpdatePanel" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <div id="div1" style="border-right: #c9d5e6 thin solid; width: 100%; border-top: #c9d5e6 thin solid;
                                        border-left: #c9d5e6 thin solid; border-bottom: #c9d5e6 thin solid">
                                        <table width="100%" cellspacing="0">
                                            <tr bgcolor="#c9d5e6" height="20px">
                                                <td width="10%">
                                                    <asp:Label ID="lblTrackingURL" CssClass="bodyCopyBold" runat="server">Process Name</asp:Label>
                                                </td>
                                                <td width="40%">
                                                    <SPS:SPSDropDownList ID="drpProcessName" runat="server" CssClass="bodyCopy" Width="98%">
                                                    </SPS:SPSDropDownList>
                                                </td>
                                                <td width="10%">
                                                    <asp:Label ID="lblDate" runat="server" CssClass="bodyCopyBold">Date</asp:Label>
                                                </td>
                                                <td width="40%" valign="middle">
                                                    <SPS:SPSTextBox ID="txtDate" runat="server" Width="100px" CssClass="bodyCopy" Text=""
                                                        MaxLength="100"></SPS:SPSTextBox>
                                                    <span class="redAsterick">* </span><span class="bodyCopyBold">(mm/dd/yyyy)</span>
                                                </td>
                                            </tr>
                                            <tr height="20px">
                                                <td colspan="4">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4" align="left" width="25%">
                                                    <asp:Button ID="btnSearch" runat="server" CssClass="bodyCopyBold" Text="Search" Width="70px" />
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="dgShippingMethod" EventName="ItemCommand" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:UpdatePanel ID="GridUpdatePanel" runat="server" ChildrenAsTriggers="true">
                                <ContentTemplate>
                                    <asp:DataGrid ID="dgShippingMethod" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                        PageSize="8" Width="100%">
                                        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                        <EditItemStyle BackColor="#999999" />
                                        <SelectedItemStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                        <PagerStyle BackColor="#284775" CssClass="BodyCopy" ForeColor="White" HorizontalAlign="Center"
                                            Mode="NumericPages" />
                                        <AlternatingItemStyle BackColor="White" ForeColor="#284775" CssClass="BodyCopy" />
                                        <ItemStyle BackColor="#F7F6F3" ForeColor="#333333" CssClass="BodyCopy" />
                                        <HeaderStyle BackColor="#284775" CssClass="TableHeader" ForeColor="White" />
                                        <Columns>
                                            <asp:BoundColumn DataField="SISBATCHPROCESSID" HeaderText="SISBATCHPROCESSID">
                                                <HeaderStyle Width="11%" />
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="PROCESSSTARTED" HeaderText="PROCESSSTARTED">
                                                <HeaderStyle Width="16%" />
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="PROCESSFINISHED" HeaderText="PROCESSFINISHED">
                                                <HeaderStyle Width="16%" HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="TOTALRECORDCOUNT" HeaderText="TOTALRECORD">
                                                <HeaderStyle Width="11%" HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="SUCCESSRECORDCOUNT" HeaderText="SUCCESS">
                                                <HeaderStyle Width="11%" HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="SKIPPEDRECORDCOUNT" HeaderText="SKIPPED">
                                                <HeaderStyle Width="11%" HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="ERRORRECORDCOUNT" HeaderText="ERROR">
                                                <HeaderStyle Width="11%" HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="WARNINGCOUNT" HeaderText="WARNING">
                                                <HeaderStyle Width="11%" HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="STATUSCODE" HeaderText="Status" Visible="True">
                                                <HeaderStyle Width="11%" HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:BoundColumn>
                                            <asp:TemplateColumn>
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkBtnEdit" runat="server" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "SISBATCHPROCESSID")%>'
                                                        CommandName="Show" CssClass="bodyCopySM" Text="Show">
                                                    </asp:LinkButton>
                                                    </tr><tr width="100%">
                                                        <td width="100%" align="center" colspan="10">
                                                            <table width="100%" align="center" runat="server" id="innertable" visible="false">
                                                                <tr height="2px">
                                                                    <td colspan="3">
                                                                    </td>
                                                                </tr>
                                                                <tr valign="middle">
                                                                    <td colspan="1" width="2px">
                                                                        &nbsp;
                                                                    </td>
                                                                    <td colspan="1" width="100%">
                                                                        <asp:DataGrid ID="dgInnerSAPNAME" runat="server" Visible="false" AutoGenerateColumns="False"
                                                                            Width="100%">
                                                                            <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                                                            <EditItemStyle BackColor="#999999" />
                                                                            <SelectedItemStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                                                            <PagerStyle BackColor="#284775" CssClass="bodyCopy" ForeColor="White" HorizontalAlign="Center"
                                                                                Mode="NumericPages" />
                                                                            <AlternatingItemStyle BackColor="White" ForeColor="#284775" CssClass="bodyCopy" />
                                                                            <ItemStyle BackColor="#F7F6F3" ForeColor="#333333" CssClass="bodyCopy" />
                                                                            <HeaderStyle BackColor="#284775" CssClass="tableHeader" ForeColor="White" />
                                                                            <Columns>
                                                                                <asp:BoundColumn DataField="RECORDID" HeaderText="RECORDID">
                                                                                    <HeaderStyle Width="10%" />
                                                                                </asp:BoundColumn>
                                                                                <asp:BoundColumn DataField="ERRORMESSAGE" HeaderText="ERRORMESSAGE">
                                                                                    <HeaderStyle Width="65%" />
                                                                                </asp:BoundColumn>
                                                                                <asp:BoundColumn DataField="UPDATEDATE" HeaderText="UPDATEDATE">
                                                                                    <HeaderStyle Width="15%" />
                                                                                </asp:BoundColumn>
                                                                                <asp:BoundColumn DataField="SECONDARYID" HeaderText="SECONDARYID">
                                                                                    <HeaderStyle Width="10%" />
                                                                                </asp:BoundColumn>
                                                                            </Columns>
                                                                        </asp:DataGrid>
                                                                    </td>
                                                                    <td colspan="1" width="2px">
                                                                        &nbsp;
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="3" width="2px">
                                                                        &nbsp;
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <itemstyle cssclass="bodycopy" horizontalalign="Center" verticalalign="Middle" />
                                                </ItemTemplate>
                                                <HeaderStyle Width="1%" />
                                            </asp:TemplateColumn>
                                        </Columns>
                                    </asp:DataGrid>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="btnSearch" EventName="Click" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <input type="hidden" id="hdnLastOpr" runat="server" />
    </form>
</body>
</html>
