<%@ Reference Page="~/e/Utility.aspx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SIAMAdmin.ViewOrderDetail" CodeFile="ViewOrderDetail.aspx.vb" CodeFileBaseClass="SIAMAdmin.UtilityClass" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>ViewOrderDetail</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<link rel="stylesheet" type="text/css" href="includes/style.css">
		<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<table width="500" align="center" border="0">
				<tr>
					<td colSpan="5" height="50" align="center"><asp:Label id="ErrorLabel" runat="server" CssClass="body" ForeColor="Red"></asp:Label></td>
				</tr>
				<tr bgcolor="silver">
					<td colSpan="5" align="center" bgcolor="silver"><span class="BodyHead">View Order 
							Detail</span>
					</td>
				</tr>
				<tr bgcolor="silver">
					<td colSpan="5">&nbsp;</td>
				</tr>
				<tr bgcolor="silver">
					<td>&nbsp;</td>
					<td colspan="4"><span class="Body">Your Order Number is 123456789</span></td>
				</tr>
				<tr bgcolor="silver">
					<td>&nbsp;</td>
					<td colspan="4"><span class="Body">Paid with CC or Paid with Account</span></td>
				</tr>
				<tr bgcolor="silver">
					<td>&nbsp;</td>
					<td colspan="4"><span class="Body">PO#</span></td>
				</tr>
				<tr bgcolor="silver">
					<td>&nbsp;</td>
					<%--<td width="300" colspan="3"><span class="Body">Billing Info:</span></td>
					<td><span class="Body">Shipping Info:</span></td>--%>
					<%--8060--%>
					<td width="300" colspan="3"><span class="Body">Billing Information:</span></td>
					<td><span class="Body">Shipping Information:</span></td>
				</tr>
				<tr bgcolor="silver">
					<td>&nbsp;</td>
					<td width="300" colspan="3"><span class="Body">Billing Name</span></td>
					<td><span class="Body">Shipping Name</span></td>
				</tr>
				<tr bgcolor="silver">
					<td>&nbsp;</td>
					<td width="300" colspan="3"><span class="Body">Billing Address</span></td>
					<td><span class="Body">Shipping Address</span></td>
				</tr>
				<tr bgcolor="silver">
					<td>&nbsp;</td>
					<td width="300" colspan="3"><span class="Body">City, State ZIP</span></td>
					<td><span class="Body">City, State ZIP</span></td>
				</tr>
				<tr bgcolor="silver">
					<td colSpan="5">&nbsp;</td>
				</tr>
				<tr>
					<td colSpan="5">&nbsp;</td>
				</tr>
				<tr>
					<td colspan="5">
						<table border="0" cellspacing="0">
							<tr>
								<td>
									<asp:Table id="OrderDetailsTable" runat="server"></asp:Table></td>
							</tr>
							<tr>
								<td height="10"></td>
							</tr>
							<tr>
								<td>
									<asp:Table id="InvoicesTable" runat="server"></asp:Table></td>
							</tr>
							
							<tr>
								<td colspan="10">&nbsp;
									<asp:DataGrid id="DataGrid1" runat="server">
										<Columns>
											<asp:TemplateColumn HeaderText="Item Number">
												<ItemTemplate>
													<asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ItemNumber").ToString() %>' ID="Label1">
													</asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Description">
												<ItemTemplate>
													<asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Description").ToString() %>' ID="Label2">
													</asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Qty">
												<ItemTemplate>
													<asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Qty").ToString() %>' ID="Label3">
													</asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Item Price">
												<ItemTemplate>
													<asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ItemPrice").ToString() %>' ID="Label4">
													</asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Total Price">
												<ItemTemplate>
													<asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TotalPrice").ToString() %>' ID="Label5">
													</asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
										</Columns>
									</asp:DataGrid>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
