Imports System.Web.UI
Imports Sony.US.ServicesPLUS.Process
Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Data
Imports System.Web.UI.WebControls
Imports System.Data
Imports Sony.US.AuditLog
Imports ServicesPlusException

Namespace SIAMAdmin

    Partial Class ModifyFunctionality
        Inherits UtilityClass
        'Dim objUtilties As Utilities = New Utilities

        Protected Sub Page_Init1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
            MyBase.IsProtectedPage(True, True)
        End Sub

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                If Not IsPostBack Then

                    If (Request.QueryString("Identity").Length = 0) Then
                        Response.Redirect("SearchFunctioanlity.aspx")
                    Else
                        txtFunctionalityId.Text = Request.QueryString("Identity")
                        txtFunctionalityName.Text = Request.QueryString("fName")
                        txtURLMapped.Text = Request.QueryString("URLMapped")
                        drpGroupName.SelectedIndex = -1
                        drpGroupName.Items.FindByValue(Request.QueryString("GroupID")).Selected = True
                        drpLive.SelectedIndex = -1
                        drpLive.Items.FindByValue(Request.QueryString("sitestatus")).Selected = True
                        If Request.QueryString("Active") = "Yes" Then
                            chkIsActive.Checked = True
                        Else
                            chkIsActive.Checked = False
                        End If
                    End If

                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Protected Sub btnUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdate.Click

            Dim oUsrRoleMgr As New UserRoleSecurityManager
            Dim iRetStatus As Integer
            Dim sFunctionalityName As String
            Dim sURLMapped As String
            Dim iStatus As Integer
            Dim objPCILogger As New PCILogger()
            Try

                'prasad added on 16-12-2009
                'Event logging code added for Modify Function. BugId #2100
                objPCILogger.EventOriginApplicationLocation = Me.Page.GetType().Name
                objPCILogger.EventOriginApplication = "ServicesPLUS Admin"
                objPCILogger.EventOriginMethod = "btnUpdate_Click"
                objPCILogger.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
                objPCILogger.OperationalUser = Environment.UserName
                objPCILogger.EventType = EventType.Update
                If Not Session.Item("siamuser") Is Nothing Then
                    'objPCILogger.UserName = DirectCast(DirectCast(Session.Item("siamuser"), System.Object), Sony.US.siam.User).UserId
                    objPCILogger.EmailAddress = DirectCast(DirectCast(Session.Item("siamuser"), System.Object), Sony.US.siam.User).EmailAddress '6524
                    objPCILogger.CustomerID = DirectCast(DirectCast(Session.Item("siamuser"), System.Object), Sony.US.siam.User).CustomerID
                    objPCILogger.CustomerID_SequenceNumber = DirectCast(DirectCast(Session.Item("siamuser"), System.Object), Sony.US.siam.User).SequenceNumber
                    objPCILogger.SIAM_ID = DirectCast(DirectCast(Session.Item("siamuser"), System.Object), Sony.US.siam.User).Identity
                    objPCILogger.LDAP_ID = DirectCast(DirectCast(Session.Item("siamuser"), System.Object), Sony.US.siam.User).AlternateUserId '6524
                End If
                objPCILogger.EventDateTime = DateTime.Now.ToLongTimeString()
                objPCILogger.HTTPRequestObjectValues = GetRequestContextValue()
                If ValidateUsrInputs() = True Then

                    sFunctionalityName = txtFunctionalityName.Text.Trim()
                    sURLMapped = txtURLMapped.Text.Trim()

                    If chkIsActive.Checked = True Then
                        iStatus = 0
                        objPCILogger.EventType = EventType.Update
                    Else
                        iStatus = 1
                        objPCILogger.EventType = EventType.Delete
                    End If

                    iRetStatus = oUsrRoleMgr.UpdateFunctionality(txtFunctionalityId.Text.Trim(), sFunctionalityName, sURLMapped, iStatus, Convert.ToInt16(drpGroupName.SelectedValue), Convert.ToInt16(drpLive.SelectedItem.Value))
                    If iRetStatus = vbNull Then
                        ErrorLabel.Text = "Error on updating Functionality."
                        objPCILogger.IndicationSuccessFailure = "Failure"
                        objPCILogger.Message = "Error on updating Functionality Name " & sFunctionalityName & " with FunctionalityId " & txtFunctionalityId.Text.Trim()
                    Else
                        ErrorLabel.Text = "Functionality updated successfully."
                        objPCILogger.IndicationSuccessFailure = "Success"
                        If iStatus = 0 Then
                            objPCILogger.Message = "Functionality Name " & sFunctionalityName & " with FunctionalityId " & txtFunctionalityId.Text.Trim() & " is updated successfully."
                        Else
                            objPCILogger.Message = "Functionality Name " & sFunctionalityName & " with FunctionalityId " & txtFunctionalityId.Text.Trim() & " is deleted successfully."
                        End If
                    End If
                Else
                    objPCILogger.IndicationSuccessFailure = "Failure"
                    objPCILogger.Message = "Invalid inputs"
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                objPCILogger.IndicationSuccessFailure = "Failure"
                objPCILogger.Message = ex.Message.ToString()
            Finally
                objPCILogger.PushLogToMSMQ()
            End Try
        End Sub

        Private Function ValidateUsrInputs() As Boolean

            Dim iRet As Boolean = False
            If txtFunctionalityName.Text.Trim.Length > 0 Then
                ErrorLabel.Text = ""
                iRet = True
            Else
                ErrorLabel.Text = "Please enter valid Functionality Name."
                iRet = False
                'prasad added on 16-12-2009
                'Added below line to Exit from this fucntion when Functionality name is empty.
                Exit Function
            End If

            If txtURLMapped.Text.Trim.Length > 0 Then
                ErrorLabel.Text = ""
                iRet = True
            Else
                ErrorLabel.Text = "Please enter valid URL Mapped."
                iRet = False
            End If
            Return iRet
        End Function

        Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click

            txtFunctionalityId.Text = ""
            txtFunctionalityName.Text = ""
            txtURLMapped.Text = ""
            chkIsActive.Checked = False
            drpLive.SelectedIndex = -1
            drpLive.SelectedIndex = 0
        End Sub
    End Class

End Namespace
