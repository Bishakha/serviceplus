Imports Sony.US.ServicesPLUS.Data
Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Process
Imports System.Drawing
Imports ServicesPlusException


Namespace SIAMAdmin

    Partial Class training_type_list
        Inherits UtilityClass
        'Dim objUtilties As Utilities = New Utilities

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub


        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            MyBase.IsProtectedPage(True, True)
            InitializeComponent()
            ApplyDataGridStyle()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            If Not IsPostBack Then
                Try
                    writeScriptsToPage()
                    Dim cdm As New CourseDataManager
                    Dim css As CourseType() = cdm.GetTypes()
                    Session.Add("training-type-list", css)
                    Me.typeDataGrid.DataSource = css
                    Me.typeDataGrid.DataBind()


                    ErrorLabel.Visible = True


                    ErrorLabel.Visible = True
                Catch ex As Exception
                    ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                    ErrorLabel.Visible = True
                End Try
            End If
        End Sub

        Sub typeDataGrid_Paging(ByVal sender As Object, ByVal e As DataGridPageChangedEventArgs)
            Try
                typeDataGrid.CurrentPageIndex = e.NewPageIndex
                Me.typeDataGrid.DataSource = Session("training-type-list")
                Me.typeDataGrid.DataBind()
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                ErrorLabel.Visible = True
            End Try
        End Sub

        Private Sub ApplyDataGridStyle()
            typeDataGrid.ApplyStyle(GetStyle(520))
            typeDataGrid.PageSize = 10
            typeDataGrid.AutoGenerateColumns = False
            typeDataGrid.GridLines = GridLines.Horizontal
            typeDataGrid.AllowSorting = True
            typeDataGrid.CellPadding = 3
            typeDataGrid.PagerStyle.Mode = PagerMode.NumericPages
            typeDataGrid.HeaderStyle.Height = Unit.Pixel(30)
            typeDataGrid.PagerStyle.Height = Unit.Pixel(10)
            typeDataGrid.HeaderStyle.Font.Size = FontUnit.Point(10)
            typeDataGrid.HeaderStyle.ForeColor = Color.GhostWhite
            typeDataGrid.HeaderStyle.BackColor = Color.FromKnownColor(KnownColor.ControlDarkDark)
            typeDataGrid.ItemStyle.Font.Size = FontUnit.Point(10)
            typeDataGrid.ItemStyle.ForeColor = Color.GhostWhite
            typeDataGrid.AlternatingItemStyle.Font.Size = FontUnit.Point(10)
            typeDataGrid.AlternatingItemStyle.ForeColor = Color.GhostWhite
        End Sub

        Private Sub typeDataGrid_ItemCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles typeDataGrid.ItemCreated
            Try
                If e.Item.ItemType <> ListItemType.Header Then
                    e.Item.ApplyStyle(GetStyle(e.Item.ItemType))
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                ErrorLabel.Visible = True
            End Try
        End Sub

        Private Sub typeDataGrid_ItemDataBound(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles typeDataGrid.ItemDataBound
            Try
                If (e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem) Then
                    Dim hyperLink As HyperLink = CType(e.Item.Cells(0).FindControl("lnkType"), HyperLink)
                    hyperLink.NavigateUrl = "training-type-edit.aspx?LineNo=" + (e.Item.ItemIndex() + typeDataGrid.CurrentPageIndex * typeDataGrid.PageSize).ToString
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                ErrorLabel.Visible = True
            End Try
        End Sub


        Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
            Response.Redirect("training-type-edit.aspx")
        End Sub

        '-- helper scripts --
        Private Sub writeScriptsToPage()
            '-- this script will make sure that this page is only accessed via the iform tag in default.aspx -         
            RegisterClientScriptBlock("redirect", "<script language=""javascript"">if ( parent.frames.length === 0 ){window.location.replace(""" + MyBase.EntryPointPage + """)}</script>")

            '-- this will clear the status bar on the browser 
            RegisterStartupScript("ClearStatus", "<script language=""javascript"">window.parent.status = ""Done."";</script>")

            '-- this script is used to confirm that a user wants to remove an application -
            RegisterStartupScript("RemoveApplication", "<script language=""javascript"">function DeleteOp(itemId){var ok = window.confirm(""Are you sure ?"");if (ok === true){document.location.href=""" + Page.Request.Url.Segments.GetValue(1).ToString() + "?Remove="" + itemId;}}</script>")
        End Sub
    End Class

End Namespace
