﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ErrorHandler.aspx.vb" Inherits="e_ErrorHandler" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
	<head>
		<title>ServicesPlus - Error</title>
		<link href="includes/style.css" type="text/css" rel="stylesheet">
	</head>
	<body>
		<form id="Form1" method="post" runat="server">
			<asp:Panel id="Panel1" runat="server"
				Width="457px" Height="78px" BorderStyle="Solid" BorderWidth="1px" CssClass="BodyHead" BackColor="#E0E0E0">
                An unhandled error occurred in the ServicesPlus Administration application. This type of error will likely
                not be logged. If the error keeps occurring, please contact <a href="mailto:ServicesPlus@am.sony.com">ServicesPlus@am.sony.com</a>
                with a description of what you were doing before the error.<br />
                <br />
                Please <a href="default.aspx">click here</a> to go back to the Administration home page.
			</asp:Panel>
		</form>
	</body>
</html>
