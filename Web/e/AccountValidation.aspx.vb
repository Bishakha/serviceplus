Imports Sony.US.siam, Sony.US.ServicesPLUS.Controls
Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Process
Imports Sony.US.SIAMUtilities
Imports System.Drawing
Imports Sony.US.AuditLog
Imports ServicesPlusException

Namespace SIAMAdmin
    Partial Class AccountValidation
        Inherits UtilityClass

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            Page.ID = Request.Url.Segments.GetValue(1)
            MyBase.IsProtectedPage(True, True)
            'buildDynamicControls()
            InitializeComponent()
            '-- apply the datagrid style --
            ApplyDataGridStyle()
        End Sub

#End Region

#Region "   Page Variables  "
        Const conTextBoxName As String = "AccountNumberTextBox"
        Const conCheckBoxName As String = "AccountNumberCheckBox"
        Private pageMode As String = String.Empty
        Private identity As String = String.Empty
        Private LDapID As String = String.Empty '7086
        'Dim objUtilties As Utilities = New Utilities
        Private theCustomer As Customer
        Dim isRedirect As Boolean = True
#End Region

#Region "Control and Page Events"

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                headerNav.Visible = False
                If Not Request.QueryString("Mode") Is Nothing Then pageMode = Request.QueryString("Mode")
                'If Not Request.QueryString("Identity") Is Nothing Then identity = Request.QueryString("Identity")'7086
                If Not Request.QueryString("LdapId") Is Nothing Then
                    LDapID = Request.QueryString("LdapId") '7086
                    Dim cm As New CustomerManager '7086
                    identity = cm.GetSiamIdByLdapId(LDapID) '7086
                End If


                'If Not identity Is Nothing Then '7086
                '    CustomerSubNav.SiamIdentity = identity.ToString()'7086
                If Not identity Is Nothing Then '7086
                    CustomerSubNav.LdapID = LDapID.ToString() '7086
                Else
                    ErrorLabel.Text = "No identity."
                    Exit Sub
                End If
                If (pageMode = "VSAPAC") Then
                    headerNav.Visible = True
                    btnSave.Visible = False
                    headerText.InnerText = "Manage Account"
                    btnAccountSearch.Text = "Update Account"
                Else
                    headerNav.Visible = False
                    btnSave.Visible = True
                    headerText.InnerText = "Validate Account Holder"
                    btnAccountSearch.Text = "Search"
                End If
                'If Not Page.IsPostBack Then
                ControlMethod(sender, e)
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Sub btnCustomerSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCustomerSearch.Click
            ControlMethod(sender, e)
        End Sub

        Private Sub pager_Datagrid2(ByVal sender As System.Object, ByVal e As DataGridPageChangedEventArgs) Handles dgSearchResult.PageIndexChanged
            dgSearchResult.CurrentPageIndex = e.NewPageIndex
            ControlMethod(sender, e)
        End Sub

        Protected Sub btnAccountSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAccountSearch.Click
            ControlMethod(sender, e)
        End Sub

        Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
            ControlMethod(sender, e)
        End Sub

        Protected Sub dgSearchResult_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgSearchResult.ItemCommand
            Try
                Dim strAccountID As String
                Dim thisAccount As Account
                Dim thisCustMng As New CustomerManager
                Dim thisArrayList As New ArrayList
                If e.CommandName = "Expand" Then
                    Dim imgExpandChange As ImageButton = CType(e.Item.FindControl("imgExpand"), ImageButton)
                    Dim dgInnerGrid As DataGrid = CType(e.Item.FindControl("dgInnerSAPNAME"), DataGrid)
                    Dim innertableHTML As HtmlTable = CType(e.Item.FindControl("innertable"), HtmlTable)
                    Dim lblNoSAPShiptoFound As Label = CType(e.Item.FindControl("lblNoSAPShipto"), Label)
                    If imgExpandChange.ImageUrl.ToString.IndexOf("plus.gif") > 0 Then
                        imgExpandChange.ImageUrl = "images/minus.gif"
                    ElseIf imgExpandChange.ImageUrl.ToString.IndexOf("minus.gif") > 0 Then
                        imgExpandChange.ImageUrl = "images/plus.gif"
                        innertableHTML.Visible = False
                        If Not dgInnerGrid Is Nothing Then
                            dgInnerGrid.Visible = False
                            dgInnerGrid = Nothing
                        End If
                        If Not lblNoSAPShiptoFound Is Nothing Then
                            lblNoSAPShiptoFound.Visible = False
                            lblNoSAPShiptoFound = Nothing
                        End If
                        Exit Sub
                    End If

                    strAccountID = CType(e.Item.Cells(0).Controls(1), Label).Text
                    thisAccount = New Account With {
                        .AccountNumber = strAccountID
                    }
                    If Not String.IsNullOrWhiteSpace(thisAccount.AccountNumber) Then
                        Dim objGlobalData As New GlobalData()
                        Try
                            objGlobalData.DistributionChannel = ConfigurationData.GeneralSettings.GlobalData.DistributionChannel.Value
                            objGlobalData.Division = ConfigurationData.GeneralSettings.GlobalData.Division.Value

                            If thisAccount.ShipTo.Country = ConfigurationData.GeneralSettings.GlobalData.SalesOrganization.US.Substring(0, 2) Then
                                objGlobalData.SalesOrganization = ConfigurationData.GeneralSettings.GlobalData.SalesOrganization.US
                                objGlobalData.Language = ConfigurationData.GeneralSettings.GlobalData.Language.US
                            Else
                                objGlobalData.SalesOrganization = ConfigurationData.GeneralSettings.GlobalData.SalesOrganization.CA
                                objGlobalData.Language = ConfigurationData.GeneralSettings.GlobalData.Language.CA_en
                            End If
                        Catch ex As Exception

                        End Try
                        thisCustMng.PopulateSAPAccount(thisAccount, objGlobalData)

                        For Each acc As Account In thisAccount.ShipToAccount
                            thisAccount = New Account
                            thisAccount.AccountNumber = acc.AccountNumber
                            thisAccount.Address.Name = acc.Address.Name
                            thisAccount.Address.Line1 = acc.Address.Address1
                            thisAccount.Address.Line2 = acc.Address.Address2
                            thisAccount.Address.Line3 = acc.Address.Address3
                            thisAccount.Address.State = acc.Address.State
                            thisAccount.Address.PostalCode = acc.Address.Zip
                            If Not String.IsNullOrWhiteSpace(thisAccount.Address.Line1) Then thisArrayList.Add(thisAccount)
                        Next
                    End If

                    If dgInnerGrid IsNot Nothing Then
                        Try
                            Dim accts() As Account = thisArrayList.ToArray(thisAccount.GetType)
                            If accts?.Length > 0 Then
                                innertableHTML.Visible = True
                                lblNoSAPShiptoFound.Visible = False
                                dgSearchResult.AutoGenerateColumns = False

                                dgInnerGrid.Visible = True
                                dgInnerGrid.AllowPaging = False
                                dgInnerGrid.GridLines = GridLines.Horizontal
                                dgInnerGrid.AllowSorting = True
                                dgInnerGrid.PagerStyle.Mode = PagerMode.NumericPages
                                dgInnerGrid.HeaderStyle.Font.Size = FontUnit.Point(10)
                                dgInnerGrid.HeaderStyle.BackColor = Color.FromKnownColor(KnownColor.ControlDarkDark)
                                dgInnerGrid.AlternatingItemStyle.Font.Size = FontUnit.Point(8)
                                dgInnerGrid.ItemStyle.Font.Size = FontUnit.Point(8)
                                dgInnerGrid.ItemStyle.BackColor = Color.SlateGray
                                dgInnerGrid.HeaderStyle.ForeColor = Color.White
                                dgInnerGrid.ItemStyle.ForeColor = Color.White
                                dgInnerGrid.AlternatingItemStyle.ForeColor = Color.White

                                dgInnerGrid.DataSource = accts
                                dgInnerGrid.DataBind()
                            End If
                            If dgInnerGrid.DataSource Is Nothing Then
                                innertableHTML.Visible = False
                                dgInnerGrid.Visible = False
                                lblNoSAPShiptoFound.Visible = True
                                lblNoSAPShiptoFound.Text = "No ship to found for account number."
                            End If
                        Catch ex As Exception
                            ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                        End Try
                    End If
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

#End Region

#Region "   Methods "

#Region "       Control Method      "
        Private Sub ControlMethod(ByVal sender As System.Object, ByVal e As System.EventArgs)
            Try
                Dim caller As String = String.Empty
                Dim thisCM As New CustomerManager

                If identity IsNot Nothing Then
                    If Not sender.ID Is Nothing Then caller = sender.ID.ToString()
                    theCustomer = thisCM.GetCustomer(identity)
                    'Utilities.LogDebug($"AccountValidation.controlMethod - Customer Detail: ({theCustomer.ToString()}).")  'TODO: Remove this after debugging.

                    Select Case caller
                        Case Page.ID.ToString
                            If Not Page.IsPostBack Then
                                Session.Remove("ControlCount")
                                BuildCustomerProfile(theCustomer)
                                BuildAccountsTable(theCustomer)
                            End If
                        Case btnAccountSearch.ID.ToString()
                            AddAccounts(theCustomer)
                            BuildAccountsTable(theCustomer)
                            getAccountInfo(theCustomer)
                            If btnAccountSearch.Text.Contains("Update Account") Then
                                If (isRedirect) Then
                                    Response.Redirect($"CustomerInfo.aspx?vu={theCustomer.LdapID}", False)
                                    HttpContext.Current.ApplicationInstance.CompleteRequest()
                                End If
                            End If
                        Case btnCustomerSearch.ID.ToString()
                            GetCustomerInfo(theCustomer)
                        Case dgSearchResult.ID.ToString()
                            getAccountInfo(theCustomer)
                        Case btnSave.ID.ToString()
                            AddAccounts(theCustomer)
                            ValidateAccounts(theCustomer)
                    End Select
                    writeScriptsToPage()
                Else
                    ErrorLabel.Text = "No identity."
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub
#End Region

#Region "       Customer Profile Table      "
        Private Sub BuildCustomerProfile(ByRef thisCustomer As Customer)
            Try
                CustomerLabel.Text = thisCustomer.FirstName + " " + thisCustomer.LastName
                Address1Label.Text = thisCustomer.Address.Line1
                Address2Label.Text = thisCustomer.Address.Line2
                Address3Label.Text = thisCustomer.Address.Line3
                CityLabel.Text = thisCustomer.Address.City
                StateLabel.Text = thisCustomer.Address.State
                ZipLabel.Text = thisCustomer.Address.PostalCode
                PhoneLabel.Text = thisCustomer.PhoneNumber
                EmailAddressLabel.Text = thisCustomer.EmailAddress
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub
#End Region

        Private Sub BuildAccountsTable(ByRef thisCustomer As Customer)
            If tbCompanyName.Text = "" Then tbCompanyName.Text = thisCustomer.CompanyName
            Try
                Dim controlNumber As Int32 = 1
                Dim thisArrayList As New ArrayList
                Dim txtTemp As SPSTextBox
                For Each thisAccount As Account In thisCustomer.SAPBillToAccounts
                    'If thisAccount.AccountNumber.Length > 0 And thisAccount.AccountNumber <> "9999999" And thisAccount.AccountNumber <> "999999" Then
                    If thisAccount.AccountNumber.Length > 0 Then
                        If Not CheckDuplicates(thisAccount.AccountNumber.ToString(), thisArrayList) Then
                            txtTemp = Page.FindControl("Form1").FindControl(conTextBoxName + controlNumber.ToString())
                            txtTemp.Text = thisAccount.AccountNumber.ToString()
                            thisArrayList.Add(thisAccount.AccountNumber)
                            controlNumber = controlNumber + 1
                            txtTemp.BackColor = Color.White
                            txtTemp.ForeColor = Color.Black
                        End If
                    End If
                Next
                For controlNumber = controlNumber To 12
                    txtTemp = Page.FindControl("Form1").FindControl(conTextBoxName + controlNumber.ToString())
                    txtTemp.Text = String.Empty
                    txtTemp.BackColor = Color.White
                    txtTemp.ForeColor = Color.Black
                Next
                If controlNumber > 0 Then controlNumber = (controlNumber - 1)
                If controlNumber = 0 And btnSave.Enabled Then
                    isRedirect = False
                    NoValidAccountNumbersLabel.Text = "No valid account numbers."
                End If

                Session.Item("ControlCount") = controlNumber
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

#Region "       add Accounts       "
        Private Sub AddAccounts(ByRef thisCustomer As Customer)
            Dim custMan As New CustomerManager
            Dim item As DataGridItem
            Dim thisTextBox As SPSTextBox
            Dim blnAccountExist As Boolean = False
            Dim strAccount As String
            Dim objPCILogger As New PCILogger()
            Dim debugString As String = $"AccountValidation.AddAccounts - START at {DateTime.Now.ToLongTimeString}{Environment.NewLine}"

            Try
                objPCILogger.EventOriginApplication = "ServicesPLUS"
                objPCILogger.EventOriginApplicationLocation = Me.Page.GetType().Name
                objPCILogger.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
                objPCILogger.OperationalUser = Environment.UserName
                objPCILogger.EventDateTime = DateTime.Now.ToLongTimeString()
                objPCILogger.HTTPRequestObjectValues = objPCILogger.GetRequestContextValue()
                objPCILogger.IndicationSuccessFailure = "Success"
                objPCILogger.CustomerID = thisCustomer.CustomerID
                objPCILogger.CustomerID_SequenceNumber = thisCustomer.SequenceNumber
                'objPCILogger.UserName = thisCustomer.UserName
                objPCILogger.EmailAddress = thisCustomer.EmailAddress
                objPCILogger.SIAM_ID = thisCustomer.SIAMIdentity
                objPCILogger.LDAP_ID = thisCustomer.LdapID
                objPCILogger.EventOriginMethod = "AddAccounts"
                objPCILogger.Message = "AddAccounts Success."
                objPCILogger.EventType = EventType.Add

                ' Loop through any search results, finding any with the "Add" box checked.
                For x As Int16 = 0 To dgSearchResult.Items.Count - 1
                    item = dgSearchResult.Items(x)
                    Dim cb As CheckBox = item.Cells(5).Controls(1)
                    If cb.Checked Then
                        For y As Int32 = 1 To 12
                            ' Find the first empty text box to drop the account number into.
                            If Page.FindControl("Form1").FindControl($"{conTextBoxName}{y}") IsNot Nothing Then
                                thisTextBox = Page.FindControl("Form1").FindControl($"{conTextBoxName}{y}")
                                If String.IsNullOrWhiteSpace(thisTextBox.Text) Then
                                    strAccount = CType(item.Cells(0).Controls(1), Label).Text
                                    If Not CheckDuplicateAccountSelection(strAccount) Then
                                        thisTextBox.Text = strAccount
                                        debugString &= $"- Account Found: {strAccount}.{Environment.NewLine}"
                                    End If
                                    cb.Checked = False
                                    cb.Enabled = False
                                    y = 13
                                End If
                            End If
                        Next

                        If cb.Checked = True Then
                            NoCustomersLabel.Text = "Can not add more than 12 accounts."
                            isRedirect = False
                            Exit For
                        End If
                        ' add this item to the validate list
                    End If
                Next
                debugString &= $"- Account Loop finished.{Environment.NewLine}"

                ' Remove all the existing SAP accounts (?)
                For Each thisSAPBillToAccount As Account In thisCustomer.SAPBillToAccounts
                    thisCustomer.RemoveAccount(thisSAPBillToAccount)
                Next

                ' Updates the base Customer object. Has nothing to do with "Subscriptions"...
                custMan.UpdateCustomerSubscription(thisCustomer)

                Dim objGlobalData As New GlobalData With {
                    .DistributionChannel = ConfigurationData.GeneralSettings.GlobalData.DistributionChannel.Value,
                    .Division = ConfigurationData.GeneralSettings.GlobalData.Division.Value,
                    .SalesOrganization = ConfigurationData.GeneralSettings.GlobalData.SalesOrganization.US,
                    .Language = ConfigurationData.GeneralSettings.GlobalData.Language.US
                }
                If thisCustomer.CountryCode = ConfigurationData.GeneralSettings.GlobalData.SalesOrganization.CA.Substring(0, 2) Then
                    objGlobalData.SalesOrganization = ConfigurationData.GeneralSettings.GlobalData.SalesOrganization.CA
                    objGlobalData.Language = ConfigurationData.GeneralSettings.GlobalData.Language.CA_en
                End If

                ' Clear the local Customer.SAPBillToAccounts
                thisCustomer.ClearAllSAPAccounts()
                ' Pull a list of Accounts from the database.
                Dim ls As New ArrayList
                ls = custMan.GetAccountData(thisCustomer.CustomerID)
                debugString &= $"- Old Account Count: {ls.Count}.{Environment.NewLine}"

                If Session.Item("ControlCount") IsNot Nothing Then  ' ASleight - Is there a point to this check?
                    For x As Int32 = 1 To 12
                        If Page.FindControl("Form1").FindControl($"{conTextBoxName}{x}") IsNot Nothing Then
                            thisTextBox = Page.FindControl("Form1").FindControl($"{conTextBoxName}{x}")
                            If Not String.IsNullOrWhiteSpace(thisTextBox.Text) Then
                                If (pageMode = "VSAPAC") Then
                                    If Not ls.Contains(thisTextBox.Text.Trim()) Then
                                        debugString &= $"- Validating Account. Customer ID: {thisCustomer.CustomerID}, TextBox: {thisTextBox.Text}.{Environment.NewLine}"
                                        debugString &= $"- Customer Detail: ({thisCustomer.ToString()}).{Environment.NewLine}"
                                        custMan.ValidateCustomerSAPAccount(thisCustomer, thisCustomer.AddSAPBillToAccount(thisTextBox.Text), objGlobalData, blSaveValidAcct:=True)
                                    End If
                                Else
                                    AddAccountToValidateList(thisTextBox.Text.Trim(), thisCustomer)
                                End If
                            End If
                        End If
                    Next
                End If
                'If (pageMode <> "VSAPAC") Then thisCustMng.UpdateCustomerSubscription(thisCustomer)
                'Utilities.LogDebug(debugString)
            Catch ex As Exception
                Utilities.LogMessages(debugString)
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                isRedirect = False
                objPCILogger.IndicationSuccessFailure = "Failure" '6524
                objPCILogger.Message = "AddAccounts Failed. " & ex.Message.ToString() '6524
            Finally
                objPCILogger.PushLogToMSMQ() '6524
            End Try
        End Sub

        Private Function CheckDuplicateAccountSelection(ByVal strAccount As String) As Boolean
            For y As Int32 = 1 To 12
                If Page.FindControl("Form1").FindControl($"{conTextBoxName}{y}") IsNot Nothing Then
                    If CType(Page.FindControl("Form1").FindControl($"{conTextBoxName}{y}"), SPSTextBox).Text = strAccount Then
                        Return True
                    End If
                End If
            Next
            Return False
        End Function

        Private Sub AddAccountToValidateList(ByVal accountNumber As String, ByRef thisCustomer As Customer)
            ' add the value to the first blank text box.
            If Not CheckDuplicates(accountNumber, thisCustomer) Then
                '-- add account number
                thisCustomer.AddSAPBillToAccount(accountNumber)
            End If
        End Sub
#End Region

#Region "       Check duplicates        "

        Private Function CheckDuplicates(ByVal accountNumber As String, ByRef thisCustomer As Customer) As Boolean
            For Each thisSAPBillToAccount As Sony.US.ServicesPLUS.Core.Account In thisCustomer.SAPBillToAccounts
                If thisSAPBillToAccount.AccountNumber = accountNumber Then
                    Return True
                End If
            Next

            Return False
        End Function

        Private Function CheckDuplicates(ByVal accountNumber As String, ByVal numberOfControls As Int32) As Boolean
            Try
                Dim thisTextBox As SPSTextBox

                For x As Int32 = 1 To numberOfControls
                    If Page.FindControl("Form1").FindControl($"{conTextBoxName}{x}") IsNot Nothing Then
                        thisTextBox = Page.FindControl("Form1").FindControl($"{conTextBoxName}{x}")
                        If thisTextBox.Text = accountNumber Then
                            Return True
                        End If
                    End If
                Next
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try

            Return False
        End Function

        Private Function CheckDuplicates(ByVal accountNumber As String, ByVal thisArrayList As ArrayList) As Boolean
            Try
                For Each thisAccountNumber As String In thisArrayList
                    If thisAccountNumber = accountNumber Then
                        Return True
                    End If
                Next
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try

            Return False
        End Function
#End Region

#Region "       Validate Accounts         "
        Private Sub ValidateAccounts(ByRef thisCustomer As Customer)
            Dim pciLog As New PCILogger()
            Dim thisCustMng As New CustomerManager
            Dim validatedAccouNumbersList As New ArrayList()
            Dim account As New Account()
            Dim ls As New ArrayList
            Dim blnValidated As Boolean = False

            Try
                'First Verify accounts
                If Not VerifyAccountNumbers(thisCustomer, validatedAccouNumbersList) Then Exit Sub

                pciLog.EventOriginApplication = "ServicesPLUS Admin"
                pciLog.EventOriginApplicationLocation = Me.Page.GetType().Name
                pciLog.EventOriginMethod = "ValidateAccounts"
                pciLog.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
                pciLog.OperationalUser = Environment.UserName
                pciLog.EventDateTime = DateTime.Now.ToLongTimeString()
                pciLog.HTTPRequestObjectValues = GetRequestContextValue()
                pciLog.EventType = EventType.Others
                If Session.Item("siamuser") IsNot Nothing Then
                    Dim siamUser = CType(Session.Item("siamuser"), User)
                    pciLog.EmailAddress = siamUser.EmailAddress
                    pciLog.CustomerID = siamUser.CustomerID
                    pciLog.CustomerID_SequenceNumber = siamUser.SequenceNumber
                    pciLog.SIAM_ID = siamUser.Identity
                    pciLog.LDAP_ID = siamUser.AlternateUserId
                End If

                '-- hide data grids and tables 
                dgSearchResult.Visible = False
                NoCustomersLabel.Visible = False
                CustomerNameResults.Visible = False
                AccountNumberTable.Visible = False

                ErrorLabel.Text = "Account has been validated in SIAM."

                ' First lets clear the customers accounts
                For Each thisSAPBillToAccount As Sony.US.ServicesPLUS.Core.Account In thisCustomer.SAPBillToAccounts
                    thisCustomer.RemoveAccount(thisSAPBillToAccount)
                Next

                Dim accounts() As Account = validatedAccouNumbersList.ToArray(account.GetType())

                ls = thisCustMng.GetAccountData(thisCustomer.CustomerID)

                If (thisCustomer.UserType = "P") Then ' added code for resolving the bug 8628
                    For Each accounnumber In ls
                        Dim invalidaccount As Boolean = True
                        For Each varaccount In accounts
                            'Changes made by Sneha on 21/07/2014 for "Update ServicesPLUS to support 10 character KUNNR"
                            'If accounnumber.ToString().Trim().Contains(varaccount.AccountNumber.Trim().Remove(0, 3)) Then
                            'We need not remove three characters because both db acc number and sap acc number will be 10 characters
                            If accounnumber.ToString().Trim().Contains(varaccount.AccountNumber.Trim()) Then
                                invalidaccount = False
                            End If
                        Next
                        If (invalidaccount) Then
                            'Changes made by Sneha on 21/07/2014 for Bug:9015"Update ServicesPLUS to support 10 character KUNNR"
                            'thisCustMng.DeleteSAPAccount(thisCustomer.CustomerID,"000"+ accounnumber)
                            'Appending with 000 is not required 
                            thisCustMng.DeleteSAPAccount(thisCustomer.CustomerID, accounnumber)
                        End If
                    Next

                End If
                thisCustMng.UpdateCustomerSubscriptionUserType(thisCustomer)

                '-- clears the in memory list of accounts 
                thisCustomer.ClearAllSAPAccounts()

                'Global Data 
                Dim objGlobalData As New GlobalData With {
                    .DistributionChannel = ConfigurationData.GeneralSettings.GlobalData.DistributionChannel.Value,
                    .Division = ConfigurationData.GeneralSettings.GlobalData.Division.Value,
                    .SalesOrganization = ConfigurationData.GeneralSettings.GlobalData.SalesOrganization.US,
                    .Language = ConfigurationData.GeneralSettings.GlobalData.Language.US
                }
                If thisCustomer.CountryCode = ConfigurationData.GeneralSettings.GlobalData.SalesOrganization.CA.Substring(0, 2) Then
                    objGlobalData.SalesOrganization = ConfigurationData.GeneralSettings.GlobalData.SalesOrganization.CA
                    objGlobalData.Language = ConfigurationData.GeneralSettings.GlobalData.Language.CA_en
                End If


                '-- add only the selected account numbers 
                If Session.Item("ControlCount") IsNot Nothing Then
                    Dim thisTextBox As SPSTextBox
                    For x As Int32 = 1 To 12
                        If Page.FindControl("Form1").FindControl($"{conTextBoxName}{x}") IsNot Nothing Then  'And Not Page.FindControl("Form1").FindControl(conCheckBoxName.ToString() + x.ToString()) Is Nothing
                            thisTextBox = Page.FindControl("Form1").FindControl($"{conTextBoxName}{x}")
                            If thisTextBox.Text.Trim() <> String.Empty Then
                                If Not ls.Contains(thisTextBox.Text.Trim()) Then
                                    blnValidated = blnValidated Or thisCustMng.ValidateCustomerSAPAccount(thisCustomer, thisCustomer.AddSAPBillToAccount(thisTextBox.Text), objGlobalData)
                                End If
                            End If
                        End If
                    Next
                End If

                trSearchCompName.Visible = False
                trAccDetail.Visible = False
                trDg.Visible = False
                btnSave.Enabled = False
                Session.Remove("ControlCount")

                Response.Redirect("CustomerInfo.aspx?vu=" + thisCustomer.LdapID, False)
                HttpContext.Current.ApplicationInstance.CompleteRequest()
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                pciLog.IndicationSuccessFailure = "Failure"
                pciLog.Message = ex.Message.ToString()
            Finally
                pciLog.PushLogToMSMQ()
            End Try
        End Sub

#End Region

        Private Sub BuildButtonLink(ByVal Message As String)
            Try
                Dim ldapid As String = ""
                If Request.QueryString("LdapId") IsNot Nothing Then ldapid = Request.QueryString("LdapId")
                RegisterClientScriptBlock("redirectLDAP", "<script language='javascript'>var vHeight=700;var vWidth=1000;if (window.screen.height < 700) vHeight=window.screen.height;if (window.screen.width < 800) vWidth=window.screen.width;window.open('" + Sony.US.SIAMUtilities.ConfigurationData.Environment.ISSAWebService.ISSAApprovalAppURL + "','','toolbar=1,location=1,directories=1,x=0,y=0,status=1,menubar=1,scrollbars=yes,resize=1,resizable=1,width= ' + vWidth + ',height=' + vHeight); window.location.replace(""CustomerInfo.aspx?LdapId=" + ldapid + """);</script>") '7086
            Catch ex1 As Threading.ThreadAbortException
                'do nothing
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

#Region "       Get Account Info        "
        Private Sub GetCustomerInfo(ByRef thisCustomer As Customer)
            Dim pciLog As New PCILogger With {
                .EventOriginApplication = "ServicesPLUS Admin",
                .EventOriginApplicationLocation = Me.Page.GetType().Name,
                .EventOriginMethod = "getCustomerInfo",
                .EventOriginServerMachineName = HttpContext.Current.Server.MachineName,
                .OperationalUser = Environment.UserName,
                .EventDateTime = DateTime.Now.ToLongTimeString(),
                .EventType = EventType.Others,
                .HTTPRequestObjectValues = GetRequestContextValue()
            }

            Try
                Dim isAccountNumber As Boolean
                Dim accts As Account() = getSAPAccounts()
                'Dim acctsObj As Object() = getSAPAccounts()

                If Session.Item("siamuser") IsNot Nothing Then
                    Dim siamUser = CType(Session.Item("siamuser"), User)
                    pciLog.EmailAddress = siamUser.EmailAddress
                    pciLog.CustomerID = siamUser.CustomerID
                    pciLog.CustomerID_SequenceNumber = siamUser.SequenceNumber
                    pciLog.SIAM_ID = siamUser.Identity
                    pciLog.LDAP_ID = siamUser.AlternateUserId
                End If

                'accts = CType(acctsObj, Account())
                CustomerNameResults.Visible = True
                For Each a As Account In accts
                    If Not String.IsNullOrWhiteSpace(a.AccountNumber) Then
                        isAccountNumber = True
                        Exit For
                    End If
                Next

                If accts IsNot Nothing Then
                    If (isAccountNumber) Then
                        dgSearchResult.Visible = True
                        dgSearchResult.DataSource = accts
                        'If accts.Length > dgSearchResult.PageSize Then dgSearchResult.AllowPaging = True
                        dgSearchResult.DataBind()

                        pciLog.IndicationSuccessFailure = "Success"
                        pciLog.Message = "Search result based on the customer name is successful"
                        pciLog.PushLogToMSMQ()
                    End If
                Else
                    dgSearchResult.DataSource = Nothing
                End If

                '-- if we don't have any results then show message --
                If dgSearchResult.DataSource Is Nothing Then
                    dgSearchResult.Visible = False
                    NoCustomersLabel.Text = "No account found by customer name."

                    pciLog.IndicationSuccessFailure = "Failure"
                    pciLog.Message = "There are no search results found by the customer name."
                    pciLog.PushLogToMSMQ()
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                pciLog.IndicationSuccessFailure = "Failure"
                pciLog.Message = ex.Message.ToString()
            Finally
                pciLog.PushLogToMSMQ()
            End Try
        End Sub

        Private Sub getAccountInfo(ByRef thisCustomer As Customer)
            Dim acctsObj As Object() = getSAPAccounts()

            'Initialising the Logger Component --Added BY Rajkumar A. Added for fixing the Bug No #2095
            Dim pciLog As New PCILogger()

            Try

                'Added By Rajkumar A. 
                pciLog.EventOriginApplication = "ServicesPLUS Admin"
                pciLog.EventOriginApplicationLocation = Me.Page.GetType().Name
                pciLog.EventOriginMethod = "getAccountInfo"
                pciLog.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
                pciLog.OperationalUser = Environment.UserName
                pciLog.EventDateTime = DateTime.Now.ToLongTimeString()
                pciLog.HTTPRequestObjectValues = GetRequestContextValue()
                pciLog.EventType = EventType.Others

                If Session.Item("siamuser") IsNot Nothing Then
                    Dim siamUser = CType(Session.Item("siamuser"), User)
                    pciLog.EmailAddress = siamUser.EmailAddress
                    pciLog.CustomerID = siamUser.CustomerID
                    pciLog.CustomerID_SequenceNumber = siamUser.SequenceNumber
                    pciLog.SIAM_ID = siamUser.Identity
                    pciLog.LDAP_ID = siamUser.AlternateUserId
                End If

                Dim thisArrayList As New ArrayList
                Dim thisAccount As New Account
                Dim thisCustMng As New CustomerManager
                Dim txtTemp As SPSTextBox
                For counter As Int32 = 1 To 12
                    txtTemp = Page.FindControl("Form1").FindControl(conTextBoxName + counter.ToString())
                    If Not txtTemp Is Nothing Then
                        thisAccount = New Account With {
                            .AccountNumber = txtTemp.Text.Trim,
                            .Customer = thisCustomer
                        }
                        If Not String.IsNullOrWhiteSpace(thisAccount.AccountNumber) Then
                            'Global Data 
                            Dim objGlobalData As New GlobalData With {
                                .DistributionChannel = ConfigurationData.GeneralSettings.GlobalData.DistributionChannel.Value,
                                .Division = ConfigurationData.GeneralSettings.GlobalData.Division.Value,
                                .SalesOrganization = ConfigurationData.GeneralSettings.GlobalData.SalesOrganization.US,
                                .Language = ConfigurationData.GeneralSettings.GlobalData.Language.US
                            }
                            If thisCustomer.CountryCode = ConfigurationData.GeneralSettings.GlobalData.SalesOrganization.CA.Substring(0, 2) Then
                                objGlobalData.SalesOrganization = ConfigurationData.GeneralSettings.GlobalData.SalesOrganization.CA
                                objGlobalData.Language = ConfigurationData.GeneralSettings.GlobalData.Language.CA_en
                            End If

                            thisCustMng.PopulateSAPAccount(thisAccount, objGlobalData)

                            If thisAccount.Validated Then
                                If thisCustomer.UserType = "P" Or thisCustomer.UserType = "A" Then
                                    thisArrayList.Add(thisAccount)
                                Else
                                    txtTemp.BackColor = Color.Red
                                    txtTemp.ForeColor = Color.White
                                    isRedirect = False
                                End If
                            Else
                                txtTemp.BackColor = Color.Red
                                txtTemp.ForeColor = Color.White
                                isRedirect = False
                            End If
                        End If
                    End If
                Next

                ApplyDataGridStyle()
                CustomerNameResults.Visible = True
                Dim accts1() As Account = thisArrayList.ToArray(thisAccount.GetType)
                If accts1?.Length > 0 Then
                    dgSearchResult.Visible = True
                    dgSearchResult.DataSource = accts1
                    dgSearchResult.DataBind()

                    pciLog.IndicationSuccessFailure = "Success"
                    pciLog.Message = "Search result based on the Account number is successful"
                    pciLog.PushLogToMSMQ()
                End If

                If dgSearchResult.DataSource Is Nothing Then
                    dgSearchResult.Visible = False
                    NoCustomersLabel.Text = "No account found by account number."
                    isRedirect = False
                    pciLog.IndicationSuccessFailure = "Failure"
                    pciLog.Message = "No serach results found based on the account number."
                    pciLog.PushLogToMSMQ()
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                pciLog.IndicationSuccessFailure = "Failure"
                pciLog.Message = ex.Message.ToString()
                isRedirect = False
            Finally
                pciLog.PushLogToMSMQ()
            End Try
        End Sub

        Private Function getSAPAccounts() As Account()
            Dim thisCM As New CustomerManager
            Dim SAPAccounts As Account() = thisCM.SearchCustomerSAPAccount(tbCompanyName.Text)

            Return SAPAccounts
        End Function

        Private Function VerifyAccountNumbers(ByRef thisCustomer As Customer, ByRef validatedAccountNumberList As ArrayList) As Boolean
            Dim blnResult As Boolean = True
            Dim pciLog As New PCILogger()
            Dim thisArrayList As New ArrayList
            Dim thisAccount As New Account
            Dim thisCustMng As New CustomerManager
            Dim txtTemp As SPSTextBox
            Dim altxtTemp As New ArrayList '6867
            Dim str As String = "Invalid Account Numbers"
            Dim objGlobalData As New GlobalData()

            Try
                pciLog.EventOriginApplication = "ServicesPLUS"
                pciLog.EventOriginApplicationLocation = Me.Page.GetType().Name
                pciLog.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
                pciLog.OperationalUser = Environment.UserName
                pciLog.EventDateTime = DateTime.Now.ToLongTimeString()
                pciLog.HTTPRequestObjectValues = pciLog.GetRequestContextValue()
                pciLog.IndicationSuccessFailure = "Success"
                pciLog.CustomerID = thisCustomer.CustomerID
                pciLog.CustomerID_SequenceNumber = thisCustomer.SequenceNumber
                pciLog.EmailAddress = thisCustomer.EmailAddress
                pciLog.SIAM_ID = thisCustomer.SIAMIdentity
                pciLog.LDAP_ID = thisCustomer.LdapID
                pciLog.EventOriginMethod = "VerifyAccountNumbers"
                pciLog.Message = "VerifyAccountNumbers Success."
                pciLog.EventType = EventType.Others

                Try
                    objGlobalData.DistributionChannel = ConfigurationData.GeneralSettings.GlobalData.DistributionChannel.Value
                    objGlobalData.Division = ConfigurationData.GeneralSettings.GlobalData.Division.Value
                    If thisCustomer.CountryCode = ConfigurationData.GeneralSettings.GlobalData.SalesOrganization.US.Substring(0, 2) Then
                        objGlobalData.SalesOrganization = ConfigurationData.GeneralSettings.GlobalData.SalesOrganization.US
                        objGlobalData.Language = ConfigurationData.GeneralSettings.GlobalData.Language.US
                    Else
                        objGlobalData.SalesOrganization = ConfigurationData.GeneralSettings.GlobalData.SalesOrganization.CA
                        objGlobalData.Language = ConfigurationData.GeneralSettings.GlobalData.Language.CA_en
                    End If
                Catch ex As Exception

                End Try

                For counter As Int32 = 1 To 12
                    txtTemp = Page.FindControl("Form1").FindControl(conTextBoxName + counter.ToString())
                    If txtTemp IsNot Nothing Then
                        thisAccount = New Account With {
                            .AccountNumber = txtTemp.Text.Trim,
                            .Customer = thisCustomer
                        }
                        If Not String.IsNullOrWhiteSpace(thisAccount.AccountNumber) Then
                            thisCustMng.PopulateSAPAccount(thisAccount, objGlobalData)

                            If thisAccount.Validated Then
                                If thisCustomer.UserType = "P" Or thisCustomer.UserType = "A" Then
                                    thisArrayList.Add(thisAccount)
                                Else
                                    txtTemp.BackColor = Color.Red
                                    txtTemp.ForeColor = Color.White
                                    blnResult = False
                                End If
                            Else
                                txtTemp.BackColor = Color.Red
                                txtTemp.ForeColor = Color.White
                                blnResult = False
                            End If

                            If Not altxtTemp.Contains(txtTemp.Text.Trim) Then
                                altxtTemp.Add(txtTemp.Text.Trim)
                            Else
                                txtTemp.BackColor = Color.Red
                                txtTemp.ForeColor = Color.White
                                NoCustomersLabel.Text = "Duplicate account numbers."
                                Return False
                            End If
                        End If

                        If txtTemp.Text.Trim() = String.Empty Then
                            txtTemp.BackColor = Color.White
                            txtTemp.ForeColor = Color.Black
                        End If
                    End If
                Next

                If blnResult = False Then
                    If objGlobalData.IsCanada Then
                        NoCustomersLabel.Text = "The account numbers marked in red are invaild account numbers for Canada."
                    Else
                        NoCustomersLabel.Text = "The account numbers marked in red are invaild account numbers for USA."
                    End If
                    Return False
                End If
                ApplyDataGridStyle()
                CustomerNameResults.Visible = True
                validatedAccountNumberList = thisArrayList
                Dim accts1() As Account = thisArrayList.ToArray(thisAccount.GetType)
                If accts1?.Length > 0 Then
                    dgSearchResult.Visible = True
                    dgSearchResult.DataSource = accts1
                    dgSearchResult.DataBind()
                End If

                If dgSearchResult.DataSource Is Nothing Then
                    dgSearchResult.Visible = False
                    NoCustomersLabel.Text = "No account found by account number."
                    blnResult = False
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                pciLog.IndicationSuccessFailure = "Failure" '6524
                pciLog.Message = "VerifyAccountNumbers Failed. " & ex.Message.ToString() '6524
                Return False
            Finally
                pciLog.PushLogToMSMQ() '6524
            End Try
            Return blnResult
        End Function

#End Region

#Region "       Helper Methods      "

        Private Sub ApplyDataGridStyle()
            dgSearchResult.AutoGenerateColumns = False
            dgSearchResult.AllowPaging = False
            dgSearchResult.GridLines = GridLines.Horizontal
            dgSearchResult.AllowSorting = True
            dgSearchResult.PagerStyle.Mode = PagerMode.NumericPages
            dgSearchResult.HeaderStyle.Font.Size = FontUnit.Point(10)
            dgSearchResult.HeaderStyle.BackColor = Color.FromKnownColor(KnownColor.ControlDarkDark)
            dgSearchResult.AlternatingItemStyle.Font.Size = FontUnit.Point(8)
            dgSearchResult.ItemStyle.Font.Size = FontUnit.Point(8)
            dgSearchResult.ItemStyle.BackColor = Color.SlateGray
            dgSearchResult.HeaderStyle.ForeColor = Color.White
            dgSearchResult.ItemStyle.ForeColor = Color.White
            dgSearchResult.AlternatingItemStyle.ForeColor = Color.White
        End Sub

        '-- helper scripts --
        Private Sub writeScriptsToPage()
            '-- this script will make sure that this page is only accessed via the iform tag in default.aspx -         
            RegisterClientScriptBlock("redirect", "<script language=""javascript"">if ( parent.frames.length === 0 ){window.location.replace(""" + MyBase.EntryPointPage + """)}</script>")

            '-- this will clear the status bar on the browser 
            RegisterStartupScript("ClearStatus", "<script language=""javascript"">window.parent.status = ""Done."";</script>")

            '-- this script is used to confirm that a user wants to remove an application -
            RegisterStartupScript("RemoveApplication", "<script language=""javascript"">function DeleteOp(itemId){var ok = window.confirm(""Are you sure ?"");if (ok === true){document.location.href=""" + Page.Request.Url.Segments.GetValue(1).ToString() + "?Remove="" + itemId;}}</script>")
        End Sub
#End Region

#End Region

    End Class
End Namespace
