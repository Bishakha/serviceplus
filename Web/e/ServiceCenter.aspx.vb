Imports Sony.US.ServicesPLUS.Process
Imports ServicesPlusException
Imports Sony.US.AuditLog
Imports System.Data

Namespace SIAMAdmin

    Partial Class ServiceCenter
        Inherits UtilityClass
        'Dim objUtilties As Utilities = New Utilities


#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            MyBase.IsProtectedPage(True, True)
            Page.ID = Request.Url.Segments.GetValue(1)
            InitializeComponent()

        End Sub




#End Region

#Region "   Events  "

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                'ErrorLabel.Visible = False
                ErrorLabel.Text = String.Empty
                lblExistingInfo.Text = String.Empty
                If Not IsPostBack Then

                    'Populate DataGrid and DropDown Combo box
                    PopulateServiceCenters()
                    PopulateDataGrid()
                    btnCancel.Enabled = False
                    btnSave.Enabled = False
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Sub btnSave_click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
            If Valid() = False Then Exit Sub
            Dim objSIAMOperation As SIAMOperation = New SIAMOperation()
            Dim objPCILogger As New PCILogger() '6524
            Try
                '6524 start
                objPCILogger.EventOriginApplication = "ServicesPLUS"
                objPCILogger.EventOriginApplicationLocation = Me.Page.GetType().Name
                objPCILogger.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
                objPCILogger.OperationalUser = Environment.UserName
                objPCILogger.EventDateTime = DateTime.Now.ToLongTimeString()
                objPCILogger.HTTPRequestObjectValues = objPCILogger.GetRequestContextValue()
                objPCILogger.IndicationSuccessFailure = "Success"
                objPCILogger.EventOriginMethod = "btnSave_click"
                objPCILogger.Message = "btnSave_click Failed."
                '6524 end

                If btnSave.Text = "Save" Then
                    objPCILogger.EventType = EventType.Add
                    objSIAMOperation.StoreServiceCenterMapping(drpPrimaryServiceCenter.SelectedValue, drpSecondaryServiceCenter.SelectedValue, 1)
                ElseIf btnSave.Text = "Update" Then
                    objPCILogger.EventType = EventType.Update
                    objSIAMOperation.StoreServiceCenterMapping(drpPrimaryServiceCenter.SelectedValue, drpSecondaryServiceCenter.SelectedValue, 2)
                End If
                ErrorLabel.Text = "Saved successfully."

                dgServiceCenter.SelectedIndex = -1
                drpPrimaryServiceCenter.SelectedIndex = 0
                drpSecondaryServiceCenter.SelectedIndex = 0

                drpPrimaryServiceCenter.Enabled = False
                drpSecondaryServiceCenter.Enabled = False

                btnSave.Enabled = False
                btnAddNew.Enabled = True
                btnCancel.Enabled = False
                btnSave.Text = "Save"
                PopulateDataGrid()




            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                objPCILogger.IndicationSuccessFailure = "Failure" '6524
                objPCILogger.Message = "btnSave_click Failed. " & ex.Message.ToString() '6524
            Finally
                objPCILogger.PushLogToMSMQ() '6524
            End Try

        End Sub

        Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
            dgServiceCenter.SelectedIndex = -1
            drpPrimaryServiceCenter.SelectedIndex = 0
            drpSecondaryServiceCenter.SelectedIndex = 0

            drpPrimaryServiceCenter.Enabled = False
            drpSecondaryServiceCenter.Enabled = False

            btnSave.Enabled = False
            btnAddNew.Enabled = True
            btnCancel.Enabled = False
            btnSave.Text = "Save"
        End Sub

        Private Sub AddNewUSer_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddNew.Click
            dgServiceCenter.SelectedIndex = -1
            drpPrimaryServiceCenter.SelectedIndex = 0
            drpSecondaryServiceCenter.SelectedIndex = 0

            drpPrimaryServiceCenter.Enabled = True
            drpSecondaryServiceCenter.Enabled = True

            btnSave.Enabled = True
            btnCancel.Enabled = True
            btnSave.Text = "Save"
            btnAddNew.Enabled = False
        End Sub

        Protected Sub dgServiceCenter_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgServiceCenter.ItemCommand
            Try
                If e.CommandName = "Edit" Then
                    dgServiceCenter.SelectedIndex = e.Item.ItemIndex
                    drpPrimaryServiceCenter.Enabled = False
                    drpSecondaryServiceCenter.Enabled = True

                    drpPrimaryServiceCenter.SelectedIndex = -1
                    drpPrimaryServiceCenter.Items.FindByValue(e.Item.Cells(4).Text).Selected = True

                    drpSecondaryServiceCenter.SelectedIndex = -1
                    drpSecondaryServiceCenter.Items.FindByValue(e.Item.Cells(5).Text).Selected = True

                    btnSave.Enabled = True
                    btnSave.Text = "Update"
                    btnAddNew.Enabled = False
                    btnCancel.Enabled = True
                ElseIf e.CommandName = "Delete" Then
                    Dim objSIAMOperation As SIAMOperation = New SIAMOperation()
                    objSIAMOperation.StoreServiceCenterMapping(Convert.ToInt16(e.Item.Cells(4).Text), Convert.ToInt16(e.Item.Cells(5).Text), 3)
                    ErrorLabel.Text = "Deleted successfully."

                    dgServiceCenter.SelectedIndex = -1
                    drpPrimaryServiceCenter.SelectedIndex = 0
                    drpSecondaryServiceCenter.SelectedIndex = 0

                    drpPrimaryServiceCenter.Enabled = False
                    drpSecondaryServiceCenter.Enabled = False

                    btnSave.Enabled = False
                    btnAddNew.Enabled = True
                    btnCancel.Enabled = False
                    btnSave.Text = "Save"
                    PopulateDataGrid()
                End If




            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub
        Private Function Valid() As Boolean
            If drpPrimaryServiceCenter.SelectedIndex = 0 Then
                ErrorLabel.Text = "Please select Primary Service Center"
                Return False
            End If
            If drpSecondaryServiceCenter.SelectedIndex = 0 Then
                ErrorLabel.Text = "Please select Secondary Service Center"
                Return False
            End If
            Return True
        End Function

        Protected Sub dgServiceCenter_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgServiceCenter.ItemDataBound
            Try
                Dim lnkbtn As LinkButton
                If (e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem) Then
                    lnkbtn = e.Item.FindControl("lnkBtnDelete")
                    If Not lnkbtn Is Nothing Then
                        lnkbtn.Attributes.Add("onclick", "javascript:ConfirmDelete();")
                    End If
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Sub dgServiceCenter_PageIndexChanged(ByVal sender As System.Object, ByVal e As DataGridPageChangedEventArgs) Handles dgServiceCenter.PageIndexChanged
            Try
                dgServiceCenter.CurrentPageIndex = e.NewPageIndex
                PopulateDataGrid()
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Sub PopulateServiceCenters()
            Dim objSIAMOperation As SIAMOperation = New SIAMOperation()
            Dim dsServiceCenterList As DataSet = objSIAMOperation.GetServiceCenterList()
            drpPrimaryServiceCenter.DataSource = dsServiceCenterList.Tables(0)
            drpPrimaryServiceCenter.DataTextField = "ADDRESS"
            drpPrimaryServiceCenter.DataValueField = "SERVICECENTERSID"
            drpPrimaryServiceCenter.DataBind()

            drpSecondaryServiceCenter.DataSource = dsServiceCenterList.Tables(0)
            drpSecondaryServiceCenter.DataTextField = "ADDRESS"
            drpSecondaryServiceCenter.DataValueField = "SERVICECENTERSID"
            drpSecondaryServiceCenter.DataBind()

            drpPrimaryServiceCenter.Items.Insert(0, "Select Primary Service Center")
            drpSecondaryServiceCenter.Items.Insert(0, "Select Secondary Service Center")
            drpPrimaryServiceCenter.Enabled = False
            drpSecondaryServiceCenter.Enabled = False

        End Sub

        Private Sub PopulateDataGrid()
            Dim objSIAMOperation As SIAMOperation = New SIAMOperation()
            Dim dsServiceCenterMapping As DataSet = objSIAMOperation.GET_PR_SEC_ServiceCenterMapping()

            dgServiceCenter.DataSource = dsServiceCenterMapping.Tables(0)
            dgServiceCenter.DataBind()
        End Sub
#End Region



#Region "       Helper Methods      "


        Private Sub writeScriptsToPage()
            '-- this script will make sure that this page is only accessed via the iform tag in default.aspx -         
            RegisterClientScriptBlock("redirect", "<script language=""javascript"">if ( parent.frames.length === 0 ){window.location.replace(""" + MyBase.EntryPointPage + """)}</script>")

            '-- this will clear the status bar on the browser 
            RegisterStartupScript("ClearStatus", "<script language=""javascript"">window.parent.status = ""Done."";</script>")

            RegisterStartupScript("RemoveApplication", "<script language=""javascript"">function DeleteOp(itemId){var ok = window.confirm(""Are you sure ?"");if (ok === true){document.location.href=""" + Page.Request.Url.Segments.GetValue(1).ToString() + "?Remove="" + itemId;}}</script>")
        End Sub

#End Region


        Protected Sub drpPrimaryServiceCenter_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles drpPrimaryServiceCenter.SelectedIndexChanged
            Try
                Dim dsDGDataTable As DataTable
                dsDGDataTable = dgServiceCenter.DataSource
                If Not dsDGDataTable Is Nothing Then
                    dsDGDataTable = dgServiceCenter.DataSource
                Else
                    PopulateDataGrid()
                    dsDGDataTable = dgServiceCenter.DataSource
                End If

                drpSecondaryServiceCenter.SelectedIndex = -1
                btnSave.Text = "Save"
                For Each dr As DataRow In dsDGDataTable.Rows
                    If dr("PRIMARYSERVICECENTERID").ToString() = drpPrimaryServiceCenter.SelectedValue Then
                        drpSecondaryServiceCenter.Items.FindByValue(dr("SECONDARYSERVICECENTERID")).Selected = True
                        lblExistingInfo.Text = "A mapping for selected primary service center is already exist. You can update with different secondary service center."
                        btnSave.Text = "Update"
                        Exit For
                    End If
                Next
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub
    End Class

End Namespace


