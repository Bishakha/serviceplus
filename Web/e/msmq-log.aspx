<%@ Reference Page="~/e/Utility.aspx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SIAMAdmin.msmq_log" CodeFile="msmq-log.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>MSMQ-LOG</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="includes/style.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body bottommargin="1" leftmargin="1" rightmargin="1" topmargin="1">
		<form id="Form1" method="post" runat="server">
		
			<asp:label id="Label4" style="Z-INDEX: 109; LEFT: 207px; POSITION: absolute; TOP: 82px" runat="server"
				Width="177px" CssClass="body">End Time(MM/DD/YYYY HH:mm:ss)</asp:label><asp:label id="Label1" style="Z-INDEX: 103; LEFT: 12px; POSITION: absolute; TOP: 35px" runat="server"
				CssClass="body">Label:</asp:label><asp:dropdownlist id="ddLabel" style="Z-INDEX: 104; LEFT: 70px; POSITION: absolute; TOP: 32px" runat="server"
				Width="104px" CssClass="body">
				<asp:ListItem Value="All">All</asp:ListItem>
				<asp:ListItem Value="Error">Error</asp:ListItem>
				<asp:ListItem Value="Verbose">Verbose</asp:ListItem>
				<asp:ListItem Value="Info">Info</asp:ListItem>
				<asp:ListItem Value="Warning">Warning</asp:ListItem>
			</asp:dropdownlist><asp:label id="Label2" style="Z-INDEX: 105; LEFT: 12px; POSITION: absolute; TOP: 60px" runat="server"
				Width="50px" CssClass="body">Text Filter:</asp:label><SPS:SPSTextBox id="txtFilter" style="Z-INDEX: 106; LEFT: 70px; POSITION: absolute; TOP: 56px"
				runat="server" Width="315px" CssClass="body"></SPS:SPSTextBox><asp:label id="Label3" style="Z-INDEX: 107; LEFT: 12px; POSITION: absolute; TOP: 83px" runat="server"
				Width="184px" CssClass="body">Begin Time(MM/DD/YYYY HH:mm:ss)</asp:label><SPS:SPSTextBox id="txtBeginTime" style="Z-INDEX: 108; LEFT: 12px; POSITION: absolute; TOP: 101px"
				runat="server" CssClass="body" Width="185px"></SPS:SPSTextBox><SPS:SPSTextBox id="txtEndTime" style="Z-INDEX: 110; LEFT: 207px; POSITION: absolute; TOP: 101px"
				runat="server" CssClass="body" Width="178px"></SPS:SPSTextBox><asp:datagrid id="gridLogMessages" style="Z-INDEX: 111; LEFT: 12px; POSITION: absolute; TOP: 151px"
				runat="server" Width="487px" OnPageIndexChanged="gridLogMessages_Paging" AutoGenerateColumns="False" OnSelectedIndexChanged="gridLogMessages_SelectedIndexChanged"
				Font-Size="Smaller" PageSize="8">
				<SelectedItemStyle BackColor="#8080FF"></SelectedItemStyle>
				<Columns>
					<asp:ButtonColumn Text="Select" HeaderText="Select" CommandName="Select"></asp:ButtonColumn>
					<asp:TemplateColumn HeaderText="Label">
						<ItemTemplate>
							<asp:Label runat="server" Text='<%# Format(DataBinder.Eval(Container, "DataItem.Label")) %>' ID="Label9">
							</asp:Label>
						</ItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Time">
						<ItemTemplate>
							<asp:Label runat="server" Text='<%# Format(DataBinder.Eval(Container, "DataItem.SentTime")) %>' ID="Label7">
							</asp:Label>
						</ItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Priority">
						<ItemTemplate>
							<asp:Label runat="server" Text='<%# Format(DataBinder.Eval(Container, "DataItem.Priority")) %>' ID="labelPriority">
							</asp:Label>
						</ItemTemplate>
					</asp:TemplateColumn>
				</Columns>
				<PagerStyle Mode="NumericPages"></PagerStyle>
			</asp:datagrid><asp:label id="Label5" style="Z-INDEX: 112; LEFT: 12px; POSITION: absolute; TOP: 129px" runat="server"
				Width="112px" CssClass="body">Log Messages:</asp:label><asp:label id="lbLogMessage" style="Z-INDEX: 113; LEFT: 13px; POSITION: absolute; TOP: 402px"
				runat="server" Width="483px" CssClass="body" EnableViewState="False"></asp:label><asp:button id="btnGet" style="Z-INDEX: 114; LEFT: 450px; POSITION: absolute; TOP: 100px" runat="server"
				Width="48px" CssClass="body" Text="Get"></asp:button><asp:label id="lbErrorText" style="Z-INDEX: 115; LEFT: 12px; POSITION: absolute; TOP: 11px"
				runat="server" Width="488px" CssClass="body" ForeColor="Red"></asp:label><asp:label id="Label6" style="Z-INDEX: 116; LEFT: 13px; POSITION: absolute; TOP: 379px" runat="server"
				Width="80px" CssClass="body">Log Message:</asp:label></form>
	</body>
</HTML>
