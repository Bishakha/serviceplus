﻿Imports Sony.US.SIAMUtilities
Partial Class e_CustomerLookUp
    Inherits System.Web.UI.UserControl

    Protected Sub cmMethod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmMethod.SelectedIndexChanged
        getShippingInformation.Visible = False
        getPayerDetails.Visible = False
        Select Case Me.cmMethod.Text
            Case "getPayerDetails"
                getPayerDetails.Visible = True
            Case "getShippingInformation"
                getShippingInformation.Visible = True

        End Select
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Sasikumar WP SPLUS_WP014
        If Not IsPostBack Then
            txtSalesOrganization.Text = ConfigurationData.GeneralSettings.GlobalData.SalesOrganization.US
            txtDistributionChannel.Text = ConfigurationData.GeneralSettings.GlobalData.DistributionChannel.Value
            txtDivision.Text = ConfigurationData.GeneralSettings.GlobalData.Division.Value
        End If
    End Sub
End Class
