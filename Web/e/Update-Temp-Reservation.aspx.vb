Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Data
Imports Sony.US.ServicesPLUS.Process
Imports System.Globalization
Imports Sony.US.ServicesPLUS.Controls
Imports System.Data
Imports ServicesPlusException
Imports Sony.US.AuditLog
Namespace SIAMAdmin
    Partial Class UpdateTempReservation
        Inherits UtilityClass
        'Dim objUtilties As Utilities = New Utilities

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Protected WithEvents txtCourseNumber As SPSTextBox


        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            MyBase.IsProtectedPage(True, True)
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                ErrorLabel.Text = String.Empty
                If Not IsPostBack Then
                    PopulateCourseLocationDropDown()
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Sub btnUpdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
            If Not ValidateForm() Then
                ErrorLabel.Text = "Error entering class info: " + ErrorLabel.Text
                Return
            Else
                Dim objPCILogger As New PCILogger() '6524
                Try
                    '6524 start
                    objPCILogger.EventOriginApplication = "ServicesPLUS"
                    objPCILogger.EventOriginApplicationLocation = Me.Page.GetType().Name
                    objPCILogger.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
                    objPCILogger.OperationalUser = Environment.UserName
                    objPCILogger.EventDateTime = DateTime.Now.ToLongTimeString()
                    objPCILogger.HTTPRequestObjectValues = objPCILogger.GetRequestContextValue()
                    objPCILogger.IndicationSuccessFailure = "Success"
                    objPCILogger.EventOriginMethod = "btnUpdate_Click"
                    objPCILogger.Message = "UpdateTempTrainingReservation Success."
                    objPCILogger.EventType = EventType.Update
                    '6524 end

                    Dim strReservation As String = String.Empty
                    Dim intReturnSequence As String = String.Empty
                    Dim chkSelectValue As CheckBox
                    For Each dgItem As DataGridItem In dgReservation.Items
                        If dgItem.ItemType = ListItemType.Item Or dgItem.ItemType = ListItemType.AlternatingItem Then
                            chkSelectValue = dgItem.FindControl("chkSelect")
                            If chkSelectValue.Checked Then
                                If strReservation = String.Empty Then
                                    strReservation = dgItem.Cells(1).Text.ToString()
                                Else
                                    strReservation = strReservation + "~" + dgItem.Cells(1).Text.ToString()
                                End If
                            End If
                        End If
                    Next

                    If strReservation <> String.Empty Then
                        Dim objCourseManager As CourseManager = New CourseManager()
                        intReturnSequence = objCourseManager.UpdateTempTrainingReservation(ddlTrainingClass.SelectedItem.Text, ddlTrainingCourse.SelectedItem.Text, ddlLocation.SelectedValue, strReservation)
                    End If
                    SearchTempClass()




                Catch ex As Exception
                    ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                    objPCILogger.IndicationSuccessFailure = "Failure" '6524
                    objPCILogger.Message = "UpdateTempTrainingReservation Failed. " & ex.Message.ToString() '6524
                Finally
                    objPCILogger.PushLogToMSMQ() '6524
                End Try
            End If
        End Sub


        Private Function ValidateForm() As Boolean
            Dim bValid = True

            lblClass.CssClass = "BodyCopy"
            If ddlTrainingClass.Items.Count > 0 Then
                If Me.ddlTrainingClass.SelectedItem.Text = "Select Class" Then
                    lblClass.CssClass = "redAsterick"
                    bValid = False
                End If
            Else
                ErrorLabel.Text = "Search for the training class first."
                bValid = False
            End If

            Return bValid
        End Function

        Private Sub PopulateCourseLocationDropDown()
            Dim objCourseManager As CourseManager
            Try
                objCourseManager = New CourseManager()
                Dim courseData As DataSet = objCourseManager.GetClassCoursesAndLocation()
                ddlTrainingCourse.DataValueField = "Coursenumber"
                ddlTrainingCourse.DataTextField = "Coursenumber"
                ddlTrainingCourse.DataSource = courseData.Tables(0)
                ddlTrainingCourse.DataBind()

                ddlLocation.DataValueField = "LocationCode"
                ddlLocation.DataTextField = "LocationName"
                ddlLocation.DataSource = courseData.Tables(1)
                ddlLocation.DataBind()

                dgReservation.DataSource = Nothing
                dgReservation.DataBind()




            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            Finally
                objCourseManager = Nothing
            End Try
        End Sub

        Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
            Try
                ddlTrainingClass.Items.Clear()
                SearchTempClass()
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub
        Private Sub SearchTempClass()
            Dim objCourseManager As CourseManager
            Try
                ErrorLabel.Text = ""
                objCourseManager = New CourseManager()
                Dim dtCertificateData As DataSet = objCourseManager.GetTempReservationAndClass(ddlTrainingCourse.SelectedItem.Text, ddlLocation.SelectedValue)
                If dtCertificateData.Tables(0).Rows.Count > 0 Then
                    ddlTrainingClass.DataValueField = "PartNumber"
                    ddlTrainingClass.DataTextField = "PartNumber"
                    ddlTrainingClass.DataSource = dtCertificateData.Tables(0)
                    ddlTrainingClass.DataBind()
                Else
                    ErrorLabel.Text = "No actual class available for this course and location. "
                End If

                If dtCertificateData.Tables(1).Rows.Count <= 0 Then
                    ErrorLabel.Text = ErrorLabel.Text + "<br/>No temporary reservation available for this course and location."
                End If
                dgReservation.DataSource = dtCertificateData.Tables(1)
                dgReservation.DataBind()





            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            Finally
                objCourseManager = Nothing
            End Try
        End Sub

        Protected Sub btnDeleteReservation_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDeleteReservation.Click
            Dim objPCILogger As New PCILogger() '6524
            Try

                '6524 start
                objPCILogger.EventOriginApplication = "ServicesPLUS"
                objPCILogger.EventOriginApplicationLocation = Me.Page.GetType().Name
                objPCILogger.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
                objPCILogger.OperationalUser = Environment.UserName
                objPCILogger.EventDateTime = DateTime.Now.ToLongTimeString()
                objPCILogger.HTTPRequestObjectValues = objPCILogger.GetRequestContextValue()
                objPCILogger.IndicationSuccessFailure = "Success"
                objPCILogger.EventOriginMethod = "btnDeleteReservation_Click"
                objPCILogger.Message = "DeleteTempTrainingReservation Success."
                objPCILogger.EventType = EventType.Delete
                '6524 end

                Dim strReservation As String = String.Empty
                Dim intReturnSequence As String = String.Empty
                Dim chkSelectValue As CheckBox
                For Each dgItem As DataGridItem In dgReservation.Items
                    If dgItem.ItemType = ListItemType.Item Or dgItem.ItemType = ListItemType.AlternatingItem Then
                        chkSelectValue = dgItem.FindControl("chkSelect")
                        If chkSelectValue.Checked Then
                            If strReservation = String.Empty Then
                                strReservation = dgItem.Cells(1).Text.ToString()
                            Else
                                strReservation = strReservation + "~" + dgItem.Cells(1).Text.ToString()
                            End If
                        End If
                    End If
                Next

                If strReservation <> String.Empty Then
                    Dim objCourseManager As CourseManager = New CourseManager()
                    intReturnSequence = objCourseManager.DeleteTempTrainingReservation(ddlTrainingCourse.SelectedItem.Text, ddlLocation.SelectedValue, strReservation)
                    ErrorLabel.Text = "Selected reservation deleted."
                End If
                SearchTempClass()




            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                objPCILogger.IndicationSuccessFailure = "Failure" '6524
                objPCILogger.Message = "DeleteTempTrainingReservation Failed. " & ex.Message.ToString() '6524
            Finally
                objPCILogger.PushLogToMSMQ() '6524
            End Try
        End Sub
    End Class

End Namespace
