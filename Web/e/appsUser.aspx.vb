Imports System.Web.UI
Imports Sony.US.ServicesPLUS.Process
Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Data
Imports System.Web.UI.WebControls
Imports Sony.US.siam, Sony.US.ServicesPLUS.Controls
Imports System.Text.RegularExpressions
Imports System.Xml
Imports System.Drawing
Imports Sony.US.AuditLog
Imports ServicesPlusException
Imports Sony.US.SIAMUtilities '6668
Imports System.Data

Namespace SIAMAdmin

    Partial Class appsUser
        Inherits UtilityClass
        'Dim objUtilties As Utilities = New Utilities
        Protected Sub Page_Init1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
            MyBase.IsProtectedPage(True, True)
        End Sub

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                If Not IsPostBack Then
                    cmdUpdate.Visible = False
                    PopulatecountryDDL()
                    PopulateStatesDDL()
                    PopulateRole()
                    PopulateUserType()

                    'Added following code for Bug # 6754, to prevent creation of InActive Users.
                    ddlUsrStatus.SelectedIndex = 1
                    ddlUsrStatus.Enabled = False
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Sub PopulateStatesDDL()
            Dim cm As New CatalogManager

            Try
                Dim data_store As New Sony.US.ServicesPLUS.Data.SISDataManager
                Dim statedetail As DataSet = data_store.GetStateDetailsByCountry(ddlCountry.SelectedItem.Text)

                ddlStates.Items.Clear()
                ddlStates.Items.Add(New ListItem("Select One", ""))

                If statedetail.Tables(0).Rows.Count > 0 Then
                    For i As Integer = 0 To statedetail.Tables(0).Rows.Count - 1
                        ddlStates.Items.Add(New ListItem(statedetail.Tables(0).Rows(i)("STATE"), statedetail.Tables(0).Rows(i)("ABBREVIATION")))
                    Next
                End If

            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try

        End Sub
        Private Sub PopulatecountryDDL()

            Try
                'Fetch from DB 
                Dim data_store As New Sony.US.ServicesPLUS.Data.SISDataManager
                'Dim countryDetail() As CountryDetail
                Dim countrydetail As DataSet = data_store.GetCountryDetails()
                countrydetail = data_store.GetCountryDetails()

                If countrydetail.Tables(0).Rows.Count > 0 Then
                    With countrydetail.Tables(0)
                        For i As Integer = 0 To countrydetail.Tables(0).Rows.Count - 1
                            ddlCountry.Items.Add(New ListItem(countrydetail.Tables(0).Rows(i)("COUNTRYNAME"), countrydetail.Tables(0).Rows(i)("COUNTRYCODE")))
                        Next
                    End With
                End If

                'bind card type array to dropdown list control   
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try

        End Sub
        Private Sub PopulateRole()
            Dim oUsrSecMgr As New UserRoleSecurityManager
            Dim user_role() As Sony.US.ServicesPLUS.Core.UserRole = oUsrSecMgr.GetUserRoles("%", "1")
            Dim iUsrRole As Sony.US.ServicesPLUS.Core.UserRole

            Dim newListItem1 As New ListItem()
            For Each iUsrRole In user_role
                Dim newListItem As New ListItem()
                newListItem.Text = ChangeCase(iUsrRole.RoleName)
                newListItem.Value = iUsrRole.RoleID
                lstRole.Items.Add(newListItem)
            Next
        End Sub

        Private Sub PopulateUserType()
            'Dim oUsrSecMgr As New UserRoleSecurityManager'6668
            'Dim user_type() As UserType = oUsrSecMgr.GetUserStatus()'6668
            'Dim iUsrType As UserType'6668

            Dim newListItem1 As New ListItem()
            newListItem1.Text = "Select One"
            newListItem1.Value = ""
            ddlUsrStatus.Items.Add(newListItem1)
            ddlUsrStatus.Items.Add(ConfigurationData.GeneralSettings.StatusCodes.Active) '6668
            ddlUsrStatus.Items.Add(ConfigurationData.GeneralSettings.StatusCodes.Inactive) '6668
            '6668 starts
            'For Each iUsrType In user_type
            '    If ChangeCase(iUsrType.StatusName) <> "Pending" Then
            '        Dim newListItem As New ListItem()
            '        newListItem.Text = ChangeCase(iUsrType.StatusName)
            '        newListItem.Value = iUsrType.StatusID
            '        ddlUsrStatus.Items.Add(newListItem)
            '    End If
            'Next
            '6668 ends
            ddlUsrStatus.Items.Add(New ListItem("Active", 1))
        End Sub

        Private Function ChangeCase(ByVal pString As String) As String
            Dim str As String = pString.ToLower()
            Return System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(str)
        End Function

        Private Function CheckUserInputs() As Boolean

            Dim iRet As Boolean = False
            Dim thisPattern As String = "^[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$"
            ' Dim thisPattern As String = "\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
            'Dim thisPattern As String = "^[a-z|A-Z][a-z|A-Z]*[\-|\.]*[a-z|A-Z][a-z|A-Z]*$" 'added for fixing bug# 160

            LabelLine1Star.Text = ""
            LabelLine2Star.Text = ""
            LabelLine3Star.Text = ""
            LabelLine4Star.Text = ""

            ErrorLabel.Text = ""
            If txtFirstName.Text.Length > 0 Then
                lblFirstName.ForeColor = Color.Black
                iRet = True

            Else
                lblFirstName.ForeColor = Color.Red
                ErrorLabel.Text = "Please enter valid First Name"
                Return False
            End If

            If txtLastName.Text.Length > 0 Then
                iRet = True
                lblLastName.ForeColor = Color.Black
            Else
                lblLastName.ForeColor = Color.Red
                ErrorLabel.Text = "Please enter valid Last Name"
                Return False
            End If

            If txtCompName.Text.Length > 0 Then
                iRet = True
                lblCompName.ForeColor = Color.Black
            Else
                lblCompName.ForeColor = Color.Red
                ErrorLabel.Text = "Please enter valid Company Name"
                Return False
            End If

            If txtAddr1.Text.Length > 0 Then
                iRet = True
                lblAddr.ForeColor = Color.Black
            Else
                lblAddr.ForeColor = Color.Red
                ErrorLabel.Text = "Please enter valid Address line1"
                Return False
            End If

            If txtAddr1.Text.Length > 35 Then
                lblAddr.ForeColor = Color.Red
                LabelLine1Star.Text = "Street Address must be 35 characters or less in length"
                Return False
            Else
                iRet = True
                lblAddr.ForeColor = Color.Black
            End If

            If txtAddr2.Text.Length > 35 Then
                lblAddr2.ForeColor = Color.Red
                LabelLine2Star.Text = "Address 2nd Line must be 35 characters or less in length"
                Return False
            Else
                iRet = True
                lblAddr2.ForeColor = Color.Black
            End If

            If txtAddr3.Text.Length > 35 Then
                lblAddr3.ForeColor = Color.Red
                LabelLine3Star.Text = "Address 3rd Line must be 35 characters or less in length"
                Return False
            Else
                iRet = True
                lblAddr3.ForeColor = Color.Black
            End If

            If txtCity.Text.Length > 0 Then
                iRet = True
                lblCity.ForeColor = Color.Black
            Else
                lblCity.ForeColor = Color.Red
                ErrorLabel.Text = "Please enter valid City"
                Return False
            End If

            If txtCity.Text.Length > 35 Then
                lblCity.ForeColor = Color.Red
                LabelLine4Star.Text = "City must be 35 characters or less in length."
                txtCity.Focus()
                Return False
            Else
                iRet = True
                lblCity.ForeColor = Color.Black
            End If

            If ddlStates.SelectedIndex > 0 Then
                iRet = True
                lblState.ForeColor = Color.Black
            Else
                lblState.ForeColor = Color.Red
                ErrorLabel.Text = "Please choose State"
                Return False
            End If

            'If ddlStates.SelectedIndex > 0 Then
            '    iRet = True
            '    lblState.ForeColor = Color.Black
            'Else
            '    lblState.ForeColor = Color.Red
            '    ErrorLabel.Text = "Please choose State"
            '    Return False
            'End If

            '6750 starts
            If txtZip.Text.Trim().Length > 0 Then
                'If Not IsNumeric(txtZip.Text.Trim()) Then
                '    ErrorLabel.Text = "Postal Code field can have only numeric value"
                '    Return False
                'End If

                Dim sZip As String = txtZip.Text.Trim()
                Dim objZipCodePattern As New Regex("\d{5}(-\d{4})?")
                If ddlCountry.SelectedValue = ConfigurationData.GeneralSettings.GlobalData.SalesOrganization.CA.Substring(0, 2) Then
                    objZipCodePattern = New Regex("^[ABCEGHJKLMNPRSTVXY]{1}\d{1}[A-Z]{1} *\d{1}[A-Z]{1}\d{1}$")
                End If

                If Not objZipCodePattern.IsMatch(sZip) Then
                    ErrorLabel.Text = "Invalid Postal/Zip Code format"
                    Return False
                End If

                If ddlCountry.SelectedValue = ConfigurationData.GeneralSettings.GlobalData.SalesOrganization.US.Substring(0, 2) Then
                    If txtZip.Text.Length < Convert.ToInt32(ConfigurationData.GeneralSettings.GlobalData.ZipCode.US) Then
                        lblZip.ForeColor = Color.Red
                        ErrorLabel.Text = "Please enter valid Postal Code"
                        Return False
                    Else
                        iRet = True
                        lblZip.ForeColor = Color.Black
                    End If
                ElseIf txtZip.Text.Length < Convert.ToInt32(ConfigurationData.GeneralSettings.GlobalData.ZipCode.CA) Then
                    lblZip.ForeColor = Color.Red
                    ErrorLabel.Text = "Please enter valid Postal Code"
                    Return False
                Else
                    iRet = True
                    lblZip.ForeColor = Color.Black
                End If

            Else
                lblZip.ForeColor = Color.Red
                ErrorLabel.Text = "Please enter valid Postal Code"
                Return False
            End If
            '6750 ends

            If txtPhone.Text.Length > 0 Then
                iRet = True
                lblPhone.ForeColor = Color.Black
            Else
                lblPhone.ForeColor = Color.Red
                ErrorLabel.Text = "Please enter valid Phone number"
                Return False
            End If
            If txtExt.Text.Trim().Length > 0 Then
                If Not IsNumeric(txtExt.Text.Trim()) Then
                    ErrorLabel.Text = "Extension field can have only numeric value"
                    Return False
                End If
            End If

            'Start Modification for E754
            'If txtEmail.Text.Length > 0 And Regex.IsMatch(Me.txtEmail.Text, thisPattern) Then 'Changed for E787
            If txtEmail.Text.Length > 0 Then
                'Dim emailAddr As String = txtEmail.Text.Trim() + lblEmailSuffix.Text.Trim()
                If Regex.IsMatch(Me.txtEmail.Text, thisPattern) Then
                    iRet = True
                    lblEmail.ForeColor = Color.Black
                Else
                    lblEmail.ForeColor = Color.Red
                    ErrorLabel.Text = "Please enter valid email address"
                    Return False
                End If
            Else
                lblEmail.ForeColor = Color.Red
                ErrorLabel.Text = "Please enter valid email address"
                Return False
            End If
            'End Modification for E754

            'Start Modification for E764
            ' If txtUsrName.Text.Length = 10 And IsNumeric(txtUsrName.Text.Trim()) Then
            If txtUsrName.Text.Length = 10 Then
                iRet = True
                lblUserName.ForeColor = Color.Black
            Else
                lblUserName.ForeColor = Color.Red
                ErrorLabel.Text = "Please enter valid user name,which is a 10-digit Global ID"
                Return False
            End If

            If lstRole.Items.Count <> 0 Then
                iRet = True
                lblStatus.ForeColor = Color.Black
            Else
                lblRole.ForeColor = Color.Red
                ErrorLabel.Text = "Please choose Role"
                Return False
            End If

            Dim index As Integer
            Dim iSelCount As Integer = 0

            For index = 0 To lstRole.Items.Count - 1
                If lstRole.Items(index).Selected Then
                    iSelCount = iSelCount + 1
                End If
            Next

            If iSelCount > 0 Then
                iRet = True
                lblRole.ForeColor = Color.Black
            Else
                lblRole.ForeColor = Color.Red
                ErrorLabel.Text = "Please choose Role"
                Return False
            End If

            If ddlUsrStatus.SelectedIndex > 0 Then
                iRet = True
                lblStatus.ForeColor = Color.Black
            Else
                lblStatus.ForeColor = Color.Red
                ErrorLabel.Text = "Please choose user status"
                Return False
            End If
            Return iRet
        End Function

        Private Sub ClearFields()
            cmdSave.Text = "  Save  "
            cmdSave.ToolTip = "Click to save user" 'Added for E785
            txtFirstName.Text = ""
            txtLastName.Text = ""
            txtCompName.Text = ""
            txtAddr1.Text = ""
            txtAddr2.Text = "" 'Added for E786
            txtAddr3.Text = ""
            txtCity.Text = ""
            ddlStates.SelectedIndex = 0
            txtZip.Text = ""
            txtPhone.Text = ""
            txtExt.Text = "" 'Added for E786
            txtFax.Text = "" 'Added for E786
            txtEmail.Text = ""
            txtUsrName.Text = ""
            'ddlRole.SelectedIndex = 0
            'ddlUserType.SelectedIndex = 0

            'Commented for bug # 6814
            'ddlUsrStatus.SelectedIndex = 0 'Added for E786

            cmdUpdate.Visible = False

            Dim index As Integer
            Dim iSelCount As Integer = 0

            For index = 0 To lstRole.Items.Count - 1
                If lstRole.Items(index).Selected Then
                    lstRole.Items(index).Selected = False
                End If
            Next

        End Sub

        Private Sub EnableFields()
            txtFirstName.Enabled = True
            txtLastName.Enabled = True
            txtCompName.Enabled = True
            txtAddr1.Enabled = True
            txtAddr2.Enabled = True
            txtAddr3.Enabled = True
            txtCity.Enabled = True
            ddlStates.Enabled = True
            ddlCountry.Enabled = True
            txtZip.Enabled = True
            txtPhone.Enabled = True
            txtExt.Enabled = True
            txtFax.Enabled = True
            txtEmail.Enabled = True
            txtUsrName.Enabled = True
            txtUserType.Enabled = True
            lstRole.Enabled = True
        End Sub
        Private Sub DisableFields()
            txtFirstName.Enabled = False
            txtLastName.Enabled = False
            txtCompName.Enabled = False
            txtAddr1.Enabled = False
            txtAddr2.Enabled = False
            txtAddr3.Enabled = False
            txtCity.Enabled = False
            ddlStates.Enabled = False
            ddlCountry.Enabled = False
            txtZip.Enabled = False
            txtPhone.Enabled = False
            txtExt.Enabled = False
            txtFax.Enabled = False
            txtEmail.Enabled = False
            txtUsrName.Enabled = False
            txtUserType.Enabled = False
            lstRole.Enabled = False
        End Sub

        Protected Sub cmdSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdSave.Click
            If cmdSave.Text = "  New  " Then
                EnableFields()
                ClearFields()
                Exit Sub
            End If

            Dim oUsrSecMgr As New UserRoleSecurityManager
            Dim iRet As Integer = 0

            Dim sa As New SecurityAdministrator
            Dim oUser As New Sony.US.siam.User
            Dim ctDate As Date = Date.Now.AddMonths(3)
            Dim customer As Customer 'E776
            'Dim siamResponse As Sony.US.siam.AddUserResponse 'E776
            Dim custmgr As New CustomerManager() 'E776

            If CheckUserInputs() = True Then
                Dim objPCILogger As New PCILogger()
                Try
                    'prasad added on 18-12-2009
                    'Event logging code added. BugId #2100
                    objPCILogger.EventOriginApplicationLocation = Me.Page.GetType().Name
                    objPCILogger.EventOriginApplication = "ServicesPLUS Admin"
                    objPCILogger.EventOriginMethod = "cmdSave_Click"
                    objPCILogger.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
                    objPCILogger.OperationalUser = Environment.UserName
                    objPCILogger.EventType = EventType.Add
                    If Session.Item("siamuser") IsNot Nothing Then
                        Dim user As User = DirectCast(Session.Item("siamuser"), User)
                        objPCILogger.EmailAddress = user.EmailAddress
                        objPCILogger.CustomerID = user.CustomerID
                        objPCILogger.CustomerID_SequenceNumber = user.SequenceNumber
                        objPCILogger.SIAM_ID = user.Identity
                        objPCILogger.LDAP_ID = user.AlternateUserId
                    End If
                    objPCILogger.EventDateTime = DateTime.Now.ToLongTimeString()
                    objPCILogger.HTTPRequestObjectValues = GetRequestContextValue()

                    oUser.EmailAddress = txtEmail.Text.Trim()
                    oUser.FirstName = txtFirstName.Text.Trim()
                    oUser.LastName = txtLastName.Text.Trim()
                    oUser.UserId = txtUsrName.Text.Trim()

                    customer = New Customer With {
                        .FirstName = txtFirstName.Text.Trim(),
                        .LastName = txtLastName.Text.Trim(),
                        .EmailAddress = txtEmail.Text.Trim(),
                        .CompanyName = txtCompName.Text.Trim(),
                        .CountryCode = ddlCountry.SelectedValue.Trim(),
                        .PhoneNumber = txtPhone.Text.Trim(),
                        .PhoneExtension = txtExt.Text.Trim(),
                        .FaxNumber = txtFax.Text.Trim(),
                        .EmailOk = False,
                        .SIAMIdentity = oUser.Identity,
                        .UserName = txtUsrName.Text.Trim(),
                        .LockStatus = 0,
                        .Password = "notused1",
                        .StatusCode = 1,
                        .UserType = "I",
                        .PasswordExpirationDate = Convert.ToString(ctDate),
                        .LdapID = txtUsrName.Text.Trim(),
                        .Address = New Sony.US.ServicesPLUS.Core.Address() With {
                            .Line1 = txtAddr1.Text.Trim(),
                            .Line2 = txtAddr2.Text.Trim(),
                            .Line3 = txtAddr3.Text.Trim(),
                            .City = txtCity.Text.Trim(),
                            .State = ddlStates.SelectedValue.Trim(),
                            .PostalCode = txtZip.Text.Trim()
                        }
                    }

                    ' call siam here to find out if the user id is a duplicate
                    If (Not New SecurityAdministrator().IsDuplicateUser(oUser)) Then
                        Dim cm As New CustomerManager
                        cm.RegisterCustomer(customer)
                        Try
                            Dim index As Integer
                            For index = 0 To lstRole.Items.Count - 1
                                If lstRole.Items(index).Selected Then
                                    iRet = oUsrSecMgr.AddUserRoleMap((customer.SIAMIdentity), Convert.ToInt32(lstRole.Items(index).Value))
                                End If
                            Next
                            'iRet = oUsrSecMgr.AddUserRoleMap(Convert.ToDecimal(oUser.Identity), Convert.ToInt32(ddlRole.SelectedValue))
                            ErrorLabel.Text = "User added successfully."
                            cmdUpdate.Visible = True
                            DisableFields()
                            hdnSIAMIdentity.Value = customer.SIAMIdentity
                            cmdUpdate.Visible = True
                            objPCILogger.IndicationSuccessFailure = "Success"
                            ' modified by Manish for bug - 2194
                            'objPCILogger.Message = "Employee/Worker with UserName " & objPCILogger.UserName & " successfully added Employee/Worker UserName " & customer.UserName & " CustomerID " & customer.CustomerID & " SIAM_ID " & customer.SIAMIdentity
                            'objPCILogger.Message = "Employee/Worker with EmailAddress " & objPCILogger.EmailAddress & " successfully added Employee/Worker EmailAddress " & customer.EmailAddress & " CustomerID " & customer.CustomerID & " SIAM_ID " & customer.SIAMIdentity'6524
                            objPCILogger.Message = "Employee/Worker with EmailAddress " & objPCILogger.EmailAddress & " successfully added Employee/Worker EmailAddress " & customer.EmailAddress & " CustomerID " & customer.CustomerID & " SIAM_ID " & customer.SIAMIdentity & " LDAP_ID " & customer.LdapID '6524


                            cmdSave.Text = "  New  "
                            cmdSave.ToolTip = "Click to create new user" 'Added for E785
                        Catch ex As Exception
                            ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                            objPCILogger.IndicationSuccessFailure = "Failure"
                            objPCILogger.Message = ex.Message.ToString()
                        Finally
                            objPCILogger.PushLogToMSMQ()
                        End Try
                        'End If
                    Else
                        ErrorLabel.Text = "Registration has encountered an error. You have attempted to register with a duplicate Email Address or Global ID. Please check your information."
                    End If
                Catch ex As Exception
                    ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                    objPCILogger.IndicationSuccessFailure = "Failure"
                    objPCILogger.Message = ex.Message.ToString()
                Finally
                    objPCILogger.PushLogToMSMQ()
                End Try


            End If 'E776
        End Sub

        Protected Sub cmdUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdUpdate.Click
            If hdnSIAMIdentity.Value.Trim() <> "" Then
                Response.Redirect("modifyUser.aspx?siamid=" & hdnSIAMIdentity.Value)
            End If
        End Sub

        Protected Sub ddlCountry_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCountry.SelectedIndexChanged

            Try
                PopulateStatesDDL()
                ZipCode()

            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Sub ZipCode()

            If ddlCountry.SelectedValue = ConfigurationData.GeneralSettings.GlobalData.SalesOrganization.US.Substring(0, 2) Then
                lblZip.Text = "Zip Code"
                txtZip.MaxLength = Convert.ToInt32(ConfigurationData.GeneralSettings.GlobalData.ZipCode.US)
                txtZip.Text = ""
            Else
                lblZip.Text = "Postal Code"
                txtZip.MaxLength = Convert.ToInt32(ConfigurationData.GeneralSettings.GlobalData.ZipCode.CA)
                txtZip.Text = ""
            End If

        End Sub
    End Class
End Namespace