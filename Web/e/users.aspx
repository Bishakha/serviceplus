<%@ Reference Page="~/e/Utility.aspx" %>

<%@ Page Language="vb" AutoEventWireup="false" Inherits="SIAMAdmin.users" CodeFile="users.aspx.vb"
    CodeFileBaseClass="SIAMAdmin.UtilityClass" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>users</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <meta http-equiv="X-UA-Compatible" content="IE=5" />
    <link href="includes/style.css" type="text/css" rel="stylesheet">

    <script type="text/javascript">
        function ConfirmDelete() {
            window.event.returnValue = confirm("This activity will delete selected user...");
        }
    </script>

    <style type="text/css">
        Body {
            filter: chroma(color=#FFFFFF);
            scrollbar-face-color: #c9d5e6;
            scrollbar-shadow-color: #632984;
            scrollbar-highlight-color: #632984;
            scrollbar-3dlight-color: #130919;
            scrollbar-darkshadow-color: #130919;
            scrollbar-track-color: #130919;
            scrollbar-arrow-color: black;
        }
    </style>
</head>
<body>
    <form id="Form1" method="post" runat="server">
    <table width="100%" align="center" border="0">
        <tr>
            <td class="PageHead" align="center" height="30">
                <asp:Label ID="lblHeader" runat="server" Text="Find ServicesPLUS User">Find ServicesPLUS User</asp:Label>
            </td>
        </tr>
        <tr>
            <td height="5">
                &nbsp;
                <asp:Label ID="ErrorLabel" runat="server" CssClass="body" ForeColor="Red" EnableViewState="False"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <table border="0" width="100%" align="center">
                    <tr>
                        <td align="center">
                            <div id="div1" style="border-right: #c9d5e6 thin solid; width: 80%; border-top: #c9d5e6 thin solid;
                                border-left: #c9d5e6 thin solid; border-bottom: #c9d5e6 thin solid">
                                <table width="100%" cellspacing="0">
                                    <tr bgcolor="#c9d5e6" height="20px">
                                        <td width="25%">
                                            <asp:Label ID="NameLabel" CssClass="BodyHead" runat="server">First Name</asp:Label>
                                        </td>
                                        <td width="25%">
                                            <asp:Label ID="Label2" runat="server" CssClass="BodyHead">Last Name</asp:Label>
                                        </td>
                                        <td width="25%">
                                            <asp:Label ID="EmailLabel" runat="server" CssClass="BodyHead">Email</asp:Label>
                                        </td>
                                        <td width="25%">
                                            <asp:Label ID="CompanyLabel" runat="server" CssClass="BodyHead">Company</asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="25%">
                                            <SPS:SPSTextBox ID="NameTextBox" runat="server" CssClass="Body"></SPS:SPSTextBox>
                                        </td>
                                        <td width="25%">
                                            <SPS:SPSTextBox ID="txtLastName" runat="server" CssClass="Body"></SPS:SPSTextBox>
                                        </td>
                                        <td width="25%">
                                            <SPS:SPSTextBox ID="EmailTextBox" runat="server" CssClass="Body"></SPS:SPSTextBox>
                                        </td>
                                        <td width="25%">
                                            <SPS:SPSTextBox ID="CompanyTextBox" runat="server" CssClass="Body"></SPS:SPSTextBox>
                                        </td>
                                    </tr>
                                    <tr bgcolor="#c9d5e6" height="20px">
                                        <td width="25%">
                                            <asp:Label ID="SIAMUserID" runat="server" CssClass="BodyHead">SIAM User ID</asp:Label>
                                        </td>
                                        <td width="25%">
                                            <asp:Label ID="lblUserID" CssClass="BodyHead" runat="server">User Name</asp:Label>
                                        </td>
                                        <td width="25%">
                                            <asp:Label ID="lblSubscriptionID" runat="server" CssClass="BodyHead">Subscription ID</asp:Label>
                                        </td>
                                        <td width="25%">
                                            <asp:Label ID="lblLDAPID" runat="server" CssClass="BodyHead">LDAP ID</asp:Label>
                                        </td>
                                        <td width="25%">
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="25%">
                                            <SPS:SPSTextBox ID="txtSiamId" runat="server" CssClass="Body" MaxLength="16"></SPS:SPSTextBox>
                                        </td>
                                        <td width="25%">
                                            <SPS:SPSTextBox ID="txtUserID" runat="server" CssClass="Body"></SPS:SPSTextBox>
                                        </td>
                                        <td width="25%">
                                            <SPS:SPSTextBox ID="txtSubscriptionID" runat="server" CssClass="Body"></SPS:SPSTextBox>
                                        </td>
                                        <td width="25%">
                                            <SPS:SPSTextBox ID="txtLDAPID" runat="server" CssClass="Body"></SPS:SPSTextBox>
                                        </td>
                                        <td width="25%">
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr bgcolor="#c9d5e6" height="20px">
                                        <td width="25%">
                                            <asp:Label ID="Label3" runat="server" CssClass="BodyHead">Status</asp:Label>
                                        </td>
                                        <td width="25%">
                                        </td>
                                        <td width="25%">
                                        </td>
                                        <td width="25%">
                                        </td>
                                        <td width="25%">
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="25%">
                                            <SPS:SPSDropDownList ID="ddlUserStatus" runat="server" CssClass="Body" Width="130">
                                                <asp:ListItem>Active</asp:ListItem>
                                                <asp:ListItem>Inactive</asp:ListItem>
                                                <asp:ListItem>Active and Inactive</asp:ListItem>
                                            </SPS:SPSDropDownList>
                                        </td>
                                        <td width="25%">
                                        </td>
                                        <td width="25%">
                                        </td>
                                        <td width="25%">
                                        </td>
                                        <td width="25%">
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr bgcolor="#c9d5e6">
                                        <td width="25%" colspan="4">
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" align="left" width="25%">
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" align="left" width="25%">
                                            <asp:Button ID="SearchButton" runat="server" CssClass="Body" Text="Search"></asp:Button>
                                            &nbsp;&nbsp;&nbsp;
                                            <asp:Button ID="ShowAllUsersButton" runat="server" CssClass="Body" Text="Show All Users">
                                            </asp:Button>
                                            &nbsp;&nbsp;
                                            <asp:Button ID="AddNewUserButton" runat="server" CssClass="Body" Text="Add New User">
                                            </asp:Button>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            &nbsp;
                        </td>
                    </tr>
                    <tr bgcolor="#c9d5e6" height="20px">
                        <td align="left">
                            &nbsp;<asp:Label ID="Label1" runat="server" CssClass="bodyCopy" Visible="False" Text="Search Result">ServicesPLUS User</asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:DataGrid ID="DataGrid1" runat="server" Width="100%" AutoGenerateColumns="False">
                                <Columns>
                                    <asp:TemplateColumn SortExpression="Identity" HeaderText="SIAM UserID">
                                        <ItemTemplate>
                                            <%--<asp:HyperLink runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Identity") %>' NavigateUrl='<%# thisPage + "?Identity="+DataBinder.Eval(Container.DataItem, "Identity").ToString()%>' ID="Hyperlink1">
																</asp:HyperLink>--%><%--7086--%>
                                            <asp:Label ID="lbldgsiamid" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Identity") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="username" SortExpression="username" HeaderText="User Name">
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="Name" SortExpression="Name" HeaderText="Name"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="EmailAddress" SortExpression="EmailAddress" HeaderText="Email Address">
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="Company" SortExpression="Company" HeaderText="Company">
                                    </asp:BoundColumn>
                                    <%--<asp:BoundColumn DataField="LdapId" SortExpression="LdapId" HeaderText="LDAP ID"></asp:BoundColumn>--%>
                                    <asp:TemplateColumn SortExpression="LdapId" HeaderText="LDAP ID">
                                        <ItemTemplate>
                                            <asp:HyperLink runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.LdapId") %>'
                                                NavigateUrl='<%# thisPage + "?LdapId="+DataBinder.Eval(Container.DataItem, "LdapId").ToString()%>'
                                                ID="Hyperlink1">
                                            </asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <%--<asp:BoundColumn DataField="ALTERNATEUSERNAME" SortExpression="ALTERNATEUSERNAME" HeaderText="LDAP ID"></asp:BoundColumn>--%>
                                    <%--<asp:TemplateColumn>
															<ItemTemplate>
																<asp:HyperLink runat="server" Text="ServicesPlus" NavigateUrl='<%# thisPage + "?Identity="+DataBinder.Eval(Container.DataItem, "Identity").ToString()%>' ID="Hyperlink2">
																</asp:HyperLink>
															</ItemTemplate>
														</asp:TemplateColumn>--%>
                                    <%--<asp:TemplateColumn>
															<ItemTemplate>
																<asp:HyperLink runat="server" Text="Delete" NavigateUrl='<%# "users.aspx?Identity=" + DataBinder.Eval(Container.DataItem, "Identity").ToString()%>' ID="Hyperlink3">
																</asp:HyperLink>
															</ItemTemplate>
														</asp:TemplateColumn>--%>
                                    <%--<asp:TemplateColumn>
															<ItemTemplate>
																<asp:LinkButton runat="server" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "Identity")%>' CommandName="Delete" Text="Delete" ID="lnkBtnDelete">
																</asp:LinkButton>
															</ItemTemplate>
														</asp:TemplateColumn>--%>
                                </Columns>
                            </asp:DataGrid>
                        </td>
                        <%--<td colSpan="2">
									<table border="1" width=100%>
										</table>
								</td>--%>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
        </tr>
    </table>
    <%--<input type=hidden id="hdnLastOpr" runat=server />--%>
    </form>
</body>
</html>
