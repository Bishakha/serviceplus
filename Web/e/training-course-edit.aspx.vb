Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Data
Imports Sony.US.ServicesPLUS.Process
Imports ServicesPlusException
Imports Sony.US.AuditLog

Namespace SIAMAdmin

    Partial Class training_course_edit
        Inherits UtilityClass

        Private blnEdit As Boolean = False
        'Dim objUtilties As Utilities = New Utilities
#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub


        Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            MyBase.IsProtectedPage(True, True)
            InitializeComponent()
            btnDelete.Attributes.Add("onClick", "return confirmDelete('Do you really want to hide this course');")
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
            If Not IsPostBack Then
                Try
                    Session.Remove("CourseModels")
                    PopulateMinorCategoryDropDown()
                    PopulateTypeDropDown()
                    PopulateLocationList()
                    PopulateModelListDropDown()
                    If (Not Request.QueryString("LineNo") Is Nothing) And (Not Session.Item("training-course-list") Is Nothing) Then
                        Dim courseSelected As Course
                        Dim itemNumber As Integer = -1
                        itemNumber = Convert.ToInt16(Request.QueryString("LineNo"))
                        courseSelected = CType(Session.Item("training-course-list"), Array)(itemNumber)
                        txtCourseNumber.Text = courseSelected.Number
                        If txtCourseNumber.Text.Trim() <> String.Empty Then
                            PopulateCourseModelGrid()
                        Else
                            Dim objTrainingCourseModel As New TrainingCourseModel
                            objTrainingCourseModel.AddCourseModel("", "", True)
                            dgModel.DataSource = objTrainingCourseModel.CourseModels
                            dgModel.DataBind()
                            Session.Add("CourseModels", objTrainingCourseModel)
                        End If
                        txtDescription.Text = courseSelected.Description
                        txtNotes.Text = courseSelected.Notes
                        txtPreRequisite.Text = courseSelected.PreRequisite
                        txtPrice.Text = courseSelected.ListPrice
                        txtTitle.Text = courseSelected.Title
                        txtNotes.Text = courseSelected.Notes
                        ddlMinorCategory.SelectedValue = courseSelected.MinorCategory.Code
                        ddlType.SelectedValue = courseSelected.Type.Code
                        If ddlType.SelectedValue <> 0 Then
                            chkDefaultLocations.ClearSelection()
                            chkDefaultLocations.Enabled = False
                            chkDefaultLocations.Visible = False

                            If ddlType.SelectedValue = 1 Then
                                lblLocationAstric.Visible = True
                                locationAstrick.Visible = False
                                lblLocationAstric.Text = "For non class room training, location is not required."
                            Else
                                LabelDefaultLocation.Text = "Training URL:"
                                lblLocationAstric.Visible = False
                                locationAstrick.Visible = True
                                txtURL.Visible = True
                                txtURL.Text = courseSelected.URL
                            End If
                        End If

                        blnEdit = True
                        txtCourseNumber.Enabled = False
                        'chkDefaultLocations.Enabled = False
                        If courseSelected.StatusCode = "1" And courseSelected.Hide = "0" Then
                            rdoStatus.SelectedIndex = 0
                            btnDelete.Visible = True
                            btnActivate.Visible = False
                        ElseIf courseSelected.StatusCode = "1" And courseSelected.Hide = "1" Then
                            rdoStatus.SelectedIndex = 1
                            btnDelete.Visible = False
                            btnActivate.Visible = True
                        ElseIf courseSelected.StatusCode = "2" Then 'And courseSelected.Hide = "1" Then ' It could be Hide = 0 or 1
                            rdoStatus.SelectedIndex = 2
                            btnDelete.Visible = False
                            btnActivate.Visible = False
                            btnUpdate.Visible = False
                        End If
                        If (Not Session("NewCourse") Is Nothing) Then
                            Session.Remove("NewCourse")
                        End If
                    Else
                        btnUpdate.Text = "  Add  "
                        Dim objTrainingCourseModel As New TrainingCourseModel
                        objTrainingCourseModel.AddCourseModel("", "", True)
                        dgModel.DataSource = objTrainingCourseModel.CourseModels
                        dgModel.DataBind()
                        Session.Add("CourseModels", objTrainingCourseModel)
                        ddlType.Items(1).Selected = True
                        'chkDefaultLocations.Items(0).Selected = True
                        chkDefaultLocations.Enabled = True
                        rdoStatus.SelectedIndex = 0
                        If (Not Session("NewCourse") Is Nothing) Then
                            Session.Remove("NewCourse")
                        End If
                        ddlType.SelectedIndex = -1
                        ddlType.Items.FindByValue(0).Selected = True
                    End If
                Catch ex As Exception
                    ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                End Try
            End If
        End Sub

        Private Sub btnUpdate_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnUpdate.Click
            Dim cm As New CourseManager
            Dim courseDataManager As New CourseDataManager
            Dim courseSelected As Course
            Dim objPCILogger As New PCILogger() '6524
            Dim item_list As New ArrayList
            Dim item As CourseLocation
            Dim bUpdate As Boolean = False
            Dim itemNumber As Integer = -1

            txtCourseNumber.Text = txtCourseNumber.Text.Trim().ToUpper()
            If Not ValidateForm() Then
                ErrorLabel.Text = "Error entering course info:" + ErrorLabel.Text
                Return
            End If

            For Each chkList As ListItem In chkDefaultLocations.Items
                If chkList.Selected = True Then
                    item = New CourseLocation
                    item.Code = chkList.Value
                    item.Name = chkList.Text
                    item_list.Add(item)
                End If
            Next

            Dim location(item_list.Count - 1) As CourseLocation
            item_list.CopyTo(location)

            Try
                objPCILogger.EventOriginApplication = "ServicesPLUS"
                objPCILogger.EventOriginApplicationLocation = Page.GetType().Name
                objPCILogger.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
                objPCILogger.OperationalUser = Environment.UserName
                objPCILogger.EventDateTime = Date.Now.ToLongTimeString()
                objPCILogger.HTTPRequestObjectValues = objPCILogger.GetRequestContextValue()
                objPCILogger.IndicationSuccessFailure = "Success"
                objPCILogger.EventOriginMethod = "btnUpdate_Click"
                objPCILogger.Message = "Update Training Course Success."
                objPCILogger.EventType = EventType.Update

                If Not String.IsNullOrWhiteSpace(Request.QueryString("LineNo")) And Session.Item("training-course-list") IsNot Nothing Then
                    bUpdate = True
                    itemNumber = Convert.ToInt16(Request.QueryString("LineNo"))
                    courseSelected = CType(Session.Item("training-course-list"), Array)(itemNumber)
                ElseIf (btnUpdate.Text = "Update") And (Session("NewCourse") IsNot Nothing) Then
                    bUpdate = True
                    courseSelected = Session("NewCourse")
                Else
                    courseSelected = New Course
                End If

                courseSelected.Number = txtCourseNumber.Text.Trim().ToUpper()
                courseSelected.Description = txtDescription.Text.Trim()
                courseSelected.Notes = txtNotes.Text.Trim()
                courseSelected.PreRequisite = txtPreRequisite.Text.Trim()
                courseSelected.ListPrice = txtPrice.Text.Trim()
                courseSelected.Title = txtTitle.Text.Trim()
                courseSelected.MinorCategory.Code = ddlMinorCategory.SelectedValue
                courseSelected.Type.Code = ddlType.SelectedValue
                courseSelected.Model = GetModelList()
                courseSelected.DefaultLocationList = GetDefaultLocationList(location)
                courseSelected.TrainingURL = txtURL.Text

                If bUpdate Then
                    Try
                        cm.UpdateCourse(courseSelected)
                        ErrorLabel.Text = "Course updated successfully"
                        rdoStatus.SelectedIndex = 0
                        btnDelete.Visible = True
                        btnActivate.Visible = False
                        If (Not Session("NewCourse") Is Nothing) Then
                            Session("NewCourse") = courseSelected
                        End If
                    Catch ex As Exception
                        ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                    End Try
                Else
                    Try
                        cm.AddCourse(courseSelected)
                        Dim courseArray(CType(Session.Item("training-course-list"), Array).Length) As Course
                        CType(Session.Item("training-course-list"), Array).CopyTo(courseArray, 0)
                        courseArray(courseArray.Length - 1) = courseSelected
                        Session("training-course-list") = courseArray
                        ErrorLabel.Text = "Course added successfully"
                        rdoStatus.SelectedIndex = 0
                        btnDelete.Visible = True
                        btnActivate.Visible = False
                        btnUpdate.Text = "Update"
                        blnEdit = True
                        Session("NewCourse") = courseSelected
                    Catch exxAddcourse As Exception
                        ErrorLabel.Text = exxAddcourse.Message
                        txtCourseNumber.Focus()
                    End Try
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                objPCILogger.IndicationSuccessFailure = "Failure" '6524
                objPCILogger.Message = "Update Training Course Failed. " & ex.Message.ToString() '6524
            Finally
                objPCILogger.PushLogToMSMQ() '6524
            End Try
            RefreshGrid()
        End Sub

        Private Sub PopulateLocationList()
            Try
                Dim courseDataManager As New CourseDataManager
                Dim locations() As CourseLocation = courseDataManager.GetLocations()
                Dim courseSelected As Course
                Dim bUpdate As Boolean = False
                Dim prevLocations() As CourseLocation = Nothing
                Dim itemNumber As Integer = -1
                Dim chkCount As Integer = 0

                For Each cl As CourseLocation In locations
                    chkDefaultLocations.Items.Add(New ListItem(cl.Name, cl.Code))
                Next

                If (Not Request.QueryString("LineNo") Is Nothing) Then 'Update
                    itemNumber = Convert.ToInt16(Request.QueryString("LineNo"))
                    courseSelected = CType(Session.Item("training-course-list"), Array)(itemNumber)
                    bUpdate = True
                    prevLocations = courseDataManager.GetDefaultLocations(courseSelected.Number)
                End If

                '2016-04-21 ASleight - Adding additional check in case prevLocations is empty
                If (prevLocations IsNot Nothing AndAlso prevLocations.Length > 0) Then
                    If bUpdate Then
                        For Each chkList As ListItem In chkDefaultLocations.Items
                            For Each pcl As CourseLocation In prevLocations
                                If pcl.Code = chkDefaultLocations.Items(chkCount).Value Then
                                    chkList.Selected = True
                                End If
                            Next
                            chkCount += 1
                        Next
                    End If
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Sub PopulateMinorCategoryDropDown()
            Try
                Dim courseDataManager As New CourseDataManager
                Dim categories() As CourseMinorCategory = courseDataManager.GetMinorCategories()
                For Each cl As CourseMinorCategory In categories
                    ddlMinorCategory.Items.Add(New ListItem(cl.Mejor_Minor_Description, cl.Code))
                Next
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Sub PopulateModelListDropDown()
            Try
                Dim courseDataManager As New CourseDataManager
                Dim objCourseModelList As ArrayList = courseDataManager.GetModelList()
                ddlModelList.DataTextField = "Model"
                ddlModelList.DataValueField = "Model"
                ddlModelList.DataSource = objCourseModelList
                ddlModelList.DataBind()
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Sub PopulateCourseModelGrid()
            Try
                Dim courseDataManager As New CourseDataManager
                Dim objTrainingCourseModel As New TrainingCourseModel
                objTrainingCourseModel = courseDataManager.GetCourseModelList(txtCourseNumber.Text.ToString())
                dgModel.DataSource = objTrainingCourseModel.CourseModels
                dgModel.DataBind()
                Session.Add("CourseModels", objTrainingCourseModel)
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Sub PopulateTypeDropDown()
            Try
                Dim courseDataManager As New CourseDataManager
                Dim types() As CourseType = courseDataManager.GetTypes()
                For Each cl As CourseType In types
                    ddlType.Items.Add(New ListItem(cl.Description, cl.Code))
                Next
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Sub btnDelete_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnDelete.Click
            Dim courseSelected As Course
            Dim objPCILogger As New PCILogger() '6524
            Try

                '6524 start
                objPCILogger.EventOriginApplication = "ServicesPLUS"
                objPCILogger.EventOriginApplicationLocation = Page.GetType().Name
                objPCILogger.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
                objPCILogger.OperationalUser = Environment.UserName
                objPCILogger.EventDateTime = Date.Now.ToLongTimeString()
                objPCILogger.HTTPRequestObjectValues = objPCILogger.GetRequestContextValue()
                objPCILogger.IndicationSuccessFailure = "Success"
                objPCILogger.EventOriginMethod = "btnDelete_Click"
                objPCILogger.Message = "btnDelete_Click Success."
                objPCILogger.EventType = EventType.Delete
                '6524 end

                If (Not Request.QueryString("LineNo") Is Nothing) Then 'Update
                    Dim itemNumber As Integer = -1
                    itemNumber = Convert.ToInt16(Request.QueryString("LineNo"))
                    courseSelected = CType(Session.Item("training-course-list"), Array)(itemNumber)
                    Dim cm As New CourseManager
                    cm.CancelCourse(courseSelected)
                    ErrorLabel.Text = "Course hidden successfully"
                    rdoStatus.SelectedIndex = 1
                    btnUpdate.Visible = False
                    btnDelete.Visible = False
                    btnActivate.Visible = True
                ElseIf (Not Session("NewCourse") Is Nothing) Then
                    courseSelected = Session("NewCourse")
                    Dim cm As New CourseManager
                    cm.CancelCourse(courseSelected)
                    ErrorLabel.Text = "Course hidden successfully"
                    rdoStatus.SelectedIndex = 1
                    btnUpdate.Visible = False
                    btnDelete.Visible = False
                    btnActivate.Visible = True
                    Session("NewCourse") = courseSelected
                End If




            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                objPCILogger.IndicationSuccessFailure = "Failure" '6524
                objPCILogger.Message = "btnDelete_Click Failed. " & ex.Message.ToString() '6524
            Finally
                objPCILogger.PushLogToMSMQ() '6524
            End Try
        End Sub

        Private Function ValidateForm() As Boolean
            Dim bValid = True

            ErrorLabel.Text = ""

            labelCourseNumber.CssClass = "Body"
            labelTitle.CssClass = "Body"
            labelDescription.CssClass = "Body"
            labelPrice.CssClass = "Body"

            LabelCourseNumberStar.Text = ""
            LabelTitleStar.Text = ""
            LabelDescriptionStar.Text = ""
            LabelPriceStar.Text = ""

            If txtCourseNumber.Text = "" Then
                labelCourseNumber.CssClass = "redAsterick"
                LabelCourseNumberStar.Text = "<span class=""redAsterick"">*</span>"
                bValid = False
            End If
            If txtTitle.Text = "" Then
                labelTitle.CssClass = "redAsterick"
                LabelTitleStar.Text = "<span class=""redAsterick"">*</span>"
                bValid = False
            End If
            If txtDescription.Text = "" Then
                labelDescription.CssClass = "redAsterick"
                LabelDescriptionStar.Text = "<span class=""redAsterick"">*</span>"
                bValid = False
            ElseIf txtDescription.Text.Trim.Length > 4000 Then
                ErrorLabel.Text = "Description must be of length 4000 or less."
                labelDescription.CssClass = "redAsterick"
                LabelDescriptionStar.Text = "<span class=""redAsterick"">*</span>"
                bValid = False
            End If

            If ddlType.SelectedValue <> 0 And ddlType.SelectedValue <> 1 Then
                If txtURL.Text = String.Empty Then
                    LabelDefaultLocation.CssClass = "redAsterick"
                    bValid = False
                End If
            Else
                LabelDefaultLocation.CssClass = "Body"
            End If
            If txtPrice.Text = "" Then
                labelPrice.CssClass = "redAsterick"
                LabelPriceStar.Text = "<span class=""redAsterick"">*</span>"
                bValid = False
            End If
            Try
                Double.Parse(txtPrice.Text)
            Catch ex As Exception
                ErrorLabel.Text = "Price must be numeric value"
            End Try
            If txtNotes.Text.Trim.Length > 4000 Then
                ErrorLabel.Text = "Notes must be of length 4000 or less."
                bValid = False
            End If
            If Not Session("CourseModels") Is Nothing Then
                Dim objTrainingCourseModel As New TrainingCourseModel
                objTrainingCourseModel = CType(Session("CourseModels"), TrainingCourseModel)
                If objTrainingCourseModel.CourseModels.Length = 0 Then
                    lblGridError.Text = "At least one model is required to add / update Course."
                    bValid = False
                ElseIf objTrainingCourseModel.CourseModels.Length = 1 And objTrainingCourseModel.CourseModels(0).IsDefault = True Then
                    lblGridError.Text = "At least one model is required to add / update Course."
                    bValid = False
                End If
            End If
            Return bValid
        End Function

        Protected Sub dgProduct_ItemCommand(ByVal source As Object, ByVal e As DataGridCommandEventArgs) Handles dgModel.ItemCommand
            Try
                Dim ireduceItemId As Short = 0
                If e.CommandName = "Delete" Then
                    If Not Session("CourseModels") Is Nothing Then
                        Dim objTrainingCourseModel As New TrainingCourseModel
                        objTrainingCourseModel = CType(Session("CourseModels"), TrainingCourseModel)
                        Dim objCourseModel As CourseModel
                        For Each objCourseModel In objTrainingCourseModel.CourseModels
                            If objCourseModel.ItemID.ToString() = e.Item.Cells(2).Text.Trim() Then
                                objTrainingCourseModel.RemoveModel(objCourseModel)
                                ireduceItemId = 1
                            End If
                            objCourseModel.ItemID = objCourseModel.ItemID - ireduceItemId
                        Next

                        If objTrainingCourseModel.CourseModels.Length = 0 Then
                            Session.Remove("CourseModels")
                            objTrainingCourseModel.AddCourseModel("", "", True)
                            Session.Add("CourseModels", objTrainingCourseModel)
                        End If
                        dgModel.DataSource = objTrainingCourseModel.CourseModels
                        dgModel.DataBind()

                    End If
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Protected Sub btnAddModel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAddModel.Click
            Try
                Dim m_Model As String = String.Empty

                If String.IsNullOrWhiteSpace(txtCourseNumber.Text) Then
                    ErrorLabel.Text = "Please enter course number."
                    ErrorLabel.Visible = True
                    lblGridError.Text = "Please enter course number."
                    lblGridError.Visible = True
                    Return
                End If
                If ddlModelList.SelectedIndex = 0 Then
                    If String.IsNullOrWhiteSpace(txtModel.Text) Then
                        lblGridError.Text = "Please select a Model or enter a new Model in the text area."
                        lblGridError.Visible = True
                        Return
                    Else
                        m_Model = txtModel.Text.Trim()
                    End If
                Else
                    m_Model = ddlModelList.SelectedItem.Text.Trim()
                End If
                If Not String.IsNullOrWhiteSpace(m_Model) Then
                    Dim result As Boolean = AddModel(m_Model)
                    If result Then
                        RefreshGrid()
                    Else
                        lblGridError.Text = "Model number already exists in the list."
                        lblGridError.Visible = True
                    End If
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Function AddModel(ByVal Model As String) As Boolean
            Dim objTrainingCourseModel As New TrainingCourseModel
            Dim returnValue As Boolean = True

            If (Not Session("CourseModels") Is Nothing) Then
                objTrainingCourseModel = CType(Session("CourseModels"), TrainingCourseModel)
            End If

            For Each objCourseModel As CourseModel In objTrainingCourseModel.CourseModels
                If objCourseModel.Model.ToUpper() = Model.ToUpper() Then
                    returnValue = False
                    Exit For
                End If
            Next

            If returnValue = True Then
                objTrainingCourseModel.AddCourseModel(Model, txtCourseNumber.Text.Trim().ToUpper(), False)
                Session.Remove("CourseModels")
                Session.Add("CourseModels", objTrainingCourseModel)
            End If

            Return returnValue

        End Function

        Private Sub RefreshGrid()
            If Not Session("CourseModels") Is Nothing Then
                Dim objTrainingCourseModel As New TrainingCourseModel
                objTrainingCourseModel = CType(Session("CourseModels"), TrainingCourseModel)
                dgModel.DataSource = objTrainingCourseModel.CourseModels
                dgModel.DataBind()
                If objTrainingCourseModel.CourseModels.Length > 0 Then
                    btnUpdate.Enabled = True
                Else
                    btnUpdate.Enabled = True
                    btnUpdate.ToolTip = "Add atleast one model to the list"
                End If
            End If
        End Sub

        Private Function GetModelList() As String
            Dim m_modellist As String = String.Empty
            Dim objTrainingCourseModel As New TrainingCourseModel
            objTrainingCourseModel = CType(Session("CourseModels"), TrainingCourseModel)
            For Each objCourseModel As CourseModel In objTrainingCourseModel.CourseModels
                m_modellist = m_modellist + objCourseModel.Model + "~"
            Next
            If m_modellist.Length > 1 Then
                m_modellist = m_modellist.Substring(0, m_modellist.Length - 1)
            Else
                m_modellist = ""
            End If

            Return m_modellist
        End Function

        Private Function GetDefaultLocationList(ByVal location() As CourseLocation) As String
            Dim m_DefaultLocationList As String = String.Empty
            For Each objCourseLocation As CourseLocation In location
                m_DefaultLocationList = m_DefaultLocationList + objCourseLocation.Code + "~"
            Next
            If m_DefaultLocationList.Length > 1 Then
                m_DefaultLocationList = m_DefaultLocationList.Substring(0, m_DefaultLocationList.Length - 1)
            Else
                m_DefaultLocationList = ""
            End If

            Return m_DefaultLocationList
        End Function

        Protected Sub btnCancel_Click1(ByVal sender As Object, ByVal e As EventArgs) Handles btnCancel.Click
            Server.Transfer("training-course-list.aspx", False)
        End Sub

        Protected Sub ddlType_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlType.SelectedIndexChanged
            Try
                If ddlType.SelectedValue = 0 Then
                    chkDefaultLocations.Enabled = True
                    chkDefaultLocations.Visible = True
                    chkDefaultLocations.SelectedIndex = -1
                    lblLocationAstric.Visible = False
                    txtURL.Visible = False
                    txtURL.Text = ""
                    LabelDefaultLocation.Text = "Default Location(s):"
                    locationAstrick.Visible = True
                Else
                    'chkDefaultLocations.ClearSelection()
                    ''As per new training clearing proposal none of these would be selected
                    ''chkDefaultLocations.Items(0).Selected = True
                    'chkDefaultLocations.Enabled = False
                    'chkDefaultLocations.Visible = False
                    'lblLocationAstric.Visible = True
                    'locationAstrick.Visible = False
                    'lblLocationAstric.Text = "For non class room training, location is not required."

                    chkDefaultLocations.ClearSelection()
                    chkDefaultLocations.Enabled = False
                    chkDefaultLocations.Visible = False

                    If ddlType.SelectedValue = 1 Then
                        lblLocationAstric.Visible = True
                        locationAstrick.Visible = False
                        lblLocationAstric.Text = "For non class room training default location is not required."
                        txtURL.Visible = False
                        txtURL.Text = ""
                    Else
                        LabelDefaultLocation.Text = "Training URL:"
                        lblLocationAstric.Visible = False
                        locationAstrick.Visible = True
                        txtURL.Visible = True
                        txtURL.Text = ""
                    End If

                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As EventArgs) Handles Me.PreRender
            If blnEdit Then
                ddlType.Enabled = False
                'chkDefaultLocations.Enabled = False
                If ddlType.SelectedValue = 0 Then
                    locationAstrick.Visible = chkDefaultLocations.Enabled
                End If

            End If
        End Sub

        Protected Sub btnActivate_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnActivate.Click
            Dim courseSelected As Course
            Dim objPCILogger As New PCILogger() '6524
            Try

                '6524 start
                objPCILogger.EventOriginApplication = "ServicesPLUS"
                objPCILogger.EventOriginApplicationLocation = Page.GetType().Name
                objPCILogger.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
                objPCILogger.OperationalUser = Environment.UserName
                objPCILogger.EventDateTime = Date.Now.ToLongTimeString()
                objPCILogger.HTTPRequestObjectValues = objPCILogger.GetRequestContextValue()
                objPCILogger.IndicationSuccessFailure = "Success"
                objPCILogger.EventOriginMethod = "btnActivate_Click"
                objPCILogger.Message = "Course Activation Success."
                objPCILogger.EventType = EventType.Update
                '6524 end

                If (Not Request.QueryString("LineNo") Is Nothing) Then 'Update
                    Dim itemNumber As Integer = -1
                    itemNumber = Convert.ToInt16(Request.QueryString("LineNo"))
                    courseSelected = CType(Session.Item("training-course-list"), Array)(itemNumber)
                    Dim cm As New CourseManager
                    cm.ActivateCourse(courseSelected)
                    ErrorLabel.Text = "Course activated successfully"
                    rdoStatus.SelectedIndex = 0
                    btnUpdate.Visible = True
                    btnDelete.Visible = True
                    btnActivate.Visible = False
                ElseIf (Not Session("NewCourse") Is Nothing) Then
                    courseSelected = Session("NewCourse")
                    Dim cm As New CourseManager
                    cm.ActivateCourse(courseSelected)
                    ErrorLabel.Text = "Course activated successfully"
                    rdoStatus.SelectedIndex = 0
                    btnUpdate.Visible = True
                    btnDelete.Visible = True
                    btnActivate.Visible = False
                    Session("NewCourse") = courseSelected
                End If




            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                objPCILogger.IndicationSuccessFailure = "Failure" '6524
                objPCILogger.Message = "Course Activation Failed. " & ex.Message.ToString() '6524
            Finally
                objPCILogger.PushLogToMSMQ() '6524
            End Try
        End Sub
    End Class

End Namespace
