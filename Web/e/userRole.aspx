<%@ Reference Page="~/e/Utility.aspx" %>

<%@ Page Language="VB" AutoEventWireup="false" CodeFile="userRole.aspx.vb" Inherits="SIAMAdmin.e_userRole" CodeFileBaseClass="SIAMAdmin.UtilityClass" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <title>User Role Master</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link href="includes/style.css" type="text/css" rel="stylesheet">
</head>

<body>
    <form name="frmRole" runat="server">
        <table width="75%" border="0" align="center" cellspacing="2">
            <tr class="PageHead">
                <td colspan="2" style="text-align: center; width: 601px;">
                    Role Master
                </td>
            </tr>
            <tr>
                <td align="center" colspan="2" style="width: 601px; height: 25px">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="2" style="height: 25px; width: 601px; background: #5d7180; text-align: center;">
                    <table border="0">
                        <tr style="height: 25px;">
                            <td class="Nav3">New</td>
                            <td class="Nav3" align="center" width="5">&nbsp;|&nbsp;</td>
                            <td><a class="Nav3" href="srhRole.aspx">Search</a></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="text-align: center; width: 601px; height: 19px;">
                    <asp:Label ID="ErrorLabel" runat="server" CssClass="body" ForeColor="Red" EnableViewState="False" />&nbsp;
                </td>
            </tr>

            <tr>
                <td colspan="2">
                    <div id="div1" style="border-right: gray thin solid; border-top: gray thin solid; border-left: gray thin solid; border-bottom: gray thin solid; width: 100%">
                        <table width="100%" border="0" cellspacing="0">
                            <tr>
                                <td style="width: 170px; text-align: right" colspan="2">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 170px; text-align: right;">
                                    <asp:Label ID="lblRole" runat="server" CssClass="body" Text="Role :"></asp:Label>
                                </td>
                                <td><span size="2" face="MS Reference Sans Serif">
                                    <SPS:SPSTextBox ID="txtRoleName" runat="server" CssClass="body" MaxLength="50"></SPS:SPSTextBox><span style="font-size: 6pt; color: #ff3333">*</span></span></td>
                            </tr>
                            <tr>
                                <td style="width: 170px; text-align: right;">
                                    <asp:Label ID="lblActive" runat="server" CssClass="body" Text="Is Active :" />
                                </td>
                                <td>
                                    <asp:CheckBox ID="chkActive" Checked="true" runat="server" CssClass="body" />
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 299px; height: 19px;" colspan="2">&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <div align="center">
                                        <asp:Button ID="cmdSave" runat="server" CssClass="body" Text="  Save  " ToolTip="Click to Save the Role" />
                                        <asp:Button ID="cmdCancel" runat="server" CssClass="body" Text=" Cancel " ToolTip="Click to Cancel the entry" />&nbsp;
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
