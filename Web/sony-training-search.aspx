<%@ Reference Page="~/SSL.aspx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="SonyFooter" Src="SonyFooter.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="PersonalMessage" Src="PersonalMessage.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="ServicePlusNav" Src="ServicePlusNav.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="TopSonyHeader" Src="TopSonyHeader.ascx" %>

<%@ Page Language="vb" AutoEventWireup="false" Inherits="ServicePLUSWebApp.sony_training_search" CodeFile="sony-training-search.aspx.vb" CodeFileBaseClass="ServicePLUSWebApp.SSL" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="<%=PageLanguage %>">
<head>
    <title>Sony Training Institute - Classes</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <meta content="sony, training, video, audio, broadcast" name="keywords">
    <link href="includes/ServicesPLUS_style.css" type="text/css" rel="stylesheet">
    <script type="text/javascript" src="includes/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="includes/svcplus_globalization.js"></script>
    <script language="javascript" src="includes/ServicesPLUS.js" type="text/javascript"></script>
</head>
<body style="color: #000000; background: #5d7180; margin: 0px;">
    <form id="Form1" method="post" runat="server">
        <center>
            <table width="680" border="0" style="width: 680px; height: 572px" role="presentation">
                <tr>
                    <td style="width: 25px; background: url('images/sp_left_bkgd.gif');">
                        <img src="images/spacer.gif" height="25" width="25" alt="">
                    </td>
                    <td style="width: 708px; background: #ffffff">
                        <table style="width: 708px; border: none;" role="presentation">
                            <tr>
                                <td>
                                    <ServicePLUSWebApp:TopSonyHeader ID="TopSonyHeader" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <nav><ServicePLUSWebApp:ServicePlusNav ID="SonyNav" runat="server" /></nav>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table style="width: 708px; border: none;" role="presentation">
                                        <tbody>
                                            <tr style="height: 81px;">
                                                <td style="width: 464px; background: #363d45 url('images/sp_int_header_top_trainist.jpg'); vertical-align: bottom; text-align: right;">
                                                    <h1 class="headerText">Sony Training Institute<sup>&reg;</sup>&nbsp;</h1>
                                                </td>
                                                <td style="width: 240px; vertical-align: top; background: #363d45">
                                                    <ServicePLUSWebApp:PersonalMessage ID="PersonalMessageDisplay" runat="server" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 464px; background: #f2f5f8;">
                                                    <table style="height: 122px; width: 464px; border: none;" role="presentation">
                                                        <tr>
                                                            <td style="width: 10px;">
                                                                <img height="1" src="images/spacer.gif" width="10" alt="">
                                                            </td>
                                                            <td style="vertical-align: top;">
                                                                <h2 class="headerTitle">Sony Training Search</h2>
                                                            </td>
                                                        </tr>
                                                        <tr style="height: 119px">
                                                            <td style="width: 10px;">
                                                                <img height="1" src="images/spacer.gif" width="10" alt="">
                                                            </td>
                                                            <td>
                                                                <table style="width: 454px; height: 40px; border: none;" role="presentation">
                                                                    <tr style="height: 29px">
                                                                        <td style="width: 140px;">
                                                                            <label for="ddMajorCategory" class="finderCopyDark">&nbsp;Category:</label>
                                                                        </td>
                                                                        <td style="width: 20px;">
                                                                            <img height="1" src="images/spacer.gif" width="2" alt="">
                                                                        </td>
                                                                        <td style="width: 250px;">
                                                                            <label for="ddModelNumber" class="finderCopyDark">&nbsp;Model Number:</label>
                                                                        </td>
                                                                        <td>
                                                                            <img height="1" src="images/spacer.gif" width="10" alt="">
                                                                        </td>
                                                                    </tr>
                                                                    <tr style="height: 29px">
                                                                        <td>
                                                                            <asp:DropDownList ID="ddMajorCategory" runat="server" Width="140" />
                                                                        </td>
                                                                        <td>&nbsp;</td>
                                                                        <td>
                                                                            <asp:DropDownList ID="ddlModelNumber" runat="server" Width="250" />
                                                                        </td>
                                                                        <td>&nbsp;</td>
                                                                    </tr>
                                                                </table>
                                                                <table style="width: 454px; height: 40px; border: none;" role="presentation">
                                                                    <tr style="height: 12px;">
                                                                        <td>
                                                                            <label for="ddTitle" class="finderCopyDark">&nbsp;Course Title:</label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:DropDownList ID="ddTitle" runat="server" Width="430" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                                <table style="width: 454px; height: 40px; border: none;" role="presentation">
                                                                    <tr style="height: 16px">
                                                                        <td style="width: 160px">
                                                                            <label for="ddLocation" class="finderCopyDark">&nbsp;Location:</label>
                                                                        </td>
                                                                        <td style="width: 10px">
                                                                            <img height="1" src="images/spacer.gif" width="2" alt="">
                                                                        </td>
                                                                        <td style="width: 152px">
                                                                            <label for="txtCourseNumber" class="finderCopyDark">&nbsp;Course Number:</label>
                                                                        </td>
                                                                        <td style="width: 10px">
                                                                            <img height="1" src="images/spacer.gif" width="2" alt="">
                                                                        </td>
                                                                        <td style="width: 126px">
                                                                            <label for="ddMonth" class="finderCopyDark">&nbsp;Month:</label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:DropDownList ID="ddLocation" runat="server" Width="160px" />
                                                                        </td>
                                                                        <td>&nbsp;</td>
                                                                        <td>
                                                                            <SPS:SPSTextBox ID="txtCourseNumber" runat="server" Width="150px" ToolTip="Search by Course Number"
                                                                                MaxLength="30" />
                                                                        </td>
                                                                        <td>&nbsp;</td>
                                                                        <td>
                                                                            <asp:DropDownList ID="ddMonth" runat="server" Width="106px" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td style="width: 246px; background: #99a8b5;">
                                                    <table width="100%" border="0" role="presentation">
                                                        <tr>
                                                            <td>&nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                            <td style="height: 124px"></td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:ImageButton ID="btnSearch" runat="server" ToolTip="Training Search" Width="125" Height="32"
                                                                    ImageUrl="images/sp_int_SearchHdr_btn.gif" AlternateText="Search for professional training classes offered by the Sony Training Institute" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr style="height: 9px;">
                                                <td style="width: 464px; background: #f2f5f8">
                                                    <img height="9" src="images/sp_int_header_btm_left_onepix.gif" width="464" alt="">
                                                </td>
                                                <td style="width: 246px; background: #99a8b5">
                                                    <img height="9" src="images/sp_int_header_btm_right.gif" width="246" alt="">
                                                </td>
                                            </tr>
                                    </table>
                                    <br>
                                    <table width="710" border="0" role="presentation">
                                        <tr>
                                            <td>
                                                <asp:Label ID="errorMessageLabel" runat="server" Width="641px" EnableViewState="False" CssClass="tableData" ForeColor="Red" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 708px">
                                    <table width="710" border="0" role="presentation">
                                        <tr style="height: 2px;">
                                            <td colspan="3">
                                                <img height="5" src="images/spacer.gif" width="20" alt="">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="20">
                                                <img height="20" src="images/spacer.gif" width="20" alt="">
                                            </td>
                                            <td id="tdResult" runat="server" visible="false" valign="top" width="655" style="width: 655px">
                                                <table width="298" border="0" role="presentation">
                                                    <tr>
                                                        <td width="60">
                                                            <asp:ImageButton ID="btnPreviousPage" runat="server" ToolTip="Previous Result Page" Width="9" Height="9"
                                                                ImageUrl="images/sp_int_leftArrow_btn.gif"></asp:ImageButton><asp:LinkButton ID="PreviousLinkButton" runat="server" ToolTip="Previous Result Page" CssClass="tableData">Previous</asp:LinkButton>&nbsp;
                                                        </td>
                                                        <td class="tableData" align="center" width="50">
                                                            <asp:Label ID="lblCurrentPage" runat="server">1</asp:Label>&nbsp;of&nbsp;
													        <asp:Label ID="lblEndingPage" runat="server">1</asp:Label>
                                                        </td>
                                                        <td width="45">
                                                            &nbsp;
													        <asp:LinkButton ID="NextLinkButton" runat="server" ToolTip="Next Result Page" CssClass="tableData">Next</asp:LinkButton><asp:ImageButton ID="btnNextPage" runat="server" ToolTip="Next Result Page" Width="9" Height="9"
                                                                ImageUrl="images/sp_int_rightArrow_btn.gif"></asp:ImageButton>
                                                        </td>
                                                        <td width="25">
                                                            <img height="4" src="images/spacer.gif" width="25" alt="">
                                                        </td>
                                                        <td class="tableData" align="right" width="40">
                                                            <asp:Label ID="lblPg1" runat="server">Page:</asp:Label>
                                                        </td>
                                                        <td width="40">
                                                            <SPS:SPSTextBox ID="txtPageNumber" runat="server" Width="40px" ToolTip="Page Number" MaxLength="5"></SPS:SPSTextBox>
                                                        </td>
                                                        <td>
                                                            <asp:ImageButton ID="btnGoToPage" runat="server" ToolTip="Goto Page" Width="40" Height="25" ImageUrl="images/sp_int_go_btn.gif"
                                                                AlternateText="Go to specified result page"></asp:ImageButton>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <table width="660" border="0" role="presentation">
                                                    <tr>
                                                        <td colspan="11" bgcolor="#d5dee9">
                                                            <img height="1" src="images/spacer.gif" width="1" alt="">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="1" bgcolor="#d5dee9">
                                                            <img height="35" src="images/spacer.gif" width="1" alt="">
                                                        </td>
                                                        <td class="tableHeader" align="center" width="103" bgcolor="#d5dee9" colspan="1" rowspan="1"
                                                            style="width: 103px">
                                                            <asp:LinkButton ID="CategorySortLink" runat="server" ToolTip="Sort By Course Category" CssClass="Body"
                                                                CommandArgument="CourseCategoryASC">Category</asp:LinkButton>&nbsp;
                                                        </td>
                                                        <td width="1" bgcolor="#d5dee9" style="width: 1px">
                                                            <img height="1" src="images/spacer.gif" width="1" alt="">
                                                        </td>
                                                        <td class="tableHeader" align="center" width="95" bgcolor="#d5dee9" colspan="1" rowspan="1">
                                                            <asp:LinkButton ID="ModelSortLink" runat="server" ToolTip="Sort By Model" CssClass="Body">Model</asp:LinkButton>&nbsp;
                                                        </td>
                                                        <td width="43" bgcolor="#d5dee9" style="width: 43px"></td>
                                                        <td class="tableHeader" align="center" width="280" bgcolor="#d5dee9">
                                                            <img height="1" src="images/spacer.gif" width="1" alt=""><asp:LinkButton ID="TitleSortLink" runat="server" ToolTip="Sort By Title" CssClass="Body" ForeColor="Black">Course Title</asp:LinkButton>&nbsp;
                                                        </td>
                                                        <td style="width: 4px" width="4" bgcolor="#d5dee9"></td>
                                                        <td class="tableHeader" style="width: 36px" align="center" width="36" bgcolor="#d5dee9"
                                                            colspan="1" rowspan="1" valign="middle">
                                                            <img height="1" src="images/spacer.gif" width="1" alt="">&nbsp;<asp:LinkButton ID="LocationSortLink" runat="server" ToolTip="Sort By Location" CssClass="Body"
                                                                ForeColor="Black">Location</asp:LinkButton>
                                                        </td>
                                                        <td style="width: 2px" width="2" bgcolor="#d5dee9"></td>
                                                        <td class="tableHeader" align="center" width="110" bgcolor="#d5dee9" colspan="1" rowspan="1">
                                                            Course 
													        Number
                                                        </td>
                                                        <td width="1" bgcolor="#d5dee9">
                                                            <img height="35" src="images/spacer.gif" width="1" alt="">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="11">
                                                            <asp:Table ID="CourseSearchResultTable" Width="665px" runat="server" CellSpacing="0"></asp:Table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="11" bgcolor="#d5dee9">
                                                            <img height="1" src="images/spacer.gif" width="1" alt="">
                                                        </td>
                                                    </tr>
                                                </table>
                                                <table width="298" border="0" role="presentation">
                                                    <tr>
                                                        <td width="60">
                                                            <asp:ImageButton ID="btnPreviousPage2" runat="server" ToolTip="Previous Results Page" Width="9" Height="9"
                                                                ImageUrl="images/sp_int_leftArrow_btn.gif"></asp:ImageButton><asp:LinkButton ID="PreviousLinkButton2" runat="server" ToolTip="Previous Results Page" CssClass="tableData">Previous</asp:LinkButton>&nbsp;
                                                        </td>
                                                        <td class="tableData" align="center" width="50">
                                                            <asp:Label ID="lblCurrentPage2" runat="server">1</asp:Label>&nbsp;of&nbsp;
													        <asp:Label ID="lblEndingPage2" runat="server">1</asp:Label>
                                                        </td>
                                                        <td width="45">
                                                            &nbsp;
													        <asp:LinkButton ID="NextLinkButton2" runat="server" ToolTip="Next Result Page" CssClass="tableData">Next</asp:LinkButton><asp:ImageButton ID="btnNextPage2" runat="server" ToolTip="Next Result Page" Width="9" Height="9"
                                                                ImageUrl="images/sp_int_rightArrow_btn.gif"></asp:ImageButton>
                                                        </td>
                                                        <td width="25">
                                                            <img height="4" src="images/spacer.gif" width="25" alt="">
                                                        </td>
                                                        <td class="tableData" align="right" width="40">
                                                            <asp:Label ID="lblPg2" runat="server">Page:</asp:Label>
                                                        </td>
                                                        <td width="40">
                                                            <SPS:SPSTextBox ID="txtPageNumber2" runat="server" Width="40px" ToolTip="Page Number" MaxLength="5"></SPS:SPSTextBox>
                                                        </td>
                                                        <td>
                                                            <asp:ImageButton ID="btnGoToPage2" runat="server" ToolTip="Goto Page" Width="40" Height="25" ImageUrl="images/sp_int_go_btn.gif"
                                                                AlternateText="Goto specified result page"></asp:ImageButton>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <table width="283" border="0" role="presentation">
                                                    <tr>
                                                        <td>
                                                            <a class="tableHeaderLink2" href="#top">
                                                                <img height="28" alt="Back to top of the results page" src="images/sp_int_back2top_btn.gif"
                                                                    width="78" border="0"></a>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <img height="1" src="images/spacer.gif" width="1" alt="">
                                            </td>
                                            <td width="20">
                                                <img height="20" src="images/spacer.gif" width="20" alt="">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 708px">
                                    <footer><ServicePLUSWebApp:SonyFooter ID="SonyFooter1" runat="server" /></footer>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td style="width: 25px; background: url('images/sp_right_bkgd.gif');">
                        <img height="5" src="images/spacer.gif" width="25" alt="">
                    </td>
                </tr>
            </table>
        </center>
    </form>
</body>
</html>
