<%@ Reference Page="~/SSL.aspx" %>

<%@ Page Language="vb" AutoEventWireup="false" Inherits="ServicePLUSWebApp.Sony_EW_PurchaseInfo"
    EnableViewStateMac="true" CodeFile="Sony-EW-PurchaseInfo.aspx.vb" CodeFileBaseClass="ServicePLUSWebApp.SSL" %>

<%@ Register TagPrefix="ServicePLUSWebApp" TagName="SonyFooter" Src="SonyFooter.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="PersonalMessage" Src="PersonalMessage.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="ServicePlusNav" Src="ServicePlusNav.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="TopSonyHeader" Src="TopSonyHeader.ascx" %>
<%@ Register Src="~/UserControl/SPSPromotion.ascx" TagName="SPSPromotion" TagPrefix="uc1" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="<%=PageLanguage %>">
<head>
    <title>Purchased product detail for Sony Extended Warranties</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <meta content="Sony Parts, Sony Professional Parts, Sony Broadcast Parts, Sony Business Parts, Sony Software, Sony Professional Software, Sony Broadcast Software, Sony Business Software"
        name="keywords">
    <link href="includes/ServicesPLUS_style.css" type="text/css" rel="stylesheet" />
    <link href="themes/base/jquery.ui.all.css" rel="stylesheet" type="text/css" />
    <link href="includes/jquery.cluetip.css" rel="stylesheet" type="text/css" />

    <script src="includes/jquery-1.4.2.min.js" type="text/javascript"></script>
    <script src="includes/jquery.ui.core.js" type="text/javascript"></script>
    <script src="includes/jquery.ui.widget.js" type="text/javascript"></script>
    <script src="includes/jquery.cluetip.js" type="text/javascript"></script>
    <script src="includes/jquery.ui.mouse.js" type="text/javascript"></script>
    <script src="includes/jquery.ui.draggable.js" type="text/javascript"></script>
    <script src="includes/jquery.ui.position.js" type="text/javascript"></script>
    <script src="includes/jquery.ui.resizable.js" type="text/javascript"></script>
    <script src="includes/jquery.ui.dialog.js" type="text/javascript"></script>
    <script src="includes/jquery-ui-1.8.1.custom.min.js" type="text/javascript"></script>
    <script src="includes/jquery-1.10.2.min.js" type="text/javascript"></script>
    <script src="includes/ServicesPLUS.js" type="text/javascript"></script>
    <script src="includes/svcplus_globalization.js" type="text/javascript"></script>
    <script type="text/javascript">
        function closedilog() {
            $("#dialog").dialog("close");
            return true;
        }
        var exception = false;
        $(document).ready(function () {
            $("#messageclose").click(function () {

                $("#dialog").dialog("close");
            });


        });                       //Document ready

        function replaceAll(txt, replace, with_this) { return txt.replace(new RegExp(replace, 'g'), with_this); }

        function validPart(partnumber, qty) {

            if (jQuery.trim(partnumber.val()) === "" && jQuery.trim(qty.val()) === "") {
                return false;
            }
            else {

                if (jQuery.trim(partnumber.val()) === "" || jQuery.trim(qty.val()) === "") {
                    if (jQuery.trim(partnumber.val()) === "") {
                        exception = true;

                        $("#divException").append("<strong>Please enter a valid part." + qty.val().tostring() + "</strong><br/>");
                        //$("#divMessage").append("<strong>Please enter a valid part." + qty.val().tostring() + "</strong>");
                        $("#divException").append("<br/>");

                    } else {
                        exception = true;
                        $("#divException").append("<br/>");
                        $("#divException").append("<strong>Please enter a valid quantity." + partnumber.val() + "</strong><br/>");
                        // $("#divMessage").append("<strong>Please enter a valid quantity." + partnumber.val() + "</strong>");
                        $("#divException").append("<br/>");
                    }
                    return false;
                }
                if (jQuery.trim(partnumber.val()) !== "" && jQuery.trim(qty.val()) !== "") {
                    if (jQuery.trim(qty.val()).match("[^0-9]")) {
                        exception = true;

                        $("#divException").append("<strong> Item " + partnumber.val() + ": Please enter a valid order quantity. </strong><br/><br/>");

                        return false;
                    }
                    if (jQuery.trim(qty.val()) <= 0) {
                        exception = true;

                        $("#divException").append("<strong> Item " + partnumber.val() + ": Please enter a valid order quantity. </strong><br/><br/>");

                        return false;
                    }
                }
                return true;
            }
        }
        function AddToCart_onclick(val) {

            exception = false;
            $("#divMessage").text('');
            var buttons = $('.ui-dialog-buttonpane').children('button');
            buttons.remove();
            $("#divException").text('');
            $("#divException").hide();
            $("#divPrg").html('<span id="progress"> Please wait...<br/> <img   src="images/progbar.gif" alt ="Progres" /></span>');
            $("#divMessage").append(' <br/>');
            var totalNull = "";
            var count = 0;
            $('#hidespstextitemno').val(val)
            $('#hidespstextqty').val(1)
            var sQty = '#hidespstextqty'
            var sPart = '#hidespstextitemno'

            var qty = $(sQty);
            var part = $(sPart);

            totalNull = totalNull + qty.val() + part.val();

            if (totalNull === "" || jQuery.trim(part.val()) === "" || jQuery.trim(qty.val()) === "") {

                $("#divMessage").text('');
                ///   $("#divMessage").html("Please enter at least one part number.");
                $("#dialog").dialog({
                    //title: 'Quick Order Add to Cart Exceptions'
                    title: 'Order Add to Cart Exceptions'

                });
                $("#divMessage").append('<br/> <br/><a  ><img src ="images/sp_int_closeWindow_btn.gif" alt ="Close" onclick="javascript:return closedilog();"  id="messageclose" /></a>');
            }
            var itemsInCart = $("#itemsInCart");
            var totalPriceOfCart = $("#totalPriceOfCart");
            var destination = itemsInCart.position();
            var position = part.position();

            $("#dialog").dialog({
                //title: '<strong>Quick Order Add to Cart Status</strong>',
                title: '<strong>Order Add to Cart Status</strong>',
                height: 200,
                width: 560,
                modal: true,
                position: 'top',
                resizable: false
            });
            count = count + 1;
            if (validPart(part, qty)) {
                count = count + 1;
                $("#divMessage").html("<br/><strong>Adding item " + replaceAll(part.val(), "-", "") + " to cart... </strong><br/>");
                var parametervalues = '{sPartNumber: "' + jQuery.trim(replaceAll(part.val(), "-", "")) + '",sPartQuantity: "' + jQuery.trim(qty[0].value) + '" }';
                //Call webmethod using ajax
                jQuery.ajax({
                    type: 'POST',
                    contentType: 'application/json; charset=utf-8',
                    data: parametervalues,
                    dataType: 'json',
                    url: 'Sony-EW-PurchaseInfo.aspx/addPartToCartwSPurchase',
                    success: function (result) {
                        count = count - 1;
                        if (result.d.differentCart === true) {
                            exception = true;
                            $("#divException").append("<br/><strong>The item has been added to a cart for items to be shipped; your previous cart with items for electronic delivery has been saved</strong><br/>");
                        }

                        if (result.d.success === 1) {
                            $("#divMessage").html("<br/><strong>Adding item " + replaceAll(result.d.partnumber, "-", "") + " to cart... </strong><br/>");
                            result.d.partnumber = '';
                            result.d.Quantity = '';
                            itemsInCart.text(result.d.items);
                            totalPriceOfCart.text(result.d.totalAmount);
                        }
                        else if (result.d.success === 2) {
                            exception = true;
                            $("#divMessage").html("<br/><strong>Adding item " + replaceAll(result.d.partnumber, "-", "") + " to cart... </strong><br/>");
                            result.d.partnumber = '';
                            result.d.Quantity = '';
                            itemsInCart.text(result.d.items);
                            totalPriceOfCart.text(result.d.totalAmount);
                            $("#divException").append("<br/><strong>" + result.d.message + "</strong><br/>");
                        }
                        else {
                            exception = true;

                            $("#divException").append("<br/><strong>" + result.d.message + "</strong><br/>");
                        }
                        //$("#divMessage").html("<br/><strong>" + result.d.message + "</strong><br/>");

                        if (count === 0) {
                            if (exception) {
                                var sHeight = $("#divException").height() + 200;

                                $("#progress").remove();
                                $("#dialog").dialog({
                                    //title: 'Quick Order Add to Cart Exceptions',
                                    title: 'Order Add to Cart Exceptions',
                                    height: sHeight.toString(),
                                    position: 'top'
                                }).dialog("moveToTop");
                                $("#divMessage").text('');
                                $("#divException").append('<br/> <br/><a ><img src ="images/sp_int_closeWindow_btn.gif" alt ="Close" onclick="javascript:return closedilog();"  id="messageclose" /></a>');
                                $("#divException").show("fast");
                            }
                            else {
                                $("#dialog").dialog("close");
                            }
                        }
                    }
                });
            }
            count = count - 1;

            if (count === 0) {
                $("#progress").remove();

                if (exception) {
                    var sHeight = $("#divException").height() + 200;

                    $("#progress").remove();
                    $("#dialog").dialog({
                        //title: 'Quick Order Add to Cart Exceptions',
                        title: 'Order Add to Cart Exceptions',
                        height: sHeight.toString(),
                        position: 'top'
                    }).dialog("moveToTop");
                    $("#divMessage").text('');
                    $("#divException").append('<br/> <br/><a ><img src ="images/sp_int_closeWindow_btn.gif" alt ="Close" onclick="javascript:return closedilog();"  id="messageclose" /></a>');
                    $("#divException").show("fast");
                    return false;//6994
                }
            }
            return false;//6994
        }
    </script>
    <%--6994 ends--%>
</head>
<body style="color: #000000; background: #5d7180; margin: 0px;">
    <%--6994 starts--%>
    <div id="ValidationMessageDiv">
    </div>
    <%--<div id="dialog" title="<strong>Quick Order Add to Cart Status</strong>">--%>
    <div id="dialog" title="<strong>Order Add to Cart Status</strong>">
        <div id="divPrg" style="font: 11px; font-family: Arial; overflow: auto;"></div>
        <div id="divMessage" style="font: 11px; font-family: Arial; overflow: auto;"></div>
        <div id="divException" style="font-family: Arial, Helvetica, sans-serif; font-size: 11px; line-height: 14px; overflow: auto; display: none;">

            <img src="images/sp_int_closeWindow_btn.gif" alt="Close" onclick="javascript:return closedilog();" id="messageclose" />
        </div>
    </div>
    <%--6994 ends--%>
    <center>
        <form id="Form1" method="post" runat="server">
            <table width="760" border="0" role="presentation">
                <tr>
                    <td width="25" style="background: url('images/sp_left_bkgd.gif')">
                        <img height="25" src="images/spacer.gif" width="25" alt="">
                    </td>
                    <td width="689" bgcolor="#ffffff">
                        <table width="710" border="0" role="presentation">
                            <tr>
                                <td width="700">
                                    <asp:ScriptManager ID="ScriptManager1" runat="server" />
                                    <ServicePLUSWebApp:TopSonyHeader ID="TopSonyHeader" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td width="700">
                                    <nav><ServicePLUSWebApp:ServicePlusNav ID="SonyNav" runat="server" /></nav>
                                </td>
                            </tr>
                            <tr>
                                <td width="700">
                                    <table width="710" border="0" role="presentation">
                                        <tbody>
                                            <tr>
                                                <td valign="top" align="right" width="464" background="images/EWPageHeader.jpg" bgcolor="#363d45"
                                                    height="82">
                                                </td>
                                                <td valign="top" width="174" bgcolor="#363d45">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td bgcolor="#f2f5f8">
                                                    <table width="464" border="0" role="presentation">
                                                        <tr>
                                                            <td width="20">
                                                                <img src="images/spacer.gif" width="20" height="20" alt="">
                                                            </td>
                                                            <td>
                                                                &nbsp;
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td bgcolor="#99a8b5">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr height="9">
                                                <td bgcolor="#f2f5f8" valign="bottom" background="images/sp_int_header_btm_left_onepix.gif">
                                                    <%--<img src="images/sp_int_header_btm_left_onepix.gif" width="464" height="9" alt="">--%>
                                                </td>
                                                <td bgcolor="" valign="bottom" background="images/sp_int_header_btm_right.gif">
                                                    <%-- <img src="images/sp_int_header_btm_right.gif" width="246" height="9" alt="">--%>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="top" colspan="2" width="100%">
                                                    <table width="100%" border="0" role="presentation">
                                                        <tr>
                                                            <td width="20">
                                                                <img height="20" src="images/spacer.gif" width="20" alt="">
                                                            </td>
                                                            <td align="center">
                                                                <table width="100%" border="0" role="presentation">
                                                                    <span runat="server" id="spnTop" visible="false">
                                                                        <tr>
                                                                            <td>
                                                                                <img height="15" src="images/spacer.gif" width="20" alt="">
                                                                                <asp:Label ID="ErrorLabel" runat="server" CssClass="redAsterick" EnableViewState="false" Text="" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="2" align="left" valign="top">
                                                                                <asp:Label ID="CartMessage0" runat="server" CssClass="bodyCopyEPurchaseBold"
                                                                                    EnableViewState="False"
                                                                                    Text="An Extended Warranty has been added to your cart. To continue to checkout, click View Cart above."
                                                                                    Visible="False"></asp:Label><%--6994 visible false--%>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <img height="15" src="images/spacer.gif" width="20" alt="">
                                                                            </td>
                                                                        </tr>
                                                                        <tr height="9">
                                                                            <td colspan="2" background="images/sp_int_header_btm_left_onepix.gif">
                                                                            </td>
                                                                        </tr>
                                                                    </span>
                                                                    <tr>
                                                                        <td>
                                                                            <img height="15" src="images/spacer.gif" width="20" alt="">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="2" align="left" valign="top">
                                                                            <asp:Label ID="ErrorValidation" runat="server" CssClass="redAsterick" EnableViewState="False" Visible="false"></asp:Label>
                                                                            <asp:Label ID="CartMessage" runat="server" CssClass="bodyCopyEPurchaseBold" EnableViewState="False"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td valign="top" width="80%">
                                                                            <table width="100%" border="0" role="presentation">
                                                                                <tr>
                                                                                    <td align="left" valign="top">
                                                                                        <asp:Label ID="lblHeader" runat="server" CssClass="headerTitleEW">Sony Extended Warranty</asp:Label>
                                                                                        <br />
                                                                                        <span class="bodyCopyBold">Fields marked with a <span style="color: red">*</span> are required.</span>

                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <img src="images/spacer.gif" width="10" alt="">
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <span class="bodyCopyBold">Enter Product Purchase Date:</span>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <SPS:SPSTextBox ID="txtDate" CssClass="finderCopyDark" runat="server"></SPS:SPSTextBox>
                                                                                        <span class="bodycopy">&nbsp;&nbsp;MM/DD/YYYY</span><span class="redAsterick">*</span>
                                                                                    </td>
                                                                                </tr>

                                                                                <tr>
                                                                                    <td>
                                                                                        <img src="images/spacer.gif" width="10" alt="">
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <span class="bodyCopyBold">Enter Serial Number:</span>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <SPS:SPSTextBox ID="txtSerialNumber" runat="server" CssClass="finderCopyDark"
                                                                                            Width="60%"></SPS:SPSTextBox><span class="redAsterick">*</span>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <table role="presentation">
                                                                                            <tr>
                                                                                                <td><span class="headerTitleEW">Add to Cart: </span></td>

                                                                                                <td>
                                                                                                    <asp:ImageButton ID="SearchModel" runat="server" ImageUrl="images/sp_int_add2Cart_btn.gif"
                                                                                                        AlternateText="Parts Finder Search" ImageAlign="AbsBottom"></asp:ImageButton>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr height="9" runat="server" id="tr1">
                                                                                    <td colspan="2" background="images/sp_int_header_btm_left_onepix.gif">
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                        <%--<td width="20%" align="right" valign="middle">
                                                                        <img src="images/SonyExtendedWarranty.jpg" runat="server" id="iProductDetails" alt="Sony Extended Warranty" />
                                                                    </td>--%>
                                                                    </tr>
                                                                    <span runat="server" id="spnbottom" visible="false">
                                                                        <tr>
                                                                            <td colspan="2" height="30" align="left" valign="top">
                                                                                <img height="15" src="images/spacer.gif" width="4" alt=""><br />
                                                                                <asp:Label ID="lblAnotherPurchaseMessage" runat="server" CssClass="bodyCopyEPurchaseBold" EnableViewState="False"></asp:Label>
                                                                                <img height="15" src="images/spacer.gif" width="4" alt=""><br />
                                                                            </td>
                                                                        </tr>
                                                                        <tr height="9">
                                                                            <td colspan="2" background="images/sp_int_header_btm_left_onepix.gif">
                                                                            </td>
                                                                        </tr>
                                                                    </span>
                                                                    <tr>
                                                                        <td>
                                                                            <img height="15" src="images/spacer.gif" width="20" alt="">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="2" align="left" height="50" valign="top">
                                                                            <span class="bodyCopy" style="line-height: normal;">Extended Warranties purchased 
                                                                        on ServicesPLUS are only available for products that are still covered under the 
                                                                        standard iginal standard Limited Product Warranty term may be voided at Sony's option.
                                                                                <br />
                                                                                For information on purchasing an Extended Warranty for a product outside the standard Warranty coverage term, please contact
                                                                                <br />
                                                                                <a href="mailto:pro-ew-sales@am.sony.com">pro-ew-sales@am.sony.com</a> or call 866-807-9268, option 4.</span><br />
                                                                            <br />
                                                                        </td>
                                                                    </tr>

                                                                </table>
                                                            </td>
                                                            <td width="20">
                                                                <img height="20" src="images/spacer.gif" width="20" alt="">
                                                            </td>
                                                        </tr>
                                                        <%--6994 starts--%>
                                                        <tr>
                                                            <td align="left" colspan="3">
                                                                <SPS:SPSTextBox ID="hidespstextitemno" runat="server" Style="display: none"></SPS:SPSTextBox>
                                                                <SPS:SPSTextBox ID="hidespstextqty" runat="server" Style="display: none"></SPS:SPSTextBox>
                                                            </td>
                                                        </tr>
                                                        <%--6994 ends--%>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td width="700">
                                    <footer><ServicePLUSWebApp:SonyFooter ID="SonyFooter1" runat="server" /></footer>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="25" style="background: url('images/sp_right_bkgd.gif')">
                        <img src="images/spacer.gif" width="25" height="20" alt="">
                    </td>
                </tr>
            </table>
        </form>
    </center>
</body>
</html>
