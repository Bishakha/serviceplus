<%@ Page Language="VB" CodeFile="sony-service-agreement-reseller.aspx.vb" Inherits="ServicePLUSWebApp.sony_service_agreement_reseller"
     AutoEventWireup="false" ViewStateMode="Enabled"%>

<%@ Register TagPrefix="ServicePLUSWebApp" TagName="SonyFooter" Src="SonyFooter.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="PersonalMessage" Src="PersonalMessage.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="ServicePlusNav" Src="ServicePlusNav.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="TopSonyHeader" Src="TopSonyHeader.ascx" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" lang="<%=PageLanguage %>">
<head runat="server">
    <title><%=Resources.Resource.ttl_ProfesstionalProduct%></title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link type="text/css" href="includes/ServicesPLUS_style.css" rel="stylesheet">
    <script src="includes/jquery-3.1.1.min.js" type="text/javascript"></script>
    <script src="includes/ServicesPLUS.js" type="text/javascript"></script>
    <script src="includes/svcplus_globalization.js" type="text/javascript"></script>
</head>
<body style="color: #000000; background: #5d7180; margin: 0px;">
    <form id="form1" method="post" runat="server">
    <input type="hidden" runat="server" id="hdnNavigator" />
    <asp:ScriptManager ID="ScriptManager1" runat="server" />
    <center>

        <table width="760" border="0" role="presentation">
            <tr>
                <td style="background: url('images/sp_left_bkgd.gif'); width: 25px;">
                    <img height="25" src="images/spacer.gif" width="25" alt="">
                </td>
                <td style="background: #ffffff;">
                    <table width="710" border="0" role="presentation">
                        <tr>
                            <td>
                                <ServicePLUSWebApp:TopSonyHeader ID="TopSonyHeader" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <nav><ServicePLUSWebApp:ServicePlusNav ID="SonyNav" runat="server" /></nav>
                            </td>
                        </tr>
                        <tr>
                            <td style="vertical-align: middle;">
                                <table width="710" border="0" role="presentation">
                                    <tr style="height: 82px;">
                                        <td style="width: 464px; background: #363d45 url('images/sp_int_header_top_PartsPLUS-ServiceAgreements.gif'); vertical-align: middle; text-align: right;">
                                            <br/>
                                            <h1 class="headerText"><%=Resources.Resource.hplnk_svc_agreement%> &nbsp;</h1>
                                        </td>
                                        <td style="background: #363d45">
                                            <img src="images/spacer.gif" width="16" alt="">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="background: #f2f5f8;">
                                            <img src="images/spacer.gif" width="16" alt="">
                                        </td>
                                        <td style="background: #99a8b5;">
                                        </td>
                                    </tr>
                                    <tr style="height: 9px;">
                                        <td style="background: url('images/sp_int_header_btm_left_onepix.gif') repeat-x #f2f5f8">
                                            <img src="images/spacer.gif" height="9" style="display: block" alt="" />
                                        </td>
                                        <td style="background: url('images/sp_int_header_btm_right.gif') repeat-x #99a8b5">
                                            <img src="images/spacer.gif" height="9" style="display: block" alt="" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr style="height: 10px;">
                            <td>
                                <img src="images/spacer.gif" height="10" alt="">
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: center;" class="tableHeader">
                                To register a SupportNET Service Agreement, please fill out the information below.
                                Fields marked with an <span class="redAsterick">*</span> are required.<br />
                                <br />                                
                                <%=Resources.Resource.el_ProductReg_Contact%>
                            </td>
                        </tr>
                        <tr>
                            <td>                                
                                <asp:Label ID="lblError" runat="server" CssClass="redAsterick" />
                                <%--<asp:ValidationSummary ID="vldSummary" CssClass="redAsterick" DisplayMode="BulletList" 
                                    HeaderText="The following fields are incomplete or incorrect:" runat="server" />--%>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                        <table style="width: 90%; margin: 0px 20px; border: none; text-align: left;" role="presentation">
                                            <tr>
                                                <td>
                                                    <%-- AGREEMENT NUMBER TABLE --%>
                                                    <table style="padding-left: 3px; width: 100%; border: none;" role="presentation">
                                                        <tr style="height: 18px; background: #d5dee9;">
                                                            <td colspan="2">
                                                                <h2 class="tableHeader">Sony Professional Service (SPS) Agreement Model Number</h2>
                                                            </td>
                                                        </tr>
                                                        <tr style="height: 25px; background: #f2f5f8;">
                                                            <td class="bodyCopy" style="width: 150px; text-align: right;">
                                                                <label for="txtAgreementNumber">Agreement Number : </label>
                                                            </td>
                                                            <td>
                                                                <SPS:SPSTextBox ID="txtAgreementNumber" runat="server" CssClass="bodyCopy" MaxLength="50" required="true"/>&nbsp;<span class="redAsterick">*</span>
                                                                <asp:RequiredFieldValidator ID="vldAgreementNumber" ControlToValidate="txtAgreementNumber"
                                                                    ErrorMessage="Agreement Numbers should begin with 'SPS'."
                                                                    CssClass="redAsterick" Display="Dynamic" runat="server"/>
                                                            </td>
                                                        </tr>
                                                        <tr style="height: 25px;">
                                                            <td colspan="2">&nbsp;</td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <%-- END-USER TABLE --%>
                                                    <table style="padding-left: 3px; width: 100%; border: none;" role="presentation">
                                                        <tr style="height: 18px; background: #d5dee9;">
                                                            <td colspan="2">
                                                                <h2 class="tableHeader">End-User Information</h2>
                                                            </td>
                                                        </tr>
                                                        <tr style="height: 25px; background: #f2f5f8;">
                                                            <td class="bodyCopy" style="width: 150px; text-align: right;">
                                                                <label for="txtFirstName"><%=Resources.Resource.el_rgn_FirstName %></label>&nbsp;
                                                            </td>
                                                            <td>
                                                                <SPS:SPSTextBox ID="txtFirstName" runat="server" CssClass="bodyCopy" MaxLength="50" required="true"/>&nbsp;<span class="redAsterick">*</span>
                                                                <asp:RequiredFieldValidator ID="vldFirstName" ErrorMessage="You must enter a First Name."
                                                                    ControlToValidate="txtFirstName" CssClass="redAsterick" Display="Dynamic" runat="server"/>
                                                            </td>
                                                        </tr>
                                                        <tr style="height: 25px;">
                                                            <td class="bodyCopy" align="right">
                                                                <label for="txtLastName"><%=Resources.Resource.el_rgn_LastName %></label>&nbsp;
                                                            </td>
                                                            <td>
                                                                <SPS:SPSTextBox ID="txtLastName" runat="server" CssClass="bodyCopy" MaxLength="50" required="true"/>&nbsp;<span class="redAsterick">*</span>
                                                                <asp:RequiredFieldValidator ID="vldLastName" ErrorMessage="You must enter a Last Name."
                                                                    ControlToValidate="txtLastName" CssClass="redAsterick" Display="Dynamic" runat="server"/>
                                                            </td>
                                                        </tr>
                                                        <tr style="height: 25px;" bgcolor="#f2f5f8">
                                                            <td class="bodyCopy" align="right">
                                                                <label for="txtEmail"><%=Resources.Resource.el_EmailAdd %></label>&nbsp;
                                                            </td>
                                                            <td>
                                                                <SPS:SPSTextBox ID="txtEmail" runat="server" CssClass="bodyCopy" MaxLength="100" required="true"/>&nbsp;<span class="redAsterick">*</span>
                                                                <asp:RequiredFieldValidator ID="reqEmail" ControlToValidate="txtEmail" Display="Dynamic"
                                                                     ErrorMessage="You must enter an Email Address." CssClass="redAsterick"
                                                                runat="server"/><asp:RegularExpressionValidator
                                                                    ID="vldEmail" ControlToValidate="txtEmail" Display="Dynamic"
                                                                    CssClass="redAsterick"  ErrorMessage="You have entered an invalid Email Address."
                                                                    ValidationExpression="^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$" runat="server" />
                                                                <%-- Validators above must have no space between them to blend into one error message. --%>
                                                            </td>
                                                        </tr>
                                                        <tr style="height: 25px;">
                                                            <td class="bodyCopy" align="right">
                                                                <label for="txtCompanyName"><%=Resources.Resource.el_rgn_CompanyName %></label>&nbsp;
                                                            </td>
                                                            <td>
                                                                <SPS:SPSTextBox ID="txtCompanyName" runat="server" CssClass="bodyCopy" MaxLength="100" required="true"/>&nbsp;<span class="redAsterick">*</span>
                                                                <asp:RequiredFieldValidator ID="vldCompanyName" ErrorMessage="You must enter a valid Company Name."
                                                                    ControlToValidate="txtCompanyName" CssClass="redAsterick" Display="Dynamic" runat="server"/>
                                                            </td>
                                                        </tr>
                                                        <tr style="height: 25px;" bgcolor="#f2f5f8">
                                                            <td class="bodyCopy" align="right">
                                                                <label for="txtAddress1"><%=Resources.Resource.el_rgn_StreetAddress %></label>&nbsp;
                                                            </td>
                                                            <td>
                                                                <SPS:SPSTextBox ID="txtAddress1" runat="server" CssClass="bodyCopy" MaxLength="50" required="true"/>&nbsp;<span class="redAsterick">*</span>
                                                                <asp:RequiredFieldValidator ID="vldAddress1" ErrorMessage="You must enter a valid Address."
                                                                    ControlToValidate="txtAddress1" CssClass="redAsterick" Display="Dynamic" runat="server"/>
                                                            </td>
                                                        </tr>
                                                        <tr style="height: 25px;">
                                                            <td class="bodyCopy" align="right">
                                                                <label for="txtAddress2"><%=Resources.Resource.el_rgn_2ndAddress %></label>&nbsp;
                                                            </td>
                                                            <td>
                                                                <SPS:SPSTextBox ID="txtAddress2" runat="server" CssClass="bodyCopy" MaxLength="50"/>
                                                            </td>
                                                        </tr>
                                                        <tr style="height: 25px;" bgcolor="#f2f5f8">
                                                            <td class="bodyCopy" align="right">
                                                                <label for="txtAddress3"><%=Resources.Resource.el_Address3rdLine %></label>&nbsp;
                                                            </td>
                                                            <td>
                                                                <SPS:SPSTextBox ID="txtAddress3" runat="server" CssClass="bodyCopy" MaxLength="50"/>
                                                            </td>
                                                        </tr>
                                                        <tr style="height: 25px;">
                                                            <td class="bodyCopy" align="right">
                                                                <label for="txtCity"><%=Resources.Resource.el_rgn_City %></label>&nbsp;
                                                            </td>
                                                            <td>
                                                                <SPS:SPSTextBox ID="txtCity" runat="server" CssClass="bodyCopy" MaxLength="35" required="true"/>&nbsp;<span class="redAsterick">*</span>
                                                                <asp:RequiredFieldValidator ID="vldCity" ErrorMessage="You must enter a City."
                                                                    ControlToValidate="txtCity" CssClass="redAsterick" Display="Dynamic" runat="server"/>
                                                            </td>
                                                        </tr>
                                                        <tr style="height: 25px;" bgcolor="#f2f5f8">
                                                            <td class="bodyCopy" align="right">
                                                                <label for="ddlState">
                                                                    <% If IsCanada Then %>
                                                                        Province :
                                                                    <% Else %>
                                                                        State :
                                                                    <% End If %>
                                                                </label>&nbsp;
                                                            </td>
                                                            <td>
                                                                <asp:DropDownList ID="ddlState" CssClass="bodyCopy" runat="server" required="true" />&nbsp;<span class="redAsterick">*</span>
                                                                <asp:RequiredFieldValidator ID="vldState" ErrorMessage="You must select a State."
                                                                    ControlToValidate="ddlState" CssClass="redAsterick" Display="Dynamic" runat="server"/>
                                                            </td>
                                                        </tr>
                                                        <tr style="height: 25px;">
                                                            <td class="bodyCopy" align="right">
                                                                <label for="txtZip">
                                                                    <% If IsCanada Then %>
                                                                        Postal Code :
                                                                    <% Else %>
                                                                        Zip Code :
                                                                    <% End If %>
                                                                </label>&nbsp;
                                                            </td>
                                                            <td>
                                                                <SPS:SPSTextBox ID="txtZip" runat="server" CssClass="bodyCopy" MaxLength="10" required="true"/>&nbsp;<span class="redAsterick">*</span>
                                                                <asp:RegularExpressionValidator ID="vldPostalCode" ControlToValidate="txtZip" Display="Dynamic"
                                                                    ValidationExpression="^[ABCEGHJKLMNPRSTVXY]{1}\d{1}[A-Z]{1} *\d{1}[A-Z]{1}\d{1}$"
                                                                        ErrorMessage="You have entered an invalid Postal Code."
                                                                    CssClass="redAsterick" runat="server"/>
                                                                <asp:RegularExpressionValidator ID="vldZipCode" Display="Dynamic"
                                                                    ControlToValidate="txtZip" ErrorMessage="You have entered an invalid Zip Code."
                                                                    ValidationExpression="^\d{5}(-\d{4})?$" CssClass="redAsterick" runat="server" />
                                                                <asp:RequiredFieldValidator
                                                                    ID="reqZipCode" ErrorMessage="You must enter a Zip/Postal Code." Display="Dynamic"
                                                                    ControlToValidate="txtZip" CssClass="redAsterick" runat="server"/>
                                                                <%-- Validators above must have no space between them to blend into one error message. --%>
                                                            </td>
                                                        </tr>
                                                        <tr style="height: 25px;" bgcolor="#f2f5f8">
                                                            <td class="bodyCopy" align="right">
                                                                <label for="txtPhone"><%=Resources.Resource.el_rgn_Phone %></label>&nbsp;
                                                            </td>
                                                            <td>
                                                                <table border="0" align="left" role="presentation">
                                                                    <tr>
                                                                        <td style="width: 165px;">
                                                                            <SPS:SPSTextBox ID="txtPhone" CssClass="bodyCopy" MaxLength="12" runat="server" required="true"/><span class="redAsterick">&nbsp;*</span>
                                                                        </td>
                                                                        <td style="width: 70px; text-align: right;">
                                                                            <label for="txtExtension" class="bodyCopy"><%=Resources.Resource.el_rgn_Extension%></label>&nbsp;
                                                                        </td>
                                                                        <td>
                                                                            <SPS:SPSTextBox ID="txtExtension" CssClass="bodyCopy" Width="50" MaxLength="6" runat="server"/>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="4">
                                                                            <%--<asp:RegularExpressionValidator ID="vldPhone" ControlToValidate="txtPhone" Display="Dynamic"
                                                                                ErrorMessage="Phone number must be in the following format: 123-555-1234."
                                                                                ValidationExpression="^\(?([0-9]{3})\)?[-]([0-9]{3})[-]([0-9]{4})$" CssClass="redAsterick"
                                                                            runat="server" />--%>
                                                                            <asp:Label ID="lblPhoneError" CssClass="redAsterick" runat="server" />
                                                                            <asp:RequiredFieldValidator ID="reqPhone" ControlToValidate="txtPhone"
                                                                                ErrorMessage="You must enter a Phone Number."
                                                                                Display="Dynamic" CssClass="redAsterick" runat="server"/>
                                                                            <%-- Validators above must have no space between them to blend into one error message. --%>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr style="height: 25px;">
                                                            <td class="bodyCopy" align="right">
                                                                <label for="txtFax"><%=Resources.Resource.el_rgn_Fax%></label>&nbsp;
                                                            </td>
                                                            <td class="bodyCopy">
                                                                <SPS:SPSTextBox ID="txtFax" CssClass="bodyCopy" MaxLength="12" runat="server"/>
                                                                <asp:Label ID="lblFaxError" CssClass="redAsterick" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr style="height: 25px;">
                                                            <td>
                                                                <img height="25" src="images/spacer.gif" alt="">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <%-- RESELLER TABLE --%>
                                                    <table style="padding-left: 3px; width: 100%; border: none;" role="presentation">
                                                        <tr style="height: 18px;" bgcolor="#d5dee9">
                                                            <td colspan="2">
                                                                <h2 class="tableHeader">Reseller Information</h2>
                                                            </td>
                                                        </tr>
                                                        <tr style="height: 25px; background: #f2f5f8;">
                                                            <td class="bodyCopy" style="width: 150px; text-align: right;">
                                                                <label for="txtResellerCompany">Reseller <%=Resources.Resource.el_rgn_CompanyName %></label>&nbsp;
                                                            </td>
                                                            <td class="bodyCopy">
                                                                <SPS:SPSTextBox ID="txtResellerCompany" CssClass="bodyCopy" runat="server" MaxLength="100"/>
                                                            </td>
                                                        </tr>
                                                        <tr style="height: 25px;">
                                                            <td class="bodyCopy" align="right">
                                                                <label for="txtResellerName">Reseller Name :</label>&nbsp;
                                                            </td>
                                                            <td class="bodyCopy">
                                                                <SPS:SPSTextBox ID="txtResellerName" CssClass="bodyCopy" MaxLength="80" runat="server"/>
                                                            </td>
                                                        </tr>
                                                        <tr style="height: 25px; background: #f2f5f8;">
                                                            <td class="bodyCopy" align="right">
                                                                <label for="txtResellerPhone"><%=Resources.Resource.el_rgn_Phone%></label>&nbsp;
                                                            </td>
                                                            <td>
                                                                <table border="0" align="left" role="presentation">
                                                                    <tr>
                                                                        <td style="width: 165px;">
                                                                            <SPS:SPSTextBox ID="txtResellerPhone" CssClass="bodyCopy" MaxLength="12" runat="server"/>                                                                    
                                                                        </td>
                                                                        <td style="width: 70px; text-align: right;">
                                                                            <label for="txtResellerExtension" class="bodyCopy"><%=Resources.Resource.el_rgn_Extension%></label>&nbsp;
                                                                        </td>
                                                                        <td>
                                                                            <SPS:SPSTextBox ID="txtResellerExtension" CssClass="bodyCopy" Width="50" MaxLength="6" runat="server" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="3">
                                                                            <asp:Label ID="lblResellerPhoneError" CssClass="redAsterick" runat="server" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr style="height: 25px;">
                                                            <td colspan="2">&nbsp;</td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <%-- FILE UPLOAD / PROOF OF PURCHASE TABLE --%>
                                                    <table style="padding-left: 3px; width: 100%; border: none;" role="presentation">
                                                        <tr style="height: 18px; background: #d5dee9;">
                                                            <td>
                                                                <h2 class="tableHeader">Proof of Purchase</h2>
                                                            </td>
                                                        </tr>
                                                        <tr style="height: 55px; background: #f2f5f8; vertical-align: top;">
                                                            <td class="bodyCopyBold">
                                                                <label for="fuReceipt">Please upload a scan of your Service Agreement receipt.<br />
                                                                <br />
                                                                Accepted file formats: .JPG, .GIF, .PDF </label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:FileUpload ID="fuReceipt" CssClass="bodyCopy" EnableViewState="true" runat="server" required="true" />&nbsp;<span class="redAsterick">*</span>
                                                                <asp:RequiredFieldValidator ID="vldReceipt" ControlToValidate="fuReceipt"
                                                                    ErrorMessage="You must attach a scan of your receipt."
                                                                    CssClass="redAsterick" Display="Dynamic" runat="server"/>
                                                                <asp:Label ID="lblReceiptRedo" CssClass="redAsterick" Visible="false" runat="server">Please select your file again.</asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr style="height: 25px;">
                                                            <td>
                                                                <img height="25" src="images/spacer.gif" alt="">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <%-- EQUIPMENT INFO TABLE --%>     
                                                    <asp:UpdatePanel ID="updEquipment" UpdateMode="Conditional" runat="server">
                                                        <ContentTemplate>                                                                                                                   
                                                            <table style="padding-left: 3px; width: 100%; border: none;" role="presentation">
                                                                <tr style="height: 18px; background: #d5dee9;">
                                                                    <td colspan="4">
                                                                        <h2 class="tableHeader">Equipment Information</h2>
                                                                    </td>
                                                                </tr>
                                                                <tr style="background: #f2f5f8;">
                                                                    <td class="tableHeader">Model Number</td>
                                                                    <td class="tableHeader">Serial Number</td>
                                                                    <td class="tableHeader">Date Purchased</td>
                                                                    <td>&nbsp;</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <SPS:SPSTextBox ID="txtModelNumber" MaxLength="30" CssClass="tableData" runat="server" /> <%-- required="true"--%>
                                                                    </td>
                                                                    <td>
                                                                        <SPS:SPSTextBox ID="txtSerialNumber" MaxLength="30" CssClass="tableData" runat="server" /> <%-- required="true"--%>
                                                                    </td>
                                                                    <td>
                                                                        <SPS:SPSTextBox ID="txtPurchaseDate" MaxLength="10" CssClass="tableData" runat="server" /> <%-- required="true"--%>
                                                                        <span class="bodyCopy">(mm/dd/yyyy)</span>
                                                                    </td>
                                                                    <td>
                                                                        <asp:ImageButton ID="btnAddProduct" ImageUrl="~/images/sp_int_add_btn.GIF" AlternateText="Add Product" CausesValidation="false" runat="server" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="4">
                                                                        <asp:Label ID="lblProductError" CssClass="redAsterick" runat="server" />
                                                                        <br />
                                                                        <asp:GridView ID="gvProducts" AutoGenerateColumns="false" Width="550" ShowHeaderWhenEmpty="True" runat="server">
                                                                            <HeaderStyle Font-Bold="true" CssClass="tableHeader" />
                                                                            <RowStyle CssClass="tableData" />
                                                                            <AlternatingRowStyle CssClass="tableData" BackColor="#f2f5f8" />
                                                                            <EmptyDataTemplate>
                                                                                <span class="tableHeader">You must input at least one product using the text boxes above.</span>
                                                                            </EmptyDataTemplate>
                                                                            <Columns>
                                                                                <asp:BoundField DataField="Model" HeaderText="Model Number"  />
                                                                                <asp:BoundField DataField="Serial" HeaderText="Serial Number" />
                                                                                <asp:BoundField DataField="DatePurchased" HeaderText="Date Purchased" DataFormatString="{0:MMM dd, yyyy}" HtmlEncode="false" />
                                                                                <asp:CommandField ButtonType="Link" ShowDeleteButton="true" />
                                                                            </Columns>
                                                                        </asp:GridView>
                                                                    </td>
                                                                </tr>
                                                                <tr style="height: 25px;">
                                                                    <td colspan="4">&nbsp;</td>
                                                                </tr>
                                                            </table>
                                                        </ContentTemplate>
                                                        <Triggers>
                                                            <asp:PostBackTrigger ControlID="imgNext" />
                                                        </Triggers>
                                                    </asp:UpdatePanel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 100%; text-align: right; vertical-align: middle;">
                                                    <asp:ImageButton ID="imgNext" ImageUrl="images/sp_int_submit_btn.gif" runat="server"
                                                        CausesValidation="true" AlternateText="Submit the Service Agreement" />
                                                </td>
                                            </tr>
                                            <tr style="height: 18px;">
                                                <td>
                                                    <img src="images/spacer.gif" height="18" alt="">
                                                </td>
                                            </tr>
                                        </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="1">
                                <footer><ServicePLUSWebApp:SonyFooter ID="SonyFooter1" runat="server" /></footer>
                            </td>
                        </tr>
                    </table>
                </td>
                <td style="background: url('images/sp_right_bkgd.gif'); width: 25px;">
                    <img src="images/spacer.gif" width="25" height="20" alt="">
                </td>
            </tr>
        </table>

    </center>
    </form>
</body>
</html>
