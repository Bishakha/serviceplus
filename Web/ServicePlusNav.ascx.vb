Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.SIAMUtilities
Imports ServicesPlusException

Namespace ServicePLUSWebApp

    Partial Class ServicePlusNav
        Inherits System.Web.UI.UserControl

#Region " Web Form Designer Generated Code "
        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub
#End Region

#Region "page members"
        'Private _callingPage As String = "NoPageDefined"
        'Private _setByProperty As Boolean = False
        'Private pageType As enumPageType = enumPageType.NoPageDefined
        'Private WithEvents NewCart As LinkButton
        'Private WithEvents LogInImage As ImageButton
        'Private WithEvents LogIn As HyperLink
        'Private WithEvents LogOut As ImageButton

        'Private servicePlusPage As String = "Members.aspx"
        'Private partsPlusPage As String = "sony-parts.aspx"
        'Private softwarePlusPage As String = "sony-software.aspx"
        'Private trainingPlusPage As String = "sony-training-catalog.aspx"

        Protected isLoggedIn As Boolean = False
        Protected itemsInCart As Integer = 0
        Protected totalPriceOfCart As Double = 0
        'Protected loginUrl As String = ""

        'Public Enum enumPageType
        '    ServicePlus = 0
        '    PartsPlus = 1
        '    SoftwarePlus = 2
        '    NoPageDefined = 3
        '    TrainingPlus = 4
        'End Enum

#Region "Properties"
        'Public Property CallingPage() As enumPageType
        '    Get
        '        Return _callingPage
        '    End Get
        '    Set(ByVal Value As enumPageType)
        '        _callingPage = Value
        '        _setByProperty = True
        '    End Set
        'End Property


        'Public Property ServicePlus_Page() As String
        '    Get
        '        Return servicePlusPage
        '    End Get
        '    Set(ByVal Value As String)
        '        servicePlusPage = Value
        '    End Set
        'End Property

        'Public Property PartsPlus_Page() As String
        '    Get
        '        Return partsPlusPage
        '    End Get
        '    Set(ByVal Value As String)
        '        partsPlusPage = Value
        '    End Set
        'End Property

        'Public Property SoftwarePlus_Page() As String
        '    Get
        '        Return softwarePlusPage
        '    End Get
        '    Set(ByVal Value As String)
        '        softwarePlusPage = Value
        '    End Set
        'End Property

        'Public Property TrainingPlus_Page() As String
        '    Get
        '        Return trainingPlusPage
        '    End Get
        '    Set(ByVal Value As String)
        '        trainingPlusPage = Value
        '    End Set
        'End Property
#End Region

#End Region

#Region "Events"

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                'NewCart As New ImageButton
                'NewCart = New LinkButton
                'NewCart.ID = "NewCart"
                ''Adding object to page
                'Me.Controls.Add(NewCart)
                'Pass language to siteminder
                ' 2016-05-19 ASleight - Updating for Shibboleth IDM
                'Dim strLanguage As String = "EN"
                'If (Session("SelectedLang") IsNot Nothing) Then
                '    strLanguage = Session("SelectedLang").ToString().Substring(0, 2).ToUpper()
                'End If
                'Dim objGlobalData As GlobalData = Nothing
                'If Session.Item("GlobalData") IsNot Nothing Then
                '    objGlobalData = Session.Item("GlobalData")
                'End If
                isLoggedIn = (HttpContextManager.Customer IsNot Nothing)

                ' Use the Global Data to set up some URLs and set Languages
                'If HttpContextManager.GlobalData.IsCanada Then
                '    If HttpContextManager.GlobalData.IsEnglish Then
                '        loginUrl = ConfigurationData.Environment.IdentityURLs.LoginENCA
                '    Else
                '        loginUrl = ConfigurationData.Environment.IdentityURLs.LoginFRCA
                '    End If
                'Else
                '    loginUrl = ConfigurationData.Environment.IdentityURLs.LoginUS
                'End If

                ' Set up the Shopping Cart variables
                If isLoggedIn Then
                    itemsInCart = getCountOfItemsInCart()
                    totalPriceOfCart = getTotalPriceOfCart()
                End If

                'LogIn = New HyperLink
                'LogIn.ImageUrl = "images/sp_hdr_login_btn.gif"
                'LogIn.ID = "LogIn"
                'LogIn.Style.Add("cursor", "hand")
                'LogIn.Attributes.Add("onclick", "openPopupWindow('" + loginUrl + "')")
                'Me.Controls.Add(LogIn)

                '    LogOut = New ImageButton
                '    LogOut.ImageUrl = "images/sp_hdr_logout_btn1.gif"
                '    LogOut.ID = "LogOut"
                '    LogOut.Style.Add("cursor", "hand")
                '    LogOut.AlternateText = "Logout"
                '    Me.Controls.Add(LogOut)
                'Catch threadEx As Threading.ThreadAbortException
                '    'do nothing
            Catch ex As Exception
                Utilities.WrapException(ex)
            End Try
        End Sub

        Private Sub Page_PreRender(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.PreRender
            'Put user code to initialize the page here
            'buildMainNav()
        End Sub
#End Region

#Region "Methods"
        'Private Sub processControl()
        '    buildMainNav() 'pageType)
        'End Sub

        'Private Sub buildMainNav() 'ByVal pageType As enumPageType)
        'Dim tr As New TableRow
        'Dim td As New TableCell

        'MainNav.Controls.Clear()
        ''Necessary to remove new cart button here because we have already added it to the controls collection during page load
        'Me.Controls.Remove(NewCart)
        'Me.Controls.Remove(LogIn)
        'Me.Controls.Remove(LogOut)

        'tr.Controls.Clear()
        'td.Controls.Clear()

        'td.Controls.Add(New LiteralControl(vbCrLf + "<table cellSpacing=""0"" cellPadding=""0"" width=""710px"" border=""0"">" + vbCrLf + vbTab + "<tr>" + vbCrLf))
        'td.Controls.Add(New LiteralControl(vbTab + vbTab + "<td  height=""29px""  background=""images/sp_hm_btm_bar.gif"" align=""center"" valign=""center"" width=""88.75px"">"))

        '' Home Page
        'If isLoggedIn Then
        '    td.Controls.Add(New LiteralControl("<a href=""Member.aspx"" class=""menuClass"">"))
        'Else
        '    td.Controls.Add(New LiteralControl("<a href=""default.aspx"" class=""menuClass"">"))
        'End If
        'td.Controls.Add(New LiteralControl(Resources.Resource.menu_home & "</a>"))
        'td.Controls.Add(New LiteralControl("</td>" + vbCrLf))

        '' Knowledge Base
        'td.Controls.Add(New LiteralControl(vbTab + vbTab + "<td height=""29px""  background=""images/sp_hm_btm_bar.gif"" align=""center"" valign=""top"" width=""88.75px"">"))
        'If isLoggedIn Then
        '    td.Controls.Add(New LiteralControl("<a href=""au-sony-knowledge-base-search.aspx"" class=""menuClassSA"">"))
        'Else
        '    td.Controls.Add(New LiteralControl("<a href=""sony-knowledge-base-search.aspx"" class=""menuClassSA"">"))
        'End If
        'td.Controls.Add(New LiteralControl(Resources.Resource.menu_knowldgebase & "</a></td>" + vbCrLf))

        '' Manuals
        'td.Controls.Add(New LiteralControl(vbTab + vbTab + "<td  height=""29px""   background=""images/sp_hm_btm_bar.gif"" align=""center"" valign=""center""  width=""88.75px"">"))
        'td.Controls.Add(New LiteralControl("<a href=""sony-operation-manual.aspx"" class=""menuClassOM"">"))
        'td.Controls.Add(New LiteralControl(Resources.Resource.menu_manuals & "</a></td>" + vbCrLf))

        '' Parts
        'td.Controls.Add(New LiteralControl(vbTab + vbTab + "<td  height=""29px""   background=""images/sp_hm_btm_bar.gif"" align=""center"" valign=""center""  width=""88.75px"">"))
        'td.Controls.Add(New LiteralControl("<a href=""sony-parts.aspx"" class=""menuClass"">"))
        'td.Controls.Add(New LiteralControl(Resources.Resource.menu_parts & "</a></td>" + vbCrLf))

        '' Repair
        'td.Controls.Add(New LiteralControl(vbTab + vbTab + "<td  height=""29px""  background=""images/sp_hm_btm_bar.gif"" align=""center"" valign=""center"" width=""88.75px"">"))
        'td.Controls.Add(New LiteralControl("<a href=""sony-repair.aspx"" class=""menuClass"">"))
        'td.Controls.Add(New LiteralControl(Resources.Resource.menu_repair & "</a></td>" + vbCrLf))

        '' Software
        'td.Controls.Add(New LiteralControl(vbTab + vbTab + "<td  height=""29px""  background=""images/sp_hm_btm_bar.gif"" align=""center"" valign=""center""  width=""88.75px"">"))
        'td.Controls.Add(New LiteralControl("<a href=""sony-software.aspx"" class=""menuClass"">"))
        'td.Controls.Add(New LiteralControl(Resources.Resource.menu_software & "</a></td>" + vbCrLf))

        '' Training
        'td.Controls.Add(New LiteralControl(vbTab + vbTab + "<td  height=""29px"" background=""images/sp_hm_btm_bar.gif"" align=""center"" valign=""center"" width=""88.75px"">"))
        'td.Controls.Add(New LiteralControl("<a href=""sony-training-catalog.aspx"" class=""menuClass"">"))
        'td.Controls.Add(New LiteralControl(Resources.Resource.menu_training & "</a></td>" + vbCrLf))

        '' Service Agreements
        'td.Controls.Add(New LiteralControl(vbTab + vbTab + "<td height=""29px""  background=""images/sp_hm_btm_bar.gif"" align=""center"" valign=""top"" width=""88.75px"">"))
        'td.Controls.Add(New LiteralControl("<a href=""sony-service-agreements.aspx"" height=""33px"" class=""menuClassSA"">"))
        'td.Controls.Add(New LiteralControl(Resources.Resource.hplnk_svc_agreement & "</a></td>" + vbCrLf))

        'td.Controls.Add(New LiteralControl(vbTab + "</tr>" + vbCrLf))

        'If isLoggedIn Then

        '    itemsInCart = getCountOfItemsInCart()
        '    totalPriceOfCart = getTotalPriceOfCart()
        '    NewCart.CssClass = "PromoWhiteText"
        '    NewCart.Text = Resources.Resource.mem_itms_nwcart_msg

        '    td.Controls.Add(New LiteralControl("<tr>"))
        '    td.Controls.Add(New LiteralControl("<td align=""left"" bgcolor=""black"" colspan=""5"">&nbsp;" + vbCrLf))
        '    td.Controls.Add(New LiteralControl("&nbsp;<img src=" + Resources.Resource.mem_vwcart_img + ">&nbsp;<a class=""PromoWhiteText"" href=""vc.aspx"">" + Resources.Resource.mem_curnt_cart_msg + "</a>" + vbCrLf))
        '    td.Controls.Add(New LiteralControl("&nbsp;<span class=""PromoWhiteText"">|</span>&nbsp;<span class=""PromoWhiteText"">" + Resources.Resource.mem_itms_cart_msg + "</span>&nbsp;<span class=""redAsterick"" id=""itemsInCart"">" + itemsInCart.ToString() + "</span>" + vbCrLf))
        '    td.Controls.Add(New LiteralControl("&nbsp;<span class=""PromoWhiteText"">|</span>&nbsp;<span class=""PromoWhiteText"">" + Resources.Resource.mem_itms_ttcart_msg + "</span>&nbsp;<span class=""redAsterick"" id=""totalPriceOfCart"">" + String.Format("{0:c}", totalPriceOfCart).ToString() + "</span>" + vbCrLf))
        '    td.Controls.Add(New LiteralControl("&nbsp;</td>" + vbCrLf))

        '    td.Controls.Add(New LiteralControl("<td align=""right"" bgcolor=""black"" colspan=4>&nbsp;" + vbCrLf))
        '    td.Controls.Add(New LiteralControl("&nbsp;<a class=""PromoWhiteText"" href=""vc.aspx"">" + Resources.Resource.mem_itms_vwcart_msg + "</a>" + vbCrLf))
        '    td.Controls.Add(New LiteralControl("<span class=""PromoWhiteText"">|</span>&nbsp;<a class=""PromoWhiteText"" href=""CustomerSavedCarts.aspx"">" + Resources.Resource.mem_itms_svcart_msg + "</a>" + vbCrLf))
        '    td.Controls.Add(New LiteralControl("<span class=""PromoWhiteText"">|</span>&nbsp;" + vbCrLf))
        '    td.Controls.Add(NewCart)
        '    td.Controls.Add(New LiteralControl("&nbsp;" + vbCrLf))

        '    td.Controls.Add(New LiteralControl("&nbsp;</td>" + vbCrLf))

        '    td.Controls.Add(New LiteralControl(vbTab + "</tr>" + vbCrLf))
        'End If

        'td.Controls.Add(New LiteralControl("</table>" + vbCrLf))
        'tr.Controls.Add(td)
        'MainNav.Controls.Add(tr)
        'End Sub

        Private Function getCountOfItemsInCart() As Integer
            Dim returnValue As Integer = 0

            Try
                If Session.Item("carts") IsNot Nothing Then returnValue = CType(Session.Item("carts"), Sony.US.ServicesPLUS.Core.ShoppingCart).ShoppingCartItems.Length
            Catch ex As Exception
                Utilities.WrapException(ex)
                'Response.Write(ex.Message.ToString())
            End Try

            Return returnValue
        End Function

        Private Function getTotalPriceOfCart() As Double
            Dim returnValue As Double = 0.0

            Try
                If Session.Item("carts") IsNot Nothing Then returnValue = CType(Session.Item("carts"), Sony.US.ServicesPLUS.Core.ShoppingCart).ShoppingCartTotalPrice
            Catch ex As Exception
                Utilities.WrapException(ex)
                'Response.Write(ex.Message.ToString())
            End Try

            Return returnValue
        End Function

        Public Sub NewCart_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NewCart.Click
            Session.Remove("carts")
            Session.Add("NewCart", "True")
            Response.Redirect("vc.aspx")
        End Sub

        'Public Sub LogOut_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles LogOut.Click
        '    Try
        '        Session.Add("logout", "true")
        '        Response.Redirect("default.aspx")
        '        Response.End()
        '    Catch threadEx As Threading.ThreadAbortException
        '        'do nothing
        '    Catch ex As Exception
        '        Response.Write(ex.Message())
        '    End Try
        'End Sub

#End Region

    End Class

End Namespace
