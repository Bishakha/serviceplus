Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Process
Imports ServicesPlusException
Imports Sony.US.AuditLog
Imports System.Data

Namespace ServicePLUSWebApp

    Partial Public Class partspromotions
        Inherits SSL

#Region " Web Form Designer Generated Code "
        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            isProtectedPage(False)
            InitializeComponent()
            'load promotion inserts
            If HttpContextManager.Customer IsNot Nothing Then
                ViewStateUserKey = System.Guid.NewGuid().ToString()
            End If
        End Sub
#End Region

        Protected promotionInsert1 As PromotionInsert
        Public cm As New CustomerManager
        Public Customer As Customer
        Public models() As Model
        Public SearchType As String
        Private selectedItemsBeforeLoggedIn As ArrayList

        Protected Sub Save_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
            System.Threading.Thread.Sleep(1000)
        End Sub

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                If HttpContextManager.Customer IsNot Nothing Then
                    'After Login
                    Customer = HttpContextManager.Customer
                    Dim addToCart As String = "False"
                    If Request.QueryString("atc") IsNot Nothing Then
                        addToCart = Request.QueryString("atc")
                    End If
                    If addToCart = "true" Then
                        'AddPartToCart()
                        'Add to cart icon clicked after login
                        If Request.QueryString("PValue") IsNot Nothing Then
                            Dim whichPart As String = Request.QueryString("PValue")
                            If ValidateRequest(whichPart) Then  'Added the code by Arshad for BUG: 9051
                                whichPart.Replace("'", String.Empty)
                                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "call popup", $"AddToCart_onclick('{whichPart}','Part');", True)
                            Else
                                Page.ClientScript.RegisterStartupScript(Me.GetType(), "ValidatePartAndKits", "ValidatePartAndKits()", True)
                            End If
                        Else 'it is the kit
                            Dim whichKit As String
                            whichKit = Request.QueryString("kitVal")
                            If ValidateRequest(whichKit) Then 'Added the code by Arshad for BUG: 9051
                                whichKit.Replace("'", String.Empty)
                                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "call popup", $"AddToCart_onclick('{whichKit}','Kit');", True)
                            Else
                                Page.ClientScript.RegisterStartupScript(Me.GetType(), "ValidatePartAndKits", "ValidatePartAndKits()", True)
                            End If
                        End If
                    Else
                        'Add to cart icon clicked before login
                        selectedItemsBeforeLoggedIn = CType(Session("SelectedItemsBeforeLoggedIn"), ArrayList)
                        'Added the below event to work in common for both parts and kits(bug 9007 and 9008)
                        If selectedItemsBeforeLoggedIn IsNot Nothing Then
                            For Each item In selectedItemsBeforeLoggedIn
                                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "call popup", $"AddToCart_onclick('{item.PartNumber}','{item.GetType().Name}');", True)
                                'Clear the Session after adding to cart
                                Session("SelectedItemsBeforeLoggedIn") = Nothing
                            Next
                        End If
                    End If
                End If

            Catch thrEX As Threading.ThreadAbortException
                '-- do nothing
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
            'AjaxPro.Utility.RegisterTypeForAjax(GetType(partspromotions))
        End Sub
        'Added the code by Arshad for BUG: 9051
        Public Function ValidateRequest(ByVal strInputValue As String) As Boolean
            Dim SpecialChars() As Char = "!@#$%^&*()'".ToCharArray
            Dim indexOf As Integer = strInputValue.IndexOfAny(SpecialChars)
            If (indexOf = -1) Then
                Return True
            Else
                Return False
            End If
        End Function

        '<AjaxPro.AjaxMethod()> _
        Private Sub AddPartToCart(ByVal whichPart As String)
            Dim objPCILogger As New PCILogger() '6524
            Dim deliveryMethod As ShoppingCartItem.DeliveryMethod
            Dim cart As ShoppingCart
            Dim promotionManager As PromotionManager = New PromotionManager
            Dim sm As New ShoppingCartManager
            Dim parts As Sony.US.ServicesPLUS.Core.Part
            Try

                If HttpContextManager.Customer IsNot Nothing Then
                    Customer = HttpContextManager.Customer
                End If

                parts = promotionManager.GetPartInPromotion(whichPart)
                deliveryMethod = ShoppingCartManager.getDeliveryMethod(parts.ProgramCode)
                cart = getShoppingCart(Customer, deliveryMethod, ErrorLabel)

                If Not IsCurrentCartSameType(cart) Then
                    ErrorLabel.Visible = True
                    If cart.Type <> ShoppingCart.enumCartType.PrintOnDemand Then
                        ErrorLabel.Text = "The item is put to different cart from the previous cart."
                    Else
                        ErrorLabel.Text = "The print on demand item is put to different cart from the previous cart."
                    End If
                End If
                sm.AddProductToCart(HttpContextManager.GlobalData, cart, parts, ShoppingCartItem.ItemOriginationType.Search, ShoppingCartItem.DeliveryMethod.Ship)
                Session("carts") = cart
            Catch ex As Exception
                objPCILogger.CustomerID = Customer.CustomerID
                objPCILogger.CustomerID_SequenceNumber = customer.SequenceNumber
                objPCILogger.EmailAddress = Customer.EmailAddress
                objPCILogger.SIAM_ID = Customer.SIAMIdentity
                objPCILogger.LDAP_ID = Customer.LdapID
                objPCILogger.EventOriginApplication = "ServicesPLUS"
                objPCILogger.EventOriginApplicationLocation = Me.Page.GetType().Name
                objPCILogger.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
                objPCILogger.OperationalUser = Environment.UserName
                objPCILogger.EventDateTime = DateTime.Now.ToLongTimeString()
                objPCILogger.HTTPRequestObjectValues = objPCILogger.GetRequestContextValue()
                objPCILogger.EventOriginMethod = "AddPartToCart"
                objPCILogger.EventType = EventType.Add
                objPCILogger.IndicationSuccessFailure = "Failure"
                objPCILogger.Message = "AddPartToCart Failed. " & ex.Message.ToString()
                objPCILogger.PushLogToMSMQ()
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        '<AjaxPro.AjaxMethod()> _
        Private Sub AddKitToCart(ByVal whichKit As String) 'As String()
            Dim cart As ShoppingCart
            Dim sm As New ShoppingCartManager
            Dim promotionManager As PromotionManager = New PromotionManager
            Dim kit As Sony.US.ServicesPLUS.Core.Kit
            Dim objPCILogger As New PCILogger()
            Try
                If HttpContextManager.Customer IsNot Nothing Then
                    Customer = HttpContextManager.Customer
                End If

                kit = promotionManager.GetKitInPromotion(whichKit)
                cart = getShoppingCart(Customer, ShoppingCartItem.DeliveryMethod.Ship, Me.ErrorLabel) 'Kit always is shipped
                sm.AddProductToCart(HttpContextManager.GlobalData, cart, kit, ShoppingCartItem.ItemOriginationType.Search)
                Session("carts") = cart
            Catch ex As Exception
                objPCILogger.EventOriginApplication = "ServicesPLUS"
                objPCILogger.EventOriginApplicationLocation = Me.Page.GetType().Name
                objPCILogger.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
                objPCILogger.OperationalUser = Environment.UserName
                objPCILogger.EventDateTime = DateTime.Now.ToLongTimeString()
                objPCILogger.HTTPRequestObjectValues = objPCILogger.GetRequestContextValue()
                objPCILogger.EventOriginMethod = "AddKitToCart"
                objPCILogger.EventType = EventType.Add
                objPCILogger.CustomerID = Customer.CustomerID
                objPCILogger.CustomerID_SequenceNumber = Customer.SequenceNumber
                objPCILogger.EmailAddress = Customer.EmailAddress
                objPCILogger.SIAM_ID = Customer.SIAMIdentity
                objPCILogger.LDAP_ID = Customer.LdapID
                objPCILogger.IndicationSuccessFailure = "Failure"
                objPCILogger.Message = "AddKitToCart Failed. " & ex.Message.ToString()
                objPCILogger.PushLogToMSMQ()
            End Try
        End Sub

        Public Sub AddPartToCart()
            Try
                If Customer Is Nothing Then
                    ErrorLabel.Text = "You must be logged in to add items to your cart."
                    Return
                End If

                If Request.QueryString("PValue") IsNot Nothing Then
                    Dim whichPart As String = Request.QueryString("PValue")
                    AddPartToCart(whichPart)
                Else 'it is the kit
                    Dim whichKit As String = Request.QueryString("kitVal")
                    AddKitToCart(whichKit)
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        'Modified the below event to work in common for both parts and kits(bug 9007 and 9008)
        <System.Web.Services.WebMethod(EnableSession:=True)>
        Public Shared Function addPartToCartwSPromotions(ByVal sOldPartNumber As String, ByVal sNewPartNumber As String, ByVal type As String) As PartresultPromotions '6994
            Dim objPCILogger As New PCILogger() '6524
            'Dim cm As CourseManager = Nothing
            Dim sm As New ShoppingCartManager
            Dim customer As New Customer
            Dim lmessage As New Label
            Dim objSSL As New SSL
            Dim cart As ShoppingCart
            Dim differentCart As Boolean = False
            Dim Conflict As Boolean = False
            Dim message As String
            Dim sPartNumberUpperCase As String = sNewPartNumber.ToUpper()
            Dim objResult As New PartresultPromotions With {
                .partnumber = sPartNumberUpperCase,
                .Quantity = 1
            }

            If HttpContextManager.Customer IsNot Nothing Then
                customer = HttpContextManager.Customer
            End If
            Dim carts As ShoppingCart() = objSSL.getAllShoppingCarts(customer, lmessage)
            message = lmessage.Text
            objResult.success = False

            Try
                If type = "Part" Then
                    If sOldPartNumber <> sNewPartNumber Then
                        sm.sourcePart = sOldPartNumber
                    End If
                End If

                ''Output1 value return form SAP helpler
                Dim output1 As String = String.Empty
                cart = sm.AddProductToCartQuickCart(carts, sPartNumberUpperCase, 1, True, output1)
                Dim bProperpart As Boolean = True
                If Not objSSL.IsCurrentCartSameType(cart) Then
                    differentCart = True
                    objResult.differentCart = True
                End If

                For Each i As ShoppingCartItem In cart.ShoppingCartItems
                    If Not i.Product.PartNumber.Equals(sPartNumberUpperCase) Then
                        HttpContext.Current.Session("carts") = cart
                        objResult.items = cart.ShoppingCartItems.Length
                        objResult.totalAmount = String.Format("{0:c}", cart.ShoppingCartTotalPrice)
                        objResult.success = 2
                        message = $"Replacement item {i.Product.PartNumber} added to cart in place of requested item {sPartNumberUpperCase}, which is no longer available."
                        objResult.message = message
                    Else
                        HttpContext.Current.Session("carts") = cart
                        objResult.items = cart.ShoppingCartItems.Length
                        objResult.totalAmount = String.Format("{0:c}", cart.ShoppingCartTotalPrice)
                        objResult.success = 1
                        message = $"Item {sPartNumberUpperCase} added to cart."
                        objResult.message = message
                    End If
                Next

                If sOldPartNumber <> sNewPartNumber Then
                    Conflict = True
                    objResult.success = 2
                    Dim objEx As New ServicesPlusBusinessException($"Replacement item {sNewPartNumber} added to cart in place of requested item {sOldPartNumber}.")
                    objEx.errorMessage = $"Replacement item {sNewPartNumber} added to cart in place of requested item {sOldPartNumber}."
                    ' objEx.setcustomMessage = True
                    Throw objEx
                End If

            Catch argEx As ArgumentException
                If argEx.ParamName = "SomePartNotFound" Then
                    message = argEx.Message.Replace("Parameter name: SomePartNotFound", "")
                ElseIf argEx.ParamName = "PartNotFound" Then
                    message = argEx.Message.Replace("Parameter name: PartNotFound", "")
                ElseIf argEx.ParamName = "BillingOnlyItem" Then
                    message = argEx.Message.Replace("Parameter name: BillingOnlyItem", "")
                End If
            Catch ex As Exception
                objPCILogger.EventOriginApplication = "ServicesPLUS"
                objPCILogger.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
                objPCILogger.OperationalUser = Environment.UserName
                objPCILogger.EventDateTime = DateTime.Now.ToLongTimeString()
                objPCILogger.HTTPRequestObjectValues = objPCILogger.GetRequestContextValue()
                objPCILogger.CustomerID = customer.CustomerID
                objPCILogger.CustomerID_SequenceNumber = customer.SequenceNumber
                objPCILogger.EmailAddress = customer.EmailAddress
                objPCILogger.SIAM_ID = customer.SIAMIdentity
                objPCILogger.LDAP_ID = customer.LdapID
                objPCILogger.EventOriginMethod = "addPartToCartwSPromotions"
                objPCILogger.EventType = EventType.View
                objPCILogger.IndicationSuccessFailure = "Failure"
                objPCILogger.Message = "addPartToCart Failed. " & ex.Message
                objPCILogger.PushLogToMSMQ()
                Dim strUniqueKey As String = Utilities.GetUniqueKey
                message = Utilities.WrapExceptionforUINumber(ex, strUniqueKey)
                message = message.Remove(message.Length - 1)
                message = message + strUniqueKey + "."
            End Try

            objResult.message = message
            Return objResult
        End Function
        'Added the below event to work in common for both parts (9008)
        <System.Web.Services.WebMethod(EnableSession:=True)>
        Public Shared Function GetConflictPart(ByVal part As String) As String
            Dim sm As New ShoppingCartManager
            Dim condParts As String = String.Empty

            Try
                If Not String.IsNullOrWhiteSpace(part) Then
                    Dim DTparts As DataSet = sm.FindConflictCard(part, HttpContextManager.GlobalData)

                    If DTparts.Tables?.Count > 0 Then
                        Dim row As DataRow() = DTparts.Tables(0).Select("PARTNUMBER='" + part + "'")
                        If row.Length > 0 Then
                            condParts = part + ":" + row(0)(1)
                        Else
                            condParts = part + ":" + part
                        End If
                    End If
                End If
            Catch ex As Exception
            End Try
            Return condParts

        End Function
    End Class

    Public Class PartresultPromotions
        Public _message As String
        Public _totalAmount As String
        Public _items As String
        Public _success As Integer
        Public _quantity As String
        Public _partnumber As String
        Public _differentCart As Boolean

        Property partnumber() As String
            Get
                Return _partnumber
            End Get
            Set(ByVal value As String)
                _partnumber = value
            End Set
        End Property
        Property message() As String
            Get
                Return _message
            End Get
            Set(ByVal value As String)
                _message = value
            End Set
        End Property
        Property Quantity() As String
            Get
                Return _quantity
            End Get
            Set(ByVal value As String)
                _quantity = value
            End Set
        End Property
        ''' <summary>
        ''' 0-Failure
        ''' 1-Success
        ''' 2-warning
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Property success() As Integer
            Get
                Return _success
            End Get
            Set(ByVal value As Integer)
                _success = value
            End Set
        End Property
        Property totalAmount() As String
            Get
                Return _totalAmount
            End Get
            Set(ByVal value As String)
                _totalAmount = value
            End Set
        End Property
        Property items() As String
            Get
                Return _items
            End Get
            Set(ByVal value As String)
                _items = value
            End Set
        End Property

        Property differentCart() As Boolean
            Get
                Return _differentCart
            End Get
            Set(ByVal value As Boolean)
                _differentCart = value
            End Set
        End Property
    End Class

End Namespace
