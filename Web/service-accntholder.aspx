<%@ Page Language="VB" AutoEventWireup="false" SmartNavigation="true" CodeFile="service-accntholder.aspx.vb" Inherits="ServicePLUSWebApp.service_accntholder" %>

<%@ Reference Page="~/SSL.aspx" %>
<%@ Register TagPrefix="uc1" TagName="ShipMethod" Src="ShipMethod.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="SonyFooter" Src="SonyFooter.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="TopSonyHeader" Src="TopSonyHeader.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="PersonalMessage" Src="PersonalMessage.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="ServicePlusNav" Src="ServicePlusNav.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="<%=PageLanguage %>">
<head>
    <title><%=Resources.Resource.header_serviceplus()%> - <%=Resources.Resource.repair_svcacc_hdr_msg()%></title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <meta content="Repair, Maintenance, Services, Sony Models, Sony Professional Models, Sony Broadcast Models, Sony Business Models, Sony Software, Sony Professional Software, Sony Broadcast Software, Sony Business Software"
        name="keywords">
    <link type="text/css" href="includes/ServicesPLUS_style.css" rel="stylesheet">
    <script type="text/javascript" src="includes/ServicesPLUS.js"></script>
    <script type="text/javascript" src="includes/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="includes/svcplus_globalization.js"></script>

    <script type="text/javascript">
        function checkServiceContract() {
            //alert ('Baba');    
            var radioObj = document.forms['Form1'].elements['group1']
            var radioLength = radioObj.length;
            var iSelect = 0;

            if (radioLength === undefined) {
                if (radioObj.checked)
                    return radioObj.value;
                else
                    return "";
            }
            for (var i = 0; i < radioLength; i++) {
                if (radioObj[i].checked) {
                    //return group1[i].value;
                    iSelect = radioObj[i].value;
                }
            }

            if (iSelect === 1) {
                if (document.forms['Form1'].txtContractNo.value === "") {
                    document.forms['Form1'].cvServiceContract.value ="<%=Resources.Resource.repair_acchldr_cntc_msg()%>";
                    return false;
                }
            }
        }

        function ValidateApproval() {
            var chk = document.Form1.chkApproval;
            if (chk.checked === true)
                return true;
            else {
                //alert ('Sai Baba');
                document.getElementById('msgError').style.visibility = 'visible';
                return false;
            }
        }

        function poNotRequired() {
            if (document.getElementById('noPoRequiredCheckBox').checked === true) {
                document.getElementById("txtPONumber").value = "NO-PO-REQ";
            }
            else {
                document.getElementById("txtPONumber").value = "";
            }

        }

    </script>
</head>
    
<body style="color: #000000; background: #5d7180; margin: 0px;">
    <form id="Form1" method="post" runat="server">
        <%="" %>
        <center>
            <table width="710" border="0" role="presentation">
                <tr>
                    <td width="25" style="background: url('images/sp_left_bkgd.gif');">
                        <img src="images/spacer.gif" width="25" height="25" alt="">
                    </td>
                    <td width="689" bgcolor="#ffffff">
                        <table width="710" border="0" role="presentation">
                            <tr>
                                <td width="582">
                                    <ServicePLUSWebApp:TopSonyHeader ID="TopSonyHeader" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td width="582" height="23">
                                    <nav><ServicePLUSWebApp:ServicePlusNav ID="SonyNav" runat="server" /></nav>
                                </td>
                            </tr>
                            <tr>
                                <td width="582">
                                    <table width="710" border="0" role="presentation">
                                        <tr>
                                            <td align="right" background="images/repair_lrg.gif" bgcolor="#363d45"
                                                height="82" style="width: 465px">
                                                <br />
                                                <h1 class="headerText"><%=Resources.Resource.repair_snysvc_srl_msg_4 %></h1>
                                            </td>
                                            <td valign="middle" width="238" bgcolor="#363d45">
                                                <ServicePLUSWebApp:PersonalMessage ID="PersonalMessageDisplay" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td bgcolor="#f2f5f8" style="width: 465px">
                                                <h2 id="lblSubHdr" class="headerTitle" style="padding-right: 20px; text-align: right;" runat="server"></h2>
                                            </td>
                                            <td width="238" bgcolor="#99a8b5">
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr height="9">
                                            <td bgcolor="#f2f5f8" style="width: 465px">
                                                <img height="9" src="images/sp_int_header_btm_left_onepix.gif" width="464" alt="">
                                            </td>
                                            <td width="238" bgcolor="#99a8b5">
                                                <img height="9" src="images/sp_int_header_btm_right.gif" width="246" alt="">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td width="582">
                                    <table width="710" border="0" role="presentation">
                                        <tr>
                                            <td style="width: 5px; height: 22px;">
                                                <img height="20" src="images/spacer.gif" width="20" alt="">
                                            </td>
                                            <td style="width: 796px; height: 22px;">
                                                <p class="promoCopyBold">
                                                    <asp:Label ID="TextMessage" runat="server"><%=Resources.Resource.repair_svcacc_lblvld_msg()%></asp:Label>
                                                </p>
                                            </td>
                                            <td style="width: 436px; height: 22px;">
                                                <img height="20" src="images/spacer.gif" width="20" alt="">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr runat="server" id="ControlRow">
                                <td>
                                    <table width="100%" border="0" role="presentation">

                                        <tr>
                                            <td style="width: 5px">
                                                <img height="20" src="images/spacer.gif" width="20" alt="">
                                            </td>
                                            <td style="width: 796px">
                                                <asp:Label ID="ErrorValidation" runat="server" CssClass="redAsterick" Text="<%$Resources:Resource,repair_svcacc_invld_msg%>" Visible="False" />
                                            </td>
                                            <td align="center" style="width: 436px">
                                                &nbsp;&nbsp;
								                    <img height="20" src="images/spacer.gif" width="20" alt="">
                                                &nbsp;&nbsp;
                                            </td>
                                        </tr>
                                        <tr valign="top">
                                            <td style="width: 5px; height: 107px;">
                                                <img height="20" src="images/spacer.gif" width="20" alt="">
                                            </td>
                                            <td style="height: 107px;" colspan="2">
                                                <table cellpadding="3" border="0" style="width: 100%" role="presentation">
                                                    <%If bShow = True Then%>
                                                    <tr valign="middle">
                                                        <td bgcolor="#d5dee9" colspan="2" style="height: 23px"><span class="tableHeader"><%=Resources.Resource.repair_svcacc_bilto_msg()%></span></td>
                                                    </tr>
                                                    <tr valign="middle">
                                                        <td style="width: 97px;" class="tableData" align="right"><%=Resources.Resource.repair_svcacc_accno_msg()%></td>
                                                        <td style="width: 570px;" colspan="2">
                                                            <asp:DropDownList ID="ddlBillTo" runat="server"
                                                                CssClass="tableData" Width="500" AutoPostBack="False" />
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style="width: 97px; height: 28px;" class="tableData" align="right">
                                                            <asp:Label ID="lblBillFax" runat="server" Text="<%$ Resources:Resource,repair_svcacc_fxno_msg %>"></asp:Label>
                                                        </td>
                                                        <td style="height: 28px" colspan="2">
                                                            <SPS:SPSTextBox ID="txtBillFax" runat="server" CssClass="tableData" MaxLength="12" Width="100px"></SPS:SPSTextBox>&nbsp;&nbsp;<span class="bodyCopy">xxx-xxx-xxxx</span>
                                                            <asp:Label ID="lblErrMsgBillFax" runat="server" Text="<%$ Resources:Resource,repair_svcacc_plsfxno_msg%>" Width="190px" CssClass="bodycopy" ForeColor="Red" Visible="False"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <%end if %>

                                                    <tr valign="middle">
                                                        <td bgcolor="#d5dee9" colspan="2" style="height: 23px"><span class="tableHeader"><%=Resources.Resource.repair_svcacc_shipto_msg()%></span></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 97px" class="tableData" align="right">
                                                        </td>
                                                        <td>
                                                            <asp:RadioButtonList ID="rdoShipOption" CssClass="tabledata" runat="server">
                                                                <asp:ListItem Selected="True" Value="0" Text="<%$ Resources:Resource,repair_svcacc_shipaddrs_msg %>"></asp:ListItem>
                                                                <asp:ListItem Value="1" Text="<%$ Resources:Resource,repair_svcacc_hldsvc_msg %>"></asp:ListItem>
                                                            </asp:RadioButtonList>
                                                        <td>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 97px; height: 28px;" class="tableData" align="right">
                                                            <asp:Label ID="lblShpCompany" runat="server"><%=Resources.Resource.repair_svcacc_cmyname_msg() %></asp:Label>
                                                        </td>
                                                        <td style="width: 567px; height: 28px;">
                                                            <SPS:SPSTextBox ID="txtShpCompany" runat="server" CssClass="tableData" MaxLength="100" Width="184px"></SPS:SPSTextBox><span class="redAsterick">*</span>&nbsp;&nbsp;
                                                        </td>

                                                    </tr>
                                                    <tr>
                                                        <td style="width: 97px" class="tableData" align="right">
                                                            <asp:Label ID="lblShpAttn" runat="server"><%=Resources.Resource.repair_svcacc_attname_msg()%></asp:Label>
                                                        </td>
                                                        <td style="width: 567px">
                                                            <SPS:SPSTextBox ID="txtShpAttnName" runat="server" CssClass="tableData" Width="184px" MaxLength="50"></SPS:SPSTextBox><span class="redAsterick">*</span>&nbsp;&nbsp;
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 97px" class="tableData" align="right">
                                                            <asp:Label ID="lblShpAddr1" runat="server"><%=Resources.Resource.repair_svcacc_adrs_msg()%></asp:Label>
                                                        </td>
                                                        <td style="width: 567px">
                                                            <SPS:SPSTextBox ID="txtShpAddr1" runat="server" CssClass="tableData" Width="184px"></SPS:SPSTextBox><asp:Label ID="LabelLine1Star" runat="server" CssClass="redAsterick"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 97px" class="tableData" align="right">
                                                            <asp:Label ID="lblShpAddr2" runat="server"><%=Resources.Resource.repair_svcacc_adrs_msg_1 ()%></asp:Label>
                                                        </td>
                                                        <td style="width: 567px">
                                                            <SPS:SPSTextBox ID="txtShpAddr2" runat="server" CssClass="tableData" Width="184px" MaxLength="50"></SPS:SPSTextBox><asp:Label ID="LabelLine2Star" runat="server" CssClass="redAsterick"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr runat="server" visible="false">
                                                        <td style="width: 97px" class="tableData" align="right">
                                                            <asp:Label ID="lblShpAddr3" runat="server"><%=Resources.Resource.repair_svcacc_adrs_msg_2()%></asp:Label>
                                                        </td>
                                                        <td style="width: 567px">
                                                            <SPS:SPSTextBox ID="txtShpAddr3" runat="server" CssClass="tableData" Width="184px" MaxLength="50"></SPS:SPSTextBox><asp:Label ID="LabelLine3Star" runat="server" CssClass="redAsterick"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 97px" class="tableData" align="right">
                                                            <asp:Label ID="lblShpCity" runat="server"><%=Resources.Resource.repair_svcacc_city_msg() %></asp:Label>
                                                        </td>
                                                        <td style="width: 567px">
                                                            <SPS:SPSTextBox ID="txtShpCity" runat="server" CssClass="tableData" Width="184px" MaxLength="50"></SPS:SPSTextBox><asp:Label ID="LabelLine4star" runat="server" CssClass="redAsterick"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 97px" class="tableData" align="right">
                                                            <asp:Label ID="lblShpState" runat="server"><%=Resources.Resource.repair_svcacc_state_msg()%></asp:Label>
                                                        </td>
                                                        <td style="width: 567px">
                                                            <asp:DropDownList ID="ddlShpState" runat="server" CssClass="tableData"></asp:DropDownList><span class="redAsterick">*</span>&nbsp;&nbsp;
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 97px" class="tableData" align="right">
                                                            <asp:Label ID="lblShpZip" runat="server"><%=Resources.Resource.repair_svcacc_zip_msg()%></asp:Label>
                                                        </td>
                                                        <td style="width: 567px">
                                                            <SPS:SPSTextBox ID="txtShpZip" runat="server" CssClass="tableData" MaxLength="10"></SPS:SPSTextBox><span class="redAsterick">*</span>&nbsp;&nbsp;
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 97px" class="tableData" align="right">
                                                            <asp:Label ID="lblShpPhno" runat="server"><%=Resources.Resource.repair_svcacc_phno_msg()%></asp:Label>
                                                        </td>
                                                        <td style="width: 567px">
                                                            <SPS:SPSTextBox ID="txtShpPhno" runat="server" CssClass="tableData" Width="100px"></SPS:SPSTextBox>
                                                            <span class="redAsterick">*</span>&nbsp;&nbsp;<span class="bodyCopy">xxx-xxx-xxxx
                                                                    <asp:Label ID="lblErrmsgShpPhone" runat="server" ForeColor="Red"
                                                                        Visible="False"><%=Resources.Resource.repair_svcacc_plsphno_msg()%></asp:Label></span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 97px; height: 30px;" class="tableData" align="right">
                                                            <asp:Label ID="lblPhoneExten" runat="server"><%=Resources.Resource.repair_svcacc_phnextn_msg()%></asp:Label>
                                                        </td>
                                                        <td style="width: 567px; height: 30px;" colspan="">
                                                            <SPS:SPSTextBox ID="txtShpPhExtn" runat="server" CssClass="tableData" MaxLength="4" Width="64px"></SPS:SPSTextBox>&nbsp;&nbsp;<span class="bodyCopy">xxxx</span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 97px; height: 28px;" class="tableData" align="right">
                                                            <asp:Label ID="lblShpFax" runat="server" Text="<%$ Resources:Resource,repair_svcacc_fxno_msg%>"></asp:Label>
                                                        </td>
                                                        <td style="height: 28px" colspan="2">
                                                            <SPS:SPSTextBox ID="txtShpFax" runat="server" CssClass="tableData" MaxLength="12" Width="100px"></SPS:SPSTextBox>&nbsp;&nbsp;<span class="bodyCopy">xxx-xxx-xxxx</span>
                                                            <asp:Label ID="lblErrMsgShpFax" runat="server" Width="190px" CssClass="bodycopy" ForeColor="Red" Visible="False"><%=Resources.Resource.repair_svcacc_plsfxno_msg()%></asp:Label>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td colspan="3" style="height: 25px">&nbsp;</td>
                                                    </tr>
                                                    <%If bShow = True Then%>
                                                    <tr>
                                                        <td style="width: 97px" class="tableData" align="right">
                                                            <asp:Label ID="lblPONumber" runat="server" Text="<%$ Resources:Resource,repair_svcacc_pono_msg %>"></asp:Label>
                                                        </td>
                                                        <td style="width: 567px" class="tableData">
                                                            <SPS:SPSTextBox ID="txtPONumber" runat="server" CssClass="tableData" MaxLength="20"></SPS:SPSTextBox>&nbsp;<span class="redAsterick">*</span>
                                                            <input type="checkbox" id="noPoRequiredCheckBox" runat="server" class="tableData" onclick="javascript: poNotRequired();" /><%=Resources.Resource.repair_svcacc_cmpnyordr_msg()%>"                                                            
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 97px" class="tableData" align="right">
                                                            <asp:Label ID="lblApprvl" runat="server" Text="<%$ Resources:Resource,repair_svcacc_aprvl_msg %>"></asp:Label>
                                                        </td>
                                                        <td style="width: 567px" colspan="2">
                                                            <asp:CheckBox ID="chkApproval" runat="server" Text="<%$Resources:Resource,repair_svcacc_aprvlbr_msg%>" CssClass="bodyCopy" />
                                                            &nbsp;<span class="redAsterick">*</span>&nbsp;<div id="msgError" class="bodycopy" style="visibility: hidden; display: inline;"><span color="red">???</span></div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style="width: 97px" class="tableData" align="right"><%=Resources.Resource.repair_svcacc_txexe_msg()%></td>
                                                        <td style="width: 567px" colspan="2">
                                                            <asp:CheckBox ID="chkTaxexempt" runat="server" CssClass="bodycopy" Text="<%$ Resources:Resource,repair_svcacc_cmnyinfo_msg %>" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 97px; height: 26px;" class="tableData" align="right"><%=Resources.Resource.repair_svcacc_pytyp_msg()%></td>
                                                        <td style="width: 567px; height: 26px;">
                                                            <span class="bodyCopy">
                                                                <asp:RadioButtonList ID="rdPayment" runat="server" CssClass="bodycopy" RepeatDirection="Horizontal"
                                                                    RepeatLayout="Flow">
                                                                    <asp:ListItem Selected="True" Value="1" Text="<%$ Resources:Resource,repair_svcacc_snyacct_msg %>"></asp:ListItem>
                                                                    <asp:ListItem Value="2" Text="<%$ Resources:Resource,repair_svcacc_crdtcrd_msg %>"></asp:ListItem>
                                                                    <asp:ListItem Value="3" Text="<%$ Resources:Resource,repair_svcacc_chk_msg %>"></asp:ListItem>
                                                                </asp:RadioButtonList></span>
                                                        </td>
                                                    </tr>
                                                    <%end if %>

                                                    <tr>
                                                        <td align="right" class="tableData" style="width: 97px; height: 30px;">
                                                            <asp:Label ID="Label1" runat="server"><%=Resources.Resource.repair_svcacc_ctname_msg()%></asp:Label>
                                                        </td>
                                                        <td style="width: 555px; height: 30px;">
                                                            <SPS:SPSTextBox ID="txtContactName" runat="server" CssClass="tableData" MaxLength="100"
                                                                Width="184px"></SPS:SPSTextBox><span class="redAsterick">*</span>&nbsp;&nbsp;
                                                                    <asp:Label ID="lblContactName" runat="server" CssClass="bodycopy" ForeColor="Red"
                                                                        Visible="False"><%=Resources.Resource.repair_svcacc_plscntpn_msg()%></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right" class="tableData" style="width: 97px; height: 30px;">
                                                            <asp:Label ID="lblEmailAddr" runat="server"><%=Resources.Resource.repair_svcacc_cntemladrs_msg ()%></asp:Label>
                                                        </td>
                                                        <td style="width: 555px; height: 30px;">
                                                            <SPS:SPSTextBox ID="txtEmailAddr" runat="server" CssClass="tableData" MaxLength="100"
                                                                Width="184px"></SPS:SPSTextBox><span class="redAsterick">*</span>&nbsp;&nbsp;
                                                                    <asp:Label ID="lblErrMsgEmailAddr" runat="server" CssClass="bodycopy" ForeColor="Red"
                                                                        Visible="False"><%=Resources.Resource.repair_svcacc_plsemladrs_msg()%></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2" style="height: 34px">
                                                            <asp:ImageButton ID="imgBtnNext" runat="server" ImageUrl="<%$Resources:Resource,img_btnNext%>" />&nbsp;
                                                        <a href='sony-repair.aspx'>
                                                            <img src="<%=Resources.Resource.sna_svc_img_7()%>" alt='Click to cancel' border="0" /></a>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 5px">&nbsp;</td>
                                            <td colspan="2">&nbsp;</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td width="582">
                                    <footer><ServicePLUSWebApp:SonyFooter ID="SonyFooter1" runat="server" /></footer>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="25" style="background: url('images/sp_right_bkgd.gif')">
                        <img src="images/spacer.gif" width="25" height="20" alt="">
                    </td>
                </tr>
            </table>
        </center>
    </form>
</body>
</html>
