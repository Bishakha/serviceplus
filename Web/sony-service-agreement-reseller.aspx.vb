﻿Imports System.Globalization
Imports System.IO
Imports System.Net.Mail
Imports ServicePLUSWebApp
Imports ServicesPlusException
Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.siam
Imports Sony.US.SIAMUtilities

Namespace ServicePLUSWebApp
    Partial Class sony_service_agreement_reseller
        Inherits SSL

        Public IsCanada As Boolean
        Dim ProductList As New List(Of ServiceAgreementItem)

        Private Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
            Try
                If Session("ProductList") IsNot Nothing Then ProductList = Session("ProductList")
                IsCanada = HttpContextManager.GlobalData.IsCanada
                vldPostalCode.Enabled = IsCanada
                vldZipCode.Enabled = Not IsCanada

                ' Restore the File Upload value between postbacks
                If (Session("fileReceipt") Is Nothing) And (fuReceipt.HasFile) Then
                    Session("fileReceipt") = fuReceipt
                ElseIf (Session("fileReceipt") IsNot Nothing) And (Not fuReceipt.HasFile) Then
                    fuReceipt = DirectCast(Session("fileReceipt"), FileUpload)
                ElseIf fuReceipt.HasFile Then
                    Session("fileReceipt") = fuReceipt
                End If

                ' Restore the product list
                RefreshProductGrid()

                If Not Page.IsPostBack Then
                    ' Fill the "State" or "Province" dropdown box. This function pulls country from Session.
                    PopulateStateDropdownList(ddlState)

                    ' If customer is logged in, pre-fill the first set of inputs.
                    If HttpContextManager.Customer IsNot Nothing Then
                        Dim customer = HttpContextManager.Customer
                        txtFirstName.Text = customer.FirstName
                        txtLastName.Text = customer.LastName
                        txtEmail.Text = customer.EmailAddress
                        txtCompanyName.Text = customer.CompanyName
                        txtAddress1.Text = customer.Address?.Address1
                        txtAddress2.Text = customer.Address?.Address2
                        txtAddress3.Text = customer.Address?.Address3
                        txtCity.Text = customer.Address?.City
                        ddlState.SelectedValue = customer.Address?.State
                        txtZip.Text = customer.Address?.Zip
                        txtPhone.Text = customer.PhoneNumber
                        txtExtension.Text = customer.PhoneExtension
                        txtFax.Text = customer.FaxNumber
                    End If
                End If
            Catch ex As Exception
                lblError.Text = Utilities.WrapExceptionforUI(ex)
                lblError.Focus()
            End Try
        End Sub

        Private Sub btnAddProduct_Click(sender As Object, e As ImageClickEventArgs) Handles btnAddProduct.Click
            Dim modelNumber As String = txtModelNumber.Text.Trim()
            Dim serialNumber As String = txtSerialNumber.Text.Trim()
            Dim dateInput As String = txtPurchaseDate.Text.Trim()
            Dim datePurchase As New DateTime
            Dim usCulture As New CultureInfo("en-US")

            Try
                ' Do basic validation on the input
                If String.IsNullOrEmpty(modelNumber) Then
                    lblProductError.Text = "Please input a valid Model Number"
                    lblProductError.Focus()
                ElseIf String.IsNullOrEmpty(serialNumber) Then
                    lblProductError.Text = "Please input a valid Serial Number"
                    lblProductError.Focus()
                ElseIf Not Date.TryParse(dateInput, usCulture, DateTimeStyles.None, datePurchase) Then    ' Force US culture for CA users
                    lblProductError.Text = "Please input a valid Purchase Date in the format: mm/dd/yyyy"
                    lblProductError.Focus()
                ElseIf datePurchase > Date.Now() Or datePurchase < Date.Now().AddYears(-50) Then
                    lblProductError.Text = "Please input a valid Purchase Date. It must be in the recent past, in the format: mm/dd/yyyy."
                    lblProductError.Focus()
                ElseIf ProductList.Count > 10 Then
                    lblProductError.Text = "Only ten products can be entered at once. Please call the number above for assistance if you need more."
                    lblProductError.Focus()
                Else
                    lblProductError.Text = ""   ' Clear old error messages
                    ' Validation is okay, add to the list and refresh the grid
                    ProductList.Add(New ServiceAgreementItem(modelNumber, serialNumber, datePurchase))
                    RefreshProductGrid()

                    ' Clear the inputs for the next item
                    txtModelNumber.Text = ""
                    txtSerialNumber.Text = ""
                    txtPurchaseDate.Text = ""
                End If
            Catch ex As Exception
                lblError.Text = Utilities.WrapExceptionforUI(ex)
                lblError.Focus()
            End Try
        End Sub

        'Protected Sub gvProducts_RowEditing(sender As Object, e As GridViewEditEventArgs) Handles gvProducts.RowEditing
        '    gvProducts.EditIndex = e.NewEditIndex
        '    RefreshProductGrid()
        'End Sub

        'Protected Sub gvProducts_RowUpdating(sender As Object, e As GridViewUpdateEventArgs) Handles gvProducts.RowUpdating
        '    With ProductList(e.RowIndex)
        '        .Model = e.NewValues("Model")
        '        .Serial = e.NewValues("Serial")
        '        .DatePurchased = e.NewValues("DatePurchased")
        '    End With
        '    RefreshProductGrid()
        'End Sub

        Private Sub gvProducts_RowDeleting(sender As Object, e As GridViewDeleteEventArgs) Handles gvProducts.RowDeleting
            Dim index As Integer = e.RowIndex

            Try
                ProductList.RemoveAt(index)
                RefreshProductGrid()
            Catch ex As Exception
                lblError.Text = Utilities.WrapExceptionforUI(ex)
                lblError.Focus()
            End Try
        End Sub

        Protected Sub imgNext_Click(sender As Object, e As ImageClickEventArgs) Handles imgNext.Click
            Dim agreement As ServiceAgreement

            Try
                If IsFormValid() Then
                    agreement = FillAgreement()         ' Compile input into the Agreement object
                    SendCustomerEmail(agreement)        ' Process and send email
                    Session("ResellerAgreement") = agreement
                    Session.Remove("ProductList")       ' Clear the temporary product list
                    Response.Redirect("sony-service-agreement-complete.aspx", False)   ' Continue to Confirmation page
                    HttpContext.Current.ApplicationInstance.CompleteRequest()
                End If
            Catch ex As Exception
                lblError.Text = Utilities.WrapExceptionforUI(ex)
                lblError.Focus()
            End Try
        End Sub

        ''' <summary>
        ''' Updates the Session variable "ProductList" and refreshes the GridView
        ''' </summary>
        Private Sub RefreshProductGrid()
            Session("ProductList") = ProductList
            gvProducts.DataSource = ProductList
            gvProducts.DataBind()
        End Sub

        Private Function IsFormValid() As Boolean
            Dim isValid As Boolean = True
            Dim fileSuffix As String
            Dim phoneNum As String
            Dim phoneFormat As New Regex("^\(?([0-9]{3})\)?[-]([0-9]{3})[-]([0-9]{4})$")    ' ###-###-####
            Dim debugString As String = $"Agreement-Reseller.IsFormValid - START at {Date.Now.ToShortTimeString}{Environment.NewLine}"

            ' Due to ASP security, file attachments are not stored between postbacks. This can be improved one day, but not by forcing the FileUpload
            '   control to re-attach the file. Storing in memory between posts will expose us to DDoS, and saving physically to server is obviously
            '   not wanted, either. Fix with a different file handler, or a way to prevent the validation postback without relying on client (JS) validation,
            '   or making the file upload the next step in form submission.
            Try
                lblProductError.Text = ""               ' Clear old error messages
                lblError.Text = ""
                lblPhoneError.Text = ""
                lblFaxError.Text = ""
                lblResellerPhoneError.Text = ""
                lblReceiptRedo.Visible = True

                If Not Page.IsValid Then Return False   ' Run ASP validation using the in-form controls.
                debugString &= $"- Page validations okay.{Environment.NewLine}"

                ' Ensure they attached their Receipt
                If fuReceipt.HasFile Then
                    If fuReceipt.PostedFile.ContentLength < 2097152 And fuReceipt.PostedFile.ContentLength > 2 Then
                        fileSuffix = Path.GetExtension(fuReceipt.PostedFile.FileName).ToLower()
                        If fileSuffix = ".jpg" Or fileSuffix = ".gif" Or fileSuffix = ".pdf" Then
                            ' Input is okay.
                            debugString &= $"- File input is okay.{Environment.NewLine}"
                        Else
                            lblError.Text = "File type must be either GIF, JPG or PDF.  "
                            isValid = False
                            lblError.Focus()
                        End If
                    Else
                        lblError.Text = "File size must not be zero or greater than 2mb.  "
                        isValid = False
                        lblError.Focus()
                    End If
                Else
                    lblError.Text = "You must attach a copy of your receipt.  "
                    isValid = False
                    lblError.Focus()
                End If

                ' Ensure they entered at least one Product
                ProductList = Session("ProductList")
                If (ProductList Is Nothing) OrElse (ProductList?.Count = 0) Then    ' 2019-06-20 ASleight - Added null safety
                    lblProductError.Text = "You must enter at least one Product for your service agreement.  "
                    isValid = False
                    lblProductError.Focus()
                End If
                debugString &= $"- Products entered: {ProductList?.Count}.{Environment.NewLine}"

                ' Check the Phone Number, format it if possible, throw a validation error if not.
                phoneNum = txtPhone.Text.Trim()
                If Not phoneFormat.IsMatch(phoneNum) Then
                    phoneNum = phoneNum.Replace("-", "").Replace(" ", "").Replace("(", "").Replace(")", "")
                    If phoneNum.Length = 10 Then    ' If it's 10 digits, reformat it the way we want it.
                        txtPhone.Text = $"{phoneNum.Substring(0, 3)}-{phoneNum.Substring(3, 3)}-{phoneNum.Substring(6)}"
                    Else
                        lblPhoneError.Text = "Please enter a 10-digit number. (ex. 123-456-7890)  "
                        lblPhoneError.Focus()
                        isValid = False
                    End If
                End If
                debugString &= $"- Phone number parsed.{Environment.NewLine}"

                ' Check the Fax Number, format it if possible.
                If Not String.IsNullOrWhiteSpace(txtFax.Text) Then
                    phoneNum = txtFax.Text.Trim()
                    If Not phoneFormat.IsMatch(phoneNum) Then
                        phoneNum = phoneNum.Replace("-", "").Replace(" ", "").Replace("(", "").Replace(")", "")
                        If phoneNum.Length = 10 Then
                            txtFax.Text = $"{phoneNum.Substring(0, 3)}-{phoneNum.Substring(3, 3)}-{phoneNum.Substring(6)}"
                        Else
                            lblFaxError.Text = "Please enter a 10-digit number. (ex. 123-456-7890)  "
                            isValid = False
                            lblFaxError.Focus()
                        End If
                    End If
                    debugString &= $"- Fax number parsed.{Environment.NewLine}"
                End If

                ' Now the same for the Reseller Phone Number
                If Not String.IsNullOrWhiteSpace(txtResellerPhone.Text) Then
                    phoneNum = txtResellerPhone.Text.Trim()
                    If Not phoneFormat.IsMatch(phoneNum) Then
                        phoneNum = phoneNum.Replace("-", "").Replace(" ", "").Replace("(", "").Replace(")", "")
                        If phoneNum.Length = 10 Then
                            txtResellerPhone.Text = $"{phoneNum.Substring(0, 3)}-{phoneNum.Substring(3, 3)}-{phoneNum.Substring(6)}"
                        Else
                            lblResellerPhoneError.Text = "Please enter a 10-digit number. (ex. 123-456-7890)  "
                            lblResellerPhoneError.Focus()
                            isValid = False
                        End If
                    End If
                    debugString &= $"- Reseller phone parsed.{Environment.NewLine}"
                End If

                ' The HTML-side validator only checks that there IS a value. Now we check if it starts with "SPS"
                If Not txtAgreementNumber.Text.Trim().ToUpper().StartsWith("SPS") Then
                    vldAgreementNumber.IsValid = False
                    isValid = False
                End If
                lblReceiptRedo.Visible = Not isValid    ' User must re-attach the file if validation fails.
                debugString &= $"- Validation complete. Value: {isValid}"

            Catch ex As Exception
                lblError.Text = Utilities.WrapExceptionforUI(ex)
                lblError.Focus()
                Utilities.LogMessages(debugString)
                Return False
            End Try
            Utilities.LogDebug(debugString)

            Return isValid
        End Function

        Private Function FillAgreement() As ServiceAgreement
            Dim agreement As New ServiceAgreement()

            Try
                agreement.AgreementNumber = txtAgreementNumber.Text.Trim()
                agreement.FirstName = txtFirstName.Text.Trim()
                agreement.LastName = txtLastName.Text.Trim()
                agreement.Email = txtEmail.Text.Trim()
                agreement.Company = txtCompanyName.Text.Trim()
                agreement.Address1 = txtAddress1.Text.Trim()
                agreement.Address2 = txtAddress2.Text.Trim()
                agreement.Address3 = txtAddress3.Text.Trim()
                agreement.City = txtCity.Text.Trim()
                agreement.State = ddlState.SelectedValue
                agreement.ZipCode = txtZip.Text.Trim()
                agreement.Phone = txtPhone.Text.Trim()
                agreement.Extension = txtExtension.Text.Trim()
                agreement.Fax = txtFax.Text.Trim()

                agreement.ResellerCompany = txtResellerCompany.Text.Trim()
                agreement.ResellerName = txtResellerName.Text.Trim()
                agreement.ResellerPhone = txtResellerPhone.Text.Trim()
                agreement.ResellerExtension = txtResellerExtension.Text.Trim()

                agreement.EquipmentList = ProductList
            Catch ex As Exception
                lblError.Text = Utilities.WrapExceptionforUI(ex)
                lblError.Focus()
            End Try

            Return agreement
        End Function

        Private Sub SendCustomerEmail(ByVal agreement As ServiceAgreement)
            Dim mailClient As SmtpClient
            Dim userAddress As String = ""
            Dim isColorRow As Boolean = False
            Dim isColorProductRow As Boolean = False
            Dim emailBody As New StringBuilder()
            Dim emailSubject As String = ""
            Dim emailSender As String = ConfigurationData.Environment.Email.ServiceAgreement.Service_Agreement_From_Mail
            Dim isProduction As Boolean = (ConfigurationData.GeneralSettings.Environment.ToUpper() = "PRODUCTION")

            Try
                ' Build the User Address
                If Not String.IsNullOrWhiteSpace(agreement.Address1) Then userAddress &= $"{agreement.Address1}<br/>"
                If Not String.IsNullOrWhiteSpace(agreement.Address2) Then userAddress &= $"{agreement.Address2}<br/>"
                If Not String.IsNullOrWhiteSpace(agreement.Address3) Then userAddress &= $"{agreement.Address3}<br/>"
                If Not String.IsNullOrWhiteSpace(agreement.City) Then userAddress &= $"{agreement.City}, "
                If Not String.IsNullOrWhiteSpace(agreement.State) Then userAddress &= $"{agreement.State} "
                If Not String.IsNullOrWhiteSpace(agreement.ZipCode) Then userAddress &= $"{agreement.ZipCode}<br/>"

                ' -- ENGLISH EMAIL -- For US and CA
                emailBody.Append("<table border=""0"" cellspacing=""0"" cellpadding=""0"" style=""font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: #666;"">")
                emailBody.Append("  <tr>")
                emailBody.Append("    <td align=""left"">")
                If HttpContextManager.GlobalData.IsCanada Then emailBody.Append($"      <p style=""font-weight: bold;"">Le texte français est ci-dessous.</p>")     ' "The French text is below."
                emailBody.Append($"      <p>{Resources.Resource_mail.ml_ProductRegistration_body_line1_en} <strong>877-398-7669</strong> {Resources.Resource_mail.ml_ProductRegistration_body_line2_en} <a style=""font-weight:bold"" href=""mailto:SupportNet@am.sony.com"">SupportNet@am.sony.com</a>.</p>")
                emailBody.Append($"      <p>{Resources.Resource_mail.ml_ProductRegistration_body_line3_en} <strong>866-766-9272</strong>. {Resources.Resource_mail.ml_ProductRegistration_body_line4_en}</p>")
                emailBody.Append($"      <p>{Resources.Resource_mail.ml_ProductRegistration_body_line5_en}</p>")      ' "Sony thanks you for your continued support."
                emailBody.Append($"      <p>{Resources.Resource_mail.ml_ProductRegistration_body_line6_en}<br />")    ' "Sony Professional Services Contract Administration Group"
                emailBody.Append($"      {Resources.Resource_mail.ml_ProductRegistration_body_line7_en}<br />")       ' "Business Solutions & Systems Company"
                emailBody.Append($"      {Resources.Resource_mail.ml_ProductRegistration_body_line8_en} {Resources.Resource_mail.ml_ProductRegistration_body_line9_en}<br />") ' "Sony Electronics Inc."
                emailBody.Append("      <a href=""http://www.sony.com/professionalservices"">http://www.sony.com/professionalservices</a><br/></p>")
                emailBody.Append("    </td>")
                emailBody.Append("  </tr>")
                emailBody.Append("  <tr>")
                emailBody.Append("    <td align=""center"">")
                emailBody.Append("      <table cellpadding=""3"" cellspacing=""1"" style=""width: 93%; vertical-align: top; font-family: Arial, Helvetica, sans-serif; color: #666; border: solid 1px #666;"">")
                emailBody.Append("        <tr>")
                emailBody.Append($"          <td style=""width: 33%; border-bottom:solid thin #666""><img src=""{Resources.Resource_mail.ml_ProductRegistration_body_imglogo_en}""/></td>")
                emailBody.Append("          <td style=""border-bottom:solid thin #666"">&nbsp;</td>")
                emailBody.Append("        </tr>")
                emailBody.Append("        <tr>")
                emailBody.Append("          <td colspan=""2"" style=""border-bottom-style:solid; border-bottom-width:thin;"">")
                emailBody.Append("            <table border=""0"" cellpadding=""1"" cellspacing=""1"" style=""width: 100%; font-family: Arial, Helvetica, sans-serif;"">")
                emailBody.Append("              <tr style=""height: 16px; background: #d5dee9"">")
                emailBody.Append($"                <td style=""font-weight: bold; text-align: center;"">ServicesPLUS - {Resources.Resource_mail.ml_ProductRegistration_body_ServiceAgreement_en}</td>")
                emailBody.Append("              </tr>")
                emailBody.Append("            </table>")
                emailBody.Append("          </td>")
                emailBody.Append("        </tr>")
                emailBody.Append($"        <tr style=""vertical-align: top; {IIf(isColorRow, " background-color:#d5dee9;", "")}"">")
                emailBody.Append($"          <td>{Resources.Resource_mail.ml_ProductRegistration_body_RegistrationDate_en}</td>")
                emailBody.Append($"          <td>{Date.Now.ToString("MMM dd, yyyy")}</td>")
                emailBody.Append("        </tr>")
                isColorRow = Not isColorRow
                emailBody.Append($"        <tr style=""vertical-align: top; {IIf(isColorRow, " background-color:#d5dee9;", "")}"">")
                emailBody.Append("          <td>Service Agreement Number:</td>")
                emailBody.Append($"          <td>{agreement.AgreementNumber}</td>")
                emailBody.Append("        </tr>")
                isColorRow = Not isColorRow

                ' End-User Information
                emailBody.Append($"        <tr{IIf(isColorRow, " style=""background-color:#d5dee9;""", "")}>")
                emailBody.Append($"          <td>{Resources.Resource_mail.ml_RARequest_companyname_en}</td>")
                emailBody.Append($"          <td>{agreement.Company}</td>")
                emailBody.Append("        </tr>")
                isColorRow = Not isColorRow
                emailBody.Append($"        <tr{IIf(isColorRow, " style=""background-color:#d5dee9;""", "")}>")
                emailBody.Append($"          <td>{Resources.Resource_mail.ml_common_Name_en}:</td>")
                emailBody.Append($"          <td>{agreement.FirstName} {agreement.LastName}</td>")
                emailBody.Append("        </tr>")
                isColorRow = Not isColorRow
                emailBody.Append($"        <tr{IIf(isColorRow, " style=""background-color:#d5dee9;""", "")}>")
                emailBody.Append($"          <td>{Resources.Resource_mail.ml_common_Email_en}:</td>")
                emailBody.Append($"          <td>{agreement.Email}</td>")
                emailBody.Append("        </tr>")
                isColorRow = Not isColorRow
                emailBody.Append($"        <tr{IIf(isColorRow, " style=""background-color:#d5dee9;""", "")}>")
                emailBody.Append($"          <td>{Resources.Resource_mail.ml_common_Address_en}:</td>")
                emailBody.Append($"          <td>{userAddress}</td>")
                emailBody.Append("        </tr>")
                isColorRow = Not isColorRow
                emailBody.Append($"        <tr{IIf(isColorRow, " style=""background-color:#d5dee9;""", "")}>")
                emailBody.Append($"          <td>{Resources.Resource_mail.ml_RARequest_phone_en}</td>")
                emailBody.Append($"          <td>{agreement.Phone}{IIf(String.IsNullOrWhiteSpace(agreement.Extension), "", $"&nbsp;&nbsp;&nbsp;Ext: {agreement.Extension}")}</td>")
                emailBody.Append("        </tr>")
                isColorRow = Not isColorRow
                If Not String.IsNullOrWhiteSpace(agreement.Fax) Then
                    emailBody.Append($"        <tr{IIf(isColorRow, " style=""background-color:#d5dee9;""", "")}>")
                    emailBody.Append($"          <td>{Resources.Resource_mail.ml_ProductRegistration_body_fax_en}:</td>")
                    emailBody.Append($"          <td>{agreement.Fax}</td>")
                    emailBody.Append("        </tr>")
                    isColorRow = Not isColorRow
                End If

                ' Reseller Information
                If agreement.HasReseller Then
                    emailBody.Append($"        <tr style=""background-color:#404040;"">")
                    emailBody.Append("          <td colspan=""2"" style=""color: white; font-weight: bold;"">Reseller Information:</td>")
                    emailBody.Append("        </tr>")
                    If Not String.IsNullOrWhiteSpace(agreement.ResellerCompany) Then
                        emailBody.Append($"        <tr{IIf(isColorRow, " style=""background-color:#d5dee9;""", "")}>")
                        emailBody.Append($"          <td>{Resources.Resource_mail.ml_RARequest_companyname_en}:</td>")
                        emailBody.Append($"          <td>{agreement.ResellerCompany}</td>")
                        emailBody.Append("        </tr>")
                        isColorRow = Not isColorRow
                    End If
                    If Not String.IsNullOrWhiteSpace(agreement.ResellerName) Then
                        emailBody.Append($"        <tr{IIf(isColorRow, " style=""background-color:#d5dee9;""", "")}>")
                        emailBody.Append($"          <td>{Resources.Resource_mail.ml_common_Name_en}:</td>")
                        emailBody.Append($"          <td>{agreement.ResellerName}</td>")
                        emailBody.Append("        </tr>")
                        isColorRow = Not isColorRow
                    End If
                    If Not String.IsNullOrWhiteSpace(agreement.ResellerPhone) Then
                        emailBody.Append($"        <tr{IIf(isColorRow, " style=""background-color:#d5dee9;""", "")}>")
                        emailBody.Append($"          <td>{Resources.Resource_mail.ml_RARequest_phone_en}</td>")
                        emailBody.Append($"          <td>{agreement.ResellerPhone}{IIf(String.IsNullOrWhiteSpace(agreement.ResellerExtension), "", $"&nbsp;&nbsp;&nbsp;Ext: {agreement.ResellerExtension}")}</td>")
                        emailBody.Append("        </tr>")
                        isColorRow = Not isColorRow
                    End If
                End If

                ' PRODUCT LIST
                emailBody.Append($"        <tr style=""background-color:#404040;"">")
                emailBody.Append($"          <td colspan=""2"" style=""color: white; font-weight: bold;"">{Resources.Resource_mail.ml_ProductRegistration_body_ProductList_en}</td>")
                emailBody.Append("        </tr>")

                emailBody.Append("        <tr>")
                emailBody.Append("          <td colspan=""2"">")
                emailBody.Append("            <table cellspacing=""0"" cellpadding=""4"" style=""vertical-align: middle; text-align: center; color: #333333; width: 90%; border: none; font-family: Arial, Helvetica, sans-serif; font-size: 11px; "">")
                emailBody.Append("              <tr style=""background-color:#d5dee9; font-weight:bold;"">")
                emailBody.Append($"                <th>{Resources.Resource_mail.ml_ProductRegistration_body_ModelNumber_en}</th>")
                emailBody.Append($"                <th>{Resources.Resource_mail.ml_ProductRegistration_body_SerialNumber_en}</th>")
                emailBody.Append($"                <th>{Resources.Resource_mail.ml_ProductRegistration_body_PurchaseDate_en}</th>")
                emailBody.Append("              </tr>")

                For Each item As ServiceAgreementItem In agreement.EquipmentList
                    emailBody.Append($"              <tr{IIf(isColorProductRow, " style=""background-color:#d5dee9;""", "")}>")
                    emailBody.Append($"                <td>{item.Model}</td><td>{item.Serial}</td><td>{item.DatePurchased.ToString("MMM dd, yyyy")}</td>")
                    emailBody.Append("              </tr>")
                    isColorProductRow = Not isColorProductRow
                Next
                emailBody.Append("            </table>")
                emailBody.Append("          </td>")
                emailBody.Append("        </tr>")
                emailBody.Append("      </table>")
                emailBody.Append("    </td>")
                emailBody.Append("  </tr>")
                emailBody.Append("</table>")

                ' FRENCH EMAIL - For CA Only
                If HttpContextManager.GlobalData.IsCanada Then
                    isColorRow = False
                    emailBody.Append("<br/><hr/><br/>")
                    emailBody.Append("<table border=""0"" cellspacing=""0"" cellpadding=""0"" style=""font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: #666;"">")
                    emailBody.Append("  <tr>")
                    emailBody.Append("    <td align=""left"">")
                    emailBody.Append($"      <p>{Resources.Resource_mail.ml_ProductRegistration_body_line1_fr} <strong>877-398-7669</strong> {Resources.Resource_mail.ml_ProductRegistration_body_line2_fr} <a style=""font-weight:bold"" href=""mailto:SupportNet@am.sony.com"">SupportNet@am.sony.com</a>.</p>")
                    emailBody.Append($"      <p>{Resources.Resource_mail.ml_ProductRegistration_body_line3_fr} <strong>866-766-9272</strong>. {Resources.Resource_mail.ml_ProductRegistration_body_line4_fr}</p>")
                    emailBody.Append($"      <p>{Resources.Resource_mail.ml_ProductRegistration_body_line5_fr}</p>")      ' "Sony thanks you for your continued support."
                    emailBody.Append($"      <p>{Resources.Resource_mail.ml_ProductRegistration_body_line6_fr}<br />")    ' "Sony Professional Services Contract Administration Group"
                    emailBody.Append($"      {Resources.Resource_mail.ml_ProductRegistration_body_line7_fr}<br />")       ' "Business Solutions & Systems Company"
                    emailBody.Append($"      {Resources.Resource_mail.ml_ProductRegistration_body_line8_fr} {Resources.Resource_mail.ml_ProductRegistration_body_line9_fr}<br />") ' "Sony Electronics Inc."
                    emailBody.Append("      <a href=""http://www.sony.com/professionalservices"">http://www.sony.com/professionalservices</a><br/></p>")
                    emailBody.Append("    </td>")
                    emailBody.Append("  </tr>")
                    emailBody.Append("  <tr>")
                    emailBody.Append("    <td align=""center"">")
                    emailBody.Append("      <table cellpadding=""3"" cellspacing=""1"" style=""width: 93%; vertical-align: top; font-family: Arial, Helvetica, sans-serif; color: #666; border: solid 1px #666;"">")
                    emailBody.Append("        <tr>")
                    emailBody.Append($"          <td style=""border-bottom:solid thin #666""><img src=""{Resources.Resource_mail.ml_ProductRegistration_body_imglogo_fr}""/></td>")
                    emailBody.Append("          <td style=""border-bottom:solid thin #666"">&nbsp;</td>")
                    emailBody.Append("        </tr>")
                    emailBody.Append("        <tr>")
                    emailBody.Append("          <td colspan=""2"" style=""border-bottom-style:solid; border-bottom-width:thin;"">")
                    emailBody.Append("            <table border=""0"" cellpadding=""1"" cellspacing=""1"" style=""width: 100%; font-family: Arial, Helvetica, sans-serif;"">")
                    emailBody.Append("              <tr style=""height: 16px; background: #d5dee9"">")
                    emailBody.Append($"                <td style=""font-weight: bold; text-align: center;"">ServicesPLUS - {Resources.Resource_mail.ml_ProductRegistration_body_ServiceAgreement_fr}</td>")
                    emailBody.Append("              </tr>")
                    emailBody.Append("            </table>")
                    emailBody.Append("          </td>")
                    emailBody.Append("        </tr>")
                    emailBody.Append($"        <tr style=""vertical-align: top; {IIf(isColorRow, " background-color:#d5dee9;", "")}"">")
                    emailBody.Append($"          <td>{Resources.Resource_mail.ml_ProductRegistration_body_RegistrationDate_fr}</td>")
                    emailBody.Append($"          <td>{Date.Now.ToString("MMM dd, yyyy")}</td>")
                    emailBody.Append("        </tr>")
                    isColorRow = Not isColorRow
                    emailBody.Append($"        <tr style=""vertical-align: top; {IIf(isColorRow, " background-color:#d5dee9;", "")}"">")
                    emailBody.Append("          <td>Service Agreement Number:</td>")
                    emailBody.Append($"          <td>{agreement.AgreementNumber}</td>")
                    emailBody.Append("        </tr>")
                    isColorRow = Not isColorRow

                    ' End-User Information
                    emailBody.Append($"        <tr{IIf(isColorRow, " style=""background-color:#d5dee9;""", "")}>")
                    emailBody.Append($"          <td>{Resources.Resource_mail.ml_RARequest_companyname_fr}</td>")
                    emailBody.Append($"          <td>{agreement.Company}</td>")
                    emailBody.Append("        </tr>")
                    isColorRow = Not isColorRow
                    emailBody.Append($"        <tr{IIf(isColorRow, " style=""background-color:#d5dee9;""", "")}>")
                    emailBody.Append($"          <td>{Resources.Resource_mail.ml_common_Name_fr}:</td>")
                    emailBody.Append($"          <td>{agreement.FirstName} {agreement.LastName}</td>")
                    emailBody.Append("        </tr>")
                    isColorRow = Not isColorRow
                    emailBody.Append($"        <tr{IIf(isColorRow, " style=""background-color:#d5dee9;""", "")}>")
                    emailBody.Append($"          <td>{Resources.Resource_mail.ml_common_Email_fr}:</td>")
                    emailBody.Append($"          <td>{agreement.Email}</td>")
                    emailBody.Append("        </tr>")
                    isColorRow = Not isColorRow
                    emailBody.Append($"        <tr{IIf(isColorRow, " style=""background-color:#d5dee9;""", "")}>")
                    emailBody.Append($"          <td>{Resources.Resource_mail.ml_common_Address_fr}:</td>")
                    emailBody.Append($"          <td>{userAddress}</td>")
                    emailBody.Append("        </tr>")
                    isColorRow = Not isColorRow
                    emailBody.Append($"        <tr{IIf(isColorRow, " style=""background-color:#d5dee9;""", "")}>")
                    emailBody.Append($"          <td>{Resources.Resource_mail.ml_RARequest_phone_fr}:</td>")
                    emailBody.Append($"          <td>{agreement.Phone} {IIf(String.IsNullOrWhiteSpace(agreement.Extension), "", $"&nbsp;&nbsp;&nbsp;Ext: {agreement.Extension}")}</td>")
                    emailBody.Append("        </tr>")
                    isColorRow = Not isColorRow
                    If Not String.IsNullOrWhiteSpace(agreement.Fax) Then
                        emailBody.Append($"        <tr{IIf(isColorRow, " style=""background-color:#d5dee9;""", "")}>")
                        emailBody.Append($"          <td>{Resources.Resource_mail.ml_ProductRegistration_body_fax_fr}:</td>")
                        emailBody.Append($"          <td>{agreement.Fax}</td>")
                        emailBody.Append("        </tr>")
                        isColorRow = Not isColorRow
                    End If

                    ' Reseller Information
                    If agreement.HasReseller Then
                        emailBody.Append($"        <tr style=""background-color:#404040;"">")
                        emailBody.Append("          <td colspan=""2"" style=""color: white; font-weight: bold; text-decoration: underline;"">Reseller Information:</td>")
                        emailBody.Append("        </tr>")
                        If Not String.IsNullOrWhiteSpace(agreement.ResellerCompany) Then
                            emailBody.Append($"        <tr{IIf(isColorRow, " style=""background-color:#d5dee9;""", "")}>")
                            emailBody.Append($"          <td>{Resources.Resource_mail.ml_RARequest_companyname_fr}:</td>")
                            emailBody.Append($"          <td>{agreement.ResellerCompany}</td>")
                            emailBody.Append("        </tr>")
                            isColorRow = Not isColorRow
                        End If
                        If Not String.IsNullOrWhiteSpace(agreement.ResellerName) Then
                            emailBody.Append($"        <tr{IIf(isColorRow, " style=""background-color:#d5dee9;""", "")}>")
                            emailBody.Append($"          <td>{Resources.Resource_mail.ml_common_Name_fr}:</td>")
                            emailBody.Append($"          <td>{agreement.ResellerName}</td>")
                            emailBody.Append("        </tr>")
                            isColorRow = Not isColorRow
                        End If
                        If Not String.IsNullOrWhiteSpace(agreement.ResellerPhone) Then
                            emailBody.Append($"        <tr{IIf(isColorRow, " style=""background-color:#d5dee9;""", "")}>")
                            emailBody.Append($"          <td>{Resources.Resource_mail.ml_RARequest_phone_fr}</td>")
                            emailBody.Append($"          <td>{agreement.ResellerPhone} {IIf(String.IsNullOrWhiteSpace(agreement.ResellerExtension), "", $"&nbsp;&nbsp;&nbsp;Ext: {agreement.ResellerExtension}")}</td>")
                            emailBody.Append("        </tr>")
                            isColorRow = Not isColorRow
                        End If
                    End If

                    ' PRODUCT LIST
                    emailBody.Append($"        <tr style=""background-color:#404040;"">")
                    emailBody.Append("          <td colspan = ""2"" style=""color: white; font-weight: bold;"">" + Resources.Resource_mail.ml_ProductRegistration_body_ProductList_fr + "</td>")
                    emailBody.Append("        </tr>")

                    emailBody.Append("        <tr>")
                    emailBody.Append("          <td colspan=""2"">")
                    emailBody.Append("            <table cellspacing=""0"" cellpadding=""4"" style=""vertical-align: middle; text-align: center; color: #333333; width: 90%; border: none; font-family: Arial, Helvetica, sans-serif; font-size: 11px; "">")
                    emailBody.Append("              <tr style=""background-color:#d5dee9; font-weight:bold;"">")
                    emailBody.Append($"                <th>{Resources.Resource_mail.ml_ProductRegistration_body_ModelNumber_fr}</th>")
                    emailBody.Append($"                <th>{Resources.Resource_mail.ml_ProductRegistration_body_SerialNumber_fr}</th>")
                    emailBody.Append($"                <th>{Resources.Resource_mail.ml_ProductRegistration_body_PurchaseDate_fr}</th>")
                    emailBody.Append("              </tr>")

                    isColorProductRow = False   ' Reset from the English table
                    For Each item As ServiceAgreementItem In agreement.EquipmentList
                        emailBody.Append($"              <tr{IIf(isColorProductRow, " style=""background-color:#d5dee9;""", "")}>")
                        emailBody.Append($"                <td>{item.Model}</td><td>{item.Serial}</td><td>{item.DatePurchased.ToString("MMM dd, yyyy")}</td>")
                        emailBody.Append("              </tr>")
                        isColorProductRow = Not isColorProductRow
                    Next
                    emailBody.Append("            </table>")
                    emailBody.Append("          </td>")
                    emailBody.Append("        </tr>")
                    emailBody.Append("      </table>")
                    emailBody.Append("    </td>")
                    emailBody.Append("  </tr>")
                    emailBody.Append("</table>")
                End If

                If isProduction Then
                    emailSubject = Resources.Resource_mail.ml_ProductRegistration_subject_en
                    If HttpContextManager.GlobalData.IsCanada Then emailSubject &= $" / {Resources.Resource_mail.ml_ProductRegistration_subject_fr}"
                Else
                    emailSubject = $"({ConfigurationData.GeneralSettings.Environment}) {Resources.Resource_mail.ml_ProductRegistration_subject_en}"
                    If HttpContextManager.GlobalData.IsCanada Then emailSubject &= $" / ({ConfigurationData.GeneralSettings.Environment}) {Resources.Resource_mail.ml_ProductRegistration_subject_fr}"
                End If

                ' Send the email
                Dim mailObj As New MailMessage(emailSender, agreement.Email, emailSubject, emailBody.ToString()) With {
                    .IsBodyHtml = True,
                    .Priority = MailPriority.Normal
                }
                If isProduction Then
                    mailObj.Bcc.Add(ConfigurationData.Environment.Email.ServiceAgreement.Service_Agreement_BCC)  ' SupportNET@am.sony.com - Multiple addresses MUST be separated by a comma, i.e. "bob@bob.com,joe@joe.com"
                Else
                    mailObj.CC.Add(ConfigurationData.Environment.Email.ServiceAgreement.Service_Agreement_BCC)
                End If
                mailObj.Attachments.Add(New Attachment(fuReceipt.PostedFile.InputStream, fuReceipt.PostedFile.FileName))

                mailClient = New SmtpClient(ConfigurationData.Environment.SMTP.EmailServer)
                Try
                    mailClient.Send(mailObj)
                Catch ex As Exception
                    Utilities.LogMessages($"An error occurred while sending the Reseller Agreement email.{Environment.NewLine}
                - Message: {ex.Message}{Environment.NewLine}
                - Stack Trace: {ex.StackTrace}{Environment.NewLine}
                - Inner Exception: {ex.InnerException.Message}{Environment.NewLine}")
                End Try
            Catch ex As Exception
                lblError.Text = Utilities.WrapExceptionforUI(ex)
                lblError.Focus()
            End Try
        End Sub
    End Class
End Namespace