<%@ Page Language="VB" AutoEventWireup="false" CodeFile="sony-operation-manual.aspx.vb"
    Inherits="ServicePLUSWebApp.sony_operation_manual" EnableViewStateMac="true" CodeFileBaseClass="ServicePLUSWebApp.SSL" %>

<%@ Register Src="UserControl/SPSPromotion.ascx" TagName="SPSPromotion" TagPrefix="uc1" %>
<%@ Register Src="RepairStatus.ascx" TagName="RepairStatus" TagPrefix="uc2" %>
<%@ Reference Page="~/SSL.aspx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="SonyFooter" Src="SonyFooter.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="TopSonyHeader" Src="TopSonyHeader.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="PersonalMessage" Src="PersonalMessage.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="ServicePlusNav" Src="ServicePlusNav.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="<%=PageLanguage %>">
<head>
    <title>Sony Operational Manual </title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <meta content="Repair, Maintenance, Services, Sony Models, Sony Professional Models, Sony Broadcast Models, Sony Business Models, Sony Software, Sony Professional Software, Sony Broadcast Software, Sony Business Software"
        name="keywords">
    <meta http-equiv="X-UA-Compatible" content="IE=5" />

    <link href="themes/base/jquery.ui.all.css" rel="stylesheet" type="text/css" />
    <link href="includes/jquery.cluetip.css" rel="stylesheet" type="text/css" />
    <link href="includes/ServicesPLUS_style.css" type="text/css" rel="stylesheet" />

    <script src="includes/jquery-1.4.2.min.js" type="text/javascript"></script>
    <script src="includes/jquery.ui.core.js" type="text/javascript"></script>
    <script src="includes/jquery.ui.widget.js" type="text/javascript"></script>
    <script src="includes/jquery.cluetip.js" type="text/javascript"></script>
    <script src="includes/jquery.ui.mouse.js" type="text/javascript"></script>
    <script src="includes/jquery.ui.draggable.js" type="text/javascript"></script>
    <script src="includes/jquery.ui.position.js" type="text/javascript"></script>
    <script src="includes/jquery.ui.resizable.js" type="text/javascript"></script>
    <script src="includes/jquery.ui.dialog.js" type="text/javascript"></script>
    <script src="includes/jquery-ui-1.8.1.custom.min.js" type="text/javascript"></script>
    <script src="includes/ServicesPLUS.js" type="text/javascript"></script>
    <script src="includes/svcplus_globalization.js" type="text/javascript"></script>

    <script type="text/javascript">
        function SetFocus() {
            $("#ModelNumber").focus(); //Sasikumar WP SPLUS_WP012
        }
        //Model  PopUp widow
        function OrderLookUp(cnt, models) {//debugger;
            var lstModels = new Array(models.split(','));
            $("#dgManual").hide();
            $("#lblError").hide();
            $("#divMessage").text('');
            var buttons = $('.ui-dialog-buttonpane').children('button');
            buttons.remove();
            $("#divMessage").append(' <br/>');
            $("divMessage").dialog("destroy");
            $("#divMessage").dialog({ // dialog box
                title: '<span class="modalpopup-title">Model Renamed</span>',
                height: 280,
                width: 560,
                modal: true,
                position: 'top',
                close: function () {
                    redirectTo();
                }
            });
            var content = "<span class='modalpopup-content'>Your Search criteria for Model Number matches models which have been renamed." + "<br/>";
            content += "Operations manuals may refer to either old model or new model names.<br/>";
            content += "Your search results will include any records which match either a new model name or an<br/>";
            content += "old model name listed below</span>";
            content += "<br>" + "<table class='modalpopup-content' style='margin-left:100px;'>"
            content += '<tr><td width="100px"><u>Old Model Name</u></td><td width="100px"><u>New Model Name</u></td></tr>';
            for (i = 0; i < cnt; i++) {
                var lstmodel = new Array(lstModels[0][i].split(':'));
                content += '<tr><td>' + lstmodel[0][0] + '</td><td>' + lstmodel[0][1] + '</td></tr>';
            }
            content += "</table>"
            $("#divMessage").append(content); //message to display in divmessage div
            $("#divMessage").append('<br/> <br/><a href="#" onclick="javascript:return ClosePopUp();"><img src ="<%=Resources.Resource.img_btnCloseWindow %>" alt ="Close" id="messagego" /></a>');
            $("#messagego").focus();
            return false;
        }

        function ClosePopUp() {
            $("#divMessage").dialog("close");
        }

        function redirectTo() {
            $("#dgManual").show();
            $("#lblError").show();
            return true;
        }
    </script>

</head>
<body style="color: #000000; background: #5d7180; margin: 0px;" onload="javascript:SetFocus();"
    onkeydown="javascript:SetTheFocusButton(event, 'btnSearch');">
    <div id="divMessage" style="font: 11px; font-family: Arial; overflow: auto;"></div>

    <form id="Form1" method="post" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" />
        <center>
            <table width="710" border="0" role="presentation">
                <tr>
                    <td style="width: 25px; background: url('images/sp_left_bkgd.gif');">
                        <img src="images/spacer.gif" height="25" width="25" alt="">
                    </td>
                    <td width="689" bgcolor="#ffffff">
                        <table width="100%" border="0" role="presentation">
                            <tr>
                                <td style="width: 714px">
                                    <ServicePLUSWebApp:TopSonyHeader ID="TopSonyHeader" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td height="23" style="width: 714px">
                                    <nav><ServicePLUSWebApp:ServicePlusNav ID="SonyNav" runat="server" /></nav>
                                </td>
                            </tr>
                            <%--Sasikumar WP SPLUS_WP012--%>
                            <tr>
                                <td>
                                    <table width="710" border="0" role="presentation">
                                        <tr style="height: 82px;">
                                            <td style="width: 465px; text-align: right; vertical-align: middle; background: #363d45 url('images/Manuals_Banner.jpg');">
                                                <h1 class="headerText"><%=Resources.Resource.opration_manual_msg()%>&nbsp;<%=Resources.Resource.opration_manual_msg_1()%>&nbsp;&nbsp;</h1>
                                            </td>
                                            <td valign="middle" width="246" bgcolor="#363d45">
                                                <ServicePLUSWebApp:PersonalMessage ID="PersonalMessageDisplay" runat="server" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <%--Sasikumar WP SPLUS_WP012--%>
                            <tr>
                                <td>
                                    <div id="hiddenSonyManual" runat="server">
                                        <table width="710" border="0" role="presentation">
                                            <tr>
                                                <td width="466" bgcolor="#f2f5f8">
                                                    <table width="464" border="0" role="presentation">
                                                        <tr>
                                                            <td width="20">
                                                            </td>
                                                            <td>
                                                                <span class="bodycopy"><span class="bodyCopySM"><%=Resources.Resource.contact_canada_support %><a href="mailto:<%=Resources.Resource.contact_canada_support_email%>"><%=Resources.Resource.contact_canada_support_email%></a></span></span>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td bgcolor="#99a8b5" valign="bottom">
                                                </td>
                                            </tr>
                                            <tr style="height: 9px;">
                                                <td width="466" bgcolor="#f2f5f8">
                                                    <img height="9" src="images/sp_int_header_btm_left_onepix.gif" width="464" alt="">
                                                </td>
                                                <td bgcolor="#99a8b5">
                                                    <img height="9" src="images/sp_int_header_btm_right.gif" width="246" alt="">
                                                </td>
                                            </tr>
                                        </table>
                                        <br />
                                    </div>
                                </td>
                            </tr>
                            <%--Sasikumar WP SPLUS_WP012--%>
                            <div id="DefaultSonyManual" runat="server">
                                <tr>
                                    <td style="width: 714px">
                                        <table width="710px" border="0" role="presentation">

                                            <tr valign="top">
                                                <td bgcolor="#f2f5f8" style="width: 465px">
                                                    <table width="464" border="0" role="presentation">
                                                        <tr>
                                                            <td width="20">
                                                                <img height="20" src="images/spacer.gif" width="20" alt="">
                                                            </td>
                                                            <td>
                                                                <h2 class="headerTitle">Search Operation Manuals</h2>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td width="20" colspan="2">
                                                                <img height="10" src="images/spacer.gif" width="20" alt="">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td width="20">
                                                                <img height="20" src="images/spacer.gif" width="20" alt="">
                                                            </td>
                                                            <td>
                                                                <asp:UpdatePanel ID="UpdatePanel2" runat="server" ChildrenAsTriggers="true">
                                                                    <ContentTemplate>
                                                                        <table id="Table1" align="left" border="0" cellpadding="1" class="bodycopy" role="presentation">
                                                                            <tr>
                                                                                <td width="20%">
                                                                                    <label id="lblModelNumber" for="ModelNumber" class="bodyCopyBold">Model Number</label>
                                                                                </td>
                                                                                <td width="20%"></td>
                                                                                <td width="20%"></td>
                                                                                <td width="40%"></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <SPS:SPSTextBox ID="ModelNumber" runat="server" CssClass="bodyCopy" aria-role="search"></SPS:SPSTextBox>
                                                                                </td>
                                                                                <td colspan="2">
                                                                                    <asp:ImageButton ID="btnSearch" runat="server" AlternateText="View search results for the provided model number."
                                                                                        ImageUrl="images/sp_int_submitGreybkgd_btn.gif" />
                                                                                </td>
                                                                                <td class="tableHeader">
                                                                                    <asp:UpdateProgress ID="UpdateProgress1" runat="server">
                                                                                        <ProgressTemplate>
                                                                                            <span id="Span4" class="bodycopy">Please wait...</span>
                                                                                            <img src="images/progbar.gif" width="100" alt="Please wait..." />
                                                                                        </ProgressTemplate>
                                                                                    </asp:UpdateProgress>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td colspan="4" height="15px">
                                                                                    <asp:Label ID="lblError" CssClass="redAsterick" runat="server" />
                                                                                </td>
                                                                                <td colspan="1" height="15">
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </ContentTemplate>
                                                                    <Triggers>
                                                                    </Triggers>
                                                                </asp:UpdatePanel>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td width="238" bgcolor="#99a8b5">
                                                    <table role="presentation">
                                                        <tr>
                                                            <td colspan="2">
                                                                <img height="10" src="images/spacer.gif" width="20" alt=""><br />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <img height="5" width="10" src="images/spacer.gif" alt="">
                                                            </td>
                                                            <td class="tableheader">
                                                                Looking for Service Manuals?
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2">
                                                                <img height="5" src="images/spacer.gif" width="20" alt=""><br />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <img height="5" width="10" src="images/spacer.gif" alt="">
                                                            </td>
                                                            <td>
                                                                <span class="finderCopyDark">To access over 1,200 service manuals via a subscription to the Electronic Service Manual Library,
                                                                    <a href="https://docs.sony.com/reflib/" target="_blank" style="color: #333;" title="Click to visit the Sony Reference Library in a new tab.">click here</a>.  Individual service manuals
                                                                    may be purchased by calling 1-800-538-7550.</span>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr style="height: 3px;">
                                                <td width="464" valign="top">
                                                    <img height="9" src="images/sp_int_header_btm_left_onepix.gif" alt="" width="464" /><br />
                                                </td>
                                                <td width="246" valign="top">
                                                    <img height="9" src="images/sp_int_header_btm_right.gif" alt="" width="246" /><br />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 714px">
                                        <table width="100%" border="0" role="presentation">
                                            <tr>
                                                <td colspan="3">
                                                    <img height="20" src="images/spacer.gif" width="20" alt="">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="2%">
                                                    <img height="1" src="images/spacer.gif" width="1" alt="">
                                                </td>
                                                <td style="border: 1px solid #d5dee9; vertical-align: top;" width="96%">
                                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                                        <ContentTemplate>
                                                            <asp:DataGrid ID="dgManual" runat="server" AutoGenerateColumns="False" CellPadding="4"
                                                                ForeColor="#333333" GridLines="None" Width="100%" AllowPaging="True" AllowSorting="True"
                                                                PageSize="8" UseAccessibleHeader="true">
                                                                <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                                                <EditItemStyle BackColor="#2461BF" />
                                                                <SelectedItemStyle BackColor="#D1DDF1" Font-Bold="True" Font-Italic="False" Font-Overline="False"
                                                                    Font-Strikeout="False" Font-Underline="False" ForeColor="#333333" />
                                                                <PagerStyle BackColor="#D5DEE9" CssClass="tableHeader" HorizontalAlign="Left" Mode="NumericPages" />
                                                                <AlternatingItemStyle BackColor="White" Font-Bold="False" Font-Italic="False" Font-Overline="False"
                                                                    Font-Strikeout="False" Font-Underline="False" />
                                                                <ItemStyle BackColor="#EFF3FB" Font-Bold="False" Font-Italic="False" Font-Overline="False"
                                                                    Font-Strikeout="False" Font-Underline="False" />
                                                                <Columns>
                                                                    <asp:BoundColumn DataField="OLD_MODIFIEDMODEL" HeaderText="Model" Visible="false"></asp:BoundColumn>

                                                                    <asp:TemplateColumn HeaderText="Model" ItemStyle-Width="35%" ItemStyle-Height="100%">
                                                                        <ItemStyle CssClass="bodyCopy" Font-Bold="False" Font-Italic="False" Font-Overline="False"
                                                                            Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Left" VerticalAlign="Middle" />
                                                                        <ItemTemplate>
                                                                            <%#DataBinder.Eval(Container, "DataItem.MODIFIEDMODEL")%>
                                                                            <br />
                                                                            <asp:Label ID="lblModel" CssClass="tableData" runat="server" Width="100%" Visible="false" />
                                                                        </ItemTemplate>
                                                                    </asp:TemplateColumn>

                                                                    <asp:BoundColumn DataField="PN_LONG" HeaderText="Part Number">
                                                                        <HeaderStyle Width="20%" />
                                                                        <ItemStyle CssClass="bodyCopy" Font-Bold="False" Font-Italic="False" Font-Overline="False"
                                                                            Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Left" VerticalAlign="Middle" />
                                                                    </asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="DESCRIPTION" HeaderText="Description">
                                                                        <HeaderStyle Width="20%" />
                                                                        <ItemStyle CssClass="bodyCopy" Font-Bold="False" Font-Italic="False" Font-Overline="False"
                                                                            Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Left" VerticalAlign="Middle" />
                                                                    </asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="VERSION" HeaderText="Version">
                                                                        <HeaderStyle Width="10%" HorizontalAlign="Center" />
                                                                        <ItemStyle CssClass="bodyCopy" Font-Bold="False" Font-Italic="False" Font-Overline="False"
                                                                            Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                    </asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="VERSION_DATE" HeaderText="Version Date" DataFormatString="{0:MM/dd/yyyy}">
                                                                        <HeaderStyle Width="15%" />
                                                                        <ItemStyle CssClass="bodyCopy" Font-Bold="False" Font-Italic="False" Font-Overline="False"
                                                                            Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Left" VerticalAlign="Middle" />
                                                                    </asp:BoundColumn>
                                                                    <asp:TemplateColumn HeaderText="View">
                                                                        <ItemTemplate>
                                                                            <a href="http://pdf.crse.com/manuals/<%#DataBinder.Eval(Container, "DataItem.FILENAME") %>"
                                                                                target="_blank" class="bodyCopy" title="Click to view this PDF manual in a new tab.">View</a>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle Width="10%" HorizontalAlign="Center" />
                                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                            Font-Underline="False" HorizontalAlign="Center" />
                                                                    </asp:TemplateColumn>
                                                                </Columns>
                                                                <HeaderStyle BackColor="#D5DEE9" CssClass="tableHeader" Font-Bold="True" />
                                                            </asp:DataGrid>
                                                        </ContentTemplate>
                                                        <Triggers>
                                                            <asp:AsyncPostBackTrigger ControlID="btnSearch" EventName="Click" />
                                                            <%-- <asp:AsyncPostBackTrigger ControlID="btnClearSearch" EventName="Click" />--%>
                                                        </Triggers>
                                                    </asp:UpdatePanel>
                                                </td>
                                                <td width="2%">
                                                    <img height="1" src="images/spacer.gif" width="1" alt="">
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 714px">
                                        <img height="1" src="images/spacer.gif" width="1" alt="">
                                    </td>
                                </tr>
                            </div>
                            <tr>
                                <td width="100%">
                                    <footer><ServicePLUSWebApp:SonyFooter ID="SonyFooter1" runat="server" /></footer>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td style="width: 25px; background: url('images/sp_right_bkgd.gif');">
                        <img height="5" src="images/spacer.gif" width="25" alt="">
                    </td>
                </tr>
            </table>
        </center>
        &nbsp;
    </form>
</body>
</html>
