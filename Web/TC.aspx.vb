Imports System.IO
Imports Sony.US.ServicesPLUS.Process
Imports Sony.US.ServicesPLUS.Core

Namespace ServicePLUSWebApp

    Partial Class TC
        Inherits SSL

        Private Const currentPageIndex As Integer = 2
        Private Const currentProcessIndex As Integer = 0

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub


        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Dim software As Software
            Dim itemNumber As Integer

            Session.Add("Accept", False)

            If (Not Page.IsPostBack) Then
                Response.CacheControl = "no-cache"
                Response.AddHeader("Pragma", "no-cache")
                Response.Expires = -1

                'If HttpContextManager.Customer IsNot Nothing Then objCustomer = HttpContextManager.Customer 'Changes done towards Bug# 1212

                If (Session.Item("CurrentPageIndex") IsNot Nothing) AndAlso (Session.Item("currentProcessIndex") IsNot Nothing) Then
                    If Convert.ToInt32(Session.Item("CurrentPageIndex").ToString()) < currentPageIndex And Convert.ToInt32(Session.Item("currentProcessIndex").ToString()) = currentProcessIndex Then
                        Session.Item("CurrentPageIndex") = currentPageIndex.ToString()
                    Else    ' Back button was clicked.
                        Response.Redirect("SessionExpired.aspx")
                    End If
                Else
                    'Response.Write("go to error page")
                    '-- current session expired, redo the whole process
                    If Request.QueryString("ln") Is Nothing Then
                        Response.Redirect("SessionExpired.aspx")
                    End If
                End If

                If Request.QueryString("ln") IsNot Nothing Then
                    itemNumber = Convert.ToInt16(Request.QueryString("ln"))
                    If itemNumber >= 0 Then
                        software = CType(Session.Item("software"), ArrayList)(itemNumber)
                        Session.Add("FreeDownloadSoftware", software)
                    Else 'check free download selected before loginned
                        If Not Session("SelectedFreeSoftwareBeforeLoggedIn") Is Nothing Then
                            software = CType(Session("SelectedFreeSoftwareBeforeLoggedIn"), Software)
                            Session.Add("FreeDownloadSoftware", software)
                            Session("SelectedFreeSoftwareBeforeLoggedIn") = Nothing 'clear
                        End If
                    End If
                End If
            End If
        End Sub
        Private Sub Decline_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Decline.Click
            Try
                Response.Redirect("TCDeclined.aspx")
            Catch ex As Threading.ThreadAbortException
                '-- do nothing
            End Try
        End Sub

        'Private Sub cancelOpenOrder()
        '    If Not Session.Item("order") Is Nothing And Not Session.Item("carts") Is Nothing Then

        '        Try
        '            Dim order As Sony.US.ServicesPLUS.Core.Order = Session.Item("order")
        '            Dim cart As Sony.US.ServicesPLUS.Core.ShoppingCart = Session.Item("carts")
        '            Dim om As New OrderManager
        '            'Modified By Sunil for Bug 2027 on 23-02-2010
        '            If Not HttpContextManager.Customer Is Nothing Then objCustomer = HttpContextManager.Customer
        '            'Modified By Sunil for Bug 2027 on 23-02-2010

        '            '***************Changes done for Bug# 1212 Starts here***************
        '            '**********Kannapiran S****************
        '            
        '            objSISParameter.UserLocation = objCustomer.UserLocation
        '            om.CancelOrder(order, cart)
        '            '***************Changes done for Bug# 1212 Ends here***************
        '        Catch ex As Exception
        '            'ErrorLabel.Text = ex.Message
        '        End Try

        '        '-- clear CurrentPageIndex, currentProcessIndex - starting over again
        '        If Not Session.Item("CurrentPageIndex") Is Nothing Then
        '            Session.Remove("CurrentPageIndex")
        '        End If
        '        If Not Session.Item("currentProcessIndex") Is Nothing Then
        '            Session.Remove("currentProcessIndex")
        '        End If
        '    End If
        'End Sub

        Private Sub Accept_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Accept.Click
            Session.Item("Accept") = True

            ' Forward to ConfirmOrder, unless this is a Software download
            If Request.QueryString("ln") Is Nothing Then Response.Redirect("ConfirmOrder.aspx")

            If Session("FreeDownloadSoftware") IsNot Nothing Then
                Dim freeDownloadSoftware = CType(Session("FreeDownloadSoftware"), Software)
                Dim orderMan As New OrderManager
                Dim downloadRecord As New DownloadRecord
                'Free download software
                'log the download record
                If HttpContextManager.Customer IsNot Nothing Then
                    downloadRecord.SIAMID = HttpContextManager.Customer.SIAMIdentity
                Else
                    downloadRecord.SIAMID = "Anonymous"
                End If
                downloadRecord.KitPartNumber = freeDownloadSoftware.KitPartNumber
                downloadRecord.DeliveryMethod = "Download"
                downloadRecord.Time = Now
                orderMan.LogDownload(downloadRecord)

                '-- clear CurrentPageIndex, currentProcessIndex - not useful for free downloads
                Session.Remove("CurrentPageIndex")
                Session.Remove("currentProcessIndex")

                'then redirect 
                Response.Redirect("SoftwareFreeDownload.aspx")
            End If
        End Sub
    End Class

End Namespace
