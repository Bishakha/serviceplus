Imports Sony.US.ServicesPLUS.Core

Namespace ServicePLUSWebApp

    Partial Class sony_service_print_form
        Inherits SSL

        Dim customer As Customer
        Dim sModelName As String
        Public mDepotServiceCharge As String = Sony.US.SIAMUtilities.ConfigurationData.GeneralSettings.RepairCosting.DepotServiceCharge

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            If Request.QueryString("model") IsNot Nothing Then
                sModelName = Request.QueryString("model").Trim()
            End If

            lblSubHdr.InnerText = sModelName + Resources.Resource.repair_svcacc_dept_msg

        End Sub

        Protected Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete

        End Sub

        Protected Sub imgBtnNext_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgBtnNext.Click
            Response.Redirect("sony-service-repair-maintenance.aspx")
        End Sub

    End Class
End Namespace