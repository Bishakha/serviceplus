<%@ Reference Page="~/SSL.aspx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="SonyFooter" Src="SonyFooter.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="ServicePlusNav" Src="ServicePlusNav.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="TopSonyHeader" Src="TopSonyHeader.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="PersonalMessage" Src="PersonalMessage.ascx" %>

<%@ Page Language="vb" AutoEventWireup="false" Inherits="ServicePLUSWebApp.NotificationPreferences" EnableViewStateMac="True" CodeFile="Notification-Preferences.aspx.vb" CodeFileBaseClass="ServicePLUSWebApp.SSL" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="<%=PageLanguage %>">
<head>
    <title>ServicesPLUS - Notification Preferences</title>
    <meta content="True" name="vs_showGrid">
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link href="includes/ServicesPLUS_style.css" type="text/css" rel="stylesheet">
    <script language="javascript" src="includes/ServicesPLUS.js" type="text/javascript"></script>
    <script type="text/javascript" src="includes/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="includes/svcplus_globalization.js"></script>
</head>
<body style="color: #000000; background: #5d7180; margin: 0px;">
    <form id="Form1" method="post" runat="server">
        <center>
            <table width="760" border="0" role="presentation">
                <tr>
                    <td width="25" style="background: url('images/sp_left_bkgd.gif');">
                        <img src="images/spacer.gif" width="25" height="25" alt="">
                    </td>
                    <td width="710" bgcolor="#ffffff">
                        <table width="710" border="0" role="presentation">
                            <tr>
                                <td>
                                    <ServicePLUSWebApp:TopSonyHeader ID="TopSonyHeader" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <nav><ServicePLUSWebApp:ServicePlusNav ID="SonyNav" runat="server" /></nav>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="710" border="0" role="presentation">
                                        <tr>
                                            <td width="464" bgcolor="#363d45" height="57" background="images/sp_int_header_top_ServicesPLUS_onepix.gif"
                                                align="right" valign="top">
                                                <br />
                                                <h1 class="headerText">ServicesPLUS &nbsp;&nbsp;</h1>
                                            </td>
                                            <td valign="top" bgcolor="#363d45">
                                                <table width="246" border="0" role="presentation">
                                                    <tr>
                                                        <td colspan="3">
                                                            <img height="24" src="images/sp_int_header_top_right.gif" width="246" alt=""></td>
                                                    </tr>
                                                    <tr>
                                                        <td width="5">&nbsp;</td>

                                                        <td width="236">
                                                            <span class="memberName">
                                                                <ServicePLUSWebApp:PersonalMessage ID="PersonalMessageDisplay" runat="server" />
                                                            </span>
                                                        </td>
                                                        <td width="5">&nbsp;</td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td bgcolor="#f2f5f8">
                                                <h2 class="headerTitle" style="padding-right: 20px; text-align: right;"><%=Resources.Resource.el_rgn_NotifyHdl1%></h2></td>
                                            </td>
                                            <td bgcolor="#99a8b5">&nbsp;</td>
                                        </tr>
                                        <tr height="9">
                                            <td bgcolor="#f2f5f8">
                                                <img height="9" src="images/sp_int_header_btm_left_onepix.gif" width="464" alt=""></td>
                                            <td bgcolor="#99a8b5">
                                                <img height="9" src="images/sp_int_header_btm_right.gif" width="246" alt=""></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="710" border="0" role="presentation">
                                        <tr>
                                            <td width="20" height="20">
                                                <img height="20" src="images/spacer.gif" width="20" alt="">
                                            </td>
                                            <td width="670" height="20">
                                                <asp:Label ID="MasterErrorLabel" runat="server" ForeColor="Red" CssClass="tableData" EnableViewState="False" />
                                            </td>
                                            <td width="20" height="20">
                                                <img height="20" src="images/spacer.gif" width="20" alt="">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="20">
                                                <img height="20" src="images/spacer.gif" width="20" alt="">
                                            </td>
                                            <td valign="top" width="670">
                                                <table cellpadding="3" width="664" border="0" role="presentation">
                                                    <tr>
                                                        <td colspan="2">
                                                            <table role="presentation">
                                                                <tr>
                                                                    <td valign="top">
                                                                        <input type="checkbox" id="optin" checked="checked" runat="server">
                                                                    </td>
                                                                    <td>
                                                                        <label for="optin" class="bodyCopy"><%=Resources.Resource.el_rgn_NotifyAgree%> </label>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr bgcolor="#d5dee9">
                                                        <td colspan="2" style="height: 25px">
                                                            <span class="tableHeader">&nbsp;&nbsp;&nbsp;<%=Resources.Resource.el_rgn_NotifyHdl1%> </span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                            <span class="bodyCopy"><%=Resources.Resource.el_rgn_NotifyContent3%></span><br />
                                                            <img height="10" src="images/spacer.gif" width="1" alt=""><br />
                                                            <asp:Panel ID="marketingInterestTable" runat="server" />
                                                            <br />
                                                            <asp:ImageButton ID="UpdateProfile" runat="server" ImageUrl="<%$Resources:Resource,sna_svc_img_65%>" Width="92" role="button"
                                                                Height="30" AlternateText="Update notification preferences" title="Click to save updated notification preferences." />&nbsp;
                                                        </td>
                                                    </tr>
                                                </table>
                                                <br />
                                            </td>
                                            <td width="20">
                                                <img height="20" src="images/spacer.gif" width="20" alt=""></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <footer><ServicePLUSWebApp:SonyFooter ID="SonyFooter1" runat="server" /></footer>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="25" style="background: url('images/sp_right_bkgd.gif')">
                        <img src="images/spacer.gif" width="25" height="20" alt="">
                    </td>
                </tr>
            </table>
        </center>
    </form>
</body>
</html>