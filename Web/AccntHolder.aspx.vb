Imports System.Data
Imports System.Globalization
Imports ServicesPlusException
Imports Sony.US.AuditLog
Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Data
Imports Sony.US.ServicesPLUS.Process
Imports Sony.US.SIAMUtilities

Namespace ServicePLUSWebApp
    Partial Class AccntHolder
        Inherits SSL

        Public Auditcustomer As Customer 'Added by Narayanan July 31 for AuditTrail
        Private usCulture As New CultureInfo("en-US")
        'Added for PCI Compliance E736
        Dim CCNo As String = ""
        Private cm As New CustomerManager
        Private pm As New PaymentManager
        Private siamID As String
        Private cart As ShoppingCart
        Private customer As Customer
        Private current_bill_to As Account
        Private current_ship_to As Account
        Private current_ship_to_list() As Account
        Private current_bill_to_list() As Account
        Private focusFormField As String = "ddlBillTo"
        Private Const currentPageIndex As String = "1"
        Private Const currentProcessIndex As String = "0"

        ' 2018-07-18 ASleight - Seems these were removed from the page at some point, and these variables were added to
        '                       get rid of parser errors in an event that cannot fire anymore.
        'Protected WithEvents rbSameBill As System.Web.UI.WebControls.RadioButton
        'Protected WithEvents NameBill As Sony.US.ServicesPLUS.Controls.SPSTextBox
        'Protected WithEvents Line1Bill As Sony.US.ServicesPLUS.Controls.SPSTextBox
        'Protected WithEvents Line2Bill As Sony.US.ServicesPLUS.Controls.SPSTextBox
        'Protected WithEvents CityBill As Sony.US.ServicesPLUS.Controls.SPSTextBox
        'Protected WithEvents ZipBill As Sony.US.ServicesPLUS.Controls.SPSTextBox
        'Protected WithEvents ddlStateBill As System.Web.UI.WebControls.DropDownList


#Region " Web Form Designer Generated Code "
        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            isProtectedPage(True)
            InitializeComponent()
        End Sub
#End Region

#Region "   Events  "
        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As EventArgs) Handles MyBase.Load
            Try
                ErrorLabel.Text = ""

                FormFieldInitialization()
                lblCCName.Visible = False
                CCName.Visible = False
                LabelNName.Visible = False
                TextBox6.Visible = False
                LabelLine5.CssClass = ""

                If (Not Page.IsPostBack) Then
                    'BuildStateDropDown()
                    populateStateDropdownList(ddlState)
                    fnZipCodeConfig()

                    lblCCName.Visible = True
                    CCName.Visible = True
                    LabelNName.Visible = True
                    TextBox6.Visible = True
                    LabelLine5.CssClass = ""
                    Response.CacheControl = "no-cache"
                    Response.AddHeader("Pragma", "no-cache")
                    Response.Expires = -1
                    If Session.Item("CurrentPageIndex") IsNot Nothing And Session.Item("currentProcessIndex") IsNot Nothing Then
                        If Convert.ToInt32(Session.Item("CurrentPageIndex").ToString()) < Convert.ToInt32(currentPageIndex) And Convert.ToInt32(Session.Item("currentProcessIndex").ToString()) = Convert.ToInt32(currentProcessIndex) Then
                            Session.Item("CurrentPageIndex") = currentPageIndex
                        Else
                            '-- go to error 
                            '-- back button clicked
                            Response.Redirect("SessionExpired.aspx&returnto=cart", False)
                            HttpContext.Current.ApplicationInstance.CompleteRequest()
                        End If
                    Else
                        'Response.Write("go to error page")
                        '-- current session expired, redo the whole process
                        Response.Redirect("SessionExpired.aspx&returnto=cart", False)
                        HttpContext.Current.ApplicationInstance.CompleteRequest()
                    End If
                    pnlNickName.Visible = False
                    pnlCCInfo.Visible = False
                    pnlSavedCard.Visible = False
                Else
                    If pnlCCInfo.Visible = True Then
                        If Session("isSaveCardClicked") = "true" Then
                            pnlSavedCard.Visible = True
                            lblCCName.Visible = False
                            CCName.Visible = False
                        Else
                            pnlSavedCard.Visible = False
                            lblCCName.Visible = True
                            CCName.Visible = True
                        End If
                    Else
                        pnlCCInfo.Visible = False
                        pnlSavedCard.Visible = False
                    End If
                End If
            Catch thrEx As Threading.ThreadAbortException
                '-- do nothing
            End Try

            Try
                customer = HttpContextManager.Customer
                If (customer IsNot Nothing) Then
                    Dim cardsNotHidden = From c In customer.CreditCards Where c.HideCardFlag = False
                    If (customer.CreditCards.Length = 0) Or (cardsNotHidden.Count = 0) Then
                        btnCCSaved.Visible = False
                        ddlMyCard.Enabled = False
                    ElseIf customer.CreditCards.Length = 1 Then
                        If String.IsNullOrWhiteSpace(customer.CreditCards(0)?.NickName) Then
                            btnCCSaved.Visible = False
                        Else
                            btnCCSaved.Visible = True
                            ddlMyCard.Enabled = True
                        End If
                    Else    ' More than one card
                        btnCCSaved.Visible = True
                        ddlMyCard.Enabled = True
                    End If

                    cart = Session.Item("carts")
                    If cart Is Nothing Then
                        ErrorLabel.Text = (Resources.Resource.el_EmptyShoppingCard) ' "Shopping cart is empty."
                        ErrorLabel.Visible = True
                        Return
                    End If

                    If Not Page.IsPostBack Then
                        'hide the credit card details on initial page load
                        hideCCDetails()
                        BuildDDls()
                        ShiptoDisplay()

                        If Not String.IsNullOrEmpty(cart.PurchaseOrderNumber) Then txtPONumber.Text = cart.PurchaseOrderNumber
                        '-- if download only item are selected the we'll select ground shipping so that the customer doesn't have to pick a shipping 
                        '-- method and so the page validation wil still work. - dwd
                        '- hide paymentmethod
                        If cart.Type = ShoppingCart.enumCartType.Download Then
                            ShipMethod.Initialize(False)
                        Else
                            ShipMethod.Initialize(True)
                        End If
                        ' ASleight - Why not use ddlShipTo.Focus() ? Why cause a postback after Page_Load?
                        RegisterStartupScript("SetFocus", "<script language='javascript'>document.getElementById('" + focusFormField + "').focus()</script>")
                    End If
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                hideAllControl()
            End Try
        End Sub

        Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
            '-- clear CurrentPageIndex, currentProcessIndex - restart
            Try
                Session.Remove("CurrentPageIndex")
                Session.Remove("currentProcessIndex")
                Response.Redirect("vc.aspx", False)
                HttpContext.Current.ApplicationInstance.CompleteRequest()
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Sub btnNext_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnNext.Click
            Dim objPCILogger As New PCILogger()
            Dim ship_to_overriden As Boolean = False
            Dim whichMethod As Integer
            Dim CCnumberaccholder() As String
            Dim customerName As String
            Dim orderMgr As New OrderManager
            Dim cartMgr As New ShoppingCartManager
            Dim payMgr As New PaymentManager
            Dim cart As ShoppingCart
            Dim ship_methods() As ShippingMethod
            Dim method As New ShippingMethod
            Dim isCreditCardOrder As Boolean = False
            Dim order As Order

            ' Credit Card variables
            Dim card As CreditCard = Nothing
            Dim nickNameList As New ArrayList
            Dim iscreditCardAdd As Boolean = True
            Dim nYear As Integer
            Dim nMonth As Integer
            Dim nDay As Integer
            Dim sCCNumber As String
            Dim exp_date As Date
            Dim whichCCType As Integer
            Dim index As Integer
            Dim card_types() As CreditCardType
            Dim credit_card_type As CreditCardType = Nothing

            Try
                isCreditCardOrder = (rbBillTo.SelectedValue = "2")
                If isCreditCardOrder Then ' "Bill to my Credit Card"
                    'If btnCCSaved.Visible = True AndAlso btnNewCC.Visible = True Then
                    '    ' ErrorLabel.Text = "Please Enter the New Creditcard Details or select Saved card."
                    'End If
                    If String.IsNullOrEmpty(CCNumber.Text) Then
                        ErrorLabel.Text = Resources.Resource.el_CreditCardOptions   '"Please Enter the New Creditcard Details or select Saved card"
                        Return
                    End If
                End If
                objPCILogger.EventOriginApplication = "ServicesPLUS"
                objPCILogger.EventOriginApplicationLocation = Me.Page.GetType().Name
                objPCILogger.EventOriginMethod = "btnNext_Click"
                objPCILogger.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
                objPCILogger.OperationalUser = Environment.UserName
                objPCILogger.EventDateTime = DateTime.Now.ToLongTimeString()
                objPCILogger.EventType = EventType.Add
                objPCILogger.HTTPRequestObjectValues = GetRequestContextValue()
                objPCILogger.CustomerID = customer.CustomerID
                objPCILogger.CustomerID_SequenceNumber = customer.SequenceNumber
                objPCILogger.EmailAddress = customer.EmailAddress
                objPCILogger.SIAM_ID = customer.SIAMIdentity
                objPCILogger.LDAP_ID = customer.LdapID
                objPCILogger.IndicationSuccessFailure = "Failure"

                If Session("token") IsNot Nothing Then
                    CCnumberaccholder = Session("token").ToString().Split("-")
                    CCNumber.Text = "XXXXXXXXXXXX" & CCnumberaccholder(2).ToString()
                End If
                ValidateForm()

                If ErrorValidation.Visible Then
                    If String.IsNullOrEmpty(ErrorValidation.Text) Then
                        ErrorValidation.Text = Resources.Resource.el_rgn_Requirevalidate   '"Please complete the required fields."
                    End If
                    objPCILogger.Message = ErrorValidation.Text
                    objPCILogger.PushLogToMSMQ()
                    Return
                End If

                'Get Cart
                cart = Session.Item("carts")
                If cart Is Nothing Then
                    ErrorLabel.Text = (Resources.Resource.el_EmptyCart)  '"Cart is empty.")
                    ErrorLabel.Visible = True
                    Return
                End If

                'Shipping Method
                ship_methods = Session.Item("shipmethods")

                whichMethod = ShipMethod.SelectedShipMethodIndex
                method = ship_methods.GetValue(whichMethod)
                method.ISBackOrderSameShippingMethod = False

                current_bill_to = Session.Item("current_bill_to")
                current_ship_to = Session.Item("current_ship_to")
                Session.Item("ShiptoCount") = ddlShipTo.Items.Count
                Session.Item("BilltoCount") = ddlBillTo.Items.Count
                current_ship_to.SAPAccount = ddlShipTo.SelectedValue

                'ship to address may have been overriden
                If current_ship_to IsNot Nothing Then
                    If current_ship_to.AddressOverride <> SISAccount.AddressOverrideType.NotAllowed Then
                        customerName = customer.FirstName & " " & customer.LastName
                        If customerName.Length > 30 Then
                            customerName = customerName.Substring(0, 30)
                        End If
                        If (txtAttnName.Text <> customerName) Or (txtAddress1.Text <> current_ship_to.Address.Line1) Or (txtAddress2.Text <> current_ship_to.Address.Line2) Then
                            ship_to_overriden = True
                        ElseIf (txtCity.Text <> current_ship_to.Address.City) Or (ddlState.SelectedValue.ToUpper() <> current_ship_to.Address.State.ToUpper()) Then
                            ship_to_overriden = True
                        ElseIf (txtZip.Text <> current_ship_to.Address.PostalCode) Or (txtPhoneShip.Text <> current_ship_to.Customer.PhoneNumber) Then
                            ship_to_overriden = True
                        ElseIf (txtShipExtension.Text <> current_ship_to.Customer.PhoneExtension) Or (txtCompanyName.Text <> customer.CompanyName) Then
                            ship_to_overriden = True
                        End If

                        If ship_to_overriden Then
                            current_ship_to.Address.Attn = txtAttnName.Text
                            current_ship_to.Address.Name = txtCompanyName.Text
                            current_ship_to.Address.Line1 = txtAddress1.Text
                            current_ship_to.Address.Line2 = txtAddress2.Text
                            current_ship_to.Address.City = txtCity.Text
                            current_ship_to.Address.State = ddlState.SelectedValue
                            current_ship_to.Address.PostalCode = txtZip.Text
                            current_ship_to.Address.PhoneNumber = FilterString(txtPhoneShip.Text, "-")
                            current_ship_to.Address.PhoneExt = txtShipExtension.Text
                        End If
                    End If
                Else 'use the first available ship to for download only
                    current_ship_to_list = Session.Item("current_ship_to_list")
                    current_ship_to = CType(current_ship_to_list.GetValue(0), SISAccount)
                End If

                If tbCalifornia.Visible Then
                    current_ship_to.TaxExempt = rbCalifornia.SelectedItem.Text
                    If current_ship_to.TaxExempt = "Yes" Then
                        current_ship_to.TaxExempt = "9"
                    Else
                        current_ship_to.TaxExempt = ""
                    End If
                Else
                    current_ship_to.TaxExempt = ""
                End If

                'potentially pay be credit card
                If isCreditCardOrder Then
                    ' Parse the CC details, but don't pass them into current_bill_to
                    nYear = CType(ddlYear.SelectedValue, Integer)
                    nMonth = CType(ddlMonth.SelectedValue, Integer)
                    nDay = Date.DaysInMonth(nYear, nMonth)
                    exp_date = New Date(nYear, nMonth, nDay)
                    sCCNumber = CCNumber.Text
                    CCNo = CCNumber.Text
                    card_types = Session.Item("card_types")
                    If ddlMyCard.SelectedIndex > 0 Then
                        whichCCType = ddlMyCard.SelectedIndex - 1
                        For index = 0 To customer.CreditCards.Length - 1
                            If customer.CreditCards(index).NickName = ddlMyCard.SelectedValue And customer.CreditCards(index).HideCardFlag = False Then
                                card = customer.CreditCards(index)
                                iscreditCardAdd = False
                                'card.CSCCode = txtCVV.Text.Trim()
                            End If
                        Next
                    Else
                        whichCCType = ddlType.SelectedIndex - 1
                        credit_card_type = card_types.GetValue(whichCCType)

                        If (Session("token") IsNot Nothing) And (nickNameList.Count > 0) Then
                            If (nickNameList.Contains(Session("token").ToString())) Then
                                iscreditCardAdd = False
                            End If
                        End If
                    End If
                End If
                cart.PurchaseOrderNumber = txtPONumber.Text.ToString()

                If current_ship_to.SoldtoAccNumbyShipParty IsNot Nothing Then
                    current_bill_to.SoldToAccount = current_ship_to.SoldtoAccNumbyShipParty
                End If

                If isCreditCardOrder Then
                    order = orderMgr.SimulateOrder(customer, cart, current_bill_to, False, current_ship_to, ship_to_overriden, method, True, card, HttpContextManager.GlobalData)
                Else
                    order = orderMgr.SimulateOrder(customer, cart, current_bill_to, False, current_ship_to, ship_to_overriden, method, False, Nothing, HttpContextManager.GlobalData)
                End If

                ' TODO: Remove debug statements
                'Utilities.LogDebug("AccntHolder.btnNext_Click - SimulateOrder Complete. Line Item Count: " & If(order?.LineItems1?.Count.ToString(), "null"))

                order.Alt_Tax_Classification = String.Empty
                If tbCalifornia.Visible AndAlso rbCalifornia.SelectedValue = "0" Then
                    order.Alt_Tax_Classification = "9"
                    order.EnableDocumentIDNumber = customer.Enabledocumentidnumber
                End If

                If isCreditCardOrder Then
                    If order.BillTo Is Nothing Then
                        order.BillTo = New Sony.US.ServicesPLUS.Core.Address
                    End If
                    order.BillTo.Line1 = CCAddress1.Text.Trim()
                    order.BillTo.Line2 = CCAddress2.Text.Trim()
                    'order.BillTo.Line3 = CCAddress3.Text.Trim()
                    order.BillTo.Attn = TextboxAttn.Text.Trim()
                    order.BillTo.City = CCCity.Text.Trim()
                    order.BillTo.State = ddlCCState.SelectedValue
                    order.BillTo.PostalCode = CCZip.Text.Trim()
                    order.BillTo.Country = customer.CountryCode

                    If (iscreditCardAdd) Then
                        'Utilities.LogDebug("AccntHolder.btnNext - Adding CC - Raw Expiry = " & exp_date.ToString("yyyy-MM-dd"))
                        card = customer.AddCreditCard(Session("token").ToString(), exp_date, credit_card_type, "", CCName.Text.Trim())
                        'Utilities.LogDebug("AccntHolder.btnNext - Adding CC - Converted Expiry = " & card.ExpirationDate)
                        customer.CreditCards(0) = card
                        Dim objCustomerManager = New CustomerManager()
                        objCustomerManager.UpdateCustomerCreditCard(customer)
                    End If
                    order.CreditCard = card

                    If Session("token") IsNot Nothing Then
                        'Utilities.LogDebug("AccntHolder.btnNext_Click - Session('token') = " & Session("token").ToString())
                        nickNameList.Add(Session("token").ToString())
                    End If
                End If
                If ddlMyCard.SelectedIndex = 0 Then ' "Select One"
                    'objPCILogger.CreditCardSequenceNumber = card.SequenceNumber
                    objPCILogger.Message = "Nickname : " & TextBox6.Text
                    objPCILogger.IndicationSuccessFailure = "Success"
                    objPCILogger.PushLogToMSMQ()
                End If

                If order.MessageType.Equals("E") Then
                    If order.MessageText.Contains("Error in calling") Then
                        Dim objEx As New Exception(order.MessageText)
                        Throw objEx
                    ElseIf order.MessageText.Contains("Error in calling Z_SV_I1448_ORDER_INJECTION BAPI") Then
                        Dim objEx As New Exception("MessageText= 00: AppCode=SPARC2, Order Number=WJqgVcJIIgaC : Error in calling Z_SV_I1448_ORDER_INJECTION BAPI in  SelSparcTokenization.Inbound.Services.SalesOrder:receiveSalesOrderNotification Service for WJqgVcJIIgaC Order Number.")
                        Throw objEx
                    Else
                        ErrorLabel.Text = order.MessageText
                        ErrorLabel.Visible = True
                        Return
                    End If
                End If
                order.ShipToAccount = current_ship_to
                order.BillToAccount = current_bill_to
                Session.Add("order", order)
                Session.Remove("current_bill_to_list")
                Session.Remove("current_ship_to")
                '<Added By Rujuta on 14th Dec '09 for ending response to solve thread abort problem> 
                ddlMyCard.Items.Clear()
                Response.Redirect("TC.aspx", False)
                HttpContext.Current.ApplicationInstance.CompleteRequest()
            Catch exThrd As Threading.ThreadAbortException
                'Do Nothing
            Catch ex As Exception
                If (ex.Message.Contains("Please select unique nickname")) Then
                    ErrorValidation.Text = Resources.Resource.el_UniqueNickName   ' "Please select unique nickname"
                ElseIf ex.Message.Contains("Error in calling") Then
                    Dim sFeature As String = "scrollbars=1,resizable=0,width=744,height=754,left=0,top=0 "
                    Dim strUniqueKey As String = Utilities.GetUniqueKey
                    ErrorLabel.Text = Utilities.WrapExceptionforUINumber(ex, strUniqueKey)
                    Dim er As String
                    'er = ErrorLabel.Text.Replace(ErrorLabel.Text, "Unexpected error encountered. For assistance, please <a  style=""color:Blue"" href=""#"" onclick=""javascript:window.open('" + ConfigurationData.GeneralSettings.URL.contact_us_popup_link + "',null,'" + sFeature + "');""> contact us </a> and mention incident number")
                    er = ErrorLabel.Text.Replace(ErrorLabel.Text, Resources.Resource.el_Err_Unexp1 + " <a  style=""color:Blue"" href=""#"" onclick=""javascript:window.open('" + ConfigurationData.GeneralSettings.URL.contact_us_popup_link + "',null,'" + sFeature + "');""> " + Resources.Resource.el_Err_Unexp2 + "</a> " + Resources.Resource.el_Err_Unexp3)
                    ErrorLabel.Text = er + " " + strUniqueKey + "."
                    ErrorLabel.Visible = True
                ElseIf ex.Message.Contains("Error in calling Z_SV_I1448_ORDER_INJECTION BAPI") Then
                    Dim sFeature As String = "scrollbars=1,resizable=0,width=744,height=754,left=0,top=0 "
                    Dim strUniqueKey As String = Utilities.GetUniqueKey
                    ErrorLabel.Text = Utilities.WrapExceptionforUINumber(ex, strUniqueKey)
                    Dim er As String
                    er = ErrorLabel.Text.Replace(ErrorLabel.Text, Resources.Resource.el_Err_Unexp1 + " <a  style=""color:Blue"" href=""#"" onclick=""javascript:window.open('" + ConfigurationData.GeneralSettings.URL.contact_us_popup_link + "',null,'" + sFeature + "');""> " + Resources.Resource.el_Err_Unexp2 + "</a> " + Resources.Resource.el_Err_Unexp3)
                    ErrorLabel.Text = er + " " + strUniqueKey + "."
                    ErrorLabel.Visible = True
                Else
                    ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                    ErrorLabel.Visible = True
                End If
                objPCILogger.Message = ex.Message
                objPCILogger.PushLogToMSMQ()
                Return
            End Try
        End Sub

        Private Sub ddlBillTo_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As EventArgs) Handles ddlBillTo.SelectedIndexChanged
            displayOnBilltochange()
        End Sub

        Private Sub hideAllControl()
            tbHeader.Visible = False
            ShipTo.Visible = False
            ShipToDetails.Visible = False
            tablebuttons.Visible = False
        End Sub

        Private Sub displayOnBilltochange()
            btnReturnToCart.Visible = False
            'if back to 'select one'
            If ddlBillTo.SelectedValue = Resources.Resource.el_SelectOne Then '"Select One" Then
                btnReturnToCart.Visible = True
                ddlShipTo.Items.Clear()
                txtCompanyName.Text = ""
                txtAttnName.Text = ""
                txtAddress1.Text = ""
                txtAddress2.Text = ""
                txtCity.Text = ""
                'txtcity.Text = ""'7901
                txtZip.Text = ""
                ddlState.ClearSelection()
                ShipTo.Visible = False
                ShipToDetails.Visible = False
                tablebuttons.Visible = False
                Return
            Else
                ShipTo.Visible = True
            End If

            'set current_bill_to 
            current_bill_to_list = Session.Item("current_bill_to_list")

            Dim arrAccount As Account() = CType(current_bill_to_list, Account())
            For Each acc As Account In arrAccount
                If Not acc Is Nothing Then
                    If (acc.AccountNumber.Equals(ddlBillTo.SelectedValue)) Then
                        current_bill_to = CType(acc, Account)
                    End If
                End If
            Next

            'clear prior ship to options
            ddlShipTo.Items.Clear()
            txtCompanyName.Text = ""
            txtAttnName.Text = ""
            txtAddress1.Text = ""
            txtAddress2.Text = ""
            'txtcity.Text = ""'7901
            txtZip.Text = ""
            txtCity.Text = ""
            ddlState.ClearSelection()

            '***************Changes done for Bug# 1212 Starts here***************
            '**********Kannapiran S****************
            If Not current_bill_to Is Nothing Then
                For Each s As Account In current_bill_to.ShipToAccount
                    ddlShipTo.Items.Add(New ListItem(s.AccountNumber + "-" + s.Address.Name + " " + s.Address.Line1 + " " + s.Address.Line2, s.AccountNumber))
                Next
            End If

            If ddlShipTo.Items.Count > 1 Then
                ddlShipTo.Items.Insert(0, Resources.Resource.el_SelectOne) '"Select One")
                ShipTo.Visible = True
                ShipToDetails.Visible = False
            Else
                ShipTo.Visible = True
                ShipToDetails.Visible = True
                tablebuttons.Visible = True
                current_ship_to_list = current_bill_to.ShipToAccount
                Dim sis_account As Account = CType(current_ship_to_list.GetValue(0), Account)
                ParseAndDisplayShipTo(sis_account)
                Session.Add("current_ship_to", sis_account)

                'If Not String.IsNullOrEmpty(customer.Enabledocumentidnumber) And customer.StatusID = 1 Then'6668
                If Not String.IsNullOrEmpty(customer.Enabledocumentidnumber) And customer.UserType = "A" Then '6668
                    If ddlState.Text = "CA" Then
                        tbCalifornia.Visible = True
                    Else
                        tbCalifornia.Visible = False
                    End If
                Else
                    tbCalifornia.Visible = False
                End If
            End If

            Session.Add("current_ship_to_list", current_bill_to.ShipToAccount)
            Session.Add("current_bill_to", current_bill_to)


        End Sub

        Private Sub ddlShipTo_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As EventArgs) Handles ddlShipTo.SelectedIndexChanged
            Try
                If ddlShipTo.SelectedValue = Resources.Resource.el_SelectOne Then '"Select One" Then
                    txtCompanyName.Text = ""
                    txtAttnName.Text = ""
                    txtAddress1.Text = ""
                    txtAddress2.Text = ""
                    'txtcity.Text = ""'7901
                    txtZip.Text = ""
                    ddlState.ClearSelection()
                    chkDropShip.Visible = False
                    txtCity.Text = ""
                    txtPhoneShip.Text = ""
                    txtShipExtension.Text = ""
                    txtCompanyName.Text = ""
                    txtAttnName.Text = ""
                    ShipToDetails.Visible = False
                    tablebuttons.Visible = False
                Else
                    ShipToDetails.Visible = True
                    tablebuttons.Visible = True
                    current_ship_to_list = Session.Item("current_ship_to_list")
                    Dim sis_account As Account = CType(current_ship_to_list.GetValue(ddlShipTo.SelectedIndex() - 1), Account)
                    ParseAndDisplayShipTo(sis_account)
                    Session.Add("current_ship_to", sis_account)

                    'If Not String.IsNullOrEmpty(customer.Enabledocumentidnumber) And customer.StatusID = 1 Then'6668
                    If Not String.IsNullOrEmpty(customer.Enabledocumentidnumber) And customer.UserType = "A" Then '6668
                        If ddlState.Text = "CA" Then
                            tbCalifornia.Visible = True
                        Else
                            tbCalifornia.Visible = False
                        End If
                    Else
                        tbCalifornia.Visible = False
                    End If

                End If

                ' "Hide credit card options for CAN Customer - Pending account"

                'Check pending customer for CAN link
                'Dim globalData As New GlobalData
                'If Not HttpContext.Current.Session.Item("GlobalData") Is Nothing Then
                '    globalData = HttpContext.Current.Session.Item("GlobalData")
                'End If

                If HttpContextManager.GlobalData.IsCanada Then
                    If customer.UserType <> "A" Then
                        rbBillTo.Items(1).Attributes.Add("style", "visibility:hidden")
                    End If
                End If

            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Sub rbBillTo_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As EventArgs) Handles rbBillTo.SelectedIndexChanged
            If rbBillTo.SelectedIndex = 1 Then
                showCCDetails()
                BuildState()
            Else
                hideCCDetails()
            End If
        End Sub

        Private Sub showCCDetails()
            Me.divCCDetails.Visible = True
            Me.divFiller.Visible = False
        End Sub

        Private Sub hideCCDetails()
            Me.divCCDetails.Visible = False
            Me.divFiller.Visible = True
        End Sub

        ' 2018-07-18 ASleight - Seems this was phased out. These controls don't exist.
        'Private Sub rbSameBill_CheckedChanged(ByVal sender As System.Object, ByVal e As EventArgs) Handles rbSameBill.CheckedChanged
        '    If rbSameBill.Checked Then
        '        Dim SAPBillToAccount As Account = Session.Item("current_bill_to")
        '        NameBill.Text = SAPBillToAccount.Address.Name
        '        Line1Bill.Text = SAPBillToAccount.Address.Line1
        '        Line2Bill.Text = SAPBillToAccount.Address.Line2
        '        CityBill.Text = SAPBillToAccount.Address.City
        '        ddlStateBill.SelectedValue = SAPBillToAccount.Address.State
        '        ZipBill.Text = SAPBillToAccount.Address.PostalCode
        '    End If
        'End Sub

        Private Sub ddlMyCard_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As EventArgs) Handles ddlMyCard.SelectedIndexChanged
            Dim objPCILogger As New PCILogger()
            objPCILogger.IndicationSuccessFailure = "Failure"
            Try
                lblCCName.Visible = True
                CCName.Visible = True
                LabelLine5.CssClass = "redAsterick"

                If ddlMyCard.SelectedIndex > 0 Then
                    Dim orderMan As New OrderManager
                    Dim cartMan As New ShoppingCartManager
                    Dim payMan As New PaymentManager
                    Dim cardList() As CreditCard
                    Dim card As CreditCard = Nothing

                    objPCILogger.EventOriginApplication = "ServicesPLUS"
                    objPCILogger.EventOriginApplicationLocation = Me.Page.GetType().Name
                    objPCILogger.EventOriginMethod = "ddlMyCard_SelectedIndexChanged"
                    objPCILogger.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
                    objPCILogger.OperationalUser = Environment.UserName
                    objPCILogger.EventDateTime = DateTime.Now.ToLongTimeString()
                    objPCILogger.EventType = EventType.View
                    objPCILogger.HTTPRequestObjectValues = GetRequestContextValue()

                    If HttpContextManager.Customer Is Nothing Then
                        objPCILogger.Message = "ddlMyCard_SelectedIndexChanged - Unable to fetch Customer details"
                        objPCILogger.PushLogToMSMQ()
                        Response.Redirect("default.aspx", False)
                        HttpContext.Current.ApplicationInstance.CompleteRequest()
                    End If
                    customer = HttpContextManager.Customer
                    objPCILogger.CustomerID = customer.CustomerID
                    objPCILogger.CustomerID_SequenceNumber = customer.SequenceNumber
                    objPCILogger.EmailAddress = customer.EmailAddress
                    objPCILogger.SIAM_ID = customer.SIAMIdentity
                    objPCILogger.LDAP_ID = customer.LdapID

                    'Load cc's from customer
                    cardList = customer.CreditCards
                    CCNumber.Text = ""

                    Dim index As Integer
                    For index = 0 To cardList.Length - 1
                        If cardList(index).NickName = ddlMyCard.SelectedValue And cardList(index).HideCardFlag = False Then
                            card = cardList(index)
                        End If
                    Next
                    If card Is Nothing Then Throw New NullReferenceException("card variable was null.")

                    'Get credit card number
                    Dim popCardNumber As String
                    Dim ccLength As Int32
                    Dim LastFour As String
                    Dim str() As String

                    If card.CreditCardNumber.Length = 16 Then
                        CCNo = card.CreditCardNumber.ToString()
                        ccLength = card.CreditCardNumber.Length - 4
                        LastFour = card.CreditCardNumber.Substring(ccLength, 4)
                        popCardNumber = ("xxxxxxxxxxxx" & LastFour)
                        CCNumber.Text = popCardNumber
                    Else
                        str = card.CreditCardNumber.Split("-")
                        popCardNumber = ("xxxxxxxxxxxx" & str(2))
                        CCNumber.Text = popCardNumber
                    End If
                    objPCILogger.CreditCardMaskedNumber = card.CreditCardNumberMasked.ToString()
                    If card.SequenceNumber IsNot Nothing Then objPCILogger.CreditCardSequenceNumber = card.SequenceNumber


                    'Set Credit Card Exp Date DDL 
                    Dim expireDate As Date = Convert.ToDateTime(card.ExpirationDate, usCulture)
                    Dim Month As String = Format(expireDate, "MM")
                    If (Month.Length < 2) Then
                        Month = "0" & Month
                    End If
                    ddlMonth.SelectedValue = Month
                    'Add  Month to show in Textbox(ddl is hidden)
                    txtMonth.Text = ddlMonth.SelectedItem.Text

                    populateYearddl()
                    Dim Year As Integer = Format(expireDate, "yyyy")
                    Dim currentYear As Integer = Convert.ToInt32(ddlYear.Items(0).Value)
                    If Year < currentYear Then
                        Me.ErrorLabel.Text = Resources.Resource.el_CreditCard_Expire_Select '"This credit card has expired, please choose another"
                        objPCILogger.Message = "Credit card is expired"
                        objPCILogger.PushLogToMSMQ()
                        Return
                    End If
                    ddlYear.SelectedValue = Year
                    txtYear.Text = Year

                    ddlType.SelectedValue = card.Type.CreditCardTypeCode.ToString()
                    txtCCType.Text = ddlType.SelectedItem.Text
                    TextBox6.Text = card.NickName

                    'should be readonly if using an existing cc
                    CCNumber.Enabled = False
                    ddlMonth.Enabled = False
                    ddlYear.Enabled = False
                    ddlType.Enabled = False
                    TextBox6.Enabled = False
                    objPCILogger.Message = "Nick Name : " & card.NickName.ToString()
                    objPCILogger.IndicationSuccessFailure = "Success"
                    objPCILogger.PushLogToMSMQ()
                Else
                    CCNumber.Enabled = False
                    ddlMonth.Enabled = False
                    ddlYear.Enabled = False
                    ddlType.Enabled = False
                    TextBox6.Enabled = False

                    CCNumber.Text = ""
                    ddlMonth.SelectedIndex = 0
                    ddlYear.SelectedIndex = 0
                    ddlType.SelectedIndex = 0
                    TextBox6.Text = ""
                End If
                pnlSavedCard.Visible = True
                lblCCName.Visible = False
                CCName.Visible = False
                focusFormField = "btnNext"
                RegisterStartupScript("SetFocus", "<script language='javascript'>document.getElementById('" + focusFormField + "').focus()</script>")
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                ErrorLabel.Visible = True
                objPCILogger.Message = ex.Message.ToString()
                objPCILogger.PushLogToMSMQ()
            End Try
        End Sub

#End Region

#Region "   Methods     "
        Private Sub fnZipCodeConfig() ' 'Config Zipcode based on sales_Org 
            Dim intLength As Int32

            If HttpContextManager.GlobalData.IsAmerica Then
                lblZip.InnerText = Resources.Resource.el_rgn_ZipCode
                lblCCZip.InnerText = Resources.Resource.el_rgn_ZipCode
                lblState.InnerText = Resources.Resource.el_rgn_State
                lblCCState.InnerText = Resources.Resource.el_rgn_State
                intLength = Convert.ToInt32(ConfigurationData.GeneralSettings.GlobalData.ZipCode.US)
            Else
                lblZip.InnerText = "Postal Code"
                lblCCZip.InnerText = "Postal Code"
                lblState.InnerText = "Province"
                lblCCState.InnerText = "Province"
                intLength = Convert.ToInt32(ConfigurationData.GeneralSettings.GlobalData.ZipCode.CA)
            End If
            txtZip.Text = ""
            txtZip.MaxLength = intLength

            CCZip.Text = ""
            CCZip.MaxLength = intLength


            'Upper case - Zip code 
            txtZip.Attributes.Add("onblur", "this.value = this.value.toUpperCase()")
            CCZip.Attributes.Add("onblur", "this.value = this.value.toUpperCase()")

            'Dim strValidate As String = "\d{" + intLength.ToString() + "}(-\d{4})?$"
            'zipRegularExpressionValidator.ValidationExpression = strValidate
            'zipRegularExpressionValidator.ErrorMessage = "</br>Please enter " + intLength.ToString() + " digit zip code."
        End Sub

        Private Sub BuildDDls()
            Try
                '-- cc months
                ddlMonth.Items.Clear()
                ddlMonth.Items.Add(New ListItem("Month", ""))
                ddlMonth.Items.Add(New ListItem("January", "01"))
                ddlMonth.Items.Add(New ListItem("February", "02"))
                ddlMonth.Items.Add(New ListItem("March", "03"))
                ddlMonth.Items.Add(New ListItem("April", "04"))
                ddlMonth.Items.Add(New ListItem("May", "05"))
                ddlMonth.Items.Add(New ListItem("June", "06"))
                ddlMonth.Items.Add(New ListItem("July", "07"))
                ddlMonth.Items.Add(New ListItem("August", "08"))
                ddlMonth.Items.Add(New ListItem("September", "09"))
                ddlMonth.Items.Add(New ListItem("October", "10"))
                ddlMonth.Items.Add(New ListItem("November", "11"))
                ddlMonth.Items.Add(New ListItem("December", "12"))

                populateYearddl()

                Dim first_one As Boolean = True
                Dim sap_accounts As Boolean = False
                Dim arrAccount As Account()
                Dim thisSB As StringBuilder
                arrAccount = New Account(customer.SAPBillToAccounts.Length - 1) {}
                Dim iCount As Integer = 0

                Dim sAccountnumer As String = String.Empty

                If customer.SAPBillToAccounts.Length = 1 Then
                    Dim objLoopAccount As Account
                    'objLoopAccount = cm.PopulateCustomerDetailWithSAPAccount(customer.SAPBillToAccounts(0))'7980
                    objLoopAccount = cm.PopulateCustomerDetailWithSAPAccountBySoldtoAccount(customer.SAPBillToAccounts(0), HttpContextManager.GlobalData) 'sasikumar WP
                    sAccountnumer = sAccountnumer + " , " + objLoopAccount.AccountNumber
                    thisSB = New StringBuilder()
                    If objLoopAccount.AccountNumber IsNot Nothing Then thisSB.Append(objLoopAccount.AccountNumber.ToString() + "-")
                    If objLoopAccount.Address IsNot Nothing Then
                        If objLoopAccount.Address.Name IsNot Nothing Then thisSB.Append(" " + objLoopAccount.Address.Name)
                        If objLoopAccount.Address.Line1 IsNot Nothing Then thisSB.Append(" " + objLoopAccount.Address.Line1)
                        If objLoopAccount.Address.Line2 IsNot Nothing Then thisSB.Append(" " + objLoopAccount.Address.Line2)
                        'ddlBillTo.Items.Add(New ListItem(thisSB.ToString(), objLoopAccount.AccountNumber))'6809
                        '6809 starts
                        'If Not objLoopAccount.Validated = False Then 'commented for 6865
                        If customer.UserType = "A" Then
                            ddlBillTo.Items.Add(New ListItem(thisSB.ToString(), objLoopAccount.AccountNumber))
                        End If
                        '6809 ends
                    End If

                    If first_one Then
                        current_bill_to = objLoopAccount
                        first_one = False
                    End If
                    arrAccount(iCount) = objLoopAccount
                    iCount = iCount + 1

                    Dim arrAccount1 As Account() = CType(arrAccount, Account())

                    For Each acc As Account In arrAccount1
                        If Not acc Is Nothing Then
                            If (acc.AccountNumber.Equals(ddlBillTo.SelectedValue)) Then
                                current_bill_to = CType(acc, Account)
                            End If
                        End If
                    Next

                    If Not current_bill_to Is Nothing Then
                        For Each s As Account In current_bill_to.ShipToAccount
                            ddlShipTo.Items.Add(New ListItem(s.AccountNumber + "-" + s.Address.Name + " " + s.Address.Line1 + " " + s.Address.Line2, s.AccountNumber))
                        Next
                        If ddlShipTo.Items.Count > 1 Then
                            ddlShipTo.Items.Insert(0, Resources.Resource.el_SelectOne) '"Select One")
                            ShipTo.Visible = True
                            ShipToDetails.Visible = False
                        Else
                            ShipTo.Visible = True
                            ShipToDetails.Visible = True
                            tablebuttons.Visible = True
                            current_ship_to_list = current_bill_to.ShipToAccount
                            Dim sis_account As Account = CType(current_ship_to_list.GetValue(0), Account)
                            ParseAndDisplayShipTo(sis_account)
                            Session.Add("current_ship_to", sis_account)

                            'If Not String.IsNullOrEmpty(customer.Enabledocumentidnumber) And customer.StatusID = 1 Then'6668
                            If Not String.IsNullOrEmpty(customer.Enabledocumentidnumber) And customer.UserType = "A" Then '6668
                                If ddlState.Text = "CA" Then
                                    tbCalifornia.Visible = True
                                Else
                                    tbCalifornia.Visible = False
                                End If
                            Else
                                tbCalifornia.Visible = False
                            End If
                        End If

                    End If

                    Session.Add("current_ship_to_list", current_bill_to.ShipToAccount)
                    Session.Add("current_bill_to", current_bill_to)
                Else
                    For Each objLoopAccount As Account In customer.SAPBillToAccounts
                        objLoopAccount = cm.PopulateCustomerDetailWithSAPAccountBySoldtoAccount(objLoopAccount, HttpContextManager.GlobalData) ' Sasikumar WP
                        sAccountnumer = sAccountnumer + " , " + objLoopAccount.AccountNumber
                        If objLoopAccount IsNot Nothing Then
                            thisSB = New StringBuilder()
                            If objLoopAccount.AccountNumber IsNot Nothing Then thisSB.Append(objLoopAccount.AccountNumber.ToString() + "-")
                            If objLoopAccount.Address IsNot Nothing Then
                                If objLoopAccount.Address.Name IsNot Nothing Then thisSB.Append(" " + objLoopAccount.Address.Name.ToString())
                                If objLoopAccount.Address.Line1 IsNot Nothing Then thisSB.Append(" " + objLoopAccount.Address.Line1.ToString())
                                If objLoopAccount.Address.Line2 IsNot Nothing Then thisSB.Append(" " + objLoopAccount.Address.Line2.ToString())
                                If customer.UserType = "A" Then
                                    ddlBillTo.Items.Add(New ListItem(thisSB.ToString(), objLoopAccount.AccountNumber))
                                End If
                            End If

                            If first_one Then
                                current_bill_to = objLoopAccount
                                first_one = False
                            End If
                            arrAccount(iCount) = objLoopAccount
                            iCount = iCount + 1
                        End If
                    Next
                End If
                Dim hasAccount As Boolean = False
                For Each ac As Account In arrAccount
                    If ac IsNot Nothing Then
                        hasAccount = True
                        Exit For
                    End If
                Next
                Dim sFeature As String = "scrollbars=1,resizable=0,width=744,height=754,left=0,top=0"

                Dim sLogmessage = "Customer Lookup webMethod call for SAP Payer Account(s)" + sAccountnumer + "  did not return Bill To Address1 value."
                If Not hasAccount Then
                    Dim objEx As New ServicesPlusBusinessException("Information not available for account(s)" + sAccountnumer + "." _
                       + " Please  " + "<a href=""#"" onclick=""javascript:window.open('" + ConfigurationData.GeneralSettings.URL.contact_us_popup_link + "',null,'" + sFeature + "');"">" + "contact us" + "</a>" + " for assistance and mention incident number ")
                    objEx.errorMessage = sLogmessage
                    objEx.setcustomMessage = True
                    Throw objEx
                End If
                Session.Add("current_bill_to_list", arrAccount)
                If ddlBillTo.SelectedIndex = 0 And ddlBillTo.Items.Count > 1 Then
                    ddlBillTo.Items.Insert(0, Resources.Resource.el_SelectOne)
                    btnReturnToCart.Visible = True
                    ddlBillTo.Text = Resources.Resource.el_SelectOne ' "Select One"
                    ShipTo.Visible = False
                    ShipToDetails.Visible = False
                    tablebuttons.Visible = False
                Else
                    If ddlShipTo.Items.Count > 1 Then
                        ShipTo.Visible = True
                        ShipToDetails.Visible = False
                    Else
                        displayOnBilltochange()
                        ShipTo.Visible = True
                        ShipToDetails.Visible = True
                    End If
                End If

                'populate credit card types
                Dim card_types() As CreditCardType
                card_types = pm.GetCreditCardTypes()
                Session.Add("card_types", card_types)

                ddlType.Items.Add(New ListItem(" ", ""))

                For Each cc As CreditCardType In card_types
                    ddlType.Items.Add(New ListItem(cc.Description, cc.CreditCardTypeCode))
                Next

                'Load cc's from customer
                Dim cards() As CreditCard
                cards = customer.CreditCards
                ddlMyCard.Items.Add(New ListItem(Resources.Resource.el_SelectOne, ""))
                ''''''''''
                For Each c As CreditCard In cards
                    '--exclude expired cards
                    Dim ccYear As Integer
                    Dim ccMonth As Integer
                    Dim cardDate As DateTime
                    Try
                        cardDate = Convert.ToDateTime(c.ExpirationDate, usCulture)
                        ccYear = cardDate.Year
                        ccMonth = cardDate.Month
                    Catch ex As Exception
                        '-- make the card invalid
                        ccYear = DateTime.Today.Year - 1
                    End Try

                    Dim currentYear As Integer
                    Dim currentMonth As Integer
                    currentYear = DateTime.Today.Year
                    currentMonth = DateTime.Today.Month

                    '-- adding hidecardflag condition to hide those cards deleted by the customer
                    If Not String.IsNullOrWhiteSpace(c.NickName) And Not c.HideCardFlag And (ccYear > currentYear Or (ccYear = currentYear And ccMonth > currentMonth)) Then
                        ddlMyCard.Items.Add(New ListItem(c.NickName))
                    End If
                Next
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                hideAllControl()
            End Try
        End Sub
        Private Sub ParseAndDisplayShipTo(ByVal sis_account As Account)

            Try
                txtAddress1.Text = sis_account.Address.Line1
                txtAddress2.Text = sis_account.Address.Line2
                'txtcity.Text = sis_account.Address.Line3'7901
                txtCity.Text = sis_account.Address.City
                ddlState.SelectedValue = sis_account.Address.State.ToUpper()
                txtZip.Text = sis_account.Address.PostalCode
                customer = HttpContextManager.Customer
                Me.txtPhoneShip.Text = customer.PhoneNumber
                Me.txtShipExtension.Text = customer.PhoneExtension
                txtCompanyName.Text = customer.CompanyName
                Dim AttnName As String = customer.FirstName + " " + customer.LastName

                If AttnName.Length() > 30 Then
                    txtAttnName.Text = AttnName.Substring(0, 30)
                Else
                    txtAttnName.Text = AttnName
                End If

                If sis_account.AddressOverride <> SISAccount.AddressOverrideType.NotAllowed Then
                    chkDropShip.Visible = True
                Else
                    chkDropShip.Visible = False
                End If
                ShiptoDisplay()
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try

        End Sub


        Private Sub ValidateForm()

            Try
                ErrorValidation.Text = ""
                ErrorValidation.Visible = False

                lblAttnName.Attributes("class") = "tableData"
                lblAddress1.Attributes("class") = "tableData"
                lblAddress2.Attributes("class") = "tableData"
                lblCity.Attributes("class") = "tableData"
                lblState.Attributes("class") = "tableData"
                lblZip.Attributes("class") = "tableData"
                lblPhoneShip.Attributes("class") = "tableData"
                lblShipExtension.Attributes("class") = "tableData"
                lblPONumber.Attributes("class") = "tableData"

                lblCCName.Attributes("class") = "tableData"
                lblCCAttn.Attributes("class") = "tableData"
                lblCCAddress1.Attributes("class") = "tableData"
                lblCCAddress2.Attributes("class") = "tableData"
                lblCCCity.Attributes("class") = "tableData"
                lblCCState.Attributes("class") = "tableData"
                lblCCZip.Attributes("class") = "tableData"

                LabelOneCard.CssClass = "tableData"
                LabelCC.CssClass = "tableData"
                LabelCCNumber.CssClass = "tableData"
                LabelExpire.CssClass = "tableData"
                LabelNName.CssClass = "tableData"

                LabelCCAddress1Star.Text = ""
                LabelCCAddress2Star.Text = ""
                LabelCCCityStar.Text = ""
                LabelCCStateStar.Text = ""
                LabelCCZipStar.Text = ""

                LabelCompanyStar.Text = ""
                LabelLine1Star.Text = ""
                LabelLine2Star.Text = ""
                LabelLine4Star.Text = ""
                LabelZipStar.Text = ""
                LabelStateStar.Text = ""
                LabelPoNumberStar.Text = ""
                LabelShipPhoneStar.Text = ""
                LabelLineAttention.Text = ""
                LabelCalifornia.Text = ""

                Dim cart As ShoppingCart
                If Not Session.Item("carts") Is Nothing Then
                    cart = Session.Item("carts")
                Else
                    ErrorLabel.Text = Resources.Resource.el_EmptyCartInfo '"No part available in the current cart. Please add to cart again."
                    ErrorLabel.Visible = True
                    Return
                End If

                If tbCalifornia.Visible = True Then
                    If rbCalifornia.SelectedIndex <> 0 And rbCalifornia.SelectedIndex <> 1 Then
                        ErrorLabel.Visible = True
                        LabelCalifornia.CssClass = "redAsterick"
                        ' labelcalifornia.text = "<span class=""redasterick"">please select elgible for california section 6378 partial tax exemption</span>"
                    End If
                End If

                If cart.Type <> ShoppingCart.enumCartType.Download Then 'not download, need to check Ship To
                    If ddlShipTo.SelectedIndex = -1 Or ddlShipTo.SelectedValue = Resources.Resource.el_SelectOne Then '"Select One" Then
                        ErrorValidation.Visible = True
                        lblShipTo.Attributes("class") = "redAsterick"
                        Me.LabelShipToStar.Text = "<span class=""redAsterick"">*</span>"
                    End If
                    If txtCompanyName.Text = "" Then
                        ErrorValidation.Visible = True
                        lblCompany.Attributes("class") = "redAsterick"
                        LabelCompanyStar.Text = "<span class=""redAsterick"">*</span>"
                    End If

                    'If Name.Text = "" Then
                    '    ErrorValidation.Visible = True
                    '    LabelName.CssClass = "redAsterick"
                    '    LabelNameStar.Text = "<span class=""redAsterick"">*</span>"
                    'End If

                    If txtAddress1.Text = "" Then
                        ErrorValidation.Visible = True
                        lblAddress1.Attributes("class") = "redAsterick"
                        LabelLine1Star.Text = "<span class=""redAsterick"">*</span>"
                    Else
                        ' Dim poBoxPattern As New Regex("P\s*O\s*BOX|P.O.\s*", RegexOptions.IgnoreCase) 'PO Box or P.O. are not allowed
                        ' the validation should not affect addresses which has p followed by o as in Proctor
                        ' removed * for the issue - Deepa V , May 25, 2006
                        Dim poBoxPattern As New Regex("P\sO\sBOX|PO\sBOX|P\.O\s*|P\sO\.\s*|PO\.\sBOX", RegexOptions.IgnoreCase)
                        If poBoxPattern.IsMatch(txtAddress1.Text) Then
                            ErrorValidation.Visible = True
                            lblAddress1.Attributes("class") = "redAsterick"
                            LabelLine1Star.Text = "<span class=""redAsterick"">*</span>"
                            Me.ErrorValidation.Text = Resources.Resource.el_Validate_StreetAddress '"Orders can not be shipped to a PO Box. Please enter a street address."
                        End If
                    End If

                    If txtAddress2.Text.Length > 0 Then 'Line 2
                        'Dim poBoxPattern As New Regex("P.?O.?\s*BOX", RegexOptions.IgnoreCase)
                        ' the validation should not affect addresses which has p followed by o as in Proctor
                        ' removed * for the issue - Deepa V , May 25, 2006
                        Dim poBoxPattern As New Regex("P\sO\sBOX|PO\sBOX|P\.O\s*|P\sO\.\s*|PO\.\sBOX", RegexOptions.IgnoreCase)
                        If poBoxPattern.IsMatch(txtAddress2.Text) Then
                            ErrorValidation.Visible = True
                            Me.ErrorValidation.Text = Resources.Resource.el_Validate_ShipTo '"Ship to address can not be PO Box."
                        End If
                    End If

                    'If ddlState.SelectedIndex = 0 Then
                    '    ErrorValidation.Visible = True
                    '    lblState.CssClass = "redAsterick"
                    '    LabelStateStar.Text = "<span class=""redAsterick"">*</span>"
                    'End If

                    Dim sZip As String = txtZip.Text.Trim()
                    Dim objZipCodePattern As New Regex("\d{5}(-\d{4})?")
                    If HttpContextManager.GlobalData.IsCanada Then
                        objZipCodePattern = New Regex("^[ABCEGHJKLMNPRSTVXY]{1}\d{1}[A-Z]{1} *\d{1}[A-Z]{1}\d{1}$")
                    End If

                    If Not objZipCodePattern.IsMatch(sZip) Then
                        ErrorValidation.Visible = True
                        lblZip.Attributes("class") = "redAsterick"
                        LabelZipStar.Text = "<span class=""redAsterick"">*</span>"
                    End If
                    'Modified for fixing Bug# 439
                    Dim sPhoneNumber As String = Me.txtPhoneShip.Text.Trim()
                    If sPhoneNumber = "" Then
                        ErrorValidation.Visible = True
                        lblPhoneShip.Attributes("class") = "redAsterick"
                        LabelShipPhoneStar.Text = "<span class=""redAsterick"">*</span>"
                    Else
                        Dim objHPhoneNumber As New Regex("(\d{3}[-]{1}\d{3}[-]{1}\d{4})")
                        If Not objHPhoneNumber.IsMatch(sPhoneNumber) Then
                            ErrorValidation.Visible = True
                            lblPhoneShip.Attributes("class") = "redAsterick"
                            LabelShipPhoneStar.Text = "<span class=""redAsterick"">*</span>"
                            Me.ErrorValidation.Text = Resources.Resource.el_InvalidaPhoneNo '"Invalid Phone number. "
                        End If
                    End If

                    Dim sExtension As String = Me.txtShipExtension.Text.Trim.ToString()
                    If sExtension.Length > 0 Then
                        Dim objExt As New Regex("^[0-9]*$")
                        If Not objExt.IsMatch(sExtension) Or sExtension.Length > 6 Then
                            'CCErrorLabel.Text = "Extension must be numeric and 6 or fewer digits."
                        End If
                    End If
                End If

                If txtAttnName.Text.Length > 35 Then
                    ErrorValidation.Visible = True
                    LabelLineAttention.CssClass = "redAsterick"
                    LabelLineAttention.Text = "<span class=""redAsterick""> " + Resources.Resource.el_ValidLength_ShipTo + "</span>" 'Ship to Name must be 35 characters or less in length.</span>"
                End If

                If txtCompanyName.Text.Length > 35 Then
                    ErrorValidation.Visible = True
                    LabelCompanyStar.CssClass = "redAsterick"
                    LabelCompanyStar.Text = "<span class=""redAsterick""> " + Resources.Resource.el_ValidLength_ShipTo + "</span>" 'Ship to Name must be 35 characters or less in length.</span>"
                End If

                If txtAddress1.Text.Length > 35 Then
                    ErrorValidation.Visible = True
                    lblAddress1.Attributes("class") = "redAsterick"
                    LabelLine1Star.Text = "<span class=""redAsterick"">" + Resources.Resource.el_ValidLength_StreetAddress + "</span>" 'Street Address must be 35 characters or less in length.</span>"
                    txtAddress1.Focus()
                End If

                If txtAddress2.Text.Length > 35 Then
                    ErrorValidation.Visible = True
                    lblAddress1.Attributes("class") = "redAsterick"
                    LabelLine2Star.Text = "<span class=""redAsterick"">" + Resources.Resource.el_ValidLength_Address2 + "</span>" 'Address 2nd Line must be 35 characters or less in length.</span>"
                    txtAddress2.Focus()
                End If
                '7901 starts
                'If txtcity.Text.Length > 35 Then
                '    ErrorValidation.Visible = True
                '    lblAddress1.CssClass = "redAsterick"
                '    LabelLine3Star.Text = "<span class=""redAsterick"">Address 3rd Line must be 35 characters or less in length.</span>"
                '    txtcity.Focus()
                'End If
                '7901 ends
                If txtCity.Text.Length > 35 Then
                    ErrorValidation.Visible = True
                    lblCity.Attributes("class") = "redAsterick"
                    LabelLine4Star.Text = "<span class=""redAsterick"">" + Resources.Resource.el_ValidLength_City + "</span>" 'City must be 35 characters or less in length.</span>"
                    txtCity.Focus()
                End If

                'PO Number 
                If txtPONumber.Text.Trim() = "" Then
                    ErrorValidation.Visible = True
                    lblPONumber.Attributes("class") = "redAsterick"
                    LabelPoNumberStar.Text = "<span class=""redAsterick"">*</span>"
                ElseIf txtPONumber.Text.Length > 20 Then
                    ErrorValidation.Visible = True
                    'CCErrorLabel.Text = "PO Length must be less or equal to 20."
                End If

                'Check if pay by credit card radio button is checked. If it is, make credit card info required
                If rbBillTo.SelectedIndex = 1 Then
                    'Modified by Sunil for Bug 2597
                    'If CCName.Text.Trim() = "" Then
                    '    ErrorLabel.Visible = True
                    '    lblCCName.CssClass = "redAsterick"
                    'End If

                    If CCName.Text.Length > 35 Then
                        ErrorLabel.Visible = True
                        lblCCName.Attributes("class") = "redAsterick"
                        'lblCCNameStar.Text = "<span class=""redAsterick"">Credit Card Name must be 35 characters or less in length.</span>"
                    End If

                    If CCAddress1.Text.Trim() = "" Then
                        ErrorLabel.Visible = True
                        lblCCAddress1.Attributes("class") = "redAsterick"
                    End If

                    If CCAddress1.Text.Length > 35 Then
                        ErrorLabel.Visible = True
                        ErrorValidation.Visible = True
                        lblCCAddress1.Attributes("class") = "redAsterick"
                        LabelCCAddress1Star.Text = "<span class=""redAsterick"">" + Resources.Resource.el_CreditCard + Resources.Resource.el_ValidLength_StreetAddress + "</span>" 'Credit Card Street Address must be 35 characters or less in length.</span>"
                        CCAddress1.Focus()
                    End If

                    If CCAddress2.Text.Length > 35 Then
                        ErrorLabel.Visible = True
                        ErrorValidation.Visible = True
                        lblCCAddress2.Attributes("class") = "redAsterick"
                        LabelCCAddress2Star.Text = "<span class=""redAsterick"">" + Resources.Resource.el_CreditCard + Resources.Resource.el_ValidLength_Address2 + "</span>" 'Credit Card Address 2nd Line must be 35 characters or less in length.</span>"
                        CCAddress2.Focus()
                    End If

                    If CCCity.Text.Trim() = "" Then
                        ErrorLabel.Visible = True
                        ErrorValidation.Visible = True
                        lblCCCity.Attributes("class") = "redAsterick"
                    End If

                    If CCCity.Text.Length > 35 Then
                        ErrorLabel.Visible = True
                        ErrorValidation.Visible = True
                        lblCCCity.Attributes("class") = "redAsterick"
                        LabelCCCityStar.Text = "<span class=""redAsterick"">" + Resources.Resource.el_CreditCard + Resources.Resource.el_ValidLength_City + "</span>" 'Credit Card City must be 35 characters or less in length.</span>"
                        CCCity.Focus()
                    End If


                    If ddlCCState.SelectedIndex = 0 Then
                        ErrorValidation.Visible = True
                        lblCCState.Attributes("class") = "redAsterick"
                        'LabelCCStateStar.Text = "<span class=""redAsterick"">*</span>"
                    End If

                    If CCZip.Text.Trim() = "" Then
                        ErrorLabel.Visible = True
                        ErrorValidation.Visible = True
                        lblCCZip.Attributes("class") = "redAsterick"
                    End If

                    Dim sZip As String = CCZip.Text.Trim()
                    Dim objZipCodePattern As New Regex("\d{5}(-\d{4})?")
                    If HttpContextManager.GlobalData.IsCanada Then
                        objZipCodePattern = New Regex("^[ABCEGHJKLMNPRSTVXY]{1}\d{1}[A-Z]{1} *\d{1}[A-Z]{1}\d{1}$")
                    End If
                    If Not objZipCodePattern.IsMatch(sZip) Then
                        ErrorValidation.Visible = True
                        lblCCZip.Attributes("class") = "redAsterick"
                    End If
                    'Dim objZipCodePattern As New Regex("\d{5}(-\d{4})?")
                    'If Not objZipCodePattern.IsMatch(sZip) Then
                    '    ErrorValidation.Visible = True
                    '    LabelCCZip.CssClass = "redAsterick"
                    '    'LabelCCZipStar.Text = "<span class=""redAsterick"">*</span>"
                    'End If

                    'If CCNumber.Text.Trim() = "" Then
                    '    ErrorLabel.Visible = True
                    '    ErrorValidation.Visible = True
                    '    LabelCCNumber.CssClass = "redAsterick"
                    '    'LabelCCNumberStar.Text = "<span class=""redAsterick"">*</span>"
                    'End If

                    'If ddlMonth.SelectedIndex = 0 Then
                    '    ErrorValidation.Visible = True
                    '    LabelExpire.CssClass = "redAsterick"
                    '    'LabelExpStar.Text = "<span class=""redAsterick"">*</span>"
                    'End If

                    'If ddlYear.SelectedIndex = 0 Then
                    '    ErrorValidation.Visible = True
                    '    LabelExpire.CssClass = "redAsterick"
                    '    'LabelExpStar.Text = "<span class=""redAsterick"">*</span>"
                    'End If

                    'If Me.ddlMonth.SelectedIndex > 0 Then
                    '    Dim selectedMonth As Integer = Me.ddlMonth.SelectedValue
                    '    If selectedMonth < DateTime.Today().Month Then
                    '        If ddlYear.SelectedValue.ToString() = "0" Then
                    '            ErrorValidation.Visible = True
                    '            LabelExpire.CssClass = "redAsterick"
                    '            LabelExpStar.Text = "<span class=""redAsterick"">*</span>"
                    '        End If

                    '        If ddlYear.SelectedValue.ToString() <= DateTime.Today.Year.ToString() Then
                    '            ErrorValidation.Visible = True
                    '            LabelExpire.CssClass = "redAsterick"
                    '            LabelExpStar.Text = "<span class=""redAsterick"">*</span>"
                    '        End If
                    '    End If
                    'End If

                    'If ddlType.SelectedIndex = 0 Then
                    '    ErrorValidation.Visible = True
                    '    LabelCC.CssClass = "redAsterick"
                    '    'LabelCardStar.Text = "<span class=""redAsterick"">*</span>"
                    'End If

                    If ddlType.SelectedIndex > 0 Then
                        If CCNumber.Text = "" Then
                            ErrorLabel.Visible = True
                            ErrorValidation.Visible = True
                            LabelCCNumber.CssClass = "redAsterick"
                            'LabelCCNumberStar.Text = "*"
                            'CCSelectionError.CssClass = "redAsterick"
                        Else
                            Dim strCCErroMessage As String = String.Empty
                            Dim strCardNumber As String = String.Empty
                            If ddlMyCard.SelectedIndex > 0 Then
                                For index = 0 To customer.CreditCards.Length - 1
                                    If customer.CreditCards(index).NickName = ddlMyCard.SelectedValue And customer.CreditCards(index).HideCardFlag = False Then
                                        strCardNumber = customer.CreditCards(index).CreditCardNumber
                                    End If
                                Next
                            Else
                                strCardNumber = CCNumber.Text
                            End If
                            Dim blnCCValidaiton As Boolean = True ' Sony.US.SIAMUtilities.Validation.IsCreditCardValid(strCardNumber, ddlType.SelectedItem.Text.Substring(0, 1).ToUpper(), strCCErroMessage)
                            'lblCreditCardError.Text = IIf(blnCCValidaiton, "", strCCErroMessage)
                            If blnCCValidaiton = False Then
                                LabelCCNumber.CssClass = "redAsterick"
                                ErrorValidation.Visible = True
                                ErrorLabel.Visible = True
                            End If
                        End If
                    Else
                        'CCSelectionError.CssClass = "redAsterick"
                    End If

                    'If CCNumber.Text = "" Then
                    '    ErrorValidation.Visible = True
                    '    LabelCCNumber.CssClass = "redAsterick"
                    '    LabelCCNumberStar.Text = "<span class=""redAsterick"">*</span>"
                    'End If
                    '6984 starts
                    'IF Checked then Credit Card Nick Name must be completed

                    Dim isControlValid As Boolean = False
                    Dim thisPattern As String = "^[a-zA-Z0-9]*$"
                    Dim thispatternsplchar As String = "[!#-$ %*=+:;,?~@^&()_{}{}|\'<>?/]" '6984

                    If CCName.Text.Length > 0 Then

                        If CCName.Text.Length > 1 And CCName.Text.Length <= 17 Then
                            If ddlMyCard.Items.FindByText(CCName.Text.ToString()) Is Nothing Or CCName.Text.ToString() = ddlMyCard.SelectedValue.ToString() Then
                                'If Regex.IsMatch(TextBox6.Text, thisPattern) = True Then'6984
                                If Not Regex.IsMatch(CCName.Text, thisPattern) = True Then '6984
                                    isControlValid = True
                                    lblCCName.Visible = True
                                    CCName.Visible = True
                                End If
                                '6984 starts
                                If Regex.IsMatch(CCName.Text, thispatternsplchar) = True Then
                                    isControlValid = True
                                    lblCCName.Visible = True
                                    CCName.Visible = True
                                    'Me.ErrorLabel.Text = "Please enter Nickname without any spaces or special characters."
                                    Me.ErrorValidation.Text = Resources.Resource.el_req_NickName '"Please enter Nickname without any spaces or special characters."
                                    lblCCName.Attributes("class") = "redAsterick"
                                End If
                                '6984 ends
                            Else
                                isControlValid = True
                                'ErrorLabel.Text = "You must select a unique Credit Card Nickname." + vbCrLf
                                ErrorValidation.Text = Resources.Resource.el_UniqueCreditCard + vbCrLf '"You must select a unique Credit Card Nickname." + vbCrLf
                                lblCCName.Visible = True
                                CCName.Visible = True
                                lblCCName.Attributes("class") = "redAsterick"
                            End If
                        Else
                            isControlValid = True
                            'ErrorLabel.Text = "Nicknames must be between 1 and 17 characters." + vbCrLf
                            ErrorValidation.Text = Resources.Resource.el_ValidLength_NickName + vbCrLf '"Nicknames must be between 1 and 17 characters." + vbCrLf
                            lblCCName.Visible = True
                            CCName.Visible = True
                            lblCCName.Attributes("class") = "redAsterick"
                        End If

                        'If isControlValid = False Then'6984
                        If isControlValid = True Then '6984
                            ErrorValidation.Visible = True
                            lblCCName.Attributes("class") = "redAsterick"
                        End If
                    End If

                End If
                'If txtCVV.Text = "" Then
                '    ErrorValidation.Visible = True
                '    lblCVV.CssClass = "redAsterick"
                'End If
                '6984 ends


                'Shipping Methods            
                If Not ShipMethod.ValidateForm Then
                    ErrorValidation.Visible = True
                End If

            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub
#End Region

        Private Sub populateYearddl()
            Dim currentYear As Integer = DateTime.Today.Year
            ddlYear.Items.Clear()
            'If Me.ddlMonth.SelectedIndex > 0 Then
            '    Dim selectedMonth As Integer = Me.ddlMonth.SelectedValue
            '    If selectedMonth < currentDate.Month Then
            '        currentYear += 1
            '    End If
            '    Dim i As Integer
            '    For i = currentYear To currentYear + 20
            '        ddlYear.Items.Add(New ListItem(i.ToString(), i.ToString()))
            '    Next i
            'End If

            Dim i As Integer
            For i = currentYear To currentYear + 20
                ddlYear.Items.Add(New ListItem(i.ToString(), i.ToString()))
            Next i

            ddlYear.Items.Insert(0, New ListItem("Year", "0"))
            ddlYear.CssClass = "tableData"
            ddlYear.SelectedValue = "0"
        End Sub

        'Private Sub noPoRequiredCheckBox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles noPoRequiredCheckBox.CheckedChanged
        '    If noPoRequiredCheckBox.Checked = True Then
        '        Me.txtPONumber.Text = "NO-PO-REQ"
        '    Else
        '        Me.txtPONumber.Text = ""
        '    End If

        'End Sub

        Private Sub ddlMonth_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As EventArgs)
            populateYearddl()
        End Sub

        Public Function SelectCCNo(ByVal aCustomer As Integer) As String
            Dim creditNo As String
            Dim objAuditDataMgr As New AuditDataManager
            creditNo = objAuditDataMgr.SelectCC(aCustomer)
            Return creditNo
        End Function

        Private Sub FormFieldInitialization()
            frm4.Attributes("onkeydown") = "SetTheFocusButton(event, 'btnNext')"
            'txtAddress2.Attributes("onkeydown") = "SetTheFocusButton(event, 'btnNext')"
            'txtCity.Attributes("onkeydown") = "SetTheFocusButton(event, 'btnNext')"
            'TextBox3.Attributes("onkeydown") = "SetTheFocusButton(event, 'btnNext')"
            'txtPONumber.Attributes("onkeydown") = "SetTheFocusButton(event, 'btnNext')"
            'CompanyName.Attributes("onkeydown") = "SetTheFocusButton(event, 'btnNext')"
            'Name.Attributes("onkeydown") = "SetTheFocusButton(event, 'btnNext')"
            'txtAddress1.Attributes("onkeydown") = "SetTheFocusButton(event, 'btnNext')"
            'txtcity.Attributes("onkeydown") = "SetTheFocusButton(event, 'btnNext')"
            'txtPhoneShip.Attributes("onkeydown") = "SetTheFocusButton(event, 'btnNext')"
            'txtShipExtension.Attributes("onkeydown") = "SetTheFocusButton(event, 'btnNext')"
            'CCNumber.Attributes("onkeydown") = "SetTheFocusButton(event, 'btnNext')"
            'TextBox6.Attributes("onkeydown") = "SetTheFocusButton(event, 'btnNext')"
        End Sub

        'Added for PCI Compliance E736
        Public Function GetAuditCCNo(ByVal thisCCNo As String) As String
            Dim returnValue As String = String.Empty

            If thisCCNo <> String.Empty Then
                Dim ccNumberLength As Int16 = thisCCNo.Length
                Dim startValue As String = thisCCNo.Substring(0, 4)
                startValue = startValue.PadRight((ccNumberLength - 4), "X")
                Dim endValue As String = thisCCNo.Substring((ccNumberLength - 4), 4)
                returnValue = startValue + endValue
            End If
            Return returnValue
        End Function

        'Modified by Sunil for Bug 2597
        Private Sub BuildState()
            Dim cm As New CatalogManager
            Dim statedetails() As StatesDetail = cm.StatesBySalesOrg(HttpContextManager.GlobalData)
            'bind card type array to dropdown list control   

            Dim strMsg As String = Resources.Resource.el_rgn_Select
            Dim defaultItem As New ListItem(strMsg, "")
            defaultItem.Selected = True

            ddlCCState.Items.Clear()
            ddlCCState.DataSource = statedetails
            ddlCCState.DataTextField = "StateAbbr"
            ddlCCState.DataValueField = "StateAbbr"
            ddlCCState.DataBind()
            ddlCCState.Items.Insert(0, defaultItem)
        End Sub

        Protected Sub chkDropShip_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles chkDropShip.CheckedChanged
            If chkDropShip.Checked Then
                txtAddress1.ReadOnly = False
                txtAddress2.ReadOnly = False
                'txtcity.ReadOnly = False'7901
                txtCity.ReadOnly = False
                txtZip.ReadOnly = False
                ddlState.Enabled = True
                txtCompanyName.ReadOnly = False
                txtAttnName.ReadOnly = False
                txtPhoneShip.ReadOnly = False
                txtShipExtension.ReadOnly = False

                txtAddress1.Style.Add("color", "#2a3d47")
                txtAddress2.Style.Add("color", "#2a3d47")
                'txtcity.Style.Add("color", "#2a3d47")'7901
                txtCity.Style.Add("color", "#2a3d47")
                txtZip.Style.Add("color", "#2a3d47")
                ddlState.Style.Add("color", "#2a3d47")
                txtCompanyName.Style.Add("color", "#2a3d47")
                txtAttnName.Style.Add("color", "#2a3d47")
                txtPhoneShip.Style.Add("color", "#2a3d47")
                txtShipExtension.Style.Add("color", "#2a3d47")

                'If Not String.IsNullOrEmpty(customer.Enabledocumentidnumber) And customer.StatusID = 1 Then'6668
                If Not String.IsNullOrEmpty(customer.Enabledocumentidnumber) And customer.UserType = "A" Then '6668
                    If ddlState.Text = "CA" Then
                        tbCalifornia.Visible = True
                    Else
                        tbCalifornia.Visible = False
                    End If
                Else
                    tbCalifornia.Visible = False
                End If

            Else
                ShiptoDisplay()
            End If
        End Sub

        Private Sub ShiptoDisplay()
            txtAddress1.ReadOnly = True
            txtAddress2.ReadOnly = True
            'txtcity.ReadOnly = True'7901
            txtCity.ReadOnly = True
            txtZip.ReadOnly = True
            ddlState.Enabled = False
            txtCompanyName.ReadOnly = True
            txtAttnName.ReadOnly = True
            txtPhoneShip.ReadOnly = True
            txtShipExtension.ReadOnly = True

            txtAddress1.Style.Add("color", "Gray")
            txtAddress2.Style.Add("color", "Gray")
            'txtcity.Style.Add("color", "Gray")'7901
            txtCity.Style.Add("color", "Gray")
            txtZip.Style.Add("color", "Gray")
            ddlState.Style.Add("color", "Gray")
            txtCompanyName.Style.Add("color", "Gray")
            txtAttnName.Style.Add("color", "Gray")
            txtPhoneShip.Style.Add("color", "Gray")
            txtShipExtension.Style.Add("color", "Gray")
        End Sub

        Protected Sub ddlState_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlState.SelectedIndexChanged
            'If Not String.IsNullOrEmpty(customer.Enabledocumentidnumber) And customer.StatusID = 1 Then'6668
            If Not String.IsNullOrEmpty(customer.Enabledocumentidnumber) And customer.UserType = "A" Then '6668
                If ddlState.Text = "CA" Then
                    tbCalifornia.Visible = True
                Else
                    tbCalifornia.Visible = False
                End If
            Else
                tbCalifornia.Visible = False
            End If
        End Sub


        Protected Sub btnHidden_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnHidden.Click
            Dim objPCILogger As New PCILogger()
            pnlNickName.Visible = True
            pnlCCInfo.Visible = True
            TextBox6.Text = ""
            TextBox6.Enabled = True
            lblCCName.Visible = True
            CCName.Visible = True
            LabelLine5.CssClass = "redAsterick"
            Try
                objPCILogger.EventOriginApplication = "ServicesPLUS"
                objPCILogger.EventOriginApplicationLocation = Me.Page.GetType().Name
                objPCILogger.EventOriginMethod = "btnHidden_Click"
                objPCILogger.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
                objPCILogger.OperationalUser = Environment.UserName
                objPCILogger.CustomerID = customer.CustomerID
                objPCILogger.CustomerID_SequenceNumber = customer.SequenceNumber
                objPCILogger.EmailAddress = customer.EmailAddress
                objPCILogger.SIAM_ID = customer.SIAMIdentity
                objPCILogger.LDAP_ID = customer.LdapID
                objPCILogger.EventType = EventType.View
                objPCILogger.EventDateTime = DateTime.Now.ToLongTimeString()
                objPCILogger.HTTPRequestObjectValues = objPCILogger.GetRequestContextValue()
                objPCILogger.IndicationSuccessFailure = "Success"
                objPCILogger.Message = "btnHidden_Click Success."

                'Dim list0 As ListItem = New ListItem
                'list0.Value = Session("ExpirationYear").ToString()
                'list0.Text = "Year"
                Dim expYear = Session("ExpirationYear").ToString()
                ddlYear.CssClass = "tableData"
                ddlYear.SelectedValue = "20" + expYear
                txtYear.Text = "20" + expYear

                'Add Expiration Month
                Dim expMonth = Session("ExpirationMonth").ToString()
                If (expMonth.Length < 2) Then expMonth = "0" + expMonth
                ddlMonth.CssClass = "tableData"
                ddlMonth.SelectedValue = expMonth
                txtMonth.Text = ddlMonth.SelectedItem.Text

                'Add CreditCard Type
                Dim card_typesVen() As CreditCardType
                Dim pm As New PaymentManager
                Dim CCType As String
                card_typesVen = pm.GetCreditCardTypes()
                ddlType.Items.Add(New ListItem(Resources.Resource.el_SelectOne, ""))
                For Each cc As CreditCardType In card_typesVen
                    ddlType.Items.Add(New ListItem(cc.Description, cc.CreditCardTypeCode))
                    If cc.Description.ToUpper() = "AMERICAN EXPRESS" Then
                        CCType = "AMEX"
                    Else
                        CCType = cc.Description.ToUpper()
                    End If

                    If CCType = Session("CreditCardType").ToString().ToUpper() Then
                        Dim list3 As ListItem = New ListItem
                        list3.Text = cc.Description
                        list3.Value = cc.CreditCardTypeCode
                        ddlType.CssClass = "tableData"
                        ddlType.SelectedValue = list3.Value.ToString()
                        'Add type to show in Textbox(ddl is hidden)
                        txtCCType.Text = ddlType.SelectedItem.Text
                    End If
                Next

                Dim cardnumber() As String
                cardnumber = Session("token").ToString().Split("-")
                CCNumber.Text = "XXXXXXXXXXXX" & cardnumber(2).ToString()
            Catch ex As Exception
                objPCILogger.IndicationSuccessFailure = "Failure" '6524
                objPCILogger.Message = "btnHidden_Click Failed. " & ex.Message.ToString() '6524
                'Dim objUtilties As Utilities = New Utilities
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            Finally
                objPCILogger.PushLogToMSMQ() '6524
            End Try
        End Sub

        Protected Sub btnCCSaved_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnCCSaved.Click
            Session.Add("isSaveCardClicked", "true")
            lblCCName.Visible = False
            CCName.Visible = False
            pnlCCInfo.Visible = True
            pnlSavedCard.Visible = True
        End Sub

        Protected Sub btnNewCC_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnNewCC.Click
            Session.Remove("isSaveCardClicked")
        End Sub
    End Class

End Namespace
