Imports ServicesPlusException
Imports Sony.US.AuditLog
Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Data
Imports Sony.US.ServicesPLUS.Process

Namespace ServicePLUSWebApp
    Partial Class training_payment_naccnt
        Inherits SSL

        Public Auditcustomer As Customer 'Added by Narayanan July 31 for AuditTrail

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub



        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            isProtectedPage(True)
            InitializeComponent()

            'noPoRequiredCheckBox.Attributes("onclick") = "javascript: var thisControl = FIND('" + TextBoxPO.ID.ToString() + "'); if (thisControl != null) {thisControl.value = 'NO-PO-REQ';}"
        End Sub

#End Region

#Region "   Page Variables      "

        Public cm As New CustomerManager
        Private cr As CourseReservation
        Public siamID As String
        Public customer As Customer
        Private focusFormField As String = "TextBoxName"
        'store class type globally
        Public IsNonClass As Integer
        'Dim objUtilties As Utilities = New Utilities
        Dim flag As String
#End Region

#Region "   Events      "

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load




            Try
                If Not Session.Item("customer") Is Nothing Then customer = Session.Item("customer")

                If rbBillTo.SelectedIndex = 0 Then
                    'CCInfoTR.Style.Add("display", "none")
                    'CCSelectTR.Style.Add("display", "none")
                    'CCTypeTR.Style.Add("display", "none")
                    'CCNumberTR.Style.Add("display", "none")
                    'CCExpDateTR.Style.Add("display", "none")
                    'CCNickNameTR.Style.Add("display", "none")
                    ''CVVInfoTR.Style.Add("display", "none")
                Else
                    'CCInfoTR.Style.Add("display", "none")
                    'CCSelectTR.Style.Add("display", "none")
                    'CCTypeTR.Style.Add("display", "none")
                    'CCNumberTR.Style.Add("display", "none")
                    'CCExpDateTR.Style.Add("display", "none")
                    'CCNickNameTR.Style.Add("display", "none")
                    'CVVInfoTR.Style.Add("display", "none")
                End If

                formFieldInitialization()

                ' added by Deepa V Feb 17,2006
                ' to check for the querystring for non classroom
                Try
                    If Not Request.QueryString("nonclass") Is Nothing Then
                        If Convert.ToInt32(Request.QueryString("nonclass")) = 1 Then
                            IsNonClass = 1
                        Else
                            IsNonClass = 0
                        End If
                    Else
                        If Not Session.Item("IsNonClassRoom") Is Nothing Then
                            If Session.Item("IsNonClassRoom").ToString() = "1" Then
                                IsNonClass = 1
                            Else
                                IsNonClass = 0
                            End If
                        End If
                    End If
                Catch ex As Exception
                    IsNonClass = 0
                End Try

                If IsNonClass = 1 Then
                    Response.Write("<script language='javascript'>window.document.title='Sony Training Institute � Training Payment'</script>")
                    lblBudget.Visible = True
                    txtBudgetCode1.Visible = True
                    lblhyphen.Visible = True
                    txtBudgetCode2.Visible = True
                End If

                If Not customer Is Nothing Then
                    If Not Page.IsPostBack Then
                        cr = Session.Item("trainingreservation")
                        If cr Is Nothing Then
                            ErrorLabel.Text = ("No course reservation.")
                            ErrorLabel.Visible = True
                            Return
                        End If
                        LoadDDLS()
                        If IsNonClass = 1 Then
                            Me.rbBillTo.Items(1).Text = "Bill to my Sony Account below"
                            ' besides vacancy, the start and end date if not specified then it also represents a waiting list
                            'included start date condition in the display message as well - Deepa V, Apr 27,2006
                        ElseIf cr.CourseSchedule.Vacancy > 0 And cr.CourseSchedule.StartDateDisplay <> "TBD" Then
                            Me.rbBillTo.Items(1).Text = "Send me an Invoice to my Email Address"
                        Else
                            Me.rbBillTo.Items(1).Text = "Send me a Quote to my Email Address"
                        End If
                        TextBoxName.Text = customer.FirstName + " " + customer.LastName

                        'Bug 750
                        txtBillToCompanyName.Text = customer.CompanyName

                        'Prepop Bill To FieldsName
                        TextBoxLine1.Text = customer.Address.Line1
                        TextBoxLine2.Text = customer.Address.Line2
                        TextBoxLine3.Text = customer.Address.Line3
                        TextBoxCity.Text = customer.Address.City
                        Dim strState = customer.Address.State
                        If Not strState = "" Then DDLState.Items.FindByValue(strState).Selected = True

                        TextBoxZip.Text = customer.Address.PostalCode

                        focusFormField = "txtBillToCompanyName"
                        RegisterStartupScript("SetFocus", "<script language='javascript'>document.getElementById('" + focusFormField + "').focus()</script>")

                        Dim temp_cust As TempCustomer = New TempCustomer()
                        Dim isverified As Object = temp_cust.GetCustomerIsverified(customer.EmailAddress)
                        If isverified Is Nothing Then
                            ErrorLabel.Visible = True
                            ErrorLabel.Text = ""
                            ErrorLabel.Text = "Your email id not verified , Please update your profile to validate your emailid."
                            btnNext.Visible = False
                            btnCancel.Visible = False
                        ElseIf isverified.Equals("0") Or isverified.Equals("") Then
                            ErrorLabel.Visible = True
                            ErrorLabel.Text = ""
                            ErrorLabel.Text = "Your email id not verified , Please update your profile to validate your emailid."
                            btnNext.Visible = False
                            btnCancel.Visible = False
                        End If

                    End If
                End If
                ErrorLabel.Visible = True
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                ErrorLabel.Visible = True
            End Try
        End Sub


        Private Sub btnNext_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnNext.Click
            'Added by Sunil on 14-12-09 for [Bug 2097] New: PCI Event Log 
            Dim objPCILogger As New PCILogger()





            Try

                'Added by Sunil on 14-12-09 for [Bug 2097] New: PCI Event Log 
                objPCILogger.EventOriginApplication = "ServicesPLUS"
                objPCILogger.EventOriginApplicationLocation = Me.Page.GetType().Name
                objPCILogger.EventOriginMethod = "btnNext_Click"
                objPCILogger.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
                objPCILogger.OperationalUser = Environment.UserName
                'objPCILogger.UserName = customer.UserName
                objPCILogger.EmailAddress = customer.EmailAddress '6524
                objPCILogger.CustomerID = customer.CustomerID
                objPCILogger.CustomerID_SequenceNumber = customer.SequenceNumber
                objPCILogger.SIAM_ID = customer.SIAMIdentity
                objPCILogger.LDAP_ID = customer.LdapID '6524
                objPCILogger.EventDateTime = DateTime.Now.ToLongTimeString()
                objPCILogger.EventType = EventType.Add
                objPCILogger.HTTPRequestObjectValues = GetRequestContextValue()

                ValidateForm()

                If ErrorLabel.Visible Then
                    ErrorLabel.Text = "Please complete the required fields." + " " + ErrorLabel.Text
                Else
                    Dim tm As New CourseManager

                    '''''''''''''''''''''''''''''''Create Order''''''''''''''''''''''''''''''''
                    Dim card As CreditCard = Nothing

                    If rbBillTo.SelectedIndex = 0 Then
                        'Get and/or Add credit card type selected 
                        'Dim nYear As Integer
                        'Dim nMonth As Integer
                        'Dim nDay As Integer
                        'Dim sCCNumber As String

                        'nYear = CType(ddlYear.SelectedItem.Value, Integer)
                        'nMonth = CType(ddlMonth.SelectedItem.Value, Integer)

                        '' nDay = CType(, Integer)
                        'nDay = DateTime.Now.DaysInMonth(nYear, nMonth)
                        'sCCNumber = TextBoxCCNumber.Text
                        'Dim card_types() As CreditCardType

                        'card_types = Session.Item("card_types")

                        'Dim exp_date As New Date(nYear, nMonth, nDay)
                        'Dim credit_card_type As Sony.US.ServicesPLUS.Core.CreditCardType
                        'Dim whichCCType As Integer
                        'Dim index As Integer

                        'If DDLCCName.SelectedIndex > 0 Then
                        '    whichCCType = DDLCCName.SelectedIndex - 1
                        '    card = customer.CreditCards.GetValue(whichCCType)
                        '    For index = 0 To customer.CreditCards.Length - 1
                        '        If customer.CreditCards(index).NickName = DDLCCName.SelectedValue And customer.CreditCards(index).HideCardFlag = False Then
                        '            card = customer.CreditCards(index)
                        '            'card.CSCCode = txtCVV.Text.Trim()
                        '        End If
                        '    Next
                        'Else
                        '    whichCCType = DDLType.SelectedIndex - 1
                        '    credit_card_type = card_types.GetValue(whichCCType)
                        '    'card = customer.AddCreditCard(sCCNumber, exp_date, credit_card_type, txtCVV.Text.Trim(), TextBoxNName.Text)
                        '    card = customer.AddCreditCard(sCCNumber, exp_date, credit_card_type, "", TextBoxNName.Text)
                        'End If

                        'Added by Sunil on 14-12-09 for [Bug 2097] New: PCI Event Log 
                        'objPCILogger.CreditCardMaskedNumber = card.CreditCardNumberMasked
                        'objPCILogger.CreditCardSequenceNumber = card.SequenceNumber

                        'Save card if rb is checked 
                        'If rbSaveCard.Checked Then
                        'cm.UpdateCustomer(customer)
                        'End If

                    End If

                    'Bill To
                    Dim bill_to As New Sony.US.ServicesPLUS.Core.Address With {
                        .Name = txtBillToCompanyName.Text,
                        .Attn = TextBoxName.Text,
                        .Line1 = TextBoxLine1.Text,
                        .Line2 = TextBoxLine2.Text,
                        .Line3 = "",
                        .City = TextBoxCity.Text,
                        .State = DDLState.SelectedValue,
                        .PostalCode = TextBoxZip.Text
                    }

                    'Finally create order
                    Dim order As Sony.US.ServicesPLUS.Core.Order
                    Dim cr As CourseReservation = Session.Item("trainingreservation")

                    If (card Is Nothing) Then 'bill by invoice
                        order = tm.Checkout(customer, bill_to)
                        'Modified by Prasad for 45
                        cr.Sentinvoiceorquote = 1
                    Else
                        order = tm.Checkout(customer, card, bill_to)
                        cr.Sentinvoiceorquote = 0
                    End If
                    order.POReference() = Me.TextBoxPO.Text

                    Session.Add("trainingorder", order)

                    'Added by Sunil on 14-12-09 for [Bug 2097] New: PCI Event Log 
                    If rbBillTo.SelectedIndex = 0 Then
                        'If DDLCCName.SelectedIndex = 0 Then
                        '    If chkSaveCard.Checked Then
                        '        objPCILogger.Message = "Nick Name of the Newly Added Credit Card: " + TextBoxNName.Text.Trim()
                        '    Else
                        '        objPCILogger.Message = "Newly Added Credit Card Details Not Saved"
                        '    End If
                        'End If
                    End If

                    objPCILogger.IndicationSuccessFailure = "Success"
                    objPCILogger.PushLogToMSMQ()

                    'for non classroom training, the redirection should be done to terms & condition page
                    'added by Deepa V Feb 17,2006
                    If IsNonClass = 1 Then
                        Response.Redirect("training-tc.aspx?nonclass=1&budgetcode=" + Me.txtBudgetCode1.Text + "-" + Me.txtBudgetCode2.Text, False)
                    Else
                        Response.Redirect("training-student.aspx", False)
                    End If
                End If
                'Added by Sunil on 14-12-09 for [Bug 2097] New: PCI Event Log 
            Catch exThrd As Threading.ThreadAbortException
                'Do Nothing
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                ErrorLabel.Visible = True
                objPCILogger.IndicationSuccessFailure = "Failure"
                objPCILogger.Message = ex.Message
                objPCILogger.PushLogToMSMQ()
            End Try

        End Sub

        Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
            If Not (Session("mincatid") Is Nothing) Then
                Dim id As Int32 = Convert.ToInt32(Session("mincatid"))
                Response.Redirect(String.Format("sony-training-catalog-2.aspx?mincatid={0}", id), True)
            Else
                Response.Redirect("sony-training-search.aspx", True)
            End If
        End Sub

        'Public Sub DDLCCName_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DDLCCName.SelectedIndexChanged
        '    'Added by Sunil on 14-12-09 for [Bug 2097] New: PCI Event Log 
        '    Dim objPCILogger As New PCILogger()
        '    Try

        '        ErrorLabel.Text = ""

        '        If DDLCCName.SelectedIndex > 0 Then

        '            'Added by Sunil on 14-12-09 for [Bug 2097] New: PCI Event Log 
        '            objPCILogger.EventOriginApplication = "ServicesPLUS"
        '            objPCILogger.EventOriginApplicationLocation = Me.Page.GetType().Name
        '            objPCILogger.EventOriginMethod = "DDLCCName_SelectedIndexChanged"
        '            objPCILogger.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
        '            objPCILogger.OperationalUser = Environment.UserName
        '            objPCILogger.EventDateTime = DateTime.Now.ToLongTimeString()
        '            objPCILogger.EventType = EventType.View
        '            objPCILogger.HTTPRequestObjectValues = GetRequestContextValue()

        '            TextBoxCCNumber.Text = ""
        '            DDLType.SelectedIndex = 0

        '            'Dim om As New OrderManager
        '            'Dim pm As New PaymentManager

        '            If Not Session.Item("customer") Is Nothing Then
        '                customer = Session.Item("customer")
        '            End If

        '            If customer Is Nothing Then
        '                'Added by Sunil on 14-12-09 for [Bug 2097] New: PCI Event Log
        '                objPCILogger.Message = "Unable to fetch Customer details"
        '                objPCILogger.PushLogToMSMQ()
        '                Response.Redirect("default.aspx")
        '            End If

        '            'Added by Sunil on 14-12-09 for [Bug 2097] New: PCI Event Log
        '            objPCILogger.UserName = customer.UserName
        '            objPCILogger.CustomerID = customer.CustomerID
        '            objPCILogger.CustomerID_SequenceNumber = customer.SequenceNumber
        '            objPCILogger.SIAM_ID = customer.SIAMIdentity

        '            'Get chosen cc from customer
        '            Dim index As Int16

        '            'Get Card Number
        '            Dim cards() As CreditCard = customer.CreditCards
        '            Dim c As CreditCard

        '            For index = 0 To cards.Length - 1
        '                If cards(index).NickName = DDLCCName.SelectedValue And cards(index).HideCardFlag = False Then
        '                    c = cards(index)
        '                End If
        '            Next

        '            Dim popCardNumber As String
        '            Dim ccLength As Int32
        '            Dim LastFour As String
        '            ccLength = c.CreditCardNumber.Length - 4
        '            LastFour = c.CreditCardNumber.Substring(ccLength, 4)
        '            popCardNumber = ("xxxxxxxxxxxx" + LastFour)
        '            TextBoxCCNumber.Text = popCardNumber

        '            'Added by Sunil on 14-12-09 for [Bug 2097] New: PCI Event Log 
        '            objPCILogger.CreditCardMaskedNumber = c.CreditCardNumberMasked
        '            objPCILogger.CreditCardSequenceNumber = c.SequenceNumber
        '            objPCILogger.Message = "Nick Name of the Credit Card: " + c.NickName

        '            'Get ExpirationDate
        '            Dim expireDate As Date
        '            expireDate = c.ExpirationDate

        '            'Set Credit Card Exp Date DDL 
        '            Dim Month As Integer
        '            Dim Year As Integer
        '            Dim ParseDate() As String = CType(expireDate, String).Split("/")
        '            Month = ParseDate(0)
        '            Year = ParseDate(2).Substring(0, 4)

        '            ddlMonth.SelectedValue = Month

        '            populateYearddl()

        '            Dim tempYear As Integer
        '            tempYear = Convert.ToInt32(ddlYear.Items(0).Value)

        '            If Year < tempYear Then
        '                Me.ErrorLabel.Text = "This credit card has expired, please choose another"
        '                Return
        '            End If

        '            ddlYear.SelectedValue = Year


        '            'Set DDLType option
        '            DDLType.SelectedValue = c.Type.CreditCardTypeCode.ToString()

        '            'Reset form validation err msg
        '            'LabelCCNumberStar.Text = ""
        '            'LabelExpStar.Text = ""
        '            'LabelOneStar.Text = ""
        '            'LabelCardStar.Text = ""
        '            LabelOneCard.CssClass = "tableData"
        '            LabelCC.CssClass = "tableData"
        '            LabelCCNumber.CssClass = "tableData"
        '            LabelExpire.CssClass = "tableData"
        '            LabelOneCard.CssClass = "tableData"

        '            TextBoxNName.Text = c.NickName
        '            'rbSaveCard.Enabled = False
        '            chkSaveCard.Enabled = False

        '            'should be readonly if using an existing cc
        '            TextBoxCCNumber.Enabled = False
        '            ddlMonth.Enabled = False
        '            ddlYear.Enabled = False
        '            DDLType.Enabled = False
        '            TextBoxNName.Enabled = False

        '            'Added by Sunil on 14-12-09 for [Bug 2097] New: PCI Event Log 
        '            objPCILogger.IndicationSuccessFailure = "Success"
        '            objPCILogger.PushLogToMSMQ()

        '        Else
        '            TextBoxCCNumber.Enabled = True
        '            ddlMonth.Enabled = True
        '            ddlYear.Enabled = True
        '            DDLType.Enabled = True
        '            TextBoxNName.Enabled = True
        '            chkSaveCard.Enabled = True
        '            'rbSaveCard.Enabled = True
        '            TextBoxCCNumber.Text = ""
        '            ddlMonth.SelectedIndex = 0
        '            ddlYear.SelectedIndex = 0
        '            DDLType.SelectedIndex = 0
        '            TextBoxNName.Text = ""
        '            focusFormField = "TextBoxNName"

        '            RegisterStartupScript("SetFocus", "<script language='javascript'>document.getElementById('" + focusFormField + "').focus()</script>")
        '        End If
        '    Catch ex As Exception
        '        ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
        '        ErrorLabel.Visible = True
        '        objPCILogger.IndicationSuccessFailure = "Failure"
        '        objPCILogger.Message = ex.Message
        '        objPCILogger.PushLogToMSMQ()
        '    End Try
        'End Sub

#Region "   Methods     "


        Private Sub LoadDDLS()
            Try
                ErrorLabel.Text = ""

                DDLState.Items.Add(New ListItem("Select One", ""))
                DDLState.Items.Add(New ListItem("AL", "AL"))
                DDLState.Items.Add(New ListItem("AK", "AK"))
                DDLState.Items.Add(New ListItem("AR", "AR"))
                DDLState.Items.Add(New ListItem("AZ", "AZ"))
                DDLState.Items.Add(New ListItem("CA", "CA"))
                DDLState.Items.Add(New ListItem("CO", "CO"))
                DDLState.Items.Add(New ListItem("CT", "CT"))
                DDLState.Items.Add(New ListItem("DC", "DC"))
                DDLState.Items.Add(New ListItem("DE", "DE"))
                DDLState.Items.Add(New ListItem("FL", "FL"))
                DDLState.Items.Add(New ListItem("GA", "GA"))
                DDLState.Items.Add(New ListItem("HI", "HI"))
                DDLState.Items.Add(New ListItem("IA", "IA"))
                DDLState.Items.Add(New ListItem("ID", "ID"))
                DDLState.Items.Add(New ListItem("IL", "IL"))
                DDLState.Items.Add(New ListItem("IN", "IN"))
                DDLState.Items.Add(New ListItem("KS", "KS"))
                DDLState.Items.Add(New ListItem("KY", "KY"))
                DDLState.Items.Add(New ListItem("LA", "LA"))
                DDLState.Items.Add(New ListItem("MA", "MA"))
                DDLState.Items.Add(New ListItem("MD", "MD"))
                DDLState.Items.Add(New ListItem("ME", "ME"))
                DDLState.Items.Add(New ListItem("MI", "MI"))
                DDLState.Items.Add(New ListItem("MN", "MN"))
                DDLState.Items.Add(New ListItem("MO", "MO"))
                DDLState.Items.Add(New ListItem("MS", "MS"))
                DDLState.Items.Add(New ListItem("MT", "MT"))
                DDLState.Items.Add(New ListItem("NC", "NC"))
                DDLState.Items.Add(New ListItem("ND", "ND"))
                DDLState.Items.Add(New ListItem("NE", "NE"))
                DDLState.Items.Add(New ListItem("NH", "NH"))
                DDLState.Items.Add(New ListItem("NJ", "NJ"))
                DDLState.Items.Add(New ListItem("NM", "NM"))
                DDLState.Items.Add(New ListItem("NV", "NV"))
                DDLState.Items.Add(New ListItem("NY", "NY"))
                DDLState.Items.Add(New ListItem("OH", "OH"))
                DDLState.Items.Add(New ListItem("OK", "OK"))
                DDLState.Items.Add(New ListItem("OR", "OR"))
                DDLState.Items.Add(New ListItem("PA", "PA"))
                DDLState.Items.Add(New ListItem("RI", "RI"))
                DDLState.Items.Add(New ListItem("SC", "SC"))
                DDLState.Items.Add(New ListItem("SD", "SD"))
                DDLState.Items.Add(New ListItem("TN", "TN"))
                DDLState.Items.Add(New ListItem("TX", "TX"))
                DDLState.Items.Add(New ListItem("UT", "UT"))
                DDLState.Items.Add(New ListItem("VA", "VA"))
                DDLState.Items.Add(New ListItem("VT", "VT"))
                DDLState.Items.Add(New ListItem("WA", "WA"))
                DDLState.Items.Add(New ListItem("WI", "WI"))
                DDLState.Items.Add(New ListItem("WV", "WV"))
                DDLState.Items.Add(New ListItem("WY", "WY"))

                'Expire Date DDL's
                'ddlMonth.Items.Add(New ListItem("Month", ""))
                'ddlMonth.Items.Add(New ListItem("January", "1"))
                'ddlMonth.Items.Add(New ListItem("February", "2"))
                'ddlMonth.Items.Add(New ListItem("March", "3"))
                'ddlMonth.Items.Add(New ListItem("April", "4"))
                'ddlMonth.Items.Add(New ListItem("May", "5"))
                'ddlMonth.Items.Add(New ListItem("June", "6"))
                'ddlMonth.Items.Add(New ListItem("July", "7"))
                'ddlMonth.Items.Add(New ListItem("August", "8"))
                'ddlMonth.Items.Add(New ListItem("September", "9"))
                'ddlMonth.Items.Add(New ListItem("October", "10"))
                'ddlMonth.Items.Add(New ListItem("November", "11"))
                'ddlMonth.Items.Add(New ListItem("December", "12"))

                'populateYearddl()


                'Dim current_year As Integer = DateTime.Now.Year
                'Dim y As Integer
                'ddlYear.Items.Add(New ListItem("Year", ""))
                'For y = current_year To current_year + 10
                '    ddlYear.Items.Add(New ListItem(y.ToString, y.ToString))
                'Next

                'Dim om As New OrderManager
                'Dim pm As New PaymentManager


                ''Load Credit cards
                'Dim card_types() As CreditCardType
                'card_types = pm.GetCreditCardTypes()

                'Session.Add("card_types", card_types)

                'DDLType.Items.Add(New ListItem("Select One", ""))
                'For Each cc As CreditCardType In card_types
                '    DDLType.Items.Add(New ListItem(cc.Description, cc.CreditCardTypeCode))
                'Next

                ''Load cc's from customer
                'Dim cards() As CreditCard
                'cards = customer.CreditCards


                'DDLCCName.Items.Add(New ListItem("Select One", ""))
                'For Each c As CreditCard In cards
                '    'Get Last four digits
                '    'Dim popCardNumber As String
                '    'Dim ccLength As Int32
                '    'Dim LastFour As String
                '    'ccLength = c.CreditCardNumber.Length - 4
                '    ' LastFour = c.CreditCardNumber.Substring(ccLength, 4)
                '    If Not c.NickName = "" Then
                '        DDLCCName.Items.Add(New ListItem(c.NickName))
                '    End If
                'Next

                'For Each c As CreditCard In cards

                '    '--exclude expired cards
                '    Dim ccYear As Integer
                '    Dim ccMonth As Integer
                '    Dim cardDate As DateTime
                '    Try
                '        cardDate = Convert.ToDateTime(c.ExpirationDate)
                '        ccYear = cardDate.Year
                '        ccMonth = cardDate.Month
                '    Catch ex As Exception
                '        '-- make the card invalid
                '        ccYear = DateTime.Today.Year - 1
                '    End Try

                '    Dim currentYear As Integer
                '    Dim currentMonth As Integer
                '    currentYear = DateTime.Today.Year
                '    currentMonth = DateTime.Today.Month

                '    If c.NickName <> "" And c.HideCardFlag = False And (ccYear > currentYear Or (ccYear = currentYear And ccMonth > currentMonth)) Then
                '        DDLCCName.Items.Add(New ListItem(c.NickName))
                '    End If
                'Next


                ErrorLabel.Visible = True


                ErrorLabel.Visible = True
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                ErrorLabel.Visible = True
            End Try

        End Sub

        'Private Sub populateYearddl()
        '    Dim currentDate As Date = DateTime.Today()
        '    Dim currentYear As Integer
        '    currentYear = currentDate.Year()
        '    ddlYear.Items.Clear()

        '    'If Me.ddlMonth.SelectedIndex > 0 Then
        '    '    Dim selectedMonth As Integer = Me.ddlMonth.SelectedValue
        '    '    If selectedMonth < currentDate.Month Then
        '    '        currentYear += 1
        '    '    End If
        '    '    Dim i As Integer
        '    '    For i = currentYear To currentYear + 20
        '    '        ddlYear.Items.Add(New ListItem(i.ToString(), i.ToString()))
        '    '    Next i
        '    'End If


        '    Dim i As Integer
        '    For i = currentYear To currentYear + 20
        '        ddlYear.Items.Add(New ListItem(i.ToString(), i.ToString()))
        '    Next i

        '    Dim list0 As ListItem = New ListItem
        '    list0.Value = "0"
        '    list0.Text = "Year"
        '    ddlYear.Items.Insert(0, list0)
        '    ddlYear.CssClass = "tableData"
        '    ddlYear.SelectedValue = "0"
        'End Sub


        Public Sub ValidateForm()
            Try

                ErrorLabel.Text = ""
                ErrorLabel.Visible = False

                LabelName.CssClass = "tableData"
                LabelLine1.CssClass = "tableData"
                LabelLine2.CssClass = "tableData"
                LabelLine3.CssClass = "tableData"
                LabelCity.CssClass = "tableData"
                LabelState.CssClass = "tableData"
                LabelZip.CssClass = "tableData"
                PoNumberLabel.CssClass = "tableData"
                'LabelOneCard.CssClass = "tableData"
                'LabelCC.CssClass = "tableData"
                'LabelCCNumber.CssClass = "tableData"
                'LabelExpire.CssClass = "tableData"
                'LabelCC.CssClass = "tableData"
                'LabelCCNumber.CssClass = "tableData"
                'LabelNName.CssClass = "tableData"
                'lblCVV.CssClass = "tableData"
                lblCompanyName.CssClass = "tableData"

                'LabelOneStar.Text = ""
                'LabelCardStar.Text = ""
                LabelNameStar.Text = ""
                LabelLine1Star.Text = ""
                LabelLine2Star.Text = ""
                LabelLine3Star.Text = ""
                LabelCityStar.Text = ""
                LabelZipStar.Text = ""
                LabelStateStar.Text = ""

                PoNumberAsterick.Text = ""

                'LabelCCNumberStar.Text = ""
                'LabelExpStar.Text = ""
                'LabelNNameStar.Text = ""

                If TextBoxName.Text = "" Then
                    ErrorLabel.Visible = True
                    lblCompanyName.CssClass = "redAsterick"
                    lblBillToCompanyNameStar.Text = "<span class=""redAsterick"">*</span>"
                End If

                If TextBoxName.Text = "" Then
                    ErrorLabel.Visible = True
                    LabelName.CssClass = "redAsterick"
                    LabelNameStar.Text = "<span class=""redAsterick"">*</span>"
                End If

                If TextBoxLine1.Text = "" Then
                    ErrorLabel.Visible = True
                    LabelLine1.CssClass = "redAsterick"
                    LabelLine1Star.Text = "<span class=""redAsterick"">*</span>"
                End If
                If TextBoxLine1.Text.Length > 35 Then
                    ErrorLabel.Visible = True
                    LabelLine1.CssClass = "redAsterick"
                    LabelLine1Star.Text = "<span class=""redAsterick"">Street Address must be 35 characters or less in length.</span>"
                    TextBoxLine1.Focus()
                End If

                If TextBoxLine2.Text.Length > 35 Then
                    ErrorLabel.Visible = True
                    LabelLine2.CssClass = "redAsterick"
                    LabelLine2Star.Text = "<span class=""redAsterick"">Address 2nd Line must be 35 characters or less in length.</span>"
                    TextBoxLine2.Focus()
                Else
                    LabelLine2Star.Text = ""
                End If

                If TextBoxLine3.Text.Length > 35 Then
                    ErrorLabel.Visible = True
                    LabelLine3.CssClass = "redAsterick"
                    LabelLine3Star.Text = "<span class=""redAsterick"">Address 3rd Line must be 35 characters or less in length.</span>"
                    TextBoxLine3.Focus()
                Else
                    LabelLine3Star.Text = ""
                End If

                If TextBoxCity.Text = "" Then
                    ErrorLabel.Visible = True
                    LabelCity.CssClass = "redAsterick"
                    LabelCityStar.Text = "<span class=""redAsterick"">*</span>"
                ElseIf TextBoxCity.Text.Length > 35 Then
                    ErrorLabel.Visible = True
                    LabelCity.CssClass = "redAsterick"
                    LabelCityStar.Text = "<span class=""redAsterick"">City must be 35 characters or less in length.</span>"
                    TextBoxCity.Focus()
                End If

                If TextBoxPO.Text = "" Then
                    ErrorLabel.Visible = True
                    PoNumberLabel.CssClass = "redAsterick"
                    PoNumberAsterick.Text = "<span class=""redAsterick"">*</span>"
                ElseIf TextBoxPO.Text.Length > 20 Then
                    ErrorLabel.Visible = True
                    PoNumberLabel.CssClass = "redAsterick"
                    PoNumberAsterick.Text = "<span class=""redAsterick"">*</span>"
                    ErrorLabel.Text = "PO Length must be less or equal to 20."
                End If

                If DDLState.SelectedIndex = 0 Then
                    ErrorLabel.Visible = True
                    LabelState.CssClass = "redAsterick"
                    LabelStateStar.Text = "<span class=""redAsterick"">*</span>"
                End If

                Dim sZip As String = TextBoxZip.Text.Trim()
                Dim objZipCodePattern As New Regex("\d{5}(-\d{4})?")
                If Not objZipCodePattern.IsMatch(sZip) Then
                    ErrorLabel.Visible = True
                    LabelZip.CssClass = "redAsterick"
                    LabelZipStar.Text = "<span class=""redAsterick"">*</span>"
                End If

                If Me.rbBillTo.SelectedIndex = 0 Then
                    'If ddlMonth.SelectedIndex = 0 Then
                    '    'ErrorLabel.Visible = True
                    '    'LabelExpire.CssClass = "redAsterick"
                    '    'LabelExpStar.Text = "<span class=""redAsterick"">*</span>"
                    'End If

                    'If ddlYear.SelectedIndex = 0 Then
                    '    'ErrorLabel.Visible = True
                    '    'LabelExpire.CssClass = "redAsterick"
                    '    'LabelExpStar.Text = "<span class=""redAsterick"">*</span>"
                    'End If

                    'If Me.ddlMonth.SelectedIndex > 0 Then
                    '    Dim selectedMonth As Integer = Me.ddlMonth.SelectedValue
                    '    If selectedMonth < DateTime.Today().Month Then
                    '        If ddlYear.SelectedValue.ToString() = "0" Then
                    '            'ErrorLabel.Visible = True
                    '            'LabelExpire.CssClass = "redAsterick"
                    '            'LabelExpStar.Text = "<span class=""redAsterick"">*</span>"
                    '        End If

                    '        If ddlYear.SelectedValue.ToString() <= DateTime.Today.Year.ToString() Then
                    '            'ErrorLabel.Visible = True
                    '            'LabelExpire.CssClass = "redAsterick"
                    '            'LabelExpStar.Text = "<span class=""redAsterick"">*</span>"
                    '        End If
                    '    End If
                    'End If


                    'If DDLCCName.SelectedIndex = 0 And DDLType.SelectedIndex = 0 Then
                    '    'ErrorLabel.Visible = True
                    '    'LabelOneCard.CssClass = "redAsterick"
                    '    'LabelCC.CssClass = "redAsterick"
                    '    'LabelOneStar.Text = "<span class=""redAsterick"">*</span>"
                    '    'LabelCardStar.Text = "<span class=""redAsterick"">*</span>"
                    'End If











                    'If DDLType.SelectedIndex > 0 Then
                    '    If TextBoxCCNumber.Text = "" Then
                    '        'ErrorLabel.Visible = True
                    '        'LabelCCNumber.CssClass = "redAsterick"
                    '        'LabelCCNumberStar.Text = "*"
                    '    Else
                    '        Dim strCCErroMessage As String = String.Empty
                    '        Dim strCardNumber As String = String.Empty
                    '        If DDLCCName.SelectedIndex > 0 Then
                    '            For index = 0 To customer.CreditCards.Length - 1
                    '                If customer.CreditCards(index).NickName = DDLCCName.SelectedValue And customer.CreditCards(index).HideCardFlag = False Then
                    '                    strCardNumber = customer.CreditCards(index).CreditCardNumber
                    '                End If
                    '            Next
                    '        Else
                    '            strCardNumber = TextBoxCCNumber.Text
                    '        End If
                    '        Dim blnCCValidaiton As Boolean = True ' Sony.US.SIAMUtilities.Validation.IsCreditCardValid(strCardNumber, DDLType.SelectedItem.Text.Substring(0, 1).ToUpper(), strCCErroMessage)
                    '        'LabelCCNumberStar.Text = IIf(blnCCValidaiton, "*", strCCErroMessage)
                    '        If blnCCValidaiton = False Then
                    '            LabelCCNumber.CssClass = "redAsterick"
                    '            ErrorLabel.Visible = True
                    '        End If
                    '    End If
                    'End If


                    'If TextBoxCCNumber.Text = "" Then
                    '    ErrorLabel.Visible = True
                    '    LabelCCNumber.CssClass = "redAsterick"
                    '    LabelCCNumberStar.Text = "<span class=""redAsterick"">*</span>"
                    'End If

                    Dim cart As ShoppingCart
                    If Not Session.Item("carts") Is Nothing Then cart = Session.Item("carts")

                    'If chkSaveCard.Checked Then
                    '    Dim isControlValid As Boolean = False
                    '    Dim thisPattern As String = "^[a-zA-Z0-9]*$"

                    '    If TextBoxNName.Text.Length > 1 And TextBoxNName.Text.Length <= 17 Then
                    '        If DDLCCName.Items.FindByText(TextBoxNName.Text.ToString()) Is Nothing Or TextBoxNName.Text.ToString() = DDLCCName.SelectedValue.ToString() Then
                    '            If Regex.IsMatch(TextBoxNName.Text, thisPattern) = True Then
                    '                isControlValid = True
                    '            End If
                    '        Else
                    '            CCErrorLabel.Text = "You must select a unique Credit Card Nickname." + vbCrLf
                    '        End If
                    '    Else
                    '        CCErrorLabel.Text = "Nicknames must be between 1 and 17 characters." + vbCrLf
                    '    End If


                    '    If isControlValid = False Then
                    '        ErrorLabel.Visible = True
                    '        'LabelNName.CssClass = "redAsterick"
                    '        'LabelNNameStar.Text = "<span class=""redAsterick"">*</span>"
                    '    End If


                    'End If
                    'If txtCVV.Text = "" Then
                    '    ErrorLabel.Visible = True
                    '    lblCVV.CssClass = "redAsterick"
                    'End If
                    ' added by Deepa V 17 feb 2006
                    ' to check if the user has entered a value in the budget code field
                ElseIf Me.rbBillTo.SelectedIndex = 1 And IsNonClass = 1 Then
                    If Trim(txtBudgetCode1.Text) = "" Or Trim(txtBudgetCode2.Text) = "" Or Trim(txtBudgetCode1.Text).Length < 4 Or Trim(txtBudgetCode2.Text).Length < 7 Then
                        ErrorLabel.Visible = True
                        lblBudget.Visible = True
                        lblBudget.CssClass = "redAsterick"
                        lblBudget.Text += "<span class=""redAsterick"">*</span>"
                    End If
                    Try
                        If Convert.ToInt32(txtBudgetCode1.Text) = Nothing Or Convert.ToDouble(txtBudgetCode1.Text) = Nothing Then
                            ErrorLabel.Visible = True
                            lblBudget.Visible = True
                            lblBudget.CssClass = "redAsterick"
                            lblBudget.Text += "<span class=""redAsterick"">*</span>"
                        End If
                    Catch ex As Exception
                        ErrorLabel.Visible = True
                        lblBudget.Visible = True
                        lblBudget.CssClass = "redAsterick"
                        lblBudget.Text += "<span class=""redAsterick"">*</span>"
                    End Try
                End If

                'Added the Vulnerability Code by Arshad 
                If rbBillTo.SelectedIndex = 0 Then
                    customer = Session.Item("customer")
                    If Not customer.CreditCards Is Nothing And customer.CreditCards.Length > 0 Then
                        Dim currentYear As Integer
                        Dim currentMonth As Integer
                        currentYear = DateTime.Today.Year
                        currentMonth = DateTime.Today.Month

                        Dim ccYear As Integer
                        Dim ccMonth As Integer
                        Dim cardDate As DateTime
                        Dim Errormsg As String = ""
                        Dim anyOneValidCreditCard As Boolean
                        Dim CCnumber() As String
                        Dim strInvalidCardNumber As String = String.Empty
                        ' TODO: Get rid of this check.
                        For Each c As CreditCard In customer.CreditCards
                            Try
                                cardDate = Convert.ToDateTime(c.ExpirationDate)
                                ccYear = cardDate.Year
                                ccMonth = cardDate.Month
                            Catch ex As Exception
                                ccYear = DateTime.Today.Year - 1
                            End Try

                            If Not (c.HideCardFlag = True) Then
                                If Not ((ccYear > currentYear Or (ccYear = currentYear And ccMonth > currentMonth))) Then
                                    anyOneValidCreditCard = False
                                    CCnumber = c.CreditCardNumber.ToString().Split("-")
                                    strInvalidCardNumber = CCnumber(2)
                                Else
                                    anyOneValidCreditCard = True
                                End If
                            End If
                        Next
                        If Not anyOneValidCreditCard Then
                            ErrorLabel.Visible = True
                            ErrorLabel.Text = String.Empty
                            Errormsg += "<span class=""redAsterick""></br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + "XXXXXXXXXXXX" & strInvalidCardNumber + " Is not a valid card.</span>"
                            ErrorLabel.Text = Errormsg + "<br/>"
                        End If
                    Else
                        ErrorLabel.Visible = True
                        ErrorLabel.Text = String.Empty
                        ErrorLabel.Text = "<span class=""redAsterick""></br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Creditcard details not found, Please enter the credit card details first from MyProfile.</span><br/>"
                    End If
                End If

                'end of arshad code.


                ' Commented by Naveen for Incident INC00000393513
                'ErrorLabel.Visible = True
                'ErrorLabel.Visible = True
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                ErrorLabel.Visible = True
            End Try

        End Sub

        Private Sub formFieldInitialization()
            txtBillToCompanyName.Attributes("onkeydown") = "SetTheFocusButton(event, 'btnNext')"
            TextBoxName.Attributes("onkeydown") = "SetTheFocusButton(event, 'btnNext')"
            TextBoxLine1.Attributes("onkeydown") = "SetTheFocusButton(event, 'btnNext')"
            TextBoxLine2.Attributes("onkeydown") = "SetTheFocusButton(event, 'btnNext')"
            'TextBoxLine3.Attributes("onkeydown") = "SetTheFocusButton(event, 'btnNext')"
            TextBoxCity.Attributes("onkeydown") = "SetTheFocusButton(event, 'btnNext')"
            TextBoxZip.Attributes("onkeydown") = "SetTheFocusButton(event, 'btnNext')"
            TextBoxPO.Attributes("onkeydown") = "SetTheFocusButton(event, 'btnNext')"
            txtBudgetCode1.Attributes("onkeydown") = "SetTheFocusButton(event, 'btnNext')"
            txtBudgetCode2.Attributes("onkeydown") = "SetTheFocusButton(event, 'btnNext')"
            'TextBoxCCNumber.Attributes("onkeydown") = "SetTheFocusButton(event, 'btnNext')"
            'TextBoxNName.Attributes("onkeydown") = "SetTheFocusButton(event, 'btnNext')"
        End Sub

#End Region

#End Region

        Public Function getAuditCCNo(ByVal thisCCNo As String)
            Dim returnValue = String.Empty

            If thisCCNo <> String.Empty Then
                Dim ccNumberLength As Int16 = thisCCNo.Length
                Dim startValue As String = thisCCNo.Substring(0, 4)
                startValue = startValue.PadRight((ccNumberLength - 4), "X")
                Dim endValue As String = thisCCNo.Substring((ccNumberLength - 4), 4)
                returnValue = startValue + endValue
            End If

            Return returnValue
        End Function

        Public Function selectCCNo(ByVal aCustomer) As String
            Dim creditNo As String
            Dim objAuditDataMgr As New AuditDataManager
            creditNo = objAuditDataMgr.SelectCC(aCustomer)
            Return creditNo
        End Function

        Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
            'If (DDLCCName.SelectedIndex = 0) Then
            '    If (ErrorLabel.Text.Length > 0 Or CCErrorLabel.Text.Length > 0) Then
            '        'txtCVV.Text = ""
            '        TextBoxCCNumber.Text = ""
            '    End If
            'End If
        End Sub

        'Protected Sub TextBoxNName_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBoxNName.TextChanged
        '    If Not TextBoxNName.Text = "" Then

        '        Try

        '            'Add Expiration Year
        '            Dim list0 As ListItem = New ListItem
        '            list0.Value = Session("ExpirationYear").ToString()
        '            list0.Text = "Year"
        '            ddlYear.CssClass = "tableData"
        '            ddlYear.SelectedValue = "20" + list0.Value.ToString()

        '            'Add Expiration Month
        '            Dim list1 As ListItem = New ListItem
        '            list1.Value = Session("ExpirationMonth").ToString()
        '            list1.Text = MonthName(list1.Value)
        '            ddlMonth.CssClass = "tableData"
        '            ddlMonth.SelectedValue = list1.Value.ToString()

        '            'Add CreditCard Type

        '            Dim card_typesVen() As CreditCardType
        '            Dim pm As New PaymentManager

        '            card_typesVen = pm.GetCreditCardTypes()
        '            DDLType.Items.Add(New ListItem("Select One", ""))
        '            For Each cc As CreditCardType In card_typesVen
        '                DDLType.Items.Add(New ListItem(cc.Description, cc.CreditCardTypeCode))
        '                If cc.Description = Session("CreditCardType").ToString() Then
        '                    Dim list3 As ListItem = New ListItem
        '                    list3.Text = cc.Description
        '                    list3.Value = cc.CreditCardTypeCode
        '                    DDLType.CssClass = "tableData"
        '                    DDLType.SelectedValue = list3.Value.ToString()
        '                End If
        '            Next

        '            TextBoxCCNumber.Text = Session("token").ToString()

        '            chkSaveCard.Checked = True

        '        Catch ex As Exception
        '            'Dim objUtilties As Utilities = New Utilities
        '            ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
        '        Finally


        '        End Try
        '    Else
        '        'PnlCCInfo.Visible = False
        '    End If


        'End Sub

        'Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click

        'End Sub
    End Class

End Namespace
