<%@ Register TagPrefix="ServicePLUSWebApp" TagName="SonyFooter" Src="SonyFooter.ascx" %>

<%@ Page Language="vb" AutoEventWireup="false" SmartNavigation="True" Inherits="ServicePLUSWebApp.ProductRegistrationThankYou"
    CodeFile="ProductRegistrationThankYou.aspx.vb" CodeFileBaseClass="ServicePLUSWebApp.SSL" %>

<%@ Register TagPrefix="ServicePLUSWebApp" TagName="TopSonyHeader" Src="TopSonyHeader.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="ServicePlusNav" Src="ServicePlusNav.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="<%=PageLanguage %>">
<head>
    <title>
        <%=Resources.Resource.ttl_ProfesstionalProduct%>
    </title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link href="includes/ServicesPLUS_style.css" type="text/css" rel="stylesheet">
    <script language="javascript" src="includes/ServicesPLUS.js" type="text/javascript"></script>
    <script type="text/javascript" src="includes/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="includes/svcplus_globalization.js"></script>
    <script type="text/javascript">
        function PrintForm() {
            my_window = window.open('print-product-registration.aspx', '', 'toolbar=0,location=0,top=0,left=0,directories=0,status=0,menubar=0,scrollbars=yes,resize=1,width=800,height=700');
            if (my_window.opener === null) my_window.opener = self;
        }
    </script>
</head>
<body style="color: #000000; background: #5d7180; margin: 0px;">
    <form id="form1" method="post" runat="server">
        <center>
            <table width="710" border="0" align="center" role="presentation">
                <tr valign="top">
                    <td width="710" bgcolor="#ffffff">
                        <table width="710" border="0" role="presentation">
                            <tr>
                                <td>
                                    <ServicePLUSWebApp:TopSonyHeader ID="TopSonyHeader" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <nav><ServicePLUSWebApp:ServicePlusNav ID="SonyNav" runat="server" /></nav>
                                    <asp:Label ID="Label1" runat="server" CssClass="redAsterick" />
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 708px" valign="middle">
                                    <table width="710" border="0" role="presentation">
                                        <tr>
                                            <td valign="top" align="right" width="464" background="images/sp_int_header_top_PartsPLUS-ServiceAgreements.gif"
                                                bgcolor="#363d45" height="82">
                                                <br />
                                                <h1 class="headerText"><%=Resources.Resource.hplnk_svc_agreement%>  &nbsp;</h1>
                                            </td>
                                            <td valign="middle" bgcolor="#363d45">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 464px" bgcolor="#f2f5f8">
                                                <img src="images/spacer.gif" width="16" alt="">&nbsp;
                                            </td>
                                            <td style="width: 246px" bgcolor="#99a8b5">
                                            </td>
                                        </tr>
                                        <tr height="9">
                                            <td style="width: 464px" bgcolor="#f2f5f8">
                                                <img height="9" src="images/sp_int_header_btm_left_onepix.gif" width="464" alt="">
                                            </td>
                                            <td style="width: 246px" bgcolor="#99a8b5">
                                                <img style="height: 9px" height="9" src="images/sp_int_header_btm_right.gif" width="246" alt="">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="710" border="0" role="presentation">
                                        <tr>
                                            <td width="20">
                                                <img src="images/spacer.gif" width="20" height="20" alt="">
                                            </td>
                                            <td width="670" valign="top">
                                                <table width="670" border="0" role="presentation">
                                                    <tr>
                                                        <td>
                                                            &nbsp;<asp:Label ID="ErrorLabel" runat="server" Text="" CssClass="redAsterick" />
                                                        </td>
                                                    </tr>
                                                    <tr runat="server" id="trConfirm" visible="true">
                                                        <td class="tableheader">
                                                            <u><%=Resources.Resource.el_PlzConfirm%></u>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                    <tr runat="server" id="trThankYou" visible="false">
                                                        <td valign="top">
                                                            <table role="presentation">
                                                                <tr>
                                                                    <td>
                                                                        <p class="bodyCopy">
                                                                            <%=Resources.Resource.el_ProductReg_TQ_Cnt1%><span class="tableheader"> <%=Resources.Resource.el_ServiceContactNo%>  </span>
                                                                            <%=Resources.Resource.el_ProductReg_TQ_Cnt2%>
                                                                            <a class="tableheader" href="mailto:supportnet@am.sony.com">
                                                                                <%=Resources.Resource.el_SupportEmailId%>
                                                                            </a>.
                                                                        </p>
                                                                        <p class="bodyCopy">
                                                                            <%=Resources.Resource.el_ProductReg_TQ_Cnt3%>
                                                                            <span class="tableheader">
                                                                                <%=Resources.Resource.el_ServiceContactNo%>   </span>
                                                                            <%=Resources.Resource.el_ProductReg_TQ_Cnt4%>
                                                                            <br />
                                                                            <br />
                                                                            <%=Resources.Resource.el_ProductReg_TQ_Cnt4%>
                                                                            <br />
                                                                            <br />
                                                                            <%=Resources.Resource.el_ProductReg_TQ_Cnt5%>
                                                                            <br />
                                                                            <%=Resources.Resource.el_ProductReg_TQ_Cnt6%>
                                                                            <br />
                                                                            <%=Resources.Resource.el_ProductReg_TQ_Cnt7%>
                                                                        </p>
                                                                        <br />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center">
                                                            <div runat="server" id="bodyDiv">
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td width="20">
                                                <img src="images/spacer.gif" width="20" height="20" alt="">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="20">
                                                <img src="images/spacer.gif" width="20" height="20" alt="">
                                            </td>
                                            <td align="right" valign="top" height="20">
                                                <table width="100%" role="presentation">
                                                    <tr>
                                                        <td align="left" class="tableheader">
                                                            <asp:ImageButton ID="imgChangeAgreement" runat="server" ImageUrl="<%$Resources:Resource,sna_svc_img_8%>"
                                                                AlternateText="Change service agreement details." />&nbsp;&nbsp;
                                                        <asp:ImageButton ID="imgCalcelAgreement" runat="server" ImageUrl="<%$Resources:Resource,sna_svc_img_7%>"
                                                            AlternateText="Cancel service agreement registration." />
                                                        </td>
                                                        <td align="right">
                                                            <asp:ImageButton ID="imgSubmit" runat="server" ImageUrl="<%$Resources:Resource,img_btnSubmit%>"
                                                                AlternateText="Submit service agreement registration." /><asp:ImageButton Visible="false"
                                                                    ID="imgPrint" runat="server" ImageUrl="<%$Resources:Resource,sna_svc_img_5%>" AlternateText="Print Registration Detail"></asp:ImageButton>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="1">
                                    <footer><ServicePLUSWebApp:SonyFooter ID="SonyFooter1" runat="server" /></footer>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </center>
    </form>
</body>
</html>
