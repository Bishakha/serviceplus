<%@ Page Language="VB" AutoEventWireup="false" CodeFile="sony-service-print-form.aspx.vb" Inherits="ServicePLUSWebApp.sony_service_print_form" %>

<%@ Reference Page="~/SSL.aspx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="SonyFooter" Src="SonyFooter.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="TopSonyHeader" Src="TopSonyHeader.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="PersonalMessage" Src="PersonalMessage.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="ServicePlusNav" Src="ServicePlusNav.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="<%=PageLanguage %>">
<head>
    <title><%=Resources.Resource.header_serviceplus()%> - <%=Resources.Resource.repair_mn_hdr_msg()%></title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <meta content="Repair, Maintenance, Services, Sony Models, Sony Professional Models, Sony Broadcast Models, Sony Business Models, Sony Software, Sony Professional Software, Sony Broadcast Software, Sony Business Software"
        name="keywords">

    <link href="includes/ServicesPLUS_style.css" type="text/css" rel="stylesheet">
    <script language="javascript" src="includes/ServicesPLUS.js" type="text/javascript"></script>
    <script type="text/javascript" src="includes/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="includes/svcplus_globalization.js"></script>
    <script type="text/javascript">
        function PrintForm() {
            my_window = window.open('Printer-Service-Result.aspx', '', 'toolbar=0,location=0,directories=0,status=0,menubar=0,scrollbars=no,resize=1,width=100,height=100');
            if (my_window.opener === null) my_window.opener = self;
        }
    </script>
</head>
<body text="#000000" bgcolor="#5d7180" leftmargin="0" topmargin="0">
    <form id="Form1" method="post" runat="server">
        <center>
            <table width="710" border="0" role="presentation">
                <tr>
                    <td width="25" background="images/sp_left_bkgd.gif">
                        <img height="25" src="images/spacer.gif" width="25" alt=""></td>
                    <td width="689" bgcolor="#ffffff">
                        <table width="710" border="0" role="presentation">
                            <tr>
                                <td width="582">
                                    <ServicePLUSWebApp:TopSonyHeader ID="TopSonyHeader" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td width="582" height="23">
                                    <nav><ServicePLUSWebApp:ServicePlusNav ID="SonyNav" runat="server" /></nav>
                                </td>
                            </tr>
                            <tr>
                                <td width="582">
                                    <table width="710" border="0" role="presentation">
                                        <tr>
                                            <td align="right" background="images/repair_lrg.gif" bgcolor="#363d45"
                                                height="82" style="width: 465px">
                                                <br>
                                                <h1 class="headerText"><%=Resources.Resource.repair_mn_tg_msg_1 %></h1>
                                            </td>
                                            <td valign="top" width="238" bgcolor="#363d45">
                                                <ServicePLUSWebApp:PersonalMessage ID="PersonalMessageDisplay" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td bgcolor="#f2f5f8" style="width: 465px">
                                                <h2 id="lblSubHdr" class="headerTitle" style="padding-right: 20px; text-align: right;" runat="server"></h2>
                                            </td>
                                            <td width="238" bgcolor="#99a8b5">
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr height="9">
                                            <td bgcolor="#f2f5f8" style="width: 465px">
                                                <img height="9" src="images/sp_int_header_btm_left_onepix.gif" width="464" alt=""></td>
                                            <td width="238" bgcolor="#99a8b5">
                                                <img height="9" src="images/sp_int_header_btm_right.gif" width="246" alt=""></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <table width="100%" border="0" role="presentation">
                                        <tr>
                                            <td style="width: 5px; height: 22px;">
                                                <img height="20" src="images/spacer.gif" width="20" alt=""></td>
                                            <td style="width: 796px; height: 22px;">
                                                <p class="promoCopyBold">
                                                    <%=Resources.Resource.repair_prnt_prntfrm_msg()%>
                                                </p>
                                            </td>
                                            <td style="width: 436px; height: 22px;">
                                                <img height="20" src="images/spacer.gif" width="20" alt=""></td>
                                        </tr>
                                        <tr>
                                            <td style="width: 5px; height: 24px;">
                                                <img height="20" src="images/spacer.gif" width="20" alt=""></td>
                                            <td style="width: 796px; height: 24px;">&nbsp;</td>
                                            <td align="center" style="width: 436px; height: 24px;">
                                                &nbsp;&nbsp;
								                    <img height="20" src="images/spacer.gif" width="20" alt="">
                                                &nbsp;&nbsp;
                                            </td>
                                        </tr>
                                        <tr valign="top">
                                            <td style="width: 5px; height: 107px;">
                                                <img height="20" src="images/spacer.gif" width="20" alt=""></td>
                                            <td style="width: 606px; height: 107px;" colspan="2">
                                                <table cellpadding="3" border="0" style="width: 100%" role="presentation">
                                                    <tr valign="top">
                                                        <td style="height: 18px;" class="tableData" colspan="2">
                                                            <ul>
                                                                <li><strong><%=Resources.Resource.repair_prnt_prntfrm_msg_1()%></strong></li>
                                                                <li><strong><%=Resources.Resource.repair_prnt_prntfrm_msg_2()%></strong></li>
                                                                <li><strong><%=Resources.Resource.repair_prnt_prntfrm_msg_3()%></strong></li>
                                                            </ul>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2" class="tableData">
                                                            <span color="red"><b><%=Resources.Resource.repair_prnt_prntfrm_msg_4 ()%>
                                                                <%=Resources.Resource.repair_prnt_prntfrm_msg_5()%> $<%=mDepotServiceCharge%> <%=Resources.Resource.repair_prnt_prntfrm_msg_6()%>
                                                                <%=Resources.Resource.repair_prnt_prntfrm_msg_7()%></b></span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2" style="height: 34px">
                                                            <a href="javascript:PrintForm(); return false;">
                                                                <img src="<%=Resources.Resource.repair_svc_img_7()%>" border="0" alt="Click to Request Print Form"></a>
                                                            &nbsp;<asp:ImageButton ID="imgBtnNext" runat="server" ImageUrl="<%$Resources:Resource,repair_svc_img_4%>" />&nbsp;                                                        
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 5px">&nbsp;</td>
                                            <td colspan="2">&nbsp;</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>

                            <tr>

                                <td width="582">
                                    <footer><ServicePLUSWebApp:SonyFooter ID="SonyFooter1" runat="server" /></footer>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="25" background="images/sp_right_bkgd.gif">
                        <img height="20" src="images/spacer.gif" width="25" alt=""></td>
                </tr>
            </table>
        </center>
    </form>
</body>
</html>
