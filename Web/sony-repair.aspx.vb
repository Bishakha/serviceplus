Imports ServicesPlusException
Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Process
Imports Sony.US.SIAMUtilities

Namespace ServicePLUSWebApp

    Partial Class sony_repair
        Inherits SSL

        Public sModelName As String
        Public isEnable As String = "0"
        Private blnRegisterforLogin As Boolean = False
        'Dim objUtilties As Utilities = New Utilities
        Public LanguageSM As String = "EN"
        Public divrprShow As Boolean

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
            Try
                divrprShow = HttpContextManager.GlobalData.IsAmerica

                If Not String.IsNullOrEmpty(Request.QueryString("InvalidModelNo")) Then
                    lblErrMsgModel.Text = GetGlobalResourceObject("Resource", "repair_error_msg")
                    lblNoMatch.Text = GetGlobalResourceObject("Resource", "repair_error_msg_1")
                    lblErrMsgModel.Visible = True
                    lblNoMatch.Visible = True
                    lblErrMsgModel.Focus()
                End If

                Session.Remove("ServiceModelNumber")
                Session.Remove("isOneRow")
                If Not IsPostBack Then
                    Session("snServiceInfo") = Nothing 'Added for fixing Bug# 135
                    ' 2016-06-07 ASleight - Updating to use SSL.aspx.vb's Populate method.
                    'PopulateStatesDDL()
                    ddlStates.Items.Clear()
                    ddlStates.Items.Add(New ListItem(GetGlobalResourceObject("Resource", "repair_selstate_msg"), ""))
                    PopulateStateDropdownList(ddlStates, True)
                End If

                ' session variable to set the way to reach confirmation page
                Session.Add("RequestPath", "repair")

                ClientScript.RegisterHiddenField("__EVENTTARGET", imgDepotService.ClientID)
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                ErrorLabel.Focus()
            End Try
        End Sub

        ' 2016-06-07 ASleight - The following method is no longer used.
        <Obsolete("Replaced by base method SSL.populateStateDropdownList")>
        Private Sub PopulateStatesDDL()
            'populateStateDropdownList(ddlStates)
            'Dim objGlobalData As New GlobalData
            'If Not (Session.Item("GlobalData")) Is Nothing Then
            '    objGlobalData = (Session.Item("GlobalData"))
            'End If
            Dim cm As New CatalogManager
            Dim statesdtls() As StatesDetail
            Dim iState As StatesDetail

            Try
                'If (objGlobalData IsNot Nothing) Then
                statesdtls = cm.StatesBySalesOrg(HttpContextManager.GlobalData)
                'Else
                '	statesdtls = cm.GetStates()
                'End If
                Dim newListItem1 As New ListItem()
                newListItem1.Text = GetGlobalResourceObject("Resource", "repair_selstate_msg")
                newListItem1.Value = ""
                ddlStates.Items.Add(newListItem1)

                For Each iState In statesdtls
                    Dim newListItem As New ListItem()
                    newListItem.Text = ChangeCase(iState.StateName)
                    newListItem.Value = iState.StateAbbr
                    ddlStates.Items.Add(newListItem)
                Next
            Catch ex As Exception
                Throw New Exception(ex.Message)
            End Try
        End Sub

        Private Function ChangeCase(ByVal pString As String) As String
            Dim str As String = pString.ToLower()
            Return System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(str)
        End Function

        Protected Sub imgBtnGo_Click(ByVal sender As Object, ByVal e As ImageClickEventArgs) Handles imgBtnGo.Click
            Dim sState As String
            sState = ddlStates.SelectedValue.ToString()

            If Len(Trim(sState)) = 0 Then
                lblErrMsgState.Text = GetGlobalResourceObject("Resource", "repair_error_msg_2")
                lblErrMsgState.Visible = True
                lblErrMsgState.Focus()
            Else
                lblErrMsgState.Visible = False
                Session("ShowServiceLoc") = ddlStates.SelectedValue.ToString().Trim().ToUpper()
                Response.Redirect("sony-pg-service-maintenance-repair.aspx", True)
            End If
        End Sub

        Protected Sub imgDepotService_Click(ByVal sender As Object, ByVal e As ImageClickEventArgs) Handles imgDepotService.Click
            Dim modelsGroups As ModelDetails()
            Dim catMan As New CatalogManager
            Dim lstConflicts As List(Of String) = Nothing
            Try
                'Dim objGlobalData As New GlobalData
                'If Not (Session.Item("GlobalData")) Is Nothing Then
                '    objGlobalData = (Session.Item("GlobalData"))
                'End If

                If Len(Trim(txtModelNumber.Text)) = 0 Then
                    lblErrMsgModel.Text = GetGlobalResourceObject("Resource", "repair_error_msg_3")
                    lblErrMsgModel.Visible = True
                    lblErrMsgModel.Focus()
                    Return
                End If
                modelsGroups = catMan.GetSrhModelDetails(txtModelNumber.Text.Trim(), HttpContextManager.GlobalData, lstConflicts)

                'Commented by chandra to avoid popup to be blocked on login page
                '2016-05-10 ASleight Altering IF statement to trigger when more than one item is returned.
                ' It used to be >= 1, which unintentionally caused the ElseIf below to never trigger.
                If modelsGroups.Length > 1 Then
                    lblErrMsgModel.Text = ""
                    lblErrMsgModel.Visible = False
                    'Session.Add("isFromMainPg", "1")
                    'Server.Transfer("PgModelSearchResult.aspx?srh=" + Trim(txtModelNumber.Text))
                    callPopUp(lstConflicts, String.Format("sony-Pg-ModelSearch-Result.aspx?srh={0}", Trim(txtModelNumber.Text)))
                    '' Response.Redirect("sony-Pg-ModelSearch-Result.aspx?srh=" + Trim(txtModelNumber.Text))
                ElseIf modelsGroups.Length = 1 Then
                    If modelsGroups(0).DiscontinuedFlag <> "Y3" And modelsGroups(0).DiscontinuedFlag <> "Y4" And modelsGroups(0).DiscontinuedFlag <> "Y6" And modelsGroups(0).DiscontinuedFlag <> "Y7" Then
                        If Session.Item("customer") Is Nothing Then
                            blnRegisterforLogin = True
                        Else
                            Session("snServiceInfo") = Nothing
                            Session.Add("ServiceModelNumber", Trim(txtModelNumber.Text.ToUpper()))
                            callPopUp(lstConflicts, "sony-service-sn.aspx")
                            ''Response.Redirect("sony-service-sn.aspx")
                            'Response.Redirect("sony-service-sn.aspx?model=" + Trim(txtModelNumber.Text.ToUpper()))
                        End If
                    Else
                        callPopUp(lstConflicts, $"sony-pg-service-maintenance-repair.aspx?model={txtModelNumber.Text.Trim().ToUpper()}&frmRepair=True")
                    End If
                Else
                    lblErrMsgModel.Text = GetGlobalResourceObject("Resource", "repair_error_msg")
                    lblNoMatch.Text = GetGlobalResourceObject("Resource", "repair_error_msg_1")
                    lblErrMsgModel.Visible = True
                    lblNoMatch.Visible = True
                    lblErrMsgModel.Focus()
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                ErrorLabel.Focus()
            End Try
        End Sub

        Private Sub callPopUp(ByVal lstConflicts As List(Of String), ByVal url As String)
            Dim strModels As String = String.Empty
            If lstConflicts.Count > 0 Then
                For Each item In lstConflicts
                    strModels = strModels + item + ","
                Next
                If Not String.IsNullOrEmpty(strModels) Then strModels = strModels.Remove(strModels.Length - 1, 1)
                ScriptManager.RegisterStartupScript(Me, Me.GetType(),
                "showPopup", "javascript:OrderLookUp(" +
                String.Format("{0},'{1}','{2}'", lstConflicts.Count, strModels, url) + ");", True)
            Else
                'Fixed bug by Sneha on 6th Feb 2014 
                Response.Redirect(url)
            End If
        End Sub
        Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As EventArgs) Handles Me.PreRender

            'Pass language to siteminder 
            'If (Session("SelectedLang") IsNot Nothing) Then
            '    LanguageSM = Session("SelectedLang").ToString().Substring(0, 2).ToUpper()
            'End If
            'Dim globalData As New Core.GlobalData
            'If Not Session.Item("GlobalData") Is Nothing Then
            '    globalData = Session.Item("GlobalData")
            'End If
            If HttpContextManager.GlobalData.SalesOrganization = ConfigurationData.GeneralSettings.GlobalData.SalesOrganization.CA Then
                If HttpContextManager.GlobalData.Language = ConfigurationData.GeneralSettings.GlobalData.Language.US Then
                    LanguageSM = ConfigurationData.GeneralSettings.GlobalData.Language.CA_en.Replace("-", "_")
                Else
                    LanguageSM = ConfigurationData.GeneralSettings.GlobalData.Language.CA.Replace("-", "_")
                End If
            Else
                LanguageSM = ConfigurationData.GeneralSettings.GlobalData.Language.US.Replace("-", "_")
            End If
            If blnRegisterforLogin = False Then
                Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), "RuntimeLogin", "<script language=""javascript"">function ShowLogin(){}</script>")
            Else
                Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), "RuntimeLogin", "<script language=""javascript"">function ShowLogin(){window.location.href='SignIn.aspx?Lang=" + LanguageSM + "&post=sr&SPGSModel=" + Trim(txtModelNumber.Text.ToUpper()) + "';}</script>")
                'Response.Write("<script language=""javascript"">ShowLogin();</script>")
            End If
            txtModelNumber.Attributes.Add("onkeydown", "if (event.keyCode && event.keyCode === 13) {document.getElementById('" + imgDepotService.ClientID + "').click(); return false;}")
        End Sub


        Protected Sub imgCheckStatus_Click(ByVal sender As Object, ByVal e As ImageClickEventArgs) Handles imgCheckStatus.Click
            Context.Items.Add("Model", txtModelChkStatus.Text)
            Context.Items.Add("Serial", txtSerialChkStatus.Text)
            Context.Items.Add("Phone", txtPhoneChkStatus.Text)
            Context.Items.Add("Notification", txtNotificationChkStatus.Text)
            Server.Transfer("sony-repair-status.aspx")
        End Sub
    End Class
End Namespace
