<%@ Page Language="VB" AutoEventWireup="false" CodeFile="print-product-registration.aspx.vb" Inherits="ServicePLUSWebApp.PrintRegisteredProduct" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
	<HEAD>
        <title>ServicesPLUS - Service Product Registration</title>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<LINK href="includes/ServicesPLUS_style.css" type="text/css" rel="stylesheet">
	    <script language="javascript" src="includes/ServicesPLUS.js" type="text/javascript"></script>
	    <script type="text/javascript" src="includes/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="includes/svcplus_globalization.js"></script>
        <script language="javascript">
            function PrintPage()
            {     
                setTimeout('window.print ()',1000)       
                //window.print ();
                //this.close ();
                setTimeout('window.close()',2000)
            }
        </script>
</HEAD>
<body text="#000000" leftMargin="0" topMargin="0" marginheight="0" marginwidth="0">
    <form id="form1" runat="server">
    <div runat=server id=bodyDiv></div>
    
    <%-- <table cellpadding=3 cellspacing=1 border=0   class="bodycopy"
    style="border-right-style:solid; border-right-width:1px; border-left-style:solid; border-left-width:1px; border-top-style:solid; border-top-width:1px; border-bottom-style:solid; border-bottom-width:1px; width:93%">
       
       <tr valign=top>
            <td style="border-bottom-style:solid; border-bottom-width:thin;"><img src="https://www.servicesplus.sel.sony.com/images/sp_int_sonylogo.gif" /></td>
            <td  style="border-bottom-style:solid; border-bottom-width:thin;" align=right valign=middle>&nbsp;</td>
        </tr>
        <tr valign=top>
            <td colspan="2" style="border-bottom-style:solid; border-bottom-width:thin;">
                <table border="0" cellpadding="1" cellspacing="1" style="width: 100%">
                    <tr>
                        <td colspan="2" style="height: 16px" align=left>
                            <strong>ServicesPLUS - Product Registration
                            </strong>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 100px">
                        </td>
                        <td style="width: 100px">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        
        <tr valign=top >
            <td style="width: 173px;">Date:</td>
            <td style="width: 376px;"><%--<%=DateAndTime.Now.Month.ToString() + "/" + DateAndTime.Now.Day.ToString() + "/" + DateAndTime.Now.Year.ToString()%>--%>&nbsp;</td>
        <%--</tr>
        <tr valign=top bgColor="#d5dee9">
            <td style="width: 173px;">Customer Address:</td>
            <td style="width: 376px;">&nbsp;</td>
        </tr>
        <tr valign=top>
                <td style="width: 173px;">Service Address:</td>
                <td style="width: 376px;">&nbsp;</td>
        </tr>
            
             
        <tr valign=top bgColor="#d5dee9">
                <td  colspan=2 style="width: 173px" class="tableheader">Ptoduct List</td>
        </tr>
        <tr valign=top>
            <td style="width: 173px" colspan=2>
            <table width="97%" border="0" align=center>
			    <tr >
					    <td width="3" height="18"><img height="15" src="/images/spacer.gif" width="3"></td>
					    <td colSpan="1" height="18">
					        <asp:DataGrid ID="dgProduct" runat=server AutoGenerateColumns="False" CellPadding="4" ForeColor="#333333" GridLines="None" Width="100%" >
                                <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                <EditItemStyle BackColor="#2461BF" />
                                <SelectedItemStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" Font-Italic="False" Font-Overline="False" Font-Strikeout="False" Font-Underline="False" />
                                <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                <AlternatingItemStyle BackColor="White" Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False" Font-Underline="False" />
                                <ItemStyle BackColor="#EFF3FB" Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False" Font-Underline="False" />
                                <HeaderStyle BackColor="#D5DEE9" Font-Bold="True"  CssClass="tableHeader" />
                                <Columns>
                                    <asp:BoundColumn DataField="ProductCode" HeaderText="Product Code">
                                        <ItemStyle Font-Bold="False" CssClass="bodyCopy" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                            Font-Underline="False" HorizontalAlign="Left" VerticalAlign="Middle" />
                                        <HeaderStyle Width="20%" />
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="ModelNumber"  HeaderText="Model Number">
                                        <ItemStyle Font-Bold="False" CssClass="bodyCopy" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                            Font-Underline="False" HorizontalAlign="Left" VerticalAlign="Middle" />
                                        <HeaderStyle Width="20%" />
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="PurchaseDate" HeaderText="Purchase Date">
                                        <ItemStyle Font-Bold="False" CssClass="bodyCopy" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                            Font-Underline="False" HorizontalAlign="Left" VerticalAlign="Middle" />
                                        <HeaderStyle Width="20%" />
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="SerialNumber" HeaderText="Serial Number">
                                        <ItemStyle Font-Bold="False" CssClass="bodyCopy" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                            Font-Underline="False" HorizontalAlign="Left" VerticalAlign="Middle" />
                                        <HeaderStyle Width="25%" />
                                    </asp:BoundColumn>
                                </Columns>
						        
					        </asp:DataGrid>
					    </td>
				    </tr>
	            <tr>
		            <td width="3" height="18"><img height="15" src="/images/spacer.gif" width="3"></td>
                    <td width="3" colspan=1 height="18" ></td>
                </tr>
		    </table>
            </td>
        </tr>
        
    </table>--%>
    
    </form>
</body>
</html>
