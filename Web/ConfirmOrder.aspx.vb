Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Process
Imports Sony.US.ServicesPLUS.Data
Imports Sony.US.siam
Imports System.Text
Imports Sony.US.SIAMUtilities
Imports Sony.US.AuditLog
Imports ServicesPlusException


Namespace ServicePLUSWebApp

    Partial Class ConfirmOrder
        Inherits SSL

        Private Const conSoftwareDownloadPath As String = "\downloads\software"
        Private Const conFileDownloadPage As String = "\FileDownloadArea.aspx"

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub



        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            isProtectedPage(True)
            InitializeComponent()
        End Sub

#End Region
        Private Const currentPageIndex As String = "3"
        Private Const currentProcessIndex As String = "0"

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                If (Not Page.IsPostBack) Then
                    Response.CacheControl = "no-cache"
                    Response.AddHeader("Pragma", "no-cache")
                    Response.Expires = -1

                    If (Session.Item("CurrentPageIndex") IsNot Nothing) And (Session.Item("currentProcessIndex") IsNot Nothing) _
                         And (Convert.ToInt32(Session.Item("CurrentPageIndex").ToString()) < Convert.ToInt32(currentPageIndex.ToString())) _
                         And (Convert.ToInt32(Session.Item("currentProcessIndex").ToString()) = Convert.ToInt32(currentProcessIndex.ToString())) Then
                        Session.Item("CurrentPageIndex") = currentPageIndex.ToString()
                    Else
                        ' Go to error page, since back button was clicked
                        Response.Redirect("SessionExpired.aspx", False)
                        HttpContext.Current.ApplicationInstance.CompleteRequest()
                    End If
                End If

                If Not Page.IsPostBack Then
                    btnConfirmOrder.Style.Add("display", "inline")
                    Me.tdLegend.Visible = False
                    DisplayOrder()
                    Dim om As New OrderDataManager
                    Dim strURL As String = om.GetSpecialTaxURL("USA", "CA", "*", "Recycling")
                    specialTaxHyperLink.Attributes.Add("onclick", "javascript: window.open('" + strURL + "','','toolbar=1,location=1,directories=1,status=1,menubar=1,scrollbars=1,resize=1,width=900,height=720');")
                    specialTaxHyperLink.Text = strURL
                    specialTaxHyperLink.NavigateUrl = "#"
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                ErrorLabel.Visible = True
            End Try

        End Sub

        Private Sub ReturnToCartImageButton_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ReturnToCartImageButton.Click
            If Session.Item("order") IsNot Nothing And Session.Item("carts") IsNot Nothing Then
                Try
                    Dim order As Order = Session.Item("order")
                    Dim cart As Sony.US.ServicesPLUS.Core.ShoppingCart = Session.Item("carts")
                Catch ex As Exception
                    ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                End Try

                '-- clear CurrentPageIndex, currentProcessIndex - starting over again
                Session.Remove("CurrentPageIndex")
                Session.Remove("currentProcessIndex")

                Response.Redirect("vc.aspx", False)
                HttpContext.Current.ApplicationInstance.CompleteRequest()
            End If
        End Sub

        Private Sub btnConfirmOrder_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnConfirmOrder.Click
            Dim objPCILogger As New PCILogger()
            Dim customer = HttpContextManager.Customer
            Dim debugMessage As String = $"ConfirmOrder.aspx - btnConfirmOrder.Click - START at {DateTime.Now.ToShortTimeString}.{Environment.NewLine}"

            Try
                objPCILogger.EventOriginApplication = "ServicesPLUS"
                objPCILogger.EventOriginApplicationLocation = Me.Page.GetType().Name
                objPCILogger.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
                objPCILogger.OperationalUser = Environment.UserName
                objPCILogger.EventDateTime = DateTime.Now.ToLongTimeString()
                objPCILogger.HTTPRequestObjectValues = objPCILogger.GetRequestContextValue()
                objPCILogger.IndicationSuccessFailure = "Success"
                objPCILogger.EventOriginMethod = "btnConfirmOrder_Click"
                objPCILogger.Message = "btnConfirmOrder_Click Success."
                objPCILogger.EventType = EventType.Add
                objPCILogger.CustomerID = customer.CustomerID
                objPCILogger.CustomerID_SequenceNumber = customer.SequenceNumber
                objPCILogger.EmailAddress = customer.EmailAddress
                objPCILogger.SIAM_ID = customer.SIAMIdentity
                objPCILogger.LDAP_ID = customer.LdapID

                If btnConfirmOrder.CommandName = "Confirm" Then
                    If Session.Item("order") IsNot Nothing And Session.Item("carts") IsNot Nothing Then
                        debugMessage &= $"- Entered second If statement.{Environment.NewLine}"
                        Dim order As Order = Session.Item("order")
                        order.Customer = customer
                        Dim cart As Sony.US.ServicesPLUS.Core.ShoppingCart = Session.Item("carts")
                        Dim om As New OrderManager
                        Dim objHTTPRequestObject As New HTTPRequestObject()
                        objHTTPRequestObject = GetRequestContextValue()
                        order.HTTP_X_Forward_For = objHTTPRequestObject.HTTP_X_Forward_For
                        order.RemoteADDR = objHTTPRequestObject.RemoteADDR
                        order.HTTPReferer = objHTTPRequestObject.HTTPReferer
                        order.HTTPURL = objHTTPRequestObject.HTTPURL
                        order.HTTPUserAgent = objHTTPRequestObject.HTTPUserAgent

                        Try
                            om.CompleteOrder(order, cart, order.ShipToAccount.TaxExempt, HttpContextManager.GlobalData)
                            If order Is Nothing Then
                                debugMessage &= $"ConfirmOrder.aspx - CompleteOrder finished, but 'order' is null."
                            Else
                                debugMessage &= $"ConfirmOrder.aspx - CompleteOrder finished. Order number: {order.OrderNumber}, MessageType: {order.MessageType}, MessageText: {order.MessageText}"
                            End If
                            'Session.Item("order") = order   ' ASleight - Why was this never done? The "OrderSuccess" page won't show recent information without it.

                            '-- Credit Card declined
                            If order.MessageType = "E" And order.MessageText.Contains("109") And order.MessageText.Contains("105") Then
                                ErrorLabel.Text = Resources.Resource.el_CreditCardDeclined_Msg
                                Me.labelAction.Visible = False
                                btnConfirmOrder.ImageUrl = "images/sp_int_returnToCheckOut_btn.gif"
                                btnConfirmOrder.CommandName = "Return To Checkout"
                                btnConfirmOrder.ToolTip = Resources.Resource.el_ReturnToCheckout    ' "Return To Checkout"
                                btnConfirmOrder.Style.Add("display", "inline")
                                Return
                            End If

                            '-- Bill-To Account not available
                            If order.MessageType = "E" And order.MessageText.Contains("150") And order.MessageText.Contains("105") Then
                                If Session.Item("BilltoCount") IsNot Nothing Then
                                    Dim intBilltoCount As String = Int32.Parse(Session.Item("BilltoCount").ToString())
                                    If intBilltoCount > 1 Then
                                        Me.labelAction.Visible = False
                                        btnConfirmOrder.ImageUrl = "images/sp_int_returnToCheckOut_btn.gif"
                                        btnConfirmOrder.CommandName = "Return To Checkout"
                                        btnConfirmOrder.ToolTip = Resources.Resource.el_ReturnToCheckout     '"Return To Checkout"
                                        btnConfirmOrder.Style.Add("display", "inline")
                                        Dim objEx As New ServicesPlusBusinessException("The bill to account you selected is not available, please select another account")
                                        objEx.setcustomMessage = False
                                        Throw objEx
                                    Else
                                        btnConfirmOrder.ImageUrl = "images/sp_int_returnToCheckOut_btn.gif"
                                        btnConfirmOrder.CommandName = "Return To Checkout"
                                        btnConfirmOrder.ToolTip = Resources.Resource.el_ReturnToCheckout     '"Return To Checkout"
                                        btnConfirmOrder.Style.Add("display", "inline")
                                        Me.labelAction.Visible = False
                                        Dim objEx As New ServicesPlusBusinessException("The bill to account you selected is not available")
                                        objEx.setcustomMessage = False
                                        Throw objEx
                                    End If
                                End If
                            End If

                        Catch ex As Exception
                            btnConfirmOrder.ImageUrl = "images/sp_int_returnToCheckOut_btn.gif"
                            btnConfirmOrder.CommandName = "Return To Checkout"
                            btnConfirmOrder.ToolTip = Resources.Resource.el_ReturnToCheckout    ' "Return To Checkout"
                            btnConfirmOrder.Style.Add("display", "inline")
                            If order.MessageType = "E" And order.MessageText.Contains("109") And order.MessageText.Contains("105") Then
                                ErrorLabel.Text = Resources.Resource.el_CreditCardDeclined_Msg      ' "Your credit card has been declined... click Return to Checkout..."
                                Me.labelAction.Visible = False
                                Return
                            End If
                            If ex.Message = "FRAUDORDER" Or String.IsNullOrEmpty(order.OrderNumber) Then
                                Me.labelAction.Visible = False
                                btnConfirmOrder.Visible = False
                                ReturnToCartImageButton.Visible = False
                            End If
                            ' ASleight 2018-03-02 Added, otherwise the code ignores errors that aren't "if"ed above.
                            ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                            Return
                        End Try

                        removeAllCartsSessionVariable()
                        Session.Remove("carts")
                        Session.Remove("CurrentPageIndex")
                        Session.Remove("currentProcessIndex")

                        Response.Redirect("OrderSuccess.aspx", False)
                        HttpContext.Current.ApplicationInstance.CompleteRequest()
                    Else
                        ErrorLabel.Text = Resources.Resource.el_Err_ProceeOrder                     ' "Error processing order."
                        btnConfirmOrder.ImageUrl = "images/sp_int_returnToCheckOut_btn.gif"
                        btnConfirmOrder.CommandName = "Return To Checkout"
                        btnConfirmOrder.ToolTip = Resources.Resource.el_ReturnToCheckout            ' "Return To Checkout"
                        btnConfirmOrder.Style.Add("display", "inline")
                        Me.labelAction.Visible = False
                    End If
                Else
                    Dim cc_only_purchase = True
                    If customer.UserType = "A" Then
                        If (customer.SAPBillToAccounts.Length > 0) Or (customer.SISLegacyBillToAccounts.Length > 0) Then
                            cc_only_purchase = False
                        End If
                    End If

                    Session.Item("CurrentPageIndex") = 0
                    Session.Item("currentProcessIndex") = 0

                    'If customer.UserType = "A" Then
                    If cc_only_purchase = False And customer.IsActive Then
                        Response.Redirect("AccntHolder.aspx", False)
                        HttpContext.Current.ApplicationInstance.CompleteRequest()
                    Else
                        Response.Redirect("NAccntHolder.aspx", False)
                        HttpContext.Current.ApplicationInstance.CompleteRequest()
                    End If
                End If
            Catch exThrd As Threading.ThreadAbortException
                ' Thread exceptions happen when Response.Redirect ends abruptly. Somewhat expected.
            Catch ex As Exception
                Utilities.LogMessages(debugMessage)
                objPCILogger.IndicationSuccessFailure = "Failure" '6524
                objPCILogger.Message = "btnConfirmOrder_Click Failed. " & ex.Message.ToString() '6524

                If ex.Message.Contains("was placed on hold") Then
                    '-- on hold account error 
                    ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                    btnConfirmOrder.ImageUrl = "images/sp_int_returnToCheckOut_btn.gif"
                    btnConfirmOrder.CommandName = "Return To Checkout"
                    btnConfirmOrder.ToolTip = Resources.Resource.el_ReturnToCheckout    ' "Return To Checkout"
                    btnConfirmOrder.Style.Add("display", "inline")
                    labelAction.Visible = False

                    '-- added 03.04.04 - dwd
                    If Session.Item("carts") IsNot Nothing Then
                        Dim sm As New ShoppingCartManager
                        sm.DeleteShoppingCart(Session.Item("carts"))
                        Session.Remove("carts")
                    End If
                Else
                    ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                    btnConfirmOrder.ImageUrl = "images/sp_int_returnToCheckOut_btn.gif"
                    btnConfirmOrder.CommandName = "Return To Checkout"
                    btnConfirmOrder.ToolTip = Resources.Resource.el_ReturnToCheckout     '"Return To Checkout"
                    btnConfirmOrder.Style.Add("display", "inline")
                End If
            Finally
                objPCILogger.PushLogToMSMQ() '6524
            End Try

        End Sub

        Private Sub DisplayOrder()
            Dim objPCILogger As New PCILogger() '6524
            Dim hasSpecialTax As Boolean = False
            Dim tempPrice As Double
            Dim specialTaxPerItem As String
            Dim specialTaxTotal As String
            Dim linePricePerItem As String
            Dim linePriceTotal As String
            'Dim isDownloadable As Boolean = False   ' Unused
            Dim promoManager As PromotionManager = New PromotionManager
            Dim order As Order
            Dim cart As ShoppingCart
            Dim customer As Customer
            Dim TR As New TableRow
            Dim TD As New TableCell
            Dim hasPromo As Boolean = False
            'Dim strDescription As String
            'Dim strPartNumber As String
            'Dim strOldPartNumber As String

            Try
                objPCILogger.EventOriginApplication = "ServicesPLUS"
                objPCILogger.EventOriginApplicationLocation = Me.Page.GetType().Name
                objPCILogger.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
                objPCILogger.OperationalUser = Environment.UserName
                objPCILogger.EventDateTime = DateTime.Now.ToLongTimeString()
                objPCILogger.HTTPRequestObjectValues = objPCILogger.GetRequestContextValue()
                objPCILogger.IndicationSuccessFailure = "Success"
                objPCILogger.EventOriginMethod = "DisplayOrder"
                objPCILogger.Message = "DisplayOrder Success."
                objPCILogger.EventType = EventType.View

                If (Session.Item("carts") Is Nothing) Or (Session.Item("order") Is Nothing) Or (HttpContextManager.Customer Is Nothing) Then
                    ErrorLabel.Text = Resources.Resource.el_Err_DisplayOrder '"Error displaying order."
                    Return
                End If
                order = Session.Item("order")
                cart = Session.Item("carts")
                customer = HttpContextManager.Customer
                objPCILogger.CustomerID = customer.CustomerID
                objPCILogger.CustomerID_SequenceNumber = customer.SequenceNumber
                objPCILogger.EmailAddress = customer.EmailAddress
                objPCILogger.SIAM_ID = customer.SIAMIdentity
                objPCILogger.LDAP_ID = customer.LdapID

                Me.tdLegend.Visible = False

                '-- open table 
                TD.Controls.Add(New LiteralControl("<table border=""0"" cellpadding=""0"" cellspacing=""0"" width=""670"">" + vbCrLf))

                '-- header line 
                TD.Controls.Add(New LiteralControl("<tr bgcolor=""#d5dee9"">" + vbCrLf))
                TD.Controls.Add(New LiteralControl("<td width=""1"" bgColor=""#d5dee9""><img height=""35"" src=""images/spacer.gif"" width=""1"" alt=""""></td>" + vbCrLf))
                TD.Controls.Add(New LiteralControl("<td class=""tableHeader"" width=""75"" bgColor=""#d5dee9"" height=""35"" align=""center"">&nbsp;&nbsp;" + Resources.Resource.el_PartsRequesterd + "</td>" + vbCrLf))
                TD.Controls.Add(New LiteralControl("<td width=""1"" bgColor=""#d5dee9""><img height=""35"" src=""images/spacer.gif"" width=""1"" alt=""""></td>" + vbCrLf))
                TD.Controls.Add(New LiteralControl("<td class=""tableHeader"" width=""75"" bgColor=""#d5dee9"" height=""35"" align=""center"">&nbsp;&nbsp;" + Resources.Resource.el_PartsSupplied + "</td>" + vbCrLf))
                TD.Controls.Add(New LiteralControl("<td width=""1"" bgColor=""#d5dee9"" height=""28""><img height=""35"" src=""images/spacer.gif"" width=""1"" alt=""""></td>" + vbCrLf))
                TD.Controls.Add(New LiteralControl("<td class=""tableHeader"" width=""150"" bgColor=""#d5dee9"" height=""28"" align=""center"">&nbsp;&nbsp;" + Resources.Resource.el_Description + "</td>" + vbCrLf))
                TD.Controls.Add(New LiteralControl("<td width=""1"" bgColor=""#d5dee9"" height=""35""><img height=""35"" src=""images/spacer.gif"" width=""1"" alt=""""></td>" + vbCrLf))
                If cart.ContainsDownloadOnlyItems Then
                    TD.Controls.Add(New LiteralControl("<td class=""tableHeader"" colspan=""3""  width=""120"" bgColor=""#d5dee9"" height=""35"" align=""center"">" + Resources.Resource.el_Quantity + Resources.Resource.el_Requested + "</td>" + vbCrLf))
                    TD.Controls.Add(New LiteralControl("<td width=""1"" bgColor=""#d5dee9"" height=""35""><img height=""28"" src=""images/spacer.gif"" width=""1"" alt=""""></td>" + vbCrLf))
                Else
                    TD.Controls.Add(New LiteralControl("<td class=""tableHeader""  width=""60"" bgColor=""#d5dee9"" height=""35"" align=""center"">" + Resources.Resource.el_Quantity + "<br/>" + Resources.Resource.el_Requested + " </td>" + vbCrLf))
                    TD.Controls.Add(New LiteralControl("<td width=""1"" bgColor=""#d5dee9""  height=""35""><img height=""28"" src=""images/spacer.gif"" width=""1"" alt=""""></td>" + vbCrLf))
                    TD.Controls.Add(New LiteralControl("<td class=""tableHeader"" align=""center"" width=""60"" bgColor=""#d5dee9"" height=""35"" align=""center"">" + Resources.Resource.el_Quantity + "<br/>" + Resources.Resource.el_Available + "</td>" + vbCrLf))
                    TD.Controls.Add(New LiteralControl("<td width=""1"" bgColor=""#d5dee9"" height=""35""><img height=""28"" src=""images/spacer.gif"" width=""1"" alt=""""></td>" + vbCrLf))
                End If

                If HttpContextManager.GlobalData.SalesOrganization = ConfigurationData.GeneralSettings.GlobalData.SalesOrganization.CA Then
                    TD.Controls.Add(New LiteralControl("<td class=""tableHeader""  width=""60"" bgColor=""#d5dee9"" height=""35"" align=""center"">" + Resources.Resource.el_ItemPrice_CAD + "</td>" + vbCrLf))
                    TD.Controls.Add(New LiteralControl("<td width=""1"" bgColor=""#d5dee9"" height=""35""><img height=""28"" src=""images/spacer.gif"" width=""1"" alt=""""></td>" + vbCrLf))
                    TD.Controls.Add(New LiteralControl("<td class=""tableHeader"" width=""60"" bgColor=""#d5dee9"" height=""35"" align=""center"">" + Resources.Resource.el_TotalPrice_CAD + "</td>" + vbCrLf))
                Else
                    TD.Controls.Add(New LiteralControl("<td class=""tableHeader""  width=""60"" bgColor=""#d5dee9"" height=""35"" align=""center"">" + Resources.Resource.el_ItemPrice + "</td>" + vbCrLf))
                    TD.Controls.Add(New LiteralControl("<td width=""1"" bgColor=""#d5dee9"" height=""35""><img height=""28"" src=""images/spacer.gif"" width=""1"" alt=""""></td>" + vbCrLf))
                    TD.Controls.Add(New LiteralControl("<td class=""tableHeader"" width=""60"" bgColor=""#d5dee9"" height=""35"" align=""center"">" + Resources.Resource.el_TotalPrice + "</td>" + vbCrLf))
                End If

                TD.Controls.Add(New LiteralControl("<td width=""1"" bgColor=""#d5dee9"" height=""35""><img height=""28"" src=""images/spacer.gif"" width=""1"" alt=""""></td>" + vbCrLf))
                'TD.Controls.Add(New LiteralControl("<td class=""tableHeader"" align=""right"" width=""80"" bgColor=""#d5dee9"" height=""35"">Backordered&nbsp;</td>" + vbCrLf))
                'TD.Controls.Add(New LiteralControl("<td width=""1"" bgColor=""#d5dee9"" height=""35""><img height=""35"" src=""images/spacer.gif"" width=""1"" alt=""""></td>" + vbCrLf))
                TD.Controls.Add(New LiteralControl("</tr>"))

                '-- spacer line 
                TD.Controls.Add(New LiteralControl(getSpacerRow))

                '-- variable to flag downloadable items --
                'isDownloadable = False   ' Unused

                '-- loop through each line item of the order
                For Each i As OrderLineItem In order.LineItems1
                    TD.Controls.Add(New LiteralControl("<tr>" + vbCrLf))
                    TD.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>" + vbCrLf))
                    '-- Requested parts number
                    TD.Controls.Add(New LiteralControl("<td bgcolor=""#f2f5f8"" class=""tableData"" align=""center"">" + i.Product.PartNumber + "</td>" + vbCrLf))
                    TD.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>" + vbCrLf))
                    '-- Supplied parts number
                    TD.Controls.Add(New LiteralControl("<td bgcolor=""#f2f5f8"" class=""tableData"" align=""center"">" + i.Product.ReplacementPartNumber + "</td>" + vbCrLf))
                    TD.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>" + vbCrLf))
                    '-- Description 
                    TD.Controls.Add(New LiteralControl("<td bgcolor=""#f2f5f8"" class=""tableData"" align=""center"">&nbsp;" + i.Product.Description + "</td>" + vbCrLf))
                    TD.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>" + vbCrLf))

                    '-- Quantity Requested
                    If cart.ContainsDownloadOnlyItems Then
                        TD.Controls.Add(New LiteralControl("<td bgcolor=""#f2f5f8"" colspan =3 class=""tableData"" align=""center"">" + i.OrderQty.ToString + "</td>" + vbCrLf))
                        TD.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>" + vbCrLf))
                    Else
                        TD.Controls.Add(New LiteralControl("<td bgcolor=""#f2f5f8"" class=""tableData"" align=""center"">" + i.OrderQty.ToString + "</td>" + vbCrLf))
                        TD.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>" + vbCrLf))
                        '-- Quantity Available
                        TD.Controls.Add(New LiteralControl("<td bgcolor=""#f2f5f8"" class=""tableData"" align=""center"">" + i.ConfirmedQty.ToString + "</td>" + vbCrLf))
                        TD.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>" + vbCrLf))
                    End If

                    '-- List Price (per item)
                    If i.Product.ListPrice > 0.0R Then
                        linePricePerItem = String.Format("{0:c}", i.Product.ListPrice / i.Quantity)   ' Price per item, formatted for Currency
                        If promoManager.IsPromotionPart(i.Product.ReplacementPartNumber) Then
                            linePricePerItem &= " <span style=""font-family: Wingdings; color: red; font-size: 12px;"">v</span>"
                            hasPromo = True
                        End If
                    Else
                        linePricePerItem = String.Empty
                    End If
                    TD.Controls.Add(New LiteralControl("<td bgcolor=""#f2f5f8"" class=""tableData"" align=""center"">" + linePricePerItem + "</td>" + vbCrLf))

                    TD.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>" + vbCrLf))

                    '-- Total Price (for this Line Item)
                    If i.Product.ListPrice > 0.0R Then
                        tempPrice = i.OrderQty * (i.Product.ListPrice / i.Quantity)
                        linePriceTotal = (String.Format("{0:c}", tempPrice)).ToString()
                    Else
                        linePriceTotal = String.Empty
                    End If
                    TD.Controls.Add(New LiteralControl("<td bgcolor=""#f2f5f8"" class=""tableData"" align=""center"">" + linePriceTotal + "</td>" + vbCrLf))
                    TD.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>" + vbCrLf))

                    TD.Controls.Add(New LiteralControl(getSpacerRow2))

                    If i.Product.SpecialTax > 0.0R Then
                        hasSpecialTax = True

                        TD.Controls.Add(New LiteralControl("<tr>" + vbCrLf))
                        TD.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>" + vbCrLf))
                        '-- parts number
                        TD.Controls.Add(New LiteralControl("<td bgcolor=""#f2f5f8"" class=""tableData"" align=""center""></td>" + vbCrLf))
                        TD.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>" + vbCrLf))
                        '-- description 
                        TD.Controls.Add(New LiteralControl("<td bgcolor=""#f2f5f8"" class=""tableData"">&nbsp;" + Resources.Resource.el_RecyclingFee3 + "&nbsp;<span class=""tableHeader""><span class=""redAsterick"">*</span></span>" + "</td>" + vbCrLf))
                        TD.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>" + vbCrLf))
                        '-- quantity 
                        TD.Controls.Add(New LiteralControl("<td bgcolor=""#f2f5f8"" class=""tableData"" align=""center"">" + i.Quantity.ToString + "</td>" + vbCrLf))
                        TD.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>" + vbCrLf))
                        '-- list price
                        specialTaxPerItem = (String.Format("{0:c}", i.Product.SpecialTax / i.Quantity))
                        TD.Controls.Add(New LiteralControl("<td bgcolor=""#f2f5f8"" class=""tableData"" align=""right"">" + specialTaxPerItem + "</td>" + vbCrLf))
                        TD.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>" + vbCrLf))
                        '-- total price
                        'tempPrice = i.Product.SpecialTax
                        'If (i.Product.SpecialTax > 0.0R) Then
                        '    specialTaxTotal = (String.Format("{0:c}", tempPrice))
                        'Else : specialTaxTotal = "$0.00"
                        '    specialTaxTotal = String.Empty
                        'End If
                        specialTaxTotal = (String.Format("{0:c}", i.Product.SpecialTax))
                        TD.Controls.Add(New LiteralControl("<td bgcolor=""#f2f5f8"" class=""tableData"" align=""right"">" + specialTaxTotal + "</td>" + vbCrLf))
                        TD.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>" + vbCrLf))
                        ' -- is item on backorder - 
                        TD.Controls.Add(New LiteralControl("<td bgcolor=""#f2f5f8"" class=""redAsterick"" align=""center"">&nbsp;</td>" + vbCrLf))
                        TD.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>" + vbCrLf))
                        TD.Controls.Add(New LiteralControl("</tr>" + vbCrLf))

                        TD.Controls.Add(New LiteralControl(getSpacerRow2))
                    End If
                Next

                ' -- display legend only if there is at least one part with promotion
                If hasPromo Then
                    tdLegend.Visible = True
                End If

                '-- subtotal line
                TD.Controls.Add(New LiteralControl("<tr>"))
                TD.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>" + vbCrLf))
                TD.Controls.Add(New LiteralControl("<td colspan=""8"" bgcolor=""#ffffff""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
                TD.Controls.Add(New LiteralControl("<td colspan=""3"" bgcolor=""#d5dee9"" align=""right"" class=""tableHeader"">SUB&nbsp;TOTAL</td>" + vbCrLf))
                TD.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
                TD.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9"" align=""right"" class=""tableHeader"">"))
                TD.Controls.Add(New LiteralControl((String.Format("{0:c}", order.AllocatedPartSubTotal)).ToString() + "&nbsp;</td>" + vbCrLf))
                TD.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>" + vbCrLf))
                TD.Controls.Add(New LiteralControl("</tr>"))

                '-- spacer line 
                TD.Controls.Add(New LiteralControl(getSpacerRow))

                '-- shipping total line
                TD.Controls.Add(New LiteralControl("<tr>"))
                TD.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>" + vbCrLf))
                TD.Controls.Add(New LiteralControl("<td colspan=""8"" bgcolor=""#ffffff""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
                TD.Controls.Add(New LiteralControl("<td colspan=""3"" bgcolor=""#d5dee9"" align=""right"" class=""tableHeader"">" + Resources.Resource.el_SHIPPING + "&nbsp;</td>" + vbCrLf))
                TD.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
                TD.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9"" align=""right"" class=""tableHeader"">"))
                TD.Controls.Add(New LiteralControl((String.Format("{0:c}", order.AllocatedShipping)).ToString() + "&nbsp;</td>" + vbCrLf))
                TD.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>" + vbCrLf))
                TD.Controls.Add(New LiteralControl("</tr>"))

                '-- spacer line 
                TD.Controls.Add(New LiteralControl(getSpacerRow))

                '-- special tax line
                If order.AllocatedSpecialTax > 0.0R Then
                    TD.Controls.Add(New LiteralControl("<tr>"))
                    TD.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>" + vbCrLf))
                    TD.Controls.Add(New LiteralControl("<td colspan=""8"" bgcolor=""#ffffff""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
                    TD.Controls.Add(New LiteralControl("<td colspan=""3"" bgcolor=""#d5dee9"" align=""right"" class=""tableHeader"">" + Resources.Resource.el_STATERECYCLINGFEE + "&nbsp;<span class=""tableHeader""><span class=""redAsterick"">*</span></span>&nbsp;</td>" + vbCrLf))
                    TD.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
                    TD.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9"" align=""right"" class=""tableHeader"">"))
                    TD.Controls.Add(New LiteralControl((String.Format("{0:c}", order.AllocatedSpecialTax)).ToString() + "&nbsp;</td>" + vbCrLf))
                    TD.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>" + vbCrLf))
                    TD.Controls.Add(New LiteralControl("</tr>"))

                    TD.Controls.Add(New LiteralControl(getSpacerRow))
                End If

                '-- tax line 
                TD.Controls.Add(New LiteralControl("<tr>" + vbCrLf))
                TD.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>" + vbCrLf))
                TD.Controls.Add(New LiteralControl("<td colspan=""8"" bgcolor=""#ffffff""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
                TD.Controls.Add(New LiteralControl("<td colspan=""3"" bgcolor=""#d5dee9"" align=""right"" class=""tableHeader"">" + Resources.Resource.el_TAX + "&nbsp;</td>" + vbCrLf))
                TD.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
                TD.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9"" align=""right"" class=""tableHeader"">"))
                TD.Controls.Add(New LiteralControl((String.Format("{0:c}", order.AllocatedTax)).ToString() + "&nbsp;</td>" + vbCrLf))
                TD.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>" + vbCrLf))
                TD.Controls.Add(New LiteralControl("</tr>" + vbCrLf))

                TD.Controls.Add(New LiteralControl(getSpacerRow))

                '-- invoice total line 
                TD.Controls.Add(New LiteralControl("<tr>" + vbCrLf))
                TD.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>" + vbCrLf))
                TD.Controls.Add(New LiteralControl("<td colspan=""8"" bgcolor=""#ffffff""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))

                'price difference for CA and US 
                If HttpContextManager.GlobalData.SalesOrganization = ConfigurationData.GeneralSettings.GlobalData.SalesOrganization.CA Then
                    TD.Controls.Add(New LiteralControl("<td colspan=""3"" bgcolor=""#d5dee9"" align=""right"" class=""tableHeader"">" + Resources.Resource.el_ORDERTOTAL_CAD + "&nbsp;</td>" + vbCrLf))
                Else
                    TD.Controls.Add(New LiteralControl("<td colspan=""3"" bgcolor=""#d5dee9"" align=""right"" class=""tableHeader"">" + Resources.Resource.el_ORDERTOTAL + "&nbsp;</td>" + vbCrLf))
                End If

                TD.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
                TD.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9"" align=""right"" class=""tableHeader"">"))
                TD.Controls.Add(New LiteralControl((String.Format("{0:c}", order.AllocatedGrandTotal)).ToString() + "&nbsp;</td>" + vbCrLf))
                TD.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>" + vbCrLf))
                TD.Controls.Add(New LiteralControl("</tr>" + vbCrLf))

                '-- spacer 3 line 
                TD.Controls.Add(New LiteralControl(getSpacerRow2))

                '-- close table
                TD.Controls.Add(New LiteralControl("</table>" + vbCrLf))
                TR.Cells.Add(TD)
                TableOrderDetail.Rows.Add(TR)
                labelSpecialTax.Visible = hasSpecialTax
                specialTaxHyperLink.Visible = hasSpecialTax
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                objPCILogger.IndicationSuccessFailure = "Failure" '6524
                objPCILogger.Message = "DisplayOrder Failed. " & ex.Message.ToString() '6524
            Finally
                objPCILogger.PushLogToMSMQ() '6524
            End Try
        End Sub

        Private Function getSpacerRow() As String
            Dim row As New StringBuilder

            row.Append("<tr>" + vbCrLf)
            row.Append("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf)
            row.Append("<td colspan=""11"" bgcolor=""#ffffff""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf)
            row.Append("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf)
            row.Append("</tr>" + vbCrLf)

            Return row.ToString()
        End Function

        Private Function getSpacerRow2() As String
            Dim row As New StringBuilder

            row.Append("<tr>" + vbCrLf)
            row.Append("<td bgcolor=""#d5dee9"" colspan=""13""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf)
            row.Append("</tr>" + vbCrLf)

            Return row.ToString()
        End Function

    End Class

End Namespace
