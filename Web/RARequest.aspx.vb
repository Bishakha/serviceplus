Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Process
Imports ServicesPlusException
Imports Sony.US.AuditLog
Imports Sony.US.SIAMUtilities

Namespace ServicePLUSWebApp

    Partial Class RARequest
        Inherits SSL

#Region " page variabels    "
        Public cm As New CustomerManager
        Public customer As Customer
        Public ra_man As New RMAManager
        Public reasons() As RMAReason
        Public isAccntHolder As Boolean = False
        'Dim objUtilties As Utilities = New Utilities
        'Dim objGlobalData As New GlobalData

#End Region

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Protected WithEvents Label3 As System.Web.UI.WebControls.Label
        Protected WithEvents Label6 As System.Web.UI.WebControls.Label
        Protected WithEvents Label9 As System.Web.UI.WebControls.Label
        Protected WithEvents Textbox1 As Sony.US.ServicesPLUS.Controls.SPSTextBox
        Protected WithEvents Label10 As System.Web.UI.WebControls.Label
        Protected WithEvents Textbox2 As Sony.US.ServicesPLUS.Controls.SPSTextBox
        Protected WithEvents Label11 As System.Web.UI.WebControls.Label
        Protected WithEvents Textbox3 As Sony.US.ServicesPLUS.Controls.SPSTextBox
        Protected WithEvents Label12 As System.Web.UI.WebControls.Label
        Protected WithEvents Textbox4 As Sony.US.ServicesPLUS.Controls.SPSTextBox
        Protected WithEvents Label13 As System.Web.UI.WebControls.Label
        Protected WithEvents Textbox5 As Sony.US.ServicesPLUS.Controls.SPSTextBox
        Protected WithEvents Textbox6 As Sony.US.ServicesPLUS.Controls.SPSTextBox
        Protected WithEvents Label14 As System.Web.UI.WebControls.Label
        Protected WithEvents Textbox7 As Sony.US.ServicesPLUS.Controls.SPSTextBox
        Protected WithEvents Label15 As System.Web.UI.WebControls.Label
        Protected WithEvents Label16 As System.Web.UI.WebControls.Label
        Protected WithEvents Textbox8 As Sony.US.ServicesPLUS.Controls.SPSTextBox
        Protected WithEvents Label17 As System.Web.UI.WebControls.Label
        Protected WithEvents Textbox9 As Sony.US.ServicesPLUS.Controls.SPSTextBox
        Protected WithEvents Label18 As System.Web.UI.WebControls.Label
        Protected WithEvents Dropdownlist4 As System.Web.UI.WebControls.DropDownList
        Protected WithEvents Label19 As System.Web.UI.WebControls.Label
        Protected WithEvents Textbox10 As Sony.US.ServicesPLUS.Controls.SPSTextBox
        Protected WithEvents Label20 As System.Web.UI.WebControls.Label
        Protected WithEvents Textbox11 As Sony.US.ServicesPLUS.Controls.SPSTextBox
        Protected WithEvents Label21 As System.Web.UI.WebControls.Label
        Protected WithEvents Textbox12 As Sony.US.ServicesPLUS.Controls.SPSTextBox
        Protected WithEvents Label22 As System.Web.UI.WebControls.Label
        Protected WithEvents Textbox13 As Sony.US.ServicesPLUS.Controls.SPSTextBox
        Protected WithEvents Label23 As System.Web.UI.WebControls.Label
        Protected WithEvents Dropdownlist5 As System.Web.UI.WebControls.DropDownList
        Protected WithEvents Label24 As System.Web.UI.WebControls.Label
        Protected WithEvents Radiobuttonlist2 As System.Web.UI.WebControls.RadioButtonList
        Protected WithEvents Label25 As System.Web.UI.WebControls.Label
        Protected WithEvents Textbox14 As Sony.US.ServicesPLUS.Controls.SPSTextBox
        Protected WithEvents Imagebutton2 As System.Web.UI.WebControls.ImageButton


        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            'isProtectedPage(True)
            InitializeComponent()
        End Sub

#End Region

#Region "   Events  "

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                'ErrorValidation.Text = ""
                If HttpContextManager.Customer Is Nothing Then
                    Response.Redirect("SignIn.aspx?Lang=" + HttpContextManager.GlobalData.LanguageForIDP + "&RAForm=True")
                End If

                formFieldInitialization()
                stype.Value = IIf(Sony.US.SIAMUtilities.ConfigurationData.GeneralSettings.Environment = "Production", "", Sony.US.SIAMUtilities.ConfigurationData.GeneralSettings.Environment)
                If Not Session.Item("customer") Is Nothing Then customer = Session.Item("customer")
                cus_email.Value = customer.EmailAddress
                If Not Page.IsPostBack Then
                    'If Not (Session.Item("GlobalData")) Is Nothing Then
                    '    objGlobalData = (Session.Item("GlobalData"))
                    'End If
                    BuildStateDropDown()
                    fnZipCodeConfig()

                    PartNumber.Attributes.Add("onchange", "javascript: return PopupWindow();")
                    'If customer.UserType = "AccountHolder" Then'6668
                    If customer.UserType = "A" Or customer.UserType = "P" Then '6668
                        isAccntHolder = True
                        PopluateAccountNumbers(customer)
                    Else
                        isAccntHolder = False
                    End If

                    ErrorValidation.Visible = False

                    If ErrorValidation.Visible Then
                        ErrorValidation.Text = "Please complete the required fields."
                    Else
                        ErrorValidation.Text = ""

                        'Load textboxes w/customer data
                        FirstName.Text = customer.FirstName
                        LastName.Text = customer.LastName
                        email.Text = customer.EmailAddress
                        CompanyName.Text = customer.CompanyName
                        Line1.Text = customer.Address.Line1
                        Line2.Text = customer.Address.Line2
                        ' Line3.Text = customer.Address.Line3
                        City.Text = customer.Address.City
                        Dim sState As String = customer.Address.State
                        If Not sState = "" Then
                            ddlState.Items.FindByValue(sState).Selected = True
                        Else
                            Try
                                ddlState.Items.FindByValue("Select One").Selected = True
                            Catch ex As Exception

                            End Try
                        End If
                        Zip.Text = customer.Address.PostalCode
                        Phone.Text = customer.PhoneNumber

                    End If

                End If
                ' DataValidate()
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                ErrorLabel.Visible = True
            End Try
        End Sub


        Protected Sub ImageButton1_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
            Dim objPCILogger As New PCILogger() '6524
            Try
                '6524 start
                objPCILogger.EventOriginApplication = "ServicesPLUS"
                objPCILogger.EventOriginApplicationLocation = Me.Page.GetType().Name
                objPCILogger.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
                objPCILogger.OperationalUser = Environment.UserName
                objPCILogger.EventDateTime = DateTime.Now.ToLongTimeString()
                objPCILogger.HTTPRequestObjectValues = objPCILogger.GetRequestContextValue()
                objPCILogger.IndicationSuccessFailure = "Success"
                objPCILogger.EventOriginMethod = "ImageButton1_Click"
                objPCILogger.Message = "Create RARequest Success."
                objPCILogger.EventType = EventType.Add
                '6524 end

                'create a new request if the form is validated
                'If ErrorValidation.Text.Length = 0 Then
                If DataValidate() Then
                    Dim request As RMA

                    Dim EmailBody As New StringBuilder(5000)
                    Dim Subject As String = String.Empty

                    request = ra_man.CreateRMARequest(customer)
                    request.InvoiceNumber = OInvoiceNumber.Text
                    If RadioButtonList1.SelectedIndex = 0 Then
                        request.Warranty = True
                    Else
                        request.Warranty = False

                    End If

                    request.OrderNumber = OOrderNumber.Text
                    Dim whichReason As Integer
                    whichReason = Reason.SelectedIndex - 1
                    reasons = Session.Item("reasons")
                    request.Reason = reasons.GetValue(whichReason)
                    request.PartNumber = PartNumber.Text
                    request.Comments = Remarks.Text
                    request.SerialNumber = SerialNumber.Text

                    If Not Price.Text = "" Then
                        request.Price = Price.Text
                    End If

                    ra_man.SubmitRMARequest(request)

                    BuildMailBodyToCustomerService(EmailBody, Subject, request)
                    ra_man.SendRARequestEmailToCustomerService(EmailBody.ToString(), Subject)

                    buildMailBodyConfirmation(EmailBody, Subject, request)
                    ra_man.SendRARequestConfirmation(request.Customer.EmailAddress, EmailBody.ToString(), Subject)

                    objPCILogger.PushLogToMSMQ() '6524
                    Response.Redirect(ConfigurationData.Environment.RA_Form_Processor.Action)


                End If

            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                objPCILogger.IndicationSuccessFailure = "Failure" '6524
                objPCILogger.Message = "Create RARequest Failed. " & ex.Message.ToString() '6524
                objPCILogger.PushLogToMSMQ() '6524
                'Return
            Finally

            End Try
        End Sub

#End Region

#Region "   Methods "
        Private Sub PopluateAccountNumbers(ByRef thisCustomer As Customer)
            Try
                If Not thisCustomer Is Nothing Then
                    Dim thisCM As New CustomerManager
                    '***************Changes done for Bug# 1212 Starts here***************
                    ''**********Kannapiran S****************
                    '
                    'objSISParameter.UserLocation = customer.UserLocation
                    '***************Changes done for Bug# 1212 Ends here***************
                    'For Each thisAccount As Account In thisCustomer.SAPBillToAccounts
                    '        thisCM.PopulateSAPAccount(thisAccount)
                    '    If thisAccount.Validated Then
                    '        Dim thisSB As New StringBuilder
                    '        If Not thisAccount.AccountNumber Is Nothing Then thisSB.Append(thisAccount.AccountNumber.ToString() + " -")
                    '        If Not thisAccount.Address.Name Is Nothing Then thisSB.Append(" " + thisAccount.Address.Name.ToString())
                    '        If Not thisAccount.Address.Line1 Is Nothing Then thisSB.Append(", " + thisAccount.Address.Line1.ToString())
                    '            If Not thisAccount.Address.Line2 Is Nothing Then thisSB.Append(", " + thisAccount.Address.Line2.ToString())
                    '            If Not thisAccount.Address.Line3 Is Nothing Then thisSB.Append(", " + thisAccount.Address.Line3.ToString())
                    '        DropDownList2.Items.Add(New ListItem(thisSB.ToString(), thisAccount.AccountNumber.ToString()))
                    '    End If
                    'Next

                    'If customer.StatusID = 3 Then'6668
                    If customer.UserType = "P" Then '6668
                        For Each objLoopAccount As Account In customer.SAPBillToAccounts
                            'If objLoopAccount.Validated Then 'commented for 6865
                            If customer.UserType = "A" Then ' ASleight - This block is unreachable. UserType = P and A?
                                Dim thisSB As New StringBuilder
                                If Not objLoopAccount.AccountNumber Is Nothing Then thisSB.Append(objLoopAccount.AccountNumber)
                                DropDownList2.Items.Add(New ListItem(thisSB.ToString(), objLoopAccount.AccountNumber))
                            End If
                        Next
                    Else
                        For Each objLoopAccount As Account In customer.SAPBillToAccounts
                            'If objLoopAccount.Validated Then 'commented for 6865
                            If customer.UserType() = "A" Then
                                ''changed for bug 6219
                                objLoopAccount = cm.PopulateCustomerDetailWithSAPAccount(objLoopAccount)
                                Dim thisSB As New StringBuilder
                                If Not objLoopAccount.AccountNumber Is Nothing Then thisSB.Append(objLoopAccount.AccountNumber.ToString() + "-")
                                If Not objLoopAccount.Address Is Nothing Then
                                    If Not objLoopAccount.Address.Name Is Nothing Then thisSB.Append(" " + objLoopAccount.Address.Name.ToString())
                                    If Not objLoopAccount.Address.Line1 Is Nothing Then thisSB.Append(" " + objLoopAccount.Address.Line1.ToString())
                                    If Not objLoopAccount.Address.Line2 Is Nothing Then thisSB.Append(" " + objLoopAccount.Address.Line2.ToString())
                                End If
                                DropDownList2.Items.Add(New ListItem(thisSB.ToString(), objLoopAccount.AccountNumber))
                            End If
                        Next
                    End If

                    DropDownList2.Items.Insert(0, New ListItem("Please select an account number", ""))
                    DropDownList2.ClearSelection()
                    DropDownList2.SelectedIndex = 0
                    ' DropDownList2.Items.FindByValue("").Selected = True
                End If

                Return
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                Return
            End Try
        End Sub

        Private Function DataValidate() As Boolean
            Dim validdata As Boolean
            validdata = True
            ErrorValidation.Visible = False

            ErrorValidation.Text = ""
            ErrorLabel.Text = ""

            LabelFN.CssClass = "bodyCopy"
            LabelLN.CssClass = "bodyCopy"
            LabelEmail.CssClass = "bodyCopy"
            LabelCO.CssClass = "bodyCopy"
            LabelLine1.CssClass = "bodyCopy"
            LabelLine2.CssClass = "bodyCopy"
            ' LabelLine3.CssClass = "bodyCopy"
            LabelCity.CssClass = "bodyCopy"
            LabelState.CssClass = "bodyCopy"
            LabelZip.CssClass = "bodyCopy"
            LabelPhone.CssClass = "bodyCopy"
            LabelRemarks.CssClass = "bodyCopy"
            Label4.CssClass = "bodyCopy"
            Label5.CssClass = "bodyCopy"


            ' LabelLine1Star.Text = ""
            ' LabelLine2Star.Text = ""
            ' LabelLine3Star.Text = ""
            ' LabelLine4Star.Text = ""
            Dim errormessage As String
            errormessage = ""

            Dim sPhoneNumber As String = Phone.Text.Trim()
            Dim objHPhoneNumber As New Regex("(\d{3}[-]{1}\d{3}[-]{1}\d{4})")
            If Not objHPhoneNumber.IsMatch(sPhoneNumber) Then
                ErrorValidation.Visible = True
                LabelPhone.CssClass = "redAsterick"
                validdata = False

            End If

            Dim sEmail As String = email.Text.Trim()
            Dim objEmail As New Regex("\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*")
            If Not objEmail.IsMatch(sEmail) Then
                ErrorValidation.Visible = True
                LabelEmail.CssClass = "redAsterick"
                validdata = False
            End If

            If FirstName.Text = "" Then
                ErrorValidation.Visible = True
                LabelFN.CssClass = "redAsterick"
                validdata = False
            End If
            If LastName.Text = "" Then
                ErrorValidation.Visible = True
                LabelLN.CssClass = "redAsterick"
            End If
            If email.Text = "" Then
                ErrorValidation.Visible = True
                LabelEmail.CssClass = "redAsterick"
                validdata = False
            End If

            If CompanyName.Text = "" Then
                ErrorValidation.Visible = True
                LabelCO.CssClass = "redAsterick"
                validdata = False
            End If

            If Line1.Text = "" Then
                ErrorValidation.Visible = True
                LabelLine1.CssClass = "redAsterick"
                '  LabelLine1Star.Text = "<span class=""redAsterick"">*</span>"
                validdata = False
                'Else
                '    LabelLine1Star.Text = "<span class=""redAsterick"">*</span>"
            End If

            If Line1.Text.Length > 35 Then
                ErrorValidation.Visible = True
                LabelLine1.CssClass = "redAsterick"
                ' LabelLine1Star.Text = "<span class=""redAsterick"">&nbsp;&nbsp;Street Address must be 35 characters or less in length.</span>"
                errormessage += " Street Address must be 35 characters or less in length.</br>"
                validdata = False
                Line1.Focus()

                'Else
                '    'LabelLine1Star.Text = "<span class=""redAsterick"">*</span>"
            End If

            If Line2.Text = "" Then
                ErrorValidation.Visible = True
                LabelLine2.CssClass = "redAsterick"
                'LabelLine4Star.Text = "<span class=""redAsterick"">*</span>"
                validdata = False
                'Else
                '    LabelLine4Star.Text = "<span class=""redAsterick"">*</span>"
            End If


            If Line2.Text.Length > 35 Then
                ErrorValidation.Visible = True
                LabelLine2.CssClass = "redAsterick"
                ' LabelLine2Star.Text = "<span class=""redAsterick"">&nbsp;&nbsp;Address 2nd Line must be 35 characters or less in length.</span>"
                errormessage += " Address 2nd Line must be 35 characters or less in length.</br>"
                validdata = False
                Line2.Focus()

            End If

            'If Line3.Text.Length > 35 Then
            '    ErrorValidation.Visible = True
            '    LabelLine3.CssClass = "redAsterick"
            '    LabelLine3Star.Text = "<span class=""redAsterick"">&nbsp;&nbsp;Address 3rd Line must be 35 characters or less in length.</span>"
            '    Line3.Focus()
            'End If

            If City.Text = "" Then
                ErrorValidation.Visible = True
                LabelCity.CssClass = "redAsterick"
                'LabelLine4Star.Text = "<span class=""redAsterick"">*</span>"
                validdata = False
                'Else
                '    LabelLine4Star.Text = "<span class=""redAsterick"">*</span>"
            End If

            If City.Text.Length > 35 Then
                ErrorValidation.Visible = True
                LabelCity.CssClass = "redAsterick"
                ' LabelLine4Star.Text = "<span class=""redAsterick"">&nbsp;&nbsp;City must be 35 characters or less in length.</span>"
                errormessage += " City must be 35 characters or less in length.</br>"
                validdata = False
                City.Focus()
                'Else
                '    LabelLine4Star.Text = "<span class=""redAsterick"">*</span>"
            End If

            If ddlState.SelectedIndex = 0 Then
                Try
                    Dim sStateAbbrev As String = ddlState.SelectedValue.ToString().Trim().ToUpper()
                    If (sStateAbbrev.Trim().Length = 0) Then
                        ErrorValidation.Visible = True
                        LabelState.CssClass = "redAsterick"
                        validdata = False
                    End If
                Catch ex As Exception

                End Try
            End If

            If Reason.SelectedIndex = 0 Then
                ErrorValidation.Visible = True
                LabelReason.CssClass = "redAsterick"
                validdata = False
            End If

            If Zip.Text = "" Then
                ErrorValidation.Visible = True
                LabelZip.CssClass = "redAsterick"
                validdata = False
            End If

            ' -- either the invoice or the order number is required -- dwd
            If OInvoiceNumber.Text = "" And OOrderNumber.Text = "" Then
                ErrorValidation.Visible = True
                LabelOOrderNumber.CssClass = "redAsterick"
                ' ErrorLabel.Text = "You must enter either an invoice or order numbers."
                errormessage += " You must enter either an invoice or order numbers.</br>"
                validdata = False
            End If

            If PartNumber.Text = "" Then
                ErrorValidation.Visible = True
                Label4.CssClass = "redAsterick"
                validdata = False
            End If

            'sathish changes start
            If PartNumber.Text <> "" Then

                If PartNumber.Text.Length > 18 Then

                    ErrorValidation.Visible = True
                    Label4.CssClass = "redAsterick"
                    'ErrorLabel.Text = "Part number should be less than 18 charecters."
                    errormessage += " Part number should be less than 18 charecters.</br>"
                    validdata = False
                End If
            End If
            'regular expression (^\d{3}\.\d{2]$)

            If Not String.IsNullOrEmpty(Price.Text) Then 'checking if its empty or not if not empty we need to validate
                If Microsoft.VisualBasic.Information.IsNumeric(Price.Text) Then
                    If Price.Text.Contains(".") Then
                        'number(9,2) can take 7 chars before precision
                        Dim result As String() = Price.Text.Split(New String() {"."}, StringSplitOptions.None)
                        If result(0).Length > 7 Then
                            ErrorValidation.Visible = True
                            Label5.CssClass = "redAsterick"
                            'ErrorLabel.Text = "Please enter valid price."
                            errormessage += "Please enter a Price value less than or equal to 9999999.99.</br>"
                            validdata = False
                        End If
                        '1234567.8923 roundof 1234567.89 
                        'If result(1).Length > 2 Then
                        '    ErrorValidation.Visible = True
                        '    Label5.CssClass = "redAsterick"
                        '    ' ErrorLabel.Text = "Please enter valid price."
                        '    errormessage += " Please enter valid price.</br>"
                        '    validdata = False
                        'End If
                    End If

                    If Not Price.Text.Contains(".") Then
                        'number(9,2) can take 7 chars before precision
                        If Price.Text.Length > 7 Then
                            ErrorValidation.Visible = True
                            Label5.CssClass = "redAsterick"
                            ' ErrorLabel.Text = "Please enter valid price."
                            errormessage += " Please enter a Price value less than or equal to 9999999.99</br>"
                            validdata = False
                        End If
                    End If
                End If
                If Not Microsoft.VisualBasic.Information.IsNumeric(Price.Text) Then
                    ErrorValidation.Visible = True
                    Label5.CssClass = "redAsterick"
                    'ErrorLabel.Text = "Please enter numeric value."
                    errormessage += " Please enter a numeric Price value.</br>"

                    validdata = False
                End If
            End If
            'sathish changes end 

            If Remarks.Text = "" Then
                ErrorValidation.Visible = True
                LabelRemarks.CssClass = "redAsterick"
                'ErrorLabel.Text = "Please enter remarks."
                errormessage += " Please enter Remarks.</br>"
                validdata = False

            End If


            If Remarks.Text <> "" Then
                If Remarks.Text.Length > 400 Then
                    ErrorValidation.Visible = True
                    LabelRemarks.CssClass = "redAsterick"
                    '  ErrorLabel.Text = "Please enter Remarks that are less than 401 characters long."
                    errormessage += " Please enter Remarks that are less than 401 characters long.</br>"
                    validdata = False
                End If
            End If
            If ErrorValidation.Visible = True Then
                ErrorValidation.Text = errormessage
            End If

            Return validdata
        End Function

        'Public Sub BuildStateDropDown()

        '    DropDownList1.Items.Add(New ListItem("Select One", ""))
        '    DropDownList1.Items.Add(New ListItem("AL", "AL"))
        '    DropDownList1.Items.Add(New ListItem("AR", "AR"))
        '    DropDownList1.Items.Add(New ListItem("AZ", "AZ"))
        '    DropDownList1.Items.Add(New ListItem("CA", "CA"))
        '    DropDownList1.Items.Add(New ListItem("CO", "CO"))
        '    DropDownList1.Items.Add(New ListItem("CT", "CT"))
        '    DropDownList1.Items.Add(New ListItem("DC", "DC"))
        '    DropDownList1.Items.Add(New ListItem("DE", "DE"))
        '    DropDownList1.Items.Add(New ListItem("FL", "FL"))
        '    DropDownList1.Items.Add(New ListItem("GA", "GA"))
        '    DropDownList1.Items.Add(New ListItem("HI", "HI"))
        '    DropDownList1.Items.Add(New ListItem("IA", "IA"))
        '    DropDownList1.Items.Add(New ListItem("ID", "ID"))
        '    DropDownList1.Items.Add(New ListItem("IL", "IL"))
        '    DropDownList1.Items.Add(New ListItem("IN", "IN"))
        '    DropDownList1.Items.Add(New ListItem("KS", "KS"))
        '    DropDownList1.Items.Add(New ListItem("KY", "KY"))
        '    DropDownList1.Items.Add(New ListItem("LA", "LA"))
        '    DropDownList1.Items.Add(New ListItem("MA", "MA"))
        '    DropDownList1.Items.Add(New ListItem("MD", "MD"))
        '    DropDownList1.Items.Add(New ListItem("ME", "ME"))
        '    DropDownList1.Items.Add(New ListItem("MI", "MI"))
        '    DropDownList1.Items.Add(New ListItem("MN", "MN"))
        '    DropDownList1.Items.Add(New ListItem("IN", "IN"))
        '    DropDownList1.Items.Add(New ListItem("MO", "MO"))
        '    DropDownList1.Items.Add(New ListItem("MS", "MS"))
        '    DropDownList1.Items.Add(New ListItem("MT", "MT"))
        '    DropDownList1.Items.Add(New ListItem("NC", "NC"))
        '    DropDownList1.Items.Add(New ListItem("ND", "ND"))
        '    DropDownList1.Items.Add(New ListItem("NE", "NE"))
        '    DropDownList1.Items.Add(New ListItem("NH", "NH"))
        '    DropDownList1.Items.Add(New ListItem("NJ", "NJ"))
        '    DropDownList1.Items.Add(New ListItem("NM", "NM"))
        '    DropDownList1.Items.Add(New ListItem("NV", "NV"))
        '    DropDownList1.Items.Add(New ListItem("NY", "NY"))
        '    DropDownList1.Items.Add(New ListItem("OH", "OH"))
        '    DropDownList1.Items.Add(New ListItem("OK", "OK"))
        '    DropDownList1.Items.Add(New ListItem("OR", "OR"))
        '    DropDownList1.Items.Add(New ListItem("PA", "PA"))
        '    DropDownList1.Items.Add(New ListItem("RI", "RI"))
        '    DropDownList1.Items.Add(New ListItem("SC", "SC"))
        '    DropDownList1.Items.Add(New ListItem("SD", "SD"))
        '    DropDownList1.Items.Add(New ListItem("TN", "TN"))
        '    DropDownList1.Items.Add(New ListItem("TX", "TX"))
        '    DropDownList1.Items.Add(New ListItem("UT", "UT"))
        '    DropDownList1.Items.Add(New ListItem("VA", "VA"))
        '    DropDownList1.Items.Add(New ListItem("VT", "VT"))
        '    DropDownList1.Items.Add(New ListItem("WA", "WA"))
        '    DropDownList1.Items.Add(New ListItem("WI", "WI"))
        '    DropDownList1.Items.Add(New ListItem("WV", "WV"))
        '    DropDownList1.Items.Add(New ListItem("WY", "WY"))


        '    'Load reasons ddl
        '    Reason.Items.Add(New ListItem("Select One", ""))
        '    reasons = ra_man.GetAllRACreditReasons()
        '    Session.Add("reasons", reasons)


        '    For Each r As RMAReason In reasons
        '        Reason.Items.Add(New ListItem(r.Description, r.Description)) ' r.ReasonID))
        '    Next
        'End Sub

        Private Sub BuildStateDropDown()
            PopulateStateDropdownList(ddlState)
            'Dim cm As New CatalogManager
            'Dim statedetails() As StatesDetail
            'statedetails = cm.StatesBySalesOrg(objGlobalData)
            ''bind card type array to dropdown list control   

            'Dim strMsg As String = Resources.Resource.el_rgn_Select
            'Dim defaultItem As New ListItem
            'defaultItem.Text = strMsg
            'defaultItem.Value = ""
            'defaultItem.Selected = True

            'DropDownList1.DataSource = statedetails
            'DropDownList1.DataTextField = "StateAbbr"
            'DropDownList1.DataValueField = "StateAbbr"
            'DropDownList1.DataBind()
            '' DropDownList1.Items.Insert(0, defaultItem)

            'Load reasons ddl
            Reason.Items.Add(New ListItem("Select One", ""))
            reasons = ra_man.GetAllRACreditReasons()
            Session.Add("reasons", reasons)


            For Each r As RMAReason In reasons
                Reason.Items.Add(New ListItem(r.Description, r.Description)) ' r.ReasonID))
            Next
        End Sub
        Private Sub fnZipCodeConfig()
            Dim intLength As Int32
            Dim strValidate As String = String.Empty

            If HttpContextManager.GlobalData.SalesOrganization = ConfigurationData.GeneralSettings.GlobalData.SalesOrganization.US Then
                LabelZip.Text = "Zip Code"
                intLength = Convert.ToInt32(ConfigurationData.GeneralSettings.GlobalData.ZipCode.US)
                strValidate = "\d{" + intLength.ToString() + "}(-\d{4})?$"
            Else
                LabelZip.Text = "Postal Code"
                intLength = Convert.ToInt32(ConfigurationData.GeneralSettings.GlobalData.ZipCode.CA)
                strValidate = "^[ABCEGHJKLMNPRSTVXY]{1}\d{1}[A-Z]{1} *\d{1}[A-Z]{1}\d{1}$"
            End If
            Zip.Text = ""
            Zip.MaxLength = intLength

            'Upper case - Zip code 
            Zip.Attributes.Add("onblur", "this.value = this.value.toUpperCase()")

            'zipRegularExpressionValidator.ValidationExpression = strValidate
            'zipRegularExpressionValidator.ErrorMessage = "</br>Please enter " + intLength.ToString() + " digit zip code."
        End Sub
        Private Sub formFieldInitialization()
            FirstName.Attributes("onkeydown") = "SetTheFocusButton(event, 'ImageButton1')"
            LastName.Attributes("onkeydown") = "SetTheFocusButton(event, 'ImageButton1')"
            email.Attributes("onkeydown") = "SetTheFocusButton(event, 'ImageButton1')"
            CompanyName.Attributes("onkeydown") = "SetTheFocusButton(event, 'ImageButton1')"
            Line1.Attributes("onkeydown") = "SetTheFocusButton(event, 'ImageButton1')"
            Line2.Attributes("onkeydown") = "SetTheFocusButton(event, 'ImageButton1')"
            'Line3.Attributes("onkeydown") = "SetTheFocusButton(event, 'ImageButton1')"
            City.Attributes("onkeydown") = "SetTheFocusButton(event, 'ImageButton1')"
            Zip.Attributes("onkeydown") = "SetTheFocusButton(event, 'ImageButton1')"
            Phone.Attributes("onkeydown") = "SetTheFocusButton(event, 'ImageButton1')"
            OOrderNumber.Attributes("onkeydown") = "SetTheFocusButton(event, 'ImageButton1')"
            OInvoiceNumber.Attributes("onkeydown") = "SetTheFocusButton(event, 'ImageButton1')"
            PartNumber.Attributes("onkeydown") = "SetTheFocusButton(event, 'ImageButton1')"
            Price.Attributes("onkeydown") = "SetTheFocusButton(event, 'ImageButton1')"
            Remarks.Attributes("onkeydown") = "SetTheFocusButton(event, 'ImageButton1')"
            SerialNumber.Attributes("onkeydown") = "SetTheFocusButton(event, 'ImageButton1')"
        End Sub

        <System.Web.Services.WebMethod(EnableSession:=True)>
        Public Shared Function GetConflictedPart(ByVal PartNumber As String) As String
            Dim ra_man As New RMAManager
            Try

                'Dim objGlobalData As New GlobalData

                'If Not HttpContext.Current.Session.Item("GlobalData") Is Nothing Then
                '    objGlobalData = HttpContext.Current.Session.Item("GlobalData")
                'End If


                Dim result = String.Empty
                result = ra_man.GetConflictedPart(PartNumber, HttpContextManager.GlobalData)
                Return result

            Catch ex As Exception
                Return ex.Message
            End Try
        End Function

#End Region

        Protected Sub Page_PreLoad(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreLoad
            'form4.Action = ConfigurationData.Environment.RA_Form_Processor.Action
            'form4.ID = ConfigurationData.Environment.RA_Form_Processor.FormId

        End Sub

        Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
            '  ImageButton1.Attributes.Add("onclick", "javascript:if(CheckAddresLength()==false) return false;")
            ImageButton1.Attributes.Add("onclick", "return validate();")
        End Sub

        Protected Sub ImageButton1_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles ImageButton1.Load

        End Sub


        Sub BuildMailBodyToCustomerService(ByRef Emailbody As StringBuilder, ByRef Subject As String, ByVal request As RMA)

            'Dim objGlobalData As New GlobalData
            'If Not Session.Item("GlobalData") Is Nothing Then
            '    objGlobalData = Session.Item("GlobalData")
            'End If

            Dim NewLine As String = "<br/>"

            Emailbody.Append(Resources.Resource_mail.ml_RARequest_csHeading_en + NewLine + NewLine)
            Emailbody.Append(Resources.Resource_mail.ml_RARequest_Datesubmitted_en + DateTime.Now.ToShortDateString() + NewLine)
            Emailbody.Append(Resources.Resource_mail.ml_RARequest_FirstName_en + request.Customer.FirstName + NewLine)
            Emailbody.Append(Resources.Resource_mail.ml_RARequest_LastName_en + request.Customer.LastName + NewLine)
            Emailbody.Append(Resources.Resource_mail.ml_RARequest_emailaddress_en + request.Customer.EmailAddress + NewLine)
            Emailbody.Append(Resources.Resource_mail.ml_RARequest_companyname_en + request.Customer.CompanyName + NewLine)
            Emailbody.Append(Resources.Resource_mail.ml_RARequest_Address1_en + request.Customer.Address.Line1 + NewLine)
            Emailbody.Append(Resources.Resource_mail.ml_RARequest_Address2_en + NewLine)
            Emailbody.Append(Resources.Resource_mail.ml_RARequest_city_en + request.Customer.Address.City + NewLine)
            Emailbody.Append(Resources.Resource_mail.ml_RARequest_state_en + request.Customer.Address.State + NewLine)
            Emailbody.Append(Resources.Resource_mail.ml_RARequest_zip_en + request.Customer.Address.PostalCode + NewLine)
            Emailbody.Append(Resources.Resource_mail.ml_RARequest_phone_en + request.Customer.PhoneNumber + NewLine)
            Emailbody.Append(Resources.Resource_mail.ml_RARequest_originalordernumber_en + request.OrderNumber + NewLine)
            Emailbody.Append(Resources.Resource_mail.ml_RARequest_originalinvoicenumber_en + request.InvoiceNumber + NewLine)
            Emailbody.Append(Resources.Resource_mail.ml_RARequest_partnumber_en + request.PartNumber + NewLine)
            Emailbody.Append(Resources.Resource_mail.ml_RARequest_price_en + request.Price.ToString() + NewLine)
            Emailbody.Append(Resources.Resource_mail.ml_RARequest_reasonforreturn_en + request.Reason.Description + NewLine)

            If request.Warranty Then
                Emailbody.Append(Resources.Resource_mail.ml_RARequest_warranty_en + "Y" + NewLine)
            Else
                Emailbody.Append(Resources.Resource_mail.ml_RARequest_warranty_en + "N" + NewLine)
            End If

            Emailbody.Append(Resources.Resource_mail.ml_RARequest_serialnumber_en + request.SerialNumber + NewLine)
            Emailbody.Append(Resources.Resource_mail.ml_RARequest_comments_en + request.Comments + NewLine + NewLine)

            If HttpContextManager.GlobalData.SalesOrganization = ConfigurationData.GeneralSettings.GlobalData.SalesOrganization.CA Then


                Emailbody.Append("--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------" + NewLine + NewLine)

                Emailbody.Append(Resources.Resource_mail.ml_RARequest_csHeading_fr + NewLine + NewLine)
                Emailbody.Append(Resources.Resource_mail.ml_RARequest_Datesubmitted_fr + DateTime.Now.ToShortDateString() + NewLine)
                Emailbody.Append(Resources.Resource_mail.ml_RARequest_FirstName_fr + request.Customer.FirstName + NewLine)
                Emailbody.Append(Resources.Resource_mail.ml_RARequest_LastName_fr + request.Customer.LastName + NewLine)
                Emailbody.Append(Resources.Resource_mail.ml_RARequest_emailaddress_fr + request.Customer.EmailAddress + NewLine)
                Emailbody.Append(Resources.Resource_mail.ml_RARequest_companyname_fr + request.Customer.CompanyName + NewLine)
                Emailbody.Append(Resources.Resource_mail.ml_RARequest_Address1_fr + request.Customer.Address.Line1 + NewLine)
                Emailbody.Append(Resources.Resource_mail.ml_RARequest_Address2_fr + NewLine)
                Emailbody.Append(Resources.Resource_mail.ml_RARequest_city_fr + request.Customer.Address.City + NewLine)
                Emailbody.Append(Resources.Resource_mail.ml_RARequest_state_fr + request.Customer.Address.State + NewLine)
                Emailbody.Append(Resources.Resource_mail.ml_RARequest_zip_fr + request.Customer.Address.PostalCode + NewLine)
                Emailbody.Append(Resources.Resource_mail.ml_RARequest_phone_fr + request.Customer.PhoneNumber + NewLine)
                Emailbody.Append(Resources.Resource_mail.ml_RARequest_originalordernumber_fr + request.OrderNumber + NewLine)
                Emailbody.Append(Resources.Resource_mail.ml_RARequest_originalinvoicenumber_fr + request.InvoiceNumber + NewLine)
                Emailbody.Append(Resources.Resource_mail.ml_RARequest_partnumber_fr + request.PartNumber + NewLine)
                Emailbody.Append(Resources.Resource_mail.ml_RARequest_price_fr + request.Price.ToString() + NewLine)
                Emailbody.Append(Resources.Resource_mail.ml_RARequest_reasonforreturn_fr + request.Reason.Description + NewLine)

                If request.Warranty Then
                    Emailbody.Append(Resources.Resource_mail.ml_RARequest_warranty_fr + "Y" + NewLine)
                Else
                    Emailbody.Append(Resources.Resource_mail.ml_RARequest_warranty_fr + "N" + NewLine)
                End If

                Emailbody.Append(Resources.Resource_mail.ml_RARequest_serialnumber_fr + request.SerialNumber + NewLine)
                Emailbody.Append(Resources.Resource_mail.ml_RARequest_comments_fr + request.Comments + NewLine + NewLine)


                Subject = Resources.Resource_mail.ml_RARequest_subject_en + "/" + Resources.Resource_mail.ml_RARequest_subject_fr
            Else
                Subject = Resources.Resource_mail.ml_RARequest_subject_en
            End If


        End Sub

        Sub buildMailBodyConfirmation(ByRef Emailbody As StringBuilder, ByRef Subject As String, ByVal request As RMA)

            'Dim objGlobalData As New GlobalData
            'If Not Session.Item("GlobalData") Is Nothing Then
            '    objGlobalData = Session.Item("GlobalData")
            'End If

            Dim NewLine As String = "<br/>"

            Emailbody.Length = 0

            Dim servicesplus_link As String = ConfigurationData.GeneralSettings.URL.Servicesplus_Link
            Dim promotions_link As String = ConfigurationData.GeneralSettings.URL.Promotions_Link
            Dim find_parts_link As String = ConfigurationData.GeneralSettings.URL.Find_Parts_Link
            Dim find_software_link As String = ConfigurationData.GeneralSettings.URL.Find_Software_Link
            Dim privacy_policy_link As String = ConfigurationData.GeneralSettings.URL.Privacy_Policy_Link
            Dim terms_and_conditions_link As String = ConfigurationData.GeneralSettings.URL.Terms_And_Conditions_Link
            Dim contact_us_link As String = ConfigurationData.GeneralSettings.URL.Contact_Us_Link
            Dim customer_service_link As String = ConfigurationData.GeneralSettings.URL.Customer_Service_Link
            Dim terms_and_conditions_link_ca As String = ConfigurationData.GeneralSettings.URL.Terms_And_Conditions_Link_ca
            Dim contact_us_link_ca As String = ConfigurationData.GeneralSettings.URL.Contact_Us_Link_ca
            Dim customer_service_link_ca As String = ConfigurationData.GeneralSettings.URL.Customer_Service_Link_ca

            Emailbody.Append(Resources.Resource_mail.ml_common_Dear_en + request.Customer.FirstName + " " + request.Customer.LastName + "," + NewLine + NewLine)
            ' EmailBody.Append("Your RA request form has been received by our customer service group.  A customer ")
            Emailbody.Append(Resources.Resource_mail.ml_RARequestcon_body_line1_en)
            Emailbody.Append(Resources.Resource_mail.ml_RARequestcon_body_line2_en + NewLine + NewLine)

            Emailbody.Append(Resources.Resource_mail.ml_RARequest_Datesubmitted_en + DateTime.Now.ToShortDateString() + NewLine)
            Emailbody.Append(Resources.Resource_mail.ml_RARequest_FirstName_en + request.Customer.FirstName + NewLine)
            Emailbody.Append(Resources.Resource_mail.ml_RARequest_LastName_en + request.Customer.LastName + NewLine)
            Emailbody.Append(Resources.Resource_mail.ml_RARequest_emailaddress_en + request.Customer.EmailAddress + NewLine)
            Emailbody.Append(Resources.Resource_mail.ml_RARequest_companyname_en + request.Customer.CompanyName + NewLine)
            Emailbody.Append(Resources.Resource_mail.ml_RARequest_Address1_en + request.Customer.Address.Line1 + NewLine)
            Emailbody.Append(Resources.Resource_mail.ml_RARequest_Address2_en + NewLine)
            Emailbody.Append(Resources.Resource_mail.ml_RARequest_city_en + request.Customer.Address.City + NewLine)
            Emailbody.Append(Resources.Resource_mail.ml_RARequest_state_en + request.Customer.Address.State + NewLine)
            Emailbody.Append(Resources.Resource_mail.ml_RARequest_zip_en + request.Customer.Address.PostalCode + NewLine)
            Emailbody.Append(Resources.Resource_mail.ml_RARequest_phone_en + request.Customer.PhoneNumber + NewLine)
            Emailbody.Append(Resources.Resource_mail.ml_RARequest_originalordernumber_en + request.OrderNumber + NewLine)
            Emailbody.Append(Resources.Resource_mail.ml_RARequest_originalinvoicenumber_en + request.InvoiceNumber + NewLine)
            Emailbody.Append(Resources.Resource_mail.ml_RARequest_partnumber_en + request.PartNumber + NewLine)
            Emailbody.Append(Resources.Resource_mail.ml_RARequest_price_en + request.Price.ToString() + NewLine)
            Emailbody.Append(Resources.Resource_mail.ml_RARequest_reasonforreturn_en + request.Reason.Description + NewLine)

            If request.Warranty Then
                Emailbody.Append(Resources.Resource_mail.ml_RARequest_warranty_en + "Y" + NewLine)
            Else
                Emailbody.Append(Resources.Resource_mail.ml_RARequest_warranty_en + "N" + NewLine)
            End If

            Emailbody.Append(Resources.Resource_mail.ml_RARequest_serialnumber_en + request.SerialNumber + NewLine)
            Emailbody.Append(Resources.Resource_mail.ml_RARequest_comments_en + request.Comments + NewLine + NewLine)

            ' end of 9062
            Emailbody.Append(Resources.Resource_mail.ml_common_Sincerely_en + NewLine)
            Emailbody.Append(Resources.Resource_mail.ml_common_ServicesPLUSTeam_en + NewLine + NewLine)
            Emailbody.Append(Resources.Resource_mail.ml_common_CopyRights_en + NewLine + NewLine)
            Emailbody.Append(Resources.Resource_mail.ml_common_PrivacyPolicy_en + privacy_policy_link + NewLine)

            If HttpContextManager.GlobalData.SalesOrganization = ConfigurationData.GeneralSettings.GlobalData.SalesOrganization.US Then
                Emailbody.Append(Resources.Resource_mail.ml_common_TC_en + terms_and_conditions_link + NewLine)
                Emailbody.Append(Resources.Resource_mail.ml_common_ContactUs_en + contact_us_link + NewLine)
                Emailbody.Append(Resources.Resource_mail.ml_common_CustomerService_en + customer_service_link + NewLine)
            Else
                Emailbody.Append(Resources.Resource_mail.ml_common_TC_en + terms_and_conditions_link_ca + NewLine)
                Emailbody.Append(Resources.Resource_mail.ml_common_ContactUs_en + contact_us_link_ca + NewLine)
                Emailbody.Append(Resources.Resource_mail.ml_common_CustomerService_en + customer_service_link_ca + NewLine)

            End If

            If HttpContextManager.GlobalData.SalesOrganization = ConfigurationData.GeneralSettings.GlobalData.SalesOrganization.CA Then


                Emailbody.Append("--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------" + NewLine + NewLine)

                Emailbody.Append(Resources.Resource_mail.ml_common_Dear_fr + request.Customer.FirstName + " " + request.Customer.LastName + "," + NewLine + NewLine)
                ' EmailBody.Append("Your RA request form has been received by our customer service group.  A customer ")
                Emailbody.Append(Resources.Resource_mail.ml_RARequestcon_body_line1_fr)
                Emailbody.Append(Resources.Resource_mail.ml_RARequestcon_body_line2_fr + NewLine + NewLine)

                Emailbody.Append(Resources.Resource_mail.ml_RARequest_Datesubmitted_fr + DateTime.Now.ToShortDateString() + NewLine)
                Emailbody.Append(Resources.Resource_mail.ml_RARequest_FirstName_fr + request.Customer.FirstName + NewLine)
                Emailbody.Append(Resources.Resource_mail.ml_RARequest_LastName_fr + request.Customer.LastName + NewLine)
                Emailbody.Append(Resources.Resource_mail.ml_RARequest_emailaddress_fr + request.Customer.EmailAddress + NewLine)
                Emailbody.Append(Resources.Resource_mail.ml_RARequest_companyname_fr + request.Customer.CompanyName + NewLine)
                Emailbody.Append(Resources.Resource_mail.ml_RARequest_Address1_fr + request.Customer.Address.Line1 + NewLine)
                Emailbody.Append(Resources.Resource_mail.ml_RARequest_Address2_fr + NewLine)
                Emailbody.Append(Resources.Resource_mail.ml_RARequest_city_fr + request.Customer.Address.City + NewLine)
                Emailbody.Append(Resources.Resource_mail.ml_RARequest_state_fr + request.Customer.Address.State + NewLine)
                Emailbody.Append(Resources.Resource_mail.ml_RARequest_zip_fr + request.Customer.Address.PostalCode + NewLine)
                Emailbody.Append(Resources.Resource_mail.ml_RARequest_phone_fr + request.Customer.PhoneNumber + NewLine)
                Emailbody.Append(Resources.Resource_mail.ml_RARequest_originalordernumber_fr + request.OrderNumber + NewLine)
                Emailbody.Append(Resources.Resource_mail.ml_RARequest_originalinvoicenumber_fr + request.InvoiceNumber + NewLine)
                Emailbody.Append(Resources.Resource_mail.ml_RARequest_partnumber_fr + request.PartNumber + NewLine)
                Emailbody.Append(Resources.Resource_mail.ml_RARequest_price_fr + request.Price.ToString() + NewLine)
                Emailbody.Append(Resources.Resource_mail.ml_RARequest_reasonforreturn_fr + request.Reason.Description + NewLine)

                If request.Warranty Then
                    Emailbody.Append(Resources.Resource_mail.ml_RARequest_warranty_fr + "Y" + NewLine)
                Else
                    Emailbody.Append(Resources.Resource_mail.ml_RARequest_warranty_fr + "N" + NewLine)
                End If

                Emailbody.Append(Resources.Resource_mail.ml_RARequest_serialnumber_fr + request.SerialNumber + NewLine)
                Emailbody.Append(Resources.Resource_mail.ml_RARequest_comments_fr + request.Comments + NewLine + NewLine)

                ' end of 9062
                Emailbody.Append(Resources.Resource_mail.ml_common_sincerely_fr + NewLine)
                Emailbody.Append(Resources.Resource_mail.ml_common_ServicesPLUSTeam_fr + NewLine + NewLine)
                Emailbody.Append(Resources.Resource_mail.ml_common_CopyRights_fr + NewLine + NewLine)
                Emailbody.Append(Resources.Resource_mail.ml_common_PrivacyPolicy_fr + privacy_policy_link + NewLine)
                Emailbody.Append(Resources.Resource_mail.ml_common_TC_fr + terms_and_conditions_link_ca + NewLine)
                Emailbody.Append(Resources.Resource_mail.ml_common_ContactUs_fr + contact_us_link_ca + NewLine)
                Emailbody.Append(Resources.Resource_mail.ml_common_CustomerService_fr + customer_service_link_ca + NewLine)


                Subject = Resources.Resource_mail.ml_RARequest_subject_en + "/" + Resources.Resource_mail.ml_RARequest_subject_fr
            Else
                Subject = Resources.Resource_mail.ml_RARequest_subject_en
            End If


        End Sub

    End Class

End Namespace
