<%@ Page Language="vb" AutoEventWireup="false" Inherits="ServicePLUSWebApp.ProductRegistrationWelcome"
    CodeFile="sony-service-agreements.aspx.vb" CodeFileBaseClass="ServicePLUSWebApp.SSL" %>

<%@ Register TagPrefix="ServicePLUSWebApp" TagName="TopSonyHeader" Src="TopSonyHeader.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="ServicePlusNav" Src="ServicePlusNav.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="SonyFooter" Src="SonyFooter.ascx" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="<%=PageLanguage %>">
<head>
    <title>
        <%=Resources.Resource.ttl_ProfesstionalProduct%></title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link type="text/css" href="includes/ServicesPLUS_style.css" rel="stylesheet">
    <script type="text/javascript" src="includes/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="includes/svcplus_globalization.js"></script>
    <script type="text/javascript" src="includes/ServicesPLUS.js"></script>
</head>
<body style="color: #000000; background: #5d7180; margin: 0px;">
    <form id="form1" method="post" runat="server">
        <center>
            <table width="710" border="0" align="center" role="presentation">
                <tr valign="top">
                    <td style="width: 25px; background: url('images/sp_left_bkgd.gif');">
                        <img height="25" src="images/spacer.gif" width="25" alt="">
                    </td>
                    <td style="width: 689px; background: #ffffff;">
                        <table width="710" border="0" role="presentation">
                            <tr>
                                <td>
                                    <ServicePLUSWebApp:TopSonyHeader ID="TopSonyHeader" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <nav><ServicePLUSWebApp:ServicePlusNav ID="SonyNav" runat="server" /></nav>
                                    <asp:Label ID="Label1" runat="server" CssClass="redAsterick"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 710px; vertical-align: middle;">
                                    <table width="708" border="0" role="presentation">
                                        <tr style="height: 82px;">
                                            <td style="width: 464px; background: #363d45 url('images/sp_int_header_top_PartsPLUS-ServiceAgreements.gif'); vertical-align: middle; text-align: right;">
                                                <h1 class="headerText"><%=Resources.Resource.hplnk_svc_agreement%> &nbsp;</h1>
                                            </td>
                                            <td valign="middle" bgcolor="#363d45">
                                            </td>
                                        </tr>
                                        <tr style="height: 16px;">
                                            <td style="width: 464px; background: #f2f5f8">
                                                <img src="images/spacer.gif" height="16" alt="">
                                            </td>
                                            <td style="width: 246px; background: #99a8b5">
                                                <img src="images/spacer.gif" height="16" alt="">
                                            </td>
                                        </tr>
                                        <tr style="height: 9px;">
                                            <td style="width: 464px; background: #f2f5f8">
                                                <img style="height: 9px; width: 464px;" src="images/sp_int_header_btm_left_onepix.gif" alt="">
                                            </td>
                                            <td style="width: 246px; background: #99a8b5">
                                                <img style="height: 9px; width: 246px;" src="images/sp_int_header_btm_right.gif" alt="">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td width="710">
                                    <img src="images/spacer.gif" width="20" height="20" alt="">
                                    <table border="0" role="presentation">
                                        <tr>
                                            <td width="20">
                                                <img src="images/spacer.gif" width="20" height="20" alt="">
                                            </td>
                                            <td align="left" colspan="2" valign="top">
                                                <asp:Label ID="lblError" runat="server" CssClass="redAsterick" />
                                                <%-- Pre-Reseller Agreement Changes - 2018-08-13

                                                <span class="tableheader">Purchase a SupportNET<sup>SM</sup> or custom Service Agreement for your Sony professional products:</span>
                                                <ul class="bodyCopy">
                                                    <li>Email <a href="mailto:SupportNET@am.sony.com">SupportNET@am.sony.com</a>, or call 877-398-7669.</li>
                                                </ul>
                                                <br />

                                                <img width="666" src="images/sp_navbar_bkgd.jpg" alt="" />                                                            
                                                                
                                                <span class="tableheader"><%=Resources.Resource.el_RegExtnWarranty%></span>
                                                <ul class="bodyCopy">
                                                    <li><%=Resources.Resource.el_RegExtnWarranty_Cnt1%> <br />
                                                        <asp:ImageButton ID="btnWarranty" PostBackUrl="product-registration-reseller.aspx" ImageUrl="images/btn_registerew.jpg"
                                                            AlternateText="Register an Extended Warranty" runat="server" />
                                                    </li>
                                                </ul>
                                                <br />

                                                <span class="tableHeader"><%=Resources.Resource.el_RegExtnWarranty_Cnt7%></span>
                                                <ul>
                                                    <li class="bodyCopy"><%=Resources.Resource.el_RegExtnWarranty_Cnt8%><br />
                                                        &nbsp;&nbsp;<%=Resources.Resource.el_RegExtnWarranty_Cnt9%>  <span class="tableheader">408-352-4160</span>.</li>
                                                </ul>--%>

                                                <span class="tableheader">Registration of SupportNET Service Agreements</span>
                                                <ul class="bodyCopy">
                                                    <li>Enter equipment covered by a SupportNET agreement here for entitlement registration.</li>
                                                    <li>For questions regarding SupportNET agreement registration, Email <a href="mailto:SupportNET@am.sony.com" title="Click to send an email to SupportNET@am.sony.com">SupportNET@am.sony.com</a> or call <a href="tel:+1-877-398-7669" title="Click to call 1-877-398-7669">1-877-398-7669</a>.<br />
                                                        <br />
                                                        <a href="sony-service-agreement-reseller.aspx" title="Visit the Service Agreement entry page."><img src="images/btn_RegisterAgreement.gif" alt="Register Service Agreement" border="0" /></a>
                                                    </li>
                                                </ul>

                                                <img width="666" src="images/sp_navbar_bkgd.jpg" alt="" />

                                                <br />
                                                <br />
                                                <span class="tableheader">Register your Sony Professional Product:</span>
                                                <ul class="bodyCopy">
                                                    <li>Want to register Sony products not covered by an Extended Warranty or SupportNET Agreement?<br />
                                                        <% If isAmerica Then %>
                                                            <a href="https://pages.pro.sony.com/Register_your_product.html" target="_blank" rel="noopener noreferrer" title="Visit the product registration page in a new tab."><img src="images/btn_RegisterProduct.gif" alt="Register Your Product" /></a>
                                                        <% Else %>
                                                            <a href="https://pages.pro.sony.com/Register_your_product_canada.html" target="_blank" rel="noopener noreferrer" title="Visit the product registration page in a new tab."><img src="images/btn_RegisterProduct.gif" alt="Register Your Product" /></a>
                                                        <% End If %>
                                                        <%--<% If isLoggedIn Then %>
                                                        <%-- Pre-Reseller Agreement Changes - 2018-08-13
                                                        <asp:ImageButton ID="btnProduct" PostBackUrl="Online-Product-Registration.aspx" ImageUrl="images/btn_regprod.jpg"
                                                            AlternateText="Register Your Product" runat="server" /> --%
                                                        <br />
                                                        <asp:ImageButton ID="btnProduct" PostBackUrl="Online-Product-Registration.aspx" ImageUrl="images/btn_RegisterProduct.gif"
                                                            AlternateText="Register Your Product" runat="server" />
                                                    <% Else %>
                                                        <br />
                                                        <span class="bodycopy" style="line-height: normal">
                                                            <span class="tableheader">You must be a ServicesPLUS user to register your Product.</span><br />
                                                            <%=Resources.Resource.el_SigupNotRegister%> <a href="Register-Account-Tax-Exempt.aspx"><%=Resources.Resource.help_cnt_toview_msg_2%></a>.</span><br />
                                                        <br />
                                                        <span class="tableheader"><a href="#" onclick="window.open('sony-service-agreement-info.aspx','','toolbar=0,location=0,top=0,left=0,directories=0,status=0,menubar=0,scrollbars=no,resize=no,width=400,height=475'); return false;">
                                                            <%=Resources.Resource.el_YShouldI%> </a></span>
                                                        <br />
                                                    <% End If %>--%>
                                                    </li>
                                                </ul>
                                            </td>
                                            <td width="15" height="20">
                                                <img src="images/spacer.gif" width="15" alt="">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <img src="images/spacer.gif" width="10" height="20" alt="">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <footer><ServicePLUSWebApp:SonyFooter ID="SonyFooter1" runat="server" /></footer>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="25" style="background: url('images/sp_right_bkgd.gif')">
                        <img src="images/spacer.gif" width="25" height="20" alt="">
                    </td>
                </tr>
            </table>
        </center>
        <a name="identifier">&nbsp;</a>
    </form>
</body>
</html>
