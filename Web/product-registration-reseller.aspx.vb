﻿Imports ServicePLUSWebApp
Imports ServicesPlusException
Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.siam
Imports Sony.US.SIAMUtilities

Partial Class product_registration_reseller
    Inherits SSL

    Public focusFormField As String = String.Empty
    Public bColor As Boolean = False

#Region "EVENTS"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As EventArgs) Handles MyBase.Load
        Try
            If Not Page.IsPostBack Then
                BuildStateDropDown()
                ZipCodeConfig()

                If Session("RegisterProduct") IsNot Nothing Then
                    Dim objProductRegistration = CType(Session("RegisterProduct"), ProductRegistration)
                    If objProductRegistration.Completed Then
                        Session.Remove("RegisterProduct")
                        FillCustomerFields()
                        'Response.Redirect("sony-service-agreements.aspx", True)
                    Else
                        FillRegistrationInfo(objProductRegistration)
                    End If
                ElseIf HttpContextManager.Customer IsNot Nothing Then
                    FillCustomerFields()
                End If
                trAgreement.Visible = HttpContextManager.Customer Is Nothing
            End If
        Catch ex As Exception
            errorMessageLabel.Text = Utilities.WrapExceptionforUI(ex)
            errorMessageLabel.Visible = True
        End Try

    End Sub

    Protected Sub imgNext_Click(ByVal sender As Object, ByVal e As ImageClickEventArgs) Handles imgNext.Click
        Try
            If Not DataValidate() Then Return
            Dim prodReg As New ProductRegistration
            If Session("RegisterProduct") IsNot Nothing Then
                prodReg = CType(Session("RegisterProduct"), ProductRegistration)
            End If
            prodReg.CustomerID = HttpContextManager.Customer.CustomerID
            prodReg.CustomerSequenceNumber = HttpContextManager.Customer.SequenceNumber

            If Not String.IsNullOrWhiteSpace(txtDealer.Text) Then
                prodReg.Dealer = $"Sony AM:{txtDealer.Text.Trim()}"
            End If

            prodReg.CustomerAddress.FirstName = FirstName.Text.Trim()
            prodReg.CustomerAddress.LastName = LastName.Text.Trim()
            prodReg.CustomerAddress.EmailAddress = Email.Text.Trim()
            prodReg.CustomerAddress.Company = CompanyName.Text.Trim()
            prodReg.CustomerAddress.Address1 = Address1.Text.Trim()
            prodReg.CustomerAddress.Address2 = Address2.Text.Trim()
            prodReg.CustomerAddress.Address3 = Address3.Text.Trim()
            prodReg.CustomerAddress.City = City.Text.Trim()
            prodReg.CustomerAddress.State = ddlCustomerInfoState.Text.Trim()
            prodReg.CustomerAddress.ZipCode = Zip.Text.Trim()
            prodReg.CustomerAddress.Phone = Phone.Text.Trim()
            prodReg.CustomerAddress.PhoneExtn = Ext.Text.Trim()
            prodReg.CustomerAddress.Fax = Fax.Text.Trim()

            prodReg.LocationAddress.FirstName = txtLocationFirstName.Text.Trim()
            prodReg.LocationAddress.LastName = txtLocationLastName.Text.Trim()
            prodReg.LocationAddress.EmailAddress = txtLocationEmailAddreess.Text.Trim()
            prodReg.LocationAddress.Company = txtLocationCompanyName.Text.Trim()
            prodReg.LocationAddress.Address1 = txtLocationAddress1.Text.Trim()
            prodReg.LocationAddress.Address2 = txtLocationAddress2.Text.Trim()
            prodReg.LocationAddress.Address3 = txtLocationAddress3.Text.Trim()
            prodReg.LocationAddress.City = txtLocationCity.Text.Trim()
            prodReg.LocationAddress.State = ddlLocationState.Text.Trim()
            prodReg.LocationAddress.ZipCode = txtLocationZip.Text.Trim()
            prodReg.LocationAddress.Phone = txtLocationPhone.Text.Trim()
            prodReg.LocationAddress.PhoneExtn = txtLocationExtension.Text.Trim()
            prodReg.LocationAddress.Fax = txtLocationFax.Text.Trim()
            prodReg.OptIN = optin.Checked
            If HttpContextManager.Customer IsNot Nothing Then
                prodReg.IsAccountHolder = (HttpContextManager.Customer.UserType = "A")
            End If
            Session("RegisterProduct") = prodReg
            Response.Redirect("product-registration.aspx", False)
        Catch ex As Exception
            errorMessageLabel.Text = Utilities.WrapExceptionforUI(ex)
            errorMessageLabel.Visible = True
        End Try
    End Sub
#End Region

#Region "Validation"
    Private Function DataValidate() As Boolean
        ErrorValidation.Text = ""
        ErrorValidation.Visible = False
        lblLocationError.Visible = False
        ErrorDealer.Visible = False

        lblCIFirstName.CssClass = "bodyCopy"
        lblCILastName.CssClass = "bodyCopy"
        lblCIEmail.CssClass = "bodyCopy"
        lblCICompanyName.CssClass = "bodyCopy"
        lblCIAddress1.CssClass = "bodyCopy"
        lblCIAddress2.CssClass = "bodyCopy"
        lblCIAddress3.CssClass = "bodyCopy"
        lblCICity.CssClass = "bodyCopy"
        lblCIState.CssClass = "bodyCopy"
        lblCIZip.CssClass = "bodyCopy"
        lblCIPhone.CssClass = "bodyCopy"
        lblDealer.CssClass = "bodyCopy"

        LabelLineCI1Star.Text = ""
        LabelLineCI2Star.Text = ""
        LabelLineCI3Star.Text = ""
        LabelLineCI4Star.Text = ""

        LabelLinePL1Star.Text = ""
        LabelLinePL2Star.Text = ""
        LabelLinePL3Star.Text = ""
        LabelLinePL4Star.Text = ""

        If String.IsNullOrWhiteSpace(FirstName.Text) Then
            lblCIFirstName.CssClass = "redAsterick"
            ErrorValidation.Text = Resources.Resource.el_InvalidFirstName '"Invalid first name."
            ErrorValidation.Visible = True
            FirstName.Focus()
        End If
        If String.IsNullOrWhiteSpace(LastName.Text) Then
            lblCILastName.CssClass = "redAsterick"
            If Not ErrorValidation.Visible Then
                ErrorValidation.Visible = True
                ErrorValidation.Text = Resources.Resource.el_InvalidLastName '"Invalid last name."
                LastName.Focus()
            End If
        End If

        If String.IsNullOrWhiteSpace(Email.Text) Then
            lblCIEmail.CssClass = "redAsterick"
            If Not ErrorValidation.Visible Then
                ErrorValidation.Visible = True
                ErrorValidation.Text = Resources.Resource.el_InvalidEmail '"You have entered an invalid email address."
                Email.Focus()
            End If
        End If
        If New SecurityAdministrator().IsValidEmail(Email.Text) = False Then
            lblCIEmail.CssClass = "redAsterick"
            If Not ErrorValidation.Visible Then
                ErrorValidation.Visible = True
                ErrorValidation.Text = Resources.Resource.el_InvalidEmail '"You have entered an invalid email address."
                Email.Focus()
            End If
        End If


        If String.IsNullOrWhiteSpace(CompanyName.Text) Then
            lblCICompanyName.CssClass = "redAsterick"
            If Not ErrorValidation.Visible Then
                ErrorValidation.Visible = True
                ErrorValidation.Text = Resources.Resource.el_InvalidCompanyName '"Invalid company name."
                CompanyName.Focus()
            End If
        End If

        If String.IsNullOrWhiteSpace(Address1.Text) Then
            lblCIAddress1.CssClass = "redAsterick"
            If Not ErrorValidation.Visible Then
                ErrorValidation.Visible = True
                ErrorValidation.Text = Resources.Resource.el_InvalidAdd1 '"Invalid Address Line 1."
                Address1.Focus()
            End If
        ElseIf Address1.Text.Length > 35 Then
            lblCIAddress1.CssClass = "redAsterick"
            If Not ErrorValidation.Visible Then
                ErrorValidation.Visible = True
                LabelLineCI1Star.Text = Resources.Resource.el_ValidLength_StreetAddress ' "Street Address must be 35 characters or less in length."
                Address1.Focus()
            End If
        End If


        If Address2.Text.Length > 35 Then
            lblCIAddress2.CssClass = "redAsterick"
            If Not ErrorValidation.Visible Then
                ErrorValidation.Visible = True
                LabelLineCI2Star.Text = Resources.Resource.el_ValidLength_Address2 '"Address 2nd Line must be 35 characters or less in length."
                Address2.Focus()
            End If
        End If

        If Address3.Text.Length > 35 Then
            lblCIAddress3.CssClass = "redAsterick"
            If Not ErrorValidation.Visible Then
                ErrorValidation.Visible = True
                LabelLineCI3Star.Text = Resources.Resource.el_ValidLength_Address3 '"Address 3rd Line must be 35 characters or less in length."
                Address3.Focus()
            End If
        End If

        If String.IsNullOrWhiteSpace(City.Text) Then
            If Not ErrorValidation.Visible Then
                ErrorValidation.Visible = True
                ErrorValidation.Text = Resources.Resource.el_InvalidCity ' "Invalid City."
                lblCICity.CssClass = "redAsterick"
                City.Focus()
            End If
        ElseIf City.Text.Length > 35 Then
            lblCICity.CssClass = "redAsterick"
            If Not ErrorValidation.Visible Then
                ErrorValidation.Visible = True
                LabelLineCI4Star.Text = Resources.Resource.el_ValidLength_City '"City must be 35 characters or less in length."
                City.Focus()
            End If
        End If

        If ddlCustomerInfoState.SelectedIndex = 0 Then
            lblCIState.CssClass = "redAsterick"
            If Not ErrorValidation.Visible Then
                ErrorValidation.Text = Resources.Resource.el_SelectState '"Select State."
                ErrorValidation.Visible = True
                ddlCustomerInfoState.Focus()
            End If
        End If

        If String.IsNullOrWhiteSpace(Zip.Text) Then
            lblCIZip.CssClass = "redAsterick"
            If Not ErrorValidation.Visible Then
                ErrorValidation.Text = Resources.Resource.el_InvalidZip ' "Invalid Zip."
                ErrorValidation.Visible = True
                Zip.Focus()
            End If
        End If

        Me.zipRegularExpressionValidator.Validate()
        If Not Page.IsValid Then
            lblCIZip.CssClass = "redAsterick"
            If Not ErrorValidation.Visible Then
                ErrorValidation.Text = Resources.Resource.el_InvalidZip '"Invalid Zip."
                ErrorValidation.Visible = True
                Zip.Focus()
            End If
        End If

        Dim sPhoneNumber As String = Phone.Text.Trim()
        Dim objHPhoneNumber As New Regex("(\d{3}[-]{1}\d{3}[-]{1}\d{4})")
        If Not objHPhoneNumber.IsMatch(sPhoneNumber) Then
            lblCIPhone.CssClass = "redAsterick"
            If Not ErrorValidation.Visible Then
                ErrorValidation.Text = Resources.Resource.el_InvalidaPhoneNo '"Invalid Phone Number."
                ErrorValidation.Visible = True
                Phone.Focus()
            End If
        End If

        Dim sExtension As String = Ext.Text.Trim.ToString()
        Dim objExt As New Regex("^[0-9]*$")
        If sExtension.Length > 0 Then
            If Not objExt.IsMatch(sExtension) Or sExtension.Length > 6 Then
                If Not ErrorValidation.Visible Then
                    ErrorValidation.Text = Resources.Resource.el_validate_Extn '"Invalid field value for extension. Extension must be numeric and 6 or fewer digits."
                    ErrorValidation.Visible = True
                    Ext.Focus()
                End If
            End If
        End If

        Dim sFaxNumber As String = Fax.Text.Trim()
        If Not sFaxNumber = "" Then
            Dim objFaxNumber As New Regex("(\d{3}[-]{1}\d{3}[-]{1}\d{4})")
            If Not objFaxNumber.IsMatch(sFaxNumber) Then
                If Not ErrorValidation.Visible Then
                    ErrorValidation.Text = Resources.Resource.el_InvalidFax '"Invalid Fax Number."
                    ErrorValidation.Visible = True
                    Fax.Focus()
                End If
            End If
        End If

        lblLIFirstName.CssClass = "bodyCopy"
        lblLILastName.CssClass = "bodyCopy"
        lblLIEmail.CssClass = "bodyCopy"
        lblLICompanyName.CssClass = "bodyCopy"
        lblLIAddress1.CssClass = "bodyCopy"
        lblLIAddress2.CssClass = "bodyCopy"
        lblLIAddress3.CssClass = "bodyCopy"
        lblLICity.CssClass = "bodyCopy"
        lblLIState.CssClass = "bodyCopy"
        lblLIZip.CssClass = "bodyCopy"
        lblLIPhone.CssClass = "bodyCopy"

        If String.IsNullOrWhiteSpace(txtLocationFirstName.Text) Then
            lblLocationError.Visible = True
            lblLIFirstName.CssClass = "redAsterick"
            lblLocationError.Text = Resources.Resource.el_InvalidFirstName '"Invalid first name."
            If Not ErrorValidation.Visible Then txtLocationFirstName.Focus()
        End If
        If String.IsNullOrWhiteSpace(txtLocationLastName.Text) Then
            lblLILastName.CssClass = "redAsterick"
            If Not lblLocationError.Visible Then
                lblLocationError.Text = Resources.Resource.el_InvalidLastName '"Invalid last name."
                lblLocationError.Visible = True
                If Not ErrorValidation.Visible Then txtLocationLastName.Focus()
            End If
        End If

        If String.IsNullOrWhiteSpace(txtLocationEmailAddreess.Text) Then
            lblLIEmail.CssClass = "redAsterick"
            If Not lblLocationError.Visible Then
                lblLocationError.Text = Resources.Resource.el_InvalidEmail ' "You have entered an invalid email address."
                lblLocationError.Visible = True
                If Not ErrorValidation.Visible Then txtLocationEmailAddreess.Focus()
            End If
        End If
        If New SecurityAdministrator().IsValidEmail(txtLocationEmailAddreess.Text) = False Then
            lblLIEmail.CssClass = "redAsterick"
            If Not lblLocationError.Visible Then
                lblLocationError.Text = Resources.Resource.el_InvalidEmail ' "You have entered an invalid email address."
                lblLocationError.Visible = True
                If Not ErrorValidation.Visible Then txtLocationEmailAddreess.Focus()
            End If
        End If



        If String.IsNullOrWhiteSpace(txtLocationCompanyName.Text) Then
            lblLICompanyName.CssClass = "redAsterick"
            If Not lblLocationError.Visible Then
                lblLocationError.Text = Resources.Resource.el_InvalidCompanyName ' "Invalid Company Name."
                lblLocationError.Visible = True
                If Not ErrorValidation.Visible Then txtLocationCompanyName.Focus()
            End If
        End If

        If String.IsNullOrWhiteSpace(txtLocationAddress1.Text) Then
            lblLIAddress1.CssClass = "redAsterick"
            If Not lblLocationError.Visible Then
                lblLocationError.Text = Resources.Resource.el_InvalidAdd1 ' "Invalid Address Line 1."
                lblLocationError.Visible = True
                If Not ErrorValidation.Visible Then txtLocationAddress1.Focus()
            End If
        End If

        If txtLocationAddress1.Text.Length > 35 Then
            lblLIAddress1.CssClass = "redAsterick"
            If Not lblLocationError.Visible Then
                LabelLinePL1Star.Text = Resources.Resource.el_ValidLength_StreetAddress '"Street Address Line must be 35 characters or less in length."
                lblLocationError.Visible = True
                If Not ErrorValidation.Visible Then txtLocationAddress1.Focus()
            End If
        End If

        If txtLocationAddress2.Text.Length > 35 Then
            lblLIAddress2.CssClass = "redAsterick"
            If Not lblLocationError.Visible Then
                LabelLinePL2Star.Text = Resources.Resource.el_ValidLength_Address2 '"Address 2nd Line must be 35 characters or less in length."
                lblLocationError.Visible = True
                If Not ErrorValidation.Visible Then txtLocationAddress2.Focus()
            End If
        End If

        If txtLocationAddress3.Text.Length > 35 Then
            lblLIAddress3.CssClass = "redAsterick"
            If Not lblLocationError.Visible Then
                LabelLinePL3Star.Text = Resources.Resource.el_ValidLength_Address3 ' "Address 3rd Line must be 35 characters or less in length."
                lblLocationError.Visible = True
                If Not ErrorValidation.Visible Then txtLocationAddress3.Focus()
            End If
        End If

        If String.IsNullOrWhiteSpace(txtLocationCity.Text) Then
            lblLICity.CssClass = "redAsterick"
            If Not lblLocationError.Visible Then
                lblLocationError.Text = Resources.Resource.el_InvalidCity ' "Invalid City."
                lblLocationError.Visible = True
                If Not ErrorValidation.Visible Then txtLocationCity.Focus()
            End If
        End If

        If txtLocationCity.Text.Length > 35 Then
            lblLICity.CssClass = "redAsterick"
            If Not lblLocationError.Visible Then
                lblLocationError.Text = Resources.Resource.el_ValidLength_City '"City must be 35 characters or less in length."
                lblLocationError.Visible = True
                If Not ErrorValidation.Visible Then txtLocationCity.Focus()
            End If
        End If

        If ddlLocationState.SelectedIndex = 0 Then
            lblLIState.CssClass = "redAsterick"
            If Not lblLocationError.Visible Then
                lblLocationError.Text = Resources.Resource.el_SelectState '"Select State."
                lblLocationError.Visible = True
                If Not ErrorValidation.Visible Then ddlLocationState.Focus()
            End If
        End If

        If String.IsNullOrWhiteSpace(txtLocationZip.Text) Then
            lblLIZip.CssClass = "redAsterick"
            If Not lblLocationError.Visible Then
                lblLocationError.Text = Resources.Resource.el_InvalidZip '"Invalid Zip."
                lblLocationError.Visible = True
                If Not ErrorValidation.Visible Then txtLocationZip.Focus()
            End If
        End If

        Me.zipLocationRegularExpressionValidator.Validate()
        If Not Page.IsValid Then
            lblLIZip.CssClass = "redAsterick"
            If Not lblLocationError.Visible Then
                lblLocationError.Text = Resources.Resource.el_InvalidZip ' "Invalid Zip."
                lblLocationError.Visible = True
                If Not ErrorValidation.Visible Then txtLocationZip.Focus()
            End If
        End If

        sPhoneNumber = txtLocationPhone.Text.Trim()
        If Not objHPhoneNumber.IsMatch(sPhoneNumber) Then
            lblLIPhone.CssClass = "redAsterick"
            If Not lblLocationError.Visible Then
                lblLocationError.Text = Resources.Resource.el_InvalidaPhoneNo ' "Invalid Phone Number."
                lblLocationError.Visible = True
                If Not ErrorValidation.Visible Then txtLocationPhone.Focus()
            End If
        End If

        sExtension = txtLocationExtension.Text.Trim.ToString()
        If sExtension.Length > 0 Then
            If Not objExt.IsMatch(sExtension) Or sExtension.Length > 6 Then
                If Not lblLocationError.Visible Then
                    lblLocationError.Text = Resources.Resource.el_validate_Extn '"Invalid field value for extension. Extension must be numeric and 6 or fewer digits."
                    lblLocationError.Visible = True
                    If Not ErrorValidation.Visible Then txtLocationExtension.Focus()
                End If
            End If
        End If

        sFaxNumber = txtLocationFax.Text.Trim()
        If Not sFaxNumber = "" Then
            Dim objFaxNumber As New Regex("(\d{3}[-]{1}\d{3}[-]{1}\d{4})")
            If Not objFaxNumber.IsMatch(sFaxNumber) Then
                If Not lblLocationError.Visible Then
                    lblLocationError.Text = Resources.Resource.el_InvalidFax '"Invalid Fax Number."
                    lblLocationError.Visible = True
                    If Not ErrorValidation.Visible Then txtLocationFax.Focus()
                End If
            End If
        End If

        If ErrorValidation.Visible Or lblLocationError.Visible Or ErrorDealer.Visible Then
            Return False
        Else
            Return True
        End If

    End Function
#End Region

#Region "Private Methods"

    Private Sub BuildStateDropDown()
        Dim defaultItem As New ListItem(Resources.Resource.el_rgn_Select, "", True) ' "Select One"

        ddlCustomerInfoState.Items.Insert(0, defaultItem)
        PopulateStateDropdownList(ddlCustomerInfoState)

        ddlLocationState.Items.Insert(0, defaultItem)
        PopulateStateDropdownList(ddlLocationState)
    End Sub

    Private Sub ZipCodeConfig()
        Dim intLength As Int32
        Dim strValidate As String = String.Empty

        If HttpContextManager.GlobalData.IsAmerica Then
            lblCIZip.Text = "Zip Code"
            lblLIZip.Text = "Zip Code"
            intLength = Convert.ToInt32(ConfigurationData.GeneralSettings.GlobalData.ZipCode.US)
            strValidate = "\d{" + intLength.ToString() + "}(-\d{4})?$"
        Else
            lblCIZip.Text = "Postal Code"
            lblLIZip.Text = "Postal Code"
            intLength = Convert.ToInt32(ConfigurationData.GeneralSettings.GlobalData.ZipCode.CA)
            strValidate = "^[ABCEGHJKLMNPRSTVXY]{1}\d{1}[A-Z]{1} *\d{1}[A-Z]{1}\d{1}$"
        End If
        Zip.Text = ""
        Zip.MaxLength = intLength
        txtLocationZip.Text = ""
        txtLocationZip.MaxLength = intLength

        zipRegularExpressionValidator.ValidationExpression = strValidate
        zipRegularExpressionValidator.ErrorMessage = "</br>Invalid " + lblCIZip.Text + "."
        zipLocationRegularExpressionValidator.ValidationExpression = strValidate
        zipRegularExpressionValidator.ErrorMessage = "</br>Invalid " + lblCIZip.Text + "."

        'Upper case - Zip code 
        Zip.Attributes.Add("onblur", "this.value = this.value.toUpperCase()")
        txtLocationZip.Attributes.Add("onblur", "this.value = this.value.toUpperCase()")
    End Sub

    Private Sub FillCustomerFields()
        If HttpContextManager.Customer Is Nothing Then Return
        Dim cust As Customer = HttpContextManager.Customer
        FirstName.Text = cust.FirstName
        LastName.Text = cust.LastName
        Email.Text = cust.EmailAddress
        CompanyName.Text = cust.CompanyName
        Address1.Text = cust.Address.Address1
        Address2.Text = cust.Address.Address2
        Address3.Text = cust.Address.Address3
        City.Text = cust.Address.City
        ddlCustomerInfoState.Text = cust.Address.State
        Zip.Text = cust.Address.Zip
        Phone.Text = cust.PhoneNumber
        Ext.Text = cust.PhoneExtension
        Fax.Text = cust.FaxNumber
    End Sub

    Private Sub FillRegistrationInfo(ByVal prodReg As ProductRegistration)
        FirstName.Text = prodReg.CustomerAddress.FirstName
        LastName.Text = prodReg.CustomerAddress.LastName
        Email.Text = prodReg.CustomerAddress.EmailAddress
        CompanyName.Text = prodReg.CustomerAddress.Company
        Address1.Text = prodReg.CustomerAddress.Address1
        Address2.Text = prodReg.CustomerAddress.Address2
        Address3.Text = prodReg.CustomerAddress.Address3
        City.Text = prodReg.CustomerAddress.City
        ddlCustomerInfoState.Text = prodReg.CustomerAddress.State
        Zip.Text = prodReg.CustomerAddress.ZipCode
        Phone.Text = prodReg.CustomerAddress.Phone
        Ext.Text = prodReg.CustomerAddress.PhoneExtn
        Fax.Text = prodReg.CustomerAddress.Fax

        txtLocationFirstName.Text = prodReg.LocationAddress.FirstName
        txtLocationLastName.Text = prodReg.LocationAddress.LastName
        txtLocationEmailAddreess.Text = prodReg.LocationAddress.EmailAddress
        txtLocationCompanyName.Text = prodReg.LocationAddress.Company
        txtLocationAddress1.Text = prodReg.LocationAddress.Address1
        txtLocationAddress2.Text = prodReg.LocationAddress.Address2
        txtLocationAddress3.Text = prodReg.LocationAddress.Address3
        txtLocationCity.Text = prodReg.LocationAddress.City
        ddlLocationState.Text = prodReg.LocationAddress.State
        txtLocationZip.Text = prodReg.LocationAddress.ZipCode
        txtLocationPhone.Text = prodReg.LocationAddress.Phone
        txtLocationExtension.Text = prodReg.LocationAddress.PhoneExtn
        txtLocationFax.Text = prodReg.LocationAddress.Fax

        txtDealer.Text = prodReg.Dealer
        optin.Checked = prodReg.OptIN
    End Sub
#End Region
End Class
