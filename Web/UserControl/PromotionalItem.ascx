<%@ Control Language="VB" AutoEventWireup="false" CodeFile="PromotionalItem.ascx.vb"
    Inherits="UserControl_PromotionalItem" %>
<link href="../includes/ServicesPLUS.js" type="text/css" rel="stylesheet"></link>
<table id="Table2" width="100%" border="0" role="presentation">
    <tr valign="top">
        <td width="582" height="">
            <%--6449 V1--%>
            <td width="582">
                <asp:PlaceHolder ID="phPromotionInserts" runat="server"></asp:PlaceHolder>
                <table height="400" width="713" border="0" style="vertical-align: top" role="presentation">
                    <tr>
                        <td width="20" height="150">
                            <img src="images/spacer.gif" width="20">
                        </td>
                        <td width="670" height="150" valign="top">
                            <a name="promo3"></a>
                            <p>
                                <span class="promoCopyBlue"><a name="Promotion">Exclusive Internet-only&nbsp;Discounts
                                    for ServicesPLUS purchases only<br>
                                </a></span>
                            </p>
                            <%--<p><a href="#promo1"><span class="promoCopyBold">Promotion #1: Save Up to 25% on Select Sony Parts.</span></a></p>
													<p><a href="#promo2"><span class="promoCopyBold">Promotion #2: Maximum Uptime Program</span></a></p>--%>
                            <p>
                                <a name="promo1" class="promoCopyBlue">Save Up to 25% on Sony Parts:</a>
                            </p>
                            <p>
                                <span class="bodyCopy">
                                    <asp:DataGrid ID="internetPromoDataGrid" runat="server" AutoGenerateColumns="False"
                                        GridLines="None" CellSpacing="2" CellPadding="1">
                                        <AlternatingItemStyle BackColor="#F2F5F8"></AlternatingItemStyle>
                                        <ItemStyle CssClass="tableData" VerticalAlign="Middle"></ItemStyle>
                                        <HeaderStyle CssClass="tableHeader" BackColor="#D5DEE9" HorizontalAlign="Left" VerticalAlign="Middle">
                                        </HeaderStyle>
                                        <Columns>
                                            <asp:TemplateColumn HeaderText="Model">
                                                <HeaderStyle HorizontalAlign="Left" Width="66px"></HeaderStyle>
                                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                <ItemTemplate>
                                                    <asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ModelNumber").ToString() %>'
                                                        ID="Label2">
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Part Number">
                                                <HeaderStyle HorizontalAlign="Left" Width="76px"></HeaderStyle>
                                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                <ItemTemplate>
                                                    <asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PartNumber").ToString() %>'
                                                        ID="Label1">
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Description">
                                                <HeaderStyle HorizontalAlign="Left" Width="120px"></HeaderStyle>
                                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                <ItemTemplate>
                                                    <asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Description").ToString() %>'
                                                        ID="Label6">
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="List Price">
                                                <HeaderStyle HorizontalAlign="Left" Width="87px"></HeaderStyle>
                                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                <ItemTemplate>
                                                    <asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ListPrice", "{0:c}").ToString() %>'
                                                        ID="Label3">
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Discounted Price">
                                                <HeaderStyle HorizontalAlign="Left" Width="87px"></HeaderStyle>
                                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                <ItemTemplate>
                                                    <asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.InternetPrice","{0:c}").ToString() %> '
                                                        ID="Label4">
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:BoundColumn HeaderText="% Discount" DataField="DiscountPercentage" DataFormatString="{0:0}">
                                                <ItemStyle HorizontalAlign="Left" />
                                                <HeaderStyle HorizontalAlign="Left" />
                                            </asp:BoundColumn>
                                            <asp:TemplateColumn HeaderText="Add Cart">
                                                <ItemTemplate>
                                                    <asp:HyperLink ID="lnkAddToCart" runat="server">
                                                        <asp:Image ID="imgAddCart" runat="server" ImageUrl="../images/sp_int_add2Cart_btn.gif" /></asp:HyperLink>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                        </Columns>
                                    </asp:DataGrid>
                                </span>
                            </p>
                            <p>
                                <span class="bodyCopy">Prices are subject to change without notice.</span>
                            </p>
                            <%--6994 V1--%>
                        </td>
                        <td width="20" height="237">
                        </td>
                    </tr>
                    <tr>
                        <td height="22">
                            <img height="20" src="images/spacer.gif" width="20">
                        </td>
                        <td colspan="2" height="22">
                        </td>
                    </tr>
                    <%--6994 V1 Starts--%>
                    <%--6994 V1--%>
                    <%--Bug 9063, Customer wants to hide the information about kit--%>
                    <%--<tr>
                        <td width="20">
                            <img height="20" src="images/spacer.gif" width="20">
                        </td>
                        <td width="670">
                            <a name="promo2"></a>
                            <p>
                                <span class="promoCopyBlue"><a name="promo2">Maximum Uptime Program -- Save up to 15%
                                    on part kits!</a><br>
                                </span><span class="promoCopyBold">Sony is offering a significant discount on select
                                    VTR and camcorder Major and Minor Overhaul kits!<br>
                                </span>
                            </p>
                            <p>
                                <span class="bodyCopy">To improve the performance and longevity of your revenue generating
                                    assets, Sony has made available Major and Minor Overhaul Parts kits for Betacam
                                    SP�, Digital Betacam� and Betacam SX� VTRs and camcorders at extremely attractive
                                    prices. Under this program:</span></p>
                            <ul>
                                <li><span class="bodyCopy">All parts must be purchased as a complete overhaul kit. (See
                                    chart below.)</span>
                                    <li><span class="bodyCopy">A 15% restock fee will be charged in case of a customer returned
                                        shipment. </span></li>
                                    <li><span class="bodyCopy">This program may not be combined with any other service promotion.</span>
                                    </li>
                            </ul>
                            <p>
                                <span class="bodyCopy">Prices, conditions, specifications and offer are subject to change
                                    without notice.</span>
                            </p>
                            <asp:DataGrid ID="dgKits" runat="server" AutoGenerateColumns="False" GridLines="None"
                                CellSpacing="2" CellPadding="1">
                                <AlternatingItemStyle BackColor="#F2F5F8"></AlternatingItemStyle>
                                <ItemStyle CssClass="tableData" HorizontalAlign="Left" VerticalAlign="Middle"></ItemStyle>
                                <HeaderStyle CssClass="tableHeader" BackColor="#D5DEE9" HorizontalAlign="Left" VerticalAlign="Middle">
                                </HeaderStyle>
                                <Columns>
                                    <asp:TemplateColumn HeaderText="Kit Name">
                                        <HeaderStyle HorizontalAlign="Left" Width="76px"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:HyperLink runat="server" ID="lnkOverhaulKit" Text='<%# DataBinder.Eval(Container, "DataItem.PartNumber").ToString()%>'>                                                                
                                            </asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Your Price">
                                        <ItemStyle HorizontalAlign="Left" />
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PartNumber").ToString() %>'
                                                ID="lblKitPartNumber" Visible="false"> </asp:Label>
                                            <a class="tableHeader" href="#" onclick="mywin=window.open('Availability.aspx?promoKit=<%# DataBinder.Eval(Container, "DataItem.PartNumber").ToString() %>','','toolbar=0,location=0,directories=0,status=0,menubar=0,scrollbars=no,resize=1,width=400,height=335');"
                                                tooltip="View Your Price">Your Price</a>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Add Cart">
                                        <ItemTemplate>
                                            <asp:HyperLink ID="lnkKitAddToCart" runat="server">
                                                <asp:Image ID="imgKitAddCart" runat="server" ImageUrl="../images/sp_int_add2Cart_btn.gif" /></asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Kit Value" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PartNumber").ToString() %>'
                                                ID="lblKitPartNumber" Visible="false"> </asp:Label>
                                            <a class="tableHeader" href="#" onclick="mywin=window.open('yourprice.aspx?promoKit=1','','toolbar=0,location=0,directories=0,status=0,menubar=0,scrollbars=no,resize=1,width=400,height=335');"
                                                tooltip="View Your Price">Your Price</a>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                            </asp:DataGrid>
                            <p>
                                <br>
                                <span class="bodyCopy">. </span>
                            </p>
                        </td>
                        <td width="20" height="865">
                            <img src="images/spacer.gif" width="20">
                        </td>
                    </tr>--%>
                    <%--6994 V1--%>
                    <%--6994 V2 ends--%>
                </table>
            </td>
    </tr>
</table>
