
Imports System
Imports System.IO
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Drawing
Imports System.Web
Imports System.Web.SessionState
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports System.Text
Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Process
Imports Sony.US.ServicesPLUS.Data
Imports ServicesPlusException
Imports Sony.US.SIAMUtilities
Imports Sony.US.AuditLog
Imports Sony.US.siam


Namespace ServicePLUSWebApp



    Partial Class YourPrice
        Inherits SSL

        Protected WithEvents PriceLabel As Label
        Private conPriceUnavailableMsg As String = Resources.Resource.el_Unavailable ' "unavailable"
        Private conNoPriceMessage As String = Resources.Resource.el_Unavailable_Msg '"Requested product not available for shipment. Please contact ServicesPLUS at 1-800-538-7550 and request status on release availability."
        Public customer As Customer

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
            Page.ID = Request.Url.AbsolutePath
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
            If HttpContextManager.GlobalData.IsCanada Then
                lblheadYourPrice.InnerHtml = Resources.Resource.el_YourPrice_CAD + ":"
                lblheadListPrice.InnerHtml = Resources.Resource.el_ListPrice_CAD + ":"
            Else
                lblheadYourPrice.InnerHtml = Resources.Resource.el_YourPrice + ":"
                lblheadListPrice.InnerHtml = Resources.Resource.el_ListPrice + ":"
            End If

            If Not Page.IsPostBack Then ControlMethod(sender, e)
        End Sub

        Private Sub ControlMethod(ByVal sender As Object, ByVal e As EventArgs)
            Try
                If HttpContextManager.Customer IsNot Nothing Then
                    customer = HttpContextManager.Customer
                    If Not String.IsNullOrWhiteSpace(Request.QueryString("softwareItem")) Then  '-- get your price for software -- 
                        ShowSoftwarePrice(customer, GetSoftwareToPrice())   ' GetSoftwareToPrice can return null/nothing. This explicitly causes a Null Reference Exception. Bravo.
                    Else 'get your price for parts
                        GetPartPrice()
                    End If
                Else
                    ErrorLabel.Text = (Resources.Resource.el_NoitemNoProvided) ' "No item number provided."
                End If

            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Sub ShowSoftwarePrice(ByVal thisCustomer As Customer, ByVal software As Software)
            Dim objPCILogger As New PCILogger()
            Try
                'Utilities.LogMessages("YourPrice.ShowSoftwarePrice - START at " + DateTime.Now.ToShortTimeString)
                Dim catMan As New CatalogManager
                Dim promoMan As New PromotionManager
                Dim corePart As New Part()
                Dim IsSpecialCharacter As New Regex(ConfigurationData.GeneralSettings.SpecialCharacter.SpecialCharacterDetail)
                Dim partNumber As String
                'Dim partDesc As String = ""
                Dim availibility As String = ""
                'Dim listPrice As String = ""
                Dim yourPrice As String = ""
                Dim strswlistprice As String = String.Empty
                Dim hasPromo As Boolean = False
                Dim dyourprice As Double = 0
                Dim dlistprice As Double = 0
                Dim recycleflag As Double = 0

                If software Is Nothing Then ' 2018-10-09 ASleight - Added to prevent unhandled Null Reference Exceptions.
                    Throw New ArgumentNullException("software", "YourPrice.ShowSoftwarePrice - Software variable was null. Possible loss of session.")
                End If
                'partDesc = software.KitDescription
                'listPrice = software.ListPrice.ToString("C")
                If Not String.IsNullOrEmpty(software.KitPartNumber) Then
                    partNumber = software.KitPartNumber
                ElseIf Not String.IsNullOrEmpty(software.ReplacementPartNumber) Then
                    partNumber = software.ReplacementPartNumber
                ElseIf Not String.IsNullOrEmpty(software.PartNumber) Then
                    partNumber = software.PartNumber
                Else
                    ErrorLabel.Text = Utilities.WrapExceptionforUI(New Exception("An error occurred while loading the Part Number from the selected software item. Please try again."))
                    Exit Sub
                End If
                'Utilities.LogMessages("YourPrice.ShowSoftwarePrice - Part Number is " + partNumber)

                If IsSpecialCharacter.IsMatch(partNumber) Then
                    ErrorLabel.Text = Resources.Resource.el_partNo_SpecialChar '"Part Number Contains Special Character."
                    Exit Sub
                End If
                yourPrice = GetYourPrice(thisCustomer, software, Product.ProductType.Software, availibility, corePart, dyourprice, dlistprice, HttpContextManager.GlobalData)
                'Utilities.LogMessages("YourPrice.ShowSoftwarePrice - getYourPrice returned value " + yourPrice)

                If promoMan.IsPromotionPart(partNumber) Then
                    lblYourPrice.Text = yourPrice + "<span style=""font-family: Wingdings; color: red; font-size: 12px;"">v</span>"
                    hasPromo = True
                End If
                Me.tdLegend.Visible = hasPromo

                strswlistprice = corePart.ListPrice.ToString("C")

                If customer IsNot Nothing Then
                    'Utilities.LogMessages("YourPrice.ShowSoftwarePrice - Recycle Flag Check")
                    recycleflag = catMan.ISRecyclingFlagCheck(partNumber, thisCustomer, HttpContextManager.GlobalData)
                    'Utilities.LogMessages("YourPrice.ShowSoftwarePrice - Recycle Flag is " + recycleflag.ToString())
                    Me.tdrecycle.Visible = recycleflag > 0
                End If
                'Utilities.LogMessages("YourPrice.ShowSoftwarePrice - Populating Controls")
                PopulateLabelControls(corePart.PartNumber, corePart.Description, strswlistprice, yourPrice, availibility, dlistprice, dyourprice, hasPromo) '7820
                'Utilities.LogMessages("YourPrice.ShowSoftwarePrice - END at " + DateTime.Now.ToShortTimeString)
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                objPCILogger.EventOriginApplication = "ServicesPLUS"
                objPCILogger.EventOriginApplicationLocation = Me.Page.GetType().Name
                objPCILogger.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
                objPCILogger.OperationalUser = Environment.UserName
                objPCILogger.EventDateTime = Date.Now.ToLongTimeString()
                objPCILogger.HTTPRequestObjectValues = objPCILogger.GetRequestContextValue()
                objPCILogger.EventOriginMethod = "showSoftwarePrice"
                objPCILogger.EventType = EventType.View
                objPCILogger.IndicationSuccessFailure = "Failure"
                objPCILogger.Message = "showSoftwarePrice Failed. " & ex.Message.ToString()
                objPCILogger.PushLogToMSMQ()
            End Try
        End Sub

        Private Function GetYourPrice(ByRef customer As Customer, ByRef product As Product, ByVal type As Product.ProductType, ByRef availibility As String, ByRef Corepart As Sony.US.ServicesPLUS.Core.Part, ByRef your_price As Double, ByRef listprice As Double, ByRef objGlobalData As GlobalData) As String 'Sasikumar WP SPLUS_WP008
            Dim yourPrice As String = String.Empty
            Try
                If customer IsNot Nothing AndAlso product IsNot Nothing Then
                    If product.ListPrice = 0.0R Then ErrorLabel.Text = conNoPriceMessage

                    Dim catMng As New CatalogManager
                    Dim dyourprice As Decimal = 0
                    Dim dlistprice As Decimal = 0
                    yourPrice = catMng.YourPrice(product, customer, type, availibility, Corepart, your_price, listprice, objGlobalData).ToString("C")
                    Dim thisSA As New SecurityAdministrator

                    Dim userType As String = String.Empty
                    userType = thisSA.GetUser(customer.SIAMIdentity).TheUser.UserType

                    If userType = "P" Then
                        yourPrice = product.ListPrice.ToString("C")
                        If product.ListPrice > 0 Then ErrorLabel.Text = Resources.Resource.el_Unavailable_Msg '"Your account registration is being verified for usage on ServicesPLUS. If you wish to expedite the verification process, please call 1-800-538-7550. Normal time to verify accounts and authorize ServicesPLUS ordering on account is less than 8 business hours after registration. Until your account is verified, you can only purchase at list price using a credit card. To purchase immediately on account, call 1-800-538-7550."
                    End If
                Else
                    yourPrice = conPriceUnavailableMsg
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                Return "N/A"
            End Try

            Return yourPrice
        End Function

        Private Function GetSoftwareToPrice() As Software
            If Session.Item("software") IsNot Nothing And Request.QueryString("softwareItem") IsNot Nothing Then
                Return CType(Session.Item("software"), ArrayList)(Request.QueryString("softwareItem"))
            Else
                Return Nothing  ' Causing an immediate Null Reference Exception in the method that requests this value.
            End If
        End Function

        Private Sub PopulateLabelControls(ByRef partNumber As String, ByRef partDesc As String, ByRef listPrice As String, ByRef yourPrice As String, ByRef availibility As String, ByRef dlistPrice As Decimal, ByRef dyourPrice As Decimal, ByRef boolhaspromo As Boolean)
            Dim discount As Integer = 0

            lblCurrentPartNumber.Text = partNumber
            lblDescription.Text = partDesc

            If listPrice = yourPrice Then
                lblheadYourPrice.Visible = False
                lblheadDisCount.Visible = False
                lblYourPrice.Visible = False
                lblDiscount.Visible = False
            Else
                lblheadYourPrice.Visible = True
                lblheadDisCount.Visible = True
                lblYourPrice.Visible = True
                lblDiscount.Visible = True
            End If
            lblheadListPrice.Visible = True
            lblListPrice.Text = listPrice
            lblListPrice.Visible = True

            lblYourPrice.Text = yourPrice
            tdLegend.Visible = False
            If boolhaspromo = True Then
                lblYourPrice.Text &= "<span style=""font-family: Wingdings; color: red; font-size: 12px;"">v</span>"
                If listPrice <> yourPrice Then Me.tdLegend.Visible = True
            End If

            If Not String.Equals(availibility, "In stock") Then
                Me.lblAvailibility.Height = 30
            End If
            Me.lblAvailibility.Text = availibility

            lblDiscount.Text = "N/A"
            If yourPrice <> "unavailable" Then
                discount = (dlistPrice - dyourPrice) / dlistPrice * 100
                If discount <> 0 Then
                    lblDiscount.Text = discount.ToString() + " %"
                End If
            End If

        End Sub

        Private Sub GetPartPrice()
            Dim debugMessage As String = $"YourPrice.getPartPrice - START at {Date.Now.ToShortTimeString}.{Environment.NewLine}"

            Try
                Dim catMan As New CatalogManager
                Dim promoMan As New PromotionManager
                Dim corepart As New Part()
                Dim isKit As String = Request.QueryString("isKit")
                Dim your_price = String.Empty
                Dim list_price = String.Empty
                Dim availibility = String.Empty
                Dim dyourprice As Double = 0
                Dim dlistprice As Double = 0
                Dim recycleflag As Double = 0
                Dim hasPromo As Boolean = False

                tdLegend.Visible = False
                If isKit = "1" Then
                    Dim whichKit As Integer = Request.QueryString("kitVal")
                    If Session.Item("kits") IsNot Nothing Then
                        Dim kits = Session.Item("kits")
                        Dim kit As Kit = kits.GetValue(whichKit)
                        If customer IsNot Nothing Then
                            Try
                                your_price = GetYourPrice(customer, kits.GetValue(whichKit), Product.ProductType.Kit, availibility, corepart, dyourprice, dlistprice, HttpContextManager.GlobalData)
                                If promoMan.IsPromotionPart(kit.PartNumber) Then
                                    lblYourPrice.Text = your_price + "<span style=""font-family: Wingdings; color: red; font-size: 12px;"">v</span>"
                                    hasPromo = True
                                End If
                            Catch nullEx As NullReferenceException
                                your_price = conPriceUnavailableMsg
                            Catch ex As Exception
                                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                            End Try
                        Else
                            your_price = conPriceUnavailableMsg
                        End If

                        list_price = kit.ListPrice.ToString("C")
                        If customer IsNot Nothing Then
                            recycleflag = catMan.ISRecyclingFlagCheck(kit.PartNumber, customer, HttpContextManager.GlobalData)
                            Me.tdrecycle.Visible = recycleflag > 0
                        End If
                        PopulateLabelControls(kit.PartNumber, corepart.Description, corepart.ListPrice, your_price, availibility, dlistprice, dyourprice, hasPromo)

                        'Added to inform user of core charge
                        If kit.HasCoreCharge Then
                            lblYourPrice.Text = "<span class=""redAsterick"">*</span>" + lblYourPrice.Text.ToString()
                            HasCoreChargeLabel.Text = "<span class=""redAsterick"">*</span>" + Resources.Resource.el_Price_FRB1 + corepart.CoreCharge.ToString("C") + Resources.Resource.el_Price_FRB2 + corepart.CoreCharge.ToString("C") + Resources.Resource.el_Price_FRB3
                        End If
                    Else
                        ErrorLabel.Text = Resources.Resource.el_Price_Err ' "Error getting your price."
                    End If
                Else
                    Dim parts() As Part
                    Dim the_part As Part = Nothing
                    Dim whichPart As Integer

                    debugMessage &= $"- Pulling partList from Session.{Environment.NewLine}"
                    parts = IIf(Session("partList") IsNot Nothing, Session.Item("partList"), Nothing)
                    whichPart = IIf(Request.QueryString("ArrayVal") IsNot Nothing, Request.QueryString("ArrayVal"), Nothing)
                    If (parts IsNot Nothing) AndAlso (parts.Length > 0) Then
                        the_part = parts.GetValue(whichPart)
                    Else
                        debugMessage &= $"- Parts array was empty or missing"
                    End If

                    If customer IsNot Nothing Then
                        Try
                            'Utilities.LogDebug("YourPrice.getPartPrice - Calling getYourPrice")
                            your_price = GetYourPrice(customer, the_part, Product.ProductType.Part, availibility, corepart, dyourprice, dlistprice, HttpContextManager.GlobalData)
                            If the_part.ReplacementPartNumber IsNot Nothing Then
                                If promoMan.IsPromotionPart(the_part.ReplacementPartNumber) Then
                                    lblYourPrice.Text = your_price + "<span style=""font-family: Wingdings; color: red; font-size: 12px;"">v</span>"
                                    hasPromo = True
                                End If
                            Else
                                If promoMan.IsPromotionPart(the_part.PartNumber) Then
                                    lblYourPrice.Text = your_price + "<span style=""font-family: Wingdings; color: red; font-size: 12px;"">v</span>"
                                    hasPromo = True
                                End If
                            End If
                        Catch nullEx As NullReferenceException
                            your_price = conPriceUnavailableMsg
                        Catch ex As Exception
                            ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                        End Try
                    Else
                        your_price = conPriceUnavailableMsg
                    End If

                    debugMessage &= $"- Checking FRB and Core charge"
                    If (the_part.IsFRB Or the_part.HasCoreCharge) Then
                        'list_price = "$" + Format(the_part.ListPrice + the_part.CoreCharge, "##0.00")
                        list_price = (the_part.ListPrice + the_part.CoreCharge).ToString("C")
                    Else
                        'list_price = "$" + Format(the_part.ListPrice, "##0.00")
                        list_price = the_part.ListPrice.ToString("C")
                    End If
                    debugMessage &= $"- list_price = {list_price}, "

                    If customer IsNot Nothing Then
                        'recycleflag = catMan.ISRecyclingFlagCheck(corepart.PartNumber, customer, HttpContextManager.GlobalData)
                        debugMessage &= $"Part Number = {the_part.PartNumber}, "
                        recycleflag = catMan.ISRecyclingFlagCheck(the_part.PartNumber, customer, HttpContextManager.GlobalData)
                        debugMessage &= $"Recycling Flag = {recycleflag}"
                        Me.tdrecycle.Visible = recycleflag > 0
                    End If

                    Dim strgetlistprice As String = corepart.ListPrice.ToString("C")
                    PopulateLabelControls(corepart.PartNumber, corepart.Description, strgetlistprice, your_price, availibility, dlistprice, dyourprice, hasPromo)
                    'Added to inform user of core charge
                    If the_part.IsFRB Then
                        lblYourPrice.Text = "<span class=""redAsterick"">*</span>" & lblYourPrice.Text
                        HasCoreChargeLabel.Text = $"<span class=""redAsterick"">*</span>{Resources.Resource.el_Price_FRB}{Resources.Resource.el_Price_FRB1}{corepart.CoreCharge.ToString("C")}{Resources.Resource.el_Price_FRB2}{corepart.CoreCharge.ToString("C")}{Resources.Resource.el_Price_FRB3}"
                    ElseIf the_part.HasCoreCharge Then
                        lblYourPrice.Text = "<span class=""redAsterick"">*</span>" & lblYourPrice.Text
                        HasCoreChargeLabel.Text = $"<span class=""redAsterick"">*</span>{Resources.Resource.el_Price_FRB1}{corepart.CoreCharge.ToString("C")}{Resources.Resource.el_Price_FRB2}{corepart.CoreCharge.ToString("C")}{Resources.Resource.el_Price_FRB3}"
                    End If
                End If
            Catch ex As Exception
                Utilities.LogMessages(debugMessage)
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                Dim objPCILogger As New PCILogger With {
                    .EventOriginApplication = "ServicesPLUS",
                    .EventOriginApplicationLocation = Me.Page.GetType().Name,
                    .EventOriginServerMachineName = HttpContext.Current.Server.MachineName,
                    .OperationalUser = Environment.UserName,
                    .EventDateTime = Date.Now.ToLongTimeString(),
                    .CustomerID = customer.CustomerID,
                    .CustomerID_SequenceNumber = customer.SequenceNumber,
                    .EmailAddress = customer.EmailAddress,
                    .SIAM_ID = customer.SIAMIdentity,
                    .LDAP_ID = customer.LdapID,
                    .EventOriginMethod = "getPartPrice",
                    .EventType = EventType.View,
                    .IndicationSuccessFailure = "Failure",
                    .Message = "getPartPrice Failed. " & ex.Message.ToString()
                }
                objPCILogger.HTTPRequestObjectValues = objPCILogger.GetRequestContextValue()
                objPCILogger.PushLogToMSMQ()
            End Try
        End Sub

    End Class

End Namespace
