Imports ServicesPlusException
Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Process
Imports Sony.US.SIAMUtilities

Namespace ServicePLUSWebApp

    Partial Class PartsPLUSResults
        Inherits SSL

        Private Const conSpacerTableCell As String = "<td bgcolor=""D5DEE9"" width=""1"" height=""28""><img src=""images/spacer.gif"" width=""1"" height=""28"" alt=""""></td>"
        Public cm As New CustomerManager
        Public customer As Customer = Nothing
        Public models() As Model
        Public parts() As Sony.US.ServicesPLUS.Core.Part
        Public kits() As Kit
        Public SearchType As String
        Public sMaximumMessage As String = String.Empty
        Public PageTitle As String = Resources.Resource.ttl_Parts '"Sony Parts for Professional Products"
        Dim PageNum As Integer = 1

#Region " Web Form Designer Generated Code "
        Protected WithEvents LabelModule As System.Web.UI.WebControls.Label
        Protected WithEvents LabelRef As System.Web.UI.WebControls.Label
        Protected WithEvents LabelPartRequested As System.Web.UI.WebControls.Label
        Protected WithEvents LabelPartSupplied As System.Web.UI.WebControls.Label
        Protected WithEvents Label4 As System.Web.UI.WebControls.Label
        Protected WithEvents Label5 As System.Web.UI.WebControls.Label
        Protected WithEvents ExplodeButton As System.Web.UI.WebControls.Button

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub
#End Region

        'Private objGlobalData As New GlobalData
        'Public prg_PleaseWait As String
        'Public atc_AddingItem As String
        'Public atc_ToCart As String
        'Public atc_ex_DifferentCart As String
        'Public atc_title_QuickOrderExceptions As String
        'Public atc_ex_InvalidPartNumber As String
        'Public atc_ex_InvalidQuantity As String
        'Public el_item As String
        'Public el_Pts_OrderQty As String
        'Public atc_title_QuickOrderStatus As String

        'Private Sub fnGetResourceData()
        '    prg_PleaseWait = Resources.Resource.prg_PleaseWait
        '    atc_AddingItem = Resources.Resource.atc_AddingItem
        '    atc_ToCart = Resources.Resource.atc_ToCart
        '    atc_ex_DifferentCart = Resources.Resource.atc_ex_DifferentCart
        '    atc_title_QuickOrderExceptions = Resources.Resource.atc_title_QuickOrderExceptions
        '    atc_ex_InvalidPartNumber = Resources.Resource.atc_ex_InvalidPartNumber
        '    atc_ex_InvalidQuantity = Resources.Resource.atc_ex_InvalidQuantity
        '    el_Pts_OrderQty = Resources.Resource.el_Pts_OrderQty

        '    el_item = Resources.Resource.el_item
        '    atc_title_QuickOrderStatus = Resources.Resource.atc_title_QuickOrderStatus
        'End Sub

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As EventArgs) Handles MyBase.Load
            Try
                Dim TotalPerPage As Integer = 50
                Dim catMan As New CatalogManager

                'fnGetResourceData()
                ' Set up the Configuration values into the HiddenField objects for the JavaScript file to read.
                quickOrderMaxItems.Value = ConfigurationData.GeneralSettings.General.QuickOrderMaxItems
                orderUploadMaxItems.Value = ConfigurationData.GeneralSettings.General.OrderUploadMaxItems

                ' Get Search Type from query string
                If (Request.QueryString("stype") IsNot Nothing) AndAlso (IsValidString(Request.QueryString("stype"))) Then
                    SearchType = Request.QueryString("stype").ToLower()
                Else
                    ErrorLabel.Text = "Invalid or missing query string detected. Please go back or try a new search."
                    Return
                End If
                'Set values for pagination
                If (Request.QueryString("PageNum") IsNot Nothing) AndAlso (IsValidString(Request.QueryString("PageNum"))) Then
                    PageNum = Request.QueryString("PageNum")
                End If

                If Not Page.IsPostBack() Then
                    tdLegend.Visible = False
                    ErrorLabel.Text = ""
                    lblSubHdr.InnerText = Resources.Resource.el_Pts_SubHeading  ' 2018-09-14 Set default page title. Moved from page markup for WCAG compliance.
                    If HttpContextManager.Customer IsNot Nothing Then
                        ' Check if user logged in during Add to Cart request
                        customer = HttpContextManager.Customer
                        AddToCartAfterLogin(ErrorLabel)
                    End If

                    ' Destroy search parameters used by Parts Finder
                    Session.Remove("selectedOption")
                    Session.Remove("selectedCategory")
                    Session.Remove("selectedModule")
                    Session.Remove("selectedModel")
                    Session.Remove("refineSearchText")
                    Session.Remove("refineSearchType")
                    Session.Remove("moduleList")

                    ' Destroy Parts Finder variables - May be obsolete
                    Session.Remove("category")
                    Session.Remove("modules")
                    Session.Remove("_module")
                    Session.Remove("model")
                    Session.Remove("modeloption")
                    Session.Remove("lblShowOption")

                    ' Perform or retrieve search results and display them.
                    If SearchType = "kits" Then
                        If Request.QueryString("kit") IsNot Nothing Then
                            Try
                                Dim kits() As Kit
                                Dim strModelNumber As String = Request.QueryString("kit").Trim()
                                If Not IsValidString(strModelNumber) Then
                                    ErrorLabel.Text = "Modified or illegal URL detected. Please go back and try again."
                                    Return
                                End If
                                kits = catMan.KitSearch(strModelNumber, "", "")
                                Session.Item("kits") = kits
                            Catch ex As Exception
                                Utilities.LogMessages("PartsResults.Page_Load - Error during Kit search")
                                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                            End Try
                        End If
                        kits = Session.Item("kits")
                        DisplayKits(PageNum, TotalPerPage)
                    ElseIf SearchType = "models" Then
                        models = Session.Item("models")
                        DisplayModels(PageNum, TotalPerPage)
                    ElseIf SearchType = "parts" Then
                        Try
                            If Request.QueryString("partNumber") IsNot Nothing Then
                                ' A part number was provided, so do a search for it.
                                Dim partnum As String = Request.QueryString("partNumber")
                                If Not IsValidString(partnum) Then
                                    ErrorLabel.Text = "Modified or illegal URL detected. Please try a new search."
                                    Return
                                End If
                                Session.Item("partList") = catMan.QuickSearch("", partnum, "", sMaximumMessage, HttpContextManager.GlobalData)
                                Session.Item("kits") = catMan.KitSearch("", partnum, "")
                            ElseIf Session.Item("SearchParts") IsNot Nothing Then
                                Try
                                    Dim strSearch() As String = Session.Item("SearchParts").ToString().Split("�")
                                    'parts = catMan.QuickSearch(strSearch(0).Trim(), strSearch(1).Trim(), strSearch(2).Trim(), "", HttpContextManager.GlobalData)
                                    'kits = catMan.KitSearch(strSearch(0).Trim(), strSearch(1).Trim(), strSearch(2).Trim()) 'also search for kit now
                                    Session.Item("partList") = catMan.QuickSearch(strSearch(0).Trim(), strSearch(1).Trim(), strSearch(2).Trim(), "", HttpContextManager.GlobalData)
                                    Session.Item("kits") = catMan.KitSearch(strSearch(0).Trim(), strSearch(1).Trim(), strSearch(2).Trim()) 'also search for kit now
                                Catch ex As Exception
                                    Dim result = "null array"
                                    If (Session("SearchParts") IsNot Nothing) Then result = Session("SearchParts").ToString
                                    Utilities.LogMessages("ERROR - While re-fetching part search parameters. Session.SearchParts = " & result)
                                    ErrorLabel.Text = Resources.Resource.el_PartsNoSearch   ' No items found, or session was lost.
                                    Return
                                End Try
                            End If

                            ' Retrieve Part and Kit results from Session Variables
                            parts = If(Session.Item("partList"), Nothing)
                            kits = If(Session.Item("kits"), Nothing)
                            If Not (parts?.Length > 0) And Not (kits?.Length > 0) Then
                                ErrorLabel.Text = Resources.Resource.el_PartsNoSearch   ' No items found, or session was lost.
                                Return
                            End If

                            DisplaySubParts(PageNum, TotalPerPage)
                        Catch ex As Exception
                            Utilities.LogMessages("PartsResults.Page_Load - Error during Parts search or retrieve")
                            ErrorLabel.Text = Utilities.WrapExceptionforUI(ex) & "<br/>Please ensure your browser can accept cookies."
                            Return
                        End Try
                    Else
                        ErrorLabel.Text = Resources.Resource.el_PartsNoSearch ' "No search criteria found. Use New Search button to re-start your search."
                        Return
                    End If

                    'Add to my Catalog
                    If Request.QueryString("atmc") IsNot Nothing Then
                        If Request.QueryString("atmc").ToLower() = "true" Then AddPartToMyCatalog()
                    End If

                End If

                Try ' Check if the user changed the Language.
                    If Session.Item("Language_Changed") IsNot Nothing AndAlso Session.Item("Language_Changed") = "1" Then
                        Session.Remove("Language_Changed")
                        If SearchType = "models" And Session.Item("SearchModel") IsNot Nothing Then
                            fnGetModule(Session.Item("SearchModel").ToString())
                            DisplayModels(PageNum, TotalPerPage)
                        ElseIf SearchType = "parts" Then
                            Try
                                If (Request.QueryString("partNumber") IsNot Nothing) Then
                                    Dim partnum As String = Request.QueryString("partNumber")
                                    If (IsValidString(partnum)) Then
                                        'parts = catMan.QuickSearch("", partnum, "", sMaximumMessage, HttpContextManager.GlobalData)
                                        'kits = catMan.KitSearch("", partnum, "")
                                        Session.Item("partList") = catMan.QuickSearch("", partnum, "", sMaximumMessage, HttpContextManager.GlobalData)
                                        Session.Item("kits") = catMan.KitSearch("", partnum, "")
                                    Else
                                        ErrorLabel.Text = "Modified or illegal URL detected. Please go back and try again."
                                        Return
                                    End If
                                ElseIf Session.Item("SearchParts") IsNot Nothing Then
                                    Try
                                        Dim strSearch() As String = Session.Item("SearchParts").ToString().Split("�")
                                        'parts = catMan.QuickSearch(strSearch(0).Trim(), strSearch(1).Trim(), strSearch(2).Trim(), "", HttpContextManager.GlobalData)
                                        'kits = catMan.KitSearch(strSearch(0).Trim(), strSearch(1).Trim(), strSearch(2).Trim()) 'also search for kit now
                                        Session.Item("partList") = catMan.QuickSearch(strSearch(0).Trim(), strSearch(1).Trim(), strSearch(2).Trim(), "", HttpContextManager.GlobalData)
                                        Session.Item("kits") = catMan.KitSearch(strSearch(0).Trim(), strSearch(1).Trim(), strSearch(2).Trim()) 'also search for kit now
                                    Catch ex As Exception
                                        Utilities.LogMessages("ERROR - While re-fetching part search parameters. Session.SearchParts = " & Session("SearchParts").ToString)
                                    End Try
                                End If

                                If (Session.Item("partList") IsNot Nothing) Then parts = Session.Item("partList")
                                If (Session.Item("kits") IsNot Nothing) Then kits = Session.Item("kits") 'also contain kit
                                If (parts Is Nothing AndAlso kits Is Nothing) OrElse (parts.Length + kits.Length = 0) Then
                                    ErrorLabel.Text = Resources.Resource.el_PartsNoSearch
                                    Return
                                Else
                                    DisplaySubParts(PageNum, TotalPerPage)
                                End If
                            Catch ex As Exception
                                Utilities.LogMessages("ERROR - While processing language change. (Parts section)")
                                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                            End Try
                            'DisplaySubParts(PageNum, TotalPerPage)
                        End If
                    End If
                Catch ex As Exception
                    Utilities.LogMessages("ERROR - While processing language change. (Outer)")
                    ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                End Try
            Catch ex As Exception
                Dim partCount = "Null Array"
                If (parts IsNot Nothing) Then partCount = parts.Length.ToString()
                Dim message = $"ERROR - PartsPLUSResults.Page_Load. Parts Count = {partCount}, Locale = {HttpContextManager.GlobalData.Language}"
                Utilities.LogMessages(message)
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

#Region "Load Module from DB  - Kayal"
        Private Sub fnGetModule(ByVal strModuleid As String)

            Dim cm As New CatalogManager

            Dim models() As Model
            Dim lstConficts As Generic.List(Of String) = Nothing
            Dim strModels As String = String.Empty
            models = cm.PartSearch(strModuleid.Trim(), HttpContextManager.GlobalData, lstConficts)
            Session.Add("models", models)
        End Sub
#End Region

        Private Sub DisplaySubParts(ByVal PageNumber As Integer, ByVal NumberPerPage As Integer)
            Dim promoMan As New PromotionManager
            Dim arrParts As Sony.US.ServicesPLUS.Core.Part()
            Dim arrKits As Kit()
            Dim p As Sony.US.ServicesPLUS.Core.Part
            Dim TR As New TableRow
            Dim TD As New TableCell
            Dim n As Integer
            Dim RowColor As String
            Dim ReplacementPartColor As String
            Dim isValidPart As Boolean
            'Dim isFRB As Boolean
            'Dim haveCoreCharge As Boolean
            Dim strDescription As String
            Dim strListPrice As String
            Dim isAnyPromoItem As Boolean = False
            Dim PrevPage As Integer
            Dim NextPage As Integer
            Dim strPriceStar As String
            Dim lnkPartNumber As String
            Dim lnkReplacementPart As String
            Dim TotalPages As Integer = 0
            Dim TotalReturned As Integer = 0
            Dim StartPageNum As Integer
            Dim EndPageNum As Integer

            tdLegend.Visible = False
            PageLabel.Text = ""
            SearchCopyLabel.Text = ""
            BackToTopLabel.Text = ""

            Try
                arrParts = IIf(Session.Item("partList") IsNot Nothing, Session.Item("partList"), Nothing)
                arrKits = IIf(Session.Item("kits") IsNot Nothing, Session.Item("kits"), Nothing)

                If (arrParts IsNot Nothing) And (Not String.IsNullOrEmpty(sMaximumMessage)) Then
                    ErrorLabel.Text = sMaximumMessage
                    Return
                ElseIf arrParts Is Nothing And arrKits Is Nothing Then
                    ErrorLabel.Text = (Resources.Resource.el_Search_NoResult) '"No results found.")
                    Return
                Else
                    DisplayResultsTable.Rows.Clear()

                    ' -- HEADER ROW
                    TD.Controls.Add(New LiteralControl("<tr style=""height: 28px; background: #d5dee9;"">"))
                    TD.Controls.Add(New LiteralControl(conSpacerTableCell))
                    TD.Controls.Add(New LiteralControl("<th width=""80"" class=""tableHeader"" height=""28"">&nbsp;&nbsp;" + Resources.Resource.el_ModelNumber + "</th>"))
                    TD.Controls.Add(New LiteralControl(conSpacerTableCell))
                    TD.Controls.Add(New LiteralControl("<th class=""tableHeader"" height=""28"">&nbsp;&nbsp;" + Resources.Resource.el_PartsRequesterd + "</th>"))
                    TD.Controls.Add(New LiteralControl(conSpacerTableCell))
                    TD.Controls.Add(New LiteralControl("<th class=""tableHeader"" height=""28"">" + Resources.Resource.el_PartsSupplied + "</th>"))
                    TD.Controls.Add(New LiteralControl(conSpacerTableCell))
                    TD.Controls.Add(New LiteralControl("<th class=""tableHeader"" height=""28"">" + Resources.Resource.el_Description1 + "</th>"))
                    TD.Controls.Add(New LiteralControl(conSpacerTableCell))
                    TD.Controls.Add(New LiteralControl($"<th width=""130"" class=""tableHeader"" height=""28"">{IIf(HttpContextManager.GlobalData.IsCanada,
                                                                    Resources.Resource.el_ListPrice_CAD, Resources.Resource.el_ListPrice)}</th>"))
                    TD.Controls.Add(New LiteralControl(conSpacerTableCell))
                    TD.Controls.Add(New LiteralControl("<th width=""26"" class=""tableHeader"" align=""left"" height=""28"">" + Resources.Resource.el_CartAdd + "</th>"))
                    TD.Controls.Add(New LiteralControl(conSpacerTableCell))
                    TD.Controls.Add(New LiteralControl("<th width=""26"" class=""tableHeader"" align=""left"" height=""28"">" + Resources.Resource.el_Cat + "</th>"))
                    TD.Controls.Add(New LiteralControl(conSpacerTableCell))
                    TD.Controls.Add(New LiteralControl("<td width=""26"" class=""tableHeader"" align=""right"" height=""28""><img src=""images/spacer.gif"" width=""1"" height=""28"" alt=""""></td>"))
                    TD.Controls.Add(New LiteralControl(conSpacerTableCell))
                    TD.Controls.Add(New LiteralControl("</tr>"))
                    TD.Controls.Add(New LiteralControl("<tr>"))
                    TD.Controls.Add(New LiteralControl("<td bgcolor=""D5DEE9"" colspan=""17""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>"))
                    TD.Controls.Add(New LiteralControl("</tr>"))

                    'TotalReturned = arrParts.Length + arrKits.Length
                    If (arrParts IsNot Nothing) Then TotalReturned += arrParts.Length
                    If (arrKits IsNot Nothing) Then TotalReturned += arrKits.Length

                    If TotalReturned > 50 Then
                        TotalPages = Int(TotalReturned / NumberPerPage) + IIf(0 < (TotalReturned Mod NumberPerPage), 1, 0)
                    Else
                        TotalPages = 1
                    End If
                    'PageNum = 1
                    StartPageNum = (PageNumber - 1) * NumberPerPage
                    If (StartPageNum < 0) Then StartPageNum = 0

                    EndPageNum = (PageNumber * NumberPerPage) - 1
                    If EndPageNum >= TotalReturned Then EndPageNum = TotalReturned - 1

                    PrevPage = PageNumber - 1
                    NextPage = PageNumber + 1

                    ' -- LOOP for each Part or Kit in results
                    For n = StartPageNum To EndPageNum
                        If (arrParts IsNot Nothing) AndAlso (n < arrParts.Length) Then
                            ' Variable resets
                            isValidPart = True
                            strPriceStar = ""
                            RowColor = IIf((n Mod 2 = 0), "#FFFFFF", "#F2F5F8")

                            p = arrParts(n)

                            lnkPartNumber = HttpUtility.UrlEncode(ReturnCharacterSub(p.PartNumber))
                            If lnkPartNumber.Contains("%2b") And p.PartNumber.Contains("/") Then
                                lnkPartNumber = lnkPartNumber.Replace("%2b", "+")
                            End If
                            lnkPartNumber = $"<a href=""sony-part-number-{lnkPartNumber}.aspx"" title=""Go to the details page for part number {p.PartNumber}"">"

                            strDescription = IIf(p.IsFRB, $"<span class=tableHeader><span class=""redAsterick"">*</span>FRB:</span> {p.Description}", p.Description)

                            If (p.IsFRB Or p.HasCoreCharge) Then
                                strListPrice = (p.ListPrice + p.CoreCharge).ToString("C")
                            Else
                                strListPrice = p.ListPrice.ToString("C")
                            End If

                            If Not String.IsNullOrWhiteSpace(p.ReplacementPartNumber) Then
                                If promoMan.IsPromotionPart(p.ReplacementPartNumber) Then
                                    strPriceStar = "<span style=""font-family: Wingdings; color: red; font-size: 12px;"">v</span>"
                                    isAnyPromoItem = True
                                End If
                            Else
                                If promoMan.IsPromotionPart(p.PartNumber) Then
                                    strPriceStar = "<span style=""font-family: Wingdings; color: red; font-size: 12px;"">v</span>"
                                    isAnyPromoItem = True
                                End If
                            End If

                            If arrParts.Length = 1 Then
                                PageTitle = $"{Resources.Resource.el_PartNumber} {p.PartNumber}"
                                lblSubHdr.InnerText = $"{Resources.Resource.el_PartNumber} {p.PartNumber}"
                            Else
                                PageTitle = Resources.Resource.ttl_Parts '"Sony Parts for Professional Products"
                                lblSubHdr.InnerText = Resources.Resource.el_Pts_SubHeading '"Search Sony Parts and Accessories"
                            End If

                            lnkReplacementPart = HttpUtility.UrlEncode(ReturnCharacterSub(p.ReplacementPartNumber))
                            If lnkReplacementPart.Contains("%2b") And p.ReplacementPartNumber.Contains("/") Then
                                lnkReplacementPart = lnkReplacementPart.Replace("%2b", "+")
                            End If
                            lnkReplacementPart = $"<a href=""sony-part-number-{lnkReplacementPart}.aspx"" title=""Go to the details page for part number {p.ReplacementPartNumber}"">"


                            '-- Row Start - Model Number, Part, Replacement, Description
                            If (p.PartNumber <> p.ReplacementPartNumber) Then
                                ReplacementPartColor = "#88ee88"
                            Else
                                ReplacementPartColor = RowColor
                            End If
                            TD.Controls.Add(New LiteralControl($"<tr>
                                {conSpacerTableCell}
                                <td bgcolor=""{RowColor}"" width=""80"" class=""tableHeader"">&nbsp;&nbsp;{p.Model}</td>
                                {conSpacerTableCell}
                                <td bgcolor=""{RowColor}"" class=""tableData"">&nbsp;&nbsp;{lnkPartNumber}{p.PartNumber}</a></td>
                                {conSpacerTableCell}
                                <td bgcolor=""{ReplacementPartColor}"" class=""tableData"">&nbsp;&nbsp;{lnkReplacementPart}{p.ReplacementPartNumber}</a></td>
                                {conSpacerTableCell}
                                <td bgcolor=""{RowColor}"" class=""tableData"">&nbsp;&nbsp;{strDescription}&nbsp;</td>
                                {conSpacerTableCell}"))


                            '-- Availability / Your Price
                            If isValidPart Then
                                If p.ReplacementPartNumber.StartsWith("991099") Then
                                    TD.Controls.Add(New LiteralControl($"<td bgcolor={RowColor} width=""1""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>"))
                                ElseIf p.Category.CategoryCode = "INT" Or p.Category.CategoryCode = "HOL" Then
                                    TD.Controls.Add(New LiteralControl($"<td bgcolor={RowColor} class=""legalCopy"">{Resources.Resource.el_InternalSale}</td>"))
                                ElseIf p.ListPrice <= 0 Or p.ListPrice = Nothing Then
                                    TD.Controls.Add(New LiteralControl($"<td bgcolor={RowColor} class=""legalCopy"">{Resources.Resource.el_PriceExcept}</td>"))
                                Else
                                    'Check if the Customer has a valid SAP account to show "Your Price"
                                    If (customer IsNot Nothing) AndAlso (customer.SAPBillToAccounts?.Length > 0 Or customer.SISLegacyBillToAccounts?.Length > 0) Then
                                        TD.Controls.Add(New LiteralControl($"<td bgcolor={RowColor} width=""130"" class=""tableData"">
                                            &nbsp;&nbsp;{strListPrice}{strPriceStar}&nbsp;&nbsp;&nbsp;<span class=""tableHeader""><a class=""Body"" href=""#""
                                            onclick=""mywin=window.open('YourPrice.aspx?isKit=0&ArrayVal={n}','','{CON_POPUP_FEATURES}'); return false;""
                                            title=""Open live pricing and availability for part number {p.PartNumber} in a new window."">{Resources.Resource.el_YourPrice}</a>
                                            </span></td>"))
                                    Else ' Show "Availability"
                                        TD.Controls.Add(New LiteralControl($"<td bgcolor={RowColor} width=""130"" class=""tableData"">
                                            &nbsp;&nbsp;{strListPrice}{strPriceStar}&nbsp;&nbsp;&nbsp;<span class=""tableHeader""><a class=""Body"" href=""#""
                                            onclick=""mywin=window.open('Availability.aspx?isKit=0&ArrayVal={n}','','{CON_POPUP_FEATURES}'); return false;""
                                            title=""Open live pricing and availability for part number {p.PartNumber} in a new window."">{Resources.Resource.el_Availability}</a>
                                            </span></td>"))
                                    End If
                                End If
                            End If
                            TD.Controls.Add(New LiteralControl(conSpacerTableCell))

                            '-- Add to Cart
                            If isValidPart Then
                                If p.ReplacementPartNumber.StartsWith("991099") Then
                                    TD.Controls.Add(New LiteralControl($"<td bgcolor={RowColor} width=""1""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>"))
                                ElseIf p.Category.CategoryCode = "INT" Or p.Category.CategoryCode = "HOL" Then
                                    TD.Controls.Add(New LiteralControl($"<td bgcolor={RowColor} width=""1""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>"))
                                ElseIf p.ListPrice = 0.0R Or p.ListPrice = Nothing Then
                                    TD.Controls.Add(New LiteralControl($"<td bgcolor={RowColor} width=""1""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>"))
                                Else
                                    If customer Is Nothing Then 'Show Login Pop-Up
                                        TD.Controls.Add(New LiteralControl($"<td bgcolor={RowColor} width=""26"" class=""tableData"" align=""center"">
                                            <a href=""NotLoggedInAddToCart.aspx?stype={SearchType}&post=ppr&PartLineNo={n}"" title=""Go to the login page to add this item to your cart.""><img
                                            src=""images/sp_int_add2Cart_btn.gif"" width=""29"" height=""20"" border=""0"" alt=""Add to Cart""></a></td>"))
                                    Else
                                        TD.Controls.Add(New LiteralControl($"<td bgcolor={RowColor} width=""26"" class=""tableData"" align=""center"">
                                            <a href=""#"" onclick=""AddToCart_onclick('{p.ReplacementPartNumber}'); return false;""
                                            title=""Add part number {p.ReplacementPartNumber} to your shopping cart""><img src=""images/sp_int_add2Cart_btn.gif""
                                            width=""29"" height=""20"" border=""0"" alt=""Add to Cart""></a></td>"))
                                    End If
                                End If
                            Else
                                TD.Controls.Add(New LiteralControl($"<td bgcolor={RowColor} width=""1""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>"))
                            End If

                            '-- Add to Catalog
                            TD.Controls.Add(New LiteralControl("<td bgcolor=""D5DEE9"" width=""1"" class=""tableData"" align=""right""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>"))
                            If isValidPart Then
                                If p.ReplacementPartNumber.StartsWith("991099") Then
                                    TD.Controls.Add(New LiteralControl($"<td bgcolor={RowColor} width=""1""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>"))
                                ElseIf p.Category.CategoryCode = "INT" Or p.Category.CategoryCode = "HOL" Then
                                    TD.Controls.Add(New LiteralControl($"<td bgcolor={RowColor} width=""1""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>"))
                                ElseIf p.ListPrice <= 0 Or p.ListPrice = Nothing Then
                                    TD.Controls.Add(New LiteralControl($"<td bgcolor={RowColor} width=""1""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>"))
                                Else
                                    If customer Is Nothing Then 'Show Login Pop-Up
                                        TD.Controls.Add(New LiteralControl($"<td bgcolor={RowColor} width=""26"" class=""tableData"" align=""center"">
                                            <a href=""NotLoggedInAddToCatalog.aspx?stype={SearchType}&post=ppr&PartLineNo={n}""
                                                title=""Go to the login page to add this item to your catalog."">
                                            <img src=""images/sp_int_add2myCat_btn.gif"" width=""29"" height=""20"" border=""0"" alt=""Add to My Catalog""></a></td>"))
                                    Else ' This is where we lose the old part number
                                        TD.Controls.Add(New LiteralControl($"<td bgcolor={RowColor} width=""26"" class=""tableData"" align=""center"">
                                            <a href=""PartsPLUSResults.aspx?pValue={n}&atmc=true&stype={SearchType}&PageNum={PageNum}""
                                                title=""Save part number {p.ReplacementPartNumber} to your catalog."">
                                            <img src=""images/sp_int_add2myCat_btn.gif"" width=""29"" height=""20"" border=""0"" alt=""Add to My Catalog""></a></td>"))
                                    End If
                                End If
                            End If

                            '-- Exploded View
                            TD.Controls.Add(New LiteralControl($"<td bgcolor={RowColor} width=""1"" class=""tableData"" align=""right""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>"))
                            If p.HasExplodedView Then
                                TD.Controls.Add(New LiteralControl($"<td bgcolor={RowColor} width=""26"" class=""tableData"" align=""center"">
                                    <a target=""_blank"" href=""View2.aspx?ArrayVal={n}""><img src=""images/sp_int_expandView_btn.gif"" border=""0"" alt=""See the Exploded View for this item""></a></td>"))
                            Else
                                TD.Controls.Add(New LiteralControl($"<td bgcolor={RowColor} width=""26""><img src=""images/spacer.gif"" width=""29"" height=""20"" alt=""""></td>"))
                            End If
                            TD.Controls.Add(New LiteralControl("<td bgcolor=""D5DEE9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>"))

                            '-- Optional "explanation" row for extra charges.
                            If (p.IsFRB) Then
                                TD.Controls.Add(New LiteralControl($"<tr><td colspan=17 bgcolor=""#d5dee9"">
                                    <span class=""legalCopy""><span class=""redAsterick"">*</span> {Resources.Resource.el_Price_FRB}{Resources.Resource.el_Price_FRB1}
                                    {p.CoreCharge.ToString("C")} {Resources.Resource.el_Price_FRB2} {p.CoreCharge.ToString("C")} {Resources.Resource.el_Price_FRB3}<br/></td></tr>"))
                                TD.Controls.Add(New LiteralControl("<tr><td colspan=17><img height=""1"" src=""images/spacer.gif"" width=""20"" alt="""">"))
                            ElseIf (p.HasCoreCharge) Then
                                TD.Controls.Add(New LiteralControl($"<tr><td colspan=17 bgcolor=""#d5dee9"">
                                    <span class=""legalCopy""><span class=""redAsterick"">*</span> {Resources.Resource.el_Price_FRB1}
                                    {p.CoreCharge.ToString("C")} {Resources.Resource.el_Price_FRB2} {p.CoreCharge.ToString("C")} {Resources.Resource.el_Price_FRB3}<br/></td></tr>"))
                                TD.Controls.Add(New LiteralControl("<tr><td colspan=17><img height=""1"" src=""images/spacer.gif"" width=""20"" alt="""">"))
                            End If
                            If (p.ProgramCode = "H") Then
                                TD.Controls.Add(New LiteralControl($"<tr><td colspan=17 bgcolor=""#d5dee9"">
                                    <span class=""legalCopy""><span class=""redAsterick"">*</span> {Resources.Resource.warning_contains_mercury}</td></tr>"))
                                TD.Controls.Add(New LiteralControl("<tr><td colspan=17><img height=""1"" src=""images/spacer.gif"" width=""20"" alt="""">"))
                            End If

                            TD.Controls.Add(New LiteralControl("</tr>"))
                        ElseIf arrKits IsNot Nothing Then 'kit
                            Dim k As Kit

                            k = arrKits(n - arrParts.Length) 'kit array starts from 0
                            isValidPart = True
                            If k.ListPrice = 0 Then
                                isValidPart = False
                            End If
                            RowColor = IIf((n Mod 2 = 0), "FFFFFF", "F2F5F8")

                            If k.IsFRB Then
                                strDescription = ("<span class=tableHeader><span class=""redAsterick"">*</span>" + Resources.Resource.el_Price_FRB + "</span> " + k.Description)
                            Else
                                strDescription = k.Description
                            End If

                            If k.IsFRB Then
                                strListPrice = (k.ListPrice + k.CoreCharge).ToString("C")
                            Else
                                strListPrice = k.ListPrice.ToString("C")
                            End If

                            TD.Controls.Add(New LiteralControl("<tr>"))
                            TD.Controls.Add(New LiteralControl("<td bgcolor=""D5DEE9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>"))
                            TD.Controls.Add(New LiteralControl($"<td bgcolor=""{RowColor}"" width=""80"" class=""tableHeader"">&nbsp;&nbsp;{k.Model.Substring(0, k.Model.Length - 3)}</td>"))
                            TD.Controls.Add(New LiteralControl("<td bgcolor=""D5DEE9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>"))
                            TD.Controls.Add(New LiteralControl($"<td bgcolor=""{RowColor}"" class=""tableHeader"">&nbsp;&nbsp;{k.Model}</td>"))
                            TD.Controls.Add(New LiteralControl("<td bgcolor=""D5DEE9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>"))
                            TD.Controls.Add(New LiteralControl($"<td bgcolor=""{RowColor}"" class=""tableData"">&nbsp;&nbsp;{k.Model}</td>"))
                            TD.Controls.Add(New LiteralControl("<td bgcolor=""D5DEE9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>"))
                            TD.Controls.Add(New LiteralControl($"<td bgcolor=""{RowColor}"" class=""tableHeader"">&nbsp;&nbsp;KIT:{k.Description}&nbsp;</td>"))
                            TD.Controls.Add(New LiteralControl("<td bgcolor=""D5DEE9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>"))

                            If (k.IsFRB) Then
                                TD.Controls.Add(New LiteralControl("<tr><td colspan=17 bgcolor=""#d5dee9""><span class=""legalCopy""><span class=""redAsterick"">*</span>" & Resources.Resource.el_Price_FRB & Resources.Resource.el_Price_FRB1))
                                TD.Controls.Add(New LiteralControl(k.CoreCharge.ToString("C")))
                                TD.Controls.Add(New LiteralControl(Resources.Resource.el_Price_FRB2))
                                TD.Controls.Add(New LiteralControl(k.CoreCharge.ToString("C") & Resources.Resource.el_Price_FRB3 & "<br/></td></tr>"))
                                TD.Controls.Add(New LiteralControl("<tr><td colspan=17><img height=""1"" src=""images/spacer.gif"" width=""20"" alt="""">"))
                            ElseIf (k.HasCoreCharge) Then
                                TD.Controls.Add(New LiteralControl("<tr><td colspan=17 bgcolor=""#d5dee9""><span class=""legalCopy""><span class=""redAsterick"">*</span>" & Resources.Resource.el_Price_FRB & Resources.Resource.el_Price_FRB1))
                                TD.Controls.Add(New LiteralControl(k.CoreCharge.ToString("C") & Resources.Resource.el_Price_FRB2))
                                TD.Controls.Add(New LiteralControl(k.CoreCharge.ToString("C") & Resources.Resource.el_Price_FRB3 & "<br/></td></tr>"))
                                TD.Controls.Add(New LiteralControl("<tr><td colspan=17><img height=""1"" src=""images/spacer.gif"" width=""20"" alt="""">"))
                            End If

                            TD.Controls.Add(New LiteralControl("</tr>"))
                        End If
                    Next

                    tdLegend.Visible = isAnyPromoItem
                    TR.Cells.Add(TD)
                    DisplayResultsTable.Rows.Add(TR)

                    If TotalPages > 1 Then
                        ' Create the HTML for pagination when required
                        Dim sb_pagination As New StringBuilder

                        ' Show "Previous" link on all but the first page.
                        If StartPageNum > 0 Then
                            sb_pagination.Append($"<a href=""PartsPLUSResults.aspx?sType=parts&PageNum={PrevPage}"" class=""tableData"" title=""Go to Previous page of search results."">
                                <img src=""images/sp_int_leftArrow_btn.gif"" border=""0"" alt=""Go to Previous page of search results."">&nbsp;{Resources.Resource.el_Previous}</a>")
                        End If

                        ' Add the "Current of Total" page counter
                        sb_pagination.Append($"&nbsp;&nbsp;&nbsp;Page {PageNum} {Resources.Resource.el_of} {TotalPages}&nbsp;&nbsp;&nbsp;&nbsp;")

                        If EndPageNum < (TotalReturned - 1) Then
                            sb_pagination.Append($"<a href=""PartsPLUSResults.aspx?sType=parts&PageNum={NextPage}"" class=""tableData"" title=""Go to Next page of search results."">
                                {Resources.Resource.el_Next}&nbsp;<img src=""images/sp_int_rightArrow_btn.gif"" border=""0"" alt=""Go to Next page of search results.""></a>")
                        End If

                        PageLabel.Text = sb_pagination.ToString()
                        PageLabel2.Text = sb_pagination.ToString()
                    End If

                    SearchCopyLabel.Text = (Resources.Resource.el_ItemSearch) '"The following items match your search.")
                    If TotalReturned > 15 Then
                        BackToTopLabel.Text = ($"&nbsp;&nbsp;&nbsp;&nbsp;<a href=""#top""><img src=""{Resources.Resource.repair_sny_type_3_img}"" border=""0""
                                alt=""Go back to the top of this page""></a>")
                    End If
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Sub DisplayModels(ByVal PageNumber As Integer, ByVal Total As Integer)
            Try
                Dim TR As New TableRow
                Dim TD As New TableCell
                'Dim p As Sony.US.ServicesPLUS.Core.Part
                Dim arrModels As Array = Nothing
                Dim m As Sony.US.ServicesPLUS.Core.Model
                Dim TotalPages As Integer
                Dim StartPageNum As Integer = (PageNumber - 1) * Total
                Dim EndPageNum As Integer = (PageNumber * Total) - 1
                Dim PrevPage As Integer = PageNumber - 1
                Dim NextPage As Integer = PageNumber + 1
                Dim RowColor As String
                Dim n As Integer

                PageLabel.Text = ""
                SearchCopyLabel.Text = ""
                BackToTopLabel.Text = ""
                DisplayResultsTable.Rows.Clear()

                If Session.Item("models") Is Nothing Then
                    ErrorLabel.Text = ("No results found.")
                Else
                    arrModels = Session.Item("models")
                    TD.Controls.Add(New LiteralControl($"<tr>
                        <th width=""120"" bgColor=""#d5dee9"" class=""tableHeader"">&nbsp;&nbsp;{Resources.Resource.el_ModelNumber}</th>
                        <td width=""1""><img height=""15"" src=""images/spacer.gif"" width=""1"" alt=""""></td>
                        <th width=""250"" bgColor=""#d5dee9"" class=""tableHeader"">&nbsp;&nbsp;{Resources.Resource.el_ModelDescription}</th>
                        <td width=""1""><img height=""15"" src=""images/spacer.gif"" width=""1"" alt=""""></td>
                        <td bgColor=""#d5dee9"">&nbsp;</td>
                    </tr>"))

                    If arrModels.Length > 50 Then
                        TotalPages = Int(arrModels.Length / Total) + IIf(0 < (arrModels.Length Mod Total), 1, 0)
                    Else
                        TotalPages = 1
                        PageNum = 1
                    End If

                    If (0 > StartPageNum) Then
                        StartPageNum = 0
                    End If

                    If EndPageNum >= arrModels.Length Then
                        EndPageNum = arrModels.Length - 1
                    End If

                    If StartPageNum > arrModels.Length Then
                        ErrorLabel.Text = (Resources.Resource.el_ModelSearch) '"The page number chosen is beyond the number of models for this search.")
                    Else
                        For n = StartPageNum To EndPageNum
                            m = arrModels(n)
                            RowColor = IIf((n Mod 2 = 0), "FFFFFF", "F2F5F8")

                            TD.Controls.Add(New LiteralControl($"<tr>
                                <td width=""120"" bgcolor=""{RowColor}"" class=""tableHeader"">&nbsp;&nbsp;<a href=""PartsPLUSSearch.aspx?ArrayVal={n}""
                                    title=""Go to the parts detail page for model {m.ModelID}"">{m.ModelID}</a></td>
                                <td width=""1"" bgcolor=""{RowColor}"">&nbsp;</td>
                                <td width=""250"" bgcolor=""{RowColor}"" class=""tableHeader"">&nbsp;&nbsp;&nbsp;<a href=""PartsPLUSSearch.aspx?ArrayVal={n}""
                                    title=""Go to the parts detail page for model {m.ModelID}"">{m.Description}</a></td>
                                <td width=""1"" bgcolor=""{RowColor}"">&nbsp;</td>
                                <td bgcolor=""{RowColor}"">&nbsp;&nbsp;&nbsp;<span class=""tableData"">{m.Destination}<br/>
                                    &nbsp;&nbsp;&nbsp;&nbsp;{m.SerialRange}</span></td>
                            </tr>"))
                        Next

                        TR.Cells.Add(TD)
                        DisplayResultsTable.Rows.Add(TR)

                        ' Create the pagination HTML, if required.
                        If TotalPages > 1 Then
                            Dim sb_pagination As New StringBuilder

                            ' Show "Previous" link on all but the first page.
                            If StartPageNum > 0 Then
                                sb_pagination.Append($"<a href=""PartsPLUSResults.aspx?sType=models&PageNum={PrevPage}"" class=""tableData"" title=""Go to Previous page of search results."">
                                    <img src=""images/sp_int_leftArrow_btn.gif"" border=""0"" alt=""Go to Previous page of search results."">&nbsp;{Resources.Resource.el_Previous}</a>")
                            End If

                            ' Add the "Current of Total" page counter
                            sb_pagination.Append($"&nbsp;&nbsp;&nbsp;Page {PageNum} {Resources.Resource.el_of} {TotalPages}&nbsp;&nbsp;&nbsp;&nbsp;")

                            ' Show "Next" link on all but the last page.
                            If EndPageNum < (arrModels.Length - 1) Then
                                sb_pagination.Append($"<a href=""PartsPLUSResults.aspx?sType=models&PageNum={NextPage}"" class=""tableData"" title=""Go to Next page of search results."">
                                    {Resources.Resource.el_Next}&nbsp;<img src=""images/sp_int_rightArrow_btn.gif"" border=""0"" alt=""Go to Next page of search results.""></a>")
                            End If

                            PageLabel.Text = sb_pagination.ToString()
                            PageLabel2.Text = sb_pagination.ToString()
                        End If

                        SearchCopyLabel.Text = Resources.Resource.el_ItemSearch1 '"The following items match your search. Please click a model to view the available parts and diagrams.")
                        If arrModels.Length > 15 Then
                            BackToTopLabel.Text = ($"&nbsp;&nbsp;&nbsp;&nbsp;<a href=""#top""><img src=""{Resources.Resource.repair_sny_type_3_img}"" border=""0""
                                alt=""Go back to the top of this page""></a>")
                        End If
                    End If
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Sub DisplayKits(ByVal PageNumber As Integer, ByVal Total As Integer)
            PageLabel.Text = ""
            SearchCopyLabel.Text = ""
            BackToTopLabel.Text = ""

            DisplayResultsTable.Rows.Clear()
            Dim TR As New TableRow
            Dim TD As New TableCell
            'Dim kits() As Sony.US.ServicesPLUS.Core.Kit'6994
            Dim arrKits As Array = Session.Item("kits")
            Dim k As Kit
            Dim promoManager As PromotionManager = New PromotionManager

            If arrKits Is Nothing Then
                ErrorLabel.Text = (Resources.Resource.el_Search_NoResult) '"No results found.")
            Else
                Try
                    ' Header Row
                    TD.Controls.Add(New LiteralControl($"<tr>
                        {conSpacerTableCell}
                        <td bgcolor=""D5DEE9"" width=""80"" class=""tableHeader"" height=""28"">&nbsp;&nbsp;{Resources.Resource.el_PartNumber}</td>
                        {conSpacerTableCell}
                        <td bgcolor=""D5DEE9"" class=""tableHeader"" height=""28"">&nbsp;&nbsp;{Resources.Resource.el_Description}&nbsp;</td>
                        {conSpacerTableCell}
                        <td bgcolor = ""D5DEE9"" width=""130"" Class=""tableHeader"" height=""28"">&nbsp;&nbsp;{IIf(HttpContextManager.GlobalData.IsCanada, Resources.Resource.el_ListPrice_CAD, Resources.Resource.el_ListPrice)}&nbsp;</td>
                        {conSpacerTableCell}
                        <td bgcolor=""D5DEE9"" width=""26"" Class=""tableHeader"" align=""right"" height=""28"">{Resources.Resource.el_AddCart}<br/>Cart</td>
                        {conSpacerTableCell}
                        <td bgcolor=""D5DEE9"" width=""26"" class=""tableHeader"" align=""left"" height=""28"">&nbsp;{Resources.Resource.el_MyCat}</td>
                        {conSpacerTableCell}
                        <td bgcolor=""D5DEE9"" width=""26"" class=""tableHeader"" align=""right"" height=""28""><img src=""images/spacer.gif"" width=""1"" height=""28"" alt=""""></td>
                        {conSpacerTableCell}
                    </tr>"))

                    TD.Controls.Add(New LiteralControl("<tr>
                        <td bgcolor=""D5DEE9"" colspan=""13""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>
                    </tr>"))

                    Dim TotalPages As Integer
                    'make total smaller for kits
                    Total = 5
                    If arrKits.Length > 5 Then
                        TotalPages = Int(arrKits.Length / Total) + IIf(0 < (arrKits.Length Mod Total), 1, 0)
                    Else
                        TotalPages = 1
                        PageNum = 1
                    End If

                    'check to make sure pagenum does not exceeed totalpages
                    'if it does set pagenum to totalpages
                    If (PageNumber > TotalPages) Then
                        PageNumber = TotalPages
                        PageNum = TotalPages
                    End If

                    Dim StartPageNum As Integer = (PageNumber - 1) * Total
                    Dim EndPageNum As Integer = (PageNumber * Total) - 1
                    If (0 > StartPageNum) Then StartPageNum = 0
                    If EndPageNum >= arrKits.Length Then EndPageNum = arrKits.Length - 1
                    Dim PrevPage As Integer = PageNumber - 1
                    Dim NextPage As Integer = PageNumber + 1

                    Dim RowColor As String
                    Dim n As Integer = 0
                    Dim isFRB As Boolean
                    Dim haveCoreCharge As Boolean
                    Dim strDescription As String
                    Dim strListPrice As String
                    Dim hasPromo As Boolean = False
                    Dim isValidPart As Boolean

                    tdLegend.Visible = False

                    For n = StartPageNum To EndPageNum
                        k = arrKits(n)
                        isValidPart = True
                        If k.ListPrice = 0 Then isValidPart = False
                        RowColor = IIf((n Mod 2 = 0), "FFFFFF", "F2F5F8")

                        ' Kit Number and Description
                        TD.Controls.Add(New LiteralControl($"<tr>
                                <td bgcolor=""D5DEE9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>
                                <td bgcolor=""{RowColor}"" width=""80"" class=""tableHeader"">&nbsp;&nbsp;{k.Model}</td>
                                <td bgcolor=""D5DEE9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>
                                <td bgcolor={RowColor} class=""tableHeader"">&nbsp;&nbsp;" & k.Description & "&nbsp;</td>
                                <td bgcolor=""D5DEE9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>"))

                        Dim strPriceStar As String = ""
                        If promoManager.IsPromotionPart(k.PartNumber) Then
                            strPriceStar &= "<span style=""font-family: Wingdings; color: red; font-size: 12px;"">v</span>"
                            hasPromo = True
                        End If

                        ' Availability / Your Price
                        If isValidPart Then
                            'Check if the Customer has a valid SAP account to show "Your Price"
                            If (customer IsNot Nothing) AndAlso (customer.SAPBillToAccounts.Length > 0 Or customer.SISLegacyBillToAccounts.Length > 0) Then
                                TD.Controls.Add(New LiteralControl($"<td bgcolor={RowColor} width=""130"" class=""tableHeader"">&nbsp;&nbsp;
                                    {k.ListPrice.ToString("C")}{strPriceStar}&nbsp;&nbsp;&nbsp;<a class=""tableHeader"" href=""#""
                                    onclick=""mywin=window.open('yourprice.aspx?kitVal={n}&isKit=1','','{CON_POPUP_FEATURES}'); return false;""
                                    title=""Click to get live price and availability for this item."">{Resources.Resource.el_YourPrice}</a></td>"))
                            Else ' Show "Availability"
                                TD.Controls.Add(New LiteralControl($"<td bgcolor={RowColor} width=""130"" class=""tableHeader"">&nbsp;&nbsp;
                                    {(k.ListPrice + k.CoreCharge).ToString("C")}{strPriceStar}&nbsp;&nbsp;&nbsp;<a class=""tableHeader"" href=""#""
                                    onclick=""mywin=window.open('Availability.aspx?kitVal={n}&isKit=1','','{CON_POPUP_FEATURES}'); return false;""
                                    title=""Click to get live price and availability for this item."">{Resources.Resource.el_Availability}</a></span></td>"))
                            End If
                        Else
                            TD.Controls.Add(New LiteralControl($"<td bgcolor={RowColor} class=""legalCopy"">{Resources.Resource.el_CallInfo}</td>"))
                        End If
                        TD.Controls.Add(New LiteralControl("<td bgcolor=""D5DEE9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>"))

                        ' Add to Cart
                        If isValidPart Then
                            If customer Is Nothing Then
                                TD.Controls.Add(New LiteralControl($"<td bgcolor={RowColor} width=""26"" class=""tableData"" align=""center"">
                                    <a href=""NotLoggedInAddToCart.aspx?stype={SearchType}&post=ppr&KitLineNo={n}""
                                        title=""Go to the login page to add this item to your cart."">
                                    <img src=""images/sp_int_add2Cart_btn.gif"" width=""29"" height=""20"" border=""0"" alt=""Add to Cart""></a></td>"))
                            Else
                                If kits IsNot Nothing Then
                                    hidespstextqty.Text = 1
                                    TD.Controls.Add(New LiteralControl($"<td bgcolor={RowColor} width=""26"" class=""tableData"" align=""center"">
                                        <a href=""PartsPLUSResults.aspx?kitVal={n}&atc=true&stype={SearchType}&PageNum={PageNum}""
                                            title=""Add part number {k.PartNumber} to your shopping cart"">
                                        <img src=""images/sp_int_add2Cart_btn.gif"" width=""29"" height=""20"" border=""0"" alt=""Add to Cart"" id =""AddToCart{n}""
                                        onclick=""return AddToCart_onclick('{k.PartNumber}')""></a></td>"))
                                Else
                                    TD.Controls.Add(New LiteralControl($"<td bgcolor={RowColor} width=""26"" class=""tableData"" align=""center"">
                                        <a href=""PartsPLUSResults.aspx?kitVal={n}&atc=true&stype={SearchType}&PageNum={PageNum}"">
                                        <img src=""images/sp_int_add2Cart_btn.gif"" width=""29"" height=""20"" border=""0"" alt=""Add to Cart"" id =""AddToCart{n}""></a></td>")) '6994
                                End If
                            End If
                        Else
                            TD.Controls.Add(New LiteralControl($"<td bgcolor={RowColor} width=""1""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>"))
                        End If
                        TD.Controls.Add(New LiteralControl("<td bgcolor=""D5DEE9"" width=""1"" class=""tableData"" align=""right""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>"))

                        ' Add to Catalog
                        If isValidPart Then
                            If customer Is Nothing Then
                                TD.Controls.Add(New LiteralControl($"<td bgcolor={RowColor} width=""26"" class=""tableData"" align=""center"">
                                    <a href=""NotLoggedInAddToCatalog.aspx?stype={SearchType}&post=ppr&KitLineNo={n}""
                                        title=""Go to the login page to add this item to your catalog."">
                                    <img src=""images/sp_int_add2myCat_btn.gif"" width=""29"" height=""20"" border=""0"" alt=""Add to My Catalog""></a></td>"))
                            Else
                                TD.Controls.Add(New LiteralControl($"<td bgcolor={RowColor} width=""26"" class=""tableData"" align=""center"">
                                    <a href=""PartsPLUSResults.aspx?pValue={n}&atmc=true&stype={SearchType}&PageNum={PageNum}""
                                        title=""Save this item to your catalog."">
                                    <img src=""images/sp_int_add2myCat_btn.gif"" width=""29"" height=""20"" border=""0"" alt=""Add to My Catalog""></a></td>"))
                            End If
                        Else
                            TD.Controls.Add(New LiteralControl($"<td bgcolor={RowColor} width=""1""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>"))
                        End If
                        TD.Controls.Add(New LiteralControl("<td bgcolor=""D5DEE9"" width=""1"" class=""tableData"" align=""right""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>"))
                        TD.Controls.Add(New LiteralControl($"<td bgcolor={RowColor} width=""26"" class=""tableData"" align=""center""><img src=""images/spacer.gif"" width=""29"" height=""20"" alt=""""></td>"))
                        TD.Controls.Add(New LiteralControl("<td bgcolor=""D5DEE9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>"))
                        TD.Controls.Add(New LiteralControl("</tr>"))

                        For Each i As KitItem In k.Items
                            isFRB = i.Product.IsFRB
                            haveCoreCharge = i.Product.HasCoreCharge

                            If isFRB Then
                                strDescription = ("<span class=tableHeader><span class=""redAsterick"">*</span>FRB:</span> " & i.Product.Description)
                            Else
                                strDescription = (i.Product.Description)
                            End If


                            If (isFRB Or haveCoreCharge) Then
                                strListPrice = String.Format("{0:c}", i.Product.ListPrice + i.Product.CoreCharge)
                            Else
                                strListPrice = String.Format("{0:c}", i.Product.ListPrice)
                            End If

                            TD.Controls.Add(New LiteralControl($"<tr>
                                {conSpacerTableCell}
                                <td width=""80"" class=""tableData"">&nbsp;&nbsp;{i.Product.PartNumber}</td>
                                {conSpacerTableCell}
                                <td class=""tableData"">&nbsp;&nbsp;{strDescription}&nbsp;</td>
                                {conSpacerTableCell}
                                <td width=""130"" class=""tableData"">&nbsp;&nbsp;{strListPrice}</td>
                                {conSpacerTableCell}
                                <td width=""26"" class=""tableData"" align=""center"">{i.Quantity}</td>
                                {conSpacerTableCell}
                                <td width=""26"" class=""tableData"" align=""center""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>
                                {conSpacerTableCell}
                                <td width=""26"" class=""tableData"" align=""center""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>
                                {conSpacerTableCell}
                            </tr>"))

                            Dim coreCharge = String.Format("{0: c}", i.Product.CoreCharge)
                            If (isFRB) Then
                                TD.Controls.Add(New LiteralControl($"<tr><td colspan=""13"" bgcolor=""#d5dee9"" class=""legalCopy"">
                                    <span class=""redAsterick"">*</span>{Resources.Resource.el_Price_FRB} {Resources.Resource.el_Price_FRB1} {coreCharge}
                                    {Resources.Resource.el_Price_FRB2} {coreCharge} {Resources.Resource.el_Price_FRB3}</td></tr>"))
                                TD.Controls.Add(New LiteralControl("<tr><td colspan=""13""><img height=""1"" src=""images/spacer.gif"" width=""20"" alt=""""></td></tr>"))
                            ElseIf (i.Product.HasCoreCharge) Then
                                TD.Controls.Add(New LiteralControl($"<tr><td colspan=""13"" bgcolor=""#d5dee9"" class=""legalCopy"">
                                    <span class=""redAsterick"">*</span>{Resources.Resource.el_Price_FRB1} {coreCharge}
                                    {Resources.Resource.el_Price_FRB2} {coreCharge} {Resources.Resource.el_Price_FRB3}</td></tr>"))
                                TD.Controls.Add(New LiteralControl("<tr><td colspan=""13""><img height=""1"" src=""images/spacer.gif"" width=""20"" alt=""""></td></tr>"))
                            End If
                            'TD.Controls.Add(New LiteralControl("</tr>"))
                        Next
                    Next

                    ' -- display legend only if there is at least one part with promotion
                    tdLegend.Visible = hasPromo

                    TR.Cells.Add(TD)
                    DisplayResultsTable.Rows.Add(TR)

                    If TotalPages > 1 Then
                        ' Create the HTML for pagination when required
                        Dim sb_pagination As New StringBuilder

                        ' Show "Previous" link on all but the first page.
                        If StartPageNum > 0 Then
                            sb_pagination.Append($"<a href=""PartsPLUSResults.aspx?sType=kits&PageNum={PrevPage}"" class=""tableData"" title=""Go to Previous page of search results."">
                                <img src=""images/sp_int_leftArrow_btn.gif"" border=""0"" alt=""Go to Previous page of search results."">&nbsp;{Resources.Resource.el_Previous}</a>")
                        End If

                        ' Add the "Current of Total" page counter
                        sb_pagination.Append($"&nbsp;&nbsp;&nbsp;Page {PageNum} {Resources.Resource.el_of} {TotalPages}&nbsp;&nbsp;&nbsp;&nbsp;")

                        If EndPageNum < (arrKits.Length - 1) Then
                            sb_pagination.Append($"<a href=""PartsPLUSResults.aspx?sType=kits&PageNum={NextPage}"" class=""tableData"" title=""Go to Next page of search results."">
                                {Resources.Resource.el_Next}&nbsp;<img src=""images/sp_int_rightArrow_btn.gif"" border=""0"" alt=""Go to Next page of search results.""></a>")
                        End If

                        PageLabel.Text = sb_pagination.ToString()
                        PageLabel2.Text = sb_pagination.ToString()
                    End If

                    SearchCopyLabel.Text = (Resources.Resource.el_ItemSearch) ' "The following items match your search."
                    If arrKits.Length > 2 Then
                        BackToTopLabel.Text = ("&nbsp;&nbsp;&nbsp;&nbsp;<a href=""#top""><img src=""images/sp_int_back2top_btn.gif"" width=""78"" height=""28"" border=""0"" alt=""Back to Top""></a>")
                    End If
                Catch ex As Exception
                    ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                End Try
            End If

        End Sub

        Private Sub AddPartToCart(ByVal whichPart As Integer)
            Dim the_part As Sony.US.ServicesPLUS.Core.Part
            Dim deliveryMethod As ShoppingCartItem.DeliveryMethod
            Dim cart As ShoppingCart
            Dim sm As New ShoppingCartManager

            the_part = parts.GetValue(whichPart)
            deliveryMethod = ShoppingCartManager.getDeliveryMethod(the_part.ProgramCode)
            cart = getShoppingCart(customer, deliveryMethod, ErrorLabel)

            '-- if we can't add the item to the cart then we need to save the existing cart and create a new cart for the item
            If Not IsCurrentCartSameType(cart) Then
                ErrorLabel.Visible = True
                If cart.Type <> ShoppingCart.enumCartType.PrintOnDemand Then
                    ErrorLabel.Text = Resources.Resource.el_CartItem1 '"The item is put to different cart from the previous cart."
                Else
                    ErrorLabel.Text = Resources.Resource.el_Cart_PrintItem '"The print on demand item is put to different cart from the previous cart."
                End If
            End If

            '-- added to make sure the model number show up in MyCatalog. - dwd
            sm.AddProductToCart(HttpContextManager.GlobalData, cart, the_part, ShoppingCartItem.ItemOriginationType.Search, ShoppingCartItem.DeliveryMethod.Ship)
            'sm.AddProductToCart(cart, the_part.PartNumber, "1")
            HttpContext.Current.Session("carts") = cart
        End Sub

        Private Sub AddKitToCart(ByVal whichKit As Integer)
            Dim cart As ShoppingCart
            Dim sm As New ShoppingCartManager
            Dim kit As Kit
            'kits = Session.Item("kits")

            kit = kits.GetValue(whichKit)
            cart = getShoppingCart(customer, ShoppingCartItem.DeliveryMethod.Ship, ErrorLabel) 'Kit always is shipped
            sm.AddProductToCart(HttpContextManager.GlobalData, cart, kit, ShoppingCartItem.ItemOriginationType.Search)
            HttpContext.Current.Session("carts") = cart
        End Sub

        Private Sub AddPartToCart()
            Try
                Dim pm As New PaymentManager
                Dim itemIndex As Int32

                If customer Is Nothing Then
                    ErrorLabel.Text = (Resources.Resource.el_Cart_MustLogin) '"You must be logged in to add items to your cart.")
                    Return
                Else
                    If SearchType = "parts" Then
                        If Request.QueryString("PValue") IsNot Nothing Then
                            itemIndex = Request.QueryString("PValue")
                            AddPartToCart(itemIndex)
                        Else 'it is the kit
                            itemIndex = Request.QueryString("kitVal")
                            AddKitToCart(itemIndex)
                        End If
                    ElseIf SearchType = "kits" Then
                        itemIndex = Request.QueryString("kitVal")
                        AddKitToCart(itemIndex)
                    Else
                        ErrorLabel.Text = Resources.Resource.el_Cart_Unable '"Unable to add item to cart. Missing result type from query string."
                        Return
                    End If
                    'Response.Redirect("PartsPlusResults.aspx?stype=" + SearchType + "&PageNum=" + PageNum.ToString())
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Sub AddPartToMyCatalog()
            Try
                Dim cm As New CatalogManager
                Dim validPart As Boolean

                If customer Is Nothing Then
                    ErrorLabel.Text = (Resources.Resource.el_Catalog_MustLogin) '"You must be logged in to add items to your catalog.")
                    Return
                Else
                    If SearchType = "parts" Then
                        Dim the_part As Sony.US.ServicesPLUS.Core.Part
                        Dim whichPart As Int32
                        If Request.QueryString("PValue") IsNot Nothing Then
                            ' 2018-12-14 Added below validation to prevent unhandled exceptions.
                            If Not Integer.TryParse(Request.QueryString("PValue"), whichPart) Then
                                Throw New FormatException($"SearchType of 'parts' was given a non-integer value for PValue query string. Value: {Request.QueryString("PValue")}")
                            End If
                            'whichPart = Int32.Parse(Request.QueryString("PValue"))
                            If whichPart > parts.Length Or whichPart < 0 Then
                                validPart = False
                            Else
                                the_part = parts.GetValue(whichPart)
                                'Validation logic
                                If the_part Is Nothing OrElse (the_part.ReplacementPartNumber.StartsWith("991099") Or the_part.Category.CategoryCode = "INT" Or the_part.Category.CategoryCode = "HOL" Or the_part.ListPrice <= 0 Or the_part.ListPrice = Nothing) Then
                                    validPart = False
                                Else
                                    validPart = True
                                    If the_part.PartNumber <> the_part.ReplacementPartNumber Then
                                        ' Fetch data for the Replacement part in case it differs, and substitute it for the Catalog entry.
                                        Dim parts() = cm.QuickSearch(String.Empty, the_part.ReplacementPartNumber, String.Empty, String.Empty, HttpContextManager.GlobalData)
                                        If parts.Length > 0 Then
                                            the_part = Array.Find(parts, Function(x) (x.PartNumber = the_part.ReplacementPartNumber))
                                        End If
                                    End If

                                    cm.AddMyCatalogItem(customer, the_part)
                                End If
                            End If
                        ElseIf Request.QueryString("kitVal") IsNot Nothing Then
                            Dim kit As Kit
                            Dim whichKit As Int32
                            whichKit = Int32.Parse(Request.QueryString("kitVal"))
                            If whichKit > kits.Length Then
                                validPart = False
                            Else
                                kit = kits.GetValue(whichKit)
                                If kit Is Nothing OrElse (kit.ListPrice = Nothing Or kit.ListPrice <= 0) Then
                                    validPart = False
                                Else
                                    validPart = True
                                    cm.AddMyCatalogItem(customer, kit)
                                End If
                            End If
                        Else
                            Throw New ArgumentException("SearchType of 'parts' was given no PValue or kitVal query strings. User may be modifying URL.")
                        End If
                    ElseIf SearchType = "kits" Then
                        Dim kit As Kit
                        Dim whichKit As Int32
                        whichKit = Int32.Parse(Request.QueryString("kitVal"))
                        If whichKit > kits.Length Then
                            validPart = False
                        Else
                            kit = kits.GetValue(whichKit)
                            If kit Is Nothing And kit.ListPrice = Nothing Or kit.ListPrice <= 0 Then
                                validPart = False
                            Else
                                validPart = True
                                cm.AddMyCatalogItem(customer, kit)
                            End If
                        End If
                    Else
                        ErrorLabel.Text = Resources.Resource.el_Catalog_Unable '"Unable to add item to catalog. Missing result type in query string."
                        Return
                    End If

                    If validPart Then
                        Response.Redirect("PartsPlusResults.aspx?stype=" + SearchType + "&PageNum=" + PageNum.ToString())
                    Else
                        Page.ClientScript.RegisterStartupScript([GetType](), "ValidatePartAndKits", "ValidatePartAndKits()", True)
                    End If
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
			End Try
		End Sub

        Private Sub btnNewSearch_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnNewSearch.Click
            If HttpContextManager.Customer Is Nothing Then
                Response.Redirect("sony-parts.aspx")
            Else
                Response.Redirect("au-sony-parts.aspx")
            End If
        End Sub

        Protected Overrides Sub Finalize()
            MyBase.Finalize()
        End Sub
    End Class

End Namespace
