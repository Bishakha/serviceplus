Imports ServicesPlusException
Imports Sony.US.AuditLog
Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Process
Imports Sony.US.siam
Imports Sony.US.SIAMUtilities

Namespace ServicePLUSWebApp

    Partial Class SignIn
        Inherits SSL

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub


        Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Init
            InitializeComponent()
        End Sub

#End Region

        Private Sub TestSnoop()
            Dim Counter1, Counter2 As Integer
            Dim Keys(), subKeys() As String
            Dim HeaderColl As NameValueCollection
            Dim output As String = ""

            HeaderColl = Request.Headers
            Keys = HeaderColl.AllKeys

            For Counter1 = 0 To Keys.GetUpperBound(0)
                'Utilities.LogMessages("Key: " & Keys(Counter1))
                output &= Keys(Counter1) & ": "
                subKeys = HeaderColl.GetValues(Counter1) ' Get all values under this key.
                For Counter2 = 0 To subKeys.GetUpperBound(0)
                    'Response.Write("Value " & CStr(Counter2) & ": " & subKeys(Counter2) & "<br/>")
                    'Utilities.LogMessages("Value " & CStr(Counter2) & ": " & subKeys(Counter2))
                    If Counter2 > 0 Then output &= ", "
                    output &= subKeys(Counter2)
                Next Counter2
                output &= Environment.NewLine
            Next Counter1
            Utilities.LogMessages(output)
        End Sub

        Private Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load, Me.Load
            Try
                'If Not Session.Item("GlobalData") Is Nothing Then
                '    objGlobalData = Session.Item("GlobalData")
                'End If
                'If Session("customer") Is Nothing Then'7031
                setSession()
                'End If'7031
            Catch ex As Exception
                Dim homePage As String = String.Empty

                TestSnoop()
                Session.Add("HOMEPAGE", ex)

                If HttpContextManager.GlobalData.IsCanada Then
                    If HttpContextManager.GlobalData.IsFrench Then
                        homePage = ConfigurationData.Environment.IdentityURLs.LogoutFRCA
                    Else
                        homePage = ConfigurationData.Environment.IdentityURLs.LogoutENCA
                    End If
                Else
                    homePage = ConfigurationData.Environment.IdentityURLs.LogoutUS
                End If

                Response.Redirect(homePage, False)
            End Try
        End Sub

        Private Function DoCallback() As Boolean
            If Not Request.QueryString("callback") Is Nothing Then
                Dim param As String = Request.QueryString("param")
                Session.Add("RegisterReturnPage", param)
                Response.Flush()
                Response.End()
                Return True
            End If
            Session("RegisterReturnPage") = Nothing
            Return False
        End Function

        Private Sub setSession()
            Dim sFeature As String = "scrollbars=1,resizable=0,width=744,height=754,left=0,top=0"
            Dim idpUserID As String = String.Empty
            Dim idpEmail As String = String.Empty
            Dim dbEmail As String = String.Empty
            Dim idpFirstName As String = String.Empty
            Dim idpLastName As String = String.Empty
            Dim sCompanyName As String = String.Empty
            'Dim hInfo As String = String.Empty
            Dim customer As Customer
            Dim customer_list As List(Of Customer)
            Dim cm As New Sony.US.ServicesPLUS.Process.CustomerManager
            Dim pciLog As New PCILogger() '6524

            Try
                '-- lets clear the session variables --
                Session.Remove("carts")
                Session.Remove("order")
                Session.Remove("customer")

                ' Prepare the log entry
                pciLog.EventOriginApplication = "ServicesPLUS"
                pciLog.EventOriginApplicationLocation = Me.Page.GetType().Name
                pciLog.EventOriginMethod = "SignIn.setSession()"
                pciLog.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
                pciLog.OperationalUser = Environment.UserName
                pciLog.EventType = EventType.Login
                pciLog.EventDateTime = Date.Now.ToLongTimeString()
                pciLog.HTTPRequestObjectValues = pciLog.GetRequestContextValue()
                pciLog.IndicationSuccessFailure = "Success"
                pciLog.Message = "Login Success"

                Try
                    ' Fetch the headers from the IDP to process
                    If (HttpContext.Current.Request.ServerVariables("HTTP_IDP_UID") IsNot Nothing) Then
                        '2016-04-29 ASleight Shibboleth returns ID headers with different names than SiteMinder
                        idpUserID = HttpContext.Current.Request.ServerVariables("HTTP_IDP_UID")
                        idpEmail = HttpContext.Current.Request.ServerVariables("HTTP_IDP_MAIL")
                        idpFirstName = HttpContext.Current.Request.ServerVariables("HTTP_IDP_GIVENNAME")
                        idpLastName = HttpContext.Current.Request.ServerVariables("HTTP_IDP_SN")
                    ElseIf ConfigurationData.GeneralSettings.Environment = "Development" Then
                        If Session("testemail") IsNot Nothing Then
                            idpUserID = Session("testldapid").ToString()
                            idpEmail = Session("testemail").ToString()
                        Else
                            idpUserID = "1000313993"
                            idpEmail = "andrew.sleight@sony.com"
                        End If
                    End If
                Catch ex As Exception
                    Utilities.LogMessages("SIGNIN: Failed to set variables from HTTP Headers. " & ex.Message)
                    Throw ex
                End Try

                dbEmail = cm.GetActiveCustomerEmailAddress(idpUserID)
                If Not String.IsNullOrEmpty(dbEmail) Then
                    If dbEmail.ToUpper().Trim() <> idpEmail.ToUpper().Trim() Then
                        'If Not (sEmailID.ToUpper().Contains(emailAddress.ToUpper().Trim())) Then
                        Try
                            Dim ht As New Hashtable From {
                                {"emailAddress", idpEmail},
                                {"ldapId", idpUserID}
                            }
                            Session("EmailIdNotMatched") = ht
                            RedirectAndComplete("default.aspx")
                        Catch ex As Exception
                            Utilities.LogMessages("SIGNIN: Failed to handle differing emails. " & ex.Message)
                            Throw ex
                        End Try
                    End If
                End If

                customer_list = cm.GetCustomersWithLDAPID(idpUserID)
                If customer_list.Count = 2 Then
                    Try
                        Dim inactiveCustomer = From c In customer_list Where c.EmailAddress.ToUpper().Trim() = idpEmail.ToUpper().Trim() And c.StatusCode = 3
                        If inactiveCustomer.Count > 0 Then
                            For Each c1 In inactiveCustomer
                                If c1.EmailAddress = idpEmail And c1.EmailAddress Then
                                    cm.DeleteCustomerRecord(c1.CustomerID)
                                End If
                            Next
                        End If
                        Dim activeCustomer = From c3 In customer_list Where c3.EmailAddress.ToUpper().Trim() <> idpEmail.ToUpper().Trim() And c3.StatusCode = 1
                        If activeCustomer.Count = 1 Then
                            cm.UpdateLdapid(idpUserID, activeCustomer.First().CustomerID)
                        End If
                    Catch ex As Exception
                        Utilities.LogMessages("SIGNIN: Failed to handle multiple user records. " & ex.Message)
                        Throw ex
                    End Try
                End If

                customer = cm.GetCustomer(idpUserID, idpEmail, idpFirstName, idpLastName)
                If customer Is Nothing Then
                    customer = New Customer
                    'Session.Add("NewUserData", sFirstName + "*" + sLastName + "*" + sEmailID) ' 6909 v9
                    If Session("existingSMcustomer") IsNot Nothing Then
                        customer = Session("existingSMcustomer")
                    Else
                        Dim securityAdmin As New SecurityAdministrator
                        Dim userData As String = String.Empty
                        Dim homePage As String = String.Empty

                        customer.LdapID = idpUserID
                        customer.EmailAddress = idpEmail
                        customer.FirstName = idpFirstName
                        customer.LastName = idpLastName
                        customer.UserName = idpEmail
                        Try
                            userData = securityAdmin.SearchUser(idpEmail)   ' Search LDAP for the user via SOAP Web Service
                        Catch ex As Exception
                            Utilities.LogMessages("SIGNIN: Failed to run SearchUser by IDP Email: " & idpEmail)
                            Throw ex
                        End Try
                        Try
                            If (Not String.IsNullOrEmpty(userData)) Then
                                Dim userSplit As String() = userData.Split("*")
                                If (userSplit.Length > 2) Then customer.CompanyName = userSplit(2).Trim()
                            End If
                        Catch ex As Exception
                            Utilities.LogMessages("SIGNIN: Failed to parse Data from SearchUser. " & ex.Message)
                            Throw ex
                        End Try
                    End If
                    ' Add Customer info to the PCI Log
                    pciLog.CustomerID = customer.CustomerID
                    pciLog.CustomerID_SequenceNumber = customer.SequenceNumber
                    pciLog.EmailAddress = customer.EmailAddress
                    pciLog.SIAM_ID = customer.SIAMIdentity
                    pciLog.LDAP_ID = customer.LdapID

                    HttpContextManager.Customer = customer
                    RedirectAndComplete("shared/Register-Existing-User.aspx")
                Else
                    pciLog.CustomerID = customer.CustomerID
                    pciLog.CustomerID_SequenceNumber = customer.SequenceNumber
                    pciLog.EmailAddress = customer.EmailAddress
                    pciLog.SIAM_ID = customer.SIAMIdentity
                    pciLog.LDAP_ID = customer.LdapID

                    Session.Add("StatusCode", customer.StatusCode)
                    If (customer.StatusCode = 3) Then
                        HttpContextManager.Customer = customer
                        RedirectAndComplete("shared/Register-Existing-User.aspx")
                    ElseIf (customer.StatusCode = 1) Then
                        If customer.LdapID.Trim() <> idpUserID.Trim() Then
                            cm.UpdateLdapid(idpUserID.Trim(), customer.CustomerID)
                        End If

                        ' Fetch any SAP Accounts, checking for invalid ones to remove.
                        Dim strInvalidAcct = New List(Of String)
                        If customer.UserType = "A" Or customer.UserType = "P" Then '6668
                            Dim objCoreAccount As Account = Nothing
                            Dim iIndex As Integer = 0
                            For Each objAccount As Account In customer.SAPBillToAccounts
                                objCoreAccount = cm.PopulateCustomerDetailWithSAPAccount(objAccount, HttpContextManager.GlobalData)
                                If objCoreAccount Is Nothing Or String.IsNullOrWhiteSpace(objCoreAccount.SoldToAccount) Or String.IsNullOrWhiteSpace(objCoreAccount.AccountNumber) Then
                                    strInvalidAcct.Add(objAccount.AccountNumber)
                                ElseIf objCoreAccount IsNot Nothing Then
                                    customer.SAPBillToAccounts(iIndex).SoldToAccount = objCoreAccount.SoldToAccount
                                End If
                                iIndex += 1
                            Next
                        End If

                        ' Remove the invalid account(s)
                        If strInvalidAcct.Count > 0 Then
                            For Each objAccount As Account In customer.SAPBillToAccounts
                                If (strInvalidAcct.Contains(objAccount.AccountNumber)) Then
                                    customer.RemoveAccount(objAccount)
                                End If
                            Next
                        End If

                        'If customer IsNot Nothing Then
                        '    If Not String.IsNullOrEmpty(Request.QueryString("SessionID")) Then
                        '        Dim pOutDataMgr As PunchOutDataManager = New PunchOutDataManager()
                        '        customer.IsPunchOutUser = pOutDataMgr.IsPunchOutSession(Request.QueryString("SessionID"))
                        '        If customer.IsPunchOutUser Then
                        '            Session("PunchOutSesison") = Request.QueryString("SessionID")
                        '        End If
                        '    End If
                        'End If
                        HttpContextManager.Customer = customer
                        SetUniqueRequestKey() ' Added by Arshad for fixing the Vulnerability with MutiSession Browser
                        ProcessRedirect()
                    Else
                        Dim statusCode As String = "null"
                        Dim ldapId As String = "null"
                        If customer IsNot Nothing Then
                            statusCode = customer.StatusCode
                            ldapId = customer.LdapID
                        End If
                        Throw New ArgumentException($"Customer.StatusCode was outside the range of expected values. Value = {statusCode}, LDAP ID = {ldapId}")
                    End If
                End If
            Catch ex As Exception
                pciLog.IndicationSuccessFailure = "Failure"
                pciLog.Message = "Login Failure. " & ex.Message
                Dim errorMessage = $"Login Failure. User Name: {idpUserID},  Message: {ex.Message}"
                If (ex.InnerException IsNot Nothing) Then errorMessage += ", Inner Exception: " + ex.InnerException.Message
                'errorMessage += "     <a href=""#"" onclick=""javascript:window.open('" + ConfigurationData.GeneralSettings.URL.contact_us_popup_link
                'errorMessage += "',null,'" + sFeature + "');"">contact us</a> for assistance and mention incident number ."
                'Dim objEx As New ServicesPlusBusinessException(errorMessage, ex)
                'objEx.setcustomMessage = True
                Utilities.LogMessages(errorMessage)
                Throw ex
            Finally
                pciLog.PushLogToMSMQ()
            End Try
        End Sub

        Private Sub SetUniqueRequestKey()
            Dim strRequestKey As String
            Dim objCookie As HttpCookie
            strRequestKey = System.Guid.NewGuid().ToString()
            objCookie = New HttpCookie("AuthToken", strRequestKey)
            Response.Cookies.Add(objCookie)
            Session.Add("AuthToken", strRequestKey)

            If ConfigurationData.GeneralSettings.NoCachPage.IndexOf(Me.GetType().Name.ToUpper) < 0 Then
                Response.Buffer = True
                Response.ExpiresAbsolute = Now().Subtract(New TimeSpan(1, 0, 0, 0))
                Response.Expires = 0
                Response.CacheControl = "no-cache"
            End If
        End Sub

        Private Sub ProcessRedirect()
            ' ASleight - The below section is now handled in Member.aspx.
            'If (HttpContextManager.Customer IsNot Nothing) And (ConfigurationData.GeneralSettings.Environment <> "Development") Then
            '    Dim customer = HttpContextManager.Customer
            '    Dim strURL As String = String.Empty
            '    'Dim siteCountryCode As String
            '    Dim cm As New CatalogManager

            '    ' If the user's profile is set to one country, but they're accessing the other country's site, redirect them to the right one
            '    'siteCountryCode = IIf(HttpContextManager.DomainURL.ToLower().Contains("sony.ca"), "CA", "US")
            '    'If (Not String.IsNullOrEmpty(customer.CountryCode)) AndAlso (customer.CountryCode <> siteCountryCode) Then
            '    '    If customer.CountryCode.ToUpper() = "CA" Then
            '    '        strURL = ConfigurationData.Environment.URL.CA
            '    '    Else
            '    '        strURL = ConfigurationData.Environment.URL.US
            '    '    End If
            '    '    strURL &= "/member.aspx?UserData=" & customer.SIAMIdentity '+ "�" + customer.EmailAddress
            '    '    Response.Redirect(strURL, False)
            '    '    HttpContext.Current.ApplicationInstance.CompleteRequest()
            '    '    Return
            '    'End If
            'End If

            If Not String.IsNullOrWhiteSpace(Request.QueryString("post")) Then
                Select Case Request.QueryString("post").ToLower()
                    Case "om"
                        RedirectAndComplete("sony-operation-manual.aspx")
                    Case "qo"
                        RedirectAndComplete("sony-parts.aspx")
                    Case "promopart"
                        RedirectAndComplete("parts-promotions.aspx")
                    Case "prc"
                        RedirectAndComplete("sony-service-agreements.aspx")
                    Case "prr"
                        RedirectAndComplete("Online-Product-Registration.aspx")
                    Case "ssm"
                        If Not String.IsNullOrWhiteSpace(Request.QueryString("ModelNumber")) Then
                            RedirectAndComplete($"sony-software-model-{Request.QueryString("ModelNumber")}.aspx")
                        Else
                            RedirectAndComplete("sony-software.aspx")
                        End If
                    Case "ssmfree"
                        RedirectAndComplete("SoftwareFreeDownload.aspx")
                    Case "ssp"
                        If Not String.IsNullOrWhiteSpace(Request.QueryString("PartNumber")) Then
                            RedirectAndComplete($"sony-software-part-{Request.QueryString("PartNumber")}.aspx")
                        Else
                            RedirectAndComplete("sony-training-catalog.aspx")
                        End If
                    Case "ppr"  ' 2018-06-04 ASleight - Added .ToLower() to Select statement for bug when logging in from
                        If Not String.IsNullOrWhiteSpace(Request.QueryString("stype")) Then
                            If Not String.IsNullOrWhiteSpace(Request.QueryString("PartLineNo")) Then
                                RedirectAndComplete($"PartsPLUSResults.aspx?stype={Request.QueryString("stype")}&PartLineNo={Request.QueryString("PartLineNo")}")
                            ElseIf Not String.IsNullOrWhiteSpace(Request.QueryString("KitLineNo")) Then
                                RedirectAndComplete($"PartsPLUSResults.aspx?stype={Request.QueryString("stype")}&KitLineNo={Request.QueryString("KitLineNo")}")
                            Else
                                RedirectAndComplete("sony-parts.aspx")
                            End If
                        Else
                            RedirectAndComplete("sony-parts.aspx")
                        End If
                    Case "pps"
                        If Not String.IsNullOrWhiteSpace(Request.QueryString("ArrayVal")) Then
                            RedirectAndComplete($"PartsPLUSSearch.aspx?ArrayVal={Request.QueryString("ArrayVal")}&returnfrom=signin")
                        Else
                            RedirectAndComplete("sony-parts.aspx")
                        End If
                    Case "ssrm", "spmsr", "sr", "srms"
                        If Not String.IsNullOrWhiteSpace(Request.QueryString("SPGSModel")) Then
                            Session.Add("ServiceModelNumber", Request.QueryString("SPGSModel"))
                            RedirectAndComplete("sony-service-sn.aspx")
                        Else
                            RedirectAndComplete("sony-repair.aspx")
                        End If
                    Case "spcd"
                        If Not String.IsNullOrWhiteSpace(Request.QueryString("groupid")) Then
                            RedirectAndComplete($"groupid{Request.QueryString("groupid")}.aspx")
                        Else
                            RedirectAndComplete("sony-part-catalog.aspx")
                        End If
                    Case "stcd"
                        If Not String.IsNullOrWhiteSpace(Request.QueryString("CourseLineno")) Then
                            Try     ' 2018-10-15 ASleight - Added safety in case the below code fails.
                                Dim itemNumber As Integer = Convert.ToInt16(Request.QueryString("CourseLineno"))
                                Dim courseSelected As CourseSchedule = CType(Session.Item("courseschedule"), ArrayList)(itemNumber)
                                Dim cr As New CourseReservation
                                cr.CourseSchedule = courseSelected
                                Session("trainingreservation") = cr
                                CloseAndRedirectCourse()
                            Catch ex As Exception
                                RedirectAndComplete("Member.aspx")
                            End Try
                        Else
                            RedirectAndComplete("sony-training-catalog.aspx")
                        End If
                    Case "stc2"
                        If Not String.IsNullOrWhiteSpace(Request.QueryString("mincatid")) Then
                            RedirectAndComplete("sony-training-catalog-2.aspx?mincatid=" + Request.QueryString("mincatid"))
                        Else
                            RedirectAndComplete("sony-training-catalog.aspx")
                        End If
                    Case "stcdatc"
                        If Not String.IsNullOrWhiteSpace(Request.QueryString("ItemNumber")) Then
                            RedirectAndComplete($"sony-training-class-details.aspx?ItemNumber={Request.QueryString("ItemNumber")}")
                        ElseIf Not String.IsNullOrWhiteSpace(Request.QueryString("ClassNumber")) Then
                            RedirectAndComplete($"sony-training-class-details.aspx?ClassNumber={Request.QueryString("ClassNumber")}")
                        ElseIf Not String.IsNullOrWhiteSpace(Request.QueryString("CourseNumber")) Then
                            RedirectAndComplete($"sony-training-class-details.aspx?CourseNumber={Request.QueryString("CourseNumber")}")
                        End If
                    Case "ewprod"
                        If Not String.IsNullOrWhiteSpace(Request.QueryString("ewc")) And Not String.IsNullOrWhiteSpace(Request.QueryString("pc")) Then
                            RedirectAndComplete($"Sony-EW-PurchaseInfo.aspx?ewc={Request.QueryString("ewc")}&pc={Request.QueryString("pc")}&mdl={Request.QueryString("mdl")}")
                        Else
                            RedirectAndComplete("sony-extended-warranties.aspx")
                        End If
                    Case "ewcert"
                        If Not String.IsNullOrWhiteSpace(Request.QueryString("onum")) Then
                            RedirectAndComplete("Sony-EWCertificate.aspx?onum=" + Request.QueryString("onum"))
                        End If
                    Case "mark"
                        RedirectAndComplete("shared/Marketing.aspx")
                    Case "taxaccount"
                        RedirectAndComplete("shared/Register-Account-Tax-Exempt.aspx")
                    Case "newuser"
                        RedirectAndComplete("shared/Register-Notifications.aspx")
                    Case "existinguser"
                        Dim statusCode As Integer = Session.Item("StatusCode")
                        If (statusCode = 3) Then
                            RedirectAndComplete("shared/Register-Existing-User.aspx")
                        ElseIf (statusCode = 1) Then
                            Session.Remove("ExistingRegister")
                            RedirectAndComplete("Member.aspx")
                        End If
                    Case "skbs"
                        RedirectAndComplete("sony-knowledge-base-search.aspx")
                    Case "sonyrepair"
                        RedirectAndComplete("sony-repair.aspx")
                    Case "ssrandm"
                        RedirectAndComplete("sony-service-repair-maintenance.aspx")
                    Case "pp1"
                        RedirectAndComplete("parts-promotions.aspx#promo1")
                    Case "rn"
                        RedirectAndComplete("shared/Register-Notifications.aspx")
                    Case "iframe"
                        RedirectAndComplete("shared/IFrames.aspx")
                    Case "rte"
                        RedirectAndComplete("shared/Register-Account-Tax-Exempt.aspx")
                    Case "rtu"
                        RedirectAndComplete("shared/Registration-Thank-You.aspx")
                    Case "token"
                        RedirectAndComplete("shared/Tokenization.aspx")
                    Case "reu"
                        RedirectAndComplete("shared/Register-Existing-User.aspx")
                    Case Else
                        RedirectAndComplete("Member.aspx")
                End Select
            Else
                If Session("LoginFrom") IsNot Nothing Then
                    If Session("LoginFrom").ToString() = "Bulletin" Then
                        RedirectAndComplete(Session("RedirectTo").ToString())
                    Else
                        RedirectAndComplete("Member.aspx")
                    End If
                ElseIf Not String.IsNullOrWhiteSpace(Request.QueryString("PartNumber")) Then 'from popup after selecting download free
                    'closeAndRedirectPart() 'go to member.aspx after log in
                    RedirectAndComplete("SoftwareFreeDownload.aspx")
                ElseIf Not String.IsNullOrWhiteSpace(Request.QueryString("TechBulletin")) Then
                    CloseAndRedirectTechBulletin()
                ElseIf Not String.IsNullOrWhiteSpace(Request.QueryString("CourseLineno")) Then
                    Dim itemNumber As Integer = Convert.ToInt16(Request.QueryString("CourseLineno"))
                    Dim courseSelected As CourseSchedule = CType(Session.Item("courseschedule"), ArrayList)(itemNumber)
                    Dim cr As New CourseReservation
                    cr.CourseSchedule = courseSelected
                    Session("trainingreservation") = cr
                    CloseAndRedirectCourse()
                ElseIf Not String.IsNullOrWhiteSpace(Request.QueryString("SPGSModel")) Then
                    CloseAndRedirectDepotRequest()
                ElseIf Not String.IsNullOrWhiteSpace(Request.QueryString("Promo")) Then
                    CloseAndRedirectPromoRequest(Request.QueryString("Promo"))
                ElseIf Not String.IsNullOrWhiteSpace(Request.QueryString("RAForm")) Then
                    CloseAndRedirectRAForm()
                Else
                    RedirectAndComplete("Member.aspx")
                End If
            End If
        End Sub

        Private Sub CloseAndRedirectPromoRequest(ByVal sPromoPath As String)
            RedirectAndComplete(sPromoPath)
        End Sub

        Private Sub CloseAndRedirectDepotRequest()
            Session.Add("ServiceModelNumber", Request.QueryString("SPGSModel"))
            RedirectAndComplete("sony-service-sn.aspx")
        End Sub

        Private Sub CloseAndRedirectTechBulletin()
            RedirectAndComplete("Sony-Technical-Bulletins-GenInfo.aspx")
        End Sub

        Private Sub CloseAndRedirectCourse()
            Dim Customer As Customer
            If HttpContextManager.Customer IsNot Nothing Then
                Customer = HttpContextManager.Customer
            Else
                Return
            End If

            If Customer.UserType.ToLower() = "c" Then
                RedirectAndComplete("training-payment-naccnt.aspx")
            Else
                RedirectAndComplete("training-payment-accnt.aspx")
            End If

        End Sub

        Private Sub CloseAndRedirectRAForm()
            RedirectAndComplete("sony-service-contacts.aspx")
        End Sub

        Private Sub RedirectAndComplete(url As String)
            Response.Redirect(url, False)
            HttpContext.Current.ApplicationInstance.CompleteRequest()
        End Sub
    End Class

End Namespace
