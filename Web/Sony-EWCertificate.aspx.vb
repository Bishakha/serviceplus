Imports Sony.US.ServicesPLUS.Process
Imports Sony.US.SIAMUtilities
Imports System.Data

Namespace ServicePLUSWebApp
    Partial Class Sony_EWCertificate
        Inherits SSL
        Dim objReportData As DataSet
        Public filename As String = "sony-Not-Authorized.aspx"

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Protected WithEvents ModelDescription As System.Web.UI.WebControls.Label


        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()

        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            If Not Session("customer") Is Nothing Then
                If Not Request.QueryString("onum") Is Nothing Then
                    If Request.QueryString("onum").Trim() <> String.Empty Then

                        checkGUID(Request.QueryString("onum"))
                        If Not objReportData Is Nothing Then
                            ConfigurationData.GeneralSettings.EWTempPDFFolder = System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath + "PDFs\EW"
                            filename = "PDFs/EW/" + System.Guid.NewGuid().ToString() + ".pdf"
                            Dim strReportFileAbsolutePath As String = System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath + "Report\EWCertificate.rpt"
                            If objReportData.Tables.Count > 0 Then
                                Dim objServicesPLUSReports As ServicesPLUSReports = New ServicesPLUSReports()
                                objServicesPLUSReports.ConvertCrReportToPDF(strReportFileAbsolutePath, Server.MapPath(filename), objReportData.Tables(0).DefaultView)
                            Else
                                filename = "sony-Not-Authorized.aspx"
                            End If
                        Else
                            filename = "sony-Not-Authorized.aspx"
                        End If
                    Else
                        filename = "sony-Not-Authorized.aspx"
                    End If
                Else
                    filename = "sony-Not-Authorized.aspx"
                End If
            Else
                If Not Request.QueryString("onum") Is Nothing Then
                    If Request.QueryString("onum").Trim() <> String.Empty Then
                        'Server.Transfer("SignIn.aspx?post=ewcert&&onum=" + Request.QueryString("onum"))
                        Server.Transfer("SignIn-Register.aspx?post=ewcert&&onum=" + Request.QueryString("onum"))
                    Else
                        filename = "sony-Not-Authorized.aspx"
                    End If
                Else
                    filename = "sony-Not-Authorized.aspx"
                End If
            End If
        End Sub

        Private Sub checkGUID(ByVal oNumGUID As String)
            Dim objExtendedWarranty As ExtendedWarrantyManager = New ExtendedWarrantyManager()
            objReportData = objExtendedWarranty.EWCertData(Request.QueryString("onum").Trim())
        End Sub

        'Private Sub LoadProductDetails(ByVal ProductCode As String, ByVal EWProductCode As String)
        '    Try
        '        ew = New ExtendedWarrantyManager
        '        ewmodels = ew.LoadEWProductDetails(ProductCode, EWProductCode)
        '        If ewmodels.Tables.Count > 0 Then
        '            If ewmodels.Tables(0).Rows.Count > 0 Then
        '                lblPrice.Text = "$" + ewmodels.Tables(0).Rows(0)("Price").ToString()
        '                lblShortDescription.Text = ewmodels.Tables(0).Rows(0)("ShortDescription").ToString()
        '                lblDesc.Text = "<P><table class=finderCopyDark width=100%><tr width=2%><td></td><td width=98%>" + ewmodels.Tables(0).Rows(0)("LongDescription").ToString() + "</td></tr></table></P>"
        '                lblHighlights.Text = ""
        '                lblHighlights.Visible = False
        '                lblHighlights.Text = "<ul>"
        '                For Each Row In ewmodels.Tables(0).Rows
        '                    lblHighlights.Text = lblHighlights.Text + "<li>" + Row("Highlights") + "</li>"
        '                Next Row
        '                lblHighlights.Text = lblHighlights.Text + "</ul>"

        '            End If
        '        End If
        '    Catch ex As Exception
        '        ErrorLabel.Text = ("Load error:" + ex.Message)
        '    End Try
        'End Sub

        'Protected Sub mnTab_MenuItemClick(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.MenuEventArgs) Handles mnTab.MenuItemClick
        '    If e.Item.Value = 0 Then
        '        mnTab.Items(0).ImageUrl = "images/Description.gif"
        '        lblDesc.Visible = True
        '        mnTab.Items(1).ImageUrl = "images/Highlights.gif"
        '        lblHighlights.Visible = False
        '        ErrorLabel.Text = ""
        '    Else
        '        mnTab.Items(1).ImageUrl = "images/Highlightslight.gif"
        '        lblHighlights.Visible = True
        '        mnTab.Items(0).ImageUrl = "images/DescriptionDark.gif"
        '        lblDesc.Visible = False
        '        ErrorLabel.Text = ""
        '    End If
        'End Sub

        'Protected Sub btnOrderEW_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnOrderEW.Click
        '    Server.Transfer("Sony-EW-PurchaseInfo.aspx?" + ViewState.Item("querystring").ToString())
        'End Sub

        'Dim exportOpts As ExportOptions = New ExportOptions()
        'Dim pdfFormatOpts As PdfRtfWordFormatOptions = New PdfRtfWordFormatOptions()
        'Dim diskOpts As DiskFileDestinationOptions = New DiskFileDestinationOptions()
        'Dim DocReport As ReportDocument = New ReportDocument()
        '        DocReport.FileName = Server.MapPath("Report\EWCertificate.rpt")
        '        exportOpts = DocReport.ExportOptions
        ''Dim mailOpts As MicrosoftMailDestinationOptions = New MicrosoftMailDestinationOptions(ExportOptions.CreateMicrosoftMailDestinationOptions())

        ''// Set the PDF format options.
        '        exportOpts.ExportFormatType = ExportFormatType.PortableDocFormat
        '        exportOpts.FormatOptions = pdfFormatOpts
        ''// Set the options and export.
        '        exportOpts.ExportDestinationType = ExportDestinationType.NoDestination

        ''exportOpts.ExportDestinationType = ExportDestinationType.MicrosoftMail
        ''mailOpts.MailToList = "chandra.maurya@am.sony.com"
        ''mailOpts.MailMessage = "Get The Message"
        ''mailOpts.MailSubject = "Certificate"

        ''exportOpts.ExportDestinationOptions = mailOpts

        ''DocReport.Export(exportOpts)
        '        DocReport.ExportToHttpResponse(exportOpts, Response, True, "EW-Certificate")
        '        Console.WriteLine("sss")
    End Class

End Namespace

