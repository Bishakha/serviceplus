﻿Imports System.Xml
Imports System.IO
Imports System.Xml.Xsl

Partial Class sony_services_contacts_popup
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim strSesion As String
        If Not Session.Item("customer") Is Nothing Then
            strSesion = 1
        Else
            strSesion = 0
        End If
        Dim strBuildNum As String = Application("Build").ToString()
        Dim xmlDoc As XmlDocument = New XmlDocument()
        xmlDoc.LoadXml("<NewDataSet><CONTACT><MAINTENANCE>0</MAINTENANCE><SESSION>" + strSesion + "</SESSION><BUILDNUM>" + strBuildNum + "</BUILDNUM></CONTACT></NewDataSet>")
        Dim xslPath As String = Server.MapPath("Contact.xslt")
        Dim sb As StringBuilder = New StringBuilder
        Dim sw As StringWriter = New StringWriter(sb)
        Dim transform As XslTransform = New XslTransform
        transform.Load(xslPath)
        transform.Transform(xmlDoc, Nothing, sw, Nothing)
        tableContact.InnerHtml = sb.ToString()
        sw.Flush()
        sb.Length = 0
    End Sub
End Class
