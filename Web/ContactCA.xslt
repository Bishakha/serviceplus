<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <Table border="0">
      <xsl:apply-templates select="//CONTACT" />
    </Table>
  </xsl:template>
  <xsl:template match="CONTACT">
    <tr>
      <td>
        <img height="1" src="images/spacer.gif" width="7" alt=""/>
      </td>
      <td>
        <table width="650" border="0">
          <tr>
            <td>
              <img height="10" src="images/spacer.gif" width="7" alt=""/>
            </td>
          </tr>
          <xsl:if test="MAINTENANCE = 1">
            <tr runat="server" id = "maintenanceTR" visible="false">
              <td >
                <span class="bodyCopy">
                  <STRONG>
                    <img height="1" src="images/spacer.gif" width="7" alt=""/>le ServicesPLUS<sup>sm</sup> site que vous essayez d'atteindre est actuellement en cours de maintenance.
                  </STRONG>
                </span>
              </td>
            </tr>
            <tr>
              <td>
                <img height="1" src="images/spacer.gif" width="7" alt="" />
                <span class="bodyCopy">Voici quelques ressources de rechange pour vous aider pendant cette période; nous vous répondrons le plus rapidement possible.</span>
              </td>
            </tr>


          </xsl:if>
          <tr>
            <td>
              <img height="5" src="images/spacer.gif" width="7" alt=""/>
            </td>
          </tr>

          <!--<tr>
            <td>
              <span class="bodyCopy">
                <STRONG>
                  <img height="1" src="images/spacer.gif" width="7" alt="" />Vous devez fournir une adresse aux Etats-Unis pour vous inscrire sur ServicesPLUS. <br/>
                                     Les commandes passées sur ServicesPLUS ne peuvent expédier qu’à des adresses aux États-Unis
                </STRONG>
              </span>
            </td>
          </tr>-->

          <tr>
            <td>
              <span class="bodyCopy">
                <STRONG>
                  <img height="10" src="images/spacer.gif" width="7" alt="" />
                </STRONG>
              </span>
            </td>
          </tr>
          
          <tr>
            <td>
              <span class="bodyCopy">
                <STRONG>
                  <img height="1" src="images/spacer.gif" width="7" alt=""/>Les coordonnées des services et du soutien pour la diffusion et les entreprises
                </STRONG>
              </span>
            </td>
          </tr>
        
          <tr>
            <td>
              <img height="10" src="images/spacer.gif" width="7" alt=""/>
            </td>
          </tr>
          <tr>
            <td>
              <span class="bodyCopy">
                <STRONG>
                  <img height="1" src="images/spacer.gif" width="7" alt=""/>Pièces de rechange:
                </STRONG>
              </span>
            </td>
          </tr>
          <tr>
            <td >
              <table width="650" border="0">
                <tr>
                  <td style="HEIGHT: 10px">
                    <img height="1" src="images/spacer.gif" width="7" alt=""/>
                    <span class="bodyCopy">
                      Service à la clientèle: la commande, suivi des commandes, les retours
                    </span>
                  </td>
                </tr>

                <tr>
                  <td>
                    <img height="1" src="images/spacer.gif" width="40" alt="" />
                    <span class="bodyCopy">
                      email: <A href="mailto:ProParts@am.sony.com?subject=Parts Customer Service:  ordering, tracking, research, and returns&amp;Body=Date:%0d%0d%0dName:%0dCompany Name:%0dSony Account Number:%0dShipping Address:%0dBilling Address:%0dPhone number:%0dIs this is a parts inquiry or Order?%0dPO Number:%0d%0dItem                                 Part number                    Quantity%0d1.%0d2.%0d3.%0d4.%0d5.%0d%0dDo you want  shipping method Next day,2 day, or ground transportation?%0d%0dOther Parts requests: ">ProParts@am.sony.com</A>
                    </span>
                  </td>
                </tr>
                <tr>
                  <td>
                    <img height="1" src="images/spacer.gif" width="7" alt="" />
                    <span class="bodyCopy">Pièces de recherche:</span>
                  </td>
                </tr>
                <tr>
                  <td style="padding-bottom: 10px">
                    <img height="1" src="images/spacer.gif" width="40" alt="" />
                    <span class="bodyCopy">
                      email: <a href="mailto:ProPartsResearch@am.sony.com">ProPartsResearch@am.sony.com</a>
                    </span>
                  </td>
                </tr>

                <tr>
                  <td>
                    <img height="1" src="images/spacer.gif" width="7" alt=""/>
                    <span class="bodyCopy">
                      Téléphone: 800-538-7550, 9h00-19h00 HNE, du lundi - vendredi.
                    </span>
                  </td>
                </tr>
                <!--<tr runat="server" id = "linkTR">
                  <td style="height: 14px">
                    <xsl:if test="MAINTENANCE = 0">
                      <img height="1" src="images/spacer.gif" width="7" alt=""/>
                      <xsl:if test="SESSION = 1">
                         <span class="bodyCopy">Link: <b>Please click on the <a href="#" onclick="window.open('RARequest.aspx','','toolbar=0,location=0,top=0,left=0,directories=0,status=0,menubar=0,scrollbars=yes,resize=no,width=800,height=650'); return false;" class="body">Return Authorization Form</a>.</b></span>
                      </xsl:if>
                      <xsl:if test="SESSION = 0">
                          <span class="bodyCopy">Link: <b>Please click on the Return Authorization Form. (Requires login, to login please <a href="#" onclick="window.location.href='SignIn.aspx?RAForm=True'; return false;">click here</a>)
                            </b>
                          </span>
                      </xsl:if>
                    </xsl:if>
                  </td>
                </tr>-->
              </table>
            </td>
          </tr>
          <tr>
            <td>
              <img height="10" src="images/spacer.gif" width="7" alt=""/>
            </td>
          </tr>
          <tr>
            <td >
              <span class="bodyCopy">
                <STRONG>
                  <img height="1" src="images/spacer.gif" width="7" alt=""/>Services de réparation:
                </STRONG>
              </span>
            </td>
          </tr>
          <tr>
            <td >
              <img height="1" src="images/spacer.gif" width="40" alt="" />
              <span class="bodyCopy">
                Email: <a href="mailto:SEL.BPC.Svc@am.sony.com">SEL.BPC.Svc@am.sony.com</a>
              </span>
            </td>
          </tr>

          <tr>
            <td>
              <img height="1" src="images/spacer.gif" width="43" alt=""/>
              <span class="bodyCopy">téléphone: 866-766-9272.</span>
            </td>
          </tr>

          <tr>
            <td>
              <img height="10" src="images/spacer.gif" width="7" alt=""/>
            </td>
          </tr>
          <tr>
            <td >
              <span class="bodyCopy">
                <STRONG>
                  <img height="1" src="images/spacer.gif" width="7" alt=""/>logiciel:
                </STRONG>
              </span>
            </td>
          </tr>
          <tr>
            <td >
              <img height="1" src="images/spacer.gif" width="40" alt="" />
              <span class="bodyCopy">
                email: <A href="mailto:ProParts@am.sony.com?subject=Parts Customer Service:  ordering, tracking, research, and returns&amp;Body=Date:%0d%0d%0dName:%0dCompany Name:%0dSony Account Number:%0dShipping Address:%0dBilling Address:%0dPhone number:%0dIs this is a parts inquiry or Order?%0dPO Number:%0d%0dItem                                 Part number                    Quantity%0d1.%0d2.%0d3.%0d4.%0d5.%0d%0dDo you want  shipping method Next day,2 day, or ground transportation?%0d%0dOther Parts requests: ">ProParts@am.sony.com</A>
              </span>
            </td>
          </tr>

          <tr>
            <td>
              <img height="1" src="images/spacer.gif" width="43" alt=""/>
              <span class="bodyCopy">Téléphone: 800-538-7550, 9h00-19h00 HNE, du lundi - vendredi.</span>
            </td>
          </tr>
          <tr>
            <td>
              <img height="10" src="images/spacer.gif" width="7" alt=""/>
            </td>
          </tr>
          <tr>
            <td >
              <span class="bodyCopy">
                <STRONG>
                  <img height="1" src="images/spacer.gif" width="7" alt=""/>Abonnements techniques:
                </STRONG>
              </span>
            </td>
          </tr>
          <tr>
            <td >
              <img height="1" src="images/spacer.gif" width="40" alt="" />
              <span class="bodyCopy">
                Email: <a href="mailto:BSSC.TIS@am.sony.com">BSSC.TIS@am.sony.com</a>
              </span>
            </td>
          </tr>

          <tr>
            <td>
              <img height="1" src="images/spacer.gif" width="43" alt=""/>
              <span class="bodyCopy">Téléphone: 408-352-4500</span>
            </td>
          </tr>
          <tr>
            <td>
              <img height="10" src="images/spacer.gif" width="7" alt=""/>
            </td>
          </tr>
          <tr>
            <td >
              <span class="bodyCopy">
                <STRONG>
                  <img height="1" src="images/spacer.gif" width="7" alt=""/>Institut de formation Sony:
                </STRONG>
              </span>
            </td>
          </tr>
          <tr>
            <td >
              <img height="1" src="images/spacer.gif" width="7" alt="" />
              <span class="bodyCopy">
                De recherche et de vous inscrire à des cours, du matériel de formation d'achat
              </span>
            </td>
          </tr>

          <tr>
            <td >
              <img height="1" src="images/spacer.gif" width="40" alt="" />
              <span class="bodyCopy">
                Email: <a href="mailto:Training@am.sony.com">Training@am.sony.com</a>
              </span>
            </td>
          </tr>

          <tr>
            <td>
              <img height="1" src="images/spacer.gif" width="43" alt=""/>
              <span class="bodyCopy">Téléphone:408-352-4500.</span>
            </td>
          </tr>

          <tr>
            <td>
              <img height="1" src="images/spacer.gif" width="43" alt=""/>
              <span class="bodyCopy">fax : 408-352-4212.</span>
            </td>
          </tr>
          <tr>
            <td>
              <img height="10" src="images/spacer.gif" width="7" alt=""/>
            </td>
          </tr>
          <tr>
            <td >
              <span class="bodyCopy">
                <STRONG>
                  <img height="1" src="images/spacer.gif" width="7" alt=""/>SupportNet<sup>sm</sup> Contrats de service et SystemWatch<sup>sm</sup>
                </STRONG>
              </span>
            </td>
          </tr>
          <tr>
            <td>
              <table width="650" border="0">
                <tr>
                  <td>
                    <img height="1" src="images/spacer.gif" width="40" alt=""/>
                    <span class="bodyCopy">
                      Email:<A href="mailto:SupportNET@am.sony.com">SupportNET@am.sony.com</A>
                    </span>
                  </td>
                </tr>
                <tr>
                  <td >
                    <img height="1" src="images/spacer.gif" width="37" alt=""/>
                    <span class="bodyCopy">
                      Phone: 877-398-7669
                    </span>
                  </td>
                </tr>
              </table>

            </td>
          </tr>
          <tr>
            <td>
              <img height="10" src="images/spacer.gif" width="7" alt=""/>
            </td>
          </tr>
          <tr>
            <td>
              <img height="10" src="images/spacer.gif" width="7" alt=""/>
            </td>
          </tr>
          <tr>
            <td >
              <span class="bodyCopy">
                <STRONG>
                  <img height="1" src="images/spacer.gif" width="7" alt=""/>Inscriptions  pour les produits professionnels
                </STRONG>
              </span>
            </td>
          </tr>
          <tr>
            <td >
              <img height="1" src="images/spacer.gif" width="40" alt="" />
              <span class="bodyCopy">
                Pas disponible pour le moment, veuillez conserver votre preuve d’achat (facture) si une demande de réparation est nécessaire.
              </span>
            </td>
          </tr>

          <tr>
            <td>
              <img height="10" src="images/spacer.gif" width="7" alt=""/>
            </td>
          </tr>
          <tr>
            <td >
              <span class="bodyCopy">
                <STRONG>
                  <img height="1" src="images/spacer.gif" width="7" alt=""/>Support technique pour les produits professionnels:
                </STRONG>
              </span>
            </td>
          </tr>
          <tr>
            <td >
              <img height="1" src="images/spacer.gif" width="7" alt="" />
              <span class="bodyCopy">
                Assistance opérationnelle de produit
              </span>
            </td>
          </tr>
          <tr>
            <td >
              <img height="1" src="images/spacer.gif" width="40" alt="" />
              <span class="bodyCopy">
                Téléphone: 800-883-6817, 08h30-20h00 HE, du lundi - vendredi
              </span>
            </td>
          </tr>
          <tr>
            <td >
              <img height="1" src="images/spacer.gif" width="7" alt="" />
              <span class="bodyCopy">
                Aide au diagnostic technique de produit
              </span>
            </td>
          </tr>
          <tr>
            <td >
              <img height="1" src="images/spacer.gif" width="40" alt="" />
              <span class="bodyCopy">
                Si vous avez un accord de SupportNet actuelle
              </span>
            </td>
          </tr>
          <tr>
            <td >
              <img height="1" src="images/spacer.gif" width="40" alt="" />
              <span class="bodyCopy">
                Téléphone: 877-398-7669, 24x7x365
              </span>
            </td>
          </tr>
          <tr>
            <td >
              <img height="1" src="images/spacer.gif" width="40" alt="" />
              <span class="bodyCopy">
                Si vous ne disposez pas d'un accord de SupportNet actuelle
              </span>
            </td>
          </tr>
          <tr>
            <td >
              <img height="1" src="images/spacer.gif" width="40" alt="" />
              <span class="bodyCopy">
                Téléphone: 866-766-9272, 24x7x365
              </span>
            </td>
          </tr>
          <tr>
            <td >
              <img height="1" src="images/spacer.gif" width="40" alt="" />
              <span class="bodyCopy">
                support Technique
              </span>
            </td>
          </tr>
          <tr>
            <td >
              <img height="1" src="images/spacer.gif" width="40" alt="" />
              <span class="bodyCopy">
                Email: <a href="mailto:bis.product.support@am.sony.com">bis.product.support@am.sony.com</a>
              </span>
            </td>
          </tr>
          <tr>
            <td>
              <img height="10" src="images/spacer.gif" width="7" alt=""/>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </xsl:template>

</xsl:stylesheet>

