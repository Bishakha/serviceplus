Imports System
Imports System.IO
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Drawing
Imports System.Web
Imports System.Web.SessionState
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Process
Imports Sony.US.ServicesPLUS.Data
Imports Sony.US.siam
Imports Sony.US.SIAMUtilities
Imports System.Text
Imports System.Text.RegularExpressions
Imports ServicesPlusException
Imports Sony.US.AuditLog


Namespace ServicePLUSWebApp

    Partial Class NotificationPreferences
		Inherits SSL

#Region "Page members"
		Const conStyleSheetForInterestControls As String = "bodyCopy"
		Const conBgColorForMarketingInterest As String = "#ffffff"
		Const conBgColorForMarketingInterestAlt As String = "#f2f5f8"
		Const conControlsPerRow As Int32 = 3
		Const conNumberOfCCsToSave As Int16 = 4
		Const conExpiresInDays As Int16 = 45
		Const conDefaultFormFieldFocus As String = "mi_SelectAll"
		Const conSelectAllMarketingInterestName As String = "mi_SelectAll"
		Const conSelectAllMarketingInterestPrefix As String = "mi_"

		Protected WithEvents marketingInterestCheckBox As System.Web.UI.WebControls.CheckBox
		Protected WithEvents selectAllInterest As System.Web.UI.WebControls.CheckBox
		Private selectAllChecked As Boolean = False
		Private selectAllUnChecked As Boolean = False
		Private selectAllCheckBoxClicked As Boolean = False
		Private updatePassword As Boolean = False
		Private updateCreditCard As Boolean = False
		'Added for PCI Compliance E736
		Public CCNo As String = ""
		Public focusFormField As String
        'Dim objUtilties As Utilities = New Utilities
		'Dim objGlobalData As New GlobalData
#End Region

#Region " Web Form Designer Generated Code "
		'This call is required by the Web Form Designer.
		<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

		End Sub

		Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
			'CODEGEN: This method call is required by the Web Form Designer
			'Do not modify it using the code editor.
			isProtectedPage(True)
            DeclareDynamicControls()
            InitializeComponent()
            Page.ID = Request.Url.AbsolutePath
        End Sub

#End Region

#Region "Events"
        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                'Added by kayal - WP010
                'If Not Session.Item("GlobalData") Is Nothing Then
                '    objGlobalData = Session.Item("GlobalData")
                'End If

                If Not IsPostBack Then
                    ProcessPage(sender, e)
                    SetFocusField(conDefaultFormFieldFocus)
                End If

                MasterErrorLabel.Text = ""
                If Session("Language_Changed") IsNot Nothing And Session("Language_Changed") = "1" Then
                    fnSetSession()
                End If
            Catch ex As Exception
                MasterErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                MasterErrorLabel.Focus()
            End Try
        End Sub

        Private Sub UpdateProfile_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles UpdateProfile.Click
            ProcessPage(sender, e)
        End Sub

#End Region

        Private Sub ProcessPage(ByVal sender As System.Object, ByRef e As System.EventArgs)
            Dim objPCILogger As New PCILogger() '6524
            Try
                '6524 start
                objPCILogger.EventOriginApplication = "ServicesPLUS"
                objPCILogger.EventOriginApplicationLocation = Me.Page.GetType().Name
                objPCILogger.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
                objPCILogger.OperationalUser = Environment.UserName
                objPCILogger.EventDateTime = DateTime.Now.ToLongTimeString()
                objPCILogger.HTTPRequestObjectValues = objPCILogger.GetRequestContextValue()
                objPCILogger.IndicationSuccessFailure = "Success"
                objPCILogger.EventOriginMethod = "processPage"
                objPCILogger.Message = "processPage Success."
                objPCILogger.EventType = EventType.Update
                '6524 end

                If HttpContextManager.Customer IsNot Nothing Then
                    Dim customer As Customer = HttpContextManager.Customer
                    Dim callingControl As String = String.Empty
                    '6524 start
                    objPCILogger.CustomerID = customer.CustomerID
                    objPCILogger.CustomerID_SequenceNumber = customer.SequenceNumber
                    'objPCILogger.UserName = customer.UserName
                    objPCILogger.EmailAddress = customer.EmailAddress '6524
                    objPCILogger.SIAM_ID = customer.SIAMIdentity
                    objPCILogger.LDAP_ID = customer.LdapID '6524
                    '6524 end
                    If sender.ID IsNot Nothing Then callingControl = sender.ID.ToString()
                    Select Case callingControl
                        Case UpdateProfile.ID.ToString()
                            UpdateThisCustomerProfile(customer)
                        Case "SelectAll"
                            CheckAllCheckBoxes()
                    End Select
                    PopulateInterestSection(customer)
                End If
            Catch ex As Exception
                objPCILogger.IndicationSuccessFailure = "Failure" '6524
                objPCILogger.Message = "processPage Failed. " & ex.Message.ToString() '6524
                MasterErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                MasterErrorLabel.Focus()
                handleError(ex)
            Finally
                objPCILogger.PushLogToMSMQ() '6524
            End Try
        End Sub

        Private Sub UpdateThisCustomerProfile(ByRef customer As Customer)
            Dim objPCILogger As New PCILogger() '6524
            Dim customerManager As New CustomerManager
            Dim blnChanege As Boolean

            Try
                objPCILogger.EventOriginApplication = "ServicesPLUS"
                objPCILogger.EventOriginApplicationLocation = Me.Page.GetType().Name
                objPCILogger.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
                objPCILogger.OperationalUser = Environment.UserName
                objPCILogger.EventDateTime = DateTime.Now.ToLongTimeString()
                objPCILogger.HTTPRequestObjectValues = objPCILogger.GetRequestContextValue()
                objPCILogger.IndicationSuccessFailure = "Success"
                objPCILogger.EventOriginMethod = "updateThisCustomerProfile"
                objPCILogger.Message = "updateThisCustomerProfile Success."
                objPCILogger.EventType = EventType.Update

                If customer IsNot Nothing Then
                    objPCILogger.CustomerID = customer.CustomerID
                    objPCILogger.CustomerID_SequenceNumber = customer.SequenceNumber
                    objPCILogger.EmailAddress = customer.EmailAddress '6524
                    objPCILogger.SIAM_ID = customer.SIAMIdentity
                    objPCILogger.LDAP_ID = customer.LdapID '6524

                    focusFormField = ""
                    MasterErrorLabel.Text = ""
                    customer.ReturnDeletedObjects = True
                    blnChanege = UpdateTheCustomerObject(customer)

                    If blnChanege Then
                        customerManager.UpdateCustomerMarketingInterest(customer)
                        customer.AcceptMarketingInterestChange()
                        customer.ReturnDeletedObjects = False
                        MasterErrorLabel.Text = "Your profile has been sucessfully updated."
                        MasterErrorLabel.Focus()
                    End If
                    If optin.Checked <> customer.EmailOk Then
                        'customer.EmailOk = optin.Checked
                        If optin.Checked = True Then
                            customer.EmailOk = True
                        Else
                            customer.EmailOk = False
                        End If
                        'customerManager.UpdateCustomer(customer)
                        customerManager.UpdateCustomerSubscription(customer)

                        MasterErrorLabel.Text = "Your profile has been sucessfully updated."
                        MasterErrorLabel.Focus()
                    ElseIf Not blnChanege And (optin.Checked = customer.EmailOk) Then
                        MasterErrorLabel.Text = "No change to update"
                        MasterErrorLabel.Focus()
                    End If
                End If
                HttpContextManager.Customer = customer
            Catch ex As Exception
                objPCILogger.IndicationSuccessFailure = "Failure" '6524
                objPCILogger.Message = "updateThisCustomerProfile Failed. " & ex.Message.ToString() '6524
                MasterErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            Finally
                objPCILogger.PushLogToMSMQ() '6524
            End Try
        End Sub

        Private Function UpdateTheCustomerObject(ByRef customer As Customer) As Boolean
            Dim returnValue As Boolean = False
            If Not customer Is Nothing Then
                returnValue = UpdateMarketingInterest(customer)

            End If
            Return returnValue
        End Function

#Region "marketing interest section"
        Private Sub PopulateInterestSection(ByRef thisCustomer As Customer)
            Dim customerManager As New CustomerManager
            Dim allMarketingInterest() As MarketingInterest

            Try
                If thisCustomer IsNot Nothing Then
                    allMarketingInterest = customerManager.GetAllMarketingInterests(HttpContextManager.GlobalData.Language)
                    'thisCustomer.ClearAllMarketingInterest()
                    BuildMarketingInterestTable(allMarketingInterest, thisCustomer)
                    optin.Checked = thisCustomer.EmailOk
                End If
            Catch ex As Exception
                MasterErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                MasterErrorLabel.Focus()
            End Try
        End Sub

        Private Sub BuildMarketingInterestTable(ByVal allMarketingInterest() As MarketingInterest, ByRef thisCustomer As Customer)
            Try
                Dim controlCounter As Int32 = 0
                Dim rowBackGroundColor As String = conBgColorForMarketingInterest

                allMarketingInterest = AddSelectAllCheckBoxToArray(allMarketingInterest)

                marketingInterestTable.Controls.Add(New LiteralControl("<table cellpadding=""0"" cellspacing=""0"" border=""0"" role=""presentation"">"))

                For Each thisMarketingInterest As MarketingInterest In allMarketingInterest
                    'thisCustomer.AddMarketingInterest(thisMarketingInterest)
                    If controlCounter = conControlsPerRow Then '- end of the row
                        marketingInterestTable.Controls.Add(New LiteralControl("</tr>" + vbCrLf))
                        controlCounter = 0
                    End If

                    If controlCounter = 0 Then '- start of the row
                        '-- change the rows background color
                        rowBackGroundColor = IIf(rowBackGroundColor = conBgColorForMarketingInterestAlt.ToString(), conBgColorForMarketingInterest, conBgColorForMarketingInterestAlt.ToString())
                        marketingInterestTable.Controls.Add(New LiteralControl("<tr bgcolor=""" + rowBackGroundColor + """>" + vbCrLf))
                    End If

                    marketingInterestCheckBox = New CheckBox With {
                        .Text = "&nbsp;" + thisMarketingInterest.Description.ToString(),
                        .ID = conSelectAllMarketingInterestPrefix.ToString() + thisMarketingInterest.MarketingInterestID.ToString(),
                        .CssClass = conStyleSheetForInterestControls,
                        .Checked = IsInterestedChecked(thisMarketingInterest.MarketingInterestID.ToString(), thisCustomer)
                    }

                    marketingInterestTable.Controls.Add(New LiteralControl(vbTab + "<td width=""220"" align=""left"">" + vbCrLf))

                    If marketingInterestCheckBox.ID.ToString() = conSelectAllMarketingInterestName Then
                        marketingInterestCheckBox.Attributes.Add("onclick", "selectMarketingInterest(" + allMarketingInterest.Length().ToString() + ", '" + conSelectAllMarketingInterestName + "', '" + conSelectAllMarketingInterestPrefix.ToString() + "')")
                        If Not Request.Form(conSelectAllMarketingInterestName) Is Nothing Then marketingInterestCheckBox.Checked = True
                    End If

                    'Me.Page.FindControl("Form1").Controls.Add(marketingInterestCheckBox)
                    marketingInterestTable.Controls.Add(marketingInterestCheckBox)
                    marketingInterestTable.Controls.Add(New LiteralControl("</td>" + vbCrLf))
                    controlCounter = (controlCounter + 1)
                Next
            Catch ex As Exception
                MasterErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                MasterErrorLabel.Focus()
            End Try
            marketingInterestTable.Controls.Add(New LiteralControl("</table>"))
        End Sub

        Private Function IsInterestedChecked(ByVal marketingInterestID As String, ByRef thisCustomer As Customer) As Boolean
            Dim returnValue As Boolean = False
            Try
                If Not thisCustomer Is Nothing And Not marketingInterestID Is Nothing Then
                    If Request.Form(marketingInterestID) Is Nothing Then
                        For Each thisMarketingInterest As MarketingInterest In thisCustomer.MarketingInterests
                            If thisMarketingInterest.MarketingInterestID = marketingInterestID And Not thisMarketingInterest.Dirty Then
                                returnValue = True
                                Exit For
                            End If
                        Next
                    Else
                        returnValue = True
                    End If
                End If
            Catch ex As Exception
                returnValue = False
            End Try

            Return returnValue
        End Function

        Private Function AddSelectAllCheckBoxToArray(ByVal marketingInterest() As MarketingInterest) As MarketingInterest()
            Dim returnMarketingInterest(marketingInterest.Length) As MarketingInterest
            Try
                If marketingInterest IsNot Nothing Then
                    Dim newMarketingInterest As New ArrayList
                    Dim addedInterest As New MarketingInterest With {
                        .MarketingInterestID = conSelectAllMarketingInterestName.Replace(conSelectAllMarketingInterestPrefix, ""),
                        .Description = Resources.Resource.el_SelectAll ' conSelectAllMarketingInterestName.Replace(conSelectAllMarketingInterestPrefix, "").ToString()
                    }
                    newMarketingInterest.Add(addedInterest)

                    For Each thisInterest As MarketingInterest In marketingInterest
                        newMarketingInterest.Add(thisInterest)
                    Next
                    newMarketingInterest.CopyTo(returnMarketingInterest)
                End If
            Catch ex As Exception
                returnMarketingInterest = Nothing
            End Try
            Return returnMarketingInterest
        End Function
#End Region

        Private Function UpdateMarketingInterest(ByRef thisCustomer As Customer) As Boolean
            If thisCustomer Is Nothing Then Return False

            Dim thisCustomerManager As New CustomerManager
            Dim allMarketingInterest() As MarketingInterest
            Dim thisCustomerMarketingInterest() As MarketingInterest
            Dim isChecked As Boolean = False
            Dim foundInthisCustomer As Boolean = False
            Dim returnValue As Boolean = False
            Try
                allMarketingInterest = thisCustomerManager.GetAllMarketingInterests(HttpContextManager.GlobalData.Language)
                thisCustomerMarketingInterest = thisCustomer.MarketingInterests

                For Each thisMarketingInterest As MarketingInterest In allMarketingInterest
                    ' Reset the flags for each loop
                    foundInthisCustomer = False
                    isChecked = False

                    If Request.Form("mi_" + thisMarketingInterest.MarketingInterestID) IsNot Nothing Then
                        isChecked = (Request.Form("mi_" + thisMarketingInterest.MarketingInterestID) = "on")
                    End If

                    For Each thisCustomerMI As MarketingInterest In thisCustomer.MarketingInterests
                        ' Check the Customer's existing interests, removing any that are now unchecked.
                        If thisCustomerMI.MarketingInterestID = thisMarketingInterest.MarketingInterestID Then
                            If Not isChecked Then
                                thisCustomer.RemoveMarketingInterest(thisCustomerMI)
                                returnValue = True
                            End If
                            foundInthisCustomer = True
                            Exit For
                        End If
                    Next

                    If isChecked Then
                        If Not foundInthisCustomer Then
                            ' If a box is checked that wasn't before, add it.
                            thisCustomer.AddMarketingInterest(thisMarketingInterest)
                            returnValue = True
                        End If
                    End If
                Next
            Catch ex As Exception
                MasterErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                MasterErrorLabel.Focus()
            End Try

            Return returnValue
        End Function

#Region "helpers"

        Private Sub CheckAllCheckBoxes()
            Try
                selectAllCheckBoxClicked = True
                If Request.Form("SelectAll") IsNot Nothing Then
                    If Request.Form("SelectAll") = "on" Then
                        selectAllInterest.Checked = True
                        selectAllChecked = True
                        selectAllUnChecked = False
                    End If
                Else
                    selectAllInterest.Checked = False
                    selectAllChecked = False
                    selectAllUnChecked = True
                End If

            Catch ex As Exception

            End Try
        End Sub

        Private Sub DeclareDynamicControls()
            Try
                selectAllInterest = New CheckBox
                selectAllInterest.Visible = False
                If Request.Form("SelectAll") IsNot Nothing Then selectAllInterest.Checked = True
                'Me.Page.FindControl("Form1").Controls.Add(selectAllInterest)
                marketingInterestTable.Controls.Add(selectAllInterest)
            Catch ex As Exception
                MasterErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                MasterErrorLabel.Focus()
            End Try

        End Sub

        Private Sub SetFocusField(ByVal callingControl As String)
            '-- if the form validation has already set the focus field then don't do anything. 
            If focusFormField Is Nothing Then

                Select Case callingControl
                    Case Page.ID.ToString()
                        focusFormField = conDefaultFormFieldFocus

                    Case UpdateProfile.ID.ToString()
                        focusFormField = conDefaultFormFieldFocus

                    Case "SelectAll"
                        focusFormField = "SelectAll"

                    Case Else
                        focusFormField = conDefaultFormFieldFocus

                End Select

            End If
        End Sub

#End Region

#Region " get and Set Session values to control for language change"
        Public Sub fnGetSession()
            Dim ObjCst As New Customer
            Dim objNotify As New MarketingInterest()
            Dim thisCustomerMarketingInterest() As MarketingInterest = ObjCst.MarketingInterests
            Session.Add("CultureData", ObjCst)
        End Sub

        Public Sub fnSetSession()
            If Session("CultureData") IsNot Nothing Then
                Dim ObjCst As New Customer
                ObjCst = Session.Item("CultureData")

                PopulateInterestSection(ObjCst)
                Session.Remove("CultureData")
                Session("Language_Changed") = "0"
            End If
        End Sub

#End Region

    End Class

End Namespace
