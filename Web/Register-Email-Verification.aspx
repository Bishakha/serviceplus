<%@ Reference Page="~/SSL.aspx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="TopSonyHeader" Src="TopSonyHeader.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="SonyFooter" Src="SonyFooter.ascx" %>

<%@ Page Language="VB" AutoEventWireup="false" SmartNavigation="true" Inherits="ServicePLUSWebApp.Register_Email_Verification" EnableViewStateMac="true" CodeFile="Register-Email-Verification.aspx.vb" CodeFileBaseClass="ServicePLUSWebApp.SSL" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" lang="<%=PageLanguage %>">
<head>
    <title>ServicesPLUS - New User Registration</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link href="includes/ServicesPLUS_style.css" type="text/css" rel="stylesheet">
    <script language="javascript" src="includes/ServicesPLUS.js" type="text/javascript"></script>
    <style type="text/css">
        .style1 {
        }

        .style2 {
            height: 31px;
        }
    </style>
    <script type="text/javascript">
        function showStuff(id) {
            document.getElementById(id).style.display = 'block';
        }
        function hideStuff(id) {
            document.getElementById(id).style.display = 'none';
        }
    </script>
</head>
<body style="color: #000000; background: #5d7180; margin: 0px;">
    <form id="form1" autocomplete="off" method="post" runat="server">
        <center>
            <table width="760" border="0" role="presentation">
                <tr>
                    <td style="width: 25px; background: url(images/sp_left_bkgd.gif);">
                        <img src="images/spacer.gif" width="25" height="25" alt="" />
                    </td>
                    <td width="710" bgcolor="#ffffff">
                        <table width="710" border="0" role="presentation">
                            <tr>
                                <td>
                                    <ServicePLUSWebApp:TopSonyHeader ID="TopSonyHeader" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <img src="images/sp_int_register_hdr_splusreg.GIF" border="0" alt="ServicesPlus Registration">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="710" border="0" role="presentation">
                                        <tr style="height: 20px;">
                                            <td width="20">
                                                <img height="20" src="images/spacer.gif" width="20" alt="">
                                            </td>
                                            <td>
                                                <img height="20" src="images/spacer.gif" width="670" alt="">
                                            </td>
                                            <td width="20">
                                                <img height="20" src="images/spacer.gif" width="20" alt="">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="17">
                                                <img height="20" src="images/spacer.gif" width="20" alt=""></td>
                                            <td width="678">
                                                <main>
                                                    <table width="670" border="0" role="presentation">
                                                    <tr bgcolor="#ffffff">
                                                        <td width="3">
                                                            <img height="1" src="images/spacer.gif" width="3" alt="">
                                                        </td>
                                                        <td width="127">
                                                            <img height="1" src="images/spacer.gif" width="125" alt="">
                                                        </td>
                                                        <td width="542">
                                                            <img height="1" src="images/spacer.gif" width="542" alt="">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="top" colspan="3" height="15">
                                                            <a class="bodyCopy" href="default.aspx">Home Page</a>
                                                        </td>
                                                    </tr>
                                                    <tr bgcolor="#d5dee9">
                                                        <td width="3" class="style2">
                                                            <img height="15" src="images/spacer.gif" width="3" alt="">
                                                        </td>
                                                        <td colspan="2" class="style2">
                                                            <span class="tableHeader">Please enter the verification code sent to your email, If you
                                                                        are facing any issue with code please click on resend code . Fields marked with a</span>
                                                            <span class="redAsterick">*</span><span class="tableHeader"> are required.</span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <img height="7" src="images/spacer.gif" width="3" alt="">
                                                        </td>
                                                        <td width="127">
                                                            <img height="7" src="images/spacer.gif" width="3" alt="">
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="ErrorValidation" runat="server" CssClass="redAsterick" />
                                                            <asp:Label ID="ErrorLabel" runat="server" CssClass="redAsterick" />
                                                            <asp:RequiredFieldValidator ValidationGroup="verificationcode" ID="RFV_txt_code" CssClass="redAsterick" runat="server"
                                                                ControlToValidate="txt_code" ErrorMessage="<%$ Resources:Resource, el_rgn_verrormsg%>" />
                                                        </td>
                                                    </tr>
                                                    <tr bgcolor="#f2f5f8">
                                                        <td>
                                                            <img height="30" src="images/spacer.gif" width="3" alt="">
                                                        </td>
                                                        <td class="bodyCopy" align="right" width="127">
                                                            <label for="txt_code" class="bodyCopy"><%=Resources.Resource.el_Rgn_verificationcode %></label>&nbsp;
                                                        </td>
                                                        <td class="bodyCopy">
                                                            &nbsp;<SPS:SPSTextBox ID="txt_code" runat="server" ValidationGroup="verificationcode" CssClass="bodyCopy" MaxLength="8"
                                                                aria-required="true"/>
                                                            <span class="redAsterick">*</span>&nbsp;&nbsp;
                                                            <asp:LinkButton ID="btn_Resend_Code" runat="server" Text="<%$ Resources:Resource, el_rgn_resend%>"
                                                                CssClass="tableHeaderLink" AlternateText="<%$ Resources:Resource, el_rgn_resend%>"
                                                                title="Click to resend your email verification code."/>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <img height="30" src="images/spacer.gif" width="3" alt="">
                                                        </td>
                                                        <td colspan="2" align="center">
                                                            <asp:ImageButton ID="NextButton" ImageUrl="~/images/sp_int_next_btn.gif"
                                                                ValidationGroup="verificationcode" runat="server"
                                                                AlternateText="Go to the next page to continue your registration." role="button"/>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3">
                                                            <footer><ServicePLUSWebApp:SonyFooter ID="SonyFooter1" runat="server" /></footer>
                                                        </td>
                                                    </tr>
                                                </table>
                                                </main>
                                            </td>
                                            <td width="20">
                                                <img height="20" src="images/spacer.gif" width="20" alt="">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td style="width: 25px; background: url(images/sp_right_bkgd.gif);">
                        <img src="images/spacer.gif" width="25" height="25" alt="" />
                    </td>
                </tr>
            </table>
        </center>
    </form>
</body>
</html>
