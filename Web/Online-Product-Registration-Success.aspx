<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Online-Product-Registration-Success.aspx.vb"
    Inherits="ServicePLUSWebApp.Online_Product_Registration_Success" CodeFileBaseClass="ServicePLUSWebApp.SSL" %>

<%@ Register TagPrefix="ServicePLUSWebApp" TagName="SonyFooter" Src="~/SonyFooter.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="TopSonyHeader" Src="~/TopSonyHeader.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="ServicePlusNav" Src="~/ServicePlusNav.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head id="Head1" runat="server">
    <title>Sony Online Product Registration</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link href="includes/ServicesPLUS_style.css" type="text/css" rel="stylesheet">
    <script type="text/javascript" src="includes/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="includes/svcplus_globalization.js"></script>

    <script language="javascript" src="includes/ServicesPLUS.js" type="text/javascript"></script>

</head>
<body style="color: #000000; background: #5d7180; margin: 0px;">
    <form id="frmOnlineProductRegistration" method="post" runat="server">
        <center>
            <table width="710" border="0" align="center" role="presentation">
                <tr>
                    <td width="710" bgcolor="#ffffff">
                        <table width="710" border="0" role="presentation">
                            <tr>
                                <td>
                                    <ServicePLUSWebApp:TopSonyHeader ID="TopSonyHeader" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <nav><ServicePLUSWebApp:ServicePlusNav ID="SonyNav" runat="server" /></nav>
                                    <asp:Label ID="Label1" runat="server" CssClass="redAsterick"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 708px" valign="middle">
                                    <table width="710" border="0" role="presentation">
                                        <tr>
                                            <td valign="top" align="right" width="464" background="images/pghdr_prodreg.jpg"
                                                bgcolor="#363d45" height="82">
                                            </td>
                                            <td valign="middle" bgcolor="#363d45">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 464px" bgcolor="#f2f5f8">
                                                <img src="images/spacer.gif" width="16" alt="">&nbsp;
                                            </td>
                                            <td style="width: 246px" bgcolor="#99a8b5">
                                            </td>
                                        </tr>
                                        <tr height="9">
                                            <td bgcolor="#f2f5f8" valign="bottom" background="images/sp_int_header_btm_left_onepix.gif">
                                                <%--<img src="images/sp_int_header_btm_left_onepix.gif" width="464" height="9" alt="">--%>
                                            </td>
                                            <td bgcolor="" valign="bottom" background="images/sp_int_header_btm_right.gif">
                                                <%-- <img src="images/sp_int_header_btm_right.gif" width="246" height="9" alt="">--%>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <img height="15" src="images/spacer.gif" width="20" alt="">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="710" height="300" border="0" role="presentation">
                                        <tr>
                                            <td width="20" height="15">
                                                <img height="15" src="images/spacer.gif" width="20" alt="">
                                            </td>
                                            <td width="678" height="100%" valign="top">
                                                <table border="0" role="main">
                                                    <tr>
                                                        <td>

                                                            <span class="bodyCopyBoldBigText2">Sony Professional Product Registration Confirmation</span>
                                                            <br />
                                                            <img height="20" src="images/spacer.gif" alt="" />
                                                            <br />
                                                            <span class="bodyCopyBold">Thank you for registering your Sony <%=sModel%> on our web site.</span>
                                                            <br />
                                                            <img height="20" src="images/spacer.gif" alt="" />
                                                            <br />
                                                            <div runat="server" id="divSuccess" visible="false">
                                                                <span class="bodyCopyBold"><%=sSucessMessage%>.</span>
                                                                <br />
                                                                <img height="20" src="images/spacer.gif" alt="" />
                                                                <br />
                                                            </div>
                                                            <span class="bodyCopy">You will be receiving an email to confirm that you have registered along with a link to Terms and Conditions of Product Warranty.</span>
                                                            <br />
                                                            <img height="10" src="images/spacer.gif" alt="" />
                                                            <br />
                                                            <span class="bodyCopy">If you would like to register a different model, please click the button below:</span>
                                                            <br />
                                                            <img height="20" src="images/spacer.gif" alt="" />
                                                            <br />
                                                            <input type="button" class="bodycopy" name="SUBMIT" value="REGISTER A DIFFERENT MODEL" style="cursor: pointer" style="width: 220px" onclick="window.location = 'Online-Product-Registration-2.aspx'; return false;" />
                                                            <br />
                                                            <img height="20" src="images/spacer.gif" alt="" />
                                                            <br />
                                                            <span class="bodyCopy">Once again, thank you for choosing Sony.</span>

                                                            <br />
                                                            <img height="20" src="images/spacer.gif" alt="" />
                                                            <br />
                                                            <span class="bodyCopy">For complete Limited Warranty Terms & Conditions, click <a href="http://pro.sony.com/bbsc/ssr/services.servicesprograms.bbsccms-services-servicesprograms-warrantyinformation.shtml"
                                                                target="_blank">here</a></span>
                                                            <br />
                                                            <img height="5" src="images/spacer.gif" alt="" />
                                                            <br />
                                                            <span class="bodyCopy">To learn more about Sony Professional products, please visit: <a href="http://www.sony.com/professional"
                                                                target="_blank">http://www.sony.com/professional</a></span>
                                                            <br />
                                                            <img height="5" src="images/spacer.gif" alt="" />
                                                            <br />
                                                            <span class="bodyCopy">To register future Sony Professional purchases, please go to: <a href="http://www.sony.com/PRO"
                                                                target="_blank">http://www.sony.com/PRO</a></span>
                                                            <br />
                                                            <img height="5" src="images/spacer.gif" alt="" />
                                                            <br />
                                                            <span class="bodyCopy">For Service and Support, please go to: <a href="http://www.sony.com/professionalservices"
                                                                target="_blank">http://www.sony.com/professionalservices</a></span>
                                                            <br />
                                                            <img height="5" src="images/spacer.gif" alt="" />
                                                            <br />
                                                            <span class="bodyCopy">For parts, software and training, please go to: <a href="http://www.sony.com/servicesplus"
                                                                target="_blank">http://www.sony.com/servicesplus</a></span>


                                                        </td>
                                                    </tr>

                                                </table>
                                            </td>
                                            <td width="20" height="15">
                                                <img height="15" src="images/spacer.gif" width="20" alt="">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="1">
                                    <footer><ServicePLUSWebApp:SonyFooter ID="SonyFooter1" runat="server" /></footer>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </center>
    </form>
</body>
</html>
