Namespace ServicePLUSWebApp
    Public Class RepairStatus
        Inherits System.Web.UI.UserControl

        Private strCarrierCode As String = String.Empty
        Private strTrackingNumber As String = String.Empty
        Private strCarrierURL As String = String.Empty


        Public Property Header() As String
            Get
                Return lblHeader.Text
            End Get
            Set(ByVal value As String)
                lblHeader.Text = value
            End Set
        End Property
        Public Property Model() As String
            Get
                Return lblModelText.Text
            End Get
            Set(ByVal value As String)
                lblModelText.Text = value
            End Set
        End Property
        Public Property Serial() As String
            Get
                Return lblSerialText.Text
            End Get
            Set(ByVal value As String)
                lblSerialText.Text = value
            End Set
        End Property
        Public Property Status() As String
            Get
                Return lblStatusText.Text
            End Get
            Set(ByVal value As String)
                lblStatusText.Text = value
            End Set
        End Property
        Public Property Enquire() As String
            Get
                Return lblEnquire.Text
            End Get
            Set(ByVal value As String)
                lblEnquire.Text = value
            End Set
        End Property

        Public Property TrackingNumber() As String
            Get
                Return strTrackingNumber
            End Get
            Set(ByVal value As String)
                strTrackingNumber = value.Replace("-", "").Replace(" ", "")
            End Set
        End Property

        Public Property CarrierCode() As String
            Get
                Return strCarrierCode
            End Get
            Set(ByVal value As String)

                strCarrierCode = value
            End Set
        End Property
        Public Property CarrierURL() As String
            Get
                Return strCarrierURL
            End Get
            Set(ByVal value As String)

                strCarrierURL = value
            End Set
        End Property

        Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
            If strCarrierCode <> String.Empty And strTrackingNumber <> String.Empty And strCarrierURL <> String.Empty Then
                trShipping.Visible = True
                lblShipingInfo.Visible = True
                lblShipingInfoText.Text = strCarrierCode + " - Tracking Number <A href='#' onclick=""window.open('" + strCarrierURL + "');return false;"">" + strTrackingNumber + "</A>"
            ElseIf strCarrierCode = String.Empty And strTrackingNumber <> String.Empty Then
                trShipping.Visible = True
                lblShipingInfo.Visible = True
                lblShipingInfoText.Text = "Tracking Number " + strTrackingNumber
            Else
                lblShipingInfo.Visible = False
                lblShipingInfoText.Text = String.Empty
                trShipping.Visible = False
            End If
        End Sub

        Public WriteOnly Property LatestSearch() As Boolean
            Set(ByVal value As Boolean)
                tdHeader.BgColor = "DarkKhaki"
            End Set
        End Property
    End Class
End Namespace