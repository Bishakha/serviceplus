Imports System.Xml
Imports System.Xml.Xsl
Imports System.Text
Imports System.IO
Namespace ServicePLUSWebApp


    Public Class UnderMaintenance
        Inherits SSL

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Dim strSesion As String
            If Not Session.Item("customer") Is Nothing Then
                strSesion = 1
            Else
                strSesion = 0
            End If
            Dim strBuildNum As String = Application("Build").ToString()
            Dim xmlDoc As XmlDocument = New XmlDocument()
            xmlDoc.LoadXml("<NewDataSet><CONTACT><MAINTENANCE>1</MAINTENANCE><SESSION>" + strSesion + "</SESSION><BUILDNUM>" + strBuildNum + "</BUILDNUM></CONTACT></NewDataSet>")
            Dim xslPath As String = Server.MapPath("Contact.xslt")
            Dim sb As StringBuilder = New StringBuilder
            Dim sw As StringWriter = New StringWriter(sb)
            Dim transform As XslTransform = New XslTransform
            transform.Load(xslPath)
            transform.Transform(xmlDoc, Nothing, sw, Nothing)
            tableContact.InnerHtml = sb.ToString()
            sw.Flush()
            sb.Length = 0
        End Sub

    End Class
End Namespace