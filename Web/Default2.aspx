﻿<%@ page language="C#" autoeventwireup="true" %>

<%@ Register TagPrefix="ServicePLUSWebApp" TagName="bodyContent" Src="bodyContent.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="TopSonyHeader" Src="TopSonyHeader.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="ServicePlusNav" Src="ServicePlusNav.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="SonyFooter" Src="SonyFooter.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="SPSPromotion" Src="~/UserControl/SPSPromotion.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>
    <link href="includes/ServicesPLUS_style.css" type="text/css" rel="stylesheet" />
   <script language="javascript" src="includes/ServicesPLUS.js" type="text/javascript"></script>
   <script type="text/javascript" src="includes/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="includes/svcplus_globalization.js"></script>


    <script language="javascript" type="text/jscript" >
       function reloadImg()
        {
             var id1 = document.getElementById("txtImageId");
             var obj = document.getElementById(id1.value);
             var src = obj.src;
            
             alert(src);
             
             var pos = src.indexOf('?');
             if (pos >= 0) 
             {
                src = src.substr(0, pos);
             }
   
             var date = new Date();
             obj.src = src + '?v=' + date.getTime();
             return false;
        }
        
       
    </script>

</head>
<body text="#000000" bgcolor="#5d7180">
    <form id="form1" runat="server">
    <div>
        <center>
            <table width="760" border="0">
                <tr>
                    <td width="25">
                        <img height="25" src="images/spacer.gif" alt="Value" id="spacer1" width="25" />
                    </td>
                    <td width="710" bgcolor="#ffffff">
                        <table width="710" border="0">
                            <tr>
                                <td>
                                    <ServicePLUSWebApp:TopSonyHeader ID="TopSonyHeader" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <nav><ServicePLUSWebApp:ServicePlusNav ID="ServicePlusNavDisplay" runat="server" /></nav>
                                    <asp:Label ID="ErrorLabel" runat="server" CssClass="redAsterick"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 710px; height: 17px">
                                </td>
                            </tr>
                            <tr>
                                <td width="710">
                                    <img height="20" src="images/spacer.gif" id="spacer2" width="20" alt="test" />
                                </td>
                            </tr>
                            <tr>
                                <td width="710">
                                    <img height="20" src="images/spacer.gif" id="spacer3" width="20" alt="test" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="710" border="0">
                                        <tr>
                                            <td width="20">
                                                <img height="20" src="images/spacer.gif" id="spacer4" width="20" alt="test" />
                                            </td>
                                            <td>
                                                <table border="0">
                                                    <tr valign="top">
                                                        <td style="width: 430px">
                                                            <table border="0">
                                                                <tr>
                                                                    <td>
                                                                        <a href="./sony-parts.aspx">
                                                                            <img style="width: 70px; height: 50px" height="50" alt="Parts and Accessories" src="images/sp_int_home_parts.gif"
                                                                                width="70" border="0" id="HomeParts" />
                                                                        </a>
                                                                    </td>
                                                                    <td width="10">
                                                                        <img src="images/spacer.gif" id="spacer5" width="10" alt="test" />
                                                                    </td>
                                                                    <td class="bodyCopySM">
                                                                        <a class="promoCopyBold" href="./sony-parts.aspx">Parts and Accessories</a>&nbsp;
                                                                        - Find and purchase Sony genuine parts, accessories, and overhaul kits for your
                                                                        Sony Professional Products.
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 70px" height="20">
                                                                        <img height="20" src="images/spacer.gif" id="spacer6" alt="test" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <a href="./sony-software.aspx">
                                                                            <img style="width: 70px; height: 50px" height="50" alt="Software and Firmware" src="images/sp_int_home_software.gif"
                                                                                width="70" border="0" id="Software and Firmware" />
                                                                        </a>
                                                                    </td>
                                                                    <td width="10">
                                                                        <img src="images/spacer.gif" width="10" id="spacer7" alt="test" />
                                                                    </td>
                                                                    <td class="bodyCopySM">
                                                                        <a class="promoCopyBold" href="./sony-software.aspx">Software and Firmware</a>&nbsp;
                                                                        - Get the latest software and firmware upgrades for Sony Professional Products.
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 70px" height="20">
                                                                        <img height="20" src="images/spacer.gif" id="spacer10" alt="test" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <a href="./sony-service-agreements.aspx">
                                                                            <img style="width: 70px; height: 50px" height="50" alt="Service Agreements" src="images/sp_int_home_serv-agrmnts.gif"
                                                                                width="70" border="0" id="serviceagreements" />
                                                                        </a>
                                                                    </td>
                                                                    <td width="10">
                                                                        <img src="images/spacer.gif" id="spacer11" width="10" alt="test" />
                                                                    </td>
                                                                    <td class="bodyCopySM">
                                                                        <span color="red"><b><a class="redAsterick" href="./sony-extended-warranties.aspx">New
                                                                            Feature: Purchase a Sony Extended Warranty</a></b></span><br />
                                                                        <a class="promoCopyBold" href="./sony-service-agreements.aspx">Service Agreements</a>
                                                                        &nbsp; - Purchase or Register your Extended Warranty or your Sony product online.
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 70px" height="20">
                                                                        <img height="20" id="spacer12" src="images/spacer.gif" alt="test" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <a href="./Sony-repair.aspx">
                                                                            <img style="width: 70px; height: 50px" height="50" alt="Depot and Onsite Repair"
                                                                                src="images/sp_int_home_repair.gif" width="70" border="0" >
                                                                        </a>
                                                                    </td>
                                                                    <td width="10">
                                                                        <img src="images/spacer.gif" id="spacer9" width="10" alt="test" />
                                                                    </td>
                                                                    <td class="bodyCopySM">
                                                                        <a class="promoCopyBold" href="./Sony-repair.aspx">Depot and Onsite Repair</a>&nbsp;
                                                                        - Request field or depot repair service and check depot service status for Sony
                                                                        Professional Products.
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 70px" height="20">
                                                                        <img height="20" src="images/spacer.gif" alt="test" id="spacer8" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <a href="./sony-training-search.aspx">
                                                                            <img style="width: 70px; height: 50px" height="50" alt="Sony Training Institute"
                                                                                src="images/sp_int_home_training.gif" width="70" border="0" />
                                                                        </a>
                                                                    </td>
                                                                    <td width="10">
                                                                        <img src="images/spacer.gif" id="spacer20" width="10" alt="test" />
                                                                    </td>
                                                                    <td class="bodyCopySM">
                                                                        <a class="promoCopyBold" href="./sony-training-search.aspx">Sony Training Institute</a>
                                                                        &nbsp; - Offering Production, Post-Production and Maintenance Courses with an emphasis
                                                                        on real-life knowledge for industry professionals. Classes feature a balance of
                                                                        presentation and hands-on experience.
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 70px" height="20">
                                                                        <img height="20" src="images/spacer.gif" id="spacer13" alt="test" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <a href="./sony-knowledge-base-search.aspx">
                                                                            <img style="width: 70px; height: 50px" height="50" alt="Knowledge Base" src="images/sp_int_home_techbulletin.gif"
                                                                                width="70" border="0" id="techbullentin" />
                                                                        </a>
                                                                    </td>
                                                                    <td width="10">
                                                                        <img src="images/spacer.gif" id="spacer14" width="10" alt="test" />
                                                                    </td>
                                                                    <td class="bodyCopySM">
                                                                        <a class="promoCopyBold" href="./sony-knowledge-base-search.aspx">Knowledge Base</a>&nbsp;-
                                                                        Search the Knowledge Base for information about Sony Business and Professional Products,
                                                                        including Technical Bulletins.
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 70px" height="20">
                                                                        <img height="20" src="images/spacer.gif" alt="test" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td width="17">
                                                            <img height="20" src="images/spacer.gif" width="17" alt="test">
                                                        </td>
                                                        <td width="230" height="260">
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        &nbsp;
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        &nbsp;
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="10">
                                                <img height="10" src="images/spacer.gif" width="10" alt="test" />
                                            </td>
                                            <td>
                                                <table border="0" cellspacing="0">
                                                    <tr>
                                                        <td>
                                                            <asp:TextBox ID="txtImageId" runat="server"></asp:TextBox>
                                                        </td>
                                                        <td width="15">
                                                            <img src="images/spacer.gif" id="testing" width="15" alt="test" />
                                                        </td>
                                                        <td>
                                                            <a href="#" onclick="return reloadImg();">Reload Image</a>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </center>
    </div>
    </form>
</body>
</html>
