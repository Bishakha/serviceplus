Imports System.IO
Imports System.Threading
Imports ServicesPlusException
Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Process
Imports Sony.US.SIAMUtilities

Namespace ServicePLUSWebApp
    Partial Class sony_knowledge_base_search
        Inherits SSL


#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Protected WithEvents lblSubmitError As Label
        Protected WithEvents lblSoftwareResults As Label
        Protected WithEvents lblNextResult As Label
        Protected WithEvents phCalendar1 As PlaceHolder
        Protected WithEvents phCalendar2 As PlaceHolder
        Protected WithEvents txtCal1 As Sony.US.ServicesPLUS.Controls.SPSTextBox
        Protected WithEvents txtCal2 As Sony.US.ServicesPLUS.Controls.SPSTextBox

        Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Init

            isProtectedPage(False)
            InitializeComponent()
            FormFieldInitialization()
        End Sub

#End Region

#Region "Page members"

#Region "constants"
        'category drop down messages
        Private Const conSearchParmsArrayLength As Short = 2
        Private Const conMaxResultsPerPage As Short = 50
        'file size denominator
        Private Const MegaBytes As Integer = 1064960
        Private Const KiloBytes As Integer = 1024
        'file path
        Private conTechBulletinPath As String
        'use to tell if the addToCart icon was clicked.
        Private Const conAddToCart As String = "addToCart"
        Private Const conTBGeneralInfo As String = "Sony-Technical-Bulletins-GenInfo.aspx"

#End Region

#Region "variables"
        'result page variables
        Shared currentResultPage As Integer = 1
        Shared endingResultPage As Integer = 1
        Shared startingResultItem As Integer = 1
        Shared resultPageRequested As Integer = 3
        Shared totalSearchResults As Long = 0
        Shared currRecordCount As Long = 0
        Shared lowerLimit As Long = 1
        Shared upperLimit As Long = 1
        Shared searchCondition As String = ""

        'sort order variables
        Private sortOrder As enumSortOrder = enumSortOrder.ProductCategoryASC
        'file download and release notes directory    
        Private downloadURL As String
        Private downloadPath As String
        Public focusField As String = "txtModelNo"
        Private SubjectKeyCollection() As String = Nothing
        'Dim objUtilties As Utilities = New Utilities
        Private searchFirstTime As Boolean = False
#End Region

#Region "enumerators"

        '-- result page
        Private Enum enumResultPageRequested
            PreviousPage = 0
            NextPage = 1
            UserEnteredPage = 2
            FirstPage = 3
            LastPage = 4
            Current = 5
        End Enum

        '-- sort order 
        Private Enum enumSortOrder
            ProductCategoryASC = 0
            ProductCategoryDESC = 1
            ModelNumberASC = 2
            ModelNumberDESC = 3
        End Enum

        '-- sort type
        Private Enum enumSortBy
            ProductCatagory = 0
            ModelNumber = 1
        End Enum
#End Region


#End Region

#Region "Events"

        Private Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
            Dim techManager As New TechBulletinManager

            Try
                If HttpContextManager.GlobalData.IsCanada Then
                    DefaultKB.Visible = False
                    hiddenKB.Visible = True
                    Return
                End If
                DefaultKB.Visible = True
                hiddenKB.Visible = False

                If HttpContextManager.Customer IsNot Nothing Then Response.Redirect("au-sony-knowledge-base-search.aspx")
                SetFileDownloadPaths()
                If Not Page.IsPostBack() Then
                    errorMessageLabel.Text = ""
                    searchCondition = ""
                    LoadDateControls()
                    HideNavigations()
                    errorMessageLabel.Text = ""
                End If

                If Not String.IsNullOrWhiteSpace(errorMessageLabel.Text) Then
                    HideNavigations()
                End If
            Catch ex As Exception
                errorMessageLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Sub btnSearch_Click(ByVal sender As Object, ByVal e As ImageClickEventArgs) Handles btnSearch.Click
            Dim IsSearchBySerialNo As Boolean = False
            Dim strKeyStrng As String = String.Empty

            Try
                searchCondition = "" ' need to clean up the previous value
                If ViewState("SubjectKey") IsNot Nothing Then ViewState.Remove("SubjectKey")

                errorMessageLabel.Text = ""
                'If txtDateFrom.Text.Trim() <> "" And txtDateTo.Text.Trim() <> "" Then
                If Not ValidateDates() Then
                    errorMessageLabel.Text = "Please select valid date from and date to"
                    errorMessageLabel.Focus()
                    Return
                End If
                'End If

                searchCondition += " Type = 1 "

                If Not String.IsNullOrWhiteSpace(txtModelNo.Text) Then
                    If Not searchCondition = "" Then searchCondition += " And "
                    txtModelNo.Text = txtModelNo.Text.Trim().ToLower().Replace("-", "")
                    searchCondition += $" (lower(Model) like '%{txtModelNo.Text}%' OR lower(old_model) like '%{txtModelNo.Text}%')"
                End If

                If Not String.IsNullOrWhiteSpace(txtSubject.Text) Then
                    strKeyStrng = txtSubject.Text
                    While strKeyStrng.IndexOf("  ") > 0
                        strKeyStrng = strKeyStrng.Replace("  ", " ")
                    End While
                    ViewState.Add("SubjectKey", strKeyStrng)
                    SubjectKeyCollection = strKeyStrng.Split(" ")
                    If SubjectKeyCollection.Length > 1 Then
                        For Each strKey As String In SubjectKeyCollection
                            If Not searchCondition = "" Then searchCondition += " And "
                            searchCondition += " lower(KEYWORDS) like '%" + strKey.ToLower() + "%'"
                        Next
                    Else
                        If Not searchCondition = "" Then searchCondition += " And "
                        searchCondition += " lower(KEYWORDS) like '%" + txtSubject.Text.Trim().ToLower() + "%'"
                    End If
                End If

                Try
                    'If txtDateFrom.Text <> "" Then
                    If Not searchCondition = "" Then searchCondition += " And "
                    searchCondition += $" DATEPUBLISHED  -  to_date('{txtDateFrom.Value}', 'YYYY-MM-DD') >= 0"       ' calDateFrom.SelectedDate.ToString("MM/dd/yyyy")
                    'End If
                Catch ex As Exception
                    errorMessageLabel.Text = "Please Enter a Valid from Date (format: mm/dd/yyyy)"
                    Return
                End Try

                Try
                    'If txtDateTo.Text <> "" Then
                    If Not searchCondition = "" Then searchCondition += " And "
                    searchCondition += $" to_date('{txtDateTo.Value}', 'YYYY-MM-DD') -   DATEPUBLISHED >= 0"       ' calDateTo.SelectedDate.ToString("MM/dd/yyyy")
                    'End If
                Catch ex As Exception
                    errorMessageLabel.Text = "Please Enter a Valid To Date (format: mm/dd/yyyy)"
                    Return
                End Try

                Try
                    If Not String.IsNullOrWhiteSpace(txtBulletinNo.Text) Then
                        If IsNumeric(txtBulletinNo.Text.Replace("-", "").Trim()) = False Then
                            errorMessageLabel.Text = "Please enter only numeric value for solution or bulletin number."
                            Return
                        Else
                            'txtBulletinNo.Text = Convert.ToInt64(txtBulletinNo.Text.Replace("-", "").Trim()).ToString()
                            If Not searchCondition = "" Then searchCondition += " And "
                            searchCondition += " to_char(ID ) like '" + txtBulletinNo.Text.Replace("-", "").Trim() + "%'"
                        End If
                    End If
                Catch ex As Exception
                    errorMessageLabel.Text = "Please enter only numeric value for solution or bulletin number."
                    Return
                End Try

                Try
                    If Not String.IsNullOrWhiteSpace(txtSerialNo.Text) Then
                        txtSerialNo.Text = Convert.ToInt64(txtSerialNo.Text.Trim()).ToString()
                        If Not searchCondition = "" Then
                            searchCondition += " And "
                        End If
                        searchCondition += Convert.ToInt64(txtSerialNo.Text.Trim()).ToString() + " between startofrange  and endofrange "
                        IsSearchBySerialNo = True
                    End If
                Catch ex As Exception
                    errorMessageLabel.Text = "Please enter only numeric value for serial number"
                    Return
                End Try

                If IsSearchBySerialNo = True Then
                    totalSearchResults = GetRecordcount(searchCondition, "true")
                Else
                    totalSearchResults = GetRecordcount(searchCondition, "false")
                End If

                lowerLimit = 0
                upperLimit = conMaxResultsPerPage
                currentResultPage = 1
                If totalSearchResults > conMaxResultsPerPage Then
                    endingResultPage = Convert.ToInt64(totalSearchResults / conMaxResultsPerPage)
                Else
                    endingResultPage = 1
                    'when the total search count is less than 50, then search only until that count
                    upperLimit = totalSearchResults
                End If

                ' StartLoggingSearch()
                Dim thread As Thread = New Thread(New ThreadStart(AddressOf StartLoggingSearch))
                thread.Start()

                If totalSearchResults > 0 Then
                    ShowNavigations()
                    searchFirstTime = True
                    PopulateDataGridByRow(searchCondition, lowerLimit, upperLimit)
                Else
                    HideNavigations()
                End If

            Catch ex As Exception
                errorMessageLabel.Text = Utilities.WrapExceptionforUI(ex)
                dgTechBulletinResults.Visible = False
            End Try
        End Sub


        Protected Sub dgTechBulletinResults_ItemCommand(ByVal source As Object, ByVal e As DataGridCommandEventArgs) Handles dgTechBulletinResults.ItemCommand
            Try
                Select Case e.CommandName
                    Case "BulletinClick"
                        Dim rec As TechBulletin = New TechBulletin()
                        rec.BulletinNo = CType(e.Item.FindControl("lblTBulletinNo"), Label).Text
                        rec.Subject = CType(e.Item.FindControl("lnkBulletin"), LinkButton).Text
                        rec.Type = IIf(CType(e.Item.FindControl("lblAccess"), HtmlAnchor).InnerText = "Free", 1, 2)
                        rec.Model = CType(e.Item.FindControl("lblModelList"), Label).Text
                        rec.PrintDate = CType(e.Item.FindControl("lblPrint"), Label).Text
                        Session.Add("TBObject", rec)
                        Dim filename As String = ""
                        Dim description As String = ""
                        If rec.Type <> 1 Then
                            If Not rec.Subject Is Nothing And Not rec.BulletinNo.ToString() Is Nothing Then
                                description = rec.Subject.ToString().Trim()
                                filename = rec.BulletinNo.ToString() + ".pdf"
                                If File.Exists(downloadPath + "/" + conTechBulletinPath + "/" + filename) Then
                                    Session.Add("TBFile", downloadURL + "/" + conTechBulletinPath + "/" + filename)
                                    If Session.Item("customer") Is Nothing Then
                                        Session.Add("RedirectTo", "sony-technical-bulletin.aspx?fn=" + rec.BulletinNo.ToString())
                                        Session.Add("LoginFrom", "Bulletin")
                                        'Response.Redirect("SignIn.aspx")
                                        Response.Redirect("SignIn-Register.aspx")
                                    Else
                                        If Session.Item("IsValidSubscription") Is Nothing Then
                                            Session.Add("RedirectTo", "sony-technical-bulletin.aspx?fn=" + rec.BulletinNo.ToString())
                                            Session.Add("LoginFrom", "Bulletin")
                                            Response.Redirect("Sony-Technical-Bulletins-GenInfo.aspx")
                                        ElseIf Convert.ToBoolean(Session.Item("IsValidSubscription")) = True Then
                                            Response.Redirect("sony-technical-bulletin.aspx?fn=" + rec.BulletinNo.ToString())
                                        Else
                                            Session.Add("RedirectTo", "sony-technical-bulletin.aspx?fn=" + rec.BulletinNo.ToString())
                                            Session.Add("LoginFrom", "Bulletin")
                                            Response.Redirect("Sony-Technical-Bulletins-GenInfo.aspx")
                                        End If
                                    End If
                                Else
                                    errorMessageLabel.Text = "File does not exist. Please call at 1-408-352-4272, 11:30 a.m. - 8:00 p.m. ET, Monday through Friday"
                                End If
                            End If
                        Else
                            If Not Session.Item("customer") Is Nothing Then
                                Response.Redirect("au-sony-knowledge-base-solution.aspx?filenum=" + rec.BulletinNo.ToString())
                            Else
                                Response.Redirect("sony-knowledge-base-solution.aspx?filenum=" + rec.BulletinNo.ToString())
                            End If
                        End If
                    Case Else

                End Select
            Catch ex As Exception
                errorMessageLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Sub dgTechBulletinResults_ItemDataBound(ByVal sender As Object, ByVal e As DataGridItemEventArgs) Handles dgTechBulletinResults.ItemDataBound
            Dim pdfPath As String = ""

            Try

                If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                    Dim rec As TechBulletin = CType(e.Item.DataItem, TechBulletin)
                    If rec IsNot Nothing Then
                        Dim lblValue As Label = CType(e.Item.FindControl("lblTBulletinNo"), Label)
                        If Not String.IsNullOrWhiteSpace(rec.BulletinNo) Then
                            lblValue.Text = rec.BulletinNo
                        Else
                            lblValue.Text = ""
                        End If
                        lblValue = Nothing

                        Dim modelArray() As String
                        Dim modeldata As String = ""
                        'Dim modelcnt As Long
                        lblValue = CType(e.Item.FindControl("lblModelList"), Label)
                        If Not String.IsNullOrWhiteSpace(rec.Model) Then
                            '- sort the model array before display
                            modelArray = rec.Model.Split(", ")
                            Array.Sort(modelArray)
                            'For modelcnt = 0 To modelArray.Length() - 1
                            '    If modelcnt <> modelArray.Length() - 1 Then
                            '        modeldata += modelArray(modelcnt) + ", "
                            '    Else
                            '        modeldata += modelArray(modelcnt)
                            '    End If
                            'Next
                            modeldata = String.Join(", ", modelArray)
                            'Added by Sneha on 11/11/13 
                            If Not String.IsNullOrEmpty(rec.OldModel) Then modeldata += $"<strong>&nbsp;(formerly known as {rec.OldModel})</strong>"
                            lblValue.Text = modeldata 'rec.Model
                        Else
                            lblValue.Text = ""
                        End If
                        lblValue = Nothing

                        lblValue = CType(e.Item.FindControl("lblPrint"), Label)
                        If Not rec.PrintDate Is Nothing Then
                            Dim dispdate As Date = rec.PrintDate
                            lblValue.Text = dispdate.ToShortDateString()
                        Else
                            lblValue.Text = ""
                        End If

                        lblValue = Nothing

                        Dim lblAccess As HtmlAnchor = CType(e.Item.FindControl("lblAccess"), HtmlAnchor)
                        If rec.Type <> 1 Then
                            lblAccess.Attributes.Add("onclick", "javascript:window.open('http://pro.sony.com/bbsc/ssr/services.servicesprograms.bbsccms-services-servicesprograms-subscription.shtml'); return false;")
                            lblAccess.InnerText = "Subscription"
                            lblAccess.HRef = "#"
                        Else
                            lblAccess.InnerText = "Free"
                        End If

                        Dim lnkValue As LinkButton = CType(e.Item.FindControl("lnkBulletin"), LinkButton)

                        If Not ViewState("SubjectKey") Is Nothing Then
                            SubjectKeyCollection = ViewState("SubjectKey").ToString().Split(" ")
                            If SubjectKeyCollection.Length >= 1 Then
                                Dim strKeyToHighlight As String = String.Empty
                                Dim blnHighlight As Boolean = True
                                For Each strKey As String In SubjectKeyCollection

                                    'For Each strInnerKey As String In SubjectKeyCollection
                                    '    If strInnerKey <> strKey And strInnerKey.ToLower().IndexOf(strKey.ToLower()) > 0 Then blnHighlight = False
                                    'Next
                                    If rec.Subject.ToLower().IndexOf(strKey.ToLower()) = 0 Then
                                        strKey = strKey + " "
                                    Else
                                        If rec.Subject.ToLower().IndexOf(strKey.ToLower()) + strKey.Length = rec.Subject.Length Then
                                            strKey = " " + strKey
                                        ElseIf rec.Subject.ToLower().IndexOf(strKey.ToLower() + ".") + strKey.Length + 1 = rec.Subject.Length Then
                                            strKey = " " + strKey
                                        ElseIf rec.Subject.ToLower().IndexOf(strKey.ToLower() + "?") + strKey.Length + 1 = rec.Subject.Length Then
                                            strKey = " " + strKey
                                        Else
                                            strKey = " " + strKey + " "
                                        End If
                                    End If
                                    If rec.Subject.ToLower().IndexOf(strKey.ToLower()) > 0 Then
                                        strKeyToHighlight = rec.Subject.Substring(rec.Subject.ToLower().IndexOf(strKey.ToLower()), strKey.Length)
                                        rec.Subject = rec.Subject.Replace(strKeyToHighlight, "<span class=""HighlightedText"">" + strKeyToHighlight + "</span>")
                                    End If
                                    'If blnHighlight And rec.Subject.ToLower.IndexOf(strKey.ToLower() + "</span>") < 0 Then
                                    'End If
                                Next
                            End If
                        End If

                        lnkValue.Text = rec.Subject.ToString().Trim()
                        Dim filename As String = String.Empty
                        If rec.Type <> 1 Then
                            If Not rec.Subject Is Nothing And Not rec.BulletinNo.ToString() Is Nothing Then
                                filename = rec.BulletinNo.ToString() + ".pdf"
                                filename = downloadPath + "/" + conTechBulletinPath + "/" + filename
                                If Not File.Exists(filename) Then
                                    lnkValue.Attributes.Add("onclick", "mywin=window.open('fileUnavailableOnline.aspx','','" + CON_POPUP_FEATURES + "'); return false;")
                                End If
                            Else
                                lnkValue.Visible = False
                            End If
                        End If
                        lnkValue = Nothing
                    End If
                End If
            Catch ex As Exception
                errorMessageLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try

        End Sub

        Private Sub btnGoToPage_Click(ByVal sender As Object, ByVal e As ImageClickEventArgs) Handles btnGoToPage.Click
            Try
                FetchSpecifiedResultSet(1)
            Catch ex As Exception
                errorMessageLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Sub btnGoToPage2_Click(ByVal sender As Object, ByVal e As ImageClickEventArgs) Handles btnGoToPage2.Click
            Try
                FetchSpecifiedResultSet(2)
            Catch ex As Exception
                errorMessageLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Sub NextLinkButton_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lnkNextPage1.ServerClick, lnkNextPage2.ServerClick
            Try
                FetchNextResultSet()
            Catch ex As Exception
                errorMessageLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Sub lnkPreviousPage_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lnkPreviousPage1.ServerClick, lnkPreviousPage2.ServerClick
            Try
                FetchPreviousResultSet()
            Catch ex As Exception
                errorMessageLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        'Protected Sub lnkWhatsNew_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lnkWhatsNew.Click
        '    Dim dateTo As Date = Date.Today()
        '    txtDateTo.Text = dateTo.ToShortDateString()
        '    txtDateFrom.Text = dateTo.AddMonths(-1).ToShortDateString()
        '    'calDateTo.SelectedDate = dateTo
        '    'calDateFrom.SelectedDate = dateTo.AddMonths(-1)
        '    txtBulletinNo.Text = String.Empty
        '    txtModelNo.Text = String.Empty
        '    txtSerialNo.Text = String.Empty
        '    txtSubject.Text = String.Empty
        '    btnSearch_Click(sender, Nothing)
        'End Sub

#End Region

#Region "Methods"

        Private Sub StartLoggingSearch()
            'TODO: Log into database
            'SEARCHID      INTEGER,
            'TBINCLUDED    VARCHAR2(3 BYTE),
            'ID            VARCHAR2(15 BYTE),
            'MODEL         VARCHAR2(50 BYTE),
            'SERIALNUMBER  INTEGER,
            'DATEFROM      DATE,
            'DATETO        DATE,
            'HITS          INTEGER,
            'UPDATEDATE    DATE  
            Try
                Dim xCUSTOMERID As Integer = "-1"
                Dim xCUSTOMERSEQUENCENUMBER As Integer = "-1"
                If Not Session.Item("customer") Is Nothing Then
                    xCUSTOMERID = Convert.ToInt32(CType(Session.Item("customer"), Customer).CustomerID)
                    xCUSTOMERSEQUENCENUMBER = Convert.ToInt32(CType(Session.Item("customer"), Customer).SequenceNumber)
                End If
                Dim xHTTP_X_FORWARDED_FOR As String = HttpContextManager.GetServerVariableValue("HTTP_X_FORWARDED_FOR")
                Dim xREMOTE_ADDR As String = HttpContextManager.GetServerVariableValue("REMOTE_ADDR")
                Dim xHTTP_REFERER As String = HttpContextManager.GetServerVariableValue("HTTP_REFERER")
                Dim xHTTP_URL As String = HttpContextManager.GetServerVariableValue("HTTP_URL")

                If xHTTP_URL = "" Then
                    xHTTP_URL = HttpContextManager.GetServerVariableValue("URL") + ReturnQueryString()
                End If

                Dim xHTTP_USER_AGENT As String = HttpContextManager.GetServerVariableValue("HTTP_USER_AGENT")

                Dim techManager As TechBulletinManager = New TechBulletinManager
                Dim intSerialNumber As Integer = Nothing
                If txtSerialNo.Text.Trim() <> "" Then
                    intSerialNumber = Convert.ToInt32(txtSerialNo.Text.Trim())
                End If
                'Dim datefrom As Date = Nothing
                'Dim dateto As Date = Nothing
                'If txtDateFrom.Text.Trim() <> "" Then
                '    datefrom = (Convert.ToDateTime(txtDateFrom.Text).Date)
                'End If
                'If txtDateTo.Text.Trim() <> "" Then
                '    dateto = (Convert.ToDateTime(txtDateTo.Text).Date)
                'End If

                techManager.LogKBSearchResult("NA", txtBulletinNo.Text, txtModelNo.Text.Trim(), intSerialNumber, txtSubject.Text, txtDateFrom.Value,
                                              txtDateTo.Value, totalSearchResults, xCUSTOMERID, xCUSTOMERSEQUENCENUMBER, xHTTP_X_FORWARDED_FOR, xREMOTE_ADDR,
                                              xHTTP_REFERER, xHTTP_URL, xHTTP_USER_AGENT)
                Threading.Thread.CurrentThread.Abort()
            Catch ex As ThreadAbortException
                'Do nothing
            Catch ex As Exception
                'Do nothing for now
            End Try
        End Sub

        Private Sub LoadDateControls()
            Try
                'Dim dateFrom As Date = Convert.ToDateTime("01/01/1967")
                'Dim dateTo As Date = DateTime.Today()

                txtDateFrom.Value = New Date(1967, 1, 1).ToString("yyyy-MM-dd")
                txtDateTo.Value = Date.Today().ToString("yyyy-MM-dd")
                'calDateFrom.SelectedDate = New DateTime("01/01/1967")
                'calDateTo.SelectedDate = DateTime.Today()
            Catch ex As Exception
                errorMessageLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Sub UpdatePageDisplay()
            lblCurrentPage.Text = currentResultPage
            lblEndingPage.Text = endingResultPage
            lblCurrentPage2.Text = currentResultPage
            lblEndingPage2.Text = endingResultPage

            ' 2018-09-26 ASleight - Writing this logic a bit better.
            tblPagination1.Visible = endingResultPage > 1
            tblPagination2.Visible = endingResultPage > 1
            lnkNextPage1.Visible = currentResultPage < endingResultPage
            lnkNextPage2.Visible = currentResultPage < endingResultPage
            lnkPreviousPage1.Visible = currentResultPage > 1
            lnkPreviousPage2.Visible = currentResultPage > 1

            'If endingResultPage = 1 Then ' only one page is available 
            '    lblPage1.Visible = False
            '    lblPage2.Visible = False
            '    txtPageNumber.Visible = False
            '    txtPageNumber2.Visible = False

            '    btnGoToPage.Visible = False
            '    btnGoToPage2.Visible = False
            '    btnNextPage.Visible = False
            '    btnNextPage2.Visible = False
            '    btnPreviousPage.Visible = False
            '    btnPreviousPage2.Visible = False
            '    PreviousLinkButton.Visible = False
            '    PreviousLinkButton2.Visible = False
            '    NextLinkButton.Visible = False
            '    NextLinkButton2.Visible = False
            'ElseIf endingResultPage = currentResultPage Then ' reached the last page
            '    lblPage1.Visible = True
            '    lblPage2.Visible = True
            '    txtPageNumber.Visible = True
            '    txtPageNumber2.Visible = True

            '    btnGoToPage.Visible = True
            '    btnGoToPage2.Visible = True
            '    btnNextPage.Visible = False
            '    btnNextPage2.Visible = False
            '    btnPreviousPage.Visible = True
            '    btnPreviousPage2.Visible = True
            '    PreviousLinkButton.Visible = True
            '    PreviousLinkButton2.Visible = True
            '    NextLinkButton.Visible = False
            '    NextLinkButton2.Visible = False
            'ElseIf currentResultPage = 1 Then ' first page
            '    lblPage1.Visible = True
            '    lblPage2.Visible = True
            '    txtPageNumber.Visible = True
            '    txtPageNumber2.Visible = True

            '    btnGoToPage.Visible = True
            '    btnGoToPage2.Visible = True
            '    btnNextPage.Visible = True
            '    btnNextPage2.Visible = True
            '    btnPreviousPage.Visible = False
            '    btnPreviousPage2.Visible = False
            '    PreviousLinkButton.Visible = False
            '    PreviousLinkButton2.Visible = False
            '    NextLinkButton.Visible = True
            '    NextLinkButton2.Visible = True
            'Else ' intermittent pages - make all buttons visible
            '    lblPage1.Visible = True
            '    lblPage2.Visible = True
            '    txtPageNumber.Visible = True
            '    txtPageNumber2.Visible = True

            '    btnGoToPage.Visible = True
            '    btnGoToPage2.Visible = True
            '    btnNextPage.Visible = True
            '    btnNextPage2.Visible = True
            '    btnPreviousPage.Visible = True
            '    btnPreviousPage2.Visible = True
            '    PreviousLinkButton.Visible = True
            '    PreviousLinkButton2.Visible = True
            '    NextLinkButton.Visible = True
            '    NextLinkButton2.Visible = True

            'End If

        End Sub

        Private Sub FetchNextResultSet()

            If lowerLimit < upperLimit Then
                lowerLimit = upperLimit + 1 ' 0-50, 51-100
            End If

            If upperLimit < totalSearchResults Then
                If (totalSearchResults - upperLimit) > conMaxResultsPerPage Then
                    upperLimit += conMaxResultsPerPage
                Else
                    upperLimit = totalSearchResults - upperLimit
                End If
            End If
            PopulateDataGridByRow(searchCondition, lowerLimit, upperLimit)
            currentResultPage += 1
            UpdatePageDisplay()

        End Sub

        Private Sub FetchPreviousResultSet()

            If lowerLimit < upperLimit And lowerLimit > 1 Then
                upperLimit = lowerLimit - 1
                If upperLimit - conMaxResultsPerPage + 1 > 0 Then
                    lowerLimit = upperLimit - conMaxResultsPerPage + 1
                Else
                    lowerLimit = 0
                End If
            End If
            PopulateDataGridByRow(searchCondition, lowerLimit, upperLimit)
            currentResultPage -= 1
            UpdatePageDisplay()
        End Sub

        Private Sub FetchSpecifiedResultSet(ByVal btnId As Integer)
            errorMessageLabel.Text = ""
            Dim tempPageNumber As Long = 0
            Dim valTxt As Long
            Try
                If txtPageNumber2.Text.Trim() <> "" And btnId = 2 Then
                    valTxt = Convert.ToInt64(txtPageNumber2.Text.Trim())
                End If
                If txtPageNumber.Text.Trim() <> "" And btnId = 1 Then
                    valTxt = Convert.ToInt64(txtPageNumber.Text.Trim())
                End If
            Catch ex As Exception
                errorMessageLabel.Text = "Please enter a valid page number"
                Return
            End Try
            If txtPageNumber.Text.Trim() <> "" Or txtPageNumber2.Text.Trim() <> "" Or valTxt < 1 Then
                If IsNumeric(txtPageNumber.Text.ToString()) And btnId = 1 Then
                    tempPageNumber = Integer.Parse(txtPageNumber.Text.ToString())
                ElseIf IsNumeric(txtPageNumber2.Text.ToString()) And btnId = 2 Then
                    tempPageNumber = Integer.Parse(txtPageNumber2.Text.ToString())
                End If
                If Not tempPageNumber > endingResultPage Then ' tempPageNumber < or = endingResultPage
                    errorMessageLabel.Text = ""
                    If tempPageNumber = 1 Then
                        lowerLimit = 0
                        If totalSearchResults > conMaxResultsPerPage Then
                            upperLimit = conMaxResultsPerPage
                        Else
                            upperLimit = totalSearchResults
                        End If
                    Else
                        ' the requested page is not the first page
                        If tempPageNumber = endingResultPage Then
                            lowerLimit = (tempPageNumber - 1) * conMaxResultsPerPage + 1
                            upperLimit = totalSearchResults
                        Else
                            ' the requested page is in between the first and the last page
                            upperLimit = conMaxResultsPerPage * tempPageNumber
                            lowerLimit = upperLimit - conMaxResultsPerPage + 1
                        End If
                    End If
                    PopulateDataGridByRow(searchCondition, lowerLimit, upperLimit)
                    currentResultPage = tempPageNumber
                    UpdatePageDisplay()
                Else
                    errorMessageLabel.Text = "Please enter a page lesser than the total number of pages"
                End If
            Else
                errorMessageLabel.Text = "Please enter a valid page number"
            End If
            txtPageNumber.Text = ""
            txtPageNumber2.Text = ""
        End Sub

        Private Function GetRecordcount(ByVal strSearch As String, ByVal searchSerialNo As String) As Long
            Dim techManager As TechBulletinManager = New TechBulletinManager
            Dim recCount As Long = techManager.getTechBulletinRecCount(strSearch, searchSerialNo)
            Return recCount
        End Function

        Private Sub PopulateDataGrid(ByVal strSearch As String)
            Dim techManager As TechBulletinManager = New TechBulletinManager
            dgTechBulletinResults.DataSource = techManager.searchTechBulletins(strSearch)
            dgTechBulletinResults.DataBind()
        End Sub

        Private Sub PopulateDataGridByRow(ByVal strSearch As String, ByVal minrow As Long, ByVal maxrow As Long)
            Dim techManager As TechBulletinManager = New TechBulletinManager
            'Added By Sneha
            If Not String.IsNullOrEmpty(txtModelNo.Text.Trim) AndAlso searchFirstTime Then
                'check for confliction
                Dim lstConficts As List(Of String) = techManager.getConflictedModels(txtModelNo.Text.Trim())
                Dim strModels As String = String.Empty
                If lstConficts.Count > 0 Then
                    'call pop up
                    For Each item In lstConficts
                        strModels = strModels + item + ","
                    Next
                    If Not String.IsNullOrEmpty(strModels) Then strModels = strModels.Remove(strModels.Length - 1, 1)
                    ScriptManager.RegisterStartupScript(Me, [GetType](),
                    "showPopup", "javascript:showModalPopUp(" +
                    String.Format("{0},'{1}'", lstConficts.Count, strModels) + ");", True)
                End If
            End If
            dgTechBulletinResults.DataSource = techManager.searchTechBulletinsByRows(strSearch, minrow, maxrow)
            dgTechBulletinResults.DataBind()
            UpdatePageDisplay()
            searchFirstTime = False
        End Sub

        Private Sub HideNavigations()
            'lblCurrentPage.Visible = False
            'lblEndingPage.Visible = False
            'lblCurrentPage2.Visible = False
            'lblEndingPage2.Visible = False
            'btnGoToPage.Visible = False
            'btnGoToPage2.Visible = False
            'btnNextPage.Visible = False
            'btnNextPage2.Visible = False
            'btnPreviousPage.Visible = False
            'btnPreviousPage2.Visible = False
            'txtPageNumber.Visible = False
            'txtPageNumber2.Visible = False
            'PreviousLinkButton.Visible = False
            'PreviousLinkButton2.Visible = False
            'NextLinkButton.Visible = False
            'NextLinkButton2.Visible = False
            'lblof1.Visible = False
            'lblof2.Visible = False
            'lblPage1.Visible = False
            'lblPage2.Visible = False
            'trBackToTop.Visible = False
            'errorMessageLabel.Text = "No matches found. Please revise your search criteria."
            'dgTechBulletinResults.Visible = False

            tblPagination1.Visible = False
            tblPagination2.Visible = False
            tblBackToTop.Visible = False
            dgTechBulletinResults.Visible = False
        End Sub

        Private Sub ShowNavigations()
            'lblCurrentPage.Visible = True
            'lblEndingPage.Visible = True
            'lblCurrentPage2.Visible = True
            'lblEndingPage2.Visible = True
            'btnGoToPage.Visible = True
            'btnGoToPage2.Visible = True
            'btnNextPage.Visible = True
            'btnNextPage2.Visible = True
            'btnPreviousPage.Visible = True
            'btnPreviousPage2.Visible = True
            'txtPageNumber.Visible = True
            'txtPageNumber2.Visible = True
            'PreviousLinkButton.Visible = True
            'PreviousLinkButton2.Visible = True
            'NextLinkButton.Visible = True
            'NextLinkButton2.Visible = True
            'lblof1.Visible = True
            'lblof2.Visible = True
            'lblPage1.Visible = True
            'lblPage2.Visible = True
            'trBackToTop.Visible = True
            tblPagination1.Visible = True
            tblPagination2.Visible = True
            tblBackToTop.Visible = True

            errorMessageLabel.Text = ""
            dgTechBulletinResults.Visible = True
        End Sub

        Private Function ValidateDates() As Boolean
            Try
                Dim date1 = txtDateFrom.Value
                Dim date2 = txtDateTo.Value
                '-- From cannot be after To
                If Date.Compare(date1, date2) > 0 Then Return False

                '-- To cannot be after today
                If Date.Compare(date2, Date.Today()) > 0 Then Return False

                '-- From cannot be before 01/01/1967
                If Date.Compare(date1, Convert.ToDateTime("01/01/1967")) < 0 Then Return False

                '-- dates are valid
                Return True
            Catch ex As Exception
                Return False
            End Try
        End Function

#End Region

#Region "File Info"
        Private Function GetFileSize(ByVal path As String) As String
            Dim fileSize As String = ""

            Try
                If File.Exists(path) Then
                    Dim thisFile As New FileInfo(path.ToString())

                    If thisFile.Length() >= MegaBytes Then
                        fileSize = (String.Format("{0:N}", (thisFile.Length / MegaBytes))).ToString() + " MB"
                    ElseIf thisFile.Length() > KiloBytes Then
                        fileSize = (String.Format("{0:N}", (thisFile.Length / KiloBytes))).ToString() + " KB"
                    Else
                        fileSize = thisFile.Length.ToString() + " Bytes"
                    End If
                End If

            Catch ex As Exception
                fileSize = "N/A"
            End Try

            Return fileSize.ToString()
        End Function

        Private Sub SetFileDownloadPaths()
            Try
                downloadURL = ConfigurationData.Environment.Download.Url
                downloadPath = ConfigurationData.Environment.Download.Physical
                conTechBulletinPath = ConfigurationData.Environment.Download.TechBulletin
            Catch ex As Exception
                errorMessageLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Function BuildReleaseNotesData(ByRef fileName As String, ByRef description As String) As String
            Dim returnValue As String = String.Empty
            If File.Exists(downloadPath + "/" + conTechBulletinPath + "/" + fileName) Then
                returnValue = "<a href=""" + downloadURL + "/" + conTechBulletinPath + "/" + fileName + """ target=""_blank"" id=""bodyClass"">"
                If Not description Is Nothing Then
                    returnValue = returnValue + description + "</a>"
                Else
                    returnValue = returnValue + "<img name=""releaseNotes"" src=""images/sp_int_releaseNotes_btn.gif"" border=""0"" height=""13"" width=""18"" alt=""Release Notes""/></a>"
                End If
            Else
                ' ref to file unavailable online page

            End If
            Return returnValue
        End Function

        Private Sub FormFieldInitialization()
            Page.ID = Request.Url.AbsolutePath.ToString()
            txtPageNumber.Attributes("onkeydown") = "SetTheFocusButton(event, 'btnGoToPage')"
            txtPageNumber2.Attributes("onkeydown") = "SetTheFocusButton(event, 'btnGoToPage2')"
            errorMessageLabel.Text = ""
        End Sub

#End Region

    End Class

End Namespace
