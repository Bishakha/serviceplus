Imports System
Imports System.IO
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Drawing
Imports System.Web
Imports System.Web.SessionState
Imports System.Web.UI
Imports System.Web.UI.Page
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports System.Text
Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Process
Imports Sony.US.ServicesPLUS.Data
Imports Sony.US.siam
Imports Sony.US.SIAMUtilities
Imports ServicesPlusException


Namespace ServicePLUSWebApp

    Partial Class Sony_Parts_CatalogGroup
        Inherits SSL

        Const conPartsCatalogDetailPath As String = "Sony-Parts-Catalog-Details.aspx?groupid="
        Const conPartsCatalogGroupPath As String = "Sony_Parts_CatalogGroup.aspx?pageno="

        Private rangePerPage As Integer
        Private partsPerRange As Integer
        'Dim objUtilties As Utilities = New Utilities
        'Dim objGlobalData As New GlobalData

        Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load

            'If Not Session.Item("GlobalData") Is Nothing Then
            '    objGlobalData = Session.Item("GlobalData")
            'End If

            If Not Page.IsPostBack Then
                buildPartsCatalogTable()
            End If
        End Sub



        Private Sub buildPartsCatalogTable()
            Dim td As New TableCell
            Dim tr As New TableRow
            Dim cm As CatalogManager = New CatalogManager

            cm.getRanges(rangePerPage, partsPerRange)

            Dim lnkBPrev As HyperLink = CType(Me.btmPrevious, HyperLink)
            Dim lnkTPrev As HyperLink = CType(Me.topPrevious, HyperLink)

            Dim lnkBNext As HyperLink = CType(Me.btmNext, HyperLink)
            Dim lnkTNext As HyperLink = CType(Me.topNext, HyperLink)

            Dim lnkBPrevImg As HyperLink = CType(Me.btmPreviousImg, HyperLink)
            Dim lnkTPrevImg As HyperLink = CType(Me.topPreviousImg, HyperLink)

            Dim lnkBNextImg As HyperLink = CType(Me.btmNextImg, HyperLink)
            Dim lnkTNextImg As HyperLink = CType(Me.topNextImg, HyperLink)

            Dim currPage As Integer = 1

            Dim prevPage As Integer = 1
            Dim nextPage As Integer = 1

            Dim prevPageRange As Long = 1
            Dim nextPageRange As Long = 1

            Try
                If Not Request.QueryString("pageno") Is Nothing Then
                    currPage = CType(Request.QueryString("pageno"), Integer)
                End If
            Catch ex As Exception
                currPage = 1
            End Try

            Dim maxGroup As Long = cm.GetMaxGroupID()

            prevPageRange = (currPage - 1) * rangePerPage * partsPerRange + 1
            nextPageRange = currPage * rangePerPage * partsPerRange + 1

            If prevPageRange > 1 Then
                lnkBPrev.Visible = True
                lnkTPrev.Visible = True
                lnkBPrevImg.Visible = True
                lnkTPrevImg.Visible = True
                prevPage = currPage - 1
            Else
                lnkBPrev.Visible = False
                lnkTPrev.Visible = False
                lnkBPrevImg.Visible = False
                lnkTPrevImg.Visible = False
                prevPage = 1
            End If

            If nextPageRange < maxGroup Then
                lnkBNext.Visible = True
                lnkTNext.Visible = True
                lnkBNextImg.Visible = True
                lnkTNextImg.Visible = True
                nextPage = currPage + 1
            Else
                lnkBNext.Visible = False
                lnkTNext.Visible = False
                lnkBNextImg.Visible = False
                lnkTNextImg.Visible = False
                nextPage = 1
            End If


            'Modified for fixing Bug# 435
            'lnkBPrev.NavigateUrl = conPartsCatalogGroupPath + prevPage.ToString()
            lnkBPrev.NavigateUrl = "page" + prevPage.ToString() + ".aspx"
            'lnkTPrev.NavigateUrl = conPartsCatalogGroupPath + prevPage.ToString()
            lnkTPrev.NavigateUrl = "page" + prevPage.ToString() + ".aspx"

            'lnkBNext.NavigateUrl = conPartsCatalogGroupPath + nextPage.ToString()
            lnkBNext.NavigateUrl = "page" + nextPage.ToString() + ".aspx"
            'lnkTNext.NavigateUrl = conPartsCatalogGroupPath + nextPage.ToString()
            lnkTNext.NavigateUrl = "page" + nextPage.ToString() + ".aspx"

            'lnkBPrevImg.NavigateUrl = conPartsCatalogGroupPath + prevPage.ToString()
            lnkBPrevImg.NavigateUrl = "page" + prevPage.ToString() + ".aspx"
            'lnkTPrevImg.NavigateUrl = conPartsCatalogGroupPath + prevPage.ToString()
            lnkTPrevImg.NavigateUrl = "page" + prevPage.ToString() + ".aspx"
            'lnkBNextImg.NavigateUrl = conPartsCatalogGroupPath + nextPage.ToString()
            lnkBNextImg.NavigateUrl = "page" + nextPage.ToString() + ".aspx"
            'lnkTNextImg.NavigateUrl = conPartsCatalogGroupPath + nextPage.ToString()
            lnkTNextImg.NavigateUrl = "page" + nextPage.ToString() + ".aspx"

            lnkBPrevImg.Style.Add("cursor", "hand")
            lnkTPrevImg.Style.Add("cursor", "hand")
            lnkBNextImg.Style.Add("cursor", "hand")
            lnkTNextImg.Style.Add("cursor", "hand")

            Dim partsGroups As PartsCatalogGroup()
            'partsGroups = cm.GetPartsGroups()
            partsGroups = cm.GetPartsGroupByPageNumber(currPage)

            Try
                Me.GridPartsGroup.DataSource = partsGroups
                Me.GridPartsGroup.DataBind()
            Catch ex As Exception
                SearchErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try

        End Sub

        Protected Sub GridPartsGroup_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridPartsGroup.RowDataBound
            Try
                If e.Row.RowType = DataControlRowType.DataRow Then
                    Dim rec As PartsCatalogGroup
                    rec = e.Row.DataItem
                    Dim lnkStartRange As HyperLink = CType(e.Row.Cells(1).Controls(0), HyperLink)
                    ''Dim lnkEndRange As HyperLink = CType(e.Row.Cells(2).Controls(0), HyperLink)
                    lnkStartRange.Style.Add("cursor", "hand")
                    'lnkEndRange.Style.Add("cursor", "hand")
                    lnkStartRange.Style.Add("text-decoration", "underline")
                    'lnkEndRange.Style.Add("text-decoration", "underline")

                    If Not rec.GroupID Is Nothing Then
                        '-->lnkStartRange.NavigateUrl = conPartsCatalogDetailPath + rec.GroupID.ToString()
                        lnkStartRange.NavigateUrl = "groupid" + rec.GroupID.ToString() + ".aspx"
                        'lnkEndRange.NavigateUrl = conPartsCatalogDetailPath + rec.GroupID.ToString()
                    Else
                    End If

                End If
            Catch ex As Exception
                SearchErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try

        End Sub

        Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
            Dim catm As New CatalogManager
            Dim parts() As Sony.US.ServicesPLUS.Core.Part
            Dim kits() As Sony.US.ServicesPLUS.Core.Kit
            Dim emptyString As String = ""

            Session.Remove("kits")

            Try
                If PartNumber.Text.Trim() = "" Then
                    SearchErrorLabel.Text = "Please enter Part Number to search"
                    Return
                End If

                '-- reusing the same quick search of parts page, so have to pass empty string on the model and description parameters
                Dim sMaximumvalue As String = String.Empty
                parts = catm.QuickSearch(emptyString, PartNumber.Text, emptyString, sMaximumvalue, HttpContextManager.GlobalData)
                kits = catm.KitSearch(emptyString, PartNumber.Text, emptyString) 'also search for kit now

                If parts.Length = 0 And Not String.IsNullOrEmpty(sMaximumvalue) Then
                    SearchErrorLabel.Text = sMaximumvalue
                    Return
                ElseIf parts.Length = 0 And kits.Length = 0 Then
                    SearchErrorLabel.Text = ("No matches found.")
                    Return
                Else
                    Session("partList") = parts
                    Session("kits") = kits
                    Response.Redirect("PartsPlusResults.aspx?stype=parts")
                End If
            Catch thrEx As Threading.ThreadAbortException
                'do nothing, expected for response.redirect
            Catch ex As Exception
                SearchErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try

        End Sub
    End Class
End Namespace

