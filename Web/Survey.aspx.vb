Imports System
Imports System.IO
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Drawing
Imports System.Web
Imports System.Web.SessionState
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Process
Imports Sony.US.ServicesPLUS.Data
Imports Sony.US.siam
Imports System.Text
Imports ServicesPlusException
Imports Sony.US.SIAMUtilities


Namespace ServicePLUSWebApp


    Partial Class Survey
        Inherits SSL
        'Dim objUtilties As Utilities = New Utilities

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub


        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            isProtectedPage(True)
            InitializeComponent()
            FormErrorsLabel.ForeColor = Color.Red
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            processPage(sender, e)
        End Sub

        Private Sub processPage(ByVal sender As System.Object, ByVal e As System.EventArgs)
            Try
                Dim sm As New SurveyManager
                Dim survey As Sony.US.ServicesPLUS.Core.Survey

                If Not Request.QueryString("survey") Is Nothing Then
                    Dim surveyId As String = Request.QueryString("survey")

                    survey = sm.GetSurvey(surveyId)

                    If Not survey Is Nothing Then
                        '-- clear error message
                        FormErrorsLabel.Text = ""
                        If Not IsPostBack Then
                            buildSurveyTable(survey)
                        Else
                            If Not ViewState.Item("NumberOfQuestions") Is Nothing Then
                                addAnswersToSurvey(ViewState.Item("NumberOfQuestions"), survey)
                            End If
                        End If
                    End If
                End If
            Catch ex As Exception
                FormErrorsLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try



        End Sub

        Private Sub buildSurveyTable(ByVal survey As Sony.US.ServicesPLUS.Core.Survey)
            Try
                Dim questionNumber As Integer = 1
                displayTopSurvey()
                For Each q As Question In survey.Questions
                    AddToTable(q.Text.ToString(), q.QuestionID, q.AnswerType)
                    questionNumber = questionNumber + 1
                Next

                ViewState.Add("NumberOfQuestions", questionNumber)
            Catch ex As Exception
                FormErrorsLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Sub addAnswersToSurvey(ByVal numberOfQuestions As Integer, ByVal survey As Sony.US.ServicesPLUS.Core.Survey)
            Try
                If Not Session.Item("customer") Is Nothing Then
                    Dim missedQuestion As New ArrayList
                    missedQuestion = ValidateForm(numberOfQuestions, survey)

                    If missedQuestion.Count = 0 Then
                        Dim sm As New SurveyManager
                        Dim Responses() As Response
                        Dim customer As Customer = Session.Item("customer")

                        Responses = sm.GetResponses(survey, customer)

                        Dim questionNumber As Integer = 1
                        Dim answer As New Response
                        Dim responseExist As Boolean = False
                        Dim x As Integer = 0

                        '-- i need to implement the functionality this way - dwd
                        'For Each thisQuestion As Question In survey.Questions

                        'Next

                        While x < survey.Questions.Length

                            answer.Question = survey.Questions.GetValue(x)

                            For Each thisResponse As Response In Responses
                                If thisResponse.Question.QuestionID = answer.Question.QuestionID Then
                                    responseExist = True
                                    Exit For
                                End If
                            Next

                            answer.Survey = survey
                            answer.Respondent = customer

                            If answer.Question.AnswerType = Question.AnswerDisplayType.RadioButtons Then
                                answer.RadioButtonResponse = CType(Request.Form(questionNumber.ToString()), Integer)
                            Else
                                If Not Request.Form(answer.Question.QuestionID.ToString()) Is Nothing Then
                                    If Request.Form(answer.Question.QuestionID.ToString()).Length > 0 And Request.Form(answer.Question.QuestionID.ToString()).Length < 255 Then
                                        answer.TextResponse = Request.Form(answer.Question.QuestionID.ToString()).Replace("'", "''")
                                    Else
                                        answer.TextResponse = " "
                                    End If
                                Else
                                    answer.TextResponse = " "
                                End If
                            End If


                            If responseExist Then
                                sm.UpdateSurveyResponse(answer)
                            Else
                                sm.AddSurveyResponse(answer)
                            End If

                            questionNumber = questionNumber + 1
                            answer = New Response
                            x = x + 1
                        End While

                        displayCompletionMessage()
                    Else
                        If Not FormErrorsLabel.Text.StartsWith("You can only enter") Then
                            FormErrorsLabel.Text = "Please answer all questions."
                        End If
                        buildSurveyTable(survey)
                    End If
                Else
                    'Pass language to siteminder
                    Dim LanguageSM As String = "en_US"
                    'If (Session("SelectedLang") IsNot Nothing) Then
                    '    strLanguage = Session("SelectedLang").ToString().Substring(0, 2).ToUpper()
                    'End 
                    'Dim objGlobalData As New GlobalData
                    'If Not Session.Item("GlobalData") Is Nothing Then
                    '    objGlobalData = Session.Item("GlobalData")
                    'End If
                    If HttpContextManager.GlobalData.SalesOrganization = ConfigurationData.GeneralSettings.GlobalData.SalesOrganization.CA Then
                        If HttpContextManager.GlobalData.Language = ConfigurationData.GeneralSettings.GlobalData.Language.US Then
                            LanguageSM = ConfigurationData.GeneralSettings.GlobalData.Language.CA_en.Replace("-", "_")
                        Else
                            LanguageSM = ConfigurationData.GeneralSettings.GlobalData.Language.CA.Replace("-", "_")
                        End If
                    Else
                        LanguageSM = ConfigurationData.GeneralSettings.GlobalData.Language.US.Replace("-", "_")
                    End If

                    FormErrorsLabel.Text = "You must be logged in to take the survey, please <a href=""#"" onclick=""top.opener.window.location='SignIn.aspx?Lang=" + LanguageSM + "&'; window.close();"" class=""redasterick""> Login </a> or <a href=""#"" onclick=""top.opener.window.location='register.aspx'; window.close();"" class=""redasterick"" Target=""_blank"">register</a>.""                    "
                End If
            Catch ex As Exception
                FormErrorsLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Function ValidateForm(ByVal numberOfQuestion As Integer, ByVal survey As Sony.US.ServicesPLUS.Core.Survey) As ArrayList
            Dim missedQuestion As New ArrayList

            Try
                Dim x As Integer = 1

                For Each q As Question In survey.Questions
                    If Request.Form(q.QuestionID.ToString()) Is Nothing And q.AnswerType = Question.AnswerDisplayType.RadioButtons Then
                        missedQuestion.Add(x.ToString())
                    ElseIf Not Request.Form(q.QuestionID.ToString()) Is Nothing And q.AnswerType = Question.AnswerDisplayType.Textbox Then
                        If Request.Form(q.QuestionID.ToString()).Length > 255 Then
                            FormErrorsLabel.Text = "You can only enter 255 characters in the comments box. You currently have " + Request.Form(q.QuestionID.ToString()).Length.ToString() + "."
                            missedQuestion.Add(x.ToString())
                        End If
                    End If
                Next
            Catch ex As Exception
                FormErrorsLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try

            Return missedQuestion
        End Function

        Private Sub AddToTable(ByVal thisQuestion As String, ByVal questionNumber As Integer, ByVal controlType As Question.AnswerDisplayType)
            Dim td As New TableCell
            Dim tr As New TableRow
            Dim checkedValue As String = ""

            If Not Request.Form(questionNumber.ToString()) Is Nothing Then
                checkedValue = Request.Form(questionNumber.ToString())
            End If


            td.Controls.Add(New LiteralControl("<table width=""667"" border=""0"" cellspacing=""0"" cellpadding=""2"">" + vbCrLf))
            td.Controls.Add(New LiteralControl("<tr>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td><span class=""bodyCopyBold"">" + vbCrLf))
            td.Controls.Add(New LiteralControl(questionNumber.ToString() + ")"))
            td.Controls.Add(New LiteralControl("</span> <span class=""bodyCopy"">" + vbCrLf))
            td.Controls.Add(New LiteralControl(thisQuestion))
            td.Controls.Add(New LiteralControl("</span></td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("</tr>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<tr>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td>" + vbCrLf))

            If controlType = Question.AnswerDisplayType.RadioButtons Then

                td.Controls.Add(New LiteralControl("<table width=""663"" border=""0"" cellspacing=""0"" cellpadding=""0"">" + vbCrLf))
                td.Controls.Add(New LiteralControl("<tr>" + vbCrLf))
                td.Controls.Add(New LiteralControl("<td class=""tableHeader"">Not Satisfied</td>" + vbCrLf))
                td.Controls.Add(New LiteralControl("<td class=""tableHeader"" align=""right"">Very Satisfied</td>" + vbCrLf))
                td.Controls.Add(New LiteralControl("<td class=""tableHeader"" align=""right"">Does Not Apply</td>" + vbCrLf))
                td.Controls.Add(New LiteralControl("</tr>" + vbCrLf))
                td.Controls.Add(New LiteralControl("</table>" + vbCrLf))
                td.Controls.Add(New LiteralControl("</td>" + vbCrLf))
                td.Controls.Add(New LiteralControl("</tr>" + vbCrLf))
                td.Controls.Add(New LiteralControl("<tr>" + vbCrLf))
                td.Controls.Add(New LiteralControl("<td>" + vbCrLf))


                td.Controls.Add(New LiteralControl("<table width=""663"" border=""0"" cellspacing=""0"" cellpadding=""0"">" + vbCrLf))
                td.Controls.Add(New LiteralControl("<tr>" + vbCrLf))

                For x As Int16 = 1 To 8
                    td.Controls.Add(New LiteralControl("<td class=""tableHeader"" width=""5"">" + x.ToString() + "</td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td width=""15"" align=""left"">" + vbCrLf))
                    If x.ToString() = checkedValue.ToString() Then
                        td.Controls.Add(New LiteralControl("<input type=""radio"" name=""" + questionNumber.ToString() + """ value=""" + x.ToString + """ checked>" + vbCrLf))
                    Else
                        td.Controls.Add(New LiteralControl("<input type=""radio"" name=""" + questionNumber.ToString() + """ value=""" + x.ToString + """>" + vbCrLf))
                    End If
                    td.Controls.Add(New LiteralControl("</td>" + vbCrLf))
                Next

                td.Controls.Add(New LiteralControl("</tr>" + vbCrLf))
                td.Controls.Add(New LiteralControl("</table>" + vbCrLf))
            Else
                Dim textArea As String = ""
                If Not Request.Form.Item(questionNumber.ToString()) Is Nothing Then textArea = Request.Form.Item(questionNumber.ToString())
                td.Controls.Add(New LiteralControl("<table width=""663"" border=""0"" cellspacing=""0"" cellpadding=""0"">" + vbCrLf))
                td.Controls.Add(New LiteralControl("<tr>" + vbCrLf))
                td.Controls.Add(New LiteralControl("<td class=""tableHeader"" >"))
                td.Controls.Add(New LiteralControl("<textarea name=""" + questionNumber.ToString() + """ cols=""50"" rows=""6"" onkeyup=""limitTextArea(this,'254')"">" + textArea + "</textarea>"))
                td.Controls.Add(New LiteralControl("</td>" + vbCrLf))
                td.Controls.Add(New LiteralControl("</tr>" + vbCrLf))
                td.Controls.Add(New LiteralControl("</table>" + vbCrLf))
            End If
            td.Controls.Add(New LiteralControl("</td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("</tr>" + vbCrLf))


            td.Controls.Add(New LiteralControl("<tr>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""#f2f5f8""><img src=""images/spacer.gif"" width=""3"" height=""5""></td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td align=""right"" class=""bodyCopy"" colspan=""2"" bgcolor=""#f2f5f8""><img src=""images/spacer.gif"" width=""3"" height=""5""></td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("</tr>" + vbCrLf))

            td.Controls.Add(New LiteralControl("</table>" + vbCrLf))

            tr.Controls.Add(td)
            SurveyTable.Controls.Add(tr)
        End Sub

        Private Sub displayCompletionMessage()
            Dim td As New TableCell
            Dim tr As New TableRow

            'bgcolor="#d5dee9" id="surveyTR"
            surveyTR.Attributes.Item("bgcolor") = "#ffffff"


            td.CssClass = "tableData"
            td.Controls.Add(New LiteralControl("Thank you for your time in completing this survey. Our number one goal is meeting and exceeding your expectations.<BR><BR>Best regards,<BR>The ServicesPLUS Team"))
            td.Controls.Add(New LiteralControl("<br><br><br><a href=""default.aspx"" class=""tableData"">ServicePLUS Home</a>"))
            tr.Controls.Add(td)
            SurveyTable.Controls.Add(tr)
            '-- hide the submit button
            SubmitFormButton.Visible = False
        End Sub

        Private Sub displayTopSurvey()
            Dim td As New TableCell
            Dim tr As New TableRow

            td.Controls.Add(New LiteralControl("<tr>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td width=""3"" bgColor=""#d5dee9""><IMG height=""15"" src=""images/spacer.gif"" width=""3""></td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td class=""bodyCopy"" bgColor=""#d5dee9"" colSpan=""2"">" + vbCrLf))
            td.Controls.Add(New LiteralControl("<IMG height=""5"" src=""images/spacer.gif"" width=""20""><br>Please complete the following survey. Please answer each question using the scale of 1-8.<br><span class=""bodyCopyBold"">1</span> meaning Not Satisfied," + vbCrLf))
            td.Controls.Add(New LiteralControl("<span class=""bodyCopyBold"">7</span> meaning Very Satisfied and" + vbCrLf))
            td.Controls.Add(New LiteralControl("<span class=""bodyCopyBold"">8</span> for Does Not Apply.<br>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<IMG height=""5"" src=""images/spacer.gif"" width=""20""></td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("</tr>" + vbCrLf))

            tr.Controls.Add(td)
            topSurveryText.Controls.Add(tr)


        End Sub

    End Class

End Namespace
