<%@ Reference Page="~/SSL.aspx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="SonyFooter" Src="SonyFooter.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="ServicePlusNav" Src="ServicePlusNav.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="PersonalMessage" Src="PersonalMessage.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="TopSonyHeader" Src="TopSonyHeader.ascx" %>
<%@ OutputCache Duration="15" VaryByParam="None" %>

<%@ Page Language="vb" AutoEventWireup="false" Inherits="ServicePLUSWebApp.CustomerOrders"
    EnableViewStateMac="true" CodeFile="CustomerOrders.aspx.vb" %>

<html lang="<%=PageLanguage %>">
<head>
    <title><%=Resources.Resource.ttl_MyOrders%></title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link rel="stylesheet" href="includes/ServicesPLUS_style.css" type="text/css">

    <script language="javascript" src="includes/ServicesPLUS.js" type="text/javascript"></script>
    <script type="text/javascript" src="includes/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="includes/svcplus_globalization.js"></script>

</head>
<body style="color: #000000; background: #5d7180; margin: 0px;">
    <form name="form1" runat="server">
        <center>
            <asp:ScriptManager ID="ScriptManager1" runat="server" />
            <table width="760" border="0" role="presentation">
                <tr>
                    <td width="25" style="background: url('images/sp_left_bkgd.gif');">
                        <img src="images/spacer.gif" width="25" height="25" alt="">
                    </td>
                    <td width="710" bgcolor="#ffffff">
                        <table width="710" border="0" role="presentation">
                            <tr>
                                <td>
                                    <ServicePLUSWebApp:TopSonyHeader ID="TopSonyHeader" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <nav role="navigation"><ServicePLUSWebApp:ServicePlusNav ID="SonyNav" runat="server" /></nav>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table style="width: 710px; border: none;" role="presentation">
                                        <tr style="height: 57px;">
                                            <td style="width: 464px; vertical-align: middle; background: #363d45 url('images/sp_int_header_top_ServicesPLUS_onepix.gif');">
                                                <h1 class="headerText" style="padding-right: 20px; text-align: right;">ServicesPLUS</h1>
                                            </td>
                                            <td style="background: #363d45; vertical-align: middle;">
                                                <ServicePLUSWebApp:PersonalMessage ID="PersonalMessageDisplay" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="background: #f2f5f8;">
                                                <h2 class="headerTitle" style="padding-right: 20px; text-align: right;"><%=Resources.Resource.el_MyOrder%></h2>
                                            </td>
                                            <td style="background: #99a8b5;">
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr style="height: 9px;">
                                            <td style="background: #f2f5f8;">
                                                <img height="9" src="images/sp_int_header_btm_left_onepix.gif" width="464" alt="">
                                            </td>
                                            <td style="background: #99a8b5;">
                                                <img height="9" src="images/sp_int_header_btm_right.gif" width="246" alt="">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="710" border="0" role="presentation">
                                        <tr>
                                            <td width="20" height="20">
                                                <img src="images/spacer.gif" width="20" height="20" alt="">
                                            </td>
                                            <td width="670" height="20">
                                                <asp:Label ID="errorMessageLabel" runat="server" CssClass="tableData" ForeColor="Red"></asp:Label>
                                            </td>
                                            <td width="20" height="20">
                                                <img src="images/spacer.gif" width="20" height="20" alt="">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="20">
                                                <img src="images/spacer.gif" width="20" height="20" alt="">
                                            </td>
                                            <td width="670">
                                                <table width="670" border="0" role="presentation">
                                                    <tr>
                                                        <td>
                                                            <table align="center" border="0" width="670" role="presentation">
                                                                <tr>
                                                                    <td bgcolor="#ffffff" align="left" style="height: 25px">
                                                                        <asp:UpdateProgress ID="UpdateProgress1" runat="server">
                                                                            <ProgressTemplate>
                                                                                <span class="bodyCopy"><%=Resources.Resource.prg_PleaseWait%> </span>
                                                                                <img src="images/progbar.gif" width="100" alt="Please Wait..." />
                                                                            </ProgressTemplate>
                                                                        </asp:UpdateProgress>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td width="670" align="center" valign="top">
                                                                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                                                            <ContentTemplate>
                                                                                <asp:DataGrid ID="dgOrders" runat="server" AutoGenerateColumns="false" CellPadding="4"
                                                                                    ForeColor="#333333" GridLines="None" Width="100%" AllowPaging="true" AllowSorting="true"
                                                                                    PageSize="8" UseAccessibleHeader="true">
                                                                                    <PagerStyle BackColor="#D5DEE9" CssClass="tableHeader" HorizontalAlign="Left" Mode="NumericPages" />
                                                                                    <HeaderStyle BackColor="#D5DEE9" CssClass="tableHeader" Font-Bold="True" />
                                                                                    <ItemStyle BackColor="#EFF3FB" CssClass="bodyCopy" HorizontalAlign="Left" VerticalAlign="Middle" />
                                                                                    <AlternatingItemStyle BackColor="White" CssClass="bodyCopy" HorizontalAlign="Left" VerticalAlign="Middle" />
                                                                                    <Columns>
                                                                                        <asp:BoundColumn DataField="conf_ordno" SortExpression="conf_ordno" HeaderText="<%$ Resources:Resource, el_gdr_OrderNumber%>" HeaderStyle-Width="15%" />
                                                                                        <asp:BoundColumn DataField="orderdate" SortExpression="orderdate" HeaderText="<%$ Resources:Resource, el_gdr_OrderDate%>" HeaderStyle-Width="20%" />
                                                                                        <asp:BoundColumn DataField="ordertype" HeaderText="<%$ Resources:Resource, el_gdr_OrderType%>" HeaderStyle-Width="17%" />
                                                                                        <asp:BoundColumn DataField="ponumber" HeaderText="<%$ Resources:Resource, repair_svcacc_pono_msg%>" HeaderStyle-Width="18%" />
                                                                                        <asp:BoundColumn DataField="PayerNumber" HeaderText="<%$ Resources:Resource, el_gdr_PayerDetails%>" HeaderStyle-Width="20%" />
                                                                                        <asp:TemplateColumn HeaderText="<%$ Resources:Resource, el_gdr_ViewDetails%>">
                                                                                            <ItemTemplate>
                                                                                                <a class="bodyCopy" aria-label="Go to the details page for Order number <%#DataBinder.Eval(Container, "DataItem.ordernumber") %>"
                                                                                                    href="MyOrderDetails.aspx?order=<%#DataBinder.Eval(Container, "DataItem.ordernumber") %>,<%#DataBinder.Eval(Container, "DataItem.sapordernumber") %>,<%#DataBinder.Eval(Container,"DataItem.conf_ordno") %>">View</a>
                                                                                            </ItemTemplate>
                                                                                            <HeaderStyle Width="10%" HorizontalAlign="Center" />
                                                                                            <ItemStyle HorizontalAlign="Center" />
                                                                                        </asp:TemplateColumn>
                                                                                    </Columns>
                                                                                </asp:DataGrid>
                                                                            </ContentTemplate>
                                                                        </asp:UpdatePanel>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <img src="images/spacer.gif" width="670" height="30" alt="">
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td width="20">
                                                <img src="images/spacer.gif" width="20" height="20" alt="">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <footer><ServicePLUSWebApp:SonyFooter ID="SonyFooter1" runat="server" /></footer>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="25" style="background: url('images/sp_right_bkgd.gif')">
                        <img src="images/spacer.gif" width="25" height="20" alt="">
                    </td>
                </tr>
            </table>
        </center>
    </form>
</body>
</html>
