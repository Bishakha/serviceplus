Namespace ServicePLUSWebApp

    Partial Class ProductRegistrationWelcome
        Inherits SSL

        Public isAmerica As Boolean
        Public isLoggedIn As Boolean
        'Public isReseller As Boolean = False

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub
#End Region
        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As EventArgs) Handles MyBase.Load
            isAmerica = HttpContextManager.GlobalData.IsAmerica
            isLoggedIn = HttpContextManager.Customer IsNot Nothing

            If Not Page.IsPostBack Then
                ' 2018-09-21 ASleight - Removing Login requirement for Product Registration. Link now redirects to a Pro Site page.
                'If Not isLoggedIn Then
                '    'btnWarranty.PostBackUrl = "SignIn-Register.aspx?post=prc"
                '    btnProduct.PostBackUrl = "SignIn-Register.aspx?post=prr"
                'End If
                ' 2018-05-31 ASleight - Additional check for Reseller for Service Agreements. 
                ' 2018-07-10 ASleight - Reseller list from data feed may not be valid. Leaving code here in case we re-implement it later.
                'Try
                '    If isLoggedIn AndAlso HttpContextManager.Customer.UserType = "A" Then
                '        Dim regMan As New OnlineProductRegistrationManager()
                '        isReseller = regMan.IsReseller(HttpContextManager.Customer.CustomerID)
                '    End If
                'Catch ex As Exception
                '    lblError.Text = Utilities.WrapExceptionforUI(ex)
                'End Try
            End If
        End Sub

        '''' <summary>
        '''' ASleight - Since we don't check for logged in status, there's no need for all of this.
        '''' Changed the button to a direct link.
        '''' </summary>
        'Protected Sub btnReseller_Click(sender As Object, e As ImageClickEventArgs) Handles btnReseller.Click
        '    lblError.Text = ""
        '    If isLoggedIn Then
        '        If HttpContextManager.Customer.UserType = "A" Then
        '            Response.Redirect("product-registration-reseller.aspx")
        '        Else
        '            lblError.Text = "You must be an approved Account Holder to register a Reseller agreement."
        '        End If
        '    Else
        '        Response.Redirect("SignIn-Register.aspx?post=prc")
        '    End If
        'End Sub

        '''' <summary>
        '''' ASleight - Changed the page so if the user isn't logged in, the "Log In or Register" challenge
        '''' is shown instead of the button. Button is now a direct link.
        '''' </summary>
        'Protected Sub btnProduct_Click(sender As Object, e As ImageClickEventArgs) Handles btnProduct.Click
        '    If isLoggedIn Then
        '        Response.Redirect("Online-Product-Registration.aspx")
        '    Else
        '        Response.Redirect("SignIn-Register.aspx?post=prc")
        '    End If
        'End Sub

        'Protected Sub btnSignUp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSignUp.Click
        '    Session.Add("RegisterReturnPage", "Online-Product-Registration.aspx")
        '    'Response.Redirect("Register.aspx")
        '    Response.Redirect("Register-Account-Tax-Exempt.aspx")
        'End Sub
    End Class
End Namespace
