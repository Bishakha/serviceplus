<%@ Reference Page="~/SSL.aspx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="SonyFooter" Src="SonyFooter.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="PersonalMessage" Src="PersonalMessage.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="ServicePlusNav" Src="ServicePlusNav.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="TopSonyHeader" Src="TopSonyHeader.ascx" %>

<%@ Page Language="vb" AutoEventWireup="false" Inherits="ServicePLUSWebApp.sony_Not_Authorized" CodeFile="sony-Not-Authorized.aspx.vb" CodeFileBaseClass="ServicePLUSWebApp.SSL" %>

<html lang="<%=PageLanguage %>">
<head>
    <title>Not Authorized</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta http-equiv="Pragma" content="no-cache">
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <meta content="Sony Software, Sony Professional Software, Sony Broadcast Software, Sony Business Software, Sony Technical Bulletins"
        name="keywords">
    <link href="includes/ServicesPLUS_style.css" type="text/css" rel="stylesheet">
    <script language="javascript" src="includes/ServicesPLUS.js" type="text/javascript"></script>
    <script type="text/javascript" src="includes/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="includes/svcplus_globalization.js"></script>
</head>
<body text="#000000" bgcolor="#5d7180" leftmargin="0" topmargin="0">
    <center>
        <form id="frmTechBulletinSearch" method="post" runat="server">
            <asp:ScriptManager ID="ScriptManager1" runat="server">
            </asp:ScriptManager>

            <table width="760" border="0" role="presentation">
                <tr>
                    <td width="25" background="images/sp_left_bkgd.gif">
                        <img height="25" src="images/spacer.gif" width="25"></td>
                    <td width="710" bgcolor="#ffffff">
                        <table width="710" border="0" role="presentation">
                            <tr>
                                <td>
                                    <ServicePLUSWebApp:TopSonyHeader ID="TopSonyHeader" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <nav><ServicePLUSWebApp:ServicePlusNav ID="ServicePlusNavDisplay" runat="server" /></nav>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="710" border="0" role="presentation">
                                        <tr>
                                            <td valign="top" align="right" width="464" background="images/sp_int_subhd_technical_bulletins.jpg"
                                                bgcolor="#363d45" height="82">
                                                <br />
                                                <h1 class="headerText">ServicesPLUS</h1>
                                            </td>
                                            <td valign="middle" bgcolor="#363d45">
                                                <ServicePLUSWebApp:PersonalMessage ID="PersonalMessageDisplay" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="466" bgcolor="#f2f5f8">
                                                <table width="464" border="0" role="presentation">
                                                    <tr>
                                                        <td width="20">
                                                        </td>
                                                        <td>
                                                            <img height="50" src="images/spacer.gif" width="25"></td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td bgcolor="#99a8b5" valign="bottom">
                                            </td>
                                        </tr>
                                        <tr height="9">
                                            <td width="466" bgcolor="#f2f5f8">
                                                <img height="9" src="images/sp_int_header_btm_left_onepix.gif" width="464"></td>
                                            <td bgcolor="#99a8b5">
                                                <img height="9" src="images/sp_int_header_btm_right.gif" width="246"></td>
                                        </tr>
                                    </table>
                                    <br />
                                    <table width="710" border="0" role="presentation">
                                        <tr>
                                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ID="errorMessageLabel" runat="server" CssClass="tableHeader" Width="641px" EnableViewState="False">You are not authorized to view this item.</asp:Label></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td width="100%">
                                    <%--"C:\BPC\servicesplus\ServicesPLUS2005\ServicesPLUSWebApp\pdfs\paging_data.pdf"--%>
                                </td>
                            </tr>
                            <tr>
                                <td style="height: 10px">
                                    <img height="50" src="images/spacer.gif" width="25"></td>
                            </tr>
                            <tr>
                                <td width="100%" align="left">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <footer><ServicePLUSWebApp:SonyFooter ID="SonyFooter1" runat="server" /></footer>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="25" background="images/sp_right_bkgd.gif">
                        <img height="5" src="images/spacer.gif" width="25"></td>
                </tr>
            </table>
        </form>

    </center>
</body>
</html>
