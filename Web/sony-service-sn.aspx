<%@ PreviousPageType VirtualPath="~/sony-repair.aspx" %>

<%@ Page Language="VB" AutoEventWireup="false" CodeFile="sony-service-sn.aspx.vb" Inherits="ServicePLUSWebApp.sony_service_sn" %>

<%@ Reference Page="~/SSL.aspx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="SonyFooter" Src="SonyFooter.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="TopSonyHeader" Src="TopSonyHeader.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="PersonalMessage" Src="PersonalMessage.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="ServicePlusNav" Src="ServicePlusNav.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="<%=PageLanguage %>">
<head>
    <title><%=Resources.Resource.header_serviceplus()%> - <%=Resources.Resource.repair_svcacc_hdr_msg()%></title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <meta content="Repair, Maintenance, Services, Sony Models, Sony Professional Models, Sony Broadcast Models, Sony Business Models, Sony Software, Sony Professional Software, Sony Broadcast Software, Sony Business Software"
        name="keywords">
    <link href="includes/ServicesPLUS_style.css" type="text/css" rel="stylesheet">

    <script type="text/javascript" src="includes/ServicesPLUS.js"></script>
    <script type="text/javascript" src="includes/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="includes/svcplus_globalization.js"></script>
    <script type="text/javascript">
        function checkServiceContract() {
            var radioObj = document.forms['Form1'].elements['group1']
            var radioLength = radioObj.length;
            var iSelect = 0;

            if (radioLength === undefined) {
                if (radioObj.checked)
                    return radioObj.value;
                else
                    return "";
            }
            for (var i = 0; i < radioLength; i++) {
                if (radioObj[i].checked) {
                    //return group1[i].value;
                    iSelect = radioObj[i].value;
                }
            }
            if (iSelect === 'c') {
                if (document.forms['Form1'].txtContractNo.value === "") {
                    //document.forms['Form1'].cvServiceContract.value = "Please enter service contract number.";
                    return false;
                }
                else {
                    return true;
                }
            }
        }
    </script>

</head>
<body style="color: #000000; background: #5d7180; margin: 0px;">
    <form id="Form1" method="post" runat="server" onsubmit="javascript:checkServiceContract();">
        <center>
            <table width="710" border="0" role="presentation">
                <tr>
                    <td width="25" style="background: url('images/sp_left_bkgd.gif')">
                        <img height="25" src="images/spacer.gif" width="25" alt="">
                    </td>
                    <td width="689" bgcolor="#ffffff">
                        <table width="710" border="0" role="presentation">
                            <tr>
                                <td width="582">
                                    <ServicePLUSWebApp:TopSonyHeader ID="TopSonyHeader" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td width="582" height="23">
                                    <nav><ServicePLUSWebApp:ServicePlusNav ID="SonyNav" runat="server" /></nav>
                                </td>
                            </tr>
                            <tr>
                                <td width="582">
                                    <table width="710" border="0" role="presentation">
                                        <tr>
                                            <td style="background: url('images/repair_lrg.gif') #363d45; height: 82px; width: 465px; text-align: right;">
                                                <br />
                                                <h1 class="headerText"><%=Resources.Resource.repair_snysvc_srl_msg_4 %> </h1>
                                            </td>
                                            <td valign="middle" width="238" bgcolor="#363d45">
                                                <ServicePLUSWebApp:PersonalMessage ID="PersonalMessageDisplay" runat="server" />
                                            </td>
                                        </tr>
                                        <tr valign="top">
                                            <td bgcolor="#f2f5f8" style="width: 465px">
                                                <h2 id="lblSubHdr" class="headerTitle" style="padding-right: 20px; text-align: right;" runat="server"></h2>
                                            </td>
                                            <td width="238" bgcolor="#99a8b5">
                                                <table align="left" width="80%" role="presentation">
                                                    <tr>
                                                        <td style="width: 3px">
                                                            &nbsp;
                                                        </td>
                                                        <td style="width: 163px">
                                                            <span class="promoCopyBold"><%=Resources.Resource.repair_pg_mdlst_msg_2()%></span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 3px">
                                                            &nbsp;
                                                        </td>
                                                        <td style="width: 163px; height: 31px">
                                                            <a href="<%=sURL%>">
                                                                <img alt="To change search model" border="0" src="<%=Resources.Resource.repair_svc_img_5()%>" /></a>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr height="9">
                                            <td bgcolor="#f2f5f8" style="width: 465px">
                                                <img height="9" src="images/sp_int_header_btm_left_onepix.gif" width="464" alt="">
                                            </td>
                                            <td width="238" bgcolor="#99a8b5">
                                                <img height="9" src="images/sp_int_header_btm_right.gif" width="246" alt="">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <table width="100%" border="0" role="presentation">
                                        <tr>
                                            <td style="width: 5px; height: 22px;">
                                                <img height="20" src="images/spacer.gif" width="20" alt="">
                                            </td>
                                            <td style="width: 796px; height: 22px;">
                                                <p class="promoCopyBold">
                                                    <%=Resources.Resource.repair_svcacc_lblvld_msg()%>
                                                </p>
                                            </td>
                                            <td style="width: 436px; height: 22px;">
                                                <img height="20" src="images/spacer.gif" width="20" alt="">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 5px">
                                                <img height="20" src="images/spacer.gif" width="20" alt="">
                                            </td>
                                            <td style="width: 796px">
                                                <asp:Label ID="ErrorValidation" runat="server" CssClass="redAsterick" Text="<%$ Resources:Resource,repair_svcacc_invld_msg%>"
                                                        Visible="False" />
                                            </td>
                                            <td align="center" style="width: 436px">
								                <img height="20" src="images/spacer.gif" width="20" alt="">
                                            </td>
                                        </tr>
                                        <tr valign="top">
                                            <td style="width: 5px; height: 107px;">
                                                <img height="20" src="images/spacer.gif" width="20" alt="">
                                            </td>
                                            <td style="height: 107px;" colspan="2">
                                                <table border="0" style="width: 93%" role="presentation">
                                                    <tr valign="top">
                                                        <td style="width: 100px; height: 26px;">
                                                            <p class="promoCopyBold">
                                                                <label id="lblSerialNo" for="txtSerialNumber" runat="server"><%=Resources.Resource.repair_snysvc_sno_msg%></label>
                                                            </p>
                                                        </td>
                                                        <td style="height: 26px; width: 376px;">
                                                            <SPS:SPSTextBox ID="txtSerialNumber" runat="server" CssClass="bodyCopy" EnableViewState="false" MaxLength="30"></SPS:SPSTextBox><span class="redAsterick">*</span>
                                                        </td>
                                                    </tr>
                                                    <tr valign="top">
                                                        <td style="width: 100px; height: 42px;">
                                                            <label for="txtAccessories" class="promoCopyBold"><%=Resources.Resource.repair_snysvc_snacc_msg%></label>
                                                        </td>
                                                        <td style="height: 42px; width: 376px;">
                                                            <SPS:SPSTextBox ID="txtAccessories" runat="server" EnableViewState="false" TextMode="MultiLine" CssClass="bodyCopy" Height="41px" Width="170px" MaxLength="250"></SPS:SPSTextBox>
                                                        </td>
                                                    </tr>
                                                    <tr valign="top">
                                                        <td style="width: 100px; height: 43px;">&nbsp;</td>
                                                        <td style="width: 376px; height: 43px"><span class="bodyCopy"><%=Resources.Resource.repair_snysvc_sninfo_msg()%> <%=sModelName%> <%=Resources.Resource.repair_snysvc_sninfo_msg_1()%> </span></td>
                                                    </tr>

                                                    <tr>
                                                        <td colspan="2">
                                                            <table border="0" width="100%" role="presentation">
                                                                <tr valign="top">
                                                                    <td colspan="2" style="height: 23px">
                                                                        <span style="font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: #666666">
                                                                            <input type="radio" name="group1" id="group1" value="c" <% if sWarranty = "C" Or sWarranty = "M" Then%>checked<%End If%>><label for="group1"><%=Resources.Resource.repair_snysvc_snsctrct_msg%></label>
                                                                            <SPS:SPSTextBox ID="txtContractNo" runat="server" Width="89px" CssClass="bodyCopy" EnableViewState="false" MaxLength="30" />
                                                                            <asp:Label ID="lblServiceErrMsg" runat="server" ForeColor="Red" Text="<%$ Resources:Resource,repair_snysvc_svcctno_msg%>"
                                                                                Visible="False" Width="206px"></asp:Label></span>
                                                                    </td>
                                                                </tr>
                                                                <tr valign="top">
                                                                    <td colspan="2" style="height: 23px">
                                                                        <span style="font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: #666666">
                                                                            <input type="radio" id="group2" name="group1" value="w" <% if sWarranty = "W" Then%>checked<%End If%>><label for="group2"><%=Resources.Resource.repair_snysvc_warnty_msg()%></label></span>
                                                                    </td>
                                                                </tr>

                                                                <tr>
                                                                    <td colspan="2" style="height: 23px">
                                                                        <span style="font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: #666666">
                                                                            <input type="radio" id="group4" name="group1" value="o" <% if sWarranty = "O" Then%>checked<%End If%>><label for="group4"><%=Resources.Resource.repair_snysvc_outwarnty_msg()%></label></span>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                            <asp:ImageButton ID="imgBtnNext" runat="server" ImageUrl="<%$ Resources:Resource,img_btnNext%>" AlternateText="<%$ Resources:Resource,el_Next %>" />&nbsp;
                                                            <a href="sony-repair.aspx">
                                                                <img src="<%=Resources.Resource.sna_svc_img_7()%>" alt="<%=Resources.Resource.el_Cancel %>" border="0" /></a>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                        </tr>
                                        <tr>
                                            <td style="width: 5px">&nbsp;</td>
                                            <td colspan="2">&nbsp;</td>
                                        </tr>

                                    </table>
                                </td>
                            </tr>

                            <tr>

                                <td width="582">
                                    <footer><ServicePLUSWebApp:SonyFooter ID="SonyFooter1" runat="server" /></footer>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="25" style="background: url('images/sp_right_bkgd.gif')">
                        <img src="images/spacer.gif" width="25" height="20" alt="">
                    </td>
                </tr>
            </table>
        </center>
    </form>
</body>
</html>
