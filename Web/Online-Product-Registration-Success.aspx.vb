﻿Imports Sony.US.ServicesPLUS.Process
Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.SIAMUtilities
Imports ServicesPlusException

Namespace ServicePLUSWebApp
    Partial Class Online_Product_Registration_Success
        Inherits SSL
        Public sModel As String = String.Empty
        Public sSucessMessage As String = String.Empty
        'Dim objUtilties As Utilities = New Utilities
        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                'Load Model Prefix
                If Session("customerpr") Is Nothing Then Response.Redirect("Online-Product-Registration.aspx", True)
                sModel = CType(Session("customerpr"), CustomerProductRegistration).MODEL_INTERESTED
                'Added by Prasad for Promotional code CR
                sSucessMessage = CType(Session("customerpr"), CustomerProductRegistration).SUCCESS_MESSAGE
                If sSucessMessage <> "" Then
                    divSuccess.Visible = True
                End If
            Catch ex As Exception
                Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

    End Class

End Namespace
