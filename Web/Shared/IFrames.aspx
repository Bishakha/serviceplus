﻿<%@ Reference Page="~/SSL.aspx" %>
<%@ Page Language="C#" AutoEventWireup="true" CodeFile="IFrames.aspx.cs" Inherits="IFrames" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ServicesPLUS - New Credit Card</title>

    <link href="includes/ServicesPLUS_style.css" rel="stylesheet" type="text/css" />
    
    <script type="text/javascript" src="../includes/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="../includes/svcplus_globalization.js"></script>
    
    <script language="javascript" type="text/javascript">
        var DataReturned = false;
        var CCToken = '';
        
        function ShowToken(vData) 
        {
            //alert(window.document.getElementById('CCToken').value);
            window.document.forms["form1"].submit();
            //form1.submit();
        }
        function PostiFrame() {
            document.getElementById("DIeCommFrame").contentWindow.document.getElementById("PayNowButton").click();
            return false;
        }

        function CloseWin() {
            window.opener.document.getElementById("btnHidden").click();
            this.window.close();
            return false;
        }
    </script>
</head>
<body>
    <form id="form1" runat="server" target="_parent" class="tableData">
    <asp:Label ID="ErrorLabel" runat="server" Text="" CssClass="redAsterick" EnableViewState="false" />
    <div id="PMFrame" runat="server">
        <input id="CCToken" type="hidden" runat="server" />
        <iframe id="DIeCommFrame" src="Tokenization.aspx?iFrame=true" style="border: none;" width="350" height="190"></iframe><br />
        <asp:ImageButton ID="ImageButton1" runat="server" AlternateText="Close" 
            ImageUrl="<%$ Resources:Resource,svc_Ifrm_sub_img%>" OnClientClick="return PostiFrame();" />
    </div>
    
    <div id="PMRespone" runat="server">
        <asp:Label ID ="lblResult" runat="server"></asp:Label><br/><br/>
        <center>
            <asp:ImageButton ID="btnClose" runat="server" AlternateText="Close" ImageUrl="<%$Resources:Resource,svc_Ifrm_cls_img%>" OnClientClick="return CloseWin();"> </asp:ImageButton>
        </center>
    </div>

    </form>
</body>
</html>
