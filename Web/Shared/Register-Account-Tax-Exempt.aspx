<%@ Reference Page="~/SSL.aspx" %>

<%@ Page Language="vb" AutoEventWireup="false" Inherits="ServicePLUSWebApp.Register_Account_Tax_Exempt"
    EnableViewStateMac="true" CodeFile="Register-Account-Tax-Exempt.aspx.vb" CodeFileBaseClass="ServicePLUSWebApp.SSL" %>

<%@ Register TagPrefix="ServicePLUSWebApp" TagName="PersonalMessage" Src="~/PersonalMessage.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="SonyFooter" Src="~/SonyFooter.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="TopSonyHeader" Src="~/TopSonyHeader.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title><%=Resources.Resource.ttl_TaxExempt%> </title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link rel="stylesheet" href="../includes/ServicesPLUS_style.css" type="text/css">

    <script language="javascript" src="../includes/ServicesPLUS.js" type="text/javascript"></script>
    <script type="text/javascript" src="../includes/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="../includes/svcplus_globalization.js"></script>

</head>
<body style="color: #000000; background: #5d7180; margin: 0px;">
    <form id="formid" autocomplete="off" method="post" runat="server">
        <center>
            <table width="760" border="0" cellpadding="0">
                <tr>
                    <td width="25" style="background: url('images/sp_left_bkgd.gif')">
                        <img height="25" src="images/spacer.gif" width="25" alt="">
                    </td>
                    <td width="710" bgcolor="#ffffff">
                        <table width="710" border="0" cellpadding="0">
                            <tr>
                                <td>
                                    <ServicePLUSWebApp:TopSonyHeader ID="TopSonyHeader" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <img src="<%=Resources.Resource.svc_Ifrm_cls_img_1()%>" width="710" height="29" alt="">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="710" border="0" cellpadding="0">
                                        <tr>
                                            <td width="20" height="20">
                                                <img src="../images/spacer.gif" width="20" height="20" alt="">
                                            </td>
                                            <td width="670" height="20">
                                                <img src="../images/spacer.gif" width="670" height="20" alt="">
                                            </td>
                                            <td width="20" height="20">
                                                <img src="../images/spacer.gif" width="20" height="20" alt="">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="20">
                                                <img src="../images/spacer.gif" width="20" height="20" alt="">
                                            </td>
                                            <td width="670" valign="top">
                                                <table width="670" border="0" cellpadding="0">
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="ErrorLabel" runat="server" CssClass="redAsterick" />
                                                        </td>
                                                    </tr>
                                                    <tr bgcolor="#f2f5f8">
                                                        <td colspan="2" class="bodycopy">
                                                            &nbsp;&nbsp;
                                                            <asp:Label ID="ErrorSonyACC" runat="server" CssClass="redAsterick" />
                                                        </td>
                                                    </tr>
                                                    <tr id="trCBSAccount" visible="false" runat="server">
                                                        <td class="bodyCopy" colspan="2" height="30">
                                                            &nbsp;&nbsp;
                                                            <asp:Label ID="LabelCBSAct" runat="server" CssClass="bodyCopy">Sony Open Account Number(s)</asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr bgcolor="#f2f5f8" id="trSonyAccount" runat="server">
                                                        <td class="bodyCopy" colspan="2" height="53">
                                                            &nbsp;&nbsp;
                                                        <asp:Label ID="LabelSonyAct" runat="server" CssClass="bodyCopy" Text="<%$ Resources:Resource, el_rgn_OpenAccount%>"></asp:Label>
                                                            &nbsp;<span class="redAsterick"> *</span>
                                                            <asp:RadioButtonList ID="rbSonyAccount" runat="server" CssClass="bodyCopy" RepeatDirection="Horizontal"
                                                                AutoPostBack="True" OnSelectedIndexChanged="rbSonyAccount_SelectedIndexChanged">
                                                                <asp:ListItem Value="1" Text="<%$ Resources:Resource, el_yes%>"></asp:ListItem>
                                                                <asp:ListItem Value="0" Text="<%$ Resources:Resource, el_no%>"></asp:ListItem>
                                                            </asp:RadioButtonList>
                                                        </td>
                                                    </tr>
                                                    <tr bgcolor="#f2f5f8">
                                                        <td colspan="3" height="39">
                                                            <asp:Label ID="lblAccountNoErrors" runat="server" ForeColor="Red" Font-Bold="true"
                                                                Visible="false" CssClass="bodyCopy" />
                                                            <asp:Table ID="tblCart" runat="server" Width="600" CellSpacing="0">
                                                            </asp:Table>
                                                            &nbsp;&nbsp;&nbsp;<asp:Button ID="AddMore2" runat="server" CssClass="bodyCopy" Text="<%$ Resources:Resource, el_rgn_AddMoreAcct%>"
                                                                Visible="False"></asp:Button>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="top" class="bodyCopy">
                                                            <img src="../images/spacer.gif" width="444" height="1" alt=""><br />
                                                            <div id="divWarningMsg" runat="server" visible="false">
                                                                <%=Resources.Resource.el_rgn_AccountReqd_CAN%>
                                                            </div>
                                                            <div id="pnlCCTaxExemptDetails" runat="server" visible="false">
                                                                <span class="tableHeader"><%=Resources.Resource.el_rgn_TaxMsg1%>  </span>
                                                                <br />
                                                                <span class="bodyCopy"><%=Resources.Resource.el_rgn_TaxMsg2%> </span>
                                                                <p>
                                                                    <span class="bodyCopy"><%=Resources.Resource.el_rgn_TaxMsg3%><a target="_blank"
                                                                        class="body" href="pdfs/TaxExemptApp.pdf"><%=Resources.Resource.el_rgn_TaxMsg4%></a>
                                                                        <%=Resources.Resource.el_rgn_TaxMsg5%> </span>
                                                                </p>
                                                                <p>
                                                                    <span class="tableHeader"><%=Resources.Resource.el_Note%></span><br />
                                                                    <span class="bodyCopy"><a href="#" class="body" onclick="javascript:window.print();">
                                                                        <%= Resources.Resource.el_rgn_TaxMsg6 %></a> <%=Resources.Resource.el_rgn_TaxMsg7%> </span>
                                                                    <br />
                                                                    <div id="divCustomerID" runat="server" visible="false">
                                                                        You<span class="bodyCopyBold">MUST</span><span class="bodyCopy"> include this Customer
                                                                        ID:
                                                                        <asp:Label ID="LabelSiamID" runat="server" CssClass="tableHeader">Label</asp:Label>
                                                                        </span><span class="tableHeader">.</span>
                                                                    </div>
                                                                </p>
                                                            </div>
                                                            <br />
                                                            <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="<%$ Resources:Resource,svc_rgstr_thnku_nxt_img%>"
                                                                AlternateText="Next"></asp:ImageButton>
                                                            <br />
                                                            <img src="../images/spacer.gif" width="444" height="40" alt="">
                                                        </td>
                                                        <td valign="top" align="center">
                                                            <img src="../images/spacer.gif" width="226" height="1" alt=""><br />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td width="20">
                                                <img src="../images/spacer.gif" width="20" height="20" alt="">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <footer><ServicePLUSWebApp:SonyFooter ID="SonyFooter1" runat="server" /></footer>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="25" style="background: url('images/sp_right_bkgd.gif')">
                        <img src="images/spacer.gif" width="25" height="20" alt="">
                    </td>
                </tr>
            </table>
        </center>
    </form>

    <script type="text/javascript">
		<%
        If focusFormField <> "" Then
            Response.Write("document.getElementById('" + focusFormField + "').focus();")
        End If
		%>					
    </script>
</body>
</html>
