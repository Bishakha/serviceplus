﻿
'******************************************************************************************************************
'Module name               : SAP Tokenization
'Program name              : Tokenization.aspx.vb
'Program Description       : Call the Paymetric Server for getting the token number
'Created by                : Venkata Srinivas Dwara
'Created date              : 24-MAY-2011
'Change history            :
'Changed by                : 
'Changed date              : 
'Change description        : 
'*******************************************************************************************************************

Imports System.Security.Cryptography
Imports ServicesPlusException
Imports Sony.US.AuditLog
Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.SIAMUtilities

Namespace ServicePLUSWebApp
    Partial Class Tokenization
        Inherits SSL

        Dim GUID As String
        Dim PresharedKey As String
        Dim BaseURL As String
        'Dim objUtilties As Utilities = New Utilities
        Private Const PayloadXML As String = "<Request><MerchantReference></MerchantReference><TotalAmount><Tax></Tax><GrandTotal></GrandTotal><CurrencyCode></CurrencyCode></TotalAmount><BillToInfo><ID></ID><Name><FirstName></FirstName><LastName></LastName><MiddleName></MiddleName></Name><Address><Address1></Address1><Address2></Address2><City></City><CountryCode>US</CountryCode><PostalCode></PostalCode><State></State></Address><PhoneNumber></PhoneNumber><Email></Email><Reference></Reference><Description></Description></BillToInfo><ShipToInfo><ID></ID><Name><FirstName></FirstName><LastName></LastName><MiddleName></MiddleName></Name><Address><Address1></Address1><Address2></Address2><City></City><CountryCode></CountryCode><PostalCode></PostalCode><State></State></Address><PhoneNumber></PhoneNumber><Email></Email><Reference></Reference><Description></Description></ShipToInfo><TokenizedCard><CCToken></CCToken><CCExpirationDate><Month></Month><Year></Year></CCExpirationDate><CCType></CCType></TokenizedCard><RedirectURL>{0}</RedirectURL></Request>"

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Dim objPCILogger As New PCILogger()
            Dim ScriptTag As LiteralControl
            Dim ParsedResponse As DIResponse
            Dim customer As Customer
            Dim returnPayload As String
            Dim signature As String

            Try
                If HttpContextManager.Customer Is Nothing Then
                    Response.Redirect("~/SignIn.aspx?post=token", False)
                    HttpContext.Current.ApplicationInstance.CompleteRequest()
                End If
                customer = HttpContextManager.Customer

                objPCILogger.EventOriginApplication = "ServicesPLUS"
                objPCILogger.EventOriginApplicationLocation = Me.Page.GetType().Name
                objPCILogger.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
                objPCILogger.OperationalUser = Environment.UserName
                objPCILogger.EventDateTime = DateTime.Now.ToLongTimeString()
                objPCILogger.HTTPRequestObjectValues = objPCILogger.GetRequestContextValue()
                objPCILogger.IndicationSuccessFailure = "Success"
                objPCILogger.EventOriginMethod = "Page_Load"
                objPCILogger.Message = "Page_Load Success."
                objPCILogger.EventType = EventType.View
                objPCILogger.CustomerID = customer.CustomerID
                objPCILogger.CustomerID_SequenceNumber = customer.SequenceNumber
                objPCILogger.EmailAddress = customer.EmailAddress
                objPCILogger.SIAM_ID = customer.SIAMIdentity
                objPCILogger.LDAP_ID = customer.LdapID

                If Not Page.IsPostBack Then
                    ' 2016-05-10 ASleight Fixed faulty logic. (String not "") OR (String not empty) will always be true.
                    'If Not Session("Environment") = "" Or Not Session("Environment") Is Nothing Then
                    If (Session("Environment") IsNot Nothing) AndAlso (Not String.IsNullOrEmpty(Session("Environment").ToString())) Then
                        ConfigurationData.TokenizationTest.Load(Session("Environment").ToString())
                        GUID = ConfigurationData.TokenizationTest.GUID
                        PresharedKey = ConfigurationData.TokenizationTest.PresharedKey
                        BaseURL = ConfigurationData.TokenizationTest.BaseURL
                    Else
                        GUID = ConfigurationData.Environment.Token.GUID
                        PresharedKey = ConfigurationData.Environment.Token.PresharedKey
                        BaseURL = ConfigurationData.Environment.Token.BaseURL
                    End If
                End If

                If Request.QueryString("r") IsNot Nothing AndAlso Request.QueryString("s") IsNot Nothing Then
                    ' we have a return payload
                    returnPayload = ModifiedBase64Decode(Request.QueryString("r"))
                    signature = Request.QueryString("s").Replace("-", "+").Replace("_", "/")
                    If SignData(returnPayload, PresharedKey) = signature Then
                        ParsedResponse = New DIResponse(returnPayload)
                        ' the signature was verified!

                        Session("token") = ParsedResponse.Token
                        Session("ExpirationMonth") = ParsedResponse.ExpirationMonth
                        Session("ExpirationYear") = ParsedResponse.ExpirationYear
                        Session("CreditCardType") = ParsedResponse.CreditCardType
                        Session("CVV") = ParsedResponse.CVV

                        Me.DIContainer.InnerHtml += "<br/>&nbsp;<br/>"
                        ScriptTag = New LiteralControl("<script type=""text/javascript"" language=""javascript"">" & _
                            "if (window.parent.document != window.document) { window.parent.document.getElementById('CCToken').value = '" + ParsedResponse.Token & "'; " & _
                            "window.parent.DataReturned = true; window.parent.ShowToken(); }</script>")
                        Me.DIContainer.Controls.Add(ScriptTag)
                        ScriptTag.Dispose()
                        ' 2016-05-10 ASleight Why are we adding the ScriptTag again after it's been disposed?
                        'Me.DIContainer.Controls.Add(ScriptTag)
                    End If
                Else
                    ScriptTag = New LiteralControl("<script type=""text/javascript"" language=""javascript"">setPMTimeout(30000)</script>")
                    ScriptTag.Dispose()     ' Disposed before doing anything with the above line.
                    ScriptTag = New LiteralControl("<script src=""" & BaseURL & "/diecomm/Preloader/EN.ashx?GUID=" & GUID & """ type=""text/javascript"" language=""javascript""></script>")
                    Me.DIContainer.Controls.Add(ScriptTag)
                    ScriptTag.Dispose()

                    Dim Url As String
                    If ConfigurationData.GeneralSettings.Environment = "Development" Then
                        Url = Request.Url.ToString()
                    Else
                        If HttpContextManager.GlobalData.IsAmerica Then
                            Url = ConfigurationData.Environment.URL.US + Request.Url.PathAndQuery
                        Else
                            Url = ConfigurationData.Environment.URL.CA + Request.Url.PathAndQuery
                        End If
                    End If

                    ScriptTag = New LiteralControl("<script type=""text/javascript"" language=""javascript"">ShowCVV=false; PerformTypeCheck = true; CCNumberLuhnErrorMessage='Invalid credit card number';" & _
                        " ExpirationDateErrorMessage='Please enter a valid expiration date'; CCNumberRequiredMessage='Please enter a valid credit card number'; CCNumberLuhnErrorMessage = 'The Credit Card Number is not a valid card number';" & _
                        " CCNumberCardTypeErrorMessage = 'Card number does not match the type selected'; UpdatePaymentPageContent('" & EncodePayload([String].Format(PayloadXML, Url), GUID) & "');" & _
                        " document.getElementById('DIContainer').getElementsByTagName('tr')[3].childNodes[0].innerHTML = 'Expiration Date (MM/YY)';</script>")
                    Me.DIContainer.Controls.Add(ScriptTag)
                End If
            Catch ex As Exception
                objPCILogger.IndicationSuccessFailure = "Failure" '6524
                objPCILogger.Message = "Page_Load Failed. " & ex.Message.ToString() '6524
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            Finally
                objPCILogger.PushLogToMSMQ() '6524
            End Try
        End Sub

        Protected Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
            Dim css As New HtmlLink()
            css.Href = "../includes/ServicesPLUS_style.css"
            css.Attributes("rel") = "stylesheet"
            css.Attributes("type") = "text/css"
            css.Attributes("media") = "all"
            Page.Header.Controls.Add(css)
        End Sub

        Public Function ModifiedBase64Decode(ByVal Data As String) As String
            Return Base64Decode(Data.Replace("-", "+").Replace("_", "/"))
        End Function

        Public Function ModifiedBase64Encode(ByVal Data As String) As String
            Return Base64Encode(Data).Replace("+", "-").Replace("_", "/")
        End Function

        Public Function Base64Encode(ByVal Data As String) As String
            Return Convert.ToBase64String(System.Text.Encoding.[Default].GetBytes(Data))
        End Function

        Public Function Base64Decode(ByVal Data As String) As String
            Return System.Text.Encoding.[Default].GetString(Convert.FromBase64String(Data))
        End Function

        Public Function EncodePayload(ByVal payload As String, ByVal merchantGuid As String) As String
            payload = payload.Replace(vbLf, "")
            Return "<MerchantRequest><MerchantGUID>" & merchantGuid & "</MerchantGUID><Signature>" & SignData(payload, PresharedKey) & "</Signature>" & payload & "</MerchantRequest>"
        End Function

        Public Shared Function SignData(ByVal dataToSign As String, ByVal signatureKey As String) As String
            Return Convert.ToBase64String(GetHMAC(dataToSign, signatureKey))
        End Function

        Public Shared Function GetHMAC(ByVal data As String, ByVal key As String) As Byte()
            Dim dataBytes As Byte() = Encoding.[Default].GetBytes(data)
            Dim keyBytes As Byte() = Encoding.[Default].GetBytes(key)
            Return GetHMAC(dataBytes, keyBytes)
        End Function

        Public Shared Function GetHMAC(ByVal contents As Byte(), ByVal key As Byte()) As Byte()
            Try
                Dim hashAlg As HMAC = HMAC.Create("HMACSHA256")
                hashAlg.Key = key
                Dim returnValue As Byte() = hashAlg.ComputeHash(contents)
                Return returnValue
            Catch e As Exception
                Return System.Text.Encoding.[Default].GetBytes(e.Message)
            End Try
        End Function

    End Class
End Namespace