﻿<%@ Reference Page="~/SSL.aspx" %>
<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Tokenization.aspx.vb" 
Inherits="ServicePLUSWebApp.Tokenization" CodeFileBaseClass="ServicePLUSWebApp.SSL" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script type="text/javascript" src="../includes/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="../includes/svcplus_globalization.js"></script>
    <script type="text/javascript" language="javascript">
        var kill;
        var PMLoaded;
        
        function setPMTimeout(timeoutVal) {
            PMLoaded = 0;
            kill = window.setTimeout(goToErrorPage, timeoutVal);
        }

        function clearPMTimeout() {
            PMLoaded = 1;
            window.clearTimeout(kill);
        }

        function goToErrorPage() {
            if (PMLoaded === 0) {
                alert("Credit card payment server down... Please try later");
                if (navigator.appName === "Microsoft Internet Explorer") {
                    window.document.execCommand('Stop');
                }
                else {
                    window.stop();
                }
            }
        }
    </script>
</head>

<body>
    <form id="frmTokenization" runat="server">
    <asp:Label ID="ErrorLabel" runat="server" Text="" CssClass="redAsterick" EnableViewState="false" />
    <div id="DIContainer" runat="server">
                               
    </div>
    </form>
</body>
</html>
