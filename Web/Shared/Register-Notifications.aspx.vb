﻿Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Process
Imports Sony.US.ServicesPLUS.Data
Imports Sony.US.siam
Imports System.Xml
Imports System.Text
Imports System.IO
Imports Sony.US.AuditLog
Imports ServicesPlusException


Namespace ServicePLUSWebApp


    Partial Class RegisterNotification
        Inherits SSL


        '-- asp generated --
#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        'Protected WithEvents TableInterests As System.Web.UI.WebControls.Table
        Protected WithEvents marketingInterestCheckBox As System.Web.UI.WebControls.CheckBox


        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
            Page.ID = Request.Url.AbsolutePath
        End Sub

#End Region



        ' -- page members --
#Region "Page members"

        Const conSelectAllMarketingInterestName = "mi_SelectAll"
        Const conSelectAllMarketingInterestPrefix = "mi_"
        Const conControlsPerRow As Int32 = 3
        Const conStyleSheetForInterestControls As String = "bodyCopy"
        Const conBgColorForMarketingInterest As String = "#ffffff"
        Const conBgColorForMarketingInterestAlt As String = "#f2f5f8"
        'Dim objUtilties As Utilities = New Utilities
        'Dim objGlobalData As New GlobalData

#End Region



        '-- events --
#Region "events"
        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                If Session.Item("customer") Is Nothing Then
                    Response.Redirect("~/SignIn.aspx?post=RN", False)
                    HttpContext.Current.ApplicationInstance.CompleteRequest()
                End If

                'Added by kayal - WP010
                'If Not Session.Item("GlobalData") Is Nothing Then
                '    objGlobalData = Session.Item("GlobalData")
                'End If

                If Not IsPostBack Then controlMethod(sender, e)
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Private Sub ImageButton1_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles submitMarketingInterest.Click
            controlMethod(sender, e)

        End Sub

#End Region



        '-- methods

#Region "Methods"

#Region "control method"

        Private Sub controlMethod(ByVal sender As System.Object, ByVal e As System.EventArgs)
            Try
                If Not Session.Item("Customer") Is Nothing Then 'And Not Session.Item("newSIAMUser") Is Nothing Then
                    Dim thisCustomer As Customer = Session.Item("Customer")
                    'Dim thisSIAMUser As Sony.US.siam.User = Session.Item("newSIAMUser")
                    Dim callingControl As String = ""

                    If Not sender.Id Is Nothing Then callingControl = sender.ID.ToString()
                    '-- process functionality based on calling control --
                    Select Case callingControl
                        Case Page.ID.ToString()
                            If Not IsPostBack Then populateInterestSection(thisCustomer)
                        Case submitMarketingInterest.ID.ToString()
                            updateMarketingInterest(thisCustomer)
                            createNewCustomer(thisCustomer) ', thisSIAMUser)

                    End Select
                End If
            Catch ex As Exception
                ErrorLabel2.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

#End Region


#Region "populate marketing interest"

        Private Sub populateInterestSection(ByVal customer As Customer)
            Try
                If Not customer Is Nothing Then
                    Dim customerManager As New CustomerManager
                    Dim allMarketingInterest() As MarketingInterest = customerManager.GetAllMarketingInterests(HttpContextManager.GlobalData.Language)

                    buildTableInterests(allMarketingInterest, customer)
                End If
            Catch ex As Exception
                Response.Write(ex.Message.ToString())
            End Try
        End Sub

        Private Sub buildTableInterests(ByVal allMarketingInterest() As MarketingInterest, ByVal customer As Customer)
            Try
                Dim controlCounter As Int32 = 0
                Dim rowBackGroundColor As String = conBgColorForMarketingInterest

                allMarketingInterest = addSelectAllCheckBoxToArray(allMarketingInterest)

                TableInterests.Controls.Add(New LiteralControl("<table cellpadding=""0"" cellspacing=""0"" border=""0"">"))

                For Each thisMarketingInterest As MarketingInterest In allMarketingInterest
                    If controlCounter = conControlsPerRow Then '- end of the row
                        TableInterests.Controls.Add(New LiteralControl("</tr>" + vbCrLf))
                        controlCounter = 0
                    End If

                    If controlCounter = 0 Then '- start of the row
                        '-- change the rows background color
                        rowBackGroundColor = IIf(rowBackGroundColor = conBgColorForMarketingInterestAlt.ToString(), conBgColorForMarketingInterest, conBgColorForMarketingInterestAlt.ToString())
                        TableInterests.Controls.Add(New LiteralControl("<tr bgcolor=""" + rowBackGroundColor + """>" + vbCrLf))
                    End If

                    marketingInterestCheckBox = New CheckBox
                    marketingInterestCheckBox.Text = "&nbsp;" + thisMarketingInterest.Description.ToString()
                    marketingInterestCheckBox.ID = conSelectAllMarketingInterestPrefix.ToString() + thisMarketingInterest.MarketingInterestID.ToString()
                    marketingInterestCheckBox.CssClass = conStyleSheetForInterestControls
                    marketingInterestCheckBox.Checked = isInterestedChecked(thisMarketingInterest.MarketingInterestID.ToString(), customer)


                    TableInterests.Controls.Add(New LiteralControl(vbTab + "<td width=""220"" align=""left"">" + vbCrLf))

                    If marketingInterestCheckBox.ID.ToString() = conSelectAllMarketingInterestName Then
                        marketingInterestCheckBox.Attributes.Add("onclick", "selectMarketingInterest(" + allMarketingInterest.Length().ToString() + ", '" + conSelectAllMarketingInterestName + "', '" + conSelectAllMarketingInterestPrefix.ToString() + "')")
                    End If

                    ' Me.Page.FindControl("Form1").Controls.Add(marketingInterestCheckBox)
                    TableInterests.Controls.Add(marketingInterestCheckBox)

                    TableInterests.Controls.Add(New LiteralControl("</td>" + vbCrLf))

                    controlCounter = (controlCounter + 1)
                Next
            Catch ex As Exception
                ErrorLabel2.Text = Utilities.WrapExceptionforUI(ex)
            End Try


            TableInterests.Controls.Add(New LiteralControl("</table>"))
        End Sub


        Private Function isInterestedChecked(ByVal marketingInterestID As String, ByVal customer As Customer) As Boolean
            Dim returnValue As Boolean = False
            Try
                If Not customer Is Nothing And Not marketingInterestID Is Nothing Then
                    If Request.Form(marketingInterestID.ToString()) Is Nothing Then
                        For Each thisMarketingInterest As MarketingInterest In customer.MarketingInterests
                            If thisMarketingInterest.MarketingInterestID.ToString = marketingInterestID.ToString() And Not thisMarketingInterest.Dirty Then
                                returnValue = True
                                Exit For
                            End If
                        Next
                    Else
                        returnValue = True
                    End If
                End If
            Catch ex As Exception
                ErrorLabel2.Text = Utilities.WrapExceptionforUI(ex)
            End Try

            Return returnValue
        End Function


        Private Function addSelectAllCheckBoxToArray(ByVal marketingInterest() As MarketingInterest) As MarketingInterest()
            Dim returnMarketingInterest(marketingInterest.Length) As MarketingInterest
            Try
                If marketingInterest IsNot Nothing Then
                    Dim newMarketingInterest As New ArrayList
                    Dim addedInterest As New MarketingInterest With {
                        .MarketingInterestID = conSelectAllMarketingInterestName.Replace(conSelectAllMarketingInterestPrefix, "").ToString(),
                        .Description = Resources.Resource.el_SelectAll
                    }
                    newMarketingInterest.Add(addedInterest)

                    For Each thisInterest As MarketingInterest In marketingInterest
                        newMarketingInterest.Add(thisInterest)
                    Next

                    newMarketingInterest.CopyTo(returnMarketingInterest)
                End If
            Catch ex As Exception
                ErrorLabel2.Text = Utilities.WrapExceptionforUI(ex)
            End Try

            Return returnMarketingInterest
        End Function


#End Region


        '-- update marketing interest
#Region "marketing interest"
        Private Function updateMarketingInterest(ByVal customer As Customer) As Boolean
            Dim returnValue As Boolean = False

            Try
                If Not customer Is Nothing Then
                    Dim customerManager As New CustomerManager
                    Dim allMarketingInterest() As MarketingInterest = customerManager.GetAllMarketingInterests(HttpContextManager.GlobalData.Language)
                    Dim customerMarketingInterest() As MarketingInterest = customer.MarketingInterests
                    Dim isChecked As Boolean = False
                    Dim foundInCustomer As Boolean = False

                    If ProductsAndOffers.Checked Then
                        customer.EmailOk = True
                    Else
                        customer.EmailOk = False
                    End If
                    customerManager.UpdateCustomerSubscription(customer)
                    For Each thisMarketingInterest As MarketingInterest In allMarketingInterest
                        If Not Request.Form("mi_" + thisMarketingInterest.MarketingInterestID.ToString()) Is Nothing Then
                            If Request.Form("mi_" + thisMarketingInterest.MarketingInterestID.ToString()).ToString = "on" Then
                                isChecked = True
                            End If
                        End If

                        For Each customerMI As MarketingInterest In customer.MarketingInterests
                            If customerMI.MarketingInterestID.ToString() = thisMarketingInterest.MarketingInterestID.ToString() Then
                                If Not isChecked Then customer.RemoveMarketingInterest(customerMI)
                                foundInCustomer = True
                                Exit For
                            End If
                        Next

                        If isChecked Then
                            If Not foundInCustomer Then customer.AddMarketingInterest(thisMarketingInterest)
                        End If

                        foundInCustomer = False
                        isChecked = False
                    Next
                End If
                returnValue = True
            Catch ex As Exception
                ErrorLabel2.Text = Utilities.WrapExceptionforUI(ex)
            End Try

            Return returnValue
        End Function
#End Region

        Private Sub createNewCustomer(ByRef thisCustomer As Customer) ', ByRef thisSIAMUser As Sony.US.siam.User)
            Try

                Dim objPCILogger As New PCILogger()
                objPCILogger.EventOriginApplication = "ServicesPLUS"
                objPCILogger.EventOriginApplicationLocation = Me.Page.GetType().Name
                objPCILogger.EventOriginMethod = "createNewCustomer"
                objPCILogger.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
                objPCILogger.OperationalUser = Environment.UserName
                objPCILogger.EventDateTime = DateTime.Now.ToLongTimeString()
                objPCILogger.HTTPRequestObjectValues = GetRequestContextValue()
                objPCILogger.EventType = EventType.Add
                objPCILogger.HTTPRequestObjectValues = GetRequestContextValue()



                Dim cm As New CustomerManager
                'thisCustomer.SIAMIdentity = thisSIAMUser.Identity
                cm.UpdateCustomerMarketingInterest(thisCustomer)
                Session.Add("customer", thisCustomer)

                'cm.SendWelcomeEmail(thisCustomer)

                objPCILogger.IndicationSuccessFailure = "Success"
                ''objPCILogger.UserIdentity = thisCustomer.CustomerID
                'objPCILogger.UserIdentitySequenceNumber = thisCustomer.SequenceNumber

                'objPCILogger.UserName = thisCustomer.UserName
                objPCILogger.EmailAddress = thisCustomer.EmailAddress '6524
                objPCILogger.CustomerID = thisCustomer.CustomerID
                objPCILogger.CustomerID_SequenceNumber = thisCustomer.SequenceNumber
                objPCILogger.SIAM_ID = thisCustomer.SIAMIdentity
                objPCILogger.LDAP_ID = thisCustomer.LdapID '6524

                objPCILogger.Message = Resources.Resource.el_rgn_Success '"Registration Completed Successfully"
                objPCILogger.PushLogToMSMQ()

                'Response.Redirect("ThankYouNA.aspx")
                'Response.Redirect("ThankYouNA.aspx", False) '6725 starts
                'If thisCustomer.UserType = "NonAccount" Then'6668
                '6956 starts
                'If thisCustomer.UserType = "C" Then '6668
                '    Response.Redirect("ThankYouNA.aspx", False)
                'Else
                '    Response.Redirect("ThankYou.aspx", False)
                'End If
                Session("custusertype") = thisCustomer.UserType
                Response.Redirect("Registration-Thank-You.aspx", False)
                HttpContext.Current.ApplicationInstance.CompleteRequest()
                '6956 ends
                '6725 ends

            Catch ex As Exception
                ErrorLabel2.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

#End Region



    End Class

End Namespace
