﻿Imports ServicesPlusException
Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Process
Imports Sony.US.siam
Imports Sony.US.SIAMUtilities


Namespace ServicePLUSWebApp
    Partial Class Register_Existing_User
        Inherits SSL

        Public focusFormField As String
        Dim myLdap As String = String.Empty
        Dim customer As New Customer
        Dim myLdapId As String
        Dim isVerified As Boolean

#Region " Web Form Designer Generated Code "
        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            Page.ID = Request.Url.Segments.GetValue(1)
            InitializeComponent()
        End Sub
#End Region

#Region "   Events  "
        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As EventArgs) Handles MyBase.Load
            Dim cust As Customer
            Dim admin As New SecurityAdministrator
            Dim data As String = String.Empty
            Dim emailid As String = String.Empty

            Try
                If HttpContextManager.Customer Is Nothing Then
                    Response.Redirect("~/SignIn.aspx?post=reu", False)
                    HttpContext.Current.ApplicationInstance.CompleteRequest()
                End If
                If Not IsPostBack Then
                    ZipCodeConfig()
                End If
                '6959
                'If HttpContextManager.Customer Is Nothing Then
                ' This can never be reached, since the above If block redirects.
                '    'show message as per '6959
                '    Try
                '        lblwelcome.Text = ""
                '        emailid = HttpContext.Current.Request.Headers("EMAIL").ToString()
                '        data = admin.SearchUser(emailid)
                '    Catch ex As Exception
                '        ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                '        Dim er As String
                '        er = ErrorLabel.Text.Replace("Unexpected error encountered. For assistance, please", Resources.Resource.el_Err_Unexp_Replace)
                '        ErrorLabel.Text = er
                '        Exit Sub
                '    End Try
                '    If Not data = "" Then
                '        Dim newUserData As String() = data.Split("*"c)
                '        lblwelcome.Text = newUserData(0) & " " & newUserData(1) & " of " & newUserData(2)
                '    End If
                'Else
                cust = HttpContextManager.Customer
                lblwelcome.Text = cust.FirstName & " " & cust.LastName & " of " & cust.CompanyName
                If Not Page.IsPostBack Then
                    txtFirstName.Text = cust.FirstName
                    txtLastName.Text = cust.LastName
                    txtCompany.Text = cust.CompanyName
                    'ControlMethod(sender, e)
                    BuildStateDropDown()
                End If
                'End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Sub NextButton_Click(ByVal sender As System.Object, ByVal e As ImageClickEventArgs) Handles NextButton.Click
            Try
                'ControlMethod(sender, e)
                If IsDataValid() Then ProcessRegistrationForm()
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub
#End Region

#Region "   Methods     "
        'Private Sub ControlMethod(ByVal sender As System.Object, ByVal e As System.EventArgs)
        '    Try
        '        Dim caller As String = String.Empty
        '        If Not sender.ID Is Nothing Then caller = sender.ID.ToString()
        '        Select Case caller
        '            Case Page.ID.ToString()
        '                BuildStateDropDown()
        '            Case NextButton.ID.ToString()
        '                If IsDataValid() Then
        '                    processRegistrationForm()
        '                End If
        '        End Select
        '    Catch ex As Exception
        '        ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
        '    End Try
        'End Sub

        Private Sub ProcessRegistrationForm()
            If ErrorValidation.Visible Then
                ErrorValidation.Text = Resources.Resource.el_rgn_Requirevalidate ' "Please complete the required fields."
                Return
            End If

            Try
                customer = HttpContextManager.Customer
                customer.UserType = "C"
                customer.CompanyName = txtCompany.Text.Trim()
                customer.Address.Line1 = txtLine1.Text.Trim()
                customer.Address.Line2 = txtLine2.Text.Trim()
                'customer.Address.Line3 = Line3.Text.Trim()'7901
                customer.Address.City = txtCity.Text.Trim()
                customer.Address.State = ddlState.SelectedValue
                customer.UserName = customer.EmailAddress
                customer.UserLocation = 1
                '---------------------------------------------
                customer.Address.PostalCode = txtZip.Text.Trim()
                customer.PhoneNumber = txtPhone.Text.Trim()
                customer.PhoneExtension = txtExtension.Text.Trim()
                customer.FaxNumber = Fax.Text.Trim()
                customer.StatusCode = customer.StatusCode
                Session.Add("siamID", customer.SIAMIdentity)
                Session.Add("newSIAMUser", customer.SIAMIdentity)
                Dim cm As New CustomerManager

                'Dim tmpCust As Customer
                ' tmpCust = cm.GetCustomer(customer.LdapID, customer.EmailAddress, customer.FirstName, customer.LastName)
                'customer = Session("customer")
                If HttpContextManager.Customer Is Nothing Then
                    'Insert into database
                    cm.RegisterCustomer(customer)
                ElseIf (customer.StatusCode = 1) Then
                    cm.UpdateCustomerSubscription(customer)
                ElseIf (customer.StatusCode = 3) Then
                    If (cm.DeleteCustomerRecord(customer.CustomerID)) Then
                        customer.StatusCode = 1
                        cm.RegisterCustomer(customer)
                    Else
                        Throw New Exception(Resources.Resource.el_rgn_Err_Rec) '"Customer Record is Not Updated.")
                    End If
                ElseIf (customer.StatusCode = 0) Then
                    customer.StatusCode = 1
                    cm.RegisterCustomer(customer)
                End If
                Session.Add("newCustomer", customer)
                HttpContextManager.Customer = customer
                Response.Redirect("Register-Account-Tax-Exempt.aspx?type=User", False)
                HttpContext.Current.ApplicationInstance.CompleteRequest()
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                Return
            End Try
        End Sub

#Region "Helper Methods "

        Private Function IsDataValid() As Boolean
            Dim rgxPhoneNumber As New Regex("(^\d{3}-\d{3}-\d{4}$)")
            Dim rgxNumeric As New Regex("^[0-9]*$")
            Dim sCompany As String = txtCompany.Text.Trim()
            Dim sPhoneNumber As String = txtPhone.Text.Trim()
            Dim sExtension As String = txtExtension.Text.Trim()
            Dim sFaxNumber As String = Fax.Text.Trim()
            Dim isValid = True

            ' 2017-01-24 ASleight Adding Company validation to the registration form, because some
            '   users have no company set in LDAP/AD, and it is a required field in the database.
            If String.IsNullOrWhiteSpace(sCompany) Then
                isValid = False
                lblCompany.CssClass = "redAsterick"
                lblCompanyRequired.Text = "<span class=""redAsterick"">Company Name is required.</span>"
                txtCompany.Focus()
            ElseIf sCompany.Length > 35 Then
                isValid = False
                lblCompany.CssClass = "redAsterick"
                lblCompanyRequired.Text = "<span class=""redAsterick"">Company Name must be 35 characters or less.</span>"
                txtCompany.Focus()
            Else
                lblCompany.CssClass = ""
                lblCompanyRequired.Text = "<span class=""redAsterick"">*</span>"
            End If

            If (String.IsNullOrWhiteSpace(sPhoneNumber)) OrElse (Not rgxPhoneNumber.IsMatch(sPhoneNumber)) Then
                isValid = False
                lblPhoneError.Text = "</br>" + Resources.Resource.el_validate_Phone 'Please enter phone number in format xxx-xxx-xxxx." '6909V4
                lblPhoneError.Visible = True
                lblPhone.CssClass = "redAsterick"
            Else
                lblPhoneError.Text = ""
                lblPhoneError.Visible = False
                lblPhone.CssClass = ""
            End If

            If sExtension.Length > 0 Then
                If Not rgxNumeric.IsMatch(sExtension) Or sExtension.Length > 6 Then
                    isValid = False
                    lblEtxErr.Text = "</br>" + Resources.Resource.el_validate_Extn 'Extension must be numeric and 6 or fewer digits." '6919
                    lblEtxErr.Visible = True
                    lblExt.CssClass = "redAsterick"
                Else
                    lblEtxErr.Visible = False
                    lblExt.CssClass = ""
                End If
            Else
                lblEtxErr.Visible = False
                lblExt.CssClass = ""
            End If

            If Not sFaxNumber = "" Then
                If Not rgxPhoneNumber.IsMatch(sFaxNumber) Then
                    isValid = False
                    lblFaxError.Text = "</br>" + Resources.Resource.el_validate_Fax 'Please enter fax number in format xxx-xxx-xxxx."
                    lblFaxError.CssClass = "redAsterick"
                    lblFax1.CssClass = "redAsterick" '6919
                End If
            End If

            If String.IsNullOrWhiteSpace(txtLine1.Text) Then
                isValid = False
                lblLine1.CssClass = "redAsterick"
                lblLine1Star.Text = "<span class=""redAsterick"">Street Address is required.</span>"
            ElseIf txtLine1.Text.Trim().Length > 35 Then
                isValid = False
                lblLine1.CssClass = "redAsterick"
                lblLine1Star.Text = "<span class=""redAsterick"">Street Address must be 35 characters or less in length.</span>"
                txtLine1.Focus()
            Else
                lblLine1Star.Text = "<span class=""redAsterick"">*</span>"
                lblLine1.CssClass = "" '6919
            End If

            If txtLine2.Text.Length > 35 Then
                isValid = False
                lblLine2.CssClass = "redAsterick"
                lblLine2Star.Text = "<span class=""redAsterick"">" + Resources.Resource.el_ValidLength_Address2 + "</span>" 'Address 2nd Line must be 35 characters or less in length.</span>"
                txtLine2.Focus()
            Else
                lblLine2.CssClass = "" '6919
            End If

            If String.IsNullOrWhiteSpace(txtCity.Text) Then
                isValid = False
                lblCity.CssClass = "redAsterick"
                lblCityStar.Text = "<span class=""redAsterick"">*</span>"
            ElseIf txtCity.Text.Length > 35 Then
                isValid = False
                lblCity.CssClass = "redAsterick"
                lblCityStar.Text = "<span class=""redAsterick"">" + Resources.Resource.el_ValidLength_City + "</span>" 'City must be 35 characters or less in length.</span>"
                txtCity.Focus()
            Else
                lblCityStar.Text = "<span class=""redAsterick"">*</span>"
                lblCity.CssClass = "" '6919
            End If

            If ddlState.SelectedIndex = 0 Then
                isValid = False
                lblState.CssClass = "redAsterick"
            Else
                lblState.CssClass = "" '6919
            End If

            If String.IsNullOrWhiteSpace(txtZip.Text) Then
                isValid = False
                lblZip.CssClass = "redAsterick"
            Else
                lblZip.CssClass = "" '6919
            End If

            Me.zipRegularExpressionValidator.Validate()
            If Not Me.zipRegularExpressionValidator.IsValid Then
                lblZip.CssClass = "redAsterick" '6919
            ElseIf Not String.IsNullOrWhiteSpace(txtZip.Text) Then
                lblZip.CssClass = "" '6919
            End If

            If Not Page.IsValid Then
                isValid = False
            End If

            ErrorValidation.Visible = Not isValid ' If data is invalid, show the error box
            Return isValid
        End Function

        Private Sub BuildStateDropDown()
            '2018-09-04 ASleight - The base SSL page class now handles State/Province population.
            'Dim cm As New CatalogManager
            'Dim statedetails() As StatesDetail
            'statedetails = cm.StatesBySalesOrg(HttpContextManager.GlobalData)
            ''bind card type array to dropdown list control   

            'Dim strMsg As String = Resources.Resource.el_rgn_Select
            'Dim defaultItem As New ListItem
            'defaultItem.Text = strMsg
            'defaultItem.Value = ""
            'defaultItem.Selected = True

            'ddlState.DataSource = statedetails
            'ddlState.DataTextField = "StateAbbr"
            'ddlState.DataValueField = "StateAbbr"
            'ddlState.DataBind()
            'ddlState.Items.Insert(0, defaultItem)
            ddlState.Items.Clear()
            ddlState.Items.Add(New ListItem(Resources.Resource.el_rgn_Select, ""))  ' Add "Select One" as the first item before appending the States/Provinces.
            PopulateStateDropdownList(ddlState)
        End Sub

        Private Sub ZipCodeConfig()
            Dim inputLength As Int32
            Dim strValidate As String = String.Empty

            If HttpContextManager.GlobalData.SalesOrganization = ConfigurationData.GeneralSettings.GlobalData.SalesOrganization.US Then
                lblZip.Text = "Zip Code"
                inputLength = Convert.ToInt32(ConfigurationData.GeneralSettings.GlobalData.ZipCode.US)
                strValidate = "\d{" + inputLength.ToString() + "}(-\d{4})?$"
            Else
                lblZip.Text = "Postal Code"
                inputLength = Convert.ToInt32(ConfigurationData.GeneralSettings.GlobalData.ZipCode.CA)
                strValidate = "^[ABCEGHJKLMNPRSTVXY]{1}\d{1}[A-Z]{1} *\d{1}[A-Z]{1}\d{1}$"
            End If
            txtZip.Text = ""
            txtZip.MaxLength = inputLength
            zipRegularExpressionValidator.ValidationExpression = strValidate
            Regularexpressionvalidator1.ValidationExpression = strValidate
            zipRegularExpressionValidator.ErrorMessage = "</br>Invalid " + lblZip.Text + "."
            Regularexpressionvalidator1.ErrorMessage = "</br>Invalid " + lblZip.Text + "."

            'Upper case - Zip code 
            txtZip.Attributes.Add("onblur", "this.value = this.value.toUpperCase()")
        End Sub
#End Region

#End Region

    End Class
End Namespace
