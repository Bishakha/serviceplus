<%@ Reference Page="~/SSL.aspx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="TopSonyHeader" Src="~/TopSonyHeader.ascx" %>

<%@ Page Language="vb" AutoEventWireup="false" Inherits="ServicePLUSWebApp.RegisterNotification"
    EnableViewStateMac="true" CodeFile="Register-Notifications.aspx.vb" CodeFileBaseClass="ServicePLUSWebApp.SSL" %>

<%@ Register TagPrefix="ServicePLUSWebApp" TagName="SonyFooter" Src="~/SonyFooter.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title><%=Resources.Resource.ttl_Notify%> </title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link href="includes/ServicesPLUS_style.css" type="text/css" rel="stylesheet">

    <script language="javascript" src="includes/ServicesPLUS.js" type="text/javascript"></script>
    <script type="text/javascript" src="../includes/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="../includes/svcplus_globalization.js"></script>


</head>
<body text="#000000" bgcolor="#5d7180" leftmargin="0" topmargin="0" marginheight="0"
    marginwidth="0">
    <form id="form1" method="post" runat="server">
    <center>
        <table width="760" border="0">
            <tr>
                <td width="25" background="../images/sp_left_bkgd.gif">
                    <img height="25" src="../images/spacer.gif" width="25" alt="">
                </td>
                <td width="710" bgcolor="#ffffff">
                    <table width="710" border="0">
                        <tr>
                            <td>
                                <ServicePLUSWebApp:TopSonyHeader ID="TopSonyHeader" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                 <img height="41" src="<%=Resources.Resource.sna_svc_img_50()%>" width="710" alt="ServicesPlus Registration">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table width="710" border="0">
                                    <tr>
                                        <td width="20" height="20">
                                            <img height="20" src="../images/spacer.gif" width="20" alt="">
                                        </td>
                                        <td width="670" height="20">
                                            <img height="20" src="../images/spacer.gif" width="670" alt="">
                                        </td>
                                        <td width="20" height="20">
                                            <img height="20" src="../images/spacer.gif" width="20" alt="">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="20">
                                            <img height="20" src="../images/spacer.gif" width="20" alt="">
                                        </td>
                                        <td width="670">
                                            <table width="670" border="0">
                                                <tr>
                                                    <td>
                                                    </td>
                                                    <td class="bodycopy">
                                                        <asp:CheckBox ID="ProductsAndOffers" runat="server" CssClass="bodyCopy" Text="<%$ Resources:Resource, el_rgn_NotifyAgree%>"
                                                            Checked="True"></asp:CheckBox>
                                                        <br/><br/>
                                                    </td>
                                                    <td>
                                                    </td>
                                                </tr>
                                                <tr bgcolor="#d5dee9">
                                                    <td>
                                                        <img height="15" src="../images/spacer.gif" width="3" alt="">
                                                    </td>
                                                    <td class="tableHeader" colspan="2">
                                                     <span class=bodycopy><%=Resources.Resource.el_rgn_NotifyHdl1%> </span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <img height="30" src="../images/spacer.gif" width="3" alt="">
                                                    </td>
                                                    <td class="bodyCopy" colspan="2">
                                                        <p>
                                                            <img height="7" src="../images/spacer.gif" width="4" alt="">
                                                            <asp:Label ID="ErrorLabel2" runat="server" CssClass="redAsterick"></asp:Label><br/>
                                                             <%=Resources.Resource.el_rgn_NotifyContent1%>
                                                        </p>
                                                        <%--<p>You may add or remove categories at any time by returning to the "My Profile" 
																	page when logged-on.</p>--%>
                                                        <p>
                                                             <%=Resources.Resource.el_rgn_NotifyContent2%>
                                                        </p>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <img height="30" src="../images/spacer.gif" width="3" alt="">
                                                    </td>
                                                    <td class="bodyCopy" colspan="2">
                                                        <asp:Panel ID="TableInterests" runat="server">
                                                        </asp:Panel>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <img height="30" src="../images/spacer.gif" width="3" alt="">
                                                    </td>
                                                    <td class="bodyCopy" colspan="2">
                                                        <img height="7" src="../images/spacer.gif" width="4" alt="">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <img height="30" src="../images/spacer.gif" width="3" alt="">
                                                    </td>
                                                    <td colspan="2">
                                                        <br/>
                                                        &nbsp;
                                                        <asp:ImageButton ID="submitMarketingInterest" runat="server" ImageUrl="<%$Resources:Resource,svc_Ifrm_sub_img%>"
                                                            AlternateText="Submit"></asp:ImageButton>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td width="20">
                                            <img height="20" src="../images/spacer.gif" width="20" alt="">
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <footer><ServicePLUSWebApp:SonyFooter ID="SonyFooter1" runat="server" /></footer>
                            </td>
                        </tr>
                    </table>
                </td>
                <td width="25" background="../images/sp_right_bkgd.gif">
                    <img height="20" src="../images/spacer.gif" width="25" alt="">
                </td>
            </tr>
        </table>
    </center>
    </form>
</body>
</html>
