Imports Sony.US.ServicesPLUS.Process
Imports ServicesPlusException
Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.SIAMUtilities

Namespace ServicePLUSWebApp

    Partial Class ProductRegistrationThankYou
        Inherits SSL

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub


        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Public isColorRow As Boolean = False
        Dim objProductRegistration As New ProductRegistration

#Region "Event"

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                If Session("RegisterProduct") Is Nothing Then
                    Session.Remove("RegisterProduct")
                    Response.Redirect("sony-service-agreements.aspx", True)
                Else
                    objProductRegistration = CType(Session("RegisterProduct"), ProductRegistration)
                    If objProductRegistration Is Nothing Then
                        Session.Remove("RegisterProduct")
                        Response.Redirect("sony-service-agreements.aspx", True)
                    ElseIf objProductRegistration.Completed And Not IsPostBack Then
                        Session.Remove("RegisterProduct")
                        Response.Redirect("sony-service-agreements.aspx", True)
                    ElseIf objProductRegistration.Completed Then
                        imgSubmit.Visible = False
                        imgChangeAgreement.Visible = False
                        imgCalcelAgreement.Visible = False
                        trConfirm.Visible = False
                        trThankYou.Visible = True
                        imgPrint.Visible = True
                    End If
                    bodyDiv.InnerHtml = BuildBody()

                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Protected Sub imgSubmit_Click(ByVal sender As Object, ByVal e As ImageClickEventArgs) Handles imgSubmit.Click
            If Session("RegisterProduct") Is Nothing Then Return

            Dim mailBody As String = String.Empty
            Dim cm As New CustomerManager
            Dim toMailID As String = ""
            Dim strDuplicateCert As String = String.Empty
            Dim enmCertificateStatus As CertificateUseType
            Try
                Try
                    objProductRegistration.HTTP_X_Forward_For = HttpContextManager.GetServerVariableValue("HTTP_X_FORWARDED_FOR")
                    objProductRegistration.RemoteADDR = HttpContextManager.GetServerVariableValue("REMOTE_ADDR")
                    objProductRegistration.HTTPReferer = HttpContextManager.GetServerVariableValue("HTTP_REFERER")
                    objProductRegistration.HTTPUserAgent = HttpContextManager.GetServerVariableValue("HTTP_USER_AGENT")
                    objProductRegistration.HTTPURL = HttpContextManager.GetServerVariableValue("HTTP_URL")

                    If String.IsNullOrWhiteSpace(objProductRegistration.HTTPURL) Then
                        objProductRegistration.HTTPURL = HttpContextManager.GetServerVariableValue("URL") + ReturnQueryString()
                    End If

                    cm.SaveServiceAgreementData(objProductRegistration)
                    objProductRegistration.Completed = True
                Catch exA As ApplicationException
                    If exA.Message = "DUPCERT" Then
                        For Each objProduct As ProductRegistration.Product In objProductRegistration.RegisteredProducts
                            If objProduct.CertificateStatus <> CertificateUseType.Valid_NotInUse And strDuplicateCert = String.Empty Then
                                strDuplicateCert = objProduct.CertificateNumber
                                enmCertificateStatus = objProduct.CertificateStatus
                            Else
                                If objProduct.CertificateNumber = strDuplicateCert Then
                                    objProduct.CertificateStatus = enmCertificateStatus
                                End If
                            End If
                        Next
                        Session.Remove("RegisterProduct")
                        Session.Add("RegisterProduct", objProductRegistration)
                        cm = Nothing
                        Response.Redirect("product-registration.aspx", True)
                        Session.Add("DUPCERT", "Duplicate Service Pack Certificate. Please reenter the Service Pack Certificate")
                    ElseIf exA.Message = "PRODUCTNOTSAVED" Then
                    End If
                Catch exE As Exception
                    Throw exE
                End Try

                If objProductRegistration.Completed = True Then
                    Dim EmailBody As New StringBuilder(5000)
                    Dim Subject As String = String.Empty

                    BuildMailBody(objProductRegistration, EmailBody, Subject)
                    toMailID = objProductRegistration.CustomerAddress.EmailAddress + IIf((objProductRegistration.LocationAddress.EmailAddress.Trim() Is String.Empty) Or objProductRegistration.CustomerAddress.EmailAddress.Trim().ToUpper() = objProductRegistration.LocationAddress.EmailAddress.Trim().ToUpper(), "", ";" + objProductRegistration.LocationAddress.EmailAddress.ToString())

                    cm.SendProductRegistrationMail(EmailBody.ToString(), toMailID, "supportnet@am.sony.com;ServicesPLUS@am.sony.com", Subject)
                    cm = Nothing

                    Session("RegisterProduct") = objProductRegistration

                    imgSubmit.Visible = False
                    imgChangeAgreement.Visible = False 'Added for fixing Bug# 558
                    imgCalcelAgreement.Visible = False 'Added for fixing Bug# 558
                    trConfirm.Visible = False
                    trThankYou.Visible = True

                    imgPrint.Visible = True
                    imgPrint.Attributes.Add("onclick", "javascript:PrintForm();return false;")
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            Finally
                cm = Nothing
            End Try
        End Sub

        Protected Sub imgChangeAgreement_Click(ByVal sender As Object, ByVal e As ImageClickEventArgs) Handles imgChangeAgreement.Click
            Response.Redirect("sony-service-agreements.aspx")
        End Sub

        Protected Sub imgCalcelAgreement_Click(ByVal sender As Object, ByVal e As ImageClickEventArgs) Handles imgCalcelAgreement.Click
            Session.Remove("RegisterProduct")
            Response.Redirect("sony-service-agreements.aspx")
        End Sub

#End Region

#Region "Private Methods"

        Private Function BuildBody() As String
            If objProductRegistration Is Nothing Then Return String.Empty

            Dim sAddr As String = String.Empty
            Dim sb = New StringBuilder()
            Dim isColorProductRow As Boolean = False

            sb.Append("<table cellpadding=""3"" cellspacing=""1"" style=""width: 100%;font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: 666666; border:1px solid #666666;"" role=""presentation"">")

            sb.Append("<tr valign=""top"">")
            sb.Append("<td colspan=""2"" style=""border-bottom-style:solid; border-bottom-width:thin;"">")
            sb.Append("<table border=""0"" cellpadding=""1"" cellspacing=""1"" style=""width: 100%"" role=""presentation"">")
            sb.Append("<tr>")
            'sb.Append("<td colspan=""2"" style=""height: 16px"" align=""center""><strong>Registered Product Detail " + vbCrLf + "</strong></td>")
            sb.Append("<td colspan=""2"" style=""height: 16px"" align=""center""><strong>Service Agreement</strong></td>")
            sb.Append("</tr>")
            sb.Append("<tr>" + vbCrLf)
            sb.Append("<td style=""width: 100px""></td>")
            sb.Append("<td style=""width: 100px""></td>")
            sb.Append("</tr>")
            sb.Append("</table>")
            sb.Append("</td>")
            sb.Append("</tr>")
            sb.Append("<tr valign=""top"">")
            sb.Append("<td style=""width: 173px;"">Date:</td>")
            sb.Append("<td style=""width: 376px;"">" + DateTime.Now.ToString("MMM dd yyyy") + "</td>")
            sb.Append("</tr>")

            sb.Append("<tr valign=""top""  bgColor=""#d5dee9"">" + vbCrLf)
            sb.Append("<td style=""width: 173px;"">Sony Account Manager:</td>")
            sb.Append("<td style=""width: 376px;"">" + objProductRegistration.Dealer + "</td>")
            sb.Append("</tr>")
            isColorRow = False

            ' Build Reseller address
            sAddr = String.Empty
            If Not String.IsNullOrWhiteSpace(objProductRegistration.CustomerAddress.Company) Then sAddr = objProductRegistration.CustomerAddress.Company
            If Not String.IsNullOrWhiteSpace(objProductRegistration.CustomerAddress.FirstName) Then sAddr &= "<br/>" & objProductRegistration.CustomerAddress.FirstName
            If Not String.IsNullOrWhiteSpace(objProductRegistration.CustomerAddress.LastName) Then sAddr &= " " & objProductRegistration.CustomerAddress.LastName
            If Not String.IsNullOrWhiteSpace(objProductRegistration.CustomerAddress.Address1) Then sAddr &= "<br/>" & objProductRegistration.CustomerAddress.Address1
            If Not String.IsNullOrWhiteSpace(objProductRegistration.CustomerAddress.Address2) Then sAddr &= "<br/>" & objProductRegistration.CustomerAddress.Address2
            If Not String.IsNullOrWhiteSpace(objProductRegistration.CustomerAddress.City) Then sAddr &= "<br/>" & objProductRegistration.CustomerAddress.City
            If Not String.IsNullOrWhiteSpace(objProductRegistration.CustomerAddress.State) Then sAddr &= ", " & objProductRegistration.CustomerAddress.State
            If Not String.IsNullOrWhiteSpace(objProductRegistration.CustomerAddress.ZipCode) Then sAddr &= " " & objProductRegistration.CustomerAddress.ZipCode
            If Not String.IsNullOrWhiteSpace(objProductRegistration.CustomerAddress.EmailAddress) Then sAddr &= "<br/><br/>Email: " & objProductRegistration.CustomerAddress.EmailAddress
            If Not String.IsNullOrWhiteSpace(objProductRegistration.CustomerAddress.Phone) Then sAddr &= "<br/><br/>Phone: " & objProductRegistration.CustomerAddress.Phone
            If Not String.IsNullOrWhiteSpace(objProductRegistration.CustomerAddress.PhoneExtn) Then sAddr &= $" ({objProductRegistration.CustomerAddress.PhoneExtn})"
            If Not String.IsNullOrWhiteSpace(objProductRegistration.CustomerAddress.Fax) Then sAddr &= "<br/>Fax: " & objProductRegistration.CustomerAddress.Fax

            sb.Append($"<tr style=""vertical-align: top;{IIf(isColorRow, "background-color:#d5dee9;", "")}"">")
            sb.Append("<td colspan=""2"" class=""bodycopy""><table cellpadding=""3"" cellspacing=""1"" style=""width: 100%; border: none;"" role=""presentation"">")
            sb.Append("<tr style=""font-weight:normal;font-style:normal;text-decoration:none;"" width=""100%"" ><td valign=top width=""50%"" valign=""top"">")
            sb.Append("<u class=""tableHeader"">Reseller Information:</u><br/>" + sAddr + "</td>")
            isColorRow = Not isColorRow

            ' Build End-User address
            sAddr = String.Empty
            If Not String.IsNullOrWhiteSpace(objProductRegistration.LocationAddress.Company) Then sAddr = objProductRegistration.LocationAddress.Company
            If Not String.IsNullOrWhiteSpace(objProductRegistration.LocationAddress.FirstName) Then sAddr &= "<br/>" + objProductRegistration.LocationAddress.FirstName
            If Not String.IsNullOrWhiteSpace(objProductRegistration.LocationAddress.LastName) Then sAddr &= " " + objProductRegistration.LocationAddress.LastName
            If Not String.IsNullOrWhiteSpace(objProductRegistration.LocationAddress.Address1) Then sAddr &= "<br/>" + objProductRegistration.LocationAddress.Address1
            If Not String.IsNullOrWhiteSpace(objProductRegistration.LocationAddress.Address2) Then sAddr &= "<br/>" + objProductRegistration.LocationAddress.Address2
            If Not String.IsNullOrWhiteSpace(objProductRegistration.LocationAddress.City) Then sAddr &= "<br/>" + objProductRegistration.LocationAddress.City
            If Not String.IsNullOrWhiteSpace(objProductRegistration.LocationAddress.State) Then sAddr &= ", " + objProductRegistration.LocationAddress.State
            If Not String.IsNullOrWhiteSpace(objProductRegistration.LocationAddress.ZipCode) Then sAddr &= " " + objProductRegistration.LocationAddress.ZipCode
            If Not String.IsNullOrWhiteSpace(objProductRegistration.LocationAddress.EmailAddress) Then sAddr &= "<br/><br/>Email: " + objProductRegistration.LocationAddress.EmailAddress
            If Not String.IsNullOrWhiteSpace(objProductRegistration.LocationAddress.Phone) Then sAddr &= "<br/><br/>Phone: " + objProductRegistration.LocationAddress.Phone
            If Not String.IsNullOrWhiteSpace(objProductRegistration.LocationAddress.PhoneExtn) Then sAddr &= $" ({objProductRegistration.LocationAddress.PhoneExtn})"
            If Not String.IsNullOrWhiteSpace(objProductRegistration.LocationAddress.Fax) Then sAddr &= "<br/>Fax: " + objProductRegistration.LocationAddress.Fax

            sb.Append("<td valign=top width=""50%"" valign=""top""><u><span class=tableheader>End-User Information:</span></u><br/>")
            sb.Append("<span class=""bodycopy"">" + sAddr + "</span></td></tr></table></td>")

            If HttpContextManager.Customer Is Nothing Then
                If objProductRegistration.OptIN Then
                    sb.Append($"<tr style=""vertical-align: top;{IIf(isColorRow, "background-color:#d5dee9;", "")}"">")
                    sb.Append("<td colspan=""2""><table role=""presentation""><tr><td valign=top><img src=""https://www.servicesplus.sel.sony.com/images/checkbox.jpg"" border=""0"" alt=""Checkmark"" /></td>")
                    sb.Append("<td><span style=""font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: 666666;"">Yes, I would like to receive information from Sony Electronics Inc. about products, services, promotions, contests and offerings that may be of interest to me.</span></td>")
                    sb.Append("</tr></table></td>")
                    sb.Append("</tr>")
                    isColorRow = Not isColorRow

                    sb.Append($"<tr style=""vertical-align: top;{IIf(isColorRow, "background-color:#d5dee9;", "")}"">")
                    sb.Append("<td colspan=""2"">&nbsp;</td>")
                    sb.Append("</tr>")
                    isColorRow = Not isColorRow
                End If
            End If

            sb.Append($"<tr style=""vertical-align: top;{IIf(isColorRow, " background-color:#d5dee9;", "")}"">")
            sb.Append("<td colspan=""2""  style=""width: 173px; vertical-align: top"">Product List</td>")
            sb.Append("</tr>")
            isColorRow = Not isColorRow

            isColorRow = False
            sb.Append("<tr valign=""top"">")
            sb.Append("<td colspan=""2"">")
            sb.Append("<table cellspacing=""0"" class=""bodycopy"" align=""center"" valign=""middle"" cellpadding=""4"" border=""0"" id=""dgProduct"" style=""color:#333333;width:90%;border-collapse:collapse; font-family: Arial, Helvetica, sans-serif; font-size: 11px; "" role=""presentation"">")
            sb.Append("<tr class=""tableHeader"" style=""background-color:#D5DEE9;font-weight:bold;"">")
            sb.Append("<td style=""width:25%;"">Certificate #</td><td style=""width:15%;"">Product Code</td><td style=""width:15%;"">Model Number</td><td style=""width:15%;"">Purchase Date</td><td style=""width:25%;"">Serial Number</td>")
            sb.Append("</tr>")

            For Each objProduct As ProductRegistration.Product In objProductRegistration.RegisteredProducts
                sb.Append($"<tr style=""font-weight:normal; font-style:normal; text-decoration:none;{IIf(isColorProductRow, " background-color:#EFF3FB;", "")}"">" + vbCrLf)
                sb.Append($"<td style=""width:25%;"">{objProduct.CertificateNumber}</td><td style=""width:15%;"">{objProduct.ProductCode}</td><td style=""width:15%;"">
                    {objProduct.ModelNumber}</td><td style=""width:15%;"">{objProduct.PurchaseDate}</td><td style=""width:25%;"">{objProduct.SerialNumber}</td>")
                sb.Append("</tr>")
                isColorProductRow = Not isColorProductRow
            Next
            sb.Append("</table>")
            sb.Append("</td>")
            sb.Append("</tr>")

            sb.Append($"<tr style=""vertical-align: top;{IIf(isColorRow, " background-color:#d5dee9;", "")}"">")
            sb.Append("<td colspan=""2"">&nbsp;</td>")
            sb.Append("</tr>")
            isColorRow = Not isColorRow

            sb.Append("</table>")

            Return sb.ToString()
        End Function

        Sub BuildMailBody(ByVal objProductRegistration As ProductRegistration, ByRef Emailbody As StringBuilder, ByRef Subject As String)
            Dim sAddr As String = String.Empty
            Dim sb As New StringBuilder()
            Dim isColorProductRow As Boolean = False

            Emailbody.Append("<table border=""0"" cellspacing=""0"" cellpadding=""0"" style=""font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: 666666;"" role=""presentation"">")
            Emailbody.Append("<tr>")
            Emailbody.Append("<table style=""width: 93%;font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: 666666;"" role=""presentation"">")
            Emailbody.Append("<tr>")
            Emailbody.Append("<td>")
            Emailbody.Append($"<p>{Resources.Resource_mail.ml_ProductRegistration_body_line1_en} <span style=""font-weight:bold"">877-398-7669</span> {Resources.Resource_mail.ml_ProductRegistration_body_line2_en} <a style=""font-weight:bold"" href=""mailto:supportnet@am.sony.com"">supportnet@am.sony.com</a>.</p>")
            Emailbody.Append($"<p>{Resources.Resource_mail.ml_ProductRegistration_body_line3_en} <span style=""font-weight:bold"">866-766-9272</span>. {Resources.Resource_mail.ml_ProductRegistration_body_line4_en}</p>")
            Emailbody.Append($"<p>{Resources.Resource_mail.ml_ProductRegistration_body_line5_en}</p>")      ' "Sony thanks you for your continued support."
            Emailbody.Append($"<p>{Resources.Resource_mail.ml_ProductRegistration_body_line6_en}<br />")    ' "Sony Professional Services Contract Administration Group"
            Emailbody.Append($"{Resources.Resource_mail.ml_ProductRegistration_body_line7_en}<br />")       ' "Business Solutions & Systems Company"
            Emailbody.Append($"{Resources.Resource_mail.ml_ProductRegistration_body_line8_en} {Resources.Resource_mail.ml_ProductRegistration_body_line9_en}<br />") ' "Sony Electronics Inc."
            Emailbody.Append("<a href=""http://www.sony.com/professionalservices"">http://www.sony.com/professionalservices</a></p>")
            Emailbody.Append("</td>")
            Emailbody.Append("</tr>")
            Emailbody.Append("</table>")
            Emailbody.Append("</tr>")
            Emailbody.Append("<tr>")
            Emailbody.Append("<td align=""center"">")
            Emailbody.Append("<table cellpadding=""3"" cellspacing=""1"" border=""0"" style=""width: 93%;font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: 666666; border:1px solid #666666;"" role=""presentation"">")
            Emailbody.Append("<tr valign=""top"">")
            Emailbody.Append($"<td style=""border-bottom-style:solid; border-bottom-width:thin; border-bottom-color:666666""><img src=""{Resources.Resource_mail.ml_ProductRegistration_body_imglogo_en}"" alt=""Product Registration""/></td>")
            Emailbody.Append("<td style=""border-bottom-style:solid; border-bottom-width:thin; border-bottom-color:666666"" align=""right"" valign=""middle"">&nbsp;</td>")
            Emailbody.Append("</tr>")
            Emailbody.Append("<tr valign=""top"">")
            Emailbody.Append("<td colspan=""2"" style=""border-bottom-style:solid; border-bottom-width:thin;"">")
            Emailbody.Append("<table border=""0"" cellpadding=""1"" cellspacing=""1"" style=""width: 100%"" role=""presentation"">")
            Emailbody.Append("<tr bgColor=""#d5dee9"">")
            Emailbody.Append($"<td style=""height: 16px"" align=""center""><strong>ServicesPLUS - {Resources.Resource_mail.ml_ProductRegistration_body_ServiceAgreement_en}</strong></td>")
            Emailbody.Append("</tr>")
            Emailbody.Append("<tr>")
            Emailbody.Append("<td style=""width: 100px""></td>")
            Emailbody.Append("</tr>")
            Emailbody.Append("</table>")
            Emailbody.Append("</td>")
            Emailbody.Append("</tr>")
            Emailbody.Append("<tr valign=""top"">" + vbCrLf)
            Emailbody.Append("<td style=""width: 173px;"">" + Resources.Resource_mail.ml_ProductRegistration_body_RegistrationDate_en + "</td>")
            Emailbody.Append("<td style=""width: 376px;"">" + DateTime.Now.ToString("MMM dd yyyy") + "</td>")
            Emailbody.Append("</tr>")

            Emailbody.Append("<tr valign=""top""  bgColor=""#d5dee9"">" + vbCrLf)
            Emailbody.Append("<td style=""width: 173px;"">" + Resources.Resource_mail.ml_ProductRegistration_body_PurchasedFrom_en + "</td>")
            Emailbody.Append("<td style=""width: 376px;"">" + objProductRegistration.Dealer + "</td>")
            Emailbody.Append("</tr>")

            ' Build the Customer Address
            sAddr = String.Empty
            If Not String.IsNullOrWhiteSpace(objProductRegistration.CustomerAddress.Company) Then sAddr = objProductRegistration.CustomerAddress.Company
            If Not String.IsNullOrWhiteSpace(objProductRegistration.CustomerAddress.FirstName) Then sAddr &= "<br/>" & objProductRegistration.CustomerAddress.FirstName
            If Not String.IsNullOrWhiteSpace(objProductRegistration.CustomerAddress.LastName) Then sAddr &= " " & objProductRegistration.CustomerAddress.LastName
            If Not String.IsNullOrWhiteSpace(objProductRegistration.CustomerAddress.Address1) Then sAddr &= "<br/>" + objProductRegistration.CustomerAddress.Address1
            If Not String.IsNullOrWhiteSpace(objProductRegistration.CustomerAddress.Address2) Then sAddr &= "<br/>" + objProductRegistration.CustomerAddress.Address2
            If Not String.IsNullOrWhiteSpace(objProductRegistration.CustomerAddress.City) Then sAddr &= "<br/>" + objProductRegistration.CustomerAddress.City
            If Not String.IsNullOrWhiteSpace(objProductRegistration.CustomerAddress.State) Then sAddr &= ", " + objProductRegistration.CustomerAddress.State
            If Not String.IsNullOrWhiteSpace(objProductRegistration.CustomerAddress.ZipCode) Then sAddr &= " " + objProductRegistration.CustomerAddress.ZipCode
            If Not String.IsNullOrWhiteSpace(objProductRegistration.CustomerAddress.EmailAddress) Then sAddr &= $"<br/><br/>{Resources.Resource_mail.ml_ProductRegistration_body_Email_en} {objProductRegistration.CustomerAddress.EmailAddress}"
            If Not String.IsNullOrWhiteSpace(objProductRegistration.CustomerAddress.Phone) Then sAddr &= $"<br/><br/>{Resources.Resource_mail.ml_ProductRegistration_body_phone_en} {objProductRegistration.CustomerAddress.Phone}"
            If Not String.IsNullOrWhiteSpace(objProductRegistration.CustomerAddress.PhoneExtn) Then sAddr &= $" ({objProductRegistration.CustomerAddress.PhoneExtn})"
            If Not String.IsNullOrWhiteSpace(objProductRegistration.CustomerAddress.Fax) Then sAddr &= $"<br/>{Resources.Resource_mail.ml_ProductRegistration_body_fax_en} {objProductRegistration.CustomerAddress.Fax}"

            isColorRow = False
            Emailbody.Append($"<tr style=""vertical-align: top;{IIf(isColorRow, " background-color:#d5dee9;", "")}"">")
            Emailbody.Append("<td colspan=""2"" class=""bodycopy""><table cellpadding=""3"" cellspacing=""1"" style=""width: 100%;font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: 666666; border: none;"" role=""presentation"">")
            Emailbody.Append("<tr style=""width: 100%"" ><td style=""vertical-align: top; width: 50%;"">")
            Emailbody.Append("<u style=""font-weight:bold; font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: 666666;""><br/>Reseller Information:</u><br/>")
            Emailbody.Append("<span style=""font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: 666666;"">" + sAddr + "</span></td>")
            isColorRow = Not isColorRow

            ' Build "Product Location" address
            sAddr = String.Empty
            If Not String.IsNullOrWhiteSpace(objProductRegistration.LocationAddress.Company) Then sAddr = objProductRegistration.LocationAddress.Company
            If Not String.IsNullOrWhiteSpace(objProductRegistration.LocationAddress.FirstName) Then sAddr &= "<br/>" + objProductRegistration.LocationAddress.FirstName
            If Not String.IsNullOrWhiteSpace(objProductRegistration.LocationAddress.LastName) Then sAddr &= " " + objProductRegistration.LocationAddress.LastName
            If Not String.IsNullOrWhiteSpace(objProductRegistration.LocationAddress.Address1) Then sAddr &= "<br/>" + objProductRegistration.LocationAddress.Address1
            If Not String.IsNullOrWhiteSpace(objProductRegistration.LocationAddress.Address2) Then sAddr &= "<br/>" + objProductRegistration.LocationAddress.Address2
            If Not String.IsNullOrWhiteSpace(objProductRegistration.LocationAddress.City) Then sAddr &= "<br/>" + objProductRegistration.LocationAddress.City
            If Not String.IsNullOrWhiteSpace(objProductRegistration.LocationAddress.State) Then sAddr &= ", " + objProductRegistration.LocationAddress.State
            If Not String.IsNullOrWhiteSpace(objProductRegistration.LocationAddress.ZipCode) Then sAddr &= " " + objProductRegistration.LocationAddress.ZipCode
            If Not String.IsNullOrWhiteSpace(objProductRegistration.LocationAddress.EmailAddress) Then sAddr &= $"<br/><br/>{Resources.Resource_mail.ml_ProductRegistration_body_Email_en} {objProductRegistration.LocationAddress.EmailAddress}"
            If Not String.IsNullOrWhiteSpace(objProductRegistration.LocationAddress.Phone) Then sAddr &= $"<br/><br/>{Resources.Resource_mail.ml_ProductRegistration_body_phone_en} {objProductRegistration.LocationAddress.Phone}"
            If Not String.IsNullOrWhiteSpace(objProductRegistration.LocationAddress.PhoneExtn) Then sAddr &= $" ({objProductRegistration.LocationAddress.PhoneExtn})"
            If Not String.IsNullOrWhiteSpace(objProductRegistration.LocationAddress.Fax) Then sAddr &= $"<br/>{Resources.Resource_mail.ml_ProductRegistration_body_fax_en} {objProductRegistration.LocationAddress.Fax}"

            Emailbody.Append("<td style=""vertical-align: top; width: 50%;""><u style=""font-weight:bold; font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: 666666;"">End-User Information:</u><br/>")
            Emailbody.Append("<span style=""font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: 666666;"">" + sAddr + "</span></td></tr></table></td>")

            If HttpContextManager.Customer Is Nothing Then
                ' 2018-06-19 ASleight - The Customer must be logged in to submit this form, so what's the point of this part? Perhaps something that was phased out?
                If objProductRegistration.OptIN Then
                    Emailbody.Append($"<tr style=""vertical-align: top;{IIf(isColorRow, " background-color:#d5dee9;", "")}"">")
                    Emailbody.Append("<td colspan=""2""><table role=""presentation""><tr><td valign=top><img src=""https://www.servicesplus.sel.sony.com/images/checkbox.jpg"" border=""0"" alt=""Checkmark"" /></td>")
                    Emailbody.Append("<td><span style=""font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: 666666;""><br/>" + Resources.Resource_mail.ml_ProductRegistration_body_Promos_en + "</span></td>")
                    Emailbody.Append("</tr></table></td>")
                    Emailbody.Append("</tr>")
                    isColorRow = Not isColorRow

                    Emailbody.Append($"<tr style=""vertical-align: top;{IIf(isColorRow, " background-color:#d5dee9;", "")}"">")
                    Emailbody.Append("<td colspan=""2"">&nbsp;</td>")
                    Emailbody.Append("</tr>")
                    isColorRow = Not isColorRow
                End If
            End If

            Emailbody.Append($"<tr style=""vertical-align: top;{IIf(isColorRow, " background-color:#d5dee9;", "")}"">")
            Emailbody.Append("<td colspan = ""2"" style=""width: 173px;"" valign=""top"">" + Resources.Resource_mail.ml_ProductRegistration_body_ProductList_en + "</td>")
            Emailbody.Append("</tr>")

            isColorRow = False  ' Reset the alternating row colours for the Product List items.
            Emailbody.Append("<tr valign=""top"">")
            Emailbody.Append("<td colspan=""2"">")
            Emailbody.Append("<table cellspacing=""0"" cellpadding=""4"" class=""bodycopy"" style=""vertical-align: middle; text-align: center; color: #333333; width: 90%; border: none; font-family: Arial, Helvetica, sans-serif; font-size: 11px; "" role=""presentation"">")
            Emailbody.Append("<tr class=""tableHeader"" style=""background-color:#D5DEE9;font-weight:bold;"">")
            Emailbody.Append($"<td style=""width:25%;"">{Resources.Resource_mail.ml_ProductRegistration_body_Certificate_en} #</td><td style=""width:15%;"">
                {Resources.Resource_mail.ml_ProductRegistration_body_ProductCode_en}</td><td style=""width:15%;"">{Resources.Resource_mail.ml_ProductRegistration_body_ModelNumber_en}</td>
                <td style=""width:15%;"">{Resources.Resource_mail.ml_ProductRegistration_body_PurchaseDate_en}</td><td style=""width:25%;"">
                {Resources.Resource_mail.ml_ProductRegistration_body_SerialNumber_en}</td>")
            Emailbody.Append("</tr>")

            For Each objProduct As ProductRegistration.Product In objProductRegistration.RegisteredProducts
                Emailbody.Append($"<tr style=""font-weight:normal; font-style:normal; text-decoration:none;{IIf(isColorProductRow, " background-color:#EFF3FB;", "")}"">" + vbCrLf)
                Emailbody.Append($"<td style=""width:25%;"">{objProduct.CertificateNumber}</td><td style=""width:15%;"">{objProduct.ProductCode}</td><td style=""width:15%;"">
                    {objProduct.ModelNumber}</td><td style=""width:15%;"">{objProduct.PurchaseDate}</td><td style=""width:25%;"">{objProduct.SerialNumber}</td>")
                Emailbody.Append("</tr>")
                isColorProductRow = Not isColorProductRow
            Next
            Emailbody.Append("</table>")
            Emailbody.Append("</td>")
            Emailbody.Append("</tr>")

            Emailbody.Append($"<tr style=""vertical-align: top;{IIf(isColorRow, " background-color:#d5dee9;", "")}"">")
            Emailbody.Append("<td colspan=""2"">&nbsp;</td>")
            Emailbody.Append("</tr>")
            isColorRow = Not isColorRow

            Emailbody.Append("</table>")
            Emailbody.Append("</td>")
            Emailbody.Append("</tr>")
            Emailbody.Append("</table>")

            If HttpContextManager.GlobalData.IsCanada Then
                Emailbody.Append("<br/><br/>")
                Emailbody.Append("<table border=""0"" cellspacing=""0"" cellpadding=""0"" style=""font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: 666666;"" role=""presentation"">")
                Emailbody.Append("<tr>")
                Emailbody.Append("<table style=""width: 93%;font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: 666666;"" role=""presentation"">")
                Emailbody.Append("<tr>")
                Emailbody.Append("<td>")
                Emailbody.Append($"<p>{Resources.Resource_mail.ml_ProductRegistration_body_line1_fr} <span style=""font-weight:bold"">877-398-7669</span> {Resources.Resource_mail.ml_ProductRegistration_body_line2_fr} <a style=""font-weight:bold"" href=""mailto:supportnet@am.sony.com"">supportnet@am.sony.com</a>.</p>")
                Emailbody.Append($"<p>{Resources.Resource_mail.ml_ProductRegistration_body_line3_fr} <span style=""font-weight:bold"">866-766-9272</span>. {Resources.Resource_mail.ml_ProductRegistration_body_line4_fr}</p>")
                Emailbody.Append($"<p>{Resources.Resource_mail.ml_ProductRegistration_body_line5_fr}</p>")      ' "Sony thanks you for your continued support."
                Emailbody.Append($"<p>{Resources.Resource_mail.ml_ProductRegistration_body_line6_fr}<br />")    ' "Sony Professional Services Contract Administration Group"
                Emailbody.Append($"{Resources.Resource_mail.ml_ProductRegistration_body_line7_fr}<br />")       ' "Business Solutions & Systems Company"
                Emailbody.Append($"{Resources.Resource_mail.ml_ProductRegistration_body_line8_fr} {Resources.Resource_mail.ml_ProductRegistration_body_line9_fr}<br />") ' "Sony Electronics Inc."
                Emailbody.Append("<a href=""http://www.sony.com/professionalservices"">http://www.sony.com/professionalservices</a></p>")
                Emailbody.Append("<br /><br />")
                Emailbody.Append("</p>")
                Emailbody.Append("</td>")
                Emailbody.Append("</tr>")
                Emailbody.Append("</table>")
                Emailbody.Append("</tr>")
                Emailbody.Append("<tr>")
                Emailbody.Append("<td align=""center"">")
                Emailbody.Append("<table cellpadding=""3"" cellspacing=""1"" border=""0"" style=""width: 93%;font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: 666666; border:1px solid #666666;"" role=""presentation"">")
                Emailbody.Append("<tr valign=""top"">")
                Emailbody.Append($"<td style=""border-bottom-style:solid; border-bottom-width:thin; border-bottom-color:666666""><img src=""{Resources.Resource_mail.ml_ProductRegistration_body_imglogo_fr}"" alt=""Registration du Produit""/></td>")
                Emailbody.Append("<td style=""border-bottom-style:solid; border-bottom-width:thin; border-bottom-color:666666"" align=""right"" valign=""middle"">&nbsp;</td>")
                Emailbody.Append("</tr>")
                Emailbody.Append("<tr valign=""top"">")
                Emailbody.Append("<td colspan=""2"" style=""border-bottom-style:solid; border-bottom-width:thin;"">")
                Emailbody.Append("<table border=""0"" cellpadding=""1"" cellspacing=""1"" style=""width: 100%"" role=""presentation"">")
                Emailbody.Append("<tr bgColor=""#d5dee9"">")
                Emailbody.Append($"<td colspan=""2"" style=""height: 16px"" align=""center""><strong>ServicesPLUS - {Resources.Resource_mail.ml_ProductRegistration_body_ServiceAgreement_fr}</strong></td>")
                Emailbody.Append("</tr>")
                Emailbody.Append("<tr>")
                Emailbody.Append("<td style=""width: 100px""></td>")
                Emailbody.Append("<td style=""width: 100px""></td>")
                Emailbody.Append("</tr>")
                Emailbody.Append("</table>")
                Emailbody.Append("</td>")
                Emailbody.Append("</tr>")
                Emailbody.Append("<tr valign=""top"">")
                Emailbody.Append("<td style=""width: 173px;"">" + Resources.Resource_mail.ml_ProductRegistration_body_RegistrationDate_fr + "</td>")
                Emailbody.Append("<td style=""width: 376px;"">" + DateTime.Now.ToString("dd MMM yyyy") + "</td>")
                Emailbody.Append("</tr>")

                Emailbody.Append("<tr valign=""top""  bgColor=""#d5dee9"">" + vbCrLf)
                Emailbody.Append("<td style=""width: 173px;"">" + Resources.Resource_mail.ml_ProductRegistration_body_PurchasedFrom_fr + "</td>")
                Emailbody.Append("<td style=""width: 376px;"">" + objProductRegistration.Dealer + "</td>")
                Emailbody.Append("</tr>")

                ' Build the Customer Address
                sAddr = String.Empty
                If Not String.IsNullOrWhiteSpace(objProductRegistration.CustomerAddress.Company) Then sAddr = objProductRegistration.CustomerAddress.Company
                If Not String.IsNullOrWhiteSpace(objProductRegistration.CustomerAddress.FirstName) Then sAddr &= "<br/>" + objProductRegistration.CustomerAddress.FirstName
                If Not String.IsNullOrWhiteSpace(objProductRegistration.CustomerAddress.LastName) Then sAddr &= " " + objProductRegistration.CustomerAddress.LastName
                If Not String.IsNullOrWhiteSpace(objProductRegistration.CustomerAddress.Address1) Then sAddr &= "<br/>" + objProductRegistration.CustomerAddress.Address1
                If Not String.IsNullOrWhiteSpace(objProductRegistration.CustomerAddress.Address2) Then sAddr &= "<br/>" + objProductRegistration.CustomerAddress.Address2
                If Not String.IsNullOrWhiteSpace(objProductRegistration.CustomerAddress.City) Then sAddr &= "<br/>" + objProductRegistration.CustomerAddress.City
                If Not String.IsNullOrWhiteSpace(objProductRegistration.CustomerAddress.State) Then sAddr &= ", " + objProductRegistration.CustomerAddress.State
                If Not String.IsNullOrWhiteSpace(objProductRegistration.CustomerAddress.ZipCode) Then sAddr &= " " + objProductRegistration.CustomerAddress.ZipCode
                If Not String.IsNullOrWhiteSpace(objProductRegistration.CustomerAddress.EmailAddress) Then sAddr &= "<br/><br/>" + Resources.Resource_mail.ml_ProductRegistration_body_Email_fr + objProductRegistration.CustomerAddress.EmailAddress
                If Not String.IsNullOrWhiteSpace(objProductRegistration.CustomerAddress.Phone) Then sAddr &= "<br/><br/>" + Resources.Resource_mail.ml_ProductRegistration_body_phone_fr + objProductRegistration.CustomerAddress.Phone
                If Not String.IsNullOrWhiteSpace(objProductRegistration.CustomerAddress.PhoneExtn) Then sAddr &= " (" + objProductRegistration.CustomerAddress.PhoneExtn + ")"
                If Not String.IsNullOrWhiteSpace(objProductRegistration.CustomerAddress.Fax) Then sAddr &= "<br/>" + Resources.Resource_mail.ml_ProductRegistration_body_fax_fr + objProductRegistration.CustomerAddress.Fax

                isColorRow = False
                Emailbody.Append($"<tr style=""vertical-align: top;{IIf(isColorRow, " background-color:#d5dee9;", "")}"">")
                Emailbody.Append("<td colspan=""2"" class=""bodycopy""><table cellpadding=""3"" cellspacing=""1"" style=""width: 100%;font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: 666666; border: none;"" role=""presentation"">")
                Emailbody.Append("<tr style=""width: 100%"" ><td style=""vertical-align: top; width: 50%;"">")
                Emailbody.Append("<u style=""font-weight:bold; font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: 666666;""><br/>" + Resources.Resource_mail.ml_ProductRegistration_body_customerinformation_en + "</u><br/>")
                Emailbody.Append("<span style=""font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: 666666;"">" + sAddr + "</span></td>")
                isColorRow = Not isColorRow

                ' Set up "Product Location" address
                If Not String.IsNullOrWhiteSpace(objProductRegistration.LocationAddress.Company) Then sAddr = objProductRegistration.LocationAddress.Company
                If Not String.IsNullOrWhiteSpace(objProductRegistration.LocationAddress.FirstName) Then sAddr &= "<br/>" + objProductRegistration.LocationAddress.FirstName
                If Not String.IsNullOrWhiteSpace(objProductRegistration.LocationAddress.LastName) Then sAddr &= " " + objProductRegistration.LocationAddress.LastName
                If Not String.IsNullOrWhiteSpace(objProductRegistration.LocationAddress.Address1) Then sAddr &= "<br/>" + objProductRegistration.LocationAddress.Address1
                If Not String.IsNullOrWhiteSpace(objProductRegistration.LocationAddress.Address2) Then sAddr &= "<br/>" + objProductRegistration.LocationAddress.Address2
                If Not String.IsNullOrWhiteSpace(objProductRegistration.LocationAddress.City) Then sAddr &= "<br/>" + objProductRegistration.LocationAddress.City
                If Not String.IsNullOrWhiteSpace(objProductRegistration.LocationAddress.State) Then sAddr &= ", " + objProductRegistration.LocationAddress.State
                If Not String.IsNullOrWhiteSpace(objProductRegistration.LocationAddress.ZipCode) Then sAddr &= " " + objProductRegistration.LocationAddress.ZipCode
                If Not String.IsNullOrWhiteSpace(objProductRegistration.LocationAddress.EmailAddress) Then sAddr &= $"<br/><br/>{Resources.Resource_mail.ml_ProductRegistration_body_Email_fr} {objProductRegistration.LocationAddress.EmailAddress}"
                If Not String.IsNullOrWhiteSpace(objProductRegistration.LocationAddress.Phone) Then sAddr &= $"<br/><br/>{Resources.Resource_mail.ml_ProductRegistration_body_phone_fr} {objProductRegistration.LocationAddress.Phone}"
                If Not String.IsNullOrWhiteSpace(objProductRegistration.LocationAddress.PhoneExtn) Then sAddr &= $" ({objProductRegistration.LocationAddress.PhoneExtn})"
                If Not String.IsNullOrWhiteSpace(objProductRegistration.LocationAddress.Fax) Then sAddr &= $"<br/>{Resources.Resource_mail.ml_ProductRegistration_body_fax_fr} {objProductRegistration.LocationAddress.Fax}"

                Emailbody.Append(vbCrLf + "<td valign=top width=""50%"" valign=""top""><u><span style=""font-weight:bold; font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: 666666;"">" + "<br/>" + Resources.Resource_mail.ml_ProductRegistration_body_ProductLocation_fr + "</span></u><br/>" + vbCrLf + "<span style=""font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: 666666;"">" + sAddr + "</span></td></tr></table></td>")

                If HttpContextManager.Customer Is Nothing Then
                    ' 2018-06-19 ASleight - The Customer must be logged in to submit this form, so what's the point of this part? Perhaps something that was phased out?
                    If objProductRegistration.OptIN Then
                        Emailbody.Append($"<tr style=""vertical-align: top;{IIf(isColorRow, " background-color:#d5dee9;", "")}"">")
                        Emailbody.Append("<td colspan=""2""><table role=""presentation""><tr><td valign=top><img src=""https://www.servicesplus.sel.sony.com/images/checkbox.jpg"" border=""0"" alt=""Checkmark"" /></td>")
                        Emailbody.Append("<td><span style=""font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: 666666;""><br/>" + Resources.Resource_mail.ml_ProductRegistration_body_Promos_fr + "</span></td>")
                        Emailbody.Append("</tr></table></td>")
                        Emailbody.Append("</tr>")
                        isColorRow = Not isColorRow

                        Emailbody.Append($"<tr style=""vertical-align: top;{IIf(isColorRow, " background-color:#d5dee9;", "")}"">")
                        Emailbody.Append("<td colspan=""2"">&nbsp;</td>")
                        Emailbody.Append("</tr>")
                        isColorRow = Not isColorRow
                    End If
                End If

                Emailbody.Append($"<tr style=""vertical-align: top;{IIf(isColorRow, " background-color:#d5dee9;", "")}"">")
                Emailbody.Append("<td colspan = ""2"" style=""width: 173px;"" valign=""top"">" + Resources.Resource_mail.ml_ProductRegistration_body_ProductList_en + "</td>")
                Emailbody.Append("</tr>")

                isColorRow = False  ' Reset the alternating row colours for the Product List items.
                Emailbody.Append("<tr valign=""top"">")
                Emailbody.Append("<td colspan=""2"">")
                Emailbody.Append("<table cellspacing=""0"" class=""bodycopy"" align=""center"" valign=""middle"" cellpadding=""4"" border=""0"" id=""dgProduct"" style=""color:#333333;width:90%;border-collapse:collapse; font-family: Arial, Helvetica, sans-serif; font-size: 11px; "" role=""presentation"">")
                Emailbody.Append("<tr class=""tableHeader"" style=""background-color:#D5DEE9;font-weight:bold;"">")
                Emailbody.Append($"<td style=""width:25%;"">{Resources.Resource_mail.ml_ProductRegistration_body_Certificate_fr} #</td><td style=""width:15%;"">
                    {Resources.Resource_mail.ml_ProductRegistration_body_ProductCode_fr}</td><td style=""width:15%;"">{Resources.Resource_mail.ml_ProductRegistration_body_ModelNumber_fr}</td>
                    <td style=""width:15%;"">{Resources.Resource_mail.ml_ProductRegistration_body_PurchaseDate_fr}</td><td style=""width:25%;"">
                    {Resources.Resource_mail.ml_ProductRegistration_body_SerialNumber_fr}</td>")
                Emailbody.Append("</tr>")

                For Each objProduct As ProductRegistration.Product In objProductRegistration.RegisteredProducts
                    Emailbody.Append($"<tr style=""font-weight:normal; font-style:normal; text-decoration:none;{IIf(isColorProductRow, " background-color:#EFF3FB;", "")}"">" + vbCrLf)
                    Emailbody.Append($"<td style=""width:25%;"">{objProduct.CertificateNumber}</td><td style=""width:15%;"">{objProduct.ProductCode}</td><td style=""width:15%;"">{objProduct.ModelNumber}</td><td style=""width:15%;"">{objProduct.PurchaseDate}</td><td style=""width:25%;"">{objProduct.SerialNumber}</td>")
                    Emailbody.Append("</tr>")
                    isColorProductRow = Not isColorProductRow
                Next
                Emailbody.Append("</table>")
                Emailbody.Append("</td>")
                Emailbody.Append("</tr>")

                Emailbody.Append($"<tr style=""vertical-align: top;{IIf(isColorRow, " background-color:#d5dee9;", "")}"">")
                Emailbody.Append("<td colspan=""2"">&nbsp;</td>")
                Emailbody.Append("</tr>")
                isColorRow = Not isColorRow

                Emailbody.Append("</table>")
                Emailbody.Append("</td>")
                Emailbody.Append("</tr>")
                Emailbody.Append("</table>")
            End If

            If ConfigurationData.GeneralSettings.Environment.ToUpper() <> "PRODUCTION" Then
                Subject = $"{Resources.Resource_mail.ml_ProductRegistration_subject_en} ({ConfigurationData.GeneralSettings.Environment})"
                If HttpContextManager.GlobalData.IsCanada Then Subject &= $" / {Resources.Resource_mail.ml_ProductRegistration_subject_fr} ({ConfigurationData.GeneralSettings.Environment})"
            Else
                Subject = Resources.Resource_mail.ml_ProductRegistration_subject_en
                If HttpContextManager.GlobalData.IsCanada Then Subject &= $" / {Resources.Resource_mail.ml_ProductRegistration_subject_fr}"
            End If
        End Sub

#End Region

    End Class

End Namespace
