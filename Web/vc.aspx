<%@ Reference Page="~/SSL.aspx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="ServicePlusNav" Src="ServicePlusNav.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="SonyFooter" Src="SonyFooter.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="Message" Src="PersonalMessage.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="TopSonyHeader" Src="TopSonyHeader.ascx" %>

<%@ Page Language="vb" AutoEventWireup="false" Inherits="ServicePLUSWebApp.vc" EnableViewStateMac="true"
    CodeFile="vc.aspx.vb" CodeFileBaseClass="ServicePLUSWebApp.SSL" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="<%=PageLanguage %>">
<head>
    <title>
        <%=Resources.Resource.ttl_ViewCart%>
    </title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <meta http-equiv="X-UA-Compatible" content="IE=5" />
    <link href="includes/ServicesPLUS_style.css" type="text/css" rel="stylesheet">

    <script src="includes/ServicesPLUS.js" type="text/javascript"></script>
    <script src="includes/jquery-1.4.2.min.js" type="text/javascript"></script>
    <script src="includes/svcplus_globalization.js" type="text/javascript"></script>

</head>
<body style="margin: 0px; background: #5d7180; color: Black;" onkeydown="javascript:SetTheFocusButton(event, 'CheckOutButton');">
    <form id="viewcart" method="post" runat="server">
        <center>
            <table width="760" border="0" role="presentation">
                <tr>
                    <td width="25" style="background: url('images/sp_left_bkgd.gif')">
                        <img height="25" src="images/spacer.gif" width="25" alt="">
                    </td>
                    <td width="710" bgcolor="#ffffff">
                        <table width="710" border="0" role="presentation">
                            <tr>
                                <td>
                                    <ServicePLUSWebApp:TopSonyHeader ID="TopSonyHeader" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <nav><ServicePLUSWebApp:ServicePlusNav ID="SonyNav" runat="server" /></nav>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="710" border="0" role="presentation">
                                        <tr>
                                            <td width="464" bgcolor="#363d45" height="57" background="images/sp_int_header_top_ServicesPLUS_onepix.gif"
                                                align="right" valign="top">
                                                <br>
                                                <h1 class="headerText">ServicesPLUS &nbsp;&nbsp;</h1>
                                            </td>
                                            <td valign="middle" bgcolor="#363d45">
                                                <ServicePLUSWebApp:Message ID="Message1" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td bgcolor="#f2f5f8">
                                                <h2 id="lblSubHdr" class="headerTitle" style="padding-right: 20px; text-align: right;" runat="server"></h2>
                                            </td>
                                            <td bgcolor="#99a8b5">
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr height="9">
                                            <td bgcolor="#f2f5f8">
                                                <img height="9" src="images/sp_int_header_btm_left_onepix.gif" width="464" alt="">
                                            </td>
                                            <td bgcolor="#99a8b5">
                                                <img height="9" src="images/sp_int_header_btm_right.gif" width="246" alt="">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="710" border="0" role="presentation">
                                        <tr>
                                            <td width="20" height="21">
                                                <img height="20" src="images/spacer.gif" width="20" alt="">
                                            </td>
                                            <td width="670" height="21">
                                                <asp:Label ID="CustomerLabel" runat="server" CssClass="redAsterick"></asp:Label>
                                            </td>
                                            <td width="20" height="21">
                                                <img height="20" src="images/spacer.gif" width="20" alt="">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="710" border="0" role="presentation">
                                        <tr>
                                            <td width="20" height="21">
                                                <img height="20" src="images/spacer.gif" width="20" alt="">
                                            </td>
                                            <td width="670" height="21">
                                                <asp:Label ID="MessageLabel" runat="server" CssClass="bodyCopy"></asp:Label>
                                            </td>
                                            <td width="20" height="21">
                                                <img height="20" src="images/spacer.gif" width="20" alt="">
                                            </td>
                                        </tr>
                                        <tr id="trCart" runat="server">
                                            <td width="20">
                                                <img height="20" src="images/spacer.gif" width="20" alt="">
                                            </td>
                                            <td width="670">
                                                <br>
                                                <asp:Table ID="tblCart" runat="server" Width="670px" />
                                                <table width="400" border="0" role="presentation">
                                                    <tr>
                                                        <td width="93">
                                                            <asp:ImageButton ID="SaveCartButton" runat="server" ImageUrl="<%$Resources:Resource,img_btnSaveCart%>"
                                                                AlternateText="Save Cart"></asp:ImageButton>
                                                        </td>
                                                        <td>
                                                            <img height="4" src="images/spacer.gif" width="10" alt="">
                                                        </td>
                                                        <td>
                                                            <asp:ImageButton ID="PunchOut" runat="server" Visible="false" ImageUrl="<%$Resources:Resource,img_btnSubmit%>"
                                                                AlternateText="Submit cart contents to your purchasing system." /><asp:ImageButton
                                                                    ID="CheckOutButton" runat="server" ImageUrl="<%$Resources:Resource,img_btnCheckOut%>"
                                                                    AlternateText="Checkout" />
                                                        </td>
                                                        <td>
                                                            <img height="4" src="images/spacer.gif" width="10" alt="">
                                                        </td>
                                                        <td>
                                                            <asp:ImageButton ID="UpdateButton" runat="server" ImageUrl="<%$Resources:Resource,img_btnUpdateCart%>"
                                                                AlternateText="Update Cart" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="5" id="tdLegend" runat="server">
                                                            <span style="font-family: Wingdings; color: red; font-size: 12px;">v</span><span class="bodyCopy"> &nbsp;<a
                                                                href="parts-promotions.aspx"><%=Resources.Resource.el_LimitedPromoPrice%></a></span>
                                                        </td>
                                                        <td runat="server">
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td width="20">
                                                <img height="20" src="images/spacer.gif" width="20" alt="">
                                            </td>
                                        </tr>
                                        <%-- ASleight - This Label doesn't seem to ever get used. Some methods toggle visibility or set it blank, but nobody gives it any text.
                                    <tr>
                                        <td width="20">
                                            <img height="20" src="images/spacer.gif" width="20">
                                        </td>
                                        <td width="670" height="20">
                                            <asp:Label ID="lblMessageText" runat="server" CssClass="redAsterick"></asp:Label>
                                        </td>
                                        <td width="20">
                                            <img height="20" src="images/spacer.gif" width="20">
                                        </td>
                                    </tr>--%>
                                        <%--8091 starts--%>
                                        <tr>
                                            <td width="20">
                                                <img height="20" src="images/spacer.gif" width="20" alt="">
                                            </td>
                                            <td width="670" height="20" id="tdshowkitinfo" runat="server">
                                                <span class="tableData"><span color="red">*</span>&nbsp;<span color="black"><%=Resources.Resource.el_KitInfo%>
                                                </span></span>
                                            </td>
                                            <td width="20">
                                                <img height="20" src="images/spacer.gif" width="20" alt="">
                                            </td>
                                        </tr>
                                        <%--8091 ends--%>
                                        <tr>
                                            <td width="20">
                                                <img height="20" src="images/spacer.gif" width="20" alt="">
                                            </td>
                                            <%--<td colspan="5"  width="670" height="21" >
													        <asp:Label ID="lblNote" Runat="server" CssClass="bodyCopy">This subtotal does not include shipping, tax and 
															any applicable recycling fees.</asp:Label></td>--%><%--7814--%>
                                            <td width="670" height="21">
                                                <asp:Label ID="lblNote" runat="server" CssClass="bodyCopy" Text="<%$ Resources:Resource, el_SubtotalInfo%>"></asp:Label>
                                            </td>
                                            <td width="20">
                                                <img height="20" src="images/spacer.gif" width="20" alt="">
                                            </td>
                                        </tr>
                                        <tr>
                                            <%--7814--%>
                                            <td width="20">
                                                <img height="20" src="images/spacer.gif" width="20" alt="">
                                            </td>
                                            <td id="tdrecyclefee" runat="server">
                                                <span class="bodyCopy">&nbsp;
                                                <%=Resources.Resource.el_Cart_Recycle1%>
                                                    <span color="red">*</span>
                                                    <%=Resources.Resource.el_Cart_Recycle2%>
                                                &nbsp;<a href="http://www.sony.com/recyclemodels" target='_blank'>http://www.sony.com/recyclemodels.</a></span>
                                            </td>
                                            <td width="20">
                                                <img height="20" src="images/spacer.gif" width="20" alt="">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <%--<td width="20"><IMG height="20" src="images/spacer.gif" width="20"></td>--%>
                            </tr>
                            <tr>
                                <td width="700">
                                    <footer><ServicePLUSWebApp:SonyFooter ID="SonyFooter1" runat="server" /></footer>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="25" style="background: url('images/sp_right_bkgd.gif')">
                        <img src="images/spacer.gif" width="25" height="20" alt="">
                    </td>
                </tr>
            </table>
        </center>
    </form>
</body>
</html>
