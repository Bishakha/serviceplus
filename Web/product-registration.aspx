<%@ Page Language="vb" AutoEventWireup="false" SmartNavigation="true"
    Inherits="ServicePLUSWebApp.ProductRegistrationAddSerial" EnableViewStateMac="true"
    CodeFile="product-registration.aspx.vb" CodeFileBaseClass="ServicePLUSWebApp.SSL" %>

<%@ Register TagPrefix="ServicePLUSWebApp" TagName="SonyFooter" Src="SonyFooter.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="TopSonyHeader" Src="TopSonyHeader.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="ServicePlusNav" Src="ServicePlusNav.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="<%=PageLanguage %>">
<head>
    <title>Sony Service Agreements for Professional Products</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <meta http-equiv="x-ua-compatible" content="IE=5" />
    <link href="includes/ServicesPLUS_style.css" type="text/css" rel="stylesheet" />

    <link href="themes/base/jquery.ui.all.css" rel="stylesheet" type="text/css" />
    <link href="includes/jquery.cluetip.css" rel="stylesheet" type="text/css" />
    <script src="includes/jquery-1.4.2.min.js" type="text/javascript"></script>
    <script src="includes/jquery.ui.core.js" type="text/javascript"></script>
    <script src="includes/jquery.ui.widget.js" type="text/javascript"></script>
    <script src="includes/jquery.cluetip.js" type="text/javascript"></script>
    <script src="includes/jquery.ui.mouse.js" type="text/javascript"></script>
    <script src="includes/jquery.ui.draggable.js" type="text/javascript"></script>
    <script src="includes/jquery.ui.position.js" type="text/javascript"></script>
    <script src="includes/jquery.ui.resizable.js" type="text/javascript"></script>
    <script src="includes/jquery.ui.dialog.js" type="text/javascript"></script>
    <script src="includes/jquery-ui-1.8.1.custom.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="includes/jquery-1.10.2.min.js"></script>
    <script language="javascript" src="includes/ServicesPLUS.js" type="text/javascript"></script>
    <script type="text/javascript" src="includes/svcplus_globalization.js"></script>

    <script type="text/javascript"> 
        function toUpper(a) {
            if (event.keyCode >= 97 && event.keyCode <= 122) {
                event.keyCode = event.keyCode - 32;
            }
        }

        function ClosePopUp() {
            $('#dialog').dialog('close');
        }
        function close() {
            var NewModelNo = $('#hdnModelNumber').val();
            $("#ddlModelNumber option[value='" + NewModelNo + "']").attr("selected", "selected");
            $('#ddlModelNumber').change();
        }

        $(function () {
            $('#dialog').append(' <br/>');
            $('#dialog').dialog({
                title: '<span class="modalpopup-title">' + '<%= Resources.Resource.el_Pts_ModelRenamed%>' + ' </span>',
                modal: true,
                height: 250,
                width: 560,
                position: 'top',
                autoOpen: false,
                close: function () {
                    close();
                }
            });
        });

        function ModelPopup() {
            var ModelNo = $('#ddlModelNumber').val();
            var NewModelNo = $('#hdnModelNumber').val();
            if (ModelNo === NewModelNo) {
                return true;
            }

            jQuery.ajax({
                type: 'POST',
                contentType: 'application/json; charset=utf-8',
                data: "{ 'ModelNo': '" + ModelNo + "' }",
                dataType: 'json',
                url: 'product-registration.aspx/CheckConflicts',
                success: function (result) {
                    if (jQuery.trim(result.d).length > 0) {
                        $('#dialog').text('');
                        var NewModelNo = result.d;
                        $('#hdnModelNumber').val(NewModelNo);
                        var content = "<span class='modalpopup-content'>The model " + ModelNo + '<%= Resources.Resource.el_ModelPopUp_Msg1%>' + NewModelNo + '<%= Resources.Resource.el_ModelPopUp_Msg2%>' + NewModelNo + "<br/>"
                        content +='<%= Resources.Resource.el_ModelPopUp_Msg3%>';
                        $('#dialog').append(content); //message to display in divmessage div
                        $('#dialog').append('<br/> <br/><a><img src ="<%=Resources.Resource.img_btnCloseWindow()%>f" alt ="Close" onclick="javascript:return ClosePopUp();" id="messagego" /></a>');
                        $('#dialog').dialog('open');
                        return false;
                    }
                }
            });
            return true;
        }
    </script>
</head>
<body style="color: #000000; background: #5d7180; margin: 0px;">
    <center>
        <form id="form1" method="post" runat="server">
            <div id="dialog" style="display: block;"></div>
            <asp:HiddenField ID="hdnModelNumber" runat="server" />
            <table style="width: 710px; text-align: center; background: #ffffff;" role="presentation">
                <tr>
                    <td>
                        <table width="710" border="0" role="presentation">
                            <tr>
                                <td>
                                    <ServicePLUSWebApp:TopSonyHeader ID="TopSonyHeader" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <nav><ServicePLUSWebApp:ServicePlusNav ID="SonyNav" runat="server" /></nav>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 708px; vertical-align: middle;">
                                    <table style="width: 710px;" role="presentation">
                                        <tr style="height: 82px;">
                                            <td style="width: 464px; text-align: right; vertical-align: top; background: #363d45 url('images/sp_int_header_top_PartsPLUS-ServiceAgreements.gif');">
                                                <br />
                                                <h1 class="headerText">Service Agreements&nbsp;</h1>
                                            </td>
                                            <td style="width: 246px; background: #363d45;">
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="background: #f2f5f8">
                                                <img src="images/spacer.gif" width="16" alt="">
                                            </td>
                                            <td style="background: #99a8b5">
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr style="height: 9px;">
                                            <td style="background: #f2f5f8">
                                                <img style="height: 9px; width: 464px;" src="images/sp_int_header_btm_left_onepix.gif" alt="">
                                            </td>
                                            <td style="background: #99a8b5">
                                                <img style="height: 9px; width: 246px;" src="images/sp_int_header_btm_right.gif" alt="">
                                            </td>
                                        </tr>
                                    </table>
                                    <img src="images/spacer.gif" width="16" alt="">
                                    <asp:Label ID="ErrorLabel" runat="server" Text="" CssClass="redAsterick" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table border="0" role="presentation">
                                        <tr>
                                            <td width="17" height="15">
                                                <img height="15" src="images/spacer.gif" width="20" alt="">
                                            </td>
                                            <td width="1" height="18">
                                                <img height="15" src="images/spacer.gif" width="3" alt="">
                                            </td>
                                            <td height="18">
                                                <span class="tableHeader"><%=Resources.Resource.el_ProductReg_Cnt1%>
                                                <span class="redAsterick">&nbsp;*</span><%=Resources.Resource.el_ProductReg_Cnt2%> </span>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="710" border="0" role="presentation">
                                        <tr style="height: 15px;">
                                            <td colspan="3">
                                                <img height="15" src="images/spacer.gif" width="710" alt="">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 20px;">
                                                <img src="images/spacer.gif" width="20" alt="">
                                            </td>
                                            <td style="width: 670px;">
                                                <table width="670" border="0" role="presentation">
                                                    <tr bgcolor="#d5dee9">
                                                        <td width="3" height="18">
                                                            <img height="15" src="images/spacer.gif" width="3" alt=""></td>
                                                        <td colspan="1" height="18"><span class="tableHeader"><%=Resources.Resource.el_EP_AddUrProduct%> </span></td>
                                                        <td colspan="1" height="18" align="right"></td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3">
                                                            <asp:Label ID="ErrorValidation" runat="server" CssClass="redAsterick" EnableViewState="False"></asp:Label>&nbsp;
                                                        </td>
                                                    </tr>
                                                    <tr bgcolor="#f2f5f8">
                                                        <td>
                                                            <img height="25" src="images/spacer.gif" width="3" alt=""></td>
                                                        <td class="bodyCopy" align="right" width="150">
                                                            <asp:Label ID="Label6" runat="server" CssClass="bodyCopy"><%=Resources.Resource.el_ProductCategory%>:</asp:Label>&nbsp;
                                                        </td>
                                                        <td>
                                                            &nbsp;
															<asp:DropDownList ID="ddlProductCategory" CssClass="bodycopy" runat="server" Width="60%" AutoPostBack="True"></asp:DropDownList>
                                                            <span class="redAsterick">*</span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <img height="25" src="images/spacer.gif" width="3" alt=""></td>
                                                        <td class="bodyCopy" align="right" width="150">
                                                            <asp:Label ID="Label2" runat="server" CssClass="bodyCopy"><%=Resources.Resource.el_ProductCode%></asp:Label>&nbsp;
                                                        </td>
                                                        <td>
                                                            &nbsp;
															<asp:DropDownList ID="ddlProductCode" CssClass="bodycopy" runat="server" Width="60%" AutoPostBack="True"></asp:DropDownList>
                                                            <span class="redAsterick">*</span>
                                                        </td>
                                                    </tr>
                                                    <tr bgcolor="#f2f5f8">
                                                        <td>
                                                            <img height="25" src="images/spacer.gif" width="3" alt=""></td>
                                                        <td class="bodyCopy" align="right" width="150">
                                                            <asp:Label ID="Label4" runat="server" CssClass="bodyCopy"><%=Resources.Resource.el_ModelNumber%>:</asp:Label>&nbsp;</td>
                                                        <td>
                                                            &nbsp;
															<asp:DropDownList ID="ddlModelNumber" CssClass="bodycopy" runat="server" Width="60%" AutoPostBack="False"></asp:DropDownList>
                                                            <span class="redAsterick">*</span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <img height="25" src="images/spacer.gif" width="3" alt=""></td>
                                                        <td class="bodyCopy" align="right" width="150">
                                                            <asp:Label ID="Label5" runat="server" CssClass="bodyCopy"><%=Resources.Resource.el_PurchaseDate%></asp:Label>&nbsp;</td>
                                                        <td>
                                                            &nbsp;
															<SPS:SPSTextBox ID="txtDate" runat="server" Width="30%" CssClass="bodyCopy" MaxLength="10"></SPS:SPSTextBox><span class="redAsterick">*</span><span class="bodycopy">&nbsp;&nbsp;MM/DD/YYYY</span>

                                                        </td>
                                                    </tr>
                                                    <tr bgcolor="#f2f5f8">
                                                        <td>
                                                            <img height="25" src="images/spacer.gif" width="3" alt=""></td>
                                                        <td class="bodyCopy" align="right" width="150">
                                                            <asp:Label ID="Label3" runat="server" CssClass="bodyCopy"><%=Resources.Resource.el_SerialNo%></asp:Label>&nbsp;</td>
                                                        <td>
                                                            &nbsp;
															<SPS:SPSTextBox ID="txtSerialNumber" runat="server" Width="45%" CssClass="bodyCopy" MaxLength="50"></SPS:SPSTextBox>
                                                            <span class="redAsterick">*</span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <img height="25" src="images/spacer.gif" width="3" alt=""></td>
                                                        <td class="bodyCopy" align="right" width="150">
                                                            <asp:Label ID="lblDealer" runat="server" CssClass="bodyCopy"><%=Resources.Resource.el_ServicePackCertificate%></asp:Label>&nbsp;</td>
                                                        <td align="left" width="550">
                                                            &nbsp;
															<SPS:SPSTextBox ID="txtCertificate" runat="server" Width="45%" CssClass="bodyCopy" MaxLength="13"></SPS:SPSTextBox>
                                                            <span class="redAsterick">*</span>
                                                        </td>
                                                    </tr>
                                                    <tr bgcolor="#f2f5f8">
                                                        <td width="3" colspan="3" height="20">
                                                            <img height="1" src="images/spacer.gif" width="3" alt=""></td>
                                                    </tr>

                                                    <tr>
                                                        <td>
                                                            <img height="25" src="images/spacer.gif" width="3" alt=""></td>
                                                        <td class="bodyCopy" align="right" width="150">&nbsp;</td>
                                                        <td align="left">
                                                            &nbsp;
															<asp:ImageButton ID="btnSaveAndAdd" runat="server" ImageUrl="<%$Resources:Resource,sna_svc_img_18%>" AlternateText="Add product to products list below"></asp:ImageButton>
                                                            &nbsp;&nbsp;
                                                        </td>
                                                    </tr>
                                                </table>
                                                <br />
                                            </td>
                                            <td style="width: 20px;">
                                                <img src="images/spacer.gif" width="20" alt="">
                                            </td>
                                        </tr>

                                        <tr style="height: 18px;">
                                            <td>
                                                <img src="images/spacer.gif" width="20" alt="">
                                            </td>
                                            <td style="padding-left: 3px; padding-right: 3px; background:#d5dee9;">
                                                <span class="tableHeader"><%=Resources.Resource.el_ProductList%></span>
                                            </td>
                                            <td>
                                                <img src="images/spacer.gif" width="20" alt="">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <img src="images/spacer.gif" width="20" alt="">
                                            </td>
                                            <td>
                                                <asp:Label ID="lblProductError" runat="server" CssClass="redAsterick"></asp:Label>
                                            </td>
                                            <td>
                                                <img src="images/spacer.gif" width="20" alt="">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <img src="images/spacer.gif" width="20" alt="">
                                            </td>
                                            <td>
                                                <table width="97%" border="0" align="center" role="presentation">
                                                    <tr>
                                                        <td width="3" height="18">
                                                            <img height="15" src="images/spacer.gif" width="3" alt=""></td>
                                                        <td valign="middle" height="18">
                                                            <asp:DataGrid ID="dgProduct" runat="server" AutoGenerateColumns="False" CellPadding="4" ForeColor="#333333" GridLines="None" Width="100%">
                                                                <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                                                <EditItemStyle BackColor="#2461BF" />
                                                                <SelectedItemStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" Font-Italic="False" Font-Overline="False" Font-Strikeout="False" Font-Underline="False" />
                                                                <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                                                <AlternatingItemStyle BackColor="White" Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False" Font-Underline="False" />
                                                                <ItemStyle BackColor="#EFF3FB" Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False" Font-Underline="False" />
                                                                <HeaderStyle BackColor="#D5DEE9" Font-Bold="True" CssClass="tableHeader" />
                                                                <Columns>
                                                                    <asp:BoundColumn DataField="CertificateNumber" HeaderText="Certificate #">
                                                                        <ItemStyle Font-Bold="False" CssClass="bodyCopy" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                            Font-Underline="False" HorizontalAlign="Left" VerticalAlign="Middle" />
                                                                        <HeaderStyle Width="25%" />
                                                                    </asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="ProductCode" HeaderText="Product Code">
                                                                        <ItemStyle Font-Bold="False" CssClass="bodyCopy" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                            Font-Underline="False" HorizontalAlign="Left" VerticalAlign="Middle" />
                                                                        <HeaderStyle Width="15%" />
                                                                    </asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="ModelNumber" HeaderText="Model Number">
                                                                        <ItemStyle Font-Bold="False" CssClass="bodyCopy" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                            Font-Underline="False" HorizontalAlign="Left" VerticalAlign="Middle" />
                                                                        <HeaderStyle Width="15%" />
                                                                    </asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="PurchaseDate" HeaderText="Purchase Date">
                                                                        <ItemStyle Font-Bold="False" CssClass="bodyCopy" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                            Font-Underline="False" HorizontalAlign="Left" VerticalAlign="Middle" />
                                                                        <HeaderStyle Width="15%" />
                                                                    </asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="SerialNumber" HeaderText="Serial Number">
                                                                        <ItemStyle Font-Bold="False" CssClass="bodyCopy" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                            Font-Underline="False" HorizontalAlign="Left" VerticalAlign="Middle" />
                                                                        <HeaderStyle Width="25%" />
                                                                    </asp:BoundColumn>
                                                                    <asp:TemplateColumn HeaderText="Remove">
                                                                        <HeaderStyle Width="5%" Font-Bold="true" Font-Italic="False" Font-Overline="False" Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                        <ItemTemplate>
                                                                            <asp:ImageButton ID="imgDeleteProduct" CommandName="Delete" runat="server" ImageUrl="Images/Delete.gif" AlternateText="Remove this product" />
                                                                        </ItemTemplate>
                                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                            Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                    </asp:TemplateColumn>
                                                                    <asp:BoundColumn DataField="ItemID" HeaderText="ItemId" Visible="false"></asp:BoundColumn>
                                                                </Columns>
                                                            </asp:DataGrid>
                                                        </td>
                                                    </tr>
                                                    <%--<tr >
														<td width="3" height="18"><img height="15" src="images/spacer.gif" width="3"></td>
											            <td width="670" height="30" align=right valign=middle>
											                <asp:ImageButton ID="imgRefreshProductList" runat=server ImageUrl="~/images/sp_refreshprolist.gif" AlternateText="Refresh Product List" />
                                                                    
												        </td>
												    </tr>--%>
                                                    <tr>
                                                        <td width="3" height="18">
                                                            <img height="15" src="images/spacer.gif" width="3" alt=""></td>
                                                        <td valign="middle" colspan="1" height="18">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="3" height="18">
                                                            <img height="15" src="images/spacer.gif" width="3" alt=""></td>
                                                        <td valign="middle" colspan="1" height="18">
                                                            <span class="bodycopy"><%=Resources.Resource.el_ProdReg_msg1%>  <a href="mailto:supportnet@am.sony.com">supportnet@am.sony.com</a></span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td width="3" height="18">
                                                            <img height="15" src="images/spacer.gif" width="3" alt=""></td>
                                                        <td width="670" height="30" align="center" valign="middle">
                                                            <input type="checkbox" runat="server" id="chkTC"><span class="bodycopy"><%=Resources.Resource.el_ProdReg_msg2%>
                                                            <a href="http://www.sony.com/professionalservices/warranty" style="color: Blue"
                                                                onclick="window.open('http://www.sony.com/professionalservices/warranty');return false;"><%=Resources.Resource.el_TermsConditions%>
                                                            </a></span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td width="3" height="18">
                                                            <img height="15" src="images/spacer.gif" width="3" alt=""></td>
                                                        <td width="670" height="30" align="right" valign="middle">
                                                            <table width="100%" role="presentation">
                                                                <tr>
                                                                    <td valign="top" align="left" class="tableHeader">
                                                                        <a href="sony-service-agreements.aspx" class="tableHeader">Go Back</a>
                                                                    </td>
                                                                    <td align="right">
                                                                        <asp:ImageButton ID="imgNext" ImageUrl="images/sp_int_next_btn.gif" runat="server" AlternateText="Next" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td>
                                                <img src="images/spacer.gif" width="20" alt="">
                                            </td>
                                        </tr>
                                        <tr style="height: 18px;">
                                            <td colspan="3"><img src="images/spacer.gif" height="18" alt=""></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <footer><ServicePLUSWebApp:SonyFooter ID="SonyFooter1" runat="server" /></footer>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <input type="hidden" runat="server" id="hdnNavigator" />
        </form>
    </center>
</body>
</html>
