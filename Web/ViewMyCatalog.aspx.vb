'===========================================================================
' This file was modified as part of an ASP.NET 2.0 Web project conversion.
' The class name was changed and the class modified to inherit from the abstract base class 
' in file 'App_Code\Migrated\Stub_ViewMyCatalog_aspx_vb.vb'.
' During runtime, this allows other classes in your web application to bind and access 
' the code-behind page using the abstract base class.
' The associated content page 'ViewMyCatalog.aspx' was also modified to refer to the new class name.
' For more information on this code pattern, please refer to http://go.microsoft.com/fwlink/?LinkId=46995 
'===========================================================================
Imports ServicesPlusException
Imports Sony.US.AuditLog
Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Process
Imports Sony.US.SIAMUtilities

Namespace ServicePLUSWebApp
    Partial Class Migrated_ViewMyCatalog
        Inherits ViewMyCatalog

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Protected WithEvents RemindMe As System.Web.UI.WebControls.Button
        Protected WithEvents btnPreviousPage2 As System.Web.UI.WebControls.ImageButton
        Protected WithEvents PreviousLinkButton2 As System.Web.UI.WebControls.LinkButton
        Protected WithEvents lblCurrentPage2 As System.Web.UI.WebControls.Label
        Protected WithEvents lblEndingPage2 As System.Web.UI.WebControls.Label
        Protected WithEvents NextLinkButton2 As System.Web.UI.WebControls.LinkButton
        Protected WithEvents btnNextPage2 As System.Web.UI.WebControls.ImageButton
        Protected WithEvents txtPageNumber2 As Sony.US.ServicesPLUS.Controls.SPSTextBox
        Protected WithEvents btnGoToPage2 As System.Web.UI.WebControls.ImageButton
        'Dim objGlobalData As New GlobalData



        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.

            isProtectedPage(True)
            InitializeComponent()
            Page.ID = Request.Url.AbsolutePath.ToString()
            txtPageNumber.Attributes("onkeydown") = "SetTheFocusButton(event)"
        End Sub

#End Region

#Region "page members"

        Private Const conReminderPage As String = "ViewMyCatalogReminder.aspx"
        Private Const conOrderCheckBoxPrefix As String = "Order"
        Private Const conDeleteReminderCheckBoxPrefix As String = "Delete_Rem"
        Private Const conDeleteCatalogCheckBoxPrefix As String = "Delete_Cat"
        Private Const conMaxResultsToDisplay As Integer = 50

        'pagination variables
        Private currentResultPage As Integer = 1
        Private endingResultPage As Integer = 1
        Private startingResultItem As Integer = 1
        Private resultPageRequested As Integer = 3
        Private totalSearchResults As Integer = 0

        Private dropDownListSortingOptions() As String = {"Model Number", "Part Number", "Date Added"}

        Private sortOrder As CustomerCatalogItem.enumSortOrder

        '-- pagination result page
        Private Enum enumResultPageRequested
            PreviousPage = 0
            NextPage = 1
            UserEnteredPage = 2
            FirstPage = 3
            LastPage = 4
        End Enum
        ''Dim objUtilties As Utilities = New Utilities
#End Region

#Region "events"
        Public prg_PleaseWait As String
        Public atc_AddingItem As String
        Public atc_ToCart As String
        Public atc_ex_DifferentCart As String
        Public atc_title_QuickOrderExceptions As String
        Public atc_ex_InvalidPartNumber As String
        Public atc_ex_InvalidQuantity As String
        Public el_item As String
        Public el_Pts_OrderQty As String
        Public atc_title_QuickOrderStatus As String
        Public atc_title_OrderStatus As String
        Public atc_title_OrderExceptions As String


        Private Sub fnGetResourceData()
            prg_PleaseWait = Resources.Resource.prg_PleaseWait
            atc_AddingItem = Resources.Resource.atc_AddingItem
            atc_ToCart = Resources.Resource.atc_ToCart
            atc_ex_DifferentCart = Resources.Resource.atc_ex_DifferentCart
            atc_title_QuickOrderExceptions = Resources.Resource.atc_title_QuickOrderExceptions
            atc_ex_InvalidPartNumber = Resources.Resource.atc_ex_InvalidPartNumber
            atc_ex_InvalidQuantity = Resources.Resource.atc_ex_InvalidQuantity
            el_Pts_OrderQty = Resources.Resource.el_Pts_OrderQty

            el_item = Resources.Resource.el_item
            atc_title_QuickOrderStatus = Resources.Resource.atc_title_QuickOrderStatus
            atc_title_OrderStatus = Resources.Resource.atc_title_OrderStatus
            atc_title_OrderExceptions = Resources.Resource.atc_title_OrderExceptions
        End Sub

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

            'If Not Session.Item("GlobalData") Is Nothing Then
            '    objGlobalData = Session.Item("GlobalData")
            'End If

            'Put user code to initialize the page here
            formFieldInitialization()
            fnGetResourceData()
            If Not IsPostBack Then ControlMethod(sender, e)
        End Sub

        Private Sub AllImage_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles SortCatalogButton.Click, btnNextPage.Click, btnPreviousPage.Click, btnGoToPage.Click, UpdateMyCatalogButton.Click, AddToCartButton.Click
            ControlMethod(sender, e)
        End Sub

        Private Sub AllLink_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PreviousLinkButton.Click, NextLinkButton.Click
            ControlMethod(sender, e)
        End Sub

#End Region

#Region "methods"
        Private Sub ControlMethod(ByVal sender As System.Object, ByVal e As System.EventArgs)
            Try
                If HttpContextManager.Customer IsNot Nothing Then
                    Dim thisCustomer As Customer = HttpContextManager.Customer
                    Dim thisUsersCatalogItems() As CustomerCatalogItem = getUsersCatalog(thisCustomer, HttpContextManager.GlobalData)
                    Dim caller As String = ""

                    If Not sender.id Is Nothing Then caller = sender.id.ToString()

                    Select Case caller
                        Case Page.ID.ToString()
                            '-- first time page is loaded
                            resultPageRequested = enumResultPageRequested.FirstPage
                            sortOrder = setTheSortOrder()
                        Case AddToCartButton.ID.ToString()
                            '-- add items to cart
                            hidelistbox.Items.Clear()
                            getCatalogItemsToAddToShoppingCart(thisCustomer, thisUsersCatalogItems) '6994
                        Case UpdateMyCatalogButton.ID.ToString()
                            '-- remove items from catalog
                            updateCatalog(thisCustomer, thisUsersCatalogItems, HttpContextManager.GlobalData) 'Sasikumar WP SPLUS_WP007
                        Case SortCatalogButton.ID.ToString()
                            '-- sort catalog items                        
                            sortOrder = setTheSortOrder()
                        Case btnPreviousPage.ID.ToString(), PreviousLinkButton.ID.ToString()
                            resultPageRequested = enumResultPageRequested.PreviousPage
                        Case btnNextPage.ID.ToString(), NextLinkButton.ID.ToString()
                            resultPageRequested = enumResultPageRequested.NextPage
                        Case btnGoToPage.ID.ToString
                            resultPageRequested = enumResultPageRequested.UserEnteredPage
                    End Select

                    BuildPage(thisCustomer, thisUsersCatalogItems)
                    updateResultPageNav()

                End If
            Catch ex As Exception
                errorMessageLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Sub BuildPage(ByRef thisCustomer As Customer, ByRef thisUsersCatalogItems() As CustomerCatalogItem)
            Try
                Dim totalRemindersDisplayed As Integer = 0
                Dim rowCount As Integer = 0

                sortCatalogItems(thisUsersCatalogItems)

                totalRemindersDisplayed = BuildMyReminderDisplayTable(thisUsersCatalogItems)
                BuildMyCatalogDisplay(thisUsersCatalogItems, totalRemindersDisplayed, conMaxResultsToDisplay)

                If thisUsersCatalogItems.Length = 0 Then
                    buttonStatus("off")
                Else
                    buttonStatus("on")
                    If Not IsPostBack Then BuildCatalogSortDDL()
                End If
            Catch ex As Exception
                errorMessageLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

#Region "build display tables"
        Private Function BuildMyReminderDisplayTable(ByVal thisUsersCatalogItems() As CustomerCatalogItem) As Integer
            Dim totalRemindersDisplayed As Integer = 0
            Try
                If thisUsersCatalogItems IsNot Nothing Then
                    Dim tr As New TableRow
                    Dim td As New TableCell
                    Dim thisOrderCB As CheckBox
                    Dim thisDeleteCB As CheckBox
                    Dim haveReminders As Boolean = False

                    '-- build column headers                 
                    td.Controls.Add(New LiteralControl(vbCrLf + "<table width=""670"" border=""0"" cellspacing=""0"" cellpadding=""0"" role=""presentation"">" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<tr>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""#b7b7b7"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""35"" alt=""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl($"<td width=""80"" class=""tableHeader"" align=""center""><label id=""lblOrder"">{Resources.Resource.el_Order}</label></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""35"" alt=""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl($"<td width=""90"" class=""tableHeader"">&nbsp;&nbsp;&nbsp;{Resources.Resource.el_ModelNumber}</td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""35"" alt=""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl($"<td width=""90"" class=""tableHeader"" align=""center"">{Resources.Resource.el_itemNumber}</td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""35"" alt=""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl($"<td width=""255"" class=""tableHeader"">&nbsp;&nbsp;&nbsp;{Resources.Resource.el_Description}</td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""35"" alt=""></td>" + vbCrLf))

                    'price difference for CA and US 
                    If HttpContextManager.GlobalData.IsCanada Then
                        td.Controls.Add(New LiteralControl("<td width=""90"" class=""tableHeader"" align=""center"">" + Resources.Resource.el_Price_CAD + "</td>" + vbCrLf))
                    Else
                        td.Controls.Add(New LiteralControl("<td width=""90"" class=""tableHeader"" align=""center"">" + Resources.Resource.el_Price + "</td>" + vbCrLf))
                    End If

                    td.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""35"" alt=""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl($"<td width=""63"" class=""tableHeader"" align=""center""><label id=""lblDelete"">{Resources.Resource.el_Delete}</label></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""#b7b7b7"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""35"" alt=""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("</tr>" + vbCrLf))

                    For Each thisCatalogItem As CustomerCatalogItem In thisUsersCatalogItems
                        If thisCatalogItem.ReminderToday Then
                            totalRemindersDisplayed += 1
                            thisOrderCB = New CheckBox
                            thisDeleteCB = New CheckBox

                            thisOrderCB.ID = conOrderCheckBoxPrefix + thisCatalogItem.SequenceNumber.ToString()
                            thisOrderCB.Checked = True
                            thisOrderCB.Attributes.Add("aria-labelledby", "lblOrder")   ' 2019-01-18 Added for WCAG Remediation

                            thisDeleteCB.ID = conDeleteReminderCheckBoxPrefix + thisCatalogItem.SequenceNumber.ToString()
                            thisDeleteCB.Checked = False
                            thisDeleteCB.Attributes.Add("aria-labelledby", "lblDelete") ' 2019-01-18 Added for WCAG Remediation

                            If Not haveReminders Then haveReminders = True
                            td.Controls.Add(New LiteralControl("<tr>" + vbCrLf))
                            td.Controls.Add(New LiteralControl("<td bgcolor=""#b7b7b7"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""></td>" + vbCrLf))
                            td.Controls.Add(New LiteralControl("<td bgcolor=""#f2f5f8"" width=""80"" class=""tableData"" align=""center"">"))
                            '-- order item checkbox
                            td.Controls.Add(thisOrderCB)
                            td.Controls.Add(New LiteralControl("</td>" + vbCrLf))
                            td.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""></td>" + vbCrLf))
                            td.Controls.Add(New LiteralControl("<td bgcolor=""#f2f5f8"" width=""90"" class=""tableData"">&nbsp;&nbsp;&nbsp;"))
                            '-- model number
                            td.Controls.Add(New LiteralControl(thisCatalogItem.ModelNumber.ToString()))
                            td.Controls.Add(New LiteralControl("</td>" + vbCrLf))
                            td.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""></td>" + vbCrLf))
                            td.Controls.Add(New LiteralControl("<td bgcolor=""#f2f5f8"" width=""90"" class=""tableData"" align=""center"">"))
                            '-- item number (part number)
                            td.Controls.Add(New LiteralControl(thisCatalogItem.Product.PartNumber.ToString()))
                            td.Controls.Add(New LiteralControl("</td>" + vbCrLf))
                            td.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""></td>" + vbCrLf))
                            td.Controls.Add(New LiteralControl("<td bgcolor=""#f2f5f8"" width=""255"" class=""tableData"">&nbsp;&nbsp;&nbsp;"))
                            '-- product description
                            td.Controls.Add(New LiteralControl(thisCatalogItem.Product.Description.ToString()))
                            td.Controls.Add(New LiteralControl("&nbsp;&nbsp;&nbsp;</td>" + vbCrLf))
                            td.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""></td>" + vbCrLf))
                            td.Controls.Add(New LiteralControl("<td bgcolor=""#f2f5f8"" width=""255"" class=""tableData"">&nbsp;&nbsp;&nbsp;"))
                            '-- list price
                            td.Controls.Add(New LiteralControl("$" + Format(thisCatalogItem.Product.ListPrice, "##0.00")))
                            td.Controls.Add(New LiteralControl("&nbsp;&nbsp;&nbsp;</td>" + vbCrLf))
                            td.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""></td>" + vbCrLf))
                            td.Controls.Add(New LiteralControl("<td bgcolor=""#f2f5f8"" width=""63"" class=""tableHeader"" align=""center"">"))
                            '-- delete item checkbox
                            td.Controls.Add(thisDeleteCB)
                            td.Controls.Add(New LiteralControl("</td>" + vbCrLf))
                            td.Controls.Add(New LiteralControl("<td bgcolor=""#b7b7b7"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""></td>" + vbCrLf))
                            td.Controls.Add(New LiteralControl("</tr>" + vbCrLf))
                        End If
                    Next

                    If Not haveReminders Then
                        td.Controls.Add(New LiteralControl("<tr>" + vbCrLf))
                        td.Controls.Add(New LiteralControl("<td bgcolor=""#b7b7b7"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""></td>" + vbCrLf))
                        td.Controls.Add(New LiteralControl("<td bgcolor=""#f2f5f8"" class=""tableData"" align=""center"" colspan=""11"">No reminder for today</td>" + vbCrLf))
                        td.Controls.Add(New LiteralControl("<td bgcolor=""#b7b7b7"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""></td>" + vbCrLf))
                        td.Controls.Add(New LiteralControl("</tr>" + vbCrLf))
                    End If

                    td.Controls.Add(New LiteralControl("</table>" + vbCrLf))


                    tr.Controls.Add(td)
                    MyReminderTable.Controls.Add(tr)
                End If
            Catch ex As Exception
                errorMessageLabel.Text = Utilities.WrapExceptionforUI(ex)

            End Try

            Return totalRemindersDisplayed
        End Function


        Private Sub BuildMyCatalogDisplay(ByVal thisUsersCatalogItems() As CustomerCatalogItem, ByVal totalRemindersDisplayed As Integer, ByVal maxResultsToDisplay As Integer)
            ' ASleight - An exception occurs within this method for part number ECM77BC (replaced by ECM77BC/9X)
            Try
                If Not thisUsersCatalogItems Is Nothing Then
                    Dim tr As New TableRow
                    Dim td As New TableCell
                    Dim thisOrderCB As CheckBox
                    Dim thisDeleteCB As CheckBox
                    Dim haveCatalogItem As Boolean = False
                    Dim displayCount As Integer = totalRemindersDisplayed + 1
                    Dim displayItemArrayList As New ArrayList
                    Dim rowCount As Integer = 0
                    Dim currentResultItem As Integer = 1
                    Try

                        td.Controls.Add(New LiteralControl(vbCrLf + "<table width=""670"" border=""0"" cellspacing=""0"" cellpadding=""0"" role=""presentation"">" + vbCrLf))

                        td.Controls.Add(New LiteralControl("<tr>" + vbCrLf))
                        td.Controls.Add(New LiteralControl("<td width=""1"" bgcolor=""#d5dee9""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""></td>" + vbCrLf))
                        td.Controls.Add(New LiteralControl("<td height=""1"" bgcolor=""#d5dee9""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""></td>" + vbCrLf))
                        td.Controls.Add(New LiteralControl("<td height=""1"" bgcolor=""#d5dee9""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""></td>" + vbCrLf))
                        td.Controls.Add(New LiteralControl("<td height=""1"" bgcolor=""#d5dee9""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""></td>" + vbCrLf))
                        td.Controls.Add(New LiteralControl("<td height=""1"" bgcolor=""#d5dee9""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""></td>" + vbCrLf))
                        td.Controls.Add(New LiteralControl("<td height=""1"" bgcolor=""#d5dee9""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""></td>" + vbCrLf))
                        td.Controls.Add(New LiteralControl("<td height=""1"" bgcolor=""#d5dee9""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""></td>" + vbCrLf))
                        td.Controls.Add(New LiteralControl("<td height=""1"" bgcolor=""#d5dee9""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""></td>" + vbCrLf))
                        td.Controls.Add(New LiteralControl("<td height=""1"" bgcolor=""#d5dee9""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""></td>" + vbCrLf))
                        td.Controls.Add(New LiteralControl("<td height=""1"" bgcolor=""#d5dee9""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""></td>" + vbCrLf))
                        td.Controls.Add(New LiteralControl("<td height=""1"" bgcolor=""#d5dee9""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""></td>" + vbCrLf))
                        td.Controls.Add(New LiteralControl("<td height=""1"" bgcolor=""#d5dee9""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""></td>" + vbCrLf))
                        '--
                        td.Controls.Add(New LiteralControl("<td height=""1"" bgcolor=""#d5dee9""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""></td>" + vbCrLf))
                        td.Controls.Add(New LiteralControl("<td height=""1"" bgcolor=""#d5dee9""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""></td>" + vbCrLf))
                        '--
                        td.Controls.Add(New LiteralControl("<td height=""1"" bgcolor=""#d5dee9""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""></td>" + vbCrLf))
                        td.Controls.Add(New LiteralControl("<td height=""1"" bgcolor=""#d5dee9""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""></td>" + vbCrLf))
                        td.Controls.Add(New LiteralControl("<td width=""1"" bgcolor=""#d5dee9""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""></td>" + vbCrLf))
                        td.Controls.Add(New LiteralControl("</tr>" + vbCrLf))


                        td.Controls.Add(New LiteralControl("<tr>" + vbCrLf))
                        td.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""35"" alt=""></td>" + vbCrLf))
                        '- order
                        td.Controls.Add(New LiteralControl("<td width=""80"" class=""tableHeader"" align=""center"" bgcolor=""#d5dee9"">Order</td>" + vbCrLf))
                        td.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""35"" alt=""></td>" + vbCrLf))
                        '- model
                        td.Controls.Add(New LiteralControl("<td width=""90"" class=""tableHeader"" align=""center"" bgcolor=""#d5dee9"">Model Number</td>" + vbCrLf))
                        td.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""35"" alt=""></td>" + vbCrLf))
                        '- part 
                        td.Controls.Add(New LiteralControl("<td width=""90"" class=""tableHeader"" align=""center"" bgcolor=""#d5dee9"">Part Number</td>" + vbCrLf))
                        td.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""35"" alt=""></td>" + vbCrLf))
                        '- desc
                        td.Controls.Add(New LiteralControl("<td width=""230"" class=""tableHeader"" align=""center"" bgcolor=""#d5dee9"">&nbsp;&nbsp;&nbsp;Description</td>" + vbCrLf))
                        td.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""35"" alt=""></td>" + vbCrLf))
                        '- date added
                        td.Controls.Add(New LiteralControl("<td width=""50"" class=""tableHeader"" bgcolor=""#d5dee9"">&nbsp;&nbsp;&nbsp;&nbsp;Date<BR>&nbsp;&nbsp;&nbsp;Added</td>" + vbCrLf))
                        td.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""35"" alt=""></td>" + vbCrLf))
                        '-- Add Reminder
                        td.Controls.Add(New LiteralControl("<td width=""25"" class=""tableHeader"" align=""center"" bgcolor=""#d5dee9"">&nbsp;</td>" + vbCrLf))
                        td.Controls.Add(New LiteralControl("<td width=""1"" class=""tableHeader"" align=""center"" bgcolor=""#d5dee9""><img src=""images/spacer.gif"" width=""1"" height=""35"" alt=""></td>" + vbCrLf))
                        '-- price
                        td.Controls.Add(New LiteralControl("<td width=""90"" class=""tableHeader"" align=""center"" bgcolor=""#d5dee9"">Price</td>" + vbCrLf))
                        td.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""35"" alt=""></td>" + vbCrLf))
                        '- delete
                        td.Controls.Add(New LiteralControl("<td width=""63"" class=""tableHeader"" align=""center"" bgcolor=""#d5dee9"">Delete</td>" + vbCrLf))
                        td.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""35"" alt=""></td>" + vbCrLf))
                        td.Controls.Add(New LiteralControl("</tr>" + vbCrLf))

                        totalSearchResults = thisUsersCatalogItems.Length
                        calcStaringResultItem(resultPageRequested)
                    Catch ex As Exception
                        errorMessageLabel.Text = Utilities.WrapExceptionforUI(New Exception("Error while building Catalog header row. ", ex))
                    End Try

                    For Each thisCatalog As CustomerCatalogItem In thisUsersCatalogItems
                        If Not thisCatalog.ReminderToday And currentResultItem >= startingResultItem Then
                            haveCatalogItem = True
                            thisOrderCB = New CheckBox With {
                                    .ID = conOrderCheckBoxPrefix + thisCatalog.SequenceNumber.ToString(),
                                    .Checked = False
                                }

                            thisDeleteCB = New CheckBox With {
                                .ID = conDeleteCatalogCheckBoxPrefix + thisCatalog.SequenceNumber.ToString(),
                                .Checked = False
                            }

                            td.Controls.Add(New LiteralControl("<tr>" + vbCrLf))
                            td.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""></td>" + vbCrLf))
                            '-- order item checkbox
                            td.Controls.Add(New LiteralControl("<td bgcolor=""#f2f5f8"" width=""80"" class=""tableData"" align=""center"">"))
                            td.Controls.Add(thisOrderCB)
                            td.Controls.Add(New LiteralControl("</td>" + vbCrLf))
                            td.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""></td>" + vbCrLf))
                            '-- model number
                            td.Controls.Add(New LiteralControl($"<td bgcolor=""#f2f5f8"" width=""90"" class=""tableData"">&nbsp;&nbsp;{thisCatalog.ModelNumber}</td>" + vbCrLf))
                            td.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""></td>" + vbCrLf))
                            '-- item number (part number)
                            td.Controls.Add(New LiteralControl("<td bgcolor=""#f2f5f8"" width=""90"" class=""tableData"" align=""center"">"))
                            If (Not String.IsNullOrEmpty(thisCatalog.Product.PartNumber)) Then
                                td.Controls.Add(New LiteralControl(thisCatalog.Product.PartNumber))
                            ElseIf (Not String.IsNullOrEmpty(thisCatalog.Product.ReplacementPartNumber)) Then
                                td.Controls.Add(New LiteralControl(thisCatalog.Product.ReplacementPartNumber))
                            End If
                            td.Controls.Add(New LiteralControl("</td>" + vbCrLf))
                            td.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""></td>" + vbCrLf))
                            '-- item description
                            td.Controls.Add(New LiteralControl($"<td bgcolor=""#f2f5f8"" width=""230"" class=""tableData"">&nbsp;&nbsp;{thisCatalog.Product.Description}&nbsp;&nbsp;&nbsp;</td>" + vbCrLf))
                            td.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""></td>" + vbCrLf))
                            '-- date added
                            td.Controls.Add(New LiteralControl($"<td bgcolor=""#f2f5f8"" width=""25"" class=""tableData"" align=""center"">&nbsp;{thisCatalog.DateAdded.ToString("MM/dd/yyyy")}&nbsp;&nbsp;&nbsp;</td>" + vbCrLf))
                            td.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""></td>" + vbCrLf))
                            '-- reminder 
                            td.Controls.Add(New LiteralControl("<td bgcolor=""#f2f5f8"" width=""25"" class=""tableData"" align=""center"">"))
                            td.Controls.Add(New LiteralControl("<a href=""#"" onclick=""mywin=window.open('" + conReminderPage + "?seq=" + rowCount.ToString() + "','','toolbar=1,location=1,directories=1,status=1,menubar=1,scrollbars=1,resize=1,width=426,height=380');""><img src=""images/sp_int_remindMe_icon.gif"" border=""0"" alt=""Add Reminder""></a>"))
                            td.Controls.Add(New LiteralControl("</td>" + vbCrLf))
                            td.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9"" width=""1"" class=""tableData"" align=""right""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""></td>" + vbCrLf))
                            '-- list price 
                            td.Controls.Add(New LiteralControl("<td bgcolor=""#f2f5f8"" width=""90"" class=""tableData"" align=""right"">"))
                            If (thisCatalog.Product.ListPrice > 0.0R) Then
                                td.Controls.Add(New LiteralControl("$" + Format(thisCatalog.Product.ListPrice, "##0.00")))
                            Else
                                td.Controls.Add(New LiteralControl("Price Exception"))
                            End If
                            td.Controls.Add(New LiteralControl("&nbsp;&nbsp;&nbsp;</td>" + vbCrLf))
                            td.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""></td>" + vbCrLf))
                            '-- delete item checkbox
                            td.Controls.Add(New LiteralControl("<td bgcolor=""#f2f5f8"" width=""63"" class=""tableHeader"" align=""center"">"))
                            td.Controls.Add(thisDeleteCB)
                            td.Controls.Add(New LiteralControl("</td>" + vbCrLf))
                            td.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""></td>" + vbCrLf))
                            td.Controls.Add(New LiteralControl("</tr>" + vbCrLf))

                            displayItemArrayList.Add(thisCatalog)
                            rowCount += 1
                            displayCount += 1
                            If displayCount > maxResultsToDisplay Then Exit For
                        End If
                        currentResultItem += 1
                    Next

                    '-- create session variable to pass to reminder page if items are in the array 
                    If displayItemArrayList.Count > 0 Then
                        Session.Item("CatalogItems") = displayItemArrayList
                    End If

                    If Not haveCatalogItem Then
                        td.Controls.Add(New LiteralControl("<tr>" + vbCrLf))
                        td.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""35"" alt=""></td>" + vbCrLf))
                        td.Controls.Add(New LiteralControl("<td class=""tableData"" align=""center"" colspan=""15"" align=""center"">No catalogs saved.</td>" + vbCrLf))
                        td.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""35"" alt=""></td>" + vbCrLf))
                        td.Controls.Add(New LiteralControl("</tr>" + vbCrLf))
                    End If
                    td.Controls.Add(New LiteralControl("<tr>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td width=""1"" bgcolor=""#d5dee9""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td height=""1"" bgcolor=""#d5dee9""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td height=""1"" bgcolor=""#d5dee9""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td height=""1"" bgcolor=""#d5dee9""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td height=""1"" bgcolor=""#d5dee9""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td height=""1"" bgcolor=""#d5dee9""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td height=""1"" bgcolor=""#d5dee9""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td height=""1"" bgcolor=""#d5dee9""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td height=""1"" bgcolor=""#d5dee9""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td height=""1"" bgcolor=""#d5dee9""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td height=""1"" bgcolor=""#d5dee9""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td height=""1"" bgcolor=""#d5dee9""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td height=""1"" bgcolor=""#d5dee9""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td height=""1"" bgcolor=""#d5dee9""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td height=""1"" bgcolor=""#d5dee9""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td height=""1"" bgcolor=""#d5dee9""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td width=""1"" bgcolor=""#d5dee9""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("</tr>" + vbCrLf))

                    td.Controls.Add(New LiteralControl("</table>" + vbCrLf))
                    tr.Controls.Add(td)
                    MyCatalogTable.Controls.Add(tr)
                End If
            Catch ex As Exception
                errorMessageLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub
#End Region

#Region "buildDrop down menues"

        Private Sub BuildCatalogSortDDL()
            Try
                SortCatalog.DataSource = dropDownListSortingOptions
                SortCatalog.DataBind()
            Catch ex As Exception
                errorMessageLabel.Text = Utilities.WrapExceptionforUI(ex)

            End Try
        End Sub

#End Region

#Region "form objects status"
        Private Sub buttonStatus(ByVal thisStatus As String)
            Select Case thisStatus.ToLower.ToString()
                Case "on"
                    AddToCartButton.Visible = True
                    UpdateMyCatalogButton.Visible = True
                    SortCatalog.Enabled = True
                    SortCatalogButton.Enabled = True
                    btnPreviousPage.Visible = True
                    btnNextPage.Visible = True
                    btnGoToPage.Visible = True
                    PreviousLinkButton.Visible = True
                    NextLinkButton.Visible = True
                    txtPageNumber.Visible = True
                    lblCurrentPage.Visible = True
                    lblEndingPage.Visible = True

                Case "off"
                    AddToCartButton.Visible = False
                    UpdateMyCatalogButton.Visible = False
                    SortCatalog.Enabled = False
                    SortCatalogButton.Enabled = False
                Case Else
                    AddToCartButton.Visible = True
                    UpdateMyCatalogButton.Visible = True
                    SortCatalog.Enabled = True
                    SortCatalogButton.Enabled = True
                    btnPreviousPage.Visible = True
                    btnNextPage.Visible = True
                    btnGoToPage.Visible = True
                    PreviousLinkButton.Visible = True
                    NextLinkButton.Visible = True
                    txtPageNumber.Visible = True
                    lblCurrentPage.Visible = True
                    lblEndingPage.Visible = True
            End Select
        End Sub

#End Region

#Region "Pagination"
        Private Sub calcCurrentResultPage()
            Try
                currentResultPage = Int(startingResultItem / conMaxResultsToDisplay) + IIf((startingResultItem Mod conMaxResultsToDisplay) = 0, 0, 1)

                If currentResultPage > endingResultPage Then currentResultPage = endingResultPage
            Catch
                currentResultPage = 1
            End Try
        End Sub

        Private Sub calcEndingResultPage()
            Try
                endingResultPage = Int(totalSearchResults / conMaxResultsToDisplay) + IIf((totalSearchResults Mod conMaxResultsToDisplay) = 0, 0, 1)
            Catch
                endingResultPage = 1
            End Try
        End Sub

        Private Sub calcStaringResultItem(ByVal thisPage As enumResultPageRequested)
            Dim tempPageNumber As Integer = 1
            Dim tempCurrentPageNumber As Integer = 1
            calcEndingResultPage()

            Try
                If txtPageNumber.Text.ToString().Trim() <> "" Then
                    If IsNumeric(txtPageNumber.Text.ToString()) Then tempPageNumber = Integer.Parse(txtPageNumber.Text.ToString())
                    If tempPageNumber > endingResultPage Then tempPageNumber = endingResultPage
                End If

                If lblCurrentPage.Text.ToString().Trim() <> "" Then
                    If IsNumeric(lblCurrentPage.Text.ToString()) Then tempCurrentPageNumber = Integer.Parse(lblCurrentPage.Text.ToString())
                End If

                Select Case (thisPage)
                    Case enumResultPageRequested.FirstPage
                        startingResultItem = 1
                    Case enumResultPageRequested.LastPage
                        If endingResultPage > 1 Then
                            startingResultItem = (((endingResultPage - 1) * conMaxResultsToDisplay) + 1)
                        Else
                            startingResultItem = 1
                        End If
                    Case enumResultPageRequested.NextPage
                        If (tempCurrentPageNumber + 1) > endingResultPage Then
                            startingResultItem = (((tempCurrentPageNumber - 1) * conMaxResultsToDisplay) + 1)
                        Else
                            startingResultItem = (((tempCurrentPageNumber) * conMaxResultsToDisplay) + 1)
                        End If

                    Case enumResultPageRequested.PreviousPage
                        If tempCurrentPageNumber > 2 Then
                            startingResultItem = (((tempCurrentPageNumber - 2) * conMaxResultsToDisplay) + 1)
                        Else
                            startingResultItem = 1
                        End If

                    Case enumResultPageRequested.UserEnteredPage
                        If tempPageNumber <= 1 Then
                            startingResultItem = 1
                        Else
                            startingResultItem = (((tempPageNumber - 1) * conMaxResultsToDisplay) + 1)
                        End If
                End Select

                calcCurrentResultPage()

            Catch
                startingResultItem = 1
            End Try
        End Sub

        Private Sub updateResultPageNav()
            Dim hidePrevButtons As Boolean = False
            Dim hideNextButtons As Boolean = False

            PreviousLinkButton.Visible = True
            btnPreviousPage.Visible = True
            NextLinkButton.Visible = True
            btnNextPage.Visible = True

            Select Case resultPageRequested
                Case enumResultPageRequested.FirstPage
                    hidePrevButtons = True
                    If endingResultPage = currentResultPage Then hideNextButtons = True

                    lblCurrentPage.Text = currentResultPage.ToString()
                Case enumResultPageRequested.LastPage
                    hideNextButtons = True

                    lblCurrentPage.Text = endingResultPage.ToString()
                Case enumResultPageRequested.NextPage
                    If endingResultPage <= (currentResultPage + 1) Then hideNextButtons = True

                    lblCurrentPage.Text = currentResultPage.ToString()
                Case enumResultPageRequested.PreviousPage
                    If currentResultPage = 1 Then hidePrevButtons = True

                    lblCurrentPage.Text = currentResultPage.ToString()
                Case enumResultPageRequested.UserEnteredPage
                    If currentResultPage = 1 Then hidePrevButtons = True
                    If endingResultPage = currentResultPage Then hideNextButtons = True

                    lblCurrentPage.Text = currentResultPage.ToString()
                Case Else
                    lblCurrentPage.Text = "1"
            End Select

            lblEndingPage.Text = endingResultPage.ToString()
            txtPageNumber.Text = ""


            If hidePrevButtons Then
                PreviousLinkButton.Visible = False
                btnPreviousPage.Visible = False
            End If

            If hideNextButtons Then
                NextLinkButton.Visible = False
                btnNextPage.Visible = False
            End If

        End Sub
#End Region

#Region "Update catalog"
        Private Sub updateCatalog(ByRef thisCustomer As Customer, ByRef thisUsersCatalogItems() As CustomerCatalogItem, ByRef objGlobalData As GlobalData)
            Dim thisKey As String
            Dim seqNum As Integer = 0
            Dim updateCatalog As Boolean = False

            '--- loop through request item 
            For Each thisKey In Request.Form
                If thisKey.ToString.StartsWith(conDeleteCatalogCheckBoxPrefix) Then
                    If IsNumeric(thisKey.Replace(conDeleteCatalogCheckBoxPrefix, "")) Then
                        seqNum = Integer.Parse(thisKey.Replace(conDeleteCatalogCheckBoxPrefix, ""))
                        removeItemFromCatalog(seqNum, thisUsersCatalogItems)
                        If Not updateCatalog Then updateCatalog = True
                    End If
                ElseIf IsNumeric(thisKey.Replace(conDeleteReminderCheckBoxPrefix, "")) Then
                    If IsNumeric(thisKey.Replace(conDeleteReminderCheckBoxPrefix, "")) Then
                        seqNum = Integer.Parse(thisKey.Replace(conDeleteReminderCheckBoxPrefix, ""))
                        removeReminderFromCatalog(seqNum, thisUsersCatalogItems)
                        If Not updateCatalog Then updateCatalog = True
                    End If
                End If
            Next

            '-- refresh catalog items before building the page
            If updateCatalog Then thisUsersCatalogItems = getUsersCatalog(thisCustomer, objGlobalData) 'Sasikumar WP SPLUS_WP007
        End Sub




        Private Sub removeItemFromCatalog(ByVal seqNum As Integer, ByRef thisUsersCatalogItems() As CustomerCatalogItem)
            Try
                Dim thisCM As New CatalogManager
                For Each thisItem As CustomerCatalogItem In thisUsersCatalogItems
                    If thisItem.SequenceNumber = seqNum Then
                        thisCM.DeleteMyCatalogItem(thisItem)
                        Exit For
                    End If
                Next
            Catch ex As Exception
                errorMessageLabel.Text = Utilities.WrapExceptionforUI(ex)

            End Try
        End Sub


        Private Sub removeReminderFromCatalog(ByVal seqNum As Integer, ByRef thisUsersCatalogItems() As CustomerCatalogItem)
            Try
                Dim thisCM As New CatalogManager
                For Each thisItem As CustomerCatalogItem In thisUsersCatalogItems
                    If thisItem.SequenceNumber = seqNum Then
                        thisItem.RemoveReminder()


                        thisCM.UpdateMyCatalogItem(thisItem)
                        Exit For
                    End If
                Next
            Catch ex As Exception
                errorMessageLabel.Text = Utilities.WrapExceptionforUI(ex)

            End Try
        End Sub

#End Region

#Region "Add item to shopping cart"
        Private Sub getCatalogItemsToAddToShoppingCart(ByVal thisCustomer As Customer, ByVal thisUsersCatalogItems() As CustomerCatalogItem)
            Dim thisKey As String
            Dim seqNum As Integer

            Try
                If thisCustomer IsNot Nothing Then
                    '--- loop through request item 
                    For Each thisKey In Request.Form
                        If thisKey.StartsWith(conOrderCheckBoxPrefix) Then
                            If IsNumeric(thisKey.Replace(conOrderCheckBoxPrefix, "")) Then
                                seqNum = Integer.Parse(thisKey.Replace(conOrderCheckBoxPrefix, ""))
                                addThisItemToShoppingCart(seqNum, thisUsersCatalogItems, thisCustomer)
                                '-- refresh catalog items before building the page -- NOT NEEDED REMOVE - DWD
                                'thisUsersCatalogItems = getUsersCatalog(thisCustomer)
                            End If
                        End If
                    Next
                End If
            Catch ex As Exception
                errorMessageLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub


        Private Sub addThisItemToShoppingCart(ByVal seqNum As Integer, ByRef thisUsersCatalogItems() As CustomerCatalogItem, ByRef thisCustomer As Customer)
            Dim objPCILogger As New PCILogger() '6524
            Try
                Dim differentCart As Boolean = False

                For Each thisItem As CustomerCatalogItem In thisUsersCatalogItems
                    If thisItem.SequenceNumber = seqNum Then
                        'Dim thisSCM As New ShoppingCartManager
                        'Dim thisCart As ShoppingCart
                        'we don't provide a choice for download only for software, we assume that it is always shipping
                        Dim carts As ShoppingCart() = getAllShoppingCarts(thisCustomer, Me.errorMessageLabel)
                        '6994 starts
                        'thisCart = thisSCM.AddItemToCart(carts, thisItem).ShoppingCart
                        'If Not IsCurrentCartSameType(thisCart) Then
                        '    differentCart = True
                        'End If
                        'Me.updateCartSessionVariable("carts", thisCart)
                        hidelistbox.Items.Add(thisItem.Product.PartNumber)
                        '6994 ends
                    End If
                Next

                RegisterStartupScript("AddToCart_onclick", "<script language='javascript'>AddToCart_onclick();</script>")

                If differentCart Then   ' ASleight - Since "6994", this will always be "False".
                    Me.errorMessageLabel.Visible = True
                    Me.errorMessageLabel.Text = "Different kinds of Items (i.e, print on demand, download only and othets ) are put in different carts"
                End If

            Catch ex As Exception
                handleError(ex)
                '6524 V21 starts
                objPCILogger.EventOriginApplication = "ServicesPLUS"
                objPCILogger.EventOriginApplicationLocation = Me.Page.GetType().Name
                objPCILogger.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
                objPCILogger.OperationalUser = Environment.UserName
                objPCILogger.EventDateTime = DateTime.Now.ToLongTimeString()
                objPCILogger.HTTPRequestObjectValues = objPCILogger.GetRequestContextValue()
                objPCILogger.EventOriginMethod = "addThisItemToShoppingCart"
                objPCILogger.EventType = EventType.Add
                '6524 V21 ends
                objPCILogger.IndicationSuccessFailure = "Failure" '6524
                objPCILogger.Message = "addThisItemToShoppingCart Failed. " & ex.Message.ToString() '6524
                objPCILogger.PushLogToMSMQ() '6524 V21
            Finally
                'objPCILogger.PushLogToMSMQ() '6524
            End Try
        End Sub

#End Region

#Region "catalog sorting"

        Private Sub sortCatalogItems(ByRef thisUsersCatalogItems() As CustomerCatalogItem)
            sortOrder = getTheSortOrder()
            CustomerCatalogItem.SortOrder = sortOrder
            Array.Sort(thisUsersCatalogItems)
        End Sub

        Private Function getTheSortOrder() As CustomerCatalogItem.enumSortOrder
            Select Case SortCatalogButton.CommandArgument.ToString()
                Case CustomerCatalogItem.enumSortOrder.ModelASC.ToString()
                    Return CustomerCatalogItem.enumSortOrder.ModelASC
                Case CustomerCatalogItem.enumSortOrder.ModelDESC.ToString()
                    Return CustomerCatalogItem.enumSortOrder.ModelDESC
                Case CustomerCatalogItem.enumSortOrder.PartNumberASC.ToString()
                    Return CustomerCatalogItem.enumSortOrder.PartNumberASC
                Case CustomerCatalogItem.enumSortOrder.PartNumberDESC.ToString()
                    Return CustomerCatalogItem.enumSortOrder.PartNumberDESC
                Case CustomerCatalogItem.enumSortOrder.DateAddedASC.ToString()
                    Return CustomerCatalogItem.enumSortOrder.DateAddedASC
                Case CustomerCatalogItem.enumSortOrder.DateAddedDESC.ToString()
                    Return CustomerCatalogItem.enumSortOrder.DateAddedDESC
                Case Else
                    Return CustomerCatalogItem.enumSortOrder.ModelASC
            End Select
        End Function

        Private Function setTheSortOrder() As CustomerCatalogItem.enumSortOrder
            Dim thisSortOrder As CustomerCatalogItem.enumSortOrder = CustomerCatalogItem.enumSortOrder.ModelASC

            Select Case SortCatalog.SelectedValue
                Case dropDownListSortingOptions(0)  'model number
                    If IsPostBack Then
                        If SortCatalogButton.CommandArgument.ToString() = CustomerCatalogItem.enumSortOrder.ModelASC.ToString() Then
                            thisSortOrder = CustomerCatalogItem.enumSortOrder.ModelDESC
                        Else
                            thisSortOrder = CustomerCatalogItem.enumSortOrder.ModelASC
                        End If
                    Else
                        thisSortOrder = CustomerCatalogItem.enumSortOrder.ModelASC
                    End If
                Case dropDownListSortingOptions(1)  'item number / part number
                    If SortCatalogButton.CommandArgument.ToString() = CustomerCatalogItem.enumSortOrder.PartNumberASC.ToString() Then
                        thisSortOrder = CustomerCatalogItem.enumSortOrder.PartNumberDESC
                    Else
                        thisSortOrder = CustomerCatalogItem.enumSortOrder.PartNumberASC
                    End If
                Case dropDownListSortingOptions(2)  'date added
                    If SortCatalogButton.CommandArgument.ToString() = CustomerCatalogItem.enumSortOrder.DateAddedASC.ToString() Then
                        thisSortOrder = CustomerCatalogItem.enumSortOrder.DateAddedDESC
                    Else
                        thisSortOrder = CustomerCatalogItem.enumSortOrder.DateAddedASC
                    End If
            End Select

            SortCatalogButton.CommandArgument = thisSortOrder.ToString()

            Return thisSortOrder
        End Function

#End Region

#Region "Helpers"

        Private Function getUsersCatalog(ByRef thisCustomer As Customer, ByRef objGlobalData As GlobalData) As CustomerCatalogItem()
            Try
                If thisCustomer IsNot Nothing Then
                    Dim thisCatalogManager As New CatalogManager
                    Return thisCatalogManager.MyCatalog(thisCustomer, objGlobalData) 'Sasikumar WP SPLUS_WP007
                End If
                Return Nothing
            Catch ex As Exception
                errorMessageLabel.Text = Utilities.WrapExceptionforUI(ex)
                Return Nothing
            End Try
        End Function

        Private Sub formFieldInitialization()
            txtPageNumber.Attributes("onkeydown") = "SetTheFocusButton(event, 'btnGoToPage')"
            'txtPageNumber2.Attributes("onkeydown") = "SetTheFocusButton(event, 'btnGoToPage2')"
        End Sub


#End Region

#End Region

        <System.Web.Services.WebMethod(EnableSession:=True)>
        Public Shared Function addPartToCartwSMyCatalog(ByVal sPartNumber As String, ByVal sPartQuantity As String) As PartresultMyCatalog '6994
            Dim objPCILogger As New PCILogger() '6524
            Dim cm As CourseManager = Nothing
            Dim sm As New ShoppingCartManager
            Dim cart As ShoppingCart
            Dim customer As New Customer
            Dim lmessage As New Label
            Dim objSSL As New SSL
            Dim sPartNumberUpperCase As String = sPartNumber.ToUpper()
            Dim message As String
            Dim output1 As String = String.Empty    ' Output1 value return form SAP helpler
            Dim partsquantity As Integer
            Dim differentCart As Boolean = False
            Dim objResult As New PartresultMyCatalog With {
                .partnumber = sPartNumberUpperCase,
                .Quantity = 1
            }

            If HttpContextManager.Customer IsNot Nothing Then
                customer = HttpContextManager.Customer
            End If

            If HttpContextManager.GlobalData.IsCanada Then
                If customer.UserType <> "A" Then
                    message = Resources.Resource.el_ATC_PendingCust ' "Message for pending customer. (Cannot add model to cart for pending account customer. )"
                    objResult.success = -1
                    objResult.message = message
                    Return objResult
                End If
            End If

            Dim carts As ShoppingCart() = objSSL.getAllShoppingCarts(customer, lmessage)
            message = lmessage.Text
            objResult.success = False
            'Test value added by sathish need to remove 
            'sPartQuantity = -1
            Try
                'objPCILogger.EventOriginApplication = "ServicesPLUS"
                ''objPCILogger.EventOriginApplicationLocation = Me.Page.GetType().Name
                'objPCILogger.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
                'objPCILogger.OperationalUser = Environment.UserName
                'objPCILogger.EventDateTime = DateTime.Now.ToLongTimeString()
                'objPCILogger.HTTPRequestObjectValues = objPCILogger.GetRequestContextValue()
                'objPCILogger.IndicationSuccessFailure = "Success"
                'objPCILogger.CustomerID = customer.CustomerID
                'objPCILogger.CustomerID_SequenceNumber = customer.SequenceNumber
                ''objPCILogger.UserName = customer.UserName
                'objPCILogger.EmailAddress = customer.EmailAddress '6524
                'objPCILogger.SIAM_ID = customer.SIAMIdentity
                'objPCILogger.LDAP_ID = customer.LdapID '6524
                'objPCILogger.EventOriginMethod = "addPartToCart"
                'objPCILogger.Message = "addPartToCart Success."
                'objPCILogger.EventType = EventType.Add

                partsquantity = Convert.ToInt64(sPartQuantity)

                If partsquantity < 0 Then
                    message = Resources.Resource.el_Qty_Negative ' "Parts quantity should not be negative ."
                    objResult.success = -1
                    objResult.message = message
                ElseIf partsquantity > 0 Then
                    'Bug 9052 :handling negative vaules .sathish -end 
                    'Sasikumar WP
                    cart = sm.AddProductToCartQuickCart(carts, sPartNumberUpperCase, sPartQuantity, True, output1, HttpContextManager.GlobalData)
                    Dim bProperpart As Boolean = True
                    If Not objSSL.IsCurrentCartSameType(cart) Then
                        differentCart = True
                        objResult.differentCart = True
                    End If
                    For Each i As ShoppingCartItem In cart.ShoppingCartItems
                        If Not i.Product.PartNumber.Equals(sPartNumberUpperCase) Then
                            HttpContext.Current.Session("carts") = cart
                            objResult.items = cart.ShoppingCartItems.Length
                            objResult.totalAmount = String.Format("{0:c}", cart.ShoppingCartTotalPrice)
                            objResult.success = 2
                            message = String.Format(Resources.Resource.atc_ReplacementAdded, i.Product.PartNumber, sPartNumberUpperCase)
                            'message = "Replacement item " + i.Product.PartNumber + " added to cart in place of requested item  " + sPartNumberUpperCase + " which is no longer available."
                        Else
                            HttpContext.Current.Session("carts") = cart
                            objResult.items = cart.ShoppingCartItems.Length
                            objResult.totalAmount = String.Format("{0:c}", cart.ShoppingCartTotalPrice)
                            objResult.success = 1
                            message = Resources.Resource.el_item + sPartNumberUpperCase + Resources.Resource.atc_AddedToCart
                            'message = "Item " + sPartNumberUpperCase + " added to cart."
                        End If
                    Next

                End If 'Bug 9052 :Added by sathish for handling negative values 
            Catch exArg As ArgumentException
                If exArg.ParamName = "SomePartNotFound" Then
                    message = exArg.Message.Replace("Parameter name: SomePartNotFound", "")
                ElseIf exArg.ParamName = "PartNotFound" Then
                    message = exArg.Message.Replace("Parameter name: PartNotFound", "")
                ElseIf exArg.ParamName = "BillingOnlyItem" Then
                    message = exArg.Message.Replace("Parameter name: BillingOnlyItem", "")
                End If
            Catch ex As Exception
                '6524 V11 starts
                objPCILogger.EventOriginApplication = "ServicesPLUS"
                'objPCILogger.EventOriginApplicationLocation = Me.Page.GetType().Name
                objPCILogger.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
                objPCILogger.OperationalUser = Environment.UserName
                objPCILogger.EventDateTime = DateTime.Now.ToLongTimeString()
                objPCILogger.HTTPRequestObjectValues = objPCILogger.GetRequestContextValue()
                objPCILogger.CustomerID = customer.CustomerID
                objPCILogger.CustomerID_SequenceNumber = customer.SequenceNumber
                'objPCILogger.UserName = customer.UserName
                objPCILogger.EmailAddress = customer.EmailAddress '6524
                objPCILogger.SIAM_ID = customer.SIAMIdentity
                objPCILogger.LDAP_ID = customer.LdapID '6524
                objPCILogger.EventOriginMethod = "addPartToCartwSMyCatalog"
                objPCILogger.EventType = EventType.View
                '6524 V11 ends
                objPCILogger.IndicationSuccessFailure = "Failure" '6524
                objPCILogger.Message = "addPartToCart Failed. " & ex.Message.ToString() '6524
                objPCILogger.PushLogToMSMQ() '6524'6524 V11
                '7813 starts
                'message = Utilities.WrapExceptionforUI(ex)'7813
                Dim strUniqueKey As String = Utilities.GetUniqueKey
                message = Utilities.WrapExceptionforUINumber(ex, strUniqueKey)
                message = message.Remove(message.Length - 1)
                message = message + strUniqueKey + "."
                '7813 ends
            Finally
                'objPCILogger.PushLogToMSMQ() '6524
            End Try
            objResult.message = message
            Return objResult
        End Function
    End Class

    Public Class PartresultMyCatalog '6994
        Public _message As String
        Public _totalAmount As String
        Public _items As String
        Public _success As Integer
        Public _quantity As String
        Public _partnumber As String
        Public _differentCart As Boolean

        Property partnumber() As String
            Get
                Return _partnumber
            End Get
            Set(ByVal value As String)
                _partnumber = value
            End Set
        End Property
        Property message() As String
            Get
                Return _message
            End Get
            Set(ByVal value As String)
                _message = value
            End Set
        End Property
        Property Quantity() As String
            Get
                Return _quantity
            End Get
            Set(ByVal value As String)
                _quantity = value
            End Set
        End Property
        ''' <summary>
        ''' 0-Failure
        ''' 1-Success
        ''' 2-warning
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Property success() As Integer
            Get
                Return _success
            End Get
            Set(ByVal value As Integer)
                _success = value
            End Set
        End Property
        Property totalAmount() As String
            Get
                Return _totalAmount
            End Get
            Set(ByVal value As String)
                _totalAmount = value
            End Set
        End Property
        Property items() As String
            Get
                Return _items
            End Get
            Set(ByVal value As String)
                _items = value
            End Set
        End Property

        Property differentCart() As Boolean
            Get
                Return _differentCart
            End Get
            Set(ByVal value As Boolean)
                _differentCart = value
            End Set
        End Property

    End Class
    '6994 ends

End Namespace
