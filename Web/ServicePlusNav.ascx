<%@ Control Language="vb" AutoEventWireup="false" Inherits="ServicePLUSWebApp.ServicePlusNav" CodeFile="ServicePlusNav.ascx.vb" %>
<%=""%>
<table style="padding: 0px; margin: 0px; width: 710px; border: none;" role="navigation">
    <tr>
        <td style="height: 29px; background: url(images/sp_hm_btm_bar.gif); text-align: center; vertical-align: middle; width: 88.75px;">
            <% If isLoggedIn Then%>
                <a href="Member.aspx" class="menuClass"><%=Resources.Resource.menu_home%></a>
            <% Else%>
                <a href="default.aspx" class="menuClass"><%=Resources.Resource.menu_home%></a>
            <% End If %>
        </td>

        <td style="height: 29px; background: url(images/sp_hm_btm_bar.gif); text-align: center; vertical-align: middle; width: 88.75px;">
            <% If isLoggedIn Then%>
                <a href="au-sony-knowledge-base-search.aspx" class="menuClass"><%=Resources.Resource.menu_knowldgebase%></a>
            <% Else%>
                <a href="sony-knowledge-base-search.aspx" class="menuClass"><%=Resources.Resource.menu_knowldgebase%></a>
            <% End If %>
        </td>
        
        <td style="height: 29px; background: url(images/sp_hm_btm_bar.gif); text-align: center; vertical-align: middle; width: 88.75px;">
            <a href="sony-operation-manual.aspx" class="menuClass"><%=Resources.Resource.menu_manuals %></a>
        </td>

        <td style="height: 29px; background: url(images/sp_hm_btm_bar.gif); text-align: center; vertical-align: middle; width: 88.75px;">
            <% If isLoggedIn Then%>
                <a href="au-sony-parts.aspx" class="menuClass"><%=Resources.Resource.menu_parts %></a>
            <% Else%>
                <a href="sony-parts.aspx" class="menuClass"><%=Resources.Resource.menu_parts %></a>
            <% End If %>
        </td>

        <td style="height: 29px; background: url(images/sp_hm_btm_bar.gif); text-align: center; vertical-align: middle; width: 88.75px;">
            <a href="sony-repair.aspx" class="menuClass"><%=Resources.Resource.menu_repair %></a>
        </td>

        <td style="height: 29px; background: url(images/sp_hm_btm_bar.gif); text-align: center; vertical-align: middle; width: 88.75px;">
            <a href="sony-software.aspx" class="menuClass"><%=Resources.Resource.menu_software %></a>
        </td>

        <td style="height: 29px; background: url(images/sp_hm_btm_bar.gif); text-align: center; vertical-align: middle; width: 88.75px;">
            <a href="sony-training-catalog.aspx" class="menuClass"><%=Resources.Resource.menu_training %></a>
        </td>

        <td style="height: 29px; background: url(images/sp_hm_btm_bar.gif); text-align: center; vertical-align: middle; width: 88.75px;">
            <a href="sony-service-agreements.aspx" class="menuClass"><%=Resources.Resource.hplnk_svc_agreement %></a>
        </td>
    </tr>

<%-- SHOPPING CART ROW --%>    
<% If isLoggedIn Then%>
    <tr>
        <td colspan="5" style="padding: 2px 0px 2px 10px; background: black; text-align: left;">
            <span class="PromoWhiteText"><img src="<%=Resources.Resource.mem_vwcart_img%>" alt="">&nbsp;<a class="PromoWhiteText" href="vc.aspx"><%=Resources.Resource.mem_curnt_cart_msg%></a>
            &nbsp;|&nbsp;<%=Resources.Resource.mem_itms_cart_msg%>&nbsp;<span class="redAsterick" id="itemsInCart"><%=itemsInCart%></span>
            &nbsp;|&nbsp;<%=Resources.Resource.mem_itms_ttcart_msg%>&nbsp;<span class="redAsterick" id="totalPriceOfCart"><%= String.Format("{0:c}", totalPriceOfCart) %></span></span>
        </td>
        <td colspan="4" style="padding: 2px 10px 2px 0px; background: black; text-align: right;">
            <span class="PromoWhiteText">&nbsp;&nbsp;<a class="PromoWhiteText" href="vc.aspx"><%=Resources.Resource.mem_itms_vwcart_msg%></a>
            |&nbsp;<a class="PromoWhiteText" href="CustomerSavedCarts.aspx"><%=Resources.Resource.mem_itms_svcart_msg%></a>
            |&nbsp;<asp:LinkButton ID="NewCart" CssClass="PromoWhiteText" runat="server"><%=Resources.Resource.mem_itms_nwcart_msg %></asp:LinkButton></span>
        </td>
    </tr>
<% End If%>
</table>