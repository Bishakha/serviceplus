<%@ Reference Page="~/SSL.aspx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="SonyFooter" Src="SonyFooter.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="TopSonyHeader" Src="TopSonyHeader.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="PersonalMessage" Src="PersonalMessage.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="ServicePlusNav" Src="ServicePlusNav.ascx" %>

<%@ Page Language="vb" AutoEventWireup="false" Inherits="ServicePLUSWebApp.Help"
    EnableViewStateMac="true" CodeFile="Help.aspx.vb" CodeFileBaseClass="ServicePLUSWebApp.SSL" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="<%=PageLanguage %>">
<head>
    <title><%=Resources.Resource.header_serviceplus()%> - <%=Resources.Resource.footer_help()%></title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <meta content="Sony Parts, Sony Professional Parts, Sony Broadcast Parts, Sony Business Parts, Sony Software, Sony Professional Software, Sony Broadcast Software, Sony Business Software"
        name="keywords">
    <link href="includes/ServicesPLUS_style.css" type="text/css" rel="stylesheet">

    <script language="javascript" src="includes/ServicesPLUS.js" type="text/javascript"></script>
    <script type="text/javascript" src="includes/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="includes/svcplus_globalization.js"></script>

</head>
<body style="color: #000000; background: #5d7180; margin: 0px;">
    <form id="Form1" method="post" runat="server">
        <center>
            <table width="710" border="0" role="presentation">
                <tr>
                    <td width="25" style="background: url('images/sp_left_bkgd.gif');">
                        <img src="images/spacer.gif" width="25" height="25" alt="">
                    </td>
                    <td style="height: 1385px" width="710" bgcolor="#ffffff">
                        <table width="650" border="0" role="presentation">
                            <tr>
                                <td>
                                    <ServicePLUSWebApp:TopSonyHeader ID="TopSonyHeader" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td style="height: 21px" height="21">
                                    <nav><ServicePLUSWebApp:ServicePlusNav ID="SonyNav" runat="server" /></nav>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="650" border="0" role="presentation">
                                        <tr>
                                            <td width="464" bgcolor="#363d45" height="57" background="images/sp_int_header_top_ServicesPLUS_onepix.gif"
                                                align="right" valign="top">
                                                <br />
                                                <h1 class="headerText"><%=Resources.Resource.header_serviceplus%> &nbsp;&nbsp;</h1>
                                            </td>
                                            <td valign="top" bgcolor="#363d45">
                                                <table width="246" border="0" role="presentation">
                                                    <tr>
                                                        <td colspan="3">
                                                            <img height="24" src="images/spacer.gif" width="246" alt="">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="5">
                                                            &nbsp;
                                                        </td>
                                                        <td width="236">
                                                            <span class="memberName">
                                                                <ServicePLUSWebApp:PersonalMessage ID="PersonalMessageDisplay" runat="server" />
                                                            </span>
                                                        </td>
                                                        <td width="5">
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td bgcolor="#f2f5f8">
                                                <h2 class="headerTitle" style="padding-right: 20px; text-align: right;"><%=Resources.Resource.footer_help%></h2>
                                            </td>
                                            <td bgcolor="#99a8b5">
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr height="9">
                                            <td bgcolor="#f2f5f8">
                                                <img height="9" src="images/sp_int_header_btm_left_onepix.gif" width="464" alt="">
                                            </td>
                                            <td bgcolor="#99a8b5">
                                                <img height="9" src="images/sp_int_header_btm_right.gif" width="246" alt="">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td style="height: 12px" align="left">
                                </td>
                            </tr>
                            <tr>
                                <td style="height: 12px" align="left">
                                    <p>
                                        <span class="bodyCopyBold">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=Resources.Resource.help_cnt_fq_msg%></span>
                                    </p>
                                </td>
                            </tr>
                            <tr>
                                <td style="height: 12px" align="left">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="height: 12px" align="left">
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="bodyCopyBold"><a href="#" onclick="window.open('sony-service-agreement-info.aspx','','toolbar=0,location=0,top=0,left=0,directories=0,status=0,menubar=0,scrollbars=no,resize=no,width=400,height=475'); return false;"><%=Resources.Resource.help_cnt_svcus_msg()%>
                                    </a></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="bodyCopyBold">
                                        <img height="29" src="images/spacer.gif" width="20" alt="">
                                        <%=Resources.Resource.help_cnt_hdi_msg()%></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="650" border="0" role="presentation">
                                        <tr>
                                            <td style="height: 178px">
                                                <table width="650" border="0" role="presentation">
                                                    <tr>
                                                        <td style="height: 8px">
                                                            <img height="1" src="images/spacer.gif" width="25" alt="">&nbsp;&nbsp;&nbsp;&nbsp;<strong>.
                                                            </strong><a style="color: #0066ff" href="#How do I register more than one student for a class?">
                                                                <span class="bodyCopyAnchor"><%=Resources.Resource.help_cnt_hdc_msg()%></span></a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="height: 8px">
                                                            <img height="1" src="images/spacer.gif" width="25" alt="">&nbsp;&nbsp;&nbsp;&nbsp;<strong>.
                                                            </strong><a style="color: #0066ff" href="#How do I add an item to my shopping cart?">
                                                                <span class="bodyCopyAnchor"><%=Resources.Resource.help_cnt_itcart_msg()%></span></a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="height: 8px">
                                                            <img height="1" src="images/spacer.gif" width="25" alt="">&nbsp;&nbsp;&nbsp;&nbsp;<strong>.
                                                            </strong><a style="color: #0066ff" href="#How do I order a part for emergency after-hours shipment?">
                                                                <span class="bodyCopyAnchor"><%=Resources.Resource.help_cnt_ship_msg()%> </span></a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="height: 8px">
                                                            <img height="1" src="images/spacer.gif" width="25" alt="">&nbsp;&nbsp;&nbsp;&nbsp;<strong>.
                                                            </strong><a style="color: #0066ff" href="#How do I know when and how my parts and/or software will ship?">
                                                                <span class="bodyCopyAnchor"><%=Resources.Resource.help_cnt_sft_msg()%></span></a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="height: 8px">
                                                            <img height="1" src="images/spacer.gif" width="25" alt="">&nbsp;&nbsp;&nbsp;&nbsp;<strong>.
                                                            </strong><a style="color: #0066ff" href="#How do I deactivate a user that is no longer with my company?">
                                                                <span class="bodyCopyAnchor"><%=Resources.Resource.help_cnt_cmp_msg()%></span></a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="height: 8px">
                                                            <img height="1" src="images/spacer.gif" width="25" alt="">&nbsp;&nbsp;&nbsp;&nbsp;<strong>.
                                                            </strong><a style="color: #0066ff" href="#How do I take advantage of the price matching program?">
                                                                <span class="bodyCopyAnchor"><%=Resources.Resource.help_cnt_mp_msg()%></span></a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="height: 8px">
                                                            <img height="1" src="images/spacer.gif" width="25" alt="">&nbsp;&nbsp;&nbsp;&nbsp;<strong>.
                                                            </strong><a style="color: #0066ff" href="#How do I return a part?"><span class="bodyCopyAnchor">
                                                                <%=Resources.Resource.help_cnt_hdrp_msg()%></span></a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="height: 8px">
                                                            <img height="1" src="images/spacer.gif" width="25" alt="">&nbsp;&nbsp;&nbsp;&nbsp;<strong>.
                                                            </strong><a style="color: #0066ff" href="#How do I find the warranty information for my product, part, or software?">
                                                                <span class="bodyCopyAnchor"><%=Resources.Resource.help_cnt_prt_msg()%></span></a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="height: 8px">
                                                            <img height="1" src="images/spacer.gif" width="25" alt="">&nbsp;&nbsp;&nbsp;&nbsp;<strong>.
                                                            </strong><a style="color: #0066ff" href="#How do I view exploded views, certain software release notes, or the technical bulletin subscription order form?">
                                                                <span class="bodyCopyAnchor"><%=Resources.Resource.help_cnt_rdr_msg()%></span></a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="height: 8px">
                                                            <img height="1" src="images/spacer.gif" width="25" alt="">&nbsp;&nbsp;&nbsp;&nbsp;<strong>.
                                                            </strong><a style="color: #0066ff" href="#How do I find the answer to a question that is not listed above?">
                                                                <span class="bodyCopyAnchor"><%=Resources.Resource.help_cnt_lis_msg()%></span></a>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <p>
                                                    <span class="bodyCopy">&nbsp; </span>
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="height: 13px">
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a name="How do I register more than one student for a class?"><span
                                                    class="bodyCopyBold"><%=Resources.Resource.help_cnt_hdc_msg()%></span></a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="height: 12px">
                                                <span class="bodyCopy">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=Resources.Resource.help_cnt_hdc_msg_1()%> </span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span class="bodyCopy">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <%=Resources.Resource.help_cnt_hdc_msg_2()%> <a
                                                    href="mailto:training@am.sony.com" title="Click to send an email to Training@am.sony.com"><%=Resources.Resource.help_cnt_hdc_msg_3()%></a> <%=Resources.Resource.help_cnt_hdc_msg_4()%> </span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="height: 13px">
                                                <p>
                                                    <span class="bodyCopy">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <%=Resources.Resource.help_cnt_hdc_msg_5()%> </span>
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="height: 22px">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="height: 11px">
                                                &nbsp;&nbsp;&nbsp;&nbsp;<a name="How do I add an item to my shopping cart?"><span
                                                    class="bodyCopyBold"> <%=Resources.Resource.help_cnt_itcart_msg()%></span></a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="height: 23px" valign="top">
                                                <span class="bodyCopy"><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; .</strong>&nbsp;&nbsp;
                                                    <%=Resources.Resource.help_cnt_itcart_msg_1()%>
                                                    <img src="./images/sp_int_add2Cart_btn.gif" alt="Add to Cart">.</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span class="bodyCopy"><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; .</strong>&nbsp;&nbsp;
                                                    <%=Resources.Resource.help_cnt_itcart_msg_2()%></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span class="bodyCopy"><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; .</strong>&nbsp;&nbsp;
                                                    <%=Resources.Resource.help_cnt_itcart_msg_3()%></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span class="bodyCopy">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                    <%=Resources.Resource.help_cnt_itcart_msg_4()%></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span class="bodyCopy">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                    <%=Resources.Resource.help_cnt_itcart_msg_5()%></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="height: 12px">
                                                <p>
                                                    <span class="bodyCopy"><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; .</strong>&nbsp;&nbsp;
                                                       <%=Resources.Resource. help_cnt_itcart_msg_6()%></span>
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="height: 21px">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="height: 13px">
                                                &nbsp;&nbsp;&nbsp;&nbsp;<a name="How do I order a part for emergency after-hours shipment?"><span
                                                    class="bodyCopyBold"> <%=Resources.Resource.help_cnt_ship_msg()%></span></a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span class="bodyCopy">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <%=Resources.Resource.help_cnt_ship_msg_1()%> </span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span class="bodyCopy">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=Resources.Resource.help_cnt_ship_msg_2()%></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <p>
                                                    <span class="bodyCopy">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <%=Resources.Resource.help_cnt_ship_msg_3()%> </span>
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="height: 21px">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="height: 11px">
                                                &nbsp;&nbsp;&nbsp;&nbsp;<a name="How do I know when and how my parts and/or software will ship?"><span
                                                    class="bodyCopyBold"> <%=Resources.Resource.help_cnt_sftsh_msg()%></span></a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span class="bodyCopy"><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; .</strong>&nbsp;&nbsp;
                                                    <%=Resources.Resource.help_cnt_sftsh_msg_1()%></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span class="bodyCopy"><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; .</strong>&nbsp;&nbsp;
                                                    <%=Resources.Resource.help_cnt_sftsh_msg_2()%></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span class="bodyCopy"><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; .</strong>&nbsp;&nbsp;
                                                    <%=Resources.Resource.help_cnt_sftsh_msg_3()%></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span class="bodyCopy"><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; .</strong>&nbsp;&nbsp;
                                                    <%=Resources.Resource.help_cnt_sftsh_msg_4()%></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span class="bodyCopy"><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; .</strong>&nbsp;&nbsp;
                                                   <%=Resources.Resource.help_cnt_sftsh_msg_5()%></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span class="bodyCopy"><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; .</strong>&nbsp;&nbsp;
                                                    <%=Resources.Resource.help_cnt_sftsh_msg_6()%></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span class="bodyCopy"><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; .</strong>&nbsp;&nbsp;
                                                    <%=Resources.Resource.help_cnt_sftsh_msg_7()%></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="height: 13px">
                                                <p>
                                                    <span class="bodyCopy"><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; .</strong>&nbsp;&nbsp;
                                                        <strong><%=Resources.Resource.help_cnt_sftsh_msg_8()%></strong><%=Resources.Resource.help_cnt_sftsh_msg_9()%></span>
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="height: 20px">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="height: 11px">
                                                &nbsp;&nbsp;&nbsp;&nbsp;<a name="How do I deactivate a user that is no longer with my company?"><span
                                                    class="bodyCopyBold"><%=Resources.Resource.help_cnt_cmp_msg()%></span></a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <p>
                                                    <span class="bodyCopy">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=Resources.Resource.help_cnt_cmp_msg_1%> <a href="mailto:ProParts@am.sony.com" title="Click to send an email to ProParts@am.sony.com">ProParts@am.sony.com</a></span>
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="height: 21px">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="height: 12px">
                                                &nbsp;&nbsp;&nbsp;&nbsp;<a name="How do I take advantage of the price matching program?"><span
                                                    class="bodyCopyBold"><%=Resources.Resource.help_cnt_mp_msg()%></span></a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span class="bodyCopy">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=Resources.Resource.help_cnt_mp_msg_1()%>  </span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="height: 13px" valign="top">
                                                <p>
                                                    <span class="bodyCopy">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=Resources.Resource.help_cnt_mp_msg_2()%>  </span>
                                                </p>
                                            </td>
                                            <tr>
                                                <td style="height: 18px" valign="top">
                                                </td>
                                            </tr>
                                        <tr>
                                            <td style="height: 13px" valign="top">
                                                <p>
                                                    <span class="bodyCopy">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=Resources.Resource.help_cnt_mp_msg_3()%></span>
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="height: 21px" valign="top">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="height: 11px">
                                                &nbsp;&nbsp;&nbsp;&nbsp;<a name="How do I return a part?"><span class="bodyCopyBold"><%=Resources.Resource.help_cnt_hdrp_msg()%></span></a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span class="bodyCopy">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ID="RaRequestLabel"
                                                    runat="server"></asp:Label></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span class="bodyCopy"><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; .</strong>&nbsp;&nbsp;
                                                    <strong><%=Resources.Resource.help_cnt_hdrp_msg_1()%></strong> <%=Resources.Resource.help_cnt_hdrp_msg_2()%></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span class="bodyCopy">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                    <%=Resources.Resource.help_cnt_hdrp_msg_3()%></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span class="bodyCopy">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                    <%=Resources.Resource.help_cnt_hdrp_msg_4()%></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="height: 11px" valign="top">
                                                <p>
                                                    <span class="bodyCopy"><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; .</strong>&nbsp;&nbsp;
                                                        <strong><%=Resources.Resource.help_cnt_hdrp_msg_5%></strong> - <%=Resources.Resource.help_cnt_hdrp_msg_6%> <a href="http://pro.sony.com/bbsccms/services/files/servicesprograms/WARNTY2f.pdf"
                                                            onclick="mywin=window.open('http://pro.sony.com/bbsccms/services/files/servicesprograms/WARNTY2f.pdf'); return false;"><%=Resources.Resource.help_cnt_hdrp_msg_7()%></a>
                                                        <%=Resources.Resource.help_cnt_hdrp_msg_8()%> </span>
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="height: 20px" valign="top">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="height: 7px">
                                                &nbsp;&nbsp;&nbsp;&nbsp;<a name="How do I find the warranty information for my product, part, or software?"><span
                                                    class="bodyCopyBold"><%=Resources.Resource.help_cnt_prt_msg()%></span></a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="height: 14px">
                                                <span class="bodyCopy">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=Resources.Resource. help_cnt_prt_msg_1()%> </span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top">
                                                <span class="bodyCopy"><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; .</strong>&nbsp;&nbsp;
                                                    <a href="TC2.aspx"><%=Resources.Resource.help_cnt_prt_sft_msg()%></a></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span class="bodyCopy"><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; .</strong>&nbsp;&nbsp;<!--Link Changed for Bug# 1000, by Kannapiran S-->
                                                    <a href="http://pro.sony.com/bbsc/ssr/services.servicesprograms.bbsccms-services-servicesprograms-warrantyinformation.shtml"
                                                        onclick="window.open('http://pro.sony.com/bbsc/ssr/services.servicesprograms.bbsccms-services-servicesprograms-warrantyinformation.shtml'); return false;">
                                                        <%=Resources.Resource.help_cnt_prodt_msg()%></a></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="height: 19px">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="height: 1px">
                                                &nbsp;&nbsp;&nbsp;&nbsp;<a name="How do I view exploded views, certain software release notes, or the technical bulletin subscription order form?"><span
                                                    class="bodyCopyBold"><%=Resources.Resource.help_cnt_rdr_msg()%></span></a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="height: 12px">
                                                <span class="bodyCopy">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=Resources.Resource.help_cnt_toview_msg()%> </span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span class="bodyCopy">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=Resources.Resource.help_cnt_toview_msg_1()%> <a href="http://www.adobe.com/acrobat"
                                                    onclick="window.open('http://www.adobe.com/acrobat'); return false;"><%=Resources.Resource.help_cnt_toview_msg_2()%></a>.
                                                </span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="height: 19px">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="height: 1px">
                                                &nbsp;&nbsp;&nbsp;&nbsp;<a name="How do I find the answer to a question that is not listed above?"><span
                                                    class="bodyCopyBold"><%=Resources.Resource.help_cnt_lis_msg()%></span></a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top">
                                                <span class="bodyCopy"><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; .</strong>&nbsp;&nbsp;
                                                   <%=Resources.Resource.help_cnt_tyur_msg%> <a href="WebUserGuide.aspx"><%=Resources.Resource.help_cnt_sg_msg%></a>&nbsp;<%=Resources.Resource.help_cnt_or_msg%> <a href="sony-service-contacts.aspx">
                                                       <%=Resources.Resource.help_cnt_cnus_msg%></a> <%=Resources.Resource.help_cnt_wuq_msg%></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <footer><ServicePLUSWebApp:SonyFooter ID="SonyFooter1" runat="server" /></footer>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>

                    </td>
                    <td width="25" style="background: url('images/sp_right_bkgd.gif')">
                        <img src="images/spacer.gif" width="25" height="20" alt="">
                    </td>
                </tr>
            </table>
        </center>
    </form>
</body>
</html>
