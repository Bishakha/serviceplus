Imports System.Xml
Imports System.Xml.Xsl
Imports System.Text
Imports System.IO
Imports Sony.US.ServicesPLUS.Data
Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Process
Imports ServicesPlusException
'6994 starts
Imports System.Web.Script.Services
Imports System.Web.Services
Imports Sony.US.AuditLog
Imports Sony.US.SIAMUtilities
Imports System.Globalization

'6994 ends

Namespace ServicePLUSWebApp


    Partial Class sony_training_catalog2
        Inherits SSL
        'Dim objUtilties As Utilities = New Utilities

#Region "Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub


        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                If Not IsPostBack() Then
                    Dim catId As Int32 = -1

                    AddToCartAfterLogin(errorMessageLabel)
                    Try
                        If Request.QueryString("mincatid") IsNot Nothing Then
                            catId = Convert.ToInt32(Request.QueryString("mincatid"))
                            Session("mincatid") = Request.QueryString("mincatid")
                        ElseIf Session("mincatid") IsNot Nothing Then
                            catId = Convert.ToInt32(Session("mincatid"))
                        End If
                    Catch fex As FormatException
                        errorMessageLabel.Text = "Modified or Bad URL detected. Please go back and try again."
                    End Try

                    If Request.QueryString("atc") IsNot Nothing Then
                        'TBD: Add selected Course to cart abd then bind
                        Dim strCourseNumber As String = IIf(Request.QueryString("CourseNumber") IsNot Nothing, Request.QueryString("CourseNumber"), "")
                        If strCourseNumber <> String.Empty Then
                            Dim objTraining As Training
                            objTraining = GetTrainingResult(strCourseNumber)
                            If objTraining.Type = Product.ProductType.ComputerBasedTraining Then
                                objTraining.Download = False
                                objTraining.Ship = True
                            Else
                                objTraining.Download = True
                                objTraining.Ship = False
                            End If

                            If Session("Customer") IsNot Nothing Then
                                'AddToCart(CType(Session("Customer"), Customer), objTraining, IIf(objTraining.Download, ShoppingCartItem.DeliveryMethod.Download, ShoppingCartItem.DeliveryMethod.Ship), errorMessageLabel) '6994
                                RegisterStartupScript("AddToCart_onclick", "<script language='javascript'>AddToCart_onclick('" + strCourseNumber.ToString() + "');</script>") '6994
                            Else
                                Dim selectedItemsBeforeLoggedIn As ArrayList
                                If Session("SelectedItemsBeforeLoggedIn") IsNot Nothing Then
                                    selectedItemsBeforeLoggedIn = CType(Session("SelectedItemsBeforeLoggedIn"), ArrayList)
                                Else
                                    selectedItemsBeforeLoggedIn = New ArrayList
                                    Session("SelectedItemsBeforeLoggedIn") = selectedItemsBeforeLoggedIn
                                End If
                                selectedItemsBeforeLoggedIn.Add(objTraining)
                                'Response.Redirect("SignIn.aspx?post=stc2&mincatid=" + id.ToString())
                                Response.Redirect("SignIn-Register.aspx?post=stc2&mincatid=" + catId.ToString())
                            End If
                        End If
                    End If

                    If catId <> -1 Then
                        Dim cm As CourseDataManager = New CourseDataManager
                        Dim xmlDoc As XmlDataDocument = cm.GetCategoryCourse(catId)
                        Dim xslPath As String = Server.MapPath("CategoryCourse.xslt")
                        'Dim xmlPath As String = Server.MapPath("SISBatchLog.xml")

                        Dim sb As StringBuilder = New StringBuilder
                        Dim sw As StringWriter = New StringWriter(sb)
                        'Dim xmlReader As XmlTextReader = New XmlTextReader(xmlPath)
                        'Dim doc As XPathDocument = New XPathDocument(xmlReader)

                        Dim transform As XslTransform = New XslTransform
                        transform.Load(xslPath)

                        transform.Transform(xmlDoc, Nothing, sw, Nothing)
                        tableCourse.InnerHtml = sb.ToString()
                        sw.Flush()
                        sb.Length = 0
                        Dim currentDay As DateTime = Date.Now
                        Dim startDate As String = currentDay.Month.ToString() + "/" + currentDay.Day.ToString() + "/" + currentDay.Year.ToString()
                        xmlDoc = cm.GetClassCatalogDetail(catId, startDate, HttpContextManager.GlobalData.SalesOrganization)
                        'xmlDoc = cm.GetClassCatalogDetail(id, startDate)
                        If xmlDoc.InnerText.ToString().Trim() = String.Empty Or xmlDoc.InnerXml.ToString().Trim() = String.Empty Then
                            MessageLabel.Visible = True
                            MessageLabel.Text = "No courses are currently scheduled for this subject."
                        Else
                            MessageLabel.Visible = False
                            xslPath = Server.MapPath("CourseDetail.xslt")
                            transform.Load(xslPath)
                            'Add Extension Object
                            Dim args As XsltArgumentList = New XsltArgumentList
                            Dim dateTimeFormObj As SSL.XsltDateTime = New SSL.XsltDateTime
                            args.AddExtensionObject("urn:xsltExtension-DateTimeFormat", dateTimeFormObj)
                            Dim courseFormObj As SSL.XsltCourse = New SSL.XsltCourse
                            args.AddExtensionObject("urn:xsltExtension-CourseFormat", courseFormObj)
                            transform.Transform(xmlDoc, args, sw, Nothing)
                            tableCourseDetail.InnerHtml = sb.ToString()
                        End If
                    End If
                End If
            Catch ex As Exception
                errorMessageLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Function GetTrainingResult(ByVal CourseNumber As String) As Training
            Dim objTraining As Training = Nothing
            Try
                Dim objCourseManager As New CourseManager
                objTraining = objCourseManager.SearchNonClassRoomTrainingForATC(CourseNumber)
            Catch ex As Exception
                errorMessageLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
            Return objTraining
        End Function

        Private Sub btnCatlogHome_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCatlogHome.Click
            Response.Redirect("sony-training-catalog.aspx")
        End Sub
        'End Class'6994
        '6994 starts
        <System.Web.Services.WebMethod(EnableSession:=True)> _
            Public Shared Function addPartToCartwSTrainingCatalog(ByVal sPartNumber As String, ByVal sPartQuantity As String) As PartresultTrainingCatalog '6994
            'Public Shared Function addPartToCartwSPurchase(ByVal sPartNumber As String, ByVal sPartQuantity As String) As PartresultPurchase '6994
            Dim objPCILogger As New PCILogger() '6524
            Dim sPartNumberUpperCase As String = sPartNumber.ToUpper()
            Dim objResult As New PartresultTrainingCatalog '6994
            'Dim objUtilties As Utilities = New Utilities
            Dim message As String
            Dim customer As New Customer
            Dim lmessage As New Label
            Dim objSSL As New SSL
            Dim sm As New ShoppingCartManager
            Dim cart As ShoppingCart
            Dim carts As ShoppingCart() = Nothing
            Dim differentCart As Boolean = False
            Dim cm As CourseManager = Nothing

            objResult.partnumber = sPartNumberUpperCase
            objResult.Quantity = 1 '6994
            If Not HttpContext.Current.Session.Item("customer") Is Nothing Then
                customer = HttpContext.Current.Session.Item("customer")
            End If
            carts = objSSL.getAllShoppingCarts(customer, lmessage)
            ''6994 starts
            'Dim objextwarmodel As ExtendedWarrantyModel = HttpContext.Current.Session.Item("sesssionobjexwarranty")
            '6994 ends
            message = lmessage.Text
            objResult.success = False

            Try
                '6524 V11 Starts
                '6524 start
                'objPCILogger.EventOriginApplication = "ServicesPLUS"
                ''objPCILogger.EventOriginApplicationLocation = Me.Page.GetType().Name
                'objPCILogger.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
                'objPCILogger.OperationalUser = Environment.UserName
                'objPCILogger.EventDateTime = DateTime.Now.ToLongTimeString()
                'objPCILogger.HTTPRequestObjectValues = objPCILogger.GetRequestContextValue()
                'objPCILogger.IndicationSuccessFailure = "Success"
                'objPCILogger.CustomerID = customer.CustomerID
                'objPCILogger.CustomerID_SequenceNumber = customer.SequenceNumber
                ''objPCILogger.UserName = customer.UserName
                'objPCILogger.EmailAddress = customer.EmailAddress '6524
                'objPCILogger.SIAM_ID = customer.SIAMIdentity
                'objPCILogger.LDAP_ID = customer.LdapID '6524
                'objPCILogger.EventOriginMethod = "addPartToCart"
                'objPCILogger.Message = "addPartToCart Success."
                'objPCILogger.EventType = EventType.Add
                '6524 end
                '6524 V11 ends
                ''Output1 value return form SAP helpler
                Dim output1 As String = String.Empty
                'cart = sm.AddProductToCartQuickCartExtendedWarranty(carts, sPartNumberUpperCase, sPartQuantity, True, output1, objextwarmodel) '6994
                cart = sm.AddProductToCartQuickCart(carts, sPartNumberUpperCase, sPartQuantity, False, output1) '6994
                Dim bProperpart As Boolean = True
                If Not objSSL.IsCurrentCartSameType(cart) Then
                    differentCart = True
                    objResult.differentCart = True
                End If
                For Each i As ShoppingCartItem In cart.ShoppingCartItems
                    If Not i.Product.PartNumber.Equals(sPartNumberUpperCase) Then
                        HttpContext.Current.Session("carts") = cart
                        objResult.items = cart.ShoppingCartItems.Length
                        objResult.totalAmount = String.Format("{0:c}", cart.ShoppingCartTotalPrice)
                        objResult.success = 2
                        message = "Replacement item " + i.Product.PartNumber + " added to cart in place of requested item  " + sPartNumberUpperCase + " which is no longer available."
                        objResult.message = message
                    Else
                        HttpContext.Current.Session("carts") = cart
                        objResult.items = cart.ShoppingCartItems.Length
                        objResult.totalAmount = String.Format("{0:c}", cart.ShoppingCartTotalPrice)
                        objResult.success = 1

                        message = "Item " + sPartNumberUpperCase + " added to cart."
                        objResult.message = message
                    End If
                Next
            Catch exArg As ArgumentException
                If exArg.ParamName = "SomePartNotFound" Then
                    message = exArg.Message.Replace("Parameter name: SomePartNotFound", "")
                ElseIf exArg.ParamName = "PartNotFound" Then
                    message = exArg.Message.Replace("Parameter name: PartNotFound", "")
                ElseIf exArg.ParamName = "BillingOnlyItem" Then
                    message = exArg.Message.Replace("Parameter name: BillingOnlyItem", "")
                End If
            Catch ex As Exception
                '6524 V11 starts
                objPCILogger.EventOriginApplication = "ServicesPLUS"
                'objPCILogger.EventOriginApplicationLocation = Me.Page.GetType().Name
                objPCILogger.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
                objPCILogger.OperationalUser = Environment.UserName
                objPCILogger.EventDateTime = DateTime.Now.ToLongTimeString()
                objPCILogger.HTTPRequestObjectValues = objPCILogger.GetRequestContextValue()
                objPCILogger.CustomerID = customer.CustomerID
                objPCILogger.CustomerID_SequenceNumber = customer.SequenceNumber
                'objPCILogger.UserName = customer.UserName
                objPCILogger.EmailAddress = customer.EmailAddress '6524
                objPCILogger.SIAM_ID = customer.SIAMIdentity
                objPCILogger.LDAP_ID = customer.LdapID '6524
                objPCILogger.EventOriginMethod = "addPartToCartwSTrainingCatalog"
                objPCILogger.EventType = EventType.View
                '6524 V11 ends
                objPCILogger.IndicationSuccessFailure = "Failure" '6524
                objPCILogger.Message = "addPartToCart Failed. " & ex.Message.ToString() '6524
                objPCILogger.PushLogToMSMQ() '6524'6524 V11
                '7813 starts
                'message = Utilities.WrapExceptionforUI(ex)'7813
                Dim strUniqueKey As String = Utilities.GetUniqueKey
                message = Utilities.WrapExceptionforUINumber(ex, strUniqueKey)
                message = message.Remove(message.Length - 1)
                message = message + strUniqueKey + "."
                '7813 ends
            Finally
                'objPCILogger.PushLogToMSMQ() '6524
            End Try
            objResult.message = message
            Return objResult
        End Function

        ' 2016-06-07 ASleight - This method is no longer used
		'Private Sub LoadGlobalData()
		'    Try
		'        Dim gdobj As GlobalData = Nothing
		'        Dim culinfo As String = String.Empty

		'        If (Session("SelectedLang") IsNot Nothing) Then
		'            Dim ci As New CultureInfo(Session("SelectedLang").ToString())
		'            Page.Culture = ci.ToString()
		'            Page.UICulture = ci.ToString()
		'        Else
		'            HttpContext.Current.Session.Add("SelectedLang", "en-US")
		'            Page.Culture = "en-US"
		'            Page.UICulture = "en-US"
		'        End If

		'        gdobj = New GlobalData()

		'        Dim RequestURL As String = String.Empty
		'        If ConfigurationData.GeneralSettings.Environment = "Development" Then
		'            RequestURL = Request.Url.AbsolutePath
		'            'RequestURL = "https://servicesplus-qa.sony.ca/default.aspx"
		'        Else
		'            RequestURL = HttpContext.Current.Request.ServerVariables("HTTP_X_FORWARDED_HOST")
		'        End If

		'        If RequestURL.Contains("sony.ca") Then
		'            gdobj.SalesOrganization = ConfigurationData.GeneralSettings.GlobalData.SalesOrganization.CA
		'            If Not Session.Item("SelectedLang") Is Nothing Then
		'                culinfo = Session.Item("SelectedLang")
		'                gdobj.Language = culinfo
		'            Else
		'                gdobj.Language = ConfigurationData.GeneralSettings.GlobalData.Language.US
		'            End If
		'        Else
		'            gdobj.SalesOrganization = ConfigurationData.GeneralSettings.GlobalData.SalesOrganization.US
		'            gdobj.Language = ConfigurationData.GeneralSettings.GlobalData.Language.US
		'        End If
		'        gdobj.DistributionChannel = ConfigurationData.GeneralSettings.GlobalData.DistributionChannel.Value
		'        gdobj.Division = ConfigurationData.GeneralSettings.GlobalData.Division.Value

		'        If (gdobj IsNot Nothing) Then
		'            HttpContext.Current.Session.Add("GlobalData", gdobj)
		'        End If
		'    Catch ex As Exception

		'    End Try
		'End Sub

    End Class
    Public Class PartresultTrainingCatalog '6994
        Public _message As String
        Public _totalAmount As String
        Public _items As String
        Public _success As Integer
        Public _quantity As String
        Public _partnumber As String
        Public _differentCart As Boolean

        Property partnumber() As String
            Get
                Return _partnumber
            End Get
            Set(ByVal value As String)
                _partnumber = value
            End Set
        End Property
        Property message() As String
            Get
                Return _message
            End Get
            Set(ByVal value As String)
                _message = value
            End Set
        End Property
        Property Quantity() As String
            Get
                Return _quantity
            End Get
            Set(ByVal value As String)
                _quantity = value
            End Set
        End Property
        ''' <summary>
        ''' 0-Failure
        ''' 1-Success
        ''' 2-warning
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Property success() As Integer
            Get
                Return _success
            End Get
            Set(ByVal value As Integer)
                _success = value
            End Set
        End Property
        Property totalAmount() As String
            Get
                Return _totalAmount
            End Get
            Set(ByVal value As String)
                _totalAmount = value
            End Set
        End Property
        Property items() As String
            Get
                Return _items
            End Get
            Set(ByVal value As String)
                _items = value
            End Set
        End Property

        Property differentCart() As Boolean
            Get
                Return _differentCart
            End Get
            Set(ByVal value As Boolean)
                _differentCart = value
            End Set
        End Property

    End Class
    '6994 ends
End Namespace
