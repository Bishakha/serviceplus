<%@ Reference Page="~/SSL.aspx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="ServicePLUSWebApp.NotLoggedInAddToCatalog" EnableViewStateMAC="true" CodeFile="NotLoggedInAddToCatalog.aspx.vb" CodeFileBaseClass="ServicePLUSWebApp.SSL" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="<%=PageLanguage %>">
	<HEAD>
		<title>ServicesPLUS - Must be Logged in</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="includes/ServicesPLUS_style.css" type="text/css" rel="stylesheet">
		<script language="javascript" src="includes/ServicesPLUS.js" type="text/javascript"></script>
		 <script type="text/javascript" src="includes/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="includes/svcplus_globalization.js"></script>
	</HEAD>
	<body bgcolor="#ffffff" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
		<form id="Form1" method="post" runat="server">
			<table width="400" border="0" cellpadding="0">
				<tr>
					<td background="images/sp_int_popup_login_hdrbkgd.gif"><img src="images/spacer.gif" width="250" height="55"></td>
				</tr>
				<tr>
					<td height="30"><img src="images/spacer.gif" width="250" height="30"></td>
				</tr>
				<tr>
					<td align="left">
						<table border="0" width="370">
							<tr>
								<td class="bodyCopyBold" width="20"></td>
								<td class="bodyCopyBold"><asp:Label id="MessageLabel" runat="server" CssClass="redAsterick"></asp:Label></td>
								<td class="bodyCopyBold" width="20"></td>
							</tr>
							<tr>
								<td width="20"></td>
								<td class="bodyCopyBold" align="right"><asp:ImageButton id="ImageButton1" runat="server" ImageUrl="<%$Resources:Resource,img_btnCloseWindow%>"
										AlternateText="Close Window"></asp:ImageButton></td>
								<td width="20"></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td class="bodyCopyBold" height="30"><img src="images/spacer.gif" width="250" height="30"></td>
				</tr>
			</table>
		</form>
	</body>
</html>
