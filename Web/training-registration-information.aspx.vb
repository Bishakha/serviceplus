Imports ServicesPlusException
Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Process


Namespace ServicePLUSWebApp


    Partial Class training_registration_information
        Inherits SSL

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub


        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Public strQuote As String = String.Empty

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            If Not IsPostBack Then
                Dim debugMessage As String = $"Training-registration-information.Page_Load - START at {DateTime.Now.ToShortTimeString}{Environment.NewLine}"

                Try
                    Dim cm As New CourseManager
                    Dim cr As CourseReservation
                    Dim customer As Customer = HttpContextManager.Customer
                    Dim order As Order
                    Dim courseSelected As CourseSchedule = Nothing
                    Dim courseType As New CourseType
                    Dim IsNonClass As Integer
                    Dim budgetCode As String = ""
                    Dim itemNumber As Integer = -1

                    If Session("trainingreservation") Is Nothing Then
                        ErrorLabel.Text = ("No course reservation found. Your session may have expired.")
                        ErrorLabel.Visible = True
                        labelAction.Visible = False
                        Return
                    End If
                    cr = Session("trainingreservation")
                    debugMessage &= $"- Training Reservation variable pulled. Calling CourseManager.Checkout{Environment.NewLine}"
                    order = cm.Checkout(customer, Nothing, Nothing) ' Session("trainingorder")

                    order.HTTP_X_Forward_For = HttpContextManager.GetServerVariableValue("HTTP_X_FORWARDED_FOR")
                    order.RemoteADDR = HttpContextManager.GetServerVariableValue("REMOTE_ADDR")
                    order.HTTPReferer = HttpContextManager.GetServerVariableValue("HTTP_REFERER")
                    order.HTTPURL = HttpContextManager.GetServerVariableValue("HTTP_URL")
                    order.HTTPUserAgent = HttpContextManager.GetServerVariableValue("HTTP_USER_AGENT")

                    If String.IsNullOrWhiteSpace(order.HTTPURL) Then order.HTTPURL = HttpContextManager.GetServerVariableValue("URL") + ReturnQueryString()
                    If Not String.IsNullOrWhiteSpace(order.Customer?.SIAMIdentity) Then cr.CustomerSIAMID = order.Customer.SIAMIdentity
                    If String.IsNullOrWhiteSpace(order.POReference) Then order.POReference = "NO-PO-REQ"

                    If Not String.IsNullOrWhiteSpace(cr.CourseSchedule?.PartNumber) And Not cr.CourseSchedule?.PartNumber.Trim() = "-" Then
                        cr.CourseSchedule.PartNumber = cm.CheckClassValidity(cr.CourseSchedule.PartNumber)
                    Else
                        cr.CourseSchedule.PartNumber = "TRAINTEMP"
                    End If
                    debugMessage &= $"- Order and Reservation parameters set. PartNumber: {cr.CourseSchedule.PartNumber}{Environment.NewLine}"

                    ' 2018-07-18 Removing payment from Training 
                    'If order.CreditCard IsNot Nothing Then
                    '    If order.CreditCard.Action = CoreObject.ActionType.ADD Then
                    '        Try
                    '            Dim custManager As New CustomerManager()
                    '            custManager.UpdateCustomerCreditCard(order.Customer)
                    '            custManager = Nothing
                    '        Catch ex As Exception
                    '            ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                    '            Return
                    '        End Try
                    '    End If
                    'End If

                    cm.CompleteOrder(order, cr)
                    debugMessage &= $"- CourseManager.CompleteOrder completed. Order Number: {order.OrderNumber} {Environment.NewLine}"

                    Try
                        If Request.QueryString("nonclass") IsNot Nothing Then
                            If Request.QueryString("nonclass") = "1" Then
                                IsNonClass = 1
                                If Request.QueryString("budgetcode") IsNot Nothing Then
                                    budgetCode = Request.QueryString("budgetcode")
                                End If
                            Else
                                IsNonClass = 0
                                budgetCode = ""
                            End If
                        End If
                    Catch ex As Exception
                        IsNonClass = 0
                        budgetCode = ""
                    End Try

                    If (Request.QueryString("ItemNumber") IsNot Nothing) And (Session.Item("courseschedule") IsNot Nothing) Then
                        itemNumber = Convert.ToInt32(Request.QueryString("ItemNumber"))
                        courseSelected = CType(Session.Item("courseschedule"), ArrayList)(itemNumber)
                    End If
                    debugMessage &= $"- IsNonClass = {IsNonClass}, BudgetCode = {budgetCode}, CourseSelected = {courseSelected?.PartNumber}{Environment.NewLine}"

                    'added to get the type code, based on the type code the emails are to be generated
                    'added by Deepa V feb 14,2006
                    courseType.PartNumber = cr.CourseSchedule.PartNumber()
                    cm.GetCourseType(courseType)
                    debugMessage &= $"- Course Type = {courseType.Code}{Environment.NewLine}"

                    If (courseType.Code <> 0) Then
                        labelAction.Text = "Training order confirmation will been sent via email to you."
                    Else
                        If cr.ParticipantsStatus() = 2 Then
                            lblSubHdr.InnerText = "Quote"
                            labelAction.Text = "Your quote has been sent to you via email."
                            Label1.Text = "Quote Number"
                        Else
                            labelAction.Text = "Training order confirmation will been sent via email to you."
                        End If
                    End If


                    Try
                        labelAction.Text = ""
                        If (courseType.Code <> 0) Then
                            Response.Write("<script language='javascript'>window.document.title='Sony Training Institute � Training Order Information'</script>")
                            cm.SendNonClassroomNotificationEMail(cr, order, "smtp_server", budgetCode)
                            'Me.imgHeader.Src = "images/sp_int_subhd_orderTrainingSuccess.gif"
                            lblSubHdr.InnerText = "Order Success"
                        Else
                            debugMessage &= $"- Sending Email{Environment.NewLine}"
                            cm.SendNotificationEMail(cr, order, "smtp_server")
                        End If
                    Catch ex As Exception
                        ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                    End Try

                    If (courseType.Code <> 0) Then
                        labelAction.Text = "Training order confirmation will been sent via email to you."
                    ElseIf cr.Participants.Length > 1 Then
                        labelAction.Text = "Registration details have been sent via email to you and the student(s)."
                    ElseIf order.Customer.EmailAddress <> cr.Participants(0).EMail Then
                        labelAction.Text = "Registration details have been sent via email to you and the student."
                    End If

                    LabelOrderNumber.Text = order.OrderNumber
                    LabelCourseNumber.Text = cr.CourseSchedule.Course.Number
                    LabelCourseTitle.Text = cr.CourseSchedule.Course.Title
                    If Not (courseType.Code <> 0) Then
                        debugMessage &= $"- Setting Labels for reservation values.{Environment.NewLine}"
                        LabelStartDate.Text = cr.CourseSchedule.StartDate.ToString("MM/dd/yyyy")
                        If LabelStartDate.Text = NullDateTime Then LabelStartDate.Text = "TBD"

                        LabelEndDate.Text = cr.CourseSchedule.EndDate.ToString("MM/dd/yyyy")
                        If LabelEndDate.Text = NullDateTime Then LabelEndDate.Text = "TBD"

                        lblTrainingTime.Text = cr.CourseSchedule.Time.ToString()
                        If String.IsNullOrWhiteSpace(lblTrainingTime.Text) Then lblTrainingTime.Text = "TBD"

                        LabelLocationAddr1.Text = cr.CourseSchedule.Location.Address1
                        LabelLocationAddr2.Text = cr.CourseSchedule.Location.Address2
                        LabelLocationCityStateZip.Text = cr.CourseSchedule.Location.CityStateZip
                        LabelLocationCountry.Text = cr.CourseSchedule.Location.Country.TerritoryName
                        LabelLocationName.Text = cr.CourseSchedule.Location.Name

                    Else 'added else condition for non classroom training
                        Label4.Visible = False
                        Label5.Visible = False
                        Label6.Visible = False

                        LabelLocationAddr1.Visible = False
                        LabelLocationAddr2.Visible = False
                        LabelLocationCityStateZip.Visible = False
                        LabelLocationCountry.Visible = False
                        LabelLocationName.Visible = False
                        trStartDate.Visible = False
                        trEndDate.Visible = False
                        trTrainingTime.Visible = False
                    End If

                    'participants
                    ' again display the table only if it is a classroom training
                    ' added by Deepa V feb 17,2006
                    If Not (courseType.Code <> 0) Then
                        TableParticipant.Rows.Clear()
                        Dim TR As New TableRow
                        Dim TD As New TableCell

                        '-- open table
                        TD.Controls.Add(New LiteralControl("<table border=""0"" cellpadding=""0"" cellspacing=""0"" width=""445"">"))

                        '-- header line 
                        TD.Controls.Add(New LiteralControl("<tr bgcolor=""#d5dee9"">" + vbCrLf))
                        TD.Controls.Add(New LiteralControl("<td width=""1"" bgColor=""#d5dee9""><img src=""images/spacer.gif"" width=""1"" height=""35"" alt=""""></td>" + vbCrLf))
                        TD.Controls.Add(New LiteralControl("<td class=""tableHeader"" width=""160"" bgColor=""#d5dee9"" align=""center"" height=""35"">Company</td>" + vbCrLf))
                        TD.Controls.Add(New LiteralControl("<td width=""1"" bgColor=""#d5dee9"" height=""28""><img src=""images/spacer.gif"" width=""1"" height=""35"" alt=""""></td>" + vbCrLf))
                        TD.Controls.Add(New LiteralControl("<td class=""tableHeader"" width=""110"" bgColor=""#d5dee9"" align=""center"" height=""28"">First Name</td>" + vbCrLf))
                        TD.Controls.Add(New LiteralControl("<td width=""1"" bgColor=""#d5dee9"" height=""35""><img src=""images/spacer.gif"" width=""1"" height=""35"" alt=""""></td>" + vbCrLf))
                        TD.Controls.Add(New LiteralControl("<td class=""tableHeader"" align=""center"" width=""110"" bgColor=""#d5dee9"" height=""35"">Last Name" + vbCrLf))
                        TD.Controls.Add(New LiteralControl("<td width=""1"" bgColor=""#d5dee9"" height=""35""><img src=""images/spacer.gif"" width=""1"" height=""28"" alt=""""></td>" + vbCrLf))

                        TD.Controls.Add(New LiteralControl("<td class=""tableHeader"" align=""center"" width=""60"" bgColor=""#d5dee9"" height=""35"">Status</td>" + vbCrLf))
                        TD.Controls.Add(New LiteralControl("<td width=""1"" bgColor=""#d5dee9"" height=""35""><img src=""images/spacer.gif"" width=""1"" height=""28"" alt=""""></td>" + vbCrLf))
                        TD.Controls.Add(New LiteralControl("</tr>"))

                        '-- spacer line 
                        TD.Controls.Add(New LiteralControl(getSpacerRow))

                        For Each cp As CourseParticipant In cr.Participants
                            TD.Controls.Add(New LiteralControl("<TR>"))
                            TD.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>"))

                            '-- Company 
                            TD.Controls.Add(New LiteralControl("<td bgcolor=""#f2f5f8"" width=""160"" class=""tableData"" align=""center"">"))
                            TD.Controls.Add(New LiteralControl(cp.Company))

                            TD.Controls.Add(New LiteralControl("</td>"))
                            TD.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>"))
                            '-- First Name
                            TD.Controls.Add(New LiteralControl("<td bgcolor=""#f2f5f8"" width=""110"" class=""tableData"" align=""center"">" + cp.FirstName + "</td>"))
                            TD.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>"))
                            '-- Last Name
                            TD.Controls.Add(New LiteralControl("<td bgcolor=""#f2f5f8"" width=""110"" class=""tableData"" align=""center"">" + cp.LastName + "</td>"))
                            TD.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>"))
                            '-- Status
                            'no status for non classroom training added by Deepa V Feb 17,2006
                            If Not (courseType.Code <> 0) Then
                                TD.Controls.Add(New LiteralControl("<td bgcolor=""#f2f5f8"" width=""60"" class=""tableData"" align=""center"">" + cp.StatusDisplay + "</td>"))
                                TD.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>"))
                            End If
                        Next

                        TD.Controls.Add(New LiteralControl("</table>"))
                        TR.Cells.Add(TD)
                        TableParticipant.Rows.Add(TR)
                    End If
                    debugMessage &= $"- Event completed okay.{Environment.NewLine}"
                Catch ex As Exception
                    Utilities.LogMessages(debugMessage)
                    ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                End Try

                'clear the completed order and reservation to prevent it from being changed
                Session.Remove("trainingreservation")
                Session.Remove("trainingorder")
            End If
        End Sub

        Private Function getSpacerRow() As String
            Dim row As New StringBuilder

            row.Append("<tr>" + vbCrLf)
            row.Append("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf)
            row.Append("<td bgcolor=""#ffffff"" width=""160""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf)
            row.Append("<td bgcolor=""#ffffff"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf)
            row.Append("<td bgcolor=""#ffffff"" width=""110""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf)
            row.Append("<td bgcolor=""#ffffff"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf)
            row.Append("<td bgcolor=""#ffffff"" width=""110""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf)
            row.Append("<td bgcolor=""#ffffff"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf)
            row.Append("<td bgcolor=""#ffffff"" width=""60""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf)
            row.Append("<td bgcolor=""#ffffff"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf)
            row.Append("</tr>" + vbCrLf)

            Return row.ToString()
        End Function
    End Class

End Namespace
