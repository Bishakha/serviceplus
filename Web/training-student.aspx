<%@ Reference Page="~/SSL.aspx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="SonyFooter" Src="SonyFooter.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="PersonalMessage" Src="PersonalMessage.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="ServicePlusNav" Src="ServicePlusNav.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="TopSonyHeader" Src="TopSonyHeader.ascx" %>

<%@ Page Language="vb" AutoEventWireup="false" Inherits="ServicePLUSWebApp.training_student" CodeFile="training-student.aspx.vb" CodeFileBaseClass="ServicePLUSWebApp.SSL" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="<%=PageLanguage %>">
<head>
    <title>Sony Training Institute � Student</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <link href="includes/ServicesPLUS_style.css" type="text/css" rel="stylesheet">
    <script language="javascript" src="includes/ServicesPLUS.js" type="text/javascript"></script>
    <script type="text/javascript" src="includes/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="includes/svcplus_globalization.js"></script>

</head>
<body style="margin: 0px; background: #5d7180; color: Black;">
    <center>
        <form id="frm4" method="post" runat="server">
            <table width="760" border="0" role="presentation">
                <tr>
                    <td width="25" style="background: url('images/sp_left_bkgd.gif')">
                        <img height="25" src="images/spacer.gif" width="25" alt="">
                    </td>
                    <td width="710" bgcolor="#ffffff">
                        <table width="710" border="0" role="presentation">
                            <tr>
                                <td>
                                    <table width="710" border="0" role="presentation">
                                        <tr>
                                            <td width="391">
                                                <ServicePLUSWebApp:TopSonyHeader ID="TopSonyHeader" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <nav><ServicePLUSWebApp:ServicePlusNav ID="SonyNav" runat="server" /></nav>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table width="710" border="0" role="presentation">
                                                    <tr>
                                                        <td width="464" bgcolor="#363d45" height="1" background="images/sp_int_header_top_ServicesPLUS_onepix.gif"
                                                            align="right" valign="top">
                                                            <br />
                                                            <h1 class="headerText">ServicesPLUS&nbsp;&nbsp;</h1>
                                                        </td>
                                                        <td valign="top" bgcolor="#363d45">
                                                            <ServicePLUSWebApp:PersonalMessage ID="PersonalMessageDisplay" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td bgcolor="#f2f5f8">
                                                            <h2 class="headerTitle" style="padding-right: 20px; text-align: right;">Student Information</h2>
                                                        </td>
                                                        <td bgcolor="#99a8b5">&nbsp;</td>
                                                    </tr>
                                                    <tr height="9">
                                                        <td bgcolor="#f2f5f8">
                                                            <img height="9" src="images/sp_int_header_btm_left_onepix.gif" width="464" alt="">
                                                        </td>
                                                        <td bgcolor="#99a8b5">
                                                            <img height="9" src="images/sp_int_header_btm_right.gif" width="246" alt="">
                                                        </td>
                                                    </tr>
                                                </table>
                                                <img src="images/spacer.gif" width="20" alt="">
                                                <asp:Label ID="lblError" runat="server" CssClass="redAsterick" />
                                                <asp:Label ID="lblErrorValidation" runat="server" CssClass="redAsterick" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table width="710" border="0" role="presentation">
                                                    <tr>
                                                        <td width="20" height="20">
                                                            <img height="20" src="images/spacer.gif" width="20" alt="">
                                                        </td>
                                                        <td width="670" height="20">
                                                            <img height="20" src="images/spacer.gif" width="670" alt="">
                                                        </td>
                                                        <td width="20" height="20">
                                                            <img height="20" src="images/spacer.gif" width="20" alt="">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="20">
                                                            <img height="20" src="images/spacer.gif" width="20" alt="">
                                                        </td>
                                                        <td valign="top" width="670">

                                                            <table cellpadding="3" width="664" border="0" role="presentation">
                                                                <tr>
                                                                    <td width="500" colspan="3">
                                                                        <span class="tableData"><strong>Please complete required fields 
																					below marked with an asterisk.</strong></span>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 133px" width="133" colspan="3">
                                                                        &nbsp;<asp:CheckBox ID="CheckBoxCopyProfile" runat="server" CssClass="tableData" Width="328px"
                                                                            Text="Copy information from your ServicesPLUS profile" AutoPostBack="True" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="tableData" style="width: 81px" align="right" width="81">
                                                                        <label for="txtCompanyName">Company: </label>
                                                                    </td>
                                                                    <td width="572">
                                                                        <SPS:SPSTextBox ID="txtCompanyName" runat="server" CssClass="tableData" Width="184px"
                                                                            MaxLength="100" />
                                                                        <span class="redAsterick">*</span>
                                                                        <asp:RequiredFieldValidator ID="vldCompanyName" ErrorMessage="You must enter a valid Company Name."
                                                                            ControlToValidate="txtCompanyName" CssClass="redAsterick" Display="Dynamic" runat="server" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="tableData" style="width: 81px" align="right" width="81" height="32">
                                                                        <label for="txtFirstName">First Name: </label>
                                                                    </td>
                                                                    <td width="572" height="32">
                                                                        <SPS:SPSTextBox ID="txtFirstName" runat="server" CssClass="tableData" Width="184px" MaxLength="100" />
                                                                        <span class="redAsterick">*</span>
                                                                        <asp:RequiredFieldValidator ID="vldFirstName" ErrorMessage="You must enter a First Name."
                                                                            ControlToValidate="txtFirstName" CssClass="redAsterick" Display="Dynamic" runat="server" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="tableData" style="width: 81px" align="right" width="81" height="32">
                                                                        <label for="txtLastName">Last Name: </label>
                                                                    </td>
                                                                    <td width="572" height="32">
                                                                        <SPS:SPSTextBox ID="txtLastName" runat="server" CssClass="tableData" Width="184px" MaxLength="100" />
                                                                        <span class="redAsterick">*</span>
                                                                        <asp:RequiredFieldValidator ID="vldLastName" ErrorMessage="You must enter a Last Name."
                                                                            ControlToValidate="txtLastName" CssClass="redAsterick" Display="Dynamic" runat="server" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="tableData" style="width: 81px" align="right" width="81">
                                                                        <label for="txtAddress1">Street Address: </label>
                                                                    </td>
                                                                    <td width="572">
                                                                        <SPS:SPSTextBox ID="txtAddress1" runat="server" CssClass="tableData" Width="184px" MaxLength="35" />
                                                                        <span class="redAsterick">*</span>
                                                                        <asp:RequiredFieldValidator ID="vldAddress1" ErrorMessage="You must enter a valid Address."
                                                                            ControlToValidate="txtAddress1" CssClass="redAsterick" Display="Dynamic" runat="server" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="tableData" style="width: 81px" align="right" width="81" height="31">
                                                                        <label for="txtAddress2">Address 2nd Line: </label>
                                                                    </td>
                                                                    <td width="572" height="31">
                                                                        <SPS:SPSTextBox ID="txtAddress2" runat="server" CssClass="tableData" Width="184px" MaxLength="35" />
                                                                        <asp:Label ID="lblLine2Star" runat="server" CssClass="redAsterick" />
                                                                    </td>
                                                                </tr>
                                                                <tr runat="server" visible="false">
                                                                    <td class="tableData" style="width: 81px" align="right" width="81" height="31">
                                                                        <label for="txtAddress3">Address 3rd Line: </label>
                                                                    </td>
                                                                    <td width="572" height="31">
                                                                        <SPS:SPSTextBox ID="txtAddress3" runat="server" CssClass="tableData" Width="184px" MaxLength="35" />
                                                                        <asp:Label ID="lblLine3Star" runat="server" CssClass="redAsterick" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="tableData" style="width: 81px" align="right" width="81">
                                                                        <label for="txtCity">City: </label>
                                                                    </td>
                                                                    <td width="572">
                                                                        <SPS:SPSTextBox ID="txtCity" runat="server" CssClass="tableData" Width="184px" MaxLength="35" />
                                                                        <span class="redAsterick">*</span>
                                                                        <asp:RequiredFieldValidator ID="vldCity" ErrorMessage="You must enter a City."
                                                                            ControlToValidate="txtCity" CssClass="redAsterick" Display="Dynamic" runat="server" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="tableData" style="width: 81px" align="right" width="81" height="29">
                                                                        <label for="ddlState">State: </label>
                                                                    </td>
                                                                    <td width="572" height="29">
                                                                        <asp:DropDownList ID="ddlState" runat="server" CssClass="tableData" />
                                                                        <span class="redAsterick">*</span>
                                                                        <asp:RequiredFieldValidator ID="vldState" ErrorMessage="You must select a State."
                                                                            ControlToValidate="ddlState" CssClass="redAsterick" Display="Dynamic" runat="server" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="tableData" style="width: 81px" align="right" width="81" height="30">
                                                                        <label for="txtZip">Zip: </label>
                                                                    </td>
                                                                    <td width="572" height="30">
                                                                        <SPS:SPSTextBox ID="txtZip" runat="server" CssClass="tableData" MaxLength="10" />
                                                                        <span class="redAsterick">*</span>
                                                                        <asp:RegularExpressionValidator ID="vldZipCode" ControlToValidate="txtZip" Display="Dynamic"
                                                                            ErrorMessage="You have entered an invalid Zip Code." CssClass="redAsterick"
                                                                            ValidationExpression="^\d{5}(-\d{4})?$" runat="server" />
                                                                        <asp:RequiredFieldValidator ID="reqZipCode" ControlToValidate="txtZip" Display="Dynamic"
                                                                            ErrorMessage="You must enter a Zip Code." CssClass="redAsterick" runat="server" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="tableData" style="width: 81px" align="right" width="81">
                                                                        <label for="txtPhone">Phone Number: </label>
                                                                    </td>
                                                                    <td width="266">
                                                                        <SPS:SPSTextBox ID="txtPhone" runat="server" CssClass="tableData" Width="100px" />
                                                                        <span class="redAsterick">*</span>
                                                                        <asp:RegularExpressionValidator ID="vldPhone" ControlToValidate="txtPhone" Display="Dynamic"
                                                                            ErrorMessage="Phone number must be in the following format: 123-555-1234."
                                                                            ValidationExpression="^\(?([0-9]{3})\)?[-]([0-9]{3})[-]([0-9]{4})$" CssClass="redAsterick" runat="server" />
                                                                        <asp:RequiredFieldValidator ID="reqPhone" ControlToValidate="txtPhone"
                                                                            ErrorMessage="You must enter a Phone Number."
                                                                            Display="Dynamic" CssClass="redAsterick" runat="server" />
                                                                        &nbsp;&nbsp;<span class="bodyCopy">xxx-xxx-xxxx</span>

                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="tableData" style="width: 81px" align="right" width="81">
                                                                        <label for="txtExtension">Extension: </label>
                                                                    </td>
                                                                    <td width="266">
                                                                        <SPS:SPSTextBox ID="txtExtension" runat="server" CssClass="tableData" Width="64px" MaxLength="4" />
                                                                        &nbsp;&nbsp;<span class="bodyCopy">xxxx</span>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="tableData" style="width: 81px" align="right" width="81">
                                                                        <label for="txtFax">Fax Number: </label>
                                                                    </td>
                                                                    <td width="266">
                                                                        <SPS:SPSTextBox ID="txtFax" runat="server" CssClass="tableData" Width="100px" />
                                                                        &nbsp;&nbsp;<span class="bodyCopy">xxx-xxx-xxxx</span>
                                                                        <asp:RegularExpressionValidator ID="vldFax" ControlToValidate="txtFax" Display="Dynamic"
                                                                            ErrorMessage="You have entered an invalid Fax number." CssClass="redAsterick"
                                                                            ValidationExpression="^\(?([0-9]{3})\)?[-]([0-9]{3})[-]([0-9]{4})$" runat="server" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="tableData" style="width: 81px" align="right" width="81">
                                                                        <label id="lblEMail" for="txtEmail" runat="server" class="tableData">E-Mail:</label>
                                                                    </td>
                                                                    <td width="266">
                                                                        <SPS:SPSTextBox ID="txtEMail" runat="server" CssClass="tableData" Width="100px" />
                                                                        <span class="redAsterick">*</span>
                                                                        <asp:RequiredFieldValidator ID="reqEmail" ControlToValidate="txtEmail" Display="Dynamic"
                                                                            ErrorMessage="You must enter an Email Address." CssClass="redAsterick" runat="server" />
                                                                        <asp:RegularExpressionValidator ID="vldEmail" ControlToValidate="txtEmail" Display="Dynamic"
                                                                            ErrorMessage="You have entered an invalid Email Address." CssClass="redAsterick"
                                                                            ValidationExpression="^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$" runat="server" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <br>
                                                            <table cellpadding="3" width="664" border="0" role="presentation">
                                                                <tr>
                                                                    <td class="tableData" align="right" width="92">&nbsp;</td>
                                                                    <td valign="top" colspan="2"></td>
                                                                    <td valign="top" width="210">&nbsp;</td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="right" width="84">
                                                                        <img height="1" src="images/spacer.gif" width="1" alt="">
                                                                    </td>
                                                                    <td width="572">
                                                                        <table width="240" border="0" role="presentation">
                                                                            <tr>
                                                                                <td width="5"></td>
                                                                                <td>
                                                                                    <asp:ImageButton ID="btnNext" runat="server" ImageUrl="images/sp_int_next_btn.gif" AlternateText="Next" />
                                                                                </td>
                                                                                <td>
                                                                                    <asp:ImageButton ID="btnCancel" runat="server" ImageUrl="images/sp_int_cancel_btn.gif" AlternateText="Cancel" />
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                    <td valign="top" width="54">&nbsp;</td>
                                                                    <td valign="top" width="210">
                                                                        <img height="1" src="images/spacer.gif" width="1" alt="">
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td width="20">
                                                            <img height="20" src="images/spacer.gif" width="20" alt="">
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>

                                            <td>
                                                <footer><ServicePLUSWebApp:SonyFooter ID="SonyFooter1" runat="server" /></footer>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="25" style="background: url('images/sp_right_bkgd.gif')">
                        <img height="20" src="images/spacer.gif" width="25" alt="">
                    </td>
                </tr>
            </table>
        </form>
    </center>
</body>
</html>
