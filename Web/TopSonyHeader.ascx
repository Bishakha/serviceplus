<%@ Register TagPrefix="ServicePLUSWebApp" TagName="TopNav" Src="TopNav.ascx" %>
<%@ Control Language="vb" AutoEventWireup="false" Inherits="ServicePLUSWebApp.TopSonyHeader" CodeFile="TopSonyHeader.ascx.vb" %>
<table style="background-color: #000; width: 100%;" role="presentation">
    <tr>
        <td>
            <table style="width: 100%" role="presentation">
                <tr>
                    <td colspan="3">
                        <table id="tblWBorder" style="height: 60px; width: 100%; border: 5px solid red;" role="presentation" runat="server">
                            <tr>
                                <td style="width: 70%; padding: 5px;">
                                    <asp:Label ID="lblEnvWarning" runat="server" />
                                </td>
                                <td style="width: 30%; padding: 5px;">
                                    <asp:Label ID="lblEnvDetails" runat="server" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="width: 50%;">
                        <img src="images/hdrlogo_sonymb.jpg" width="123" height="66" alt="Sony Logo">
                    </td>
                    <td style="width: 20%; vertical-align: middle; text-align: right; padding-right: 10px;">
                        <img src="/images/american_flag.jpg" id="img_flag" runat="server" alt="" />
                    </td>
                    <td style="width: 30%; vertical-align: middle; text-align: left;">
                        <span class="headerText" style="color: white"><%=Resources.Resource.header_serviceplus%></span><br />
                        <span class="headerTitle" style="color: white"><%=Resources.Resource.header_forprefssional%></span>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <ServicePLUSWebApp:TopNav ID="Topnav2" runat="server" />
        </td>
    </tr>
</table>
