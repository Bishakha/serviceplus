﻿
Imports ServicePLUSWebApp
Imports ServicesPlusException
Imports Sony.US.ServicesPLUS.Core

Partial Class sony_service_agreement_complete
    Inherits SSL

    Public isColorRow As Boolean = False
    Public isColorProductRow As Boolean = False
    Public agreement As ServiceAgreement
    Public userAddress As String

    Private Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try
            If Session("ResellerAgreement") Is Nothing Then
                lblError.Text = "Your agreement details could not be found. Your browser session may have been lost." &
                    "If you don't receive your email within 2 hours, please check your Spam folder, then try submitting this form again."
                Return
            End If
            agreement = Session("ResellerAgreement")

            ' Build the User Address
            If Not String.IsNullOrWhiteSpace(agreement?.Address1) Then userAddress &= $"{agreement.Address1}<br/>"
            If Not String.IsNullOrWhiteSpace(agreement?.Address2) Then userAddress &= $"{agreement.Address2}<br/>"
            If Not String.IsNullOrWhiteSpace(agreement?.Address3) Then userAddress &= $"{agreement.Address3}<br/>"
            If Not String.IsNullOrWhiteSpace(agreement?.City) Then userAddress &= $"{agreement.City}, "
            If Not String.IsNullOrWhiteSpace(agreement?.State) Then userAddress &= $"{agreement.State} "
            If Not String.IsNullOrWhiteSpace(agreement?.ZipCode) Then userAddress &= $"{agreement.ZipCode}<br/>"
        Catch ex As Exception
            lblError.Text = Utilities.WrapExceptionforUI(ex)
        End Try
    End Sub
End Class
