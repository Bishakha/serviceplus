Imports Sony.US.SIAMUtilities

Namespace ServicePLUSWebApp

    Partial Class TopSonyHeader
        Inherits UserControl

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub


        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
            If (Not Page.IsPostBack) Then
                'Dim RequestURL As String = String.Empty
                'If ConfigurationData.GeneralSettings.Environment = "Development" Then
                '	'For Canada: Comment the below line  and uncomment the next. For US just viseversa. Note: Its only for local devbox
                '	RequestURL = Request.Url.AbsolutePath
                '	'RequestURL = "https://servicesplus-qa.sony.ca/default.aspx"
                'Else
                '	RequestURL = HttpContext.Current.Request.ServerVariables("HTTP_X_FORWARDED_HOST")
                'End If

                ' Switch the US Flag to a Canadian one when applicable.
                If HttpContextManager.GlobalData.IsCanada Then img_flag.Src = "/images/Canadian-Flag.jpg"
            End If

            ' Prepare the Environment warning label at the top
            Dim strCurrentEnvironment As String = ConfigurationData.GeneralSettings.Environment.Trim()

            If strCurrentEnvironment.ToLower() <> "production" Then
                Dim strConnString As String = ConfigurationData.Environment.DataBase.ConnectionString
                Dim strSplit() As String = Split(strConnString, ";")
                Dim strDBPortion() As String = Split(strSplit(0), "=")
                Dim strDBValue As String = strDBPortion(1).ToLower()
                Dim strSiteUrl As String = IIf(HttpContextManager.GlobalData.IsAmerica,
                                               ConfigurationData.GeneralSettings.GlobalData.ProductionUrl.US,
                                               ConfigurationData.GeneralSettings.GlobalData.ProductionUrl.CA)

                lblEnvWarning.Text = $"<span class=""popupRedPrice"">THIS SITE IS FOR TESTING ONLY.</span><br/>
                                        <span class=""bodyCopySM"" style=""color: white;""> The production site is at <a href=""{strSiteUrl}"" style=""color: #DDD;"">{strSiteUrl}</a></span>"
                lblEnvDetails.Text = $"<span class=""bodyCopySM"" style=""color: white;""> Build No.: {Application("Build")}<br/>
                                        Environment: {strCurrentEnvironment}<br/>
                                        DB: {strDBValue}</span>"
            Else
                'lblEnvWarning.Visible = False
                'lblEnvWarning.Text = String.Empty
                'lblEnvDetails.Visible = False
                'lblEnvDetails.Text = String.Empty
                tblWBorder.Visible = False
            End If

            Dim url = IO.Path.GetFileName(HttpContext.Current.Request.Url.AbsolutePath.ToString()).ToLower()
            If url = "register-account-tax-exempt.aspx" Or url = "register-existing-user.aspx" Or url = "register-notifications.aspx" _
                Or url = "registration-thank-you.aspx" Or url = "marketing.aspx" Then
                Topnav2.Visible = False
            End If
        End Sub

        Public WriteOnly Property ShowDefaultIcon() As Boolean
            Set(ByVal Value As Boolean)
                Topnav2.Visible = Value
            End Set
        End Property
    End Class
End Namespace
