Namespace ServicePLUSWebApp

Partial Class RASuccess
    Inherits SSL

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents ErrorLabel As System.Web.UI.WebControls.Label
    Protected WithEvents ErrorValidation As System.Web.UI.WebControls.Label
    Protected WithEvents LabelFN As System.Web.UI.WebControls.Label
        Protected WithEvents FirstName As Sony.US.ServicesPLUS.Controls.SPSTextBox
        Protected WithEvents LabelLN As System.Web.UI.WebControls.Label
        Protected WithEvents LastName As Sony.US.ServicesPLUS.Controls.SPSTextBox
        Protected WithEvents LabelEmail As System.Web.UI.WebControls.Label
        Protected WithEvents Email As Sony.US.ServicesPLUS.Controls.SPSTextBox
        Protected WithEvents LabelCO As System.Web.UI.WebControls.Label
        Protected WithEvents CompanyName As Sony.US.ServicesPLUS.Controls.SPSTextBox
        Protected WithEvents LabelLine1 As System.Web.UI.WebControls.Label
        Protected WithEvents Line1 As Sony.US.ServicesPLUS.Controls.SPSTextBox
        Protected WithEvents Line2 As Sony.US.ServicesPLUS.Controls.SPSTextBox
        Protected WithEvents LabelCity As System.Web.UI.WebControls.Label
        Protected WithEvents City As Sony.US.ServicesPLUS.Controls.SPSTextBox
        Protected WithEvents LabelState As System.Web.UI.WebControls.Label
        Protected WithEvents DropDownList1 As System.Web.UI.WebControls.DropDownList
        Protected WithEvents LabelZip As System.Web.UI.WebControls.Label
        Protected WithEvents Zip As Sony.US.ServicesPLUS.Controls.SPSTextBox
        Protected WithEvents LabelPhone As System.Web.UI.WebControls.Label
        Protected WithEvents Phone As Sony.US.ServicesPLUS.Controls.SPSTextBox
        Protected WithEvents Label1 As System.Web.UI.WebControls.Label
        Protected WithEvents DropDownList2 As System.Web.UI.WebControls.DropDownList
        Protected WithEvents LabelOOrderNumber As System.Web.UI.WebControls.Label
        Protected WithEvents OOrderNumber As Sony.US.ServicesPLUS.Controls.SPSTextBox
        Protected WithEvents LabelOInvoiceNumber As System.Web.UI.WebControls.Label
        Protected WithEvents OInvoiceNumber As Sony.US.ServicesPLUS.Controls.SPSTextBox
        Protected WithEvents Label4 As System.Web.UI.WebControls.Label
        Protected WithEvents PartNumber As Sony.US.ServicesPLUS.Controls.SPSTextBox
        Protected WithEvents Label5 As System.Web.UI.WebControls.Label
        Protected WithEvents Price As Sony.US.ServicesPLUS.Controls.SPSTextBox
        Protected WithEvents LabelReason As System.Web.UI.WebControls.Label
        Protected WithEvents Reason As System.Web.UI.WebControls.DropDownList
        Protected WithEvents Label7 As System.Web.UI.WebControls.Label
        Protected WithEvents RadioButtonList1 As System.Web.UI.WebControls.RadioButtonList
        Protected WithEvents Label8 As System.Web.UI.WebControls.Label
        Protected WithEvents SerialNumber As Sony.US.ServicesPLUS.Controls.SPSTextBox
    Protected WithEvents ImageButton1 As System.Web.UI.WebControls.ImageButton


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
    End Sub

End Class

End Namespace
