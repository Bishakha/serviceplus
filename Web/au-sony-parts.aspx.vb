Imports System.IO
Imports System.Data
Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Process
Imports ServicesPlusException
Imports Sony.US.SIAMUtilities
Imports Sony.US.AuditLog
Imports Excel


Namespace ServicePLUSWebApp

    Partial Class PartsPLUS
        Inherits SSL

        Public csm As New CustomerManager
        Public siamID As String
        Public customer As Customer
        Protected WithEvents txtHiddenSession As Sony.US.ServicesPLUS.Controls.SPSTextBox
        Public sb As New StringBuilder
        Public isAmerica As Boolean

#Region " Web Form Designer Generated Code "
        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub
#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                isAmerica = HttpContextManager.GlobalData.IsAmerica

                'Grab customer from session if available
                If HttpContextManager.Customer IsNot Nothing Then
                    customer = HttpContextManager.Customer
                Else
                    Response.Redirect("sony-parts.aspx", False)
                    Return
                    'AddToCart.Attributes("onclick") = "mywin=window.open('SignIn.aspx','refLogin','toolbar=0,location=0,directories=0,status=0,menubar=0,scrollbars=no,resize=1,width=400,height=400');"
                End If

                'fnGetResourceData()
                ' Set up the Configuration values into the HiddenField objects for the JavaScript file to read.
                quickOrderMaxItems.Value = ConfigurationData.GeneralSettings.General.QuickOrderMaxItems
                orderUploadMaxItems.Value = ConfigurationData.GeneralSettings.General.OrderUploadMaxItems

                'Reset label values
                'NotFoundLabel.Text = ""
                Search1ErrorLabel.Text = ""
                Search2ErrorLabel.Text = ""
                lblQuickOrderError.Text = ""

                'Controls focus of textboxes
                formFieldInitialization()

                If Not Page.IsPostBack() Then
                    ' Destroy PartsFinder search variables
                    Session.Remove("selectedOption")
                    Session.Remove("selectedCategory")
                    Session.Remove("selectedModule")
                    Session.Remove("selectedModel")
                    Session.Remove("refineSearchText")
                    Session.Remove("refineSearchType")
                    Session.Remove("partList")
                    Session.Remove("moduleList")
                    Session.Remove("models")
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                ErrorLabel.Focus()
            End Try
        End Sub

        Private Sub btnSearchModel_Click(ByVal sender As System.Object, ByVal e As ImageClickEventArgs) Handles btnSearchModel.Click
            Try
                Dim cm As New CatalogManager
                Dim models() As Model
                Dim strModels As String = String.Empty
                Dim lstConficts As Generic.List(Of String) = Nothing

                Search1ErrorLabel.Text = ""
                Session.Remove("SearchModel")
                Session.Remove("SearchParts")

                If txtModelNumber.Text = "" Then
                    Search1ErrorLabel.Text = Resources.Resource.el_Pts_Valid_ModelNo '  "You must enter a model number."
                    Search1ErrorLabel.Focus()
                    Return
                Else
                    Session.Add("SearchModel", txtModelNumber.Text)
                    models = cm.PartSearch(txtModelNumber.Text.Trim(), HttpContextManager.GlobalData, lstConficts)

                    If models.Length = 0 Then
                        Search1ErrorLabel.Text = Resources.Resource.el_Pts_NoModel ' "No models found."
                        Search1ErrorLabel.Focus()
                    Else
                        'Added by Snehalatha Ramamurthy on 17th Oct 2013
                        'Current logic: To call the modal popup that shows old model names and 
                        'its new modal names if there are conflicts else take the user to PartsPLUSResults.aspx
                        Session.Add("models", models)
                        If lstConficts.Count > 0 Then
                            'TODO: Test update: strModels = String.Join(",", lstConficts.ToArray())
                            For Each item In lstConficts
                                strModels = strModels + item + ","
                            Next
                            If Not String.IsNullOrEmpty(strModels) Then strModels = strModels.Remove(strModels.Length - 1, 1)
                            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "showPopup",
                                String.Format("javascript:OrderLookUp({0},'{1}');", lstConficts.Count, strModels), True)
                        Else
                            Response.Redirect("PartsPLUSResults.aspx?stype=models")
                        End If
                        'Previous logic
                        'Session.Add("models", models)
                        'Response.Redirect("PartsPLUSResults.aspx?stype=models")
                    End If
                End If
            Catch ex As Exception
                Search1ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                Search1ErrorLabel.Focus()
            End Try
        End Sub

        Private Sub btnSearchPart_Click(ByVal sender As System.Object, ByVal e As ImageClickEventArgs) Handles btnSearchPart.Click
            Try
                Dim catm As New CatalogManager
                Dim parts() As Part
                Dim kits() As Kit
                Dim sMaximvalue As String = String.Empty

                Search2ErrorLabel.Text = ""
                Session.Remove("SearchModel")
                Session.Remove("SearchParts")

                If (txtPrefix.Text = "" And txtPartNumber.Text = "" And txtDescription.Text = "") Then
                    Search2ErrorLabel.Text = Resources.Resource.el_pts_valid_srch '  "You must enter at least one search criteria."
                    Search2ErrorLabel.Focus()
                Else
                    'kill kits session if there is one so that shopping cart focus will be on this search
                    Session.Remove("kits")
                    parts = catm.QuickSearch(txtPrefix.Text.Trim(), txtPartNumber.Text.Trim(), txtDescription.Text.Trim(), sMaximvalue, HttpContextManager.GlobalData)
                    kits = catm.KitSearch(txtPrefix.Text.Trim(), txtPartNumber.Text.Trim(), txtDescription.Text.Trim()) 'also search for kit 
                    Session("SearchParts") = txtPrefix.Text.Trim() + "�" + txtPartNumber.Text.Trim() + "�" + txtDescription.Text.Trim()

                    If parts.Length = 0 And Not String.IsNullOrEmpty(sMaximvalue) Then
                        Search2ErrorLabel.Text = sMaximvalue    ' Provide the error message from the search
                        Search2ErrorLabel.Focus()
                        Return
                    ElseIf parts.Length = 0 And kits.Length = 0 Then
                        Search2ErrorLabel.Text = Resources.Resource.el_pts_noMatch  ' No matches found.
                        Search2ErrorLabel.Focus()
                        Return
                    Else
                        Session("partList") = parts
                        Session("kits") = kits
                        Response.Redirect("PartsPlusResults.aspx?stype=parts", False)
                    End If
                End If
            Catch ex As Exception
                Utilities.LogMessages($"au-sony-parts.btnSearchPart_Click - ERROR - Prefix: {txtPrefix.Text}, PartNumber: {txtPartNumber.Text}, Description: {txtDescription.Text}")
                Search2ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                Search2ErrorLabel.Focus()
            End Try
        End Sub

        ''' <summary>
        ''' Parses a comma-delimited file into a pair of part numbers and quantities.
        ''' Populates the Quick Order textboxes with the data.
        ''' </summary>
        Private Sub btnUpload_Click(ByVal sender As System.Object, ByVal e As ImageClickEventArgs) Handles btnUpload.Click
            Try
                Dim extension As String = Path.GetExtension(fuQuickOrder.PostedFile.FileName).ToLower()
                Dim connectionString As String = ""
                Dim itemCount As Integer = 0
                Dim orderItems As String = ""   ' String for holding array values in a JSON-friendly format.
                Dim partNumber As String = ""
                Dim quantity As String = ""

                lblQuickOrderError.Text = ""

                If fuQuickOrder.HasFile Then
                    ' Check file size - 2 byte to 50KB 
                    If fuQuickOrder.PostedFile.ContentLength < 51300 And fuQuickOrder.PostedFile.ContentLength > 2 Then
                        ' Excel parser block for parsing .xl* filetypes
                        If extension = ".xls" Or extension = ".xlsx" Then
                            Dim excelReader As IExcelDataReader = Nothing
                            Dim dataSet As DataSet
                            Dim dataTable As DataTable

                            Select Case extension
                                Case ".xls"
                                    excelReader = ExcelReaderFactory.CreateBinaryReader(fuQuickOrder.PostedFile.InputStream)
                                    Exit Select
                                Case ".xlsx"
                                    excelReader = ExcelReaderFactory.CreateOpenXmlReader(fuQuickOrder.PostedFile.InputStream)
                                    Exit Select
                            End Select

                            ' Get the dataset from parser and dump into a table
                            dataSet = excelReader.AsDataSet()
                            dataTable = dataSet.Tables.Item(0) ' Use first sheet

                            For Each row As DataRow In dataTable.Rows
                                If (itemCount <= ConfigurationData.GeneralSettings.General.OrderUploadMaxItems) Then
                                    partNumber = row.ItemArray.GetValue(0).ToString.Trim
                                    If row.ItemArray.Length = 1 Then   ' If there is no Quantity column, force a value of 1
                                        quantity = "1"
                                    Else   ' This CANNOT be done with a Ternary Operator (IIf) because it will still process the "false" portion and throw IndexOutOfRange.
                                        quantity = row.ItemArray.GetValue(1).ToString.Trim
                                    End If
                                    If (IsValidString(partNumber)) And (IsValidString(quantity)) Then               ' Ensure the user isn't trying to inject scripts, etc.
                                        If (itemCount > 0) OrElse (Integer.TryParse(quantity, New Integer)) Then    ' If the first row doesn't have a valid Integer quantity, we assume it's a header row and ignore it.
                                            If (itemCount > 0) Then orderItems &= ", "
                                            orderItems &= String.Format("'{0}', '{1}'", partNumber, quantity)
                                            itemCount += 1
                                        End If
                                    End If
                                End If
                            Next

                            ' StreamReader block for parsing commma delimited file
                        ElseIf extension = ".csv" Then
                            Dim fileReader As StreamReader
                            Dim fileLine As String
                            Dim linePair As String()

                            fileReader = New StreamReader(fuQuickOrder.PostedFile.InputStream)

                            Do Until fileReader.EndOfStream
                                If (itemCount <= ConfigurationData.GeneralSettings.General.OrderUploadMaxItems) Then
                                    fileLine = fileReader.ReadLine()
                                    linePair = fileLine.Split(",")
                                    partNumber = linePair(0).Trim
                                    If (linePair.Length < 2) Then   ' If there is no Quantity column, force a value of 1
                                        quantity = "1"
                                    Else   ' This CANNOT be done with a Ternary Operator (IIf) because it will still process the "false" portion and throw IndexOutOfRange.
                                        quantity = linePair(1).Trim
                                    End If
                                    If (IsValidString(partNumber)) And (IsValidString(quantity)) Then   ' Ensure the user isn't trying to inject scripts, etc.
                                        If (itemCount > 0) OrElse (Integer.TryParse(quantity, New Integer)) Then   ' If the first row doesn't have a valid Integer quantity, we assume it's a header row and ignore it.
                                            If (itemCount > 0) Then orderItems &= ","
                                            orderItems &= String.Format("'{0}', '{1}'", partNumber, quantity)
                                            itemCount += 1
                                        End If
                                    End If
                                End If
                            Loop
                        Else
                            ' Invalid file extension
                            lblQuickOrderError.Text = "Your file could not be processed. Please upload a .XLS, XLSX or a comma-delimited .CSV file with valid part number and quantity columns."
                            lblQuickOrderError.Focus()
                        End If


                        If itemCount = 0 Then
                            lblQuickOrderError.Text = "Your file could not be processed. Please upload a .XLS, XLSX or a comma-delimited .CSV file with valid part number and quantity columns."
                            lblQuickOrderError.Focus()
                        Else
                            ' Session("OrderItems") = orderItems
                            ' lblQuickOrderError.Text = "File parsed, " & itemCount & " items found. Order items = " & orderItems
                            ClientScript.RegisterArrayDeclaration("orderItems", orderItems)
                            ClientScript.RegisterStartupScript(Page.GetType(), "ProcessOrderList", "<script type='text/javascript'>ProcessOrderList();</script>")
                        End If

                    Else
                        lblQuickOrderError.Text = "Invalid file size. Please upload a file less than 50KB."
                        lblQuickOrderError.Focus()
                    End If ' End of file size check block

                    ' Clear the control's data to prepare for the next upload, if any.
                    fuQuickOrder = New FileUpload

                Else
                    lblQuickOrderError.Text = "Please upload a .XLS, XLSX or a comma-delimited .CSV file with valid part number and quantity columns."
                    lblQuickOrderError.Focus()
                End If ' End of HasFile block

            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                ErrorLabel.Focus()
            End Try
        End Sub

        ' 2018-10-03 ASleight - This function is no longer called anywhere.
        'Private Sub ValidateQuickOrderFields(ByVal ArrPartNumber() As String, ByVal ArrQty() As String)
        '    Dim i As Integer
        '    Dim objNotNaturalPattern As New Regex("[^0-9]")

        '    lblQuickOrderError.Visible = False
        '    For i = 0 To 9
        '        If Not ArrPartNumber(i) = "" And Not ArrQty(i) = "" Then
        '            If objNotNaturalPattern.IsMatch(ArrQty(i)) Or ArrQty(i) = "0" Then
        '                sb.Append(Resources.Resource.atc_ex_InvalidQuantityForPart & " " & ArrPartNumber(i).ToString() & ".<br/>")
        '                lblQuickOrderError.Visible = True
        '            End If
        '        End If
        '    Next
        'End Sub

        ' 2018-10-03 ASleight - This function is no longer called anywhere.
        'Private Sub ValidateNoBlanks(ByVal ArrPartNumber() As String, ByVal ArrQty() As String)
        '    Dim i As Integer

        '    lblQuickOrderError.Visible = False
        '    For i = 0 To 9
        '        If ArrPartNumber(i) = "" And ArrQty(i) = "" Then
        '            lblQuickOrderError.Visible = False
        '        Else
        '            If ArrPartNumber(i) = "" Or ArrQty(i) = "" Then
        '                lblQuickOrderError.Text = Resources.Resource.atc_ex_InvalidQuantityForPart '"Please enter a valid part number and quantity."
        '                lblQuickOrderError.Visible = True
        '                lblQuickOrderError.Focus()
        '                Return
        '            End If
        '        End If
        '    Next
        'End Sub

        Private Sub formFieldInitialization()
            PartNumber1.Attributes("onkeydown") = "SetTheFocusButton(event, 'AddToCart')"
            PartNumber2.Attributes("onkeydown") = "SetTheFocusButton(event, 'AddToCart')"
            PartNumber3.Attributes("onkeydown") = "SetTheFocusButton(event, 'AddToCart')"
            PartNumber4.Attributes("onkeydown") = "SetTheFocusButton(event, 'AddToCart')"
            PartNumber5.Attributes("onkeydown") = "SetTheFocusButton(event, 'AddToCart')"

            PartNumber6.Attributes("onkeydown") = "SetTheFocusButton(event, 'AddToCart')"
            PartNumber7.Attributes("onkeydown") = "SetTheFocusButton(event, 'AddToCart')"
            PartNumber8.Attributes("onkeydown") = "SetTheFocusButton(event, 'AddToCart')"
            PartNumber9.Attributes("onkeydown") = "SetTheFocusButton(event, 'AddToCart')"

            Qty1.Attributes("onkeydown") = "SetTheFocusButton(event, 'AddToCart')"
            Qty2.Attributes("onkeydown") = "SetTheFocusButton(event, 'AddToCart')"
            Qty3.Attributes("onkeydown") = "SetTheFocusButton(event, 'AddToCart')"
            Qty4.Attributes("onkeydown") = "SetTheFocusButton(event, 'AddToCart')"
            Qty5.Attributes("onkeydown") = "SetTheFocusButton(event, 'AddToCart')"

            Qty6.Attributes("onkeydown") = "SetTheFocusButton(event, 'AddToCart')"
            Qty7.Attributes("onkeydown") = "SetTheFocusButton(event, 'AddToCart')"
            Qty8.Attributes("onkeydown") = "SetTheFocusButton(event, 'AddToCart')"
            Qty9.Attributes("onkeydown") = "SetTheFocusButton(event, 'AddToCart')"
            Qty10.Attributes("onkeydown") = "SetTheFocusButton(event, 'AddToCart')"

            txtPrefix.Attributes("onkeydown") = "SetTheFocusButton(event, 'btnSearchPart')"
            txtPartNumber.Attributes("onkeydown") = "SetTheFocusButton(event, 'btnSearchPart')"
            txtDescription.Attributes("onkeydown") = "SetTheFocusButton(event, 'btnSearchPart')"

            txtModelNumber.Attributes("onkeydown") = "SetTheFocusButton(event, 'btnSearchModel')"

        End Sub

        <System.Web.Services.WebMethod(EnableSession:=True)>
        Public Shared Function addPartToCart(ByVal sOldPartNumber As String, ByVal sPartQuantity As String, ByVal sTextboxid As String, ByVal sNewPartNumber As String) As Partresult

            Dim objPCILogger As New PCILogger() '6524
            Dim partNumber As String
            Dim cartResult As New Partresult
            'Dim objUtilties As Utilities = New Utilities
            Dim message As String
            Dim Conflict As Boolean = False
            Dim customer As New Customer
            Dim lmessage As New Label
            Dim objSSL As New SSL
            Dim carts As ShoppingCart()
            Dim sm As New ShoppingCartManager
            Dim cart As ShoppingCart
            Dim differentCart As Boolean = False
            Dim cm As CourseManager = Nothing
            Dim addedProduct As Product

            partNumber = sNewPartNumber.ToUpper().Trim()
            cartResult.partnumber = partNumber
            cartResult.textboxid = sTextboxid
            cartResult.success = False

            customer = HttpContextManager.Customer
            If customer Is Nothing Then
                cartResult.success = -1
                cartResult.message = "Must be logged in, or an error occured loading user data."
                Return cartResult
            End If

            If HttpContextManager.GlobalData.IsCanada Then
                If customer.UserType = "P" Then
                    message = Resources.Resource.el_ATC_PendingCust ' "Message for pending customer. (Cannot add model to cart for pending account customer. )"
                    cartResult.success = -1
                    cartResult.message = message
                    Return cartResult
                End If
            End If

            carts = objSSL.getAllShoppingCarts(customer, lmessage)
            message = lmessage.Text

            Try
                Dim output1 As String = String.Empty

                If sOldPartNumber <> sNewPartNumber Then
                    sm.sourcePart = sOldPartNumber
                End If

                'Ccheck replacement in DB 
                Try
                    ' Dim sm As New ShoppingCartManager
                    Dim DTparts As DataSet = sm.FindConflictCard(sOldPartNumber.ToUpper(), HttpContextManager.GlobalData)

                    If DTparts Is Nothing OrElse DTparts.Tables.Count = 0 OrElse DTparts.Tables(0).Rows.Count = 0 Then
                        If HttpContextManager.GlobalData.SalesOrganization = ConfigurationData.GeneralSettings.GlobalData.SalesOrganization.CA Then
                            message = Resources.Resource.el_InvalidParts_CA.Replace("�", sOldPartNumber.ToUpper())   ' "Part (�) is not available for CANADA"
                        Else
                            message = Resources.Resource.el_InvalidParts_US.Replace("�", sOldPartNumber.ToUpper())   ' "Part (�) is not available for USA"
                        End If
                        cartResult.success = -1
                        cartResult.message = message
                        Return cartResult
                    Else
                        Dim row As DataRow() = DTparts.Tables(0).Select("PARTNUMBER='" + sOldPartNumber + "'")
                        If row.Length > 0 Then
                            partNumber = row(0)(1).ToString().ToUpper()
                        End If
                    End If

                Catch ex As Exception

                End Try
                cart = sm.AddProductToCartQuickCart(carts, partNumber, sPartQuantity, True, output1, HttpContextManager.GlobalData)

                Dim bProperpart As Boolean = True
                If Not objSSL.IsCurrentCartSameType(cart) Then
                    differentCart = True
                    cartResult.differentCart = True
                End If
                addedProduct = cart.ShoppingCartItems.GetValue(cart.ShoppingCartItems.Length - 1).Product
                cartResult.items = cart.ShoppingCartItems.Length
                cartResult.totalAmount = String.Format("{0:c}", cart.ShoppingCartTotalPrice)
                If addedProduct.ReplacementPartNumber.ToUpper() <> partNumber Then
                    cartResult.success = 2
                    message = String.Format(Resources.Resource.atc_ReplacementAdded, addedProduct.ReplacementPartNumber, partNumber)
                Else
                    cartResult.success = 1
                    message = partNumber + Resources.Resource.atc_AddedToCart
                End If
                For Each i As ShoppingCartItem In cart.ShoppingCartItems

                    If Not i.Product.PartNumber.Equals(partNumber) Then
                        HttpContext.Current.Session("carts") = cart
                        cartResult.items = cart.ShoppingCartItems.Length
                        cartResult.totalAmount = String.Format("{0:c}", cart.ShoppingCartTotalPrice)
                        cartResult.success = 2
                        'message = "Replacement item " + i.Product.PartNumber + " added to cart in place of requested item  " + sPartNumberUpperCase + " which is no longer available."
                        message = String.Format(Resources.Resource.atc_ReplacementAdded, i.Product.PartNumber, partNumber)
                        cartResult.message = message
                    Else
                        HttpContext.Current.Session("carts") = cart
                        cartResult.items = cart.ShoppingCartItems.Length
                        cartResult.totalAmount = String.Format("{0:c}", cart.ShoppingCartTotalPrice)
                        cartResult.success = 1
                        message = Resources.Resource.el_item + " " + partNumber + " " + Resources.Resource.atc_AddedToCart ' " added to cart."
                        cartResult.message = message
                    End If
                Next

                If sOldPartNumber <> sNewPartNumber Then
                    Conflict = True
                    cartResult.success = 2
                    '  message = "Replacement item " + sNewPartNumber + " added to cart in place of requested item  " + sOldPartNumber + "which is no longer available."
                    'objResult.message = message

                    'message = "Replacement item " + i.Product.PartNumber + " added to cart in place of requested item  " + sPartNumberUpperCase + " which is no longer available."
                    '' message = Resources.Resource.atc_ReplacementAdded + i.Product.PartNumber + Resources.Resource.el_ATC_Msg2 + sPartNumberUpperCase + Resources.Resource.el_ATC_Msg3

                    ' Dim objEx As New ServicesPlusBusinessException("Replacement item " + sNewPartNumber + " added to cart in place of requested item  " + sOldPartNumber + " which is no longer available")
                    'objEx.errorMessage = "Replacement item " + sNewPartNumber + " added to cart in place of requested item  " + sOldPartNumber + "."
                    Dim errMessage = String.Format(Resources.Resource.atc_ReplacementAdded, sNewPartNumber, sOldPartNumber)
                    Dim objEx As New ServicesPlusBusinessException(errMessage)
                    objEx.errorMessage = errMessage
                    objEx.setcustomMessage = True 'added by sathish for 9019
                    objEx.DisplayIncidentNumber = False 'added by sathish for 9019
                    Throw objEx
                End If



            Catch exArgumentalExceptio As ArgumentException
                If exArgumentalExceptio.ParamName = "SomePartNotFound" Then
                    message = exArgumentalExceptio.Message.Replace("Parameter name: SomePartNotFound", "")
                ElseIf exArgumentalExceptio.ParamName = "PartNotFound" Then
                    message = exArgumentalExceptio.Message.Replace("Parameter name: PartNotFound", "")
                ElseIf exArgumentalExceptio.ParamName = "BillingOnlyItem" Then
                    message = exArgumentalExceptio.Message.Replace("Parameter name: BillingOnlyItem", "")
                End If
            Catch ex As Exception
                '6524 V11 starts
                objPCILogger.EventOriginApplication = "ServicesPLUS"
                'objPCILogger.EventOriginApplicationLocation = Me.Page.GetType().Name
                objPCILogger.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
                objPCILogger.OperationalUser = Environment.UserName
                objPCILogger.EventDateTime = DateTime.Now.ToLongTimeString()
                objPCILogger.HTTPRequestObjectValues = objPCILogger.GetRequestContextValue()
                objPCILogger.CustomerID = customer.CustomerID
                objPCILogger.CustomerID_SequenceNumber = customer.SequenceNumber
                'objPCILogger.UserName = customer.UserName
                objPCILogger.EmailAddress = customer.EmailAddress '6524
                objPCILogger.SIAM_ID = customer.SIAMIdentity
                objPCILogger.LDAP_ID = customer.LdapID '6524
                objPCILogger.EventOriginMethod = "addPartToCart"
                objPCILogger.EventType = EventType.View
                '6524 V11 ends

                If Conflict Then
                    objPCILogger.IndicationSuccessFailure = "Information" '6524
                    objPCILogger.Message = "addPartToCart successful. " & ex.Message.ToString()
                Else
                    objPCILogger.IndicationSuccessFailure = "Failure" '6524
                    objPCILogger.Message = "addPartToCart Failed. " & ex.Message.ToString() '6524
                End If

                objPCILogger.PushLogToMSMQ()
                message = Utilities.WrapExceptionforUI(ex)
            Finally
                'objPCILogger.PushLogToMSMQ() '6524'6524 V11
            End Try
            cartResult.message = message
            Return cartResult
        End Function

        <System.Web.Services.WebMethod(EnableSession:=True)>
        Public Shared Function GetConflictPart(ByVal sList As String) As String
            Dim sm As New ShoppingCartManager
            Dim parts As String() = sList.ToUpper().Split(",")
            Dim partlist As List(Of KeyValuePair(Of String, String)) = New List(Of KeyValuePair(Of String, String))
            Dim condParts As String = String.Empty
            Dim locPart As String()

            Try
                For Each Part In parts
                    locPart = Part.Split(":")
                    partlist.Add(New KeyValuePair(Of String, String)(locPart(0), locPart(1) + ":" + locPart(2)))
                    condParts += locPart(0) + ","
                Next
                If Not String.IsNullOrEmpty(condParts) Then
                    condParts = condParts.Remove(condParts.Length - 1)
                    condParts = String.Empty
                    For Each pair As KeyValuePair(Of String, String) In partlist
                        condParts += pair.Key + ":" + pair.Value + ":" + pair.Key + ","
                    Next
                    condParts = condParts.Remove(condParts.Length - 1)
                End If

            Catch ex As Exception

            End Try
            Return condParts
        End Function

        'Protected Sub btn_UploadHelp_click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnUploadHelp.Click
        '    Dim url As String = "OrderUploadHelp.aspx"
        '    Dim s As String = "window.open('" & url + "', 'uploadhelp_window', 'width=402,height=362,left=100,top=100,resizable=no,menubar=no,scrollbar=no,status=no,location=no');"
        '    ClientScript.RegisterStartupScript(Me.GetType(), "script", s, True)
        'End Sub
    End Class

    Public Class QuickOrderPair
        Public PartNumber As String
        Public Quantity As Integer
    End Class

    Public Class Partresult
        Public _message As String
        Public _totalAmount As String
        Public _items As String
        Public _success As Integer
        Public _textboxid As String
        Public _partnumber As String
        Public _differentCart As Boolean

        Property partnumber() As String
            Get
                Return _partnumber
            End Get
            Set(ByVal value As String)
                _partnumber = value
            End Set
        End Property
        Property message() As String
            Get
                Return _message
            End Get
            Set(ByVal value As String)
                _message = value
            End Set
        End Property
        Property textboxid() As String
            Get
                Return _textboxid
            End Get
            Set(ByVal value As String)
                _textboxid = value
            End Set
        End Property
        ''' <summary>
        ''' 0-Failure
        ''' 1-Success
        ''' 2-warning
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Property success() As Integer
            Get
                Return _success
            End Get
            Set(ByVal value As Integer)
                _success = value
            End Set
        End Property
        Property totalAmount() As String
            Get
                Return _totalAmount
            End Get
            Set(ByVal value As String)
                _totalAmount = value
            End Set
        End Property
        Property items() As String
            Get
                Return _items
            End Get
            Set(ByVal value As String)
                _items = value
            End Set
        End Property

        Property differentCart() As Boolean
            Get
                Return _differentCart
            End Get
            Set(ByVal value As Boolean)
                _differentCart = value
            End Set
        End Property

    End Class

End Namespace
