Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Process

Namespace ServicePLUSWebApp

    Partial Class PersonalMessage
        Inherits UserControl

        Public NumReminders As String = "0"

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub


        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.PreRender
            Dim customer As Customer = HttpContextManager.Customer
            Dim catMan As New CatalogManager
            Dim my_cat_items() As CustomerCatalogItem
            Dim my_reminders() As CustomerCatalogItem = Nothing

            Me.Visible = (customer IsNot Nothing)   ' Only show the reminder if logged in.
            If customer IsNot Nothing Then
                my_cat_items = catMan.MyReminder(customer)
                If my_cat_items IsNot Nothing Then
                    my_reminders = catMan.MyCatalogReminders(my_cat_items)
                End If

                If my_reminders IsNot Nothing Then
                    If my_reminders.Length > 0 Then
                        NumReminders = $"<span class=""redAsterick"" style=""color: ##ff8080;"">{my_reminders.Length}</span>"   ' 2018-07-18 Changed colour for WCAG contrast ratio compliance.
                    End If
                End If

                'LabelMessage.Text = $"<a class=""memberMessage"" href=""./ViewMyCatalog.aspx"" style=""text-align: center; vertical-align: middle;"" aria-label=""Go to the My Catalog page to view your reminders and more."">{Resources.Resource.mem_hdr_msg} {NumReminders} {Resources.Resource.mem_hdr_msg_1}</a>"
            End If
        End Sub

        Protected Sub InitializeCulture()
            'If HttpContextManager.SelectedLang IsNot Nothing Then
            '	Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(HttpContextManager.SelectedLang)
            '	Thread.CurrentThread.CurrentUICulture = New CultureInfo(HttpContextManager.SelectedLang)
            'End If

            ' base.InitializeCulture();
        End Sub

    End Class

End Namespace
