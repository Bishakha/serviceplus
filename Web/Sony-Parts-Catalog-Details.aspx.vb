Imports ServicesPlusException
Imports Sony.US.AuditLog
Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Process

Namespace ServicePLUSWebApp

    Partial Class Sony_Parts_Catalog_Details
        Inherits SSL

        Dim customer As Customer
		Dim parts() As Part
        'Dim objUtilties As Utilities = New Utilities

        Const conPartsCatalogDetailPath As String = "Sony-Parts-Catalog-Details.aspx?groupid="

        Private rangePerPage As Integer
        Private partsPerRange As Integer

        Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Dim lnkBPrev As HyperLink = CType(Me.btmPrevious, HyperLink)
            Dim lnkTPrev As HyperLink = CType(Me.topPrevious, HyperLink)
            Dim lnkBNext As HyperLink = CType(Me.btmNext, HyperLink)
            Dim lnkTNext As HyperLink = CType(Me.topNext, HyperLink)
            Dim lnkBPrevImg As HyperLink = CType(Me.btmPreviousImg, HyperLink)
            Dim lnkTPrevImg As HyperLink = CType(Me.topPreviousImg, HyperLink)
            Dim lnkBNextImg As HyperLink = CType(Me.btmNextImg, HyperLink)
            Dim lnkTNextImg As HyperLink = CType(Me.topNextImg, HyperLink)
            Dim groupid As Long
            Dim prevGroupId As Long
            Dim nextGroupId As Long = 1
            Dim cm As CatalogManager = New CatalogManager
            Dim maxGroupId As Long = cm.GetMaxGroupID()
            Dim whichPart As Integer = 0
            Dim prevCatalogadded As Boolean = False
            Dim addToMyCatalog As String = "False"

            Try
                ' Reset Error Label
                ErrorLabel.Text = ""
                ' Grab customer from session if available
                If Session.Item("customer") IsNot Nothing Then
                    customer = Session.Item("customer")
                    AddToCartAfterLogin(ErrorLabel)
                End If

                If Session.Item("partList") IsNot Nothing Then
                    parts = Session.Item("partList")
                End If

                Try
                    If Request.QueryString("groupid") IsNot Nothing Then
                        groupid = Convert.ToInt32(Request.QueryString("groupid"))
                    Else
                        groupid = 0
                    End If
                Catch ex As Exception
                    groupid = 0
                End Try

                Session.Add("groupid", groupid)
                maxGroupId = cm.GetMaxGroupID()
                cm.getRanges(rangePerPage, partsPerRange)

                If groupid > partsPerRange Then
                    prevGroupId = groupid - partsPerRange
                Else
                    prevGroupId = 1
                    lnkBPrev.Visible = False
                    lnkTPrev.Visible = False
                    lnkBPrevImg.Visible = False
                    lnkTPrevImg.Visible = False
                End If

                If groupid < maxGroupId Then
                    nextGroupId = groupid + partsPerRange
                Else
                    nextGroupId = 1
                    lnkBNext.Visible = False
                    lnkTNext.Visible = False
                    lnkBNextImg.Visible = False
                    lnkTNextImg.Visible = False
                End If

                'Modified for fixing Bug# 436
                'lnkBPrev.NavigateUrl = conPartsCatalogDetailPath + prevGroupId.ToString()
                lnkBPrev.NavigateUrl = "groupid" + prevGroupId.ToString() + ".aspx"
                'lnkTPrev.NavigateUrl = conPartsCatalogDetailPath + prevGroupId.ToString()
                lnkTPrev.NavigateUrl = "groupid" + prevGroupId.ToString() + ".aspx"

                'lnkBNext.NavigateUrl = conPartsCatalogDetailPath + nextGroupId.ToString()
                lnkBNext.NavigateUrl = "groupid" + nextGroupId.ToString() + ".aspx"
                'lnkTNext.NavigateUrl = conPartsCatalogDetailPath + nextGroupId.ToString()
                lnkTNext.NavigateUrl = "groupid" + nextGroupId.ToString() + ".aspx"

                'lnkBPrevImg.NavigateUrl = conPartsCatalogDetailPath + prevGroupId.ToString()
                lnkBPrevImg.NavigateUrl = "groupid" + prevGroupId.ToString() + ".aspx"
                'lnkTPrevImg.NavigateUrl = conPartsCatalogDetailPath + prevGroupId.ToString()
                lnkTPrevImg.NavigateUrl = "groupid" + prevGroupId.ToString() + ".aspx"
                'lnkBNextImg.NavigateUrl = conPartsCatalogDetailPath + nextGroupId.ToString()
                lnkBNextImg.NavigateUrl = "groupid" + nextGroupId.ToString() + ".aspx"
                'lnkTNextImg.NavigateUrl = conPartsCatalogDetailPath + nextGroupId.ToString()
                lnkTNextImg.NavigateUrl = "groupid" + nextGroupId.ToString() + ".aspx"

                lnkBPrevImg.Style.Add("cursor", "hand")
                lnkTPrevImg.Style.Add("cursor", "hand")
                lnkBNextImg.Style.Add("cursor", "hand")
                lnkTNextImg.Style.Add("cursor", "hand")

                If Request.QueryString("PValue") IsNot Nothing Then
                    whichPart = Request.QueryString("PValue")
                End If

                If Session.Item("addCatPvalue") IsNot Nothing Then
                    prevCatalogadded = True
                    whichPart = CType(Session.Item("addCatPvalue").ToString(), Integer)
                    Session.Remove("addCatPvalue")
                End If

                'Add to my Catalog
                If Request.QueryString("atmc") IsNot Nothing Then
                    addToMyCatalog = Request.QueryString("atmc")
                End If

                If addToMyCatalog = "true" Or prevCatalogadded = True Then
                    AddPartToMyCatalog(whichPart)
                End If
                populateSearchResult()
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                lnkBPrev.Visible = False
                lnkTPrev.Visible = False
                lnkBPrevImg.Visible = False
                lnkTPrevImg.Visible = False

                lnkBNext.Visible = False
                lnkTNext.Visible = False
                lnkBNextImg.Visible = False
                lnkTNextImg.Visible = False
            End Try
        End Sub

        '      Private Sub AddPartToCart(ByVal whichPart As Integer)
        '          Dim the_part As Sony.US.ServicesPLUS.Core.Part
        '          Dim deliveryMethod As ShoppingCartItem.DeliveryMethod
        '          Dim cart As ShoppingCart
        '          Dim sm As New ShoppingCartManager

        '	the_part = parts.GetValue(whichPart)
        '          deliveryMethod = ShoppingCartManager.getDeliveryMethod(the_part.ProgramCode)
        '	cart = getShoppingCart(customer, deliveryMethod, Me.ErrorLabel)

        '          '-- if we can't add the item to the cart then we need to save the existing cart and create a new cart for the item
        '          If Not IsCurrentCartSameType(cart) Then
        '              ErrorLabel.Visible = True
        '              If cart.Type <> ShoppingCart.enumCartType.PrintOnDemand Then
        '                  ErrorLabel.Text = "The item is put to different cart from the previous cart."
        '              Else
        '                  ErrorLabel.Text = "The print on demand item is put to different cart from the previous cart."
        '              End If
        '          End If

        '          '-- added to make sure the model number show up in MyCatalog. - dwd
        '	sm.AddProductToCart(HttpContextManager.GlobalData, cart, the_part, ShoppingCartItem.ItemOriginationType.Search, ShoppingCartItem.DeliveryMethod.Ship)
        '	Session("carts") = cart
        'End Sub

        Private Sub AddPartToMyCatalog(ByVal whichPart As Integer)
            Try
                Dim cm As New CatalogManager

                If customer Is Nothing Then
                    ErrorLabel.Text = ("You must be logged in to add items to your catalog.")
                    Return
                Else
                    If Request.QueryString("PValue") IsNot Nothing Then '6916
                        Dim the_part As Sony.US.ServicesPLUS.Core.Part
                        the_part = parts.GetValue(whichPart)
                        cm.AddMyCatalogItem(customer, the_part)
                    End If '6916
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Sub populateSearchResult()
            Dim groupid As Int64 = 0
            Dim totalCount As Integer = 0
            Dim cm As CatalogManager = New CatalogManager
            Dim parts() As Sony.US.ServicesPLUS.Core.Part

            Try
                If Session.Item("groupid") IsNot Nothing Then
                    groupid = Convert.ToInt64(Session.Item("groupid"))
                End If
            Catch ex As Exception
                groupid = 0
            End Try

            '' parts = cm.BrowsePartsSearch(groupid)
            'Dim pat As DataTable = cm.BrowsePartsSearch(groupid, 0, 40, totalCount)'6916
            If groupid = 0 Then
                Exit Sub
            Else
                parts = cm.BrowsePartsSearch(groupid, 0, 40, totalCount) '6916
                '-- need to add the parts result to session so that add to cart/cat.
                '-- without login can determine based on the line #
                'Me.GridPartsGroup.DataSource = pat '6916
                Me.GridPartsGroup.DataSource = parts '6916
                Me.GridPartsGroup.DataBind()
                Session.Add("partList", parts)  '6916
            End If
        End Sub

        Protected Sub GridPartsGroup_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridPartsGroup.RowDataBound
            Try
                If e.Row.RowType <> DataControlRowType.DataRow Then
                    Exit Sub
                End If

                Dim rec As Part = e.Row.DataItem
                Dim n As Integer = e.Row.DataItemIndex
                Dim groupid As String = ""

                If Session.Item("groupid") IsNot Nothing Then
                    groupid = CType(Session.Item("groupid"), String)
                End If

                Dim lnkPartRequested As HyperLink = CType(e.Row.Cells(1).Controls(1), HyperLink)
                lnkPartRequested.Style.Add("cursor", "hand")
                lnkPartRequested.Style.Add("text-decoration", "underline")
                lnkPartRequested.NavigateUrl = "sony-part-number-" + ReturnCharacterSub(rec.PartNumber) + ".aspx"
                lnkPartRequested.Text = rec.PartNumber.ToString()

                Dim lnkPartSupplied As HyperLink = CType(e.Row.Cells(2).Controls(1), HyperLink)
                lnkPartSupplied.Style.Add("cursor", "hand")
                lnkPartSupplied.Style.Add("text-decoration", "underline")
                lnkPartSupplied.NavigateUrl = "sony-part-number-" + ReturnCharacterSub(rec.ReplacementPartNumber) + ".aspx"
                lnkPartSupplied.Text = rec.ReplacementPartNumber.ToString()

                Dim lnkAvailability As HyperLink = CType(e.Row.Cells(4).Controls(1), HyperLink)
                lnkAvailability.Style.Add("cursor", "hand")
                lnkAvailability.Style.Add("text-decoration", "underline")

                Dim lnkCart As HyperLink = CType(e.Row.Cells(5).Controls(1), HyperLink)
                lnkCart.Style.Add("cursor", "hand")

                Dim lnkCat As HyperLink = CType(e.Row.Cells(6).Controls(1), HyperLink)
                lnkCat.Style.Add("cursor", "hand")
                '7449 Starts
                Dim litAvailability As System.Web.UI.WebControls.Literal = CType(e.Row.FindControl("litAvailability"), System.Web.UI.WebControls.Literal)

                If rec.ListPrice = 0 Then
                    litAvailability.Visible = True
                    lnkAvailability.Visible = False
                    lnkCart.Visible = False
                    lnkCat.Visible = False
                Else
                    litAvailability.Visible = False
                    lnkAvailability.Visible = True
                    lnkCart.Visible = True
                    lnkCat.Visible = True
                    If customer Is Nothing Then
                        lnkAvailability.Attributes.Add("onclick", "mywin=window.open('Availability.aspx?isKit=0&ArrayVal=" + n.ToString() + "','','" + CON_POPUP_FEATURES + "');")
                        lnkCart.Attributes.Add("onclick", "window.location.href='NotLoggedInAddToCart.aspx?groupid=" + groupid.ToString() + "&PartLineNo=" + n.ToString() + "';")
                        lnkCat.Attributes.Add("onclick", "window.location.href='NotLoggedInAddToCatalog.aspx?groupid=" + groupid.ToString() + "&PartLineNo=" + n.ToString() + "';")
                    Else
                        If (customer.SAPBillToAccounts.Length > 0 Or customer.SISLegacyBillToAccounts.Length > 0) Then
                            lnkAvailability.Attributes.Add("onclick", "mywin=window.open('yourprice.aspx?isKit=0&ArrayVal=" + n.ToString() + "','','" + CON_POPUP_FEATURES + "');")
                        Else
                            lnkAvailability.Attributes.Add("onclick", "mywin=window.open('Availability.aspx?isKit=0&ArrayVal=" + n.ToString() + "','','" + CON_POPUP_FEATURES + "');")
                        End If
                        'lnkCart.NavigateUrl = conPartsCatalogDetailPath + groupid + "&pValue=" + n.ToString() + "&atc=true"'6994
                        '6994 starts
                        hidespstextqty.Text = 1
                        lnkCart.Attributes.Add("onclick", "return AddToCart_onclick('" + rec.PartNumber.ToString() + "')")
                        '6994 ends
                        lnkCat.NavigateUrl = conPartsCatalogDetailPath + groupid + "&pValue=" + n.ToString() + "&atmc=true"
                    End If
                End If
                '7449 Ends
                'If customer Is Nothing Then
                '    lnkAvailability.Attributes.Add("onclick", "mywin=window.open('Availability.aspx?isKit=0&ArrayVal=" + n.ToString() + "','','" + CON_POPUP_FEATURES + "');")
                '    lnkCart.Attributes.Add("onclick", "window.location.href='NotLoggedInAddToCart.aspx?groupid=" + groupid.ToString() + "&PartLineNo=" + n.ToString() + "';")
                '    lnkCat.Attributes.Add("onclick", "window.location.href='NotLoggedInAddToCatalog.aspx?groupid=" + groupid.ToString() + "&PartLineNo=" + n.ToString() + "';")
                'Else
                '    If (customer.SAPBillToAccounts.Length > 0 Or customer.SISLegacyBillToAccounts.Length > 0) Then
                '        lnkAvailability.Attributes.Add("onclick", "mywin=window.open('yourprice.aspx?isKit=0&ArrayVal=" + n.ToString() + "','','" + CON_POPUP_FEATURES + "');")
                '    Else
                '        lnkAvailability.Attributes.Add("onclick", "mywin=window.open('Availability.aspx?isKit=0&ArrayVal=" + n.ToString() + "','','" + CON_POPUP_FEATURES + "');")
                '    End If
                '    lnkCart.NavigateUrl = conPartsCatalogDetailPath + groupid + "&pValue=" + n.ToString() + "&atc=true"
                '    lnkCat.NavigateUrl = conPartsCatalogDetailPath + groupid + "&pValue=" + n.ToString() + "&atmc=true"
                'End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
            Dim catm As New CatalogManager
            Dim parts() As Sony.US.ServicesPLUS.Core.Part
            Dim kits() As Sony.US.ServicesPLUS.Core.Kit
            Dim emptyString As String = ""

            Session.Remove("kits")

            Try
                If PartNumber.Text.Trim() = "" Then
                    SearchErrorLabel.Text = "Please enter Part Number to search"
                    Return
                End If

                '-- reusing the same quick search of parts page, so have to pass empty string on the model and description parameters
                Dim sMaximumvalue As String = String.Empty
                parts = catm.QuickSearch(emptyString, PartNumber.Text, emptyString, sMaximumvalue, HttpContextManager.GlobalData)
                kits = catm.KitSearch(emptyString, PartNumber.Text, emptyString) 'also search for kit now

                If parts.Length = 0 And Not String.IsNullOrEmpty(sMaximumvalue) Then
                    SearchErrorLabel.Text = sMaximumvalue
                    Return
                ElseIf parts.Length = 0 And kits.Length = 0 Then
                    SearchErrorLabel.Text = ("No matches found.")
                    Return
                Else
                    Session("partList") = parts
                    Session("kits") = kits
                    Response.Redirect("PartsPlusResults.aspx?stype=parts")
                End If
            Catch thrEx As Threading.ThreadAbortException
                'do nothing, expected for response.redirect
            Catch ex As Exception
                SearchErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        'End Class'6994
        '6994 starts
        <System.Web.Services.WebMethod(EnableSession:=True)> _
        Public Shared Function addPartToCartwSCatalog(ByVal sPartNumber As String) As PartresultCatalogDetails '6994
            Dim objPCILogger As New PCILogger() '6524
            Dim sPartNumberUpperCase As String = sPartNumber.ToUpper()
            Dim objResult As New PartresultCatalogDetails '6994
            'Dim objUtilties As Utilities = New Utilities
            Dim message As String = ""
            Dim customer As New Customer
            'Dim lmessage As New Label
            Dim objSSL As New SSL
            Dim carts As ShoppingCart() = Nothing
            Dim sm As New ShoppingCartManager
            Dim cart As ShoppingCart
            Dim differentCart As Boolean = False
            Dim cm As CourseManager = Nothing
            Dim output1 As String = String.Empty    ' Output1 value return form SAP helpler

            objResult.partnumber = sPartNumberUpperCase
            objResult.Quantity = 1 '6994
            objResult.success = False
            'message = lmessage.Text

            Try
                If HttpContext.Current.Session.Item("customer") IsNot Nothing Then
                    customer = HttpContext.Current.Session.Item("customer")
                    'carts = objSSL.getAllShoppingCarts(customer, lmessage)
                    carts = objSSL.getAllShoppingCarts(customer, New Label)
                End If

                cart = sm.AddProductToCartQuickCart(carts, sPartNumberUpperCase, "1", True, output1)
                Dim bProperpart As Boolean = True
                If Not objSSL.IsCurrentCartSameType(cart) Then
                    differentCart = True
                    objResult.differentCart = True
                End If
                For Each i As ShoppingCartItem In cart.ShoppingCartItems
                    If Not i.Product.PartNumber.Equals(sPartNumberUpperCase) Then
                        HttpContext.Current.Session.Item("carts") = cart
                        objResult.items = cart.ShoppingCartItems.Length
                        objResult.totalAmount = String.Format("{0:c}", cart.ShoppingCartTotalPrice)
                        objResult.success = 2
                        message = "Replacement item " + i.Product.PartNumber + " added to cart in place of requested item  " + sPartNumberUpperCase + " which is no longer available."
                        objResult.message = message
                    Else
                        HttpContext.Current.Session.Item("carts") = cart
                        objResult.items = cart.ShoppingCartItems.Length
                        objResult.totalAmount = String.Format("{0:c}", cart.ShoppingCartTotalPrice)
                        objResult.success = 1
                        message = "Item " + sPartNumberUpperCase + " added to cart."
                        objResult.message = message
                    End If
                Next

            Catch exArg As ArgumentException
                If exArg.ParamName = "SomePartNotFound" Then
                    message = exArg.Message.Replace("Parameter name: SomePartNotFound", "")
                ElseIf exArg.ParamName = "PartNotFound" Then
                    message = exArg.Message.Replace("Parameter name: PartNotFound", "")
                ElseIf exArg.ParamName = "BillingOnlyItem" Then
                    message = exArg.Message.Replace("Parameter name: BillingOnlyItem", "")
                End If
            Catch ex As Exception
                '6524 V11 starts
                objPCILogger.EventOriginApplication = "ServicesPLUS"
                'objPCILogger.EventOriginApplicationLocation = Me.Page.GetType().Name
                objPCILogger.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
                objPCILogger.OperationalUser = Environment.UserName
                objPCILogger.EventDateTime = DateTime.Now.ToLongTimeString()
                objPCILogger.HTTPRequestObjectValues = objPCILogger.GetRequestContextValue()
                objPCILogger.CustomerID = customer.CustomerID
                objPCILogger.CustomerID_SequenceNumber = customer.SequenceNumber
                'objPCILogger.UserName = customer.UserName
                objPCILogger.EmailAddress = customer.EmailAddress '6524
                objPCILogger.SIAM_ID = customer.SIAMIdentity
                objPCILogger.LDAP_ID = customer.LdapID '6524
                objPCILogger.EventOriginMethod = "addPartToCartwSCatalog"
                objPCILogger.EventType = EventType.View
                '6524 V11 ends
                objPCILogger.IndicationSuccessFailure = "Failure" '6524
                objPCILogger.Message = "addPartToCart Failed. " & ex.Message.ToString() '6524
                objPCILogger.PushLogToMSMQ() '6524'6524 V11
                '7813 starts
                'message = Utilities.WrapExceptionforUI(ex)'7813
                Dim strUniqueKey As String = Utilities.GetUniqueKey
                message = Utilities.WrapExceptionforUINumber(ex, strUniqueKey)
                message = message.Remove(message.Length - 1)
                message = message + strUniqueKey + "."
                '7813 ends
            End Try
            objResult.message = message
            Return objResult
        End Function
	End Class

    Public Class PartresultCatalogDetails '6994
        Public _message As String
        Public _totalAmount As String
        Public _items As String
        Public _success As Integer
        Public _quantity As String
        Public _partnumber As String
        Public _differentCart As Boolean

        Property partnumber() As String
            Get
                Return _partnumber
            End Get
            Set(ByVal value As String)
                _partnumber = value
            End Set
        End Property
        Property message() As String
            Get
                Return _message
            End Get
            Set(ByVal value As String)
                _message = value
            End Set
        End Property
        Property Quantity() As String
            Get
                Return _quantity
            End Get
            Set(ByVal value As String)
                _quantity = value
            End Set
        End Property
        ''' <summary>
        ''' 0-Failure
        ''' 1-Success
        ''' 2-warning
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Property success() As Integer
            Get
                Return _success
            End Get
            Set(ByVal value As Integer)
                _success = value
            End Set
        End Property
        Property totalAmount() As String
            Get
                Return _totalAmount
            End Get
            Set(ByVal value As String)
                _totalAmount = value
            End Set
        End Property
        Property items() As String
            Get
                Return _items
            End Get
            Set(ByVal value As String)
                _items = value
            End Set
        End Property

        Property differentCart() As Boolean
            Get
                Return _differentCart
            End Get
            Set(ByVal value As Boolean)
                _differentCart = value
            End Set
        End Property

    End Class
    '6994 ends
End Namespace

