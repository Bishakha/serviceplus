Imports ServicesPlusException
Imports Sony.US.AuditLog
Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Data
Imports Sony.US.ServicesPLUS.Process

Namespace ServicePLUSWebApp

    Partial Class training_payment_accnt
        Inherits SSL

#Region " Web Form Designer Generated Code "
        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            isProtectedPage(True)
            InitializeComponent()
        End Sub
#End Region

#Region "   Page variables  "
        Public Auditcustomer As Customer
        Private customer As Customer = Nothing
        Private cm As New CustomerManager
        Private pm As New PaymentManager
        Private siamID As String
        Private cr As CourseReservation
        Private current_bill_to As Account
        Private current_bill_to_list() As Account
        Private focusFormField As String = "ddlBillTo"
        Public IsNonClass As Boolean = False
#End Region

#Region "   Events  "

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                If rbBillTo.SelectedIndex = 1 Then
                    trCCName.Style.Add("display", "inline")
                    trCCBilling.Style.Add("display", "inline")
                    trCCAddress1.Style.Add("display", "inline")
                    trCCAddress2.Style.Add("display", "inline")
                    trCCAddress3.Style.Add("display", "inline")
                    trCCCity.Style.Add("display", "inline")
                    trCCState.Style.Add("display", "inline")
                    trCCZip.Style.Add("display", "inline")
                Else
                    trCCName.Style.Add("display", "none")
                    trCCBilling.Style.Add("display", "none")
                    trCCAddress1.Style.Add("display", "none")
                    trCCAddress2.Style.Add("display", "none")
                    trCCAddress3.Style.Add("display", "none")
                    trCCCity.Style.Add("display", "none")
                    trCCState.Style.Add("display", "none")
                    trCCZip.Style.Add("display", "none")
                End If
                If HttpContextManager.Customer IsNot Nothing Then customer = HttpContextManager.Customer

                formFieldInitialization()

                Try
                    If Request.QueryString("nonclass") IsNot Nothing Then
                        IsNonClass = IIf(Request.QueryString("nonclass") = "1", True, False)
                    ElseIf Session.Item("IsNonClassRoom") IsNot Nothing Then
                        IsNonClass = IIf(Session.Item("IsNonClassRoom").ToString() = "1", True, False)
                    End If
                Catch ex As Exception
                    IsNonClass = False
                End Try

                If IsNonClass Then
                    Page.Title = "Sony Training Institute � Training Payment"   ' 2018-07-12 ASleight - No need to inject JavaScript for this.
                    'Response.Write("<script language='javascript'>window.document.title='Sony Training Institute � Training Payment'</script>")
                End If

                If customer IsNot Nothing Then
                    If Not Page.IsPostBack Then
                        cr = Session.Item("trainingreservation")
                        If cr Is Nothing Then
                            ErrorLabel.Text = ("No course reservation found. Your session may have expired.")
                            ErrorLabel.Visible = True
                            Return
                        End If
                        BuildDDls()
                        BuildState()
                        If IsNonClass Then
                            rbBillTo.Items(2).Text = ""
                            rbBillTo.Items.Remove(rbBillTo.Items(2))
                            ' besides vacancy, the start and end date if not specified then it also represents a waiting list
                            'included start date condition in the display message as well - Deepa V, Apr 27,2006
                        ElseIf cr.CourseSchedule.Vacancy > 0 And cr.CourseSchedule.StartDateDisplay <> "TBD" Then
                            rbBillTo.Items(2).Text = "Send me an Invoice to my Email Address"
                        Else
                            rbBillTo.Items(2).Text = "Send me a Quote to my Email Address"
                        End If
                        'If Not cr. Is Nothing Then TextBox4.Text = cart.PurchaseOrderNumber.ToString()
                        RegisterStartupScript("SetFocus", "<script language='javascript'>document.getElementById('" + focusFormField + "').focus()</script>")
                    End If
                End If
            Catch ex As Exception
                hideControls()
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Public Sub hideControls()
            dMain.Visible = False
            btnNext.Visible = False
        End Sub

        Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As ImageClickEventArgs) Handles btnCancel.Click
            If Session("mincatid") IsNot Nothing Then
                Dim id As Int32 = Convert.ToInt32(Session("mincatid"))
                Response.Redirect(String.Format("sony-training-catalog-2.aspx?mincatid={0}", id), True)
            Else
                Response.Redirect("sony-training-search.aspx", True)
            End If
        End Sub


        Private Sub btnNext_Click(ByVal sender As System.Object, ByVal e As ImageClickEventArgs) Handles btnNext.Click
            Dim objPCILogger As New PCILogger()
            Try
                'Added by Sunil on 14-12-09 for [Bug 2097] New: PCI Event Log 
                objPCILogger.EventOriginApplication = "ServicesPLUS"
                objPCILogger.EventOriginApplicationLocation = Page.GetType().Name
                objPCILogger.EventOriginMethod = "btnNext_Click"
                objPCILogger.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
                objPCILogger.OperationalUser = Environment.UserName
                objPCILogger.EmailAddress = customer.EmailAddress
                objPCILogger.CustomerID = customer.CustomerID
                objPCILogger.CustomerID_SequenceNumber = customer.SequenceNumber
                objPCILogger.SIAM_ID = customer.SIAMIdentity
                objPCILogger.LDAP_ID = customer.LdapID
                objPCILogger.EventDateTime = DateTime.Now.ToLongTimeString()
                objPCILogger.EventType = EventType.Add
                objPCILogger.HTTPRequestObjectValues = GetRequestContextValue()

                ErrorLabel.Visible = False

                ValidateForm()

                If ErrorValidation.Visible Then
                    ErrorValidation.Text = "Please complete the required fields." + " " + ErrorValidation.Text
                Else
                    Dim tm As New CourseManager
                    Dim card As CreditCard = Nothing
                    Dim order As Order
                    Dim cr As CourseReservation = Session.Item("trainingreservation")

                    current_bill_to = Session.Item("current_trainingbill_to")

                    'Finally create order
                    If cr Is Nothing Then
                        ErrorLabel.Text = ("No course reservation.")
                        ErrorLabel.Visible = True
                        Return
                    End If

                    'Bug 1082: for account holder City will be the Address4 of billto address
                    current_bill_to.Address.City = current_bill_to.Address.Line4
                    If card IsNot Nothing Then
                        order = tm.Checkout(customer, card, current_bill_to.Address)
                        cr.Sentinvoiceorquote = 0
                    Else
                        order = tm.Checkout(customer, current_bill_to)
                        If rbBillTo.SelectedIndex = 2 Then 'bill by invoice
                            cr.Sentinvoiceorquote = 1
                        Else
                            cr.Sentinvoiceorquote = 0
                        End If
                    End If
                    order.BillTo = current_bill_to.BillTo
                    order.POReference() = TextBox4.Text
                    If rbBillTo.SelectedIndex = 1 Then
                        order.CreditCard = card
                        order.BillTo.Line1 = CCAddress1.Text.Trim()
                        order.BillTo.Line2 = CCAddress2.Text.Trim()
                        order.BillTo.Line3 = CCAddress3.Text.Trim()
                        order.BillTo.Attn = CCName.Text.Trim()
                        order.BillTo.City = CCCity.Text.Trim()
                        order.BillTo.State = ddlCCState.SelectedValue
                        order.BillTo.PostalCode = CCZip.Text.Trim()
                    End If
                    Session("trainingorder") = order

                    Session.Remove("current_trainingbill_to_list")

                    objPCILogger.IndicationSuccessFailure = "Success"
                    objPCILogger.PushLogToMSMQ()

                    If IsNonClass Then
                        Response.Redirect("training-tc.aspx?nonclass=1", False)
                    Else
                        Response.Redirect("training-student.aspx", False)
                    End If
                End If
                'Added by Sunil on 14-12-09 for [Bug 2097] New: PCI Event Log 
            Catch exThrd As Threading.ThreadAbortException
                'Do Nothing
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                ErrorLabel.Visible = True
                objPCILogger.IndicationSuccessFailure = "Failure"
                objPCILogger.Message = ex.Message
                objPCILogger.PushLogToMSMQ()
                Return
            End Try
        End Sub
        Private Sub ddlBillTo_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlBillTo.SelectedIndexChanged
            'set current_bill_to 
            current_bill_to_list = Session.Item("current_trainingbill_to_list")
            If ddlBillTo.SelectedIndex() >= 1 Then
                current_bill_to = CType(current_bill_to_list.GetValue(ddlBillTo.SelectedIndex() - 1), Account)

                Session.Add("current_trainingbill_to", current_bill_to)
            Else
                Session.Add("current_trainingbill_to", Nothing)
            End If

        End Sub

        Private Sub rbBillTo_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbBillTo.SelectedIndexChanged

        End Sub
#End Region

#Region "   Methods     "

        Private Sub BuildDDls()
            '-- cc ddl's
            Try
                '-- cc months
                'ddlMonth.Items.Add(New ListItem("Month", ""))
                'ddlMonth.Items.Add(New ListItem("January", "1"))
                'ddlMonth.Items.Add(New ListItem("February", "2"))
                'ddlMonth.Items.Add(New ListItem("March", "3"))
                'ddlMonth.Items.Add(New ListItem("April", "4"))
                'ddlMonth.Items.Add(New ListItem("May", "5"))
                'ddlMonth.Items.Add(New ListItem("June", "6"))
                'ddlMonth.Items.Add(New ListItem("July", "7"))
                'ddlMonth.Items.Add(New ListItem("August", "8"))
                'ddlMonth.Items.Add(New ListItem("September", "9"))
                'ddlMonth.Items.Add(New ListItem("October", "10"))
                'ddlMonth.Items.Add(New ListItem("November", "11"))
                'ddlMonth.Items.Add(New ListItem("December", "12"))

                'populateYearddl()
                '-- cc years
                'Dim current_year As Integer = DateTime.Now.Year
                'Dim y As Integer
                'ddlYear.Items.Add(New ListItem("Year", ""))
                'For y = current_year To current_year + 10
                '    ddlYear.Items.Add(New ListItem(y.ToString, y.ToString))
                'Next


                Dim first_one As Boolean = True
                Dim sap_accounts As Boolean = False

                ''-- populate ddl with SAP accounts
                'Try
                '    'populate with SAP accounts if present
                '    For Each a As Account In customer.SAPBillToAccounts
                '        If a.Validated Then
                '            sap_accounts = True

                '            'get all address details per sap bill_to - bs 03/01/2004
                '            sis.SAPPayorInquiry(a, True)
                '            Dim thisSB As New StringBuilder
                '            If Not a.AccountNumber Is Nothing Then thisSB.Append(a.AccountNumber.ToString() + "-")
                '            If Not a.Address Is Nothing Then
                '                If Not a.Address.Name Is Nothing Then thisSB.Append(" " + a.Address.Name.ToString())
                '                If Not a.Address.Line1 Is Nothing Then thisSB.Append(" " + a.Address.Line1.ToString())
                '                If Not a.Address.Line2 Is Nothing Then thisSB.Append(" " + a.Address.Line2.ToString())
                '                If Not a.Address.Line3 Is Nothing Then thisSB.Append(" " + a.Address.Line3.ToString())
                '            End If

                '            ddlBillTo.Items.Add(New ListItem(thisSB.ToString(), a.AccountNumber))

                '            If first_one Then
                '                current_bill_to = a
                '                first_one = False
                '            End If
                '        End If
                '    Next
                'Catch ex As Exception
                '    ErrorLabel.Text = "Error building SAP accounts. " + ex.Message.ToString()
                'End Try

                ''-- populate ddl with SIS accounts
                'Try
                '    '***************Changes done for Bug# 1212 Starts here***************
                '    '**********Kannapiran S****************
                '    If ddlBillTo.Items.Count = 0 Then
                '        For Each b As SISAccount In customer.SISLegacyBillToAccounts
                '            cm.PopulateLegacyAccount(b)
                '            '***************Changes done for Bug# 1212 Ends here***************

                '            Dim thisSB As New StringBuilder
                '            If Not b.AccountNumber Is Nothing Then thisSB.Append(b.AccountNumber.ToString() + "-")
                '            If Not b.Address Is Nothing Then
                '                If Not b.Address.Name Is Nothing Then thisSB.Append(" " + b.Address.Name.ToString())
                '                If Not b.Address.Line1 Is Nothing Then thisSB.Append(" " + b.Address.Line1.ToString())
                '                If Not b.Address.Line2 Is Nothing Then thisSB.Append(" " + b.Address.Line2.ToString())
                '                If Not b.Address.Line3 Is Nothing Then thisSB.Append(" " + b.Address.Line3.ToString())
                '            End If

                '            ddlBillTo.Items.Add(New ListItem(thisSB.ToString(), b.AccountNumber))

                '            If first_one Then
                '                current_bill_to = b
                '                first_one = False
                '            End If
                '        Next
                '        Session.Add("current_trainingbill_to_list", customer.SISLegacyBillToAccounts)
                '    Else
                '        'if some weren't validated then would have an issue with this
                '        'customer service should make sure all valid or all not valid
                '        Session.Add("current_trainingbill_to_list", customer.SAPBillToAccounts)
                '    End If
                'Catch ex As Exception
                '    ErrorLabel.Text = "Error building SIS accounts. " + ex.Message.ToString()
                'End Try
                Dim arrAccount As Account()
                arrAccount = New Account(customer.SAPBillToAccounts.Length - 1) {}
                Dim iCount As Integer = 0
                'If customer.StatusID = 3 Then'6668
                If customer.UserType = "P" Then '6668
                    For Each objLoopAccount As Account In customer.SAPBillToAccounts
                        'If objLoopAccount.Validated Then
                        Dim thisSB As New StringBuilder
                        If Not objLoopAccount.AccountNumber Is Nothing Then thisSB.Append(objLoopAccount.AccountNumber.ToString())
                        ddlBillTo.Items.Add(New ListItem(thisSB.ToString(), objLoopAccount.AccountNumber))

                        If first_one Then
                            current_bill_to = objLoopAccount
                            first_one = False
                        End If

                        'End If
                    Next
                    'ddlBillTo.Enabled = False
                Else
                    For Each objLoopAccount As Account In customer.SAPBillToAccounts
                        'If objLoopAccount.Validated Then
                        ''changed for bug 6219
                        objLoopAccount = cm.PopulateCustomerDetailWithSAPAccount(objLoopAccount)
                        If Not objLoopAccount Is Nothing Then


                            Dim thisSB As New StringBuilder
                            If Not objLoopAccount.AccountNumber Is Nothing Then thisSB.Append(objLoopAccount.AccountNumber.ToString() + "-")
                            If Not objLoopAccount.Address Is Nothing Then
                                If Not objLoopAccount.Address.Name Is Nothing Then thisSB.Append(" " + objLoopAccount.Address.Name.ToString())
                                If Not objLoopAccount.Address.Line1 Is Nothing Then thisSB.Append(" " + objLoopAccount.Address.Line1.ToString())
                                If Not objLoopAccount.Address.Line2 Is Nothing Then thisSB.Append(" " + objLoopAccount.Address.Line2.ToString())
                            End If
                            ddlBillTo.Items.Add(New ListItem(thisSB.ToString(), objLoopAccount.AccountNumber))

                            If first_one Then
                                current_bill_to = objLoopAccount
                                first_one = False
                            End If
                            arrAccount(iCount) = objLoopAccount
                            iCount = iCount + 1
                        End If
                        'End If
                    Next
                End If

                '-- populate ddl with ship to data 
                If ddlBillTo.Items.Count > 1 Then
                    ddlBillTo.Items.Insert(0, "Select One")
                Else
                    Session.Add("current_trainingbill_to", current_bill_to) 'need to set it since user don't need to select one
                End If
                Session.Add("current_trainingbill_to_list", arrAccount)
                'populate credit card types
                Dim card_types() As CreditCardType
                card_types = pm.GetCreditCardTypes()
                Session.Add("card_types", card_types)

                'ddlType.Items.Add(New ListItem("Select One", ""))

                'For Each cc As CreditCardType In card_types
                '    ddlType.Items.Add(New ListItem(cc.Description, cc.CreditCardTypeCode))
                'Next

                'Load cc's from customer
                Dim cards() As CreditCard
                cards = customer.CreditCards
                'ddlMyCard.Items.Add(New ListItem("Select One", ""))
                'For Each c As CreditCard In cards
                '    If Not c.NickName = "" Then
                '        ddlMyCard.Items.Add(New ListItem(c.NickName))
                '    End If
                'Next
                For Each c As CreditCard In cards
                    '--exclude expired cards
                    Dim ccYear As Integer
                    Dim ccMonth As Integer
                    Dim cardDate As DateTime
                    Try
                        cardDate = Convert.ToDateTime(c.ExpirationDate)
                        ccYear = cardDate.Year
                        ccMonth = cardDate.Month
                    Catch ex As Exception
                        '-- make the card invalid
                        ccYear = DateTime.Today.Year - 1
                    End Try

                    Dim currentYear As Integer
                    Dim currentMonth As Integer
                    currentYear = DateTime.Today.Year
                    currentMonth = DateTime.Today.Month

                    'If c.NickName <> "" And c.HideCardFlag = False And (ccYear > currentYear Or (ccYear = currentYear And ccMonth > currentMonth)) Then
                    '    ddlMyCard.Items.Add(New ListItem(c.NickName))
                    'End If
                Next




            Catch ex As Exception
                hideControls()
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub


        Private Sub ValidateForm()
            Try
                ErrorValidation.Text = ""
                ErrorValidation.Visible = False

                LabelPoNumber.CssClass = "tableData"
                'LabelOneCard.CssClass = "tableData"
                'LabelCC.CssClass = "tableData"
                'LabelCCNumber.CssClass = "tableData"
                'LabelExpire.CssClass = "tableData"
                'LabelNName.CssClass = "tableData"
                LabelBillTo.CssClass = "tableData"
                'lblCVV.CssClass = "tableData"
                'Modified by Sunil for Bug 2597
                LabelCCName.CssClass = "tableData"
                LabelCCAddress1.CssClass = "tableData"
                LabelCCAddress2.CssClass = "tableData"
                LabelCCAddress3.CssClass = "tableData"
                LabelCCCity.CssClass = "tableData"
                LabelCCState.CssClass = "tableData"
                LabelCCZip.CssClass = "tableData"

                LabelCCNameStar.Text = ""
                LabelCCAddress1Star.Text = ""
                LabelCCAddress2Star.Text = ""
                LabelCCAddress3Star.Text = ""
                LabelCCCityStar.Text = ""
                LabelCCStateStar.Text = ""
                LabelCCZipStar.Text = ""

                'LabelOneStar.Text = ""
                'LabelCardStar.Text = ""

                'LabelCCNumberStar.Text = ""
                'LabelExpStar.Text = ""
                'LabelNNameStar.CssClass = "tableData"

                'Bill To
                current_bill_to = Session.Item("current_trainingbill_to")
                If current_bill_to Is Nothing Then
                    ErrorValidation.Visible = True
                    LabelBillTo.CssClass = "redAsterick"
                    LabelBillToStar.Text = "<span class=""redAsterick"">*</span>"
                End If

                'PO Number 
                If TextBox4.Text = "" Then
                    ErrorValidation.Visible = True
                    LabelPoNumber.CssClass = "redAsterick"
                    LabelPoNumberStar.Text = "<span class=""redAsterick"">*</span>"
                ElseIf TextBox4.Text.Length > 20 Then
                    ErrorValidation.Visible = True
                    'CCErrorLabel.Text = "PO Length must be less or equal to 20."
                    ErrorValidation.Text = "PO Length must be less or equal to 20."
                    LabelPoNumber.CssClass = "redAsterick"
                    LabelPoNumberStar.Text = "<span class=""redAsterick"">*</span>"
                End If

                'Check if pay by credit card radio button is checked. If it is, make credit card info required
                If rbBillTo.SelectedIndex = 1 Then
                    'Modified by Sunil for Bug 2597
                    If CCName.Text.Trim() = "" Then
                        ErrorLabel.Visible = True
                        LabelCCName.CssClass = "redAsterick"
                        LabelCCNameStar.Text = "*"
                    End If

                    If CCAddress1.Text.Trim() = "" Then
                        ErrorLabel.Visible = True
                        LabelCCAddress1.CssClass = "redAsterick"
                        LabelCCAddress1Star.Text = "*"
                    End If
                    If CCAddress1.Text.Length > 35 Then
                        ErrorLabel.Visible = True
                        LabelCCAddress1.CssClass = "redAsterick"
                        LabelCCAddress1Star.Text = "<span class=""redAsterick"">Credit Card Street Address must be 35 characters or less in length.</span>"
                        CCAddress1.Focus()
                    End If
                    If CCAddress2.Text.Trim() = "" Then
                        ErrorLabel.Visible = True
                        LabelCCAddress2.CssClass = "redAsterick"
                        LabelCCAddress2Star.Text = "*"
                    End If
                    If CCAddress2.Text.Length > 35 Then
                        ErrorLabel.Visible = True
                        LabelCCAddress2.CssClass = "redAsterick"
                        LabelCCAddress2Star.Text = "<span class=""redAsterick"">Credit Card Address 2nd Line must be 35 characters or less in length.</span>"
                        CCAddress2.Focus()
                    End If

                    If CCAddress3.Text.Length > 35 Then
                        ErrorLabel.Visible = True
                        LabelCCAddress3.CssClass = "redAsterick"
                        LabelCCAddress3Star.Text = "<span class=""redAsterick"">Credit Card Address 3rd Line must be 35 characters or less in length.</span>"
                        CCAddress3.Focus()
                    End If

                    If CCCity.Text.Trim() = "" Then
                        ErrorLabel.Visible = True
                        LabelCCCity.CssClass = "redAsterick"
                        LabelCCCityStar.Text = "*"
                    ElseIf CCCity.Text.Length > 35 Then
                        ErrorLabel.Visible = True
                        LabelCCCity.CssClass = "redAsterick"
                        LabelCCCityStar.Text = "<span class=""redAsterick"">Credit Card City must be 35 characters or less in length.</span>"
                        CCCity.Focus()
                    End If

                    If ddlCCState.SelectedIndex = 0 Then
                        ErrorValidation.Visible = True
                        LabelCCState.CssClass = "redAsterick"
                        LabelCCStateStar.Text = "<span class=""redAsterick"">*</span>"
                    End If

                    If CCZip.Text.Trim() = "" Then
                        ErrorLabel.Visible = True
                        LabelCCZip.CssClass = "redAsterick"
                        LabelCCZipStar.Text = "*"
                    End If

                    Dim sZip As String = CCZip.Text.Trim()
                    Dim objZipCodePattern As New Regex("\d{5}(-\d{4})?")
                    If Not objZipCodePattern.IsMatch(sZip) Then
                        ErrorValidation.Visible = True
                        LabelCCZip.CssClass = "redAsterick"
                        LabelCCZipStar.Text = "<span class=""redAsterick"">*</span>"
                    End If

                    'If ddlMonth.SelectedIndex = 0 Then
                    '    ErrorValidation.Visible = True
                    '    LabelExpire.CssClass = "redAsterick"
                    '    LabelExpStar.Text = "<span class=""redAsterick"">*</span>"
                    'End If

                    'If ddlYear.SelectedIndex = 0 Then
                    '    ErrorValidation.Visible = True
                    '    LabelExpire.CssClass = "redAsterick"
                    '    LabelExpStar.Text = "<span class=""redAsterick"">*</span>"
                    'End If

                    'If Me.ddlMonth.SelectedIndex > 0 Then
                    '    Dim selectedMonth As Integer = Me.ddlMonth.SelectedValue
                    '    If selectedMonth < DateTime.Today().Month Then
                    '        If ddlYear.SelectedValue.ToString() = "0" Then
                    '            ErrorValidation.Visible = True
                    '            LabelExpire.CssClass = "redAsterick"
                    '            LabelExpStar.Text = "<span class=""redAsterick"">*</span>"
                    '        End If

                    '        If ddlYear.SelectedValue.ToString() <= DateTime.Today.Year.ToString() Then
                    '            ErrorValidation.Visible = True
                    '            LabelExpire.CssClass = "redAsterick"
                    '            LabelExpStar.Text = "<span class=""redAsterick"">*</span>"
                    '        End If
                    '    End If
                    'End If



                    'If ddlType.SelectedIndex = 0 Then
                    '    ErrorValidation.Visible = True
                    '    LabelCC.CssClass = "redAsterick"
                    '    LabelCardStar.Text = "*"
                    'End If

                    'If ddlType.SelectedIndex > 0 Then
                    '    If CCNumber.Text = "" Then
                    '        ErrorLabel.Visible = True
                    '        LabelCCNumber.CssClass = "redAsterick"
                    '        LabelCCNumberStar.Text = "*"
                    '    Else
                    '        Dim strCCErroMessage As String = String.Empty
                    '        Dim strCardNumber As String = String.Empty
                    '        If ddlMyCard.SelectedIndex > 0 Then
                    '            For index = 0 To customer.CreditCards.Length - 1
                    '                If customer.CreditCards(index).NickName = ddlMyCard.SelectedValue And customer.CreditCards(index).HideCardFlag = False Then
                    '                    strCardNumber = customer.CreditCards(index).CreditCardNumber
                    '                End If
                    '            Next
                    '        Else
                    '            strCardNumber = CCNumber.Text
                    '        End If
                    '        Dim blnCCValidaiton As Boolean = True ' Sony.US.SIAMUtilities.Validation.IsCreditCardValid(strCardNumber, ddlType.SelectedItem.Text.Substring(0, 1).ToUpper(), strCCErroMessage)
                    '        LabelCCNumberStar.Text = IIf(blnCCValidaiton, "*", strCCErroMessage)
                    '        If blnCCValidaiton = False Then
                    '            LabelCCNumber.CssClass = "redAsterick"
                    '            ErrorLabel.Visible = True
                    '        End If
                    '    End If
                    'End If

                    'If CCNumber.Text = "" Then
                    '    ErrorValidation.Visible = True
                    '    LabelCCNumber.CssClass = "redAsterick"
                    '    LabelCCNumberStar.Text = "<span class=""redAsterick"">*</span>"
                    'End If

                    'IF Checked then Credit Card Nick Name must be completed
                    'If CheckBox1.Checked Then
                    '    Dim isControlValid As Boolean = False
                    '    Dim thisPattern As String = "^[a-zA-Z0-9]*$"

                    '    If TextBox6.Text.Length > 1 And TextBox6.Text.Length <= 17 Then
                    '        If ddlMyCard.Items.FindByText(TextBox6.Text.ToString()) Is Nothing Or TextBox6.Text.ToString() = ddlMyCard.SelectedValue.ToString() Then
                    '            If Regex.IsMatch(TextBox6.Text, thisPattern) = True Then
                    '                isControlValid = True
                    '            End If
                    '        Else
                    '            CCErrorLabel.Text = "You must select a unique Credit Card Nickname." + vbCrLf
                    '        End If
                    '    Else
                    '        CCErrorLabel.Text = "Nicknames must be between 1 and 17 characters." + vbCrLf
                    '    End If

                    '    If isControlValid = False Then
                    '        ErrorValidation.Visible = True
                    '        LabelNName.CssClass = "redAsterick"
                    '        LabelNNameStar.Text = "<span class=""redAsterick"">*</span>"
                    '    End If

                    'End If
                    'If txtCVV.Text = "" Then
                    '    ErrorValidation.Visible = True

                    '    lblCVV.CssClass = "redAsterick"
                    'End If
                End If




            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub


        'Private Sub populateYearddl()
        '    Dim currentDate As Date = DateTime.Today()
        '    Dim currentYear As Integer
        '    currentYear = currentDate.Year()
        '    ddlYear.Items.Clear()

        '    'If Me.ddlMonth.SelectedIndex > 0 Then
        '    '    Dim selectedMonth As Integer = Me.ddlMonth.SelectedValue
        '    '    If selectedMonth < currentDate.Month Then
        '    '        currentYear += 1
        '    '    End If
        '    '    Dim i As Integer
        '    '    For i = currentYear To currentYear + 20
        '    '        ddlYear.Items.Add(New ListItem(i.ToString(), i.ToString()))
        '    '    Next i
        '    'End If

        '    Dim i As Integer
        '    For i = currentYear To currentYear + 20
        '        ddlYear.Items.Add(New ListItem(i.ToString(), i.ToString()))
        '    Next i

        '    Dim list0 As ListItem = New ListItem
        '    list0.Value = "0"
        '    list0.Text = "Year"
        '    ddlYear.Items.Insert(0, list0)
        '    ddlYear.CssClass = "tableData"
        '    ddlYear.SelectedValue = "0"
        'End Sub

        Private Sub formFieldInitialization()
            TextBox4.Attributes("onkeydown") = "SetTheFocusButton(event, 'btnNext')"
            'CCNumber.Attributes("onkeydown") = "SetTheFocusButton(event, 'btnNext')"
            'TextBox6.Attributes("onkeydown") = "SetTheFocusButton(event, 'btnNext')"
        End Sub

#End Region


        'Protected Sub ddlMonth_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        '    'populateYearddl()
        'End Sub
        Public Function getAuditCCNo(ByVal thisCCNo As String) As String
            Dim returnValue As String = ""

            If thisCCNo <> String.Empty Then
                Dim ccNumberLength As Int16 = thisCCNo.Length
                Dim startValue As String = thisCCNo.Substring(0, 4)
                startValue = startValue.PadRight((ccNumberLength - 4), "X")
                Dim endValue As String = thisCCNo.Substring((ccNumberLength - 4), 4)
                returnValue = startValue + endValue
            End If

            Return returnValue
        End Function


        Public Function selectCCNo(ByVal aCustomer As Integer) As String
            Dim creditNo As String
            Dim objAuditDataMgr As New AuditDataManager
            creditNo = objAuditDataMgr.SelectCC(aCustomer)

            Return creditNo
        End Function

        Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
            'If (ddlMyCard.SelectedIndex = 0) Then
            '    If (ErrorLabel.Text.Length > 0 Or ErrorValidation.Text.Length > 0 Or CCErrorLabel.Text.Length > 0) Then
            '        txtCVV.Text = ""
            '        CCNumber.Text = ""
            '    End If
            'End If
        End Sub

        Private Sub BuildState()
            ddlCCState.Items.Add(New ListItem("Select One", ""))
            ddlCCState.Items.Add(New ListItem("AL", "AL"))
            ddlCCState.Items.Add(New ListItem("AK", "AK"))
            ddlCCState.Items.Add(New ListItem("AR", "AR"))
            ddlCCState.Items.Add(New ListItem("AZ", "AZ"))
            ddlCCState.Items.Add(New ListItem("CA", "CA"))
            ddlCCState.Items.Add(New ListItem("CO", "CO"))
            ddlCCState.Items.Add(New ListItem("CT", "CT"))
            ddlCCState.Items.Add(New ListItem("DC", "DC"))
            ddlCCState.Items.Add(New ListItem("DE", "DE"))
            ddlCCState.Items.Add(New ListItem("FL", "FL"))
            ddlCCState.Items.Add(New ListItem("GA", "GA"))
            ddlCCState.Items.Add(New ListItem("HI", "HI"))
            ddlCCState.Items.Add(New ListItem("IA", "IA"))
            ddlCCState.Items.Add(New ListItem("ID", "ID"))
            ddlCCState.Items.Add(New ListItem("IL", "IL"))
            ddlCCState.Items.Add(New ListItem("IN", "IN"))
            ddlCCState.Items.Add(New ListItem("KS", "KS"))
            ddlCCState.Items.Add(New ListItem("KY", "KY"))
            ddlCCState.Items.Add(New ListItem("LA", "LA"))
            ddlCCState.Items.Add(New ListItem("MA", "MA"))
            ddlCCState.Items.Add(New ListItem("MD", "MD"))
            ddlCCState.Items.Add(New ListItem("ME", "ME"))
            ddlCCState.Items.Add(New ListItem("MI", "MI"))
            ddlCCState.Items.Add(New ListItem("MN", "MN"))
            ddlCCState.Items.Add(New ListItem("MO", "MO"))
            ddlCCState.Items.Add(New ListItem("MS", "MS"))
            ddlCCState.Items.Add(New ListItem("MT", "MT"))
            ddlCCState.Items.Add(New ListItem("NC", "NC"))
            ddlCCState.Items.Add(New ListItem("ND", "ND"))
            ddlCCState.Items.Add(New ListItem("NE", "NE"))
            ddlCCState.Items.Add(New ListItem("NH", "NH"))
            ddlCCState.Items.Add(New ListItem("NJ", "NJ"))
            ddlCCState.Items.Add(New ListItem("NM", "NM"))
            ddlCCState.Items.Add(New ListItem("NV", "NV"))
            ddlCCState.Items.Add(New ListItem("NY", "NY"))
            ddlCCState.Items.Add(New ListItem("OH", "OH"))
            ddlCCState.Items.Add(New ListItem("OK", "OK"))
            ddlCCState.Items.Add(New ListItem("OR", "OR"))
            ddlCCState.Items.Add(New ListItem("PA", "PA"))
            ddlCCState.Items.Add(New ListItem("RI", "RI"))
            ddlCCState.Items.Add(New ListItem("SC", "SC"))
            ddlCCState.Items.Add(New ListItem("SD", "SD"))
            ddlCCState.Items.Add(New ListItem("TN", "TN"))
            ddlCCState.Items.Add(New ListItem("TX", "TX"))
            ddlCCState.Items.Add(New ListItem("UT", "UT"))
            ddlCCState.Items.Add(New ListItem("VA", "VA"))
            ddlCCState.Items.Add(New ListItem("VT", "VT"))
            ddlCCState.Items.Add(New ListItem("WA", "WA"))
            ddlCCState.Items.Add(New ListItem("WI", "WI"))
            ddlCCState.Items.Add(New ListItem("WV", "WV"))
            ddlCCState.Items.Add(New ListItem("WY", "WY"))
        End Sub

    End Class

End Namespace
