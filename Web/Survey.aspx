<%@ Reference Page="~/SSL.aspx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="ServicePlusNav" Src="ServicePlusNav.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="Message" Src="PersonalMessage.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="TopSonyHeader" Src="TopSonyHeader.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="SonyFooter" Src="SonyFooter.ascx" %>

<%@ Page Language="vb" AutoEventWireup="false" Inherits="ServicePLUSWebApp.Survey" EnableViewStateMac="true" CodeFile="Survey.aspx.vb" CodeFileBaseClass="ServicePLUSWebApp.SSL" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="<%=PageLanguage %>">
<head>
    <title>ServicesPLUS - Survey</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link href="includes/ServicesPLUS_style.css" type="text/css" rel="stylesheet">
    <script language="javascript" src="includes/ServicesPLUS.js" type="text/javascript"></script>
    <script type="text/javascript" src="includes/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="includes/svcplus_globalization.js"></script>
    <script type="text/javascript">
        function limitTextArea(thisControl, thisLength) {

            if (thisControl.value.length >= thisLength) {
                thisControl.value = thisControl.value.substring(0, thisLength);
                alert('You have reached the max length of the text area.');
            }
        }
    </script>
</head>
<body style="color: #000000; background: #5d7180; margin: 0px;">
    <form name="form1" runat="server">
        <center>
            <table width="760" border="0" role="presentation">
                <tr>
                    <td width="25" style="background: url('images/sp_left_bkgd.gif');">
                        <img src="images/spacer.gif" width="25" height="25" alt="">
                    </td>
                    <td width="710" bgcolor="#ffffff">
                        <table width="710" border="0" role="presentation">
                            <tr>
                                <td>
                                    <ServicePLUSWebApp:TopSonyHeader ID="TopSonyHeader" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <nav><ServicePLUSWebApp:ServicePlusNav ID="SonyNav" runat="server" /></nav>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <img height="43" src="images/sp_int_satSurvey_hdr.gif" width="710" alt="Survey">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="710" border="0" role="presentation">
                                        <tr>
                                            <td width="20" height="20">
                                                <img height="20" src="images/spacer.gif" width="20" alt="">
                                            </td>
                                            <td width="670" height="20">
                                                <asp:Label ID="FormErrorsLabel" runat="server" CssClass="tableData" ForeColor="Red"></asp:Label>
                                            </td>
                                            <td width="20" height="20">
                                                <img height="20" src="images/spacer.gif" width="20" alt="">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="20">
                                                <img height="20" src="images/spacer.gif" width="20" alt="">
                                            </td>
                                            <td width="670">

                                                <table width="670" border="0" role="presentation">
                                                    <tbody>
                                                        <tr>
                                                            <td width="3" bgcolor="#ffffff">
                                                                <img height="1" src="images/spacer.gif" width="3" alt="">
                                                            </td>
                                                            <td width="125" bgcolor="#ffffff">
                                                                <img height="1" src="images/spacer.gif" width="125" alt="">
                                                            </td>
                                                            <td width="542" bgcolor="#ffffff">
                                                                <img height="1" src="images/spacer.gif" width="542" alt="">
                                                            </td>
                                                        </tr>
                                                        <tr bgcolor="#d5dee9" id="surveyTR" runat="server">
                                                            <td width="3">
                                                                <img src="images/spacer.gif" width="3" height="15" alt="">
                                                            </td>
                                                            <td colspan="2" class="bodyCopy">
                                                                <img src="images/spacer.gif" width="20" height="5" alt=""><br>
                                                                <asp:Table ID="topSurveryText" runat="server"></asp:Table>
                                                                <img src="images/spacer.gif" width="20" height="5" alt="">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="3" height="12"></td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <img height="30" src="images/spacer.gif" width="3" alt="">
                                                            </td>
                                                            <td class="bodyCopy" colspan="2">
                                                                <img height="5" src="images/spacer.gif" width="3" alt=""><br>
                                                                <asp:Table ID="SurveyTable" runat="server"></asp:Table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <img height="30" src="images/spacer.gif" width="3" alt="">
                                                            </td>
                                                            <td colspan="2">
                                                                <br>
                                                                <asp:ImageButton ID="SubmitFormButton" runat="server" Height="30" Width="92" ImageUrl="<%=Resources.Resource.img_btnSubmit()%>"
                                                                    AlternateText="Submit"></asp:ImageButton>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                            <td width="20">
                                                <img height="20" src="images/spacer.gif" width="20" alt="">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>

                                <td>
                                    <footer><ServicePLUSWebApp:SonyFooter ID="SonyFooter1" runat="server" /></footer>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="25" style="background: url('images/sp_right_bkgd.gif')">
                        <img src="images/spacer.gif" width="25" height="20" alt="">
                    </td>
                </tr>
            </table>
        </center>
    </form>
</body>
</html>
