<%@ Reference Page="~/SSL.aspx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="TopSonyHeader" Src="TopSonyHeader.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="ServicePlusNav" Src="ServicePlusNav.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="PersonalMessage" Src="PersonalMessage.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="SonyFooter" Src="SonyFooter.ascx" %>

<%@ Page Language="vb" AutoEventWireup="false" Inherits="ServicePLUSWebApp.TC2" EnableViewStateMac="true" CodeFile="TC2.aspx.vb" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="<%=PageLanguage %>">
<head>
    <title><%=Resources.Resource.tc_hdr_msg()%></title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link href="includes/ServicesPLUS_style.css" type="text/css" rel="stylesheet">
    <script language="javascript" src="includes/ServicesPLUS.js" type="text/javascript"></script>
    <script type="text/javascript" src="includes/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="includes/svcplus_globalization.js"></script>
    <style type="text/css">
        .style1 {
            height: 1px;
        }

        .style2 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: 11px;
            line-height: 13px;
            color: #666666;
            height: 1px;
        }
    </style>
</head>
<body text="#000000" bgcolor="#5d7180" leftmargin="0" topmargin="0" marginheight="0" marginwidth="0">
    <form id="Form1" method="post" runat="server">
        <center>
            <table width="760" border="0" role="presentation">
                <tr>
                    <td style="width: 24px" width="24" background="images/sp_left_bkgd.gif">
                        <img height="25" src="images/spacer.gif" width="25" alt="">
                    </td>
                    <td width="710" bgcolor="#ffffff">
                        <table width="710" border="0" role="presentation">
                            <tr>
                                <td>
                                    <ServicePLUSWebApp:TopSonyHeader ID="TopSonyHeader" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <nav><ServicePLUSWebApp:ServicePlusNav ID="SonyNav" runat="server" /></nav>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="710" border="0" role="presentation">
                                        <tr>
                                            <td width="464" bgcolor="#363d45" height="57" background="images/sp_int_header_top_ServicesPLUS_onepix.gif"
                                                align="right" valign="top">
                                                <br>
                                                <h1 class="headerText"><%=Resources.Resource.header_serviceplus()%> &nbsp;&nbsp;</h1>
                                            </td>
                                            <td valign="middle" bgcolor="#363d45">
                                                <ServicePLUSWebApp:PersonalMessage ID="PersonalMessageDisplay" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td bgcolor="#f2f5f8">
                                                <h2 class="headerTitle" style="padding-right: 20px; text-align: right;"><%=Resources.Resource.tc_hdr_msg%></h2>
                                            </td>
                                            <td bgcolor="#99a8b5">&nbsp;</td>
                                        </tr>
                                        <tr height="9">
                                            <td bgcolor="#f2f5f8">
                                                <img height="9" src="images/sp_int_header_btm_left_onepix.gif" width="464" alt="">
                                            </td>
                                            <td bgcolor="#99a8b5">
                                                <img height="9" src="images/sp_int_header_btm_right.gif" width="246" alt="">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="600" border="0" role="presentation">
                                        <tr>
                                            <td>
                                                <img height="29" src="images/spacer.gif" width="15" alt="" />
                                            </td>
                                            <td align="center">
                                                <br />
                                                <h3 class="bodyCopyBold" style="line-height: 20px;"><%=Resources.Resource.tc_limpw_msg()%></h3>
                                            </td>
                                        </tr>
                                        <tr valign="top">
                                            <td class="style1">
                                                <img height="25" src="images/spacer.gif" width="15" alt="" />
                                            </td>
                                            <td class="style2">
                                                <p>
                                                    <span class="bodyCopy"><a href="http://pro.sony.com/bbsc/ssr/services.servicesprograms.bbsccms-services-servicesprograms-warrantyinformation.shtml"
                                                        target="_blank" title="Click to view the full Limited Product Warranty in a new tab."><%=Resources.Resource.tc_clickhre_msg%></a><%=Resources.Resource.tc_clickhre_msg_1%>
                                                    </span>
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <img height="20" src="images/spacer.gif" width="15" alt="">
                                            </td>
                                            <td align="center">
                                                <h3 class="bodyCopyBold"><%=Resources.Resource.tc_part_msg()%></h3>
                                            </td>
                                        </tr>
                                        <tr valign="top">
                                            <td>
                                                <img height="29" src="images/spacer.gif" width="15" alt="">
                                            </td>
                                            <td>
                                                <p>
                                                    <span class="bodyCopy"><span class="bodyCopyBold"><%=Resources.Resource.tc_noticlaim_msg()%></span> &nbsp;<%=Resources.Resource.tc_noticlaim_msg_1()%></span>
                                                </p>
                                                <p>
                                                    <span class="bodyCopy"><span class="bodyCopyBold"><%=Resources.Resource. tc_exrmdy_msg()%></span>&nbsp;<%=Resources.Resource.tc_exrmdy_msg_1()%>&nbsp;</span>
                                                </p>
                                                <p>
                                                    <span class="bodyCopyBold"><%=Resources.Resource.tc_exlmtd_msg()%></span> &nbsp;<%=Resources.Resource.tc_exlmtd_msg_1()%>&nbsp;
                                                </p>
                                                <p>
                                                    <span class="bodyCopyBold"><%=Resources.Resource.tc_disofwrnty_msg()%> </span><span class="bodyCopy"><%=Resources.Resource.tc_disofwrnty_msg_1()%> </span>
                                                </p>
                                                <p>
                                                    <span class="bodyCopyBold"><%=Resources.Resource.tc_lmtoflblty_msg()%> </span><span class="bodyCopy"><%=Resources.Resource.tc_lmtoflblty_msg_1 ()%></span>
                                                </p>
                                                <span class="legalCopy"><a class="legalLink" href="http://pro.sony.com/bbsccms/services/files/servicesprograms/WARNTY2f.pdf"
                                                    target="_blank" title="Click to view the Limited Parts Warranty PDF document in a new tab."><%=Resources.Resource.tc_lmtprtswrnty_msg()%></a> </span>
                                                <br />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <img height="29" src="images/spacer.gif" width="15" alt="">
                                            </td>
                                            <td align="center">
                                                <h3 class="bodyCopyBold"><%=Resources.Resource.tc_sftr_msg()%></h3>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <img height="29" src="images/spacer.gif" width="15" alt="">
                                            </td>
                                            <td>
                                                <p class="bodyCopyBold"><%=Resources.Resource.tc_sftragrmnt_msg()%></p>
                                                <p class="bodyCopy">
                                                    <span class="bodyCopyBold"><%=Resources.Resource.tc_imprdcar_msg()%></span>
                                                        <%=Resources.Resource.tc_imprdcar_msg_1()%>
                                                </p>
                                                <p>
                                                    <span class="bodyCopyBold"><%=Resources.Resource.tc_sftlic_msg()%>&nbsp;</span><%=Resources.Resource.tc_sftlic_msg_1()%>
                                                </p>
                                                <p>
                                                    <span class="bodyCopyBold"><%=Resources.Resource.tc_grntoflic_msg()%></span><%=Resources.Resource.tc_licrghts_msg()%>&nbsp;&nbsp;<br>
                                                    - <%=Resources.Resource.tc_licrghts_msg_1()%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
														<br>
                                                    - <%=Resources.Resource.tc_licrghts_msg_2()%>
                                                </p>
                                                <p>
                                                    <span class="bodyCopyBold"><%=Resources.Resource.tc_mpeg_msg()%> </span><%=Resources.Resource.tc_mpeg_msg_1()%>
                                                </p>
                                                <p>
                                                    <span class="bodyCopyBold"><%=Resources.Resource.tc_desclim_msg()%></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>
                                                    - <%=Resources.Resource.tc_desclim_msg_1()%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>
                                                    - <%=Resources.Resource.tc_desclim_msg_2()%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>
                                                    - <%=Resources.Resource.tc_desclim_msg_3()%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>
                                                    - <%=Resources.Resource.tc_desclim_msg_4()%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>
                                                    -<%=Resources.Resource.tc_desclim_msg_5()%>&nbsp;
                                                </p>
                                                <p>
                                                    <span class="bodyCopyBold"><%=Resources.Resource.tc_cpyrght_msg()%></span>
                                                    &nbsp;<%=Resources.Resource.tc_cpyrght_msg_1()%>
                                                &nbsp;
													<p>
                                                        <span class="bodyCopyBold"><%=Resources.Resource.tc_prsprt_msg()%></span>
                                                        <%=Resources.Resource.tc_prsprt_msg_1()%>
                                                <p>
                                                    <span class="bodyCopyBold"><%=Resources.Resource.tc_prsprt_msg_2()%></span>
                                                    <%=Resources.Resource.tc_prsprt_msg_3()%>
                                                <p>
                                                    <span class="bodyCopyBold"><%=Resources.Resource.tc_exwarsft_msg()%></span>
                                                    &nbsp;<%=Resources.Resource.tc_exwarsft_msg_1()%>
                                                &nbsp;
													<p><span class="bodyCopyBold"><%=Resources.Resource.tc_limlia_msg()%></span><%=Resources.Resource.tc_limlia_msg_1()%></p>
                                                <p>&nbsp;</p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <img height="29" src="images/spacer.gif" width="15" alt="">
                                            </td>
                                            <td align="center"><h3 class="bodyCopyBold"><%=Resources.Resource.tc_training_msg()%></h3></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <img height="29" src="images/spacer.gif" width="15" alt="">
                                            </td>
                                            <td>
                                                <p class="bodyCopy">
                                                    <span class="bodyCopyBold"><%=Resources.Resource.tc_htltra_msg()%>&nbsp;</span>
                                                        <%=Resources.Resource.tc_htltra_msg_1()%>
                                                </p>
                                                <p>
                                                    <span class="bodyCopyBold"><%=Resources.Resource.tc_htltra_msg_1()%>&nbsp;</span><%=Resources.Resource.tc_htltra_msg_2()%>
                                                </p>
                                                <p>
                                                    <%=Resources.Resource.tc_htltra_msg_3()%>
                                                </p>
                                                <p>&nbsp;</p>
                                            </td>
                                        </tr>
                                    </table>

                                </td>
                            </tr>
                            <tr>

                                <td>
                                    <footer><ServicePLUSWebApp:SonyFooter ID="SonyFooter1" runat="server" /></footer>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="25" background="images/sp_right_bkgd.gif">
                        <img height="20" src="images/spacer.gif" width="25" alt="">
                    </td>
                </tr>
            </table>
        </center>
    </form>
</body>
</html>
