﻿var exception = false;

// Resource strings
var atc_ex_InvalidPartNumber = "";
var atc_ex_InvalidQuantityForPart, atc_ex_InvalidPartAndQuantity, atc_ex_DifferentCart;
var atc_title_OrderStatus, atc_title_OrderExceptions, atc_title_QuickOrderStatus;
var atc_title_QuickOrderExceptions, atc_AddingItem, atc_ToCart, atc_warn_LongWait;
var prg_PleaseWait;
var quickOrderMaxItems;
var orderUploadMaxItems;

// Load Resource and Configuration variables from the code-behind. Cannot use ASP tags in a .js file.
function initResourceStrings() {
    atc_ex_InvalidPartNumber = $("#atc_ex_InvalidPartNumber").val();
    atc_ex_InvalidQuantityForPart = $("#atc_ex_InvalidQuantityForPart").val();
    atc_ex_InvalidPartAndQuantity = $("#atc_ex_InvalidPartAndQuantity").val();
    atc_ex_DifferentCart = $("#atc_ex_DifferentCart").val();
    atc_title_OrderStatus = $("#atc_title_OrderStatus").val();
    atc_title_OrderExceptions = $("#atc_title_OrderExceptions").val();
    atc_title_QuickOrderStatus = $("#atc_title_QuickOrderStatus").val();
    atc_title_QuickOrderExceptions = $("#atc_title_QuickOrderExceptions").val();
    atc_AddingItem = $("#atc_AddingItem").val();
    atc_ToCart = $("#atc_ToCart").val();
    atc_warn_LongWait = $("#atc_warn_LongWait").val();
    prg_PleaseWait = $("#prg_PleaseWait").val();
    quickOrderMaxItems = parseInt($("#quickOrderMaxItems").val(), 10);
    orderUploadMaxItems = parseInt($("#orderUploadMaxItems").val(), 10);
    //closeButtonHtml = '<br/><br/><a><img src ="' + img_btnCloseWindow + '" alt ="Close" onclick="javascript:return closedilog();"  id="messageclose" /></a>';
}

function closedilog() {
    $("#dialog").dialog("close");
    return true;
}

function replaceAll(txt, replace, with_this) { return txt.replace(new RegExp(replace, 'g'), with_this); }

function validPart(partNumber, quantity) {
    if ((partNumber == "") && (quantity == "")) {
        return false;
    } else if (partNumber == "") {
        exception = true;
        $("#divException").append("<br/><b>" + atc_ex_InvalidPartNumber + "</b><br/>");   // 'Please enter a valid part.'
        return false;
    } else if ((quantity == "") || (quantity.match("[^0-9]")) || (quantity <= 0)) {
        exception = true;
        $("#divException").append("<br/><b>" + atc_ex_InvalidQuantityForPart + " " + partNumber + ".</b><br/>");   // 'Please enter a valid order quantity for part number '
        return false;
    }
    return true;
}

// Clears the text areas and resets flags
function resetModal() {
    if (atc_ex_InvalidPartNumber == "")   // Set up the Resource Strings the first time.
        initResourceStrings();
    exception = false;
    $("#divException").text('');
    $("#divException").hide();
    $("#divMessage").text('');
    $("#divCloseButton").hide();
    $("#divPrg").html('');
}

//function AddToCart_onclick(partNumber) {
//    AddToCart_onclick(partNumber, "1");
//}

function AddToCart_onclick(partNumber, quantity) {
    var ajaxPromises = [];  // For storing each AJAX call and ensuring we wait until they're all done.
    resetModal();           // Clears the text areas and resets flags
    partNumber = replaceAll(jQuery.trim(partNumber), "-", "");
    //if (quantity == "" || quantity == null)
        //quantity = "1";
    quantity = quantity || "1";
    quantity = jQuery.trim(quantity);
    $("#hidespstextitemno").val(partNumber);
    $("#hidespstextqty").val(quantity);

    $("#dialog").dialog({
        title: atc_title_OrderStatus,  // Order Add to Cart Status
        top: 75,
        height: 200,
        width: 400,
        modal: true,
        resizable: false
    });
    // Remove the default "Close" button in the top-right of the dialog
    $('.ui-dialog-titlebar').children('button').remove();

    if (validPart(partNumber, quantity)) {
        $("#divPrg").html('<span id="progress">' + atc_AddingItem + partNumber + atc_ToCart + '<br/> <img src="images/progbar.gif" alt ="Progress" /></span><br/>');
        ajaxPromises.push(CallWebMethod(partNumber, quantity));
    } else {
        exception = true;
        $("#divException").append(atc_ex_InvalidPartAndQuantity);
    }

    // Call the AJAX method, performing ".done" when they're finished.
    $.when.apply($, ajaxPromises).done(function() {
        $("#divPrg").html('');

        if (exception) {
            $("#dialog").dialog({
                title: atc_title_OrderExceptions, // 'Order Add to Cart Exceptions'
                top: 75,
                height: ($("#divMessage").height() + $("#divException").height() + $("#divCloseButton").height() + 80),
                width: 400,
                modal: true
            }).dialog("moveToTop");
            // Remove the default "Close" button in the top-right of the dialog
            $('.ui-dialog-titlebar').children('button').remove();
            $("#divException").show("fast");
        }
        $("#divCloseButton").show("fast");
        return false; //6994
    });
}

function QuickOrder_OnClick() {
    var isAnyValidParts = false;
    var partNumber;
    var quantity;
    var ajaxPromises = [];   // For handling the AJAX calls and ensuring we wait until they're done.
    resetModal();   // Clears the text areas and resets flags

    $("#dialog").dialog({ // dialog box
        title: atc_title_QuickOrderStatus,
        top: 75,
        height: 300,
        width: 400,
        modal: true
    });
    // Remove the default "Close" button in the top-right of the dialog
    $('.ui-dialog-titlebar').children('button').remove();
    $("#divPrg").html('<span id="progress">' + prg_PleaseWait + '<br/> <img src="images/progbar.gif" alt ="Progress" /><br/>' + atc_warn_LongWait + '</span><br/>');

    for (i = 1; i <= quickOrderMaxItems; i++) {
        partNumber = replaceAll(jQuery.trim($('#PartNumber' + i).val()), "-", "");
        quantity = jQuery.trim($('#Qty' + i).val());

        if (validPart(partNumber, quantity)) {
            isAnyValidParts = true;
            //$("#divPrg").append("<br/>" + atc_AddingItem + partNumber + atc_ToCart);
            ajaxPromises.push(CallWebMethod(partNumber, quantity));
        }
    }
    $("#divPrg").append("<br/>" + atc_AddingItem + "x" + ajaxPromises.length + atc_ToCart);

    if (isAnyValidParts == false) {
        exception = true;
        $("#divException").append(atc_ex_InvalidPartAndQuantity);
    }

    // Call all of the AJAX methods asynchronously as an array, performing ".done" when they're all finished.
    $.when.apply($, ajaxPromises).done(function() {
        $("#divPrg").html('');

        if (exception) {
            $("#dialog").dialog({
                title: atc_title_QuickOrderExceptions, // 'Quick Order Add to Cart Exceptions'
                top: 75,
                height: ($("#divMessage").height() + $("#divException").height() + $("#divCloseButton").height() + 80),
                width: 400,
                modal: true
            }).dialog("moveToTop");
            // Remove the default "Close" button in the top-right of the dialog
            $('.ui-dialog-titlebar').children('button').remove();
            //$("#divMessage").text('');
            $("#divException").show("fast");
        } else {
            //$("#dialog").dialog("close");
        }
        $("#divCloseButton").show("fast");
        return false;
    });
}

function ProcessOrderList() {
    var isAnyValidParts = false;
    var partNumber;
    var quantity;
    var ajaxPromises = [];   // For handling the AJAX calls and ensuring we wait until they're done.
    resetModal();   // Clears the text areas and resets flags

    $("#dialog").dialog({ // dialog box
        title: atc_title_QuickOrderStatus,
        top: 75,
        height: 300,
        width: 400,
        modal: true
    });
    // Remove the default "Close" button in the top-right of the dialog
    $('.ui-dialog-titlebar').children('button').remove();
    $("#divPrg").html('<span id="progress">' + prg_PleaseWait + '<br/> <img src="images/progbar.gif" alt ="Progress" /><br/>' + atc_warn_LongWait + '</span><br/>');

    for (i = 0; i < orderItems.length; i += 2) {
        partNumber = replaceAll(jQuery.trim(orderItems[i]), "-", "");
        quantity = jQuery.trim(orderItems[i + 1]);
        //$("#divMessage").append("Part/Quantity: " + partNumber + "/" + quantity + "<br/>");   // For Debugging only

        if (validPart(partNumber, quantity)) {
            isAnyValidParts = true;
            //$("#divPrg").append("<br/>" + atc_AddingItem + partNumber + atc_ToCart);
            ajaxPromises.push(CallWebMethod(partNumber, quantity));
        }
    }
    $("#divPrg").append("<br/>" + atc_AddingItem + "x" + ajaxPromises.length + atc_ToCart);

    if (isAnyValidParts == false) {
        exception = true;
        $("#divException").append(atc_ex_InvalidPartAndQuantity);
    }

    // Call all of the AJAX methods asynchronously as an array, performing ".done" when they're all finished.
    $.when.apply($, ajaxPromises).done(function() {
        $("#divPrg").html('');

        if (exception) {
            $("#dialog").dialog({
                title: atc_title_QuickOrderExceptions, // 'Quick Order Add to Cart Exceptions'
                top: 75,
                height: ($("#divMessage").height() + $("#divException").height() + $("#divCloseButton").height() + 80),
                width: 400,
                modal: true
            }).dialog("moveToTop");
            // Remove the default "Close" button in the top-right of the dialog
            $('.ui-dialog-titlebar').children('button').remove();
            //$("#divMessage").text('');
            $("#divException").show("fast");
        } else {
            //$("#dialog").dialog("close");
        }
        $("#divCloseButton").show("fast");
        return false;
    });
}

function CallWebMethod(partNumber, quantity) {
    var parametervalues = '{partNumber: "' + partNumber + '",partQuantity: "' + quantity + '" }';
    return jQuery.ajax({
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        data: parametervalues,
        dataType: 'json',
        url: window.location.pathname + '/AddToCartWithResults',
        success: function(result) {
            if (result.d.success == 1) {
                // The part was added as-is (no replacement)
                $("#divMessage").append("<b>" + result.d.message + "</b><br/>");
                $("#itemsInCart").text(result.d.items);
                $("#totalPriceOfCart").text(result.d.totalAmount);

            } else if (result.d.success == 2) {
                // The part has been replaced. Replacement was added instead.
                exception = true;
                $("#divMessage").append("<b>" + result.d.message + "</b><br/>");
                $("#itemsInCart").text(result.d.items);
                $("#totalPriceOfCart").text(result.d.totalAmount);
            } else {
                // An actual error occurred.
                exception = true;
                $("#divException").append("<b>" + result.d.message + "</b><br/>");
            }

            if (result.d.differentCart == true) {
                exception = true;
                $("#divException").append("<b>" + atc_ex_DifferentCart + " (" + result.d.PartNumber + ")</b><br/>"); // "Item added to a different cart"
            }
        }
    });
}