
if (document.images) {
    //rollovers
    myCatalog_btn = new Image(91, 27);
    myCatalog_btn_off = new Image(91, 27);
    myOrders_btn = new Image(78, 27);
    myOrders_btn_off = new Image(78, 27);
    myProfile_btn = new Image(78, 27);
    myProfile_btn_off = new Image(78, 27);
    logout_btn = new Image(43, 27);
    logout_btn_off = new Image(43, 27);
    viewCart_btn = new Image(81, 27);
    viewCart_btn_off = new Image(81, 27);
}

function imageSwap(daImage, daSrc) {
    var objStr, obj;

    if (document.images) {
        if (typeof daImage == 'string') {
            objStr = 'document.images.' + daImage;
            obj = eval(objStr);
            obj.src = daSrc;
        } else if ((typeof daImage == 'object') && daImage && daImage.src) {
            daImage.src = daSrc;
        }
    }
}

function launchWin(url, popup) {
    if (popup == 1) {
        newWin = open(url, "", "");
    } else {
        window.location.href = url;
    }
}

function launchWin1(url, newWidth, newHeight) {
    newWin = open(url, "", "toolbar=1,location=1,directories=1,status=1,menubar=1,scrollbars=1,resize=1,width=" + newWidth + ",height=" + newHeight);
}

function LinkSelect(form, sel) {
    c = sel.selectedIndex;
    adrs = sel.options[c].value;
    if (adrs != "-") {
        LinkWin = window.open("", "_top");
        LinkWin.location.href = adrs;
    }
}

function launchWin2(url, newWidth, newHeight) {
    newWin = window.open(url, "", "toolbar=0,location=0,directories=0,status=0,menubar=0,scrollbars=no,resize=0,width=" + newWidth + ",height=" + newHeight);
}

function launchWin3(url, id, newWidth, newHeight) {
    newWin = window.open(url, id, "toolbar=0,location=0,directories=0,status=0,menubar=0,scrollbars=no,resize=0,width=" + newWidth + ",height=" + newHeight);
}

function LinkSelect(form, sel) {
    c = sel.selectedIndex;
    adrs = sel.options[c].value;
    if (adrs != "-") {
        LinkWin = window.open("", "_top");
        LinkWin.location.href = adrs;
    }
}

function launchWin3(url, newWidth, newHeight) {
    newWin = open(url, "", "toolbar=1,location=1,directories=1,status=1,menubar=1,scrollbars=1,resize=1,width=" + newWidth + ",height=" + newHeight);
    if (newWin.opener == null) newWin.opener = self;
}

function launchWin4(url, newWidth, newHeight) {
    newWin = open(url, "", "toolbar=0,location=0,directories=0,status=0,menubar=0,scrollbars=0,resize=0,width=" + newWidth + ",height=" + newHeight);
    if (newWin.opener == null) newWin.opener = self;
}

function selectFreeDownloadSoftwareItemWithoutLoggedIn(url, newWidth, newHeight, urlParam) {
    newWin = open(url + "?" + urlParam, "", "toolbar=0,location=0,directories=0,status=0,menubar=0,scrollbars=0,resize=0,width=" + newWidth + ",height=" + newHeight);
    if (newWin.opener == null) newWin.opener = self;
}

function selectSoftwareItemWithoutLoggedIn(url, newWidth, newHeight, urlParam, itemN) {
    var theform;

    if (window.navigator.appName.toLowerCase().indexOf('netscape') > -1) {
        theform = document.forms['Form1'];
    } else {
        theform = document.Form1;
    }

    var num = theform.elements.length;
    var downloadElementName = "download" + itemN;
    var shipElementName = "ship" + itemN;
    var downloadRadioBtn = null;
    var shipRadioBtn = null;
    var downloadParam = "&Download=";
    var shipParam = "&Ship=";

    for (var i = 0; i < num; i++) {
        if (downloadRadioBtn != null && shipRadioBtn != null)
            break;
        if (theform.elements[i].id == downloadElementName)
            downloadRadioBtn = theform.elements[i];
        if (theform.elements[i].id == shipElementName)
            shipRadioBtn = theform.elements[i];
    }
    if (downloadRadioBtn != null)
        downloadParam += downloadRadioBtn.checked ? "Y" : "N";
    else
        downloadParam += "N";

    if (shipRadioBtn != null)
        shipParam += shipRadioBtn.checked ? "Y" : "N";
    else
        shipParam += "N";

    if (downloadRadioBtn != null && !downloadRadioBtn.checked && shipRadioBtn != null && !shipRadioBtn.checked) //post back with error message
    {
        var newURL = document.location.href + "?nodelivermethod=true"
        document.location.href = newURL
    } else {
        newWin = open(url + "?" + urlParam + downloadParam + shipParam, "", "toolbar=0,location=0,directories=0,status=0,menubar=0,scrollbars=0,resize=0,width=" + newWidth + ",height=" + newHeight);
        if (newWin.opener == null) newWin.opener = self;
    }
}

function selectPartWithoutLoggedIn(url, newWidth, newHeight, urlParam) {
    newWin = open(url + "?" + urlParam, "", "toolbar=0,location=0,directories=0,status=0,menubar=0,scrollbars=0,resize=0,width=" + newWidth + ",height=" + newHeight);
    if (newWin.opener == null) newWin.opener = self;
}

function selectCourseWithoutLoggedIn(url, newWidth, newHeight, itemN) {
    var theform;
    newWin = open(url + "?selectedCourse=" + itemN, "", "toolbar=0,location=0,directories=0,status=0,menubar=0,scrollbars=0,resize=0,width=" + newWidth + ",height=" + newHeight);
    if (newWin.opener == null) newWin.opener = self;
}

function parentRedirect(url, bClose) {
    opener.document.location = url;
    opener.focus()
    if (bClose == 1)
        close()
}

function SetTheFocusButton(e, thisControlsName) {
    var charCode = (navigator.appName == "Netscape") ? e.which : e.keyCode;
    if (charCode == 13) document.getElementById(thisControlsName.toString()).focus();
}


function closeWindow() {
    window.close
}
//New Functions for dropdown menu

function FIND(item) {
    if (window.mmIsOpera) return (document.getElementById(item));
    if (document.all) return (document.all[item]);
    if (document.getElementById) return (document.getElementById(item));
    return (false);
}

function getY(oElement) {
    var iReturnValue = 0;
    while (oElement != null) {
        iReturnValue += oElement.offsetTop;
        oElement = oElement.offsetParent;
    }
    return iReturnValue;
}

function getX(oElement) {
    var iReturnValue = 0;
    while (oElement != null) {
        iReturnValue += oElement.offsetLeft;
        oElement = oElement.offsetParent;
    }
    return iReturnValue;
}

function showMenu(daImage, id, daSrc) {
    var elem, referElem;

    imageSwap(daImage, daSrc)
    referElem = FIND('services');
    elem = FIND(id);

    elem.style.visibility = 'visible';
    elem.style.left = getX(referElem);
    elem.style.top = getY(referElem) + 29;
}

function showMenu2(daImage, id, daSrc) {
    var elem, referElem;

    imageSwap(daImage, daSrc)

    switch (id) {
        case "servicesMenuLoggedIn":
            referElem = FIND('services');
            break;
        case "servicesMenuMain":
            referElem = FIND('services');
            break;
        case "softwareMenu":
            referElem = FIND('software');
            break;
        case "partsMenu":
            referElem = FIND('parts');
            break;
        case "trainingMenu":
            referElem = FIND('training');
            break;
    }
    elem = FIND(id);
    elem.style.visibility = 'visible';
    elem.style.left = getX(referElem);
    elem.style.top = getY(referElem) + 29;
}

function hideMenu(daImage, id, daSrc) {
    imageSwap(daImage, daSrc)
    FIND(id).style.visibility = 'hidden';
}

function selectMarketingInterest(numberOfCheckboxs, checkboxName, namePrefix) {
    var SelectAllCB = FIND(checkboxName.toString());
    if (SelectAllCB != null) {
        var isSelectAllChecked = SelectAllCB.checked;;
        var thisCheckBox;
        for (var checkBoxNumber = 1; checkBoxNumber < numberOfCheckboxs; checkBoxNumber++) {
            thisCheckBox = FIND(namePrefix.toString() + checkBoxNumber.toString())
            if (thisCheckBox != null) { thisCheckBox.checked = isSelectAllChecked; }
        }
    }
}

// Below Functions added by Venkat for Tokenization
function ShowToken(vData) {
    //alert(window.document.getElementById('CCToken').value);
    window.document.forms["form1"].submit();
    //form1.submit();
}

function PostiFrame() {
    document.getElementById("DIeCommFrame").contentWindow.document.getElementById("PayNowButton").click();
    return false;
}

function CloseWin() {
    this.window.close();
    return false;
}

function popup() {
    var width = 370;
    var height = 240;
    var left = (screen.width - width) / 2;
    var top = (screen.height - height) / 2;
    var params = 'width=' + width + ', height=' + height;
    params += ', top=' + top + ', left=' + left;
    params += ', directories=no, location=no, menubar=no';
    params += ', resizable=no, scrollbars=no, status=no';
    params += ', toolbar=no';
    newwin = window.open('Shared/IFrames.aspx?iFrame=true', 'PopWin', params);
    if (window.focus) { newwin.focus() }

    return true;
} 