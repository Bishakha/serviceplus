<%@ Reference Page="~/YourPrice.aspx" %>
<%@ Reference Page="~/SSL.aspx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="SonyFooter" Src="SonyFooter.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="PersonalMessage" Src="PersonalMessage.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="ServicePlusNav" Src="ServicePlusNav.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="TopSonyHeader" Src="TopSonyHeader.ascx" %>

<%@ Page Language="vb" AutoEventWireup="false" Inherits="ServicePLUSWebApp.SoftwarePLUSModelSearch"
    EnableViewStateMac="true" CodeFile="sony-software-model.aspx.vb" CodeFileBaseClass="ServicePLUSWebApp.SSL" %>

<html lang="<%=PageLanguage %>">
<head>
    <title>
        <%=Resources.Resource.sftware_hdr_msg()%></title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <meta name="keywords" content="Sony Software, Sony Professional Software, Sony Broadcast Software, Sony Business Software" />
    <link href="includes/ServicesPLUS_style.css" type="text/css" rel="stylesheet" />
    <link href="themes/base/jquery.ui.all.css" rel="stylesheet" type="text/css" />
    <link href="includes/jquery.cluetip.css" rel="stylesheet" type="text/css" />
    
    <script src="includes/jquery-1.10.2.min.js" type="text/javascript"></script>
    <script src="includes/jquery.ui.core.js" type="text/javascript"></script>
    <script src="includes/jquery.ui.widget.js" type="text/javascript"></script>
    <script src="includes/jquery.cluetip.js" type="text/javascript"></script>
    <script src="includes/jquery.ui.mouse.js" type="text/javascript"></script>
    <script src="includes/jquery.ui.draggable.js" type="text/javascript"></script>
    <script src="includes/jquery.ui.position.js" type="text/javascript"></script>
    <script src="includes/jquery.ui.resizable.js" type="text/javascript"></script>
    <script src="includes/jquery.ui.dialog.js" type="text/javascript"></script>
    <script src="includes/jquery-ui-1.8.1.custom.min.js" type="text/javascript"></script>
    <script src="includes/ServicesPLUS.js" type="text/javascript"></script>
    <script src="includes/svcplus_globalization.js" type="text/javascript"></script>

    <script type="text/javascript">
        var exception = false;

        function closedilog() {
            $("#dialog").dialog("close");
            return true;
        }

        $(document).ready(function () {
            $("#messageclose").click(function () {
                $("#dialog").dialog("close");
            });
        });

        function replaceAll(txt, replace, with_this) { return txt.replace(new RegExp(replace, 'g'), with_this); }

        function validPart(partnumber, qty) {
            if (jQuery.trim(partnumber.val()) === "" && jQuery.trim(qty.val()) === "") {
                return false;
            } else {
                if (jQuery.trim(partnumber.val()) === "" || jQuery.trim(qty.val()) === "") {
                    exception = true;
                    if (jQuery.trim(partnumber.val()) === "") {
                        $("#divException").append("<strong>Please enter a valid part number.</strong><br/>");
                        $("#divException").append("<br/>");
                    } else {
                        $("#divException").append("<strong>Please enter a valid quantity." + partnumber.val() + "</strong><br/>");
                        $("#divException").append("<br/>");
                    }
                    return false;
                }
                if (jQuery.trim(partnumber.val()) !== "" && jQuery.trim(qty.val()) !== "") {
                    if (jQuery.trim(qty.val()).match("[^0-9]")) {
                        exception = true;
                        $("#divException").append("<strong> Item " + partnumber.val() + ": Please enter a valid order quantity. </strong><br/><br/>");
                        return false;
                    }
                    if (jQuery.trim(qty.val()) <= 0) {
                        exception = true;
                        $("#divException").append("<strong> Item " + partnumber.val() + ": Please enter a valid order quantity. </strong><br/><br/>");
                        return false;
                    }
                }
                return true;
            }
        }

        function AddToCart_onclick(val) {
            exception = false;

            $("#divMessage").text('');
            var buttons = $('.ui-dialog-buttonpane').children('button');
            buttons.remove();
            $("#divException").text('');
            $("#divException").hide();
            $("#divPrg").html('<span id="progress"> Please wait...<br/> <img src="images/progbar.gif" alt ="Progress" /></span>');
            $("#divMessage").append(' <br/>');
            var totalNull = "";
            var count = 0;
            $('#hidespstextitemno').val(val);
            $('#hidespstextqty').val(1);
            var sQty = '#hidespstextqty';
            var sPart = '#hidespstextitemno';
            var qty = $(sQty);
            var part = $(sPart);

            totalNull = totalNull + qty.val() + part.val();

            if (totalNull === "" || jQuery.trim(part.val()) === "" || jQuery.trim(qty.val()) === "") {
                $("#divMessage").text('');
                $("#dialog").dialog({
                    title: 'Order Add to Cart Exceptions'
                });
                $("#divMessage").append('<br/> <br/><a  ><img src ="images/sp_int_closeWindow_btn.gif" alt ="Close" onclick="javascript:return closedilog();"  id="messageclose" /></a>');
            }
            var itemsInCart = $("#itemsInCart");
            var totalPriceOfCart = $("#totalPriceOfCart");
            //var destination = itemsInCart.position();
            //var position = part.position();

            $("#dialog").dialog({
                //title: '<strong>Quick Order Add to Cart Status</strong>',
                title: '<strong>Order Add to Cart Status</strong>',
                height: 200,
                width: 560,
                modal: true,
                position: 'top',
                resizable: false
            });
            count = count + 1;
            if (validPart(part, qty)) {
                count = count + 1;
                $("#divMessage").html("<br/><strong>Adding item " + replaceAll(part.val(), "-", "") + " to cart... </strong><br/>");
                var parametervalues = '{sPartNumber: "' + jQuery.trim(replaceAll(part.val(), "-", "")) + '",sPartQuantity: "' + jQuery.trim(qty[0].value) + '" }';
                //Call webmethod using ajax
                jQuery.ajax({
                    type: 'POST',
                    contentType: 'application/json; charset=utf-8',
                    data: parametervalues,
                    dataType: 'json',
                    url: 'sony-software-model.aspx/addPartToCartwSSoftware',
                    success: function (result) {
                        count = count - 1;

                        if (result.d.differentCart === true) {
                            exception = true;
                            $("#divException").append("<br/><strong>The item has been added to a cart for items to be shipped; your previous cart with items for electronic delivery has been saved</strong><br/>");
                        }

                        if (result.d.success === 1) {
                            $("#divMessage").html("<br/><strong>Adding item " + replaceAll(result.d.partnumber, "-", "") + " to cart... </strong><br/>");
                            result.d.partnumber = '';
                            result.d.Quantity = '';
                            itemsInCart.text(result.d.items);
                            totalPriceOfCart.text(result.d.totalAmount);
                        } else if (result.d.success === 2) {
                            exception = true;
                            $("#divMessage").html("<br/><strong>Adding item " + replaceAll(result.d.partnumber, "-", "") + " to cart... </strong><br/>");
                            result.d.partnumber = '';
                            result.d.Quantity = '';
                            itemsInCart.text(result.d.items);
                            totalPriceOfCart.text(result.d.totalAmount);
                            $("#divException").append("<br/><strong>" + result.d.message + "</strong><br/>");
                        } else {
                            exception = true;
                            $("#divException").append("<br/><strong>" + result.d.message + "</strong><br/>");
                        }
                        //$("#divMessage").html("<br/><strong>" + result.d.message + "</strong><br/>");

                        if (count === 0) {
                            if (exception) {
                                var sHeight = $("#divException").height() + 200;

                                $("#progress").remove();
                                $("#dialog").dialog({
                                    //title: 'Quick Order Add to Cart Exceptions',
                                    title: 'Order Add to Cart Exceptions',
                                    height: sHeight.toString(),
                                    position: 'top'
                                }).dialog("moveToTop");
                                $("#divMessage").text('');
                                $("#divException").append('<br/> <br/><a ><img src ="images/sp_int_closeWindow_btn.gif" alt ="Close" onclick="javascript:return closedilog();"  id="messageclose" /></a>');
                                $("#divException").show("fast");
                            } else {
                                $("#dialog").dialog("close");
                            }
                        }
                    }
                });
            }
            count = count - 1;

            if (count === 0) {
                $("#progress").remove();
                if (exception) {
                    var sHeight = $("#divException").height() + 200;

                    $("#progress").remove();
                    $("#dialog").dialog({
                        //title: 'Quick Order Add to Cart Exceptions',
                        title: 'Order Add to Cart Exceptions',
                        height: sHeight.toString(),
                        position: 'top'
                    }).dialog("moveToTop");
                    $("#divMessage").text('');
                    $("#divException").append('<br/> <br/><a ><img src ="images/sp_int_closeWindow_btn.gif" alt ="Close" onclick="javascript:return closedilog();"  id="messageclose" /></a>');
                    $("#divException").show("fast");
                    return false; //6994
                }
            }
            return false; //6994
        }
    </script>

    <script type="text/javascript">
        function selectSoftwareItemWithoutLoggedIn(url, newWidth, newHeight, urlParam, itemN) {
            window.location.href = url + "?" + urlParam;
        }

        function selectSoftwareItemWithLoggedIn(itemN, partsearch, popuppartno) {
            var theform;

            if (window.navigator.appName.toLowerCase().indexOf('netscape') > -1) {
                theform = document.forms['Form1'];
            } else {
                theform = document.Form1;
            }

            var num = theform.elements.length;
            var downloadElementName = "download" + itemN;
            var shipElementName = "ship" + itemN;
            var downloadRadioBtn = null;
            var shipRadioBtn = null;
            var downloadParam = "&Download=";
            var shipParam = "&Ship=";

            for (var i = 0; i < num; i++) {
                if (downloadRadioBtn !== null && shipRadioBtn !== null)
                    break;
                if (theform.elements[i].id === downloadElementName)
                    downloadRadioBtn = theform.elements[i];
                if (theform.elements[i].id === shipElementName)
                    shipRadioBtn = theform.elements[i];
            }
            if (downloadRadioBtn !== null)
                downloadParam += downloadRadioBtn.checked ? "Y" : "N";
            else
                downloadParam += "N";

            if (shipRadioBtn !== null)
                shipParam += shipRadioBtn.checked ? "Y" : "N";
            else
                shipParam += "N";

            if (partsearch === 1) {
                if (downloadRadioBtn !== null && !downloadRadioBtn.checked && shipRadioBtn !== null && !shipRadioBtn.checked) { //post back with error message
                    //var newURL = document.location.href + "&nodelivermethod=true"
                    document.location.href = "sony-software-model.aspx?partsearch=1&delivermethod=false";
                } else {
                    //newWin = window.open(url+"?"+urlParam+downloadParam+shipParam,"","toolbar=0,location=0,directories=0,status=0,menubar=0,scrollbars=0,resize=1,width=" + newWidth + ",height=" + newHeight);
                    //if (newWin.opener === null) 
                    //	newWin.opener = self;
                    //var newURL = "sony-software-model.aspx?partsearch=1&addtocart=" + itemN + downloadParam + shipParam//6994
                    //document.location.href = newURL//6994
                    AddToCart_onclick(popuppartno);//6994
                }
            } else {
                if (downloadRadioBtn !== null && !downloadRadioBtn.checked && shipRadioBtn !== null && !shipRadioBtn.checked) { //post back with error message
                    //var newURL = document.location.href + "&nodelivermethod=true"
                    document.location.href = "sony-software-model.aspx?delivermethod=false";
                } else {
                    //newWin = window.open(url+"?"+urlParam+downloadParam+shipParam,"","toolbar=0,location=0,directories=0,status=0,menubar=0,scrollbars=0,resize=1,width=" + newWidth + ",height=" + newHeight);
                    //if (newWin.opener === null) 
                    //	newWin.opener = self;
                    //var newURL = "sony-software-model.aspx?addtocart=" + itemN + downloadParam + shipParam//6994
                    //document.location.href = newURL//6994
                    AddToCart_onclick(popuppartno); //6994
                }
            }
        }
    </script>

</head>
<body style="color: #000000; background: #5d7180; margin: 0px;">
    <div id="ValidationMessageDiv"></div>
    <div id="dialog" title="<strong>Order Add to Cart Status</strong>">
        <div id="divPrg" style="font: 11px; font-family: Arial; overflow: auto;"></div>
        <div id="divMessage" style="font: 11px; font-family: Arial; overflow: auto;"></div>
        <div id="divException" style="font-family: Arial, Helvetica, sans-serif; font-size: 11px; line-height: 14px; overflow: auto; display: none;">
            <img src="images/sp_int_closeWindow_btn.gif" alt="Close" onclick="javascript:return closedilog();"
                id="messageclose" />
        </div>
    </div>
    <form id="Form1" method="post" runat="server">
        <SPS:SPSTextBox ID="hidespstextitemno" runat="server" Style="display: none" />
        <SPS:SPSTextBox ID="hidespstextqty" runat="server" Style="display: none" />
        <center>
            <table width="760" border="0" role="presentation">
                <tr>
                    <td style="width: 25px; background: url(images/sp_left_bkgd.gif);">
                        &nbsp;
                    </td>
                    <td width="710" bgcolor="#ffffff">
                        <table width="710" border="0" role="presentation">
                            <tr>
                                <td>
                                    <ServicePLUSWebApp:TopSonyHeader ID="TopSonyHeader" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <nav><ServicePLUSWebApp:ServicePlusNav ID="SonyNav" runat="server" /></nav>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="710" border="0" role="presentation">
                                        <tr>
                                            <td style="width: 464px; height: 82px; background: #363d45 url(<%=imageURL%>); text-align: right; vertical-align: middle; padding-right: 10px;">
                                                <h1 class="headerText"><%=headerText%></h1>
                                            </td>
                                            <td style="background: #363d45; text-align: center; vertical-align: middle; padding: 10px;">
                                                <ServicePLUSWebApp:PersonalMessage ID="PersonalMessageDisplay" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 464px; background: #f2f5f8; padding: 5px;">
                                                <h2 id="lblSubHdr" class="headerTitle" runat="server" />
                                            </td>
                                            <td style="background: #99a8b5; vertical-align: bottom;">
                                                <asp:ImageButton ID="btnSearch" runat="server" AlternateText="<% $Resources:Resource,el_newSearch %>"
                                                    ImageUrl="<% $Resources:Resource,img_btnNewSearch %>" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="height: 9px; width: 464px; background: #f2f5f8 url(images/sp_int_header_btm_left_onepix.gif);">
                                                <img src="images/spacer.gif" alt="" />
                                            </td>
                                            <td style="height: 9px; width: 246px; background: #99a8b5 url(images/sp_int_header_btm_right.gif);">
                                                <img src="images/spacer.gif" alt="" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="errorMessageLabel" Width="641px" ForeColor="Red" CssClass="tableData"
                                        EnableViewState="False" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table style="width: 710px; margin: 0px; border: none; padding: 0px;" role="presentation">
                                        <tr style="padding-top: 5px; padding-left: 20px; vertical-align: top;">
                                            <td>
                                                <%-- Top Paging Navigation --%>
                                                <table id="tblPagerTop" style="border: none; margin: 0px; padding: 0px 5px;" role="presentation" runat="server">
                                                    <tr>
                                                        <td>
                                                            <asp:ImageButton ID="btnPreviousPage" ImageUrl="images/sp_int_leftArrow_btn.gif"
                                                                Height="9" Width="9" ToolTip="Previous Page" runat="server" /><asp:LinkButton ID="PreviousLinkButton"
                                                                    CssClass="tableData" ToolTip="Previous Page" runat="server">
                                                                <%=Resources.Resource.el_Previous%></asp:LinkButton>
                                                        </td>
                                                        <td class="tableData" align="center">
                                                            <%=Resources.Resource.el_Page%>&nbsp;<asp:Label ID="lblCurrentPage" Text="1" runat="server" />
                                                            &nbsp;<%=Resources.Resource.el_of%>&nbsp;
                                                        <asp:Label ID="lblEndingPage" Text="1" runat="server" />
                                                        </td>
                                                        <td>
                                                            <asp:LinkButton ID="NextLinkButton" CssClass="tableData" ToolTip="Next Page" runat="server">
                                                        <%=Resources.Resource.el_Next%></asp:LinkButton><asp:ImageButton ID="btnNextPage"
                                                            runat="server" ImageUrl="images/sp_int_rightArrow_btn.gif" Height="9" Width="9"
                                                            ToolTip="Next Page" />
                                                        </td>
                                                        <td class="tableData" style="text-align: right; padding-left: 25px;">
                                                            <%=Resources.Resource.el_Page%>
                                                        </td>
                                                        <td>
                                                            <SPS:SPSTextBox ID="txtPageNumber" Width="40px" MaxLength="5" ToolTip="Page Number"
                                                                runat="server" />
                                                        </td>
                                                        <td>
                                                            <asp:ImageButton ID="btnGoToPage" ImageUrl="<%$ Resources:Resource,img_btnGoTo%>"
                                                                AlternateText="Go to specified result page" ToolTip="Goto Page" runat="server" />
                                                        </td>
                                                    </tr>
                                                </table>
                                                <%-- Software Listing Table --%>
                                                <table width="670" border="0" role="presentation">
                                                    <tr style="height: 26px; background: #d5dee9">
                                                        <td class="tableHeader" style="width: 82px; text-align: center;">
                                                            <asp:LinkButton ID="lnkVersion" runat="server" CssClass="tableHeader" CommandArgument="VersionASC"
                                                                ToolTip="Sort By Version">Version</asp:LinkButton>
                                                            <img id="imgVersion" runat="server" src="images/arrow_down.gif" alt="">
                                                        </td>
                                                        <td class="tableHeader" style="width: 82px; text-align: center;">
                                                            <asp:LinkButton ID="lnkKitPart1" runat="server" CssClass="tableHeader" CommandArgument="KitPartNumberASC"
                                                                ToolTip="Sort By Kit Part Number"><%=Resources.Resource.el_PartNumber%></asp:LinkButton>
                                                            <img id="imgKitPartSort" runat="server" src="images/arrow_up.gif" visible="false"
                                                                alt="">
                                                        </td>
                                                        <td class="tableHeader" style="padding-left: 5px;">
                                                            <%=Resources.Resource.el_Description%>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3" width="649">
                                                            <asp:Table ID="SoftwareSearchResultTable" Width="650" CellPadding="0"
                                                                runat="server">
                                                            </asp:Table>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <%-- Bottom Page Navigation --%>
                                                <table id="tblPagerBottom" style="border: none; margin: 0px; padding: 0px 5px;" role="presentation" runat="server">
                                                    <tr>
                                                        <td>
                                                            <asp:ImageButton ID="btnPreviousPage2" ImageUrl="images/sp_int_leftArrow_btn.gif"
                                                                Height="9" Width="9" ToolTip="Previous Page" runat="server" /><asp:LinkButton ID="PreviousLinkButton2"
                                                                    CssClass="tableData" ToolTip="Previous Page" runat="server"><%=Resources.Resource.el_Previous%></asp:LinkButton>
                                                        </td>
                                                        <td class="tableData" align="center">
                                                            <%=Resources.Resource.el_Page%>&nbsp;<asp:Label ID="lblCurrentPage2" Text="1" runat="server" />
                                                            &nbsp;<%=Resources.Resource.el_of%>&nbsp;
                                                        <asp:Label ID="lblEndingPage2" Text="1" runat="server" />
                                                        </td>
                                                        <td>
                                                            <asp:LinkButton ID="NextLinkButton2" CssClass="tableData" ToolTip="Next Page" runat="server">
                                                        <%=Resources.Resource.el_Next%></asp:LinkButton><asp:ImageButton ID="btnNextPage2"
                                                            runat="server" ImageUrl="images/sp_int_rightArrow_btn.gif" Height="9" Width="9"
                                                            ToolTip="Next Page" />
                                                        </td>
                                                        <td class="tableData" style="text-align: right; padding-left: 25px;">
                                                            <%=Resources.Resource.el_Page%>
                                                        </td>
                                                        <td>
                                                            <SPS:SPSTextBox ID="txtPageNumber2" Width="40px" MaxLength="5" ToolTip="Page Number"
                                                                runat="server" />
                                                        </td>
                                                        <td>
                                                            <asp:ImageButton ID="btnGoToPage2" ImageUrl="<%$Resources:Resource,img_btnGoTo%>"
                                                                AlternateText="Goto specified result page" ToolTip="Goto Page" runat="server" />
                                                        </td>
                                                    </tr>
                                                </table>
                                                <%-- Go To Top --%>
                                                <div>
                                                    <a class="tableHeaderLink" href="#top">
                                                        <img src="<%=Resources.Resource.repair_sny_type_3_img%>" alt="Back to Top" /></a>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <footer><ServicePLUSWebApp:SonyFooter ID="SonyFooter1" runat="server" /></footer>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td style="width: 25px; background: url(images/sp_right_bkgd.gif);">
                        &nbsp;
                    </td>
                </tr>
            </table>
        </center>
    </form>
</body>
</html>
