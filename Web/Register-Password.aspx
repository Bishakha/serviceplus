<%@ Reference Page="~/SSL.aspx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="TopSonyHeader" Src="TopSonyHeader.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="SonyFooter" Src="SonyFooter.ascx" %>

<%@ Page Language="vb" AutoEventWireup="false" SmartNavigation="true" Inherits="ServicePLUSWebApp.Register_Password"
    EnableViewStateMac="true" CodeFile="Register-Password.aspx.vb" CodeFileBaseClass="ServicePLUSWebApp.SSL" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="<%=PageLanguage %>">
<head>
    <title><%=Resources.Resource.ttl_Register%>  </title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link href="includes/ServicesPLUS_style.css" type="text/css" rel="stylesheet">

    <script language="javascript" src="includes/ServicesPLUS.js" type="text/javascript"></script>
    <script type="text/javascript" src="includes/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="includes/svcplus_globalization.js"></script>

    <style type="text/css">
        .style1 {
        }

        .style6 {
            width: 150px;
        }
    </style>

    <script type="text/javascript">
        function showStuff(id) {
            document.getElementById(id).style.display = 'block';
        }
        function hideStuff(id) {
            document.getElementById(id).style.display = 'none';
        }
    </script>

</head>
<body style="color: #000000; background: #5d7180; margin: 0px;" onload="hideStuff('progress')">
    <form id="form1" autocomplete="off" method="post" runat="server">
        <center>
            <table width="760" border="0" role="presentation">
                <tr>
                    <td width="25" style="background: url('images/sp_left_bkgd.gif');">
                        <img src="images/spacer.gif" width="25" height="25" alt="">
                    </td>
                    <td width="710" bgcolor="#ffffff">
                        <table width="710" border="0" role="presentation">
                            <tr>
                                <td>
                                    <ServicePLUSWebApp:TopSonyHeader ID="TopSonyHeader" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <%--<img src="images/sp_int_register_hdr.gif" border="0">--%>
                                    <img src="<%=Resources.Resource.sna_svc_img_49()%>" border="0" alt="Register">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="710" border="0" role="presentation">
                                        <tr>
                                            <td width="17">
                                                <img height="20" src="images/spacer.gif" width="20" alt="">
                                            </td>
                                            <td width="678">
                                                <table width="670" border="0" role="presentation">
                                                    <tbody>
                                                        <tr bgcolor="#ffffff">
                                                            <td width="3">
                                                                <img height="1" src="images/spacer.gif" width="3" alt="">
                                                            </td>
                                                            <td width="127">
                                                                <img height="1" src="images/spacer.gif" width="125" alt="">
                                                            </td>
                                                            <td width="542">
                                                                <img height="1" src="images/spacer.gif" width="542" alt="">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <img height="7" src="images/spacer.gif" width="3" alt="">
                                                            </td>
                                                            <td width="127">
                                                                <img height="7" src="images/spacer.gif" width="3" alt="">
                                                            </td>
                                                            <td>
                                                                <img height="7" src="images/spacer.gif" width="3" alt="">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <img height="7" src="images/spacer.gif" width="3" alt="">
                                                            </td>
                                                            <%--<td width="127"><img height="7" src="images/spacer.gif" width="3"></td>--%>
                                                            <td colspan="2">
                                                                <asp:Label ID="ErrorValidation" runat="server" CssClass="redAsterick"></asp:Label><asp:Label
                                                                    ID="ErrorLabel" runat="server" CssClass="redAsterick"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr bgcolor="#d5dee9">
                                                            <td>
                                                                <img height="15" src="images/spacer.gif" width="3" alt="">
                                                            </td>
                                                            <td class="tableHeader" colspan="2">
                                                                <%=Resources.Resource.el_rgn_psdSubHdl1%>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <img height="30" src="images/spacer.gif" width="3" alt="">
                                                            </td>
                                                            <td class="bodyCopy" colspan="2">
                                                                <img height="7" src="images/spacer.gif" width="4" alt=""><br />
                                                                <%=Resources.Resource.el_rgn_psdSubHdl2%>
                                                                <ul>
                                                                    <li><span class="bodyCopy"><%=Resources.Resource.el_rgn_psdHint1%> </span>
                                                                        <asp:Label ID="lblemail" runat="server" Text="Label" Font-Bold="True"></asp:Label><br />
                                                                    </li>
                                                                    <li><span class="bodyCopy"><%=Resources.Resource.el_rgn_psdHint2%> </span>
                                                                        <br />
                                                                    </li>
                                                                    <li><span class="bodyCopy"><%=Resources.Resource.el_rgn_psdHint3%> </span>
                                                                        <br />
                                                                    </li>
                                                                    <li><span class="bodyCopy"><%=Resources.Resource.el_rgn_psdHint4%> </span>
                                                                        <br />
                                                                    </li>
                                                                </ul>
                                                                <%--<br />--%>
                                                                <strong>
                                                                    <asp:Label ID="ErrorPassword" runat="server" CssClass="redAsterick"></asp:Label><br />
                                                                    <asp:Label ID="ErrorPasswordNew" runat="server" CssClass="redAsterick"></asp:Label>
                                                                    <br />
                                                                    <img height="7" src="images/spacer.gif" width="4" alt="">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <img height="30" src="images/spacer.gif" width="3" alt="">
                                                            </td>
                                                            <td colspan="2">
                                                                <table width="660" border="0" role="presentation">
                                                                    <tr>
                                                                        <td align="left" class="bodyCopy" colspan="2">
                                                                            <strong><%=Resources.Resource.el_rgn_psdHint5%>  </strong>
                                                                        </td>
                                                                        <td>
                                                                            &nbsp;
                                                                        </td>
                                                                        <td>
                                                                            &nbsp;
                                                                        </td>
                                                                        <td>
                                                                            &nbsp;
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="left" class="bodyCopy" colspan="5">
                                                                            <table role="presentation">
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:Label ID="LabelPWD" runat="server" CssClass="bodyCopy" Text="<%$ Resources:Resource, el_rgn_Password%>" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <SPS:SPSTextBox ID="Password" runat="server" CssClass="bodyCopy" TextMode="Password" />
                                                                                        <span class="redAsterick">*</span>
                                                                                    </td>
                                                                                    <td class="style6">
                                                                                        &nbsp;
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:Label ID="LabelCPWD" runat="server" CssClass="bodyCopy" Text="<%$ Resources:Resource, el_rgn_ConfirmPassword%>"> </asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <SPS:SPSTextBox ID="cPassword" runat="server" CssClass="bodyCopy" TextMode="Password" />
                                                                                        <span class="redAsterick">*</span>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="right" class="style1">
                                                                            &nbsp;
                                                                        </td>
                                                                        <td>
                                                                            &nbsp;
                                                                        </td>
                                                                        <td align="right">
                                                                            &nbsp;
                                                                        </td>
                                                                        <td>
                                                                            &nbsp;
                                                                        </td>
                                                                        <td width="2">
                                                                            &nbsp;
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="left" class="bodyCopy" colspan="3">
                                                                            <strong><%=Resources.Resource.el_rgn_pswdNote1%>    </strong>
                                                                            <asp:Label ID="lblemail1" runat="server" Text="Label" Font-Bold="True"></asp:Label>
                                                                            <strong><%=Resources.Resource.el_rgn_pswdNote2%>   </strong>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="left" class="bodyCopy" colspan="3">
                                                                            &nbsp;
                                                                        </td>
                                                                        <td align="right">
                                                                            &nbsp;
                                                                        </td>
                                                                        <td>
                                                                            &nbsp;
                                                                        </td>
                                                                        <td width="2">
                                                                            &nbsp;
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="center" class="bodyCopy" colspan="5">
                                                                            <strong>
                                                                                <asp:ImageButton ID="NextButton" runat="server" OnClientClick="showStuff('progress')"
                                                                                    ImageUrl="<%$Resources:Resource,img_btnSubmit%>" AlternateText="Next"></asp:ImageButton>
                                                                        </td>
                                                                        <td>
                                                                        </td>
                                                                        <td width="2">
                                                                            &nbsp;
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="center" class="bodyCopy" colspan="4">
                                                                            <strong><span id="progress">Please wait...<br />
                                                                                <img src="images/progbar.gif" alt="Progres" /></strong>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td height="20" class="style1">
                                                                            &nbsp;
                                                                        </td>
                                                                        <td align="right">
                                                                            &nbsp;
                                                                        </td>
                                                                        <td>
                                                                            &nbsp;
                                                                        </td>
                                                                        <td valign="middle" align="center">
                                                                            &nbsp;
                                                                        </td>
                                                                        <td>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                </table>
                                            </td>
                                            <td width="20">
                                                <img height="20" src="images/spacer.gif" width="20" alt="">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <footer><ServicePLUSWebApp:SonyFooter ID="SonyFooter1" runat="server" /></footer>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="25" style="background: url('images/sp_right_bkgd.gif')">
                        <img src="images/spacer.gif" width="25" height="20" alt="">
                    </td>
                </tr>
            </table>
        </center>
    </form>

</body>
</html>
