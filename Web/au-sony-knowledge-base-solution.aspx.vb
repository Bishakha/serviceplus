Imports System.Data
Imports ServicesPlusException
Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Process

Namespace ServicePLUSWebApp
    Partial Class au_sony_knowledge_base_solution
        Inherits SSL
        Private recTechBulletin As TechBulletin = New TechBulletin()
        'Dim objUtilties As Utilities = New Utilities

#Region " Web Form Designer Generated Code "

        Public pagetitle As String = String.Empty
        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            isProtectedPage(False)
            InitializeComponent()
        End Sub

#End Region

#Region "Events"
        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                Dim isValidSolutionId As Boolean = False   ' ASleight - By assuming a value of False, we don't need 5 "Else" statements repeating the same code.
                Dim solutionID As Integer = 0

                If HttpContextManager.Customer Is Nothing Then Response.Redirect("sony-knowledge-base-solution.aspx?filenum=" + Request.QueryString("filenum"))

                If Not String.IsNullOrEmpty(Request.QueryString("filenum")) Then
                    If Integer.TryParse(Request.QueryString("filenum"), solutionID) Then
                        'If solutionID <> 0 Then
                        Dim techManager As TechBulletinManager = New TechBulletinManager
                        Dim strSolutionId = solutionID.ToString()
                        Dim dsSolution As DataSet = techManager.GetKnowledgeBaseSolution(strSolutionId)

                        lblSolutionHeader.Text = "Knowledge Base ID " + strSolutionId

                        If (dsSolution IsNot Nothing) AndAlso (dsSolution.Tables.Count > 0) AndAlso (dsSolution.Tables(0).Rows.Count > 0) Then
                            isValidSolutionId = True
                            solutiontable.Visible = True
                            lblDatePublishedValue.Text = CType(dsSolution.Tables(0).Rows(0)("DATEPUBLISHED"), Date).ToString("MM/dd/yyyy")
                            lblModelValuse.Text = dsSolution.Tables(0).Rows(0)("MODEL").ToString()
                            lblProblemQuestionValue.Text = dsSolution.Tables(0).Rows(0)("SUBJECTPROBLEM").ToString()
                            lblSolutionAnswerValue.Text = dsSolution.Tables(0).Rows(0)("SOLUTION").ToString()
                            pagetitle = lblProblemQuestionValue.Text
                            ViewState.Add("BulletinNo", strSolutionId)
                            ViewState.Add("Subject", pagetitle)
                        End If
                    End If
                End If
                If isValidSolutionId Then
                    If Not IsPostBack Then
                        SaveKBView()
                    End If
                Else   ' ASleight - The below code was repeated 5 times in Else statements. Makes more sense like this.
                    solutiontable.Visible = False
                    pagetitle = "Knowledge Base"
                    errorMessageLabel.Text = "No solution for this search."
                End If
            Catch ex As Exception

            End Try
        End Sub

        Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
            ImgNo.Attributes.Add("onclick", "javascript:document.getElementById('feedbackTr').style.visibility='hidden';")
            imgYes.Attributes.Add("onclick", "javascript:document.getElementById('feedbackTr').style.visibility='hidden';")
        End Sub

        Protected Sub imgYes_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgYes.Click
            Try
                LogTransaction(1, 1, "")
            Catch ex As Exception
                Utilities.WrapExceptionforUI(ex)
                Response.Redirect("Member.aspx")
            End Try
            FeedBackResponse()
        End Sub

        Protected Sub ImgNo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgNo.Click
            Try
                LogTransaction(1, 0, "")
            Catch ex As Exception
                Utilities.WrapExceptionforUI(ex)
                Response.Redirect("Member.aspx")
            End Try
            FeedBackResponse()
        End Sub

        Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSubmit.Click
            Try
                If Not String.IsNullOrEmpty(txtSuggestion.Text) Then
                    LogTransaction(2, 0, txtSuggestion.Text)
                    SuggestionResponse()
                Else
                    'Dim lblFBresponse As New Label()
                    'lblFBresponse.CssClass = "redAsterick"
                    'lblFBresponse.Text = "Please enter your suggestions to submit."
                    lblFeedbackMessage.CssClass = "redAsterick"
                    lblFeedbackMessage.Text = "Please enter your suggestions to submit."
                    btnSubmit.Visible = True
                    txtSuggestion.Visible = True
                    'feedbackPlaceHolder.Controls.Clear()
                    'feedbackPlaceHolder.Controls.Add(lblFBresponse)
                End If
            Catch ex As Exception
                Utilities.WrapExceptionforUI(ex)
                Response.Redirect("Member.aspx")
            End Try
        End Sub
#End Region

        Private Sub SaveKBView()
            Try
                LogTransaction(0, -1, "")
            Catch ex As Exception
                Utilities.WrapExceptionforUI(ex)
                Response.Redirect("Member.aspx")
            End Try
        End Sub

        Private Sub LogTransaction(ByVal TransactionType As Int16, ByVal FeedBackAnswer As Int16, ByVal Suggestion As String)
            Dim xCUSTOMERID As Integer = -1
            Dim xCUSTOMERSEQUENCENUMBER As Integer = -1
            If HttpContextManager.Customer IsNot Nothing Then
                xCUSTOMERID = Convert.ToInt32(HttpContextManager.Customer.CustomerID)
                xCUSTOMERSEQUENCENUMBER = Convert.ToInt32(HttpContextManager.Customer.SequenceNumber)
            End If
            Dim xHTTP_X_FORWARDED_FOR As String = HttpContextManager.GetServerVariableValue("HTTP_X_FORWARDED_FOR")
            Dim xREMOTE_ADDR As String = HttpContextManager.GetServerVariableValue("REMOTE_ADDR")
            Dim xHTTP_REFERER As String = HttpContextManager.GetServerVariableValue("HTTP_REFERER")
            Dim xHTTP_URL As String = HttpContextManager.GetServerVariableValue("HTTP_URL")

            If xHTTP_URL = "" Then
                xHTTP_URL = HttpContextManager.GetServerVariableValue("URL") + ReturnQueryString()
            End If

            Dim xHTTP_USER_AGENT As String = HttpContextManager.GetServerVariableValue("HTTP_USER_AGENT")

            Try
                Dim intFeedBackID As Int64 = 0
                If ViewState("FeedBackID") IsNot Nothing Then
                    intFeedBackID = Convert.ToInt64(ViewState("FeedBackID").ToString())
                End If
                recTechBulletin.Type = 1
                recTechBulletin.BulletinNo = ViewState("BulletinNo").ToString()
                recTechBulletin.Subject = ViewState("Subject").ToString()
                If Not recTechBulletin Is Nothing Then
                    Dim tbManager As TechBulletinManager = New TechBulletinManager()
                    intFeedBackID = (tbManager.SaveFeedBack(recTechBulletin, FeedBackAnswer, intFeedBackID, TransactionType, Suggestion, xCUSTOMERID, xCUSTOMERSEQUENCENUMBER, xHTTP_X_FORWARDED_FOR, xREMOTE_ADDR, xHTTP_REFERER, xHTTP_URL, xHTTP_USER_AGENT))
                    ViewState.Add("FeedBackID", intFeedBackID)
                End If
            Catch ex As Exception
                Utilities.WrapExceptionforUI(ex)
                Response.Redirect("Member.aspx")
            End Try
        End Sub

        Private Sub FeedBackResponse()
            'Dim lblFBresponse As New Label()
            feedbackTr.Visible = False
            'lblFBresponse.CssClass = "TableHeader"
            'lblFBresponse.Text = "Thank you for your feedback. Do you have any suggestions for improving the above information?"
            lblFeedbackMessage.CssClass = "TableHeader"
            lblFeedbackMessage.Text = "Thank you for your feedback. Do you have any suggestions for improving the above information?"
            btnSubmit.Visible = True
            txtSuggestion.Visible = True
            'txtSuggestion.Focus()   ' ASleight - This is causing AJAX errors in newer browsers. Code tries to Focus before the control is available inside an UpdatePanel.
            'feedbackPlaceHolder.Controls.Add(lblFBresponse)
        End Sub

        Private Sub SuggestionResponse()
            'Dim lblFBresponse As New Label()
            feedbackTr.Visible = False
            'lblFBresponse.CssClass = "TableHeader"
            'lblFBresponse.Text = "Thank you for your suggestions."
            lblFeedbackMessage.CssClass = "TableHeader"
            lblFeedbackMessage.Text = "Thank you for your suggestions."
            btnSubmit.Visible = False
            txtSuggestion.Visible = False
            'feedbackPlaceHolder.Controls.Clear()
            'feedbackPlaceHolder.Controls.Add(lblFBresponse)
        End Sub
    End Class

End Namespace
