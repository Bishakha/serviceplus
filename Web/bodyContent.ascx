<%@ Control Language="vb" AutoEventWireup="false" Inherits="ServicePLUSWebApp.bodyContent" CodeFile="bodyContent.ascx.vb" %>

<table width="710" border="0">
    <tr>
        <td width="20">
            <img height="20" src="images/spacer.gif" width="20" alt=""></td>
        <td>
            <table border="0">
                <tr>
                    <td style="width: 430px">
                        <table border="0">
                            <tr>
                                <td>
                                    <img style="width: 70px; height: 50px" height="50" src="images/sp_int_home_promo_training.jpg"
                                        width="70">
                                </td>
                                <td width="10">
                                    <img src="images/spacer.gif" width="10" alt=""></td>
                                <td class="bodyCopySM">
                                    <a class="promoCopyBold" href="./sony-parts.aspx">PartsPLUS</a>
                                    provides access to hundreds of exploded view diagrams for Sony Professional 
											Products and their full complement of Sony Parts.
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 70px" height="20">
                                    <img height="20" src="images/spacer.gif" alt=""></td>
                            </tr>
                            <tr>
                                <td>
                                    <img style="width: 70px; height: 50px" height="50" src="images/sp_int_home_promo_training.jpg"
                                        width="70">
                                </td>
                                <td width="10">
                                    <img src="images/spacer.gif" width="10" alt=""></td>
                                <td class="bodyCopySM">
                                    <a class="promoCopyBold" href="./sony-parts.aspx">PartsPLUS</a>
                                    provides access to hundreds of exploded view diagrams for Sony Professional 
											Products and their full complement of Sony Parts.
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 70px" height="20">
                                    <img height="20" src="images/spacer.gif" alt=""></td>
                            </tr>
                            <tr>
                                <td>
                                    <img style="width: 70px; height: 50px" height="50" src="images/sp_int_home_promo_training.jpg"
                                        width="70" alt="Training Promotion">
                                </td>
                                <td width="10">
                                    <img src="images/spacer.gif" width="10" alt=""></td>
                                <td class="bodyCopySM">
                                    <a class="promoCopyBold" href="./sony-software.aspx">SoftwarePLUS</a> is your 
											warehouse for software upgrades for Sony Professional Products.
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 70px" height="20">
                                    <img height="20" src="images/spacer.gif" alt=""></td>
                            </tr>
                            <tr>
                                <td>
                                    <img style="width: 70px; height: 50px" height="50" src="images/sp_int_home_promo_training.jpg"
                                        width="70" alt="">
                                </td>
                                <td width="10">
                                    <img src="images/spacer.gif" width="10" alt=""></td>
                                <td class="bodyCopySM">
                                    <a class="promoCopyBold" href="./sony-training-search.aspx">Sony Training Institute</a>
                                    offers Production, Post-Production and Maintenance Courses with an emphasis on 
											real-life, need-to-know knowledge for video and audio professionals. Classes 
											feature a balance of presentation and hands-on experience.
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="20">
                        <img height="20" src="images/spacer.gif" width="20" alt=""></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td width="20">
            <img height="20" src="images/spacer.gif" width="20" alt=""></td>
        <td style="width: 479px" height="20"></td>
    </tr>
    <tr>
        <td width="20">
            <img height="20" src="images/spacer.gif" width="20" alt=""></td>
    </tr>
</table>
