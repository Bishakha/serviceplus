Imports System
Imports System.IO
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Drawing
Imports System.Web
Imports System.Web.SessionState
Imports System.Web.UI
Imports System.Web.UI.Page
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports System.Text
Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Process
Imports Sony.US.ServicesPLUS.Data
Imports Sony.US.siam
Imports Sony.US.SIAMUtilities
Imports ServicesPlusException


Namespace ServicePLUSWebApp


    Partial Class sony_part_catalog
        Inherits SSL
        Const conPartsCatalogGroupPath As String = "Sony_Parts_CatalogGroup.aspx?pageno="
        'Dim objUtilties As Utilities = New Utilities
        'Dim objGlobalData As New GlobalData

        Protected Sub GridPartsGroup_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridPartsGroup.RowDataBound
            Try
                If e.Row.RowType = DataControlRowType.DataRow Then
                    Dim rec As PartsCatalogGroup = e.Row.DataItem
                    Dim lnkStartRange As HyperLink = CType(e.Row.Cells(1).Controls(0), HyperLink)
                    'Dim lnkEndRange As HyperLink = CType(e.Row.Cells(2).Controls(0), HyperLink)
                    lnkStartRange.Style.Add("cursor", "hand")
                    'lnkEndRange.Style.Add("cursor", "hand")
                    lnkStartRange.Style.Add("text-decoration", "underline")
                    'lnkEndRange.Style.Add("text-decoration", "underline")

                    If Not String.IsNullOrWhiteSpace(rec.GroupID) Then
                        '-->lnkStartRange.NavigateUrl = conPartsCatalogGroupPath + rec.GroupID.ToString()
                        'lnkStartRange.NavigateUrl = "Sony_Parts_CatalogGroup/page" + rec.GroupID.ToString() + ".aspx"
                        lnkStartRange.NavigateUrl = "page" + rec.GroupID + ".aspx"
                        'lnkEndRange.NavigateUrl = conPartsCatalogGroupPath + rec.GroupID.ToString()
                    End If

                    'If Not String.IsNullOrWhiteSpace(rec.RangeEnd) Then lnkEndRange.Text = rec.RangeEnd
                    If Not String.IsNullOrWhiteSpace(rec.RangeStart) Then lnkStartRange.Text = rec.RangeStart
                End If
            Catch ex As Exception
                SearchErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Dim partsGroups As PartsCatalogGroup()
            Dim cm As New CatalogManager

            Try
                partsGroups = cm.GetPrePartsRange()
                GridPartsGroup.DataSource = partsGroups
                GridPartsGroup.DataBind()
            Catch ex As Exception
                SearchErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
            Dim catm As New CatalogManager
            Dim parts() As Sony.US.ServicesPLUS.Core.Part
            Dim kits() As Sony.US.ServicesPLUS.Core.Kit
            Dim emptyString As String = ""
            Dim sMaximumvalue As String = String.Empty

            Session.Remove("kits")  ' Clear any old Search results
            Session.Remove("partList")
            SearchErrorLabel.Text = ""

            Try
                If String.IsNullOrWhiteSpace(PartNumber.Text) Then
                    SearchErrorLabel.Text = "Please enter Part Number to search"
                    Return
                End If

                '-- reusing the same quick search of parts page, so have to pass empty string on the model and description parameters
                parts = catm.QuickSearch(emptyString, PartNumber.Text, emptyString, sMaximumvalue, HttpContextManager.GlobalData)
                kits = catm.KitSearch(emptyString, PartNumber.Text, emptyString) 'also search for kit now

                If parts.Length = 0 And Not String.IsNullOrEmpty(sMaximumvalue) Then
                    SearchErrorLabel.Text = sMaximumvalue
                    Return
                ElseIf parts.Length = 0 And kits.Length = 0 Then
                    SearchErrorLabel.Text = "No matches found."
                    Return
                Else
                    Session.Add("partList", parts)
                    Session.Add("kits", kits)
                    Response.Redirect("PartsPlusResults.aspx?stype=parts", False)
                    HttpContext.Current.ApplicationInstance.CompleteRequest()
                End If
            Catch thrEx As Threading.ThreadAbortException
                'do nothing, expected for response.redirect
            Catch ex As Exception
                SearchErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try

        End Sub
    End Class
End Namespace
