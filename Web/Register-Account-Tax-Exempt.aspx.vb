﻿Imports ServicesPlusException
Imports Sony.US.ServicesPLUS.Controls
Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Process


Namespace ServicePLUSWebApp

    Partial Class Register_Account_Tax_Exempt
        Inherits SSL

        Dim customer As Customer = Nothing
        Public focusFormField As String
        Protected WithEvents TaxExempt As RadioButtonList
        Protected WithEvents isTaxExempt As CheckBox
        Protected WithEvents chkBoxTaxExempt As CheckBox
        Protected WithEvents AddMore As Button

#Region " Web Form Designer Generated Code "
        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As EventArgs) Handles MyBase.Init
            InitializeComponent()
        End Sub
#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As EventArgs) Handles MyBase.Load, Me.Load
            Try
                'Dim blVisible As Boolean = True
                Dim rbTax As RadioButtonList
                Dim isCanada As Boolean = HttpContextManager.GlobalData.IsCanada

                If HttpContextManager.Customer IsNot Nothing Then
                    customer = HttpContextManager.Customer
                    LabelSiamID.Text = GenerateSiamIDImageString(customer.SIAMIdentity)
                End If

                If Not Page.IsPostBack Then Session.Remove("newCustomer") ' Fix for bug if the user goes "back" from a prior page.

                ' Hide all warning messages. Make them visible as needed.
                pnlCCTaxExemptDetails.Visible = False
                divWarningMsg.Visible = False

                If rbSonyAccount.SelectedValue = "1" Then       ' I have a Sony Account
                    tblAccounts.Rows.Add(PopulateAccountNumberData(CreateAccountNumberBoxes()))
                    AddMoreAccountTextBoxs()
                ElseIf rbSonyAccount.SelectedValue = "0" Then   ' No Sony Account
                    If isCanada Then
                        divWarningMsg.Visible = True    ' Show the "Account Required" message
                    Else
                        tblAccounts.Rows.Add(PopulateTaxData(CreateTaxExemptQuestion()))
                        If Page.FindControl("rbTaxExempt") IsNot Nothing Then
                            rbTax = Page.FindControl("rbTaxExempt")
                            If rbTax.SelectedValue = "1" Then
                                divCustomerID.Visible = (customer IsNot Nothing)
                                pnlCCTaxExemptDetails.Visible = True
                            End If
                        End If
                    End If
                End If

                If Session("Language_Changed") IsNot Nothing AndAlso Session("Language_Changed").ToString() = "1" Then
                    fnSetSession()
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Sub btnNext_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnNext.Click
            Try
                If rbSonyAccount.SelectedValue = "1" And Not AreAccountsInputProperly() Then
                    Return  ' The Function called above will take care of any error messages.
                End If

                If IsRegistrationValid() Then
                    Response.Redirect("~/Registration.aspx", False)
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        ''' <summary>
        ''' Checks for input errors in the account numbers. Expects less than 11 characters and
        ''' no duplicate entries, and Canadian users to enter at least one number.
        ''' Does NOT validate accounts with SAP.
        ''' </summary>
        Private Function AreAccountsInputProperly() As Boolean
            Dim accounts As New List(Of String)
            Dim inputAccount As String
            ErrorLabel.Text = ""
            For i As Integer = 1 To 12
                inputAccount = Request.Form("tb" & i)
                If Not String.IsNullOrEmpty(inputAccount) Then
                    inputAccount = inputAccount.Trim()
                    If accounts.Contains(inputAccount) Then     ' Duplicate account number
                        ErrorLabel.Text = Resources.Resource.el_rgn_Err_AcctDuplicate
                        Return False
                    ElseIf inputAccount.Length > 10 Then        ' Account length
                        ErrorLabel.Text = Resources.Resource.el_rgn_Err_AccntNoValidate
                        Return False
                    Else
                        accounts.Add(inputAccount)
                    End If
                End If
            Next
            If rbSonyAccount.SelectedValue = "1" And accounts.Count = 0 And HttpContextManager.GlobalData.IsCanada Then
                ' Canadians must be Account Holders, and must enter at least one valid account number.
                ErrorLabel.Text = "You must enter at least one valid Sony Account number."
                Return False
            End If
            Return True
        End Function

        ''' <summary>
        ''' Checks that account numbers are valid in SAP, prepares the Customer object and
        '''  stores it in Session("newCustomer"). Sets error messages if anything fails.
        ''' </summary>
        Private Function IsRegistrationValid() As Boolean
            Dim custMan As New CustomerManager()
            Dim wasAccountAdded As Boolean = False
            Dim badAccountNumbers As String = ""
            Dim globalData As GlobalData = HttpContextManager.GlobalData

            Try
                customer = If(Session("newCustomer"), New Customer)     ' Pulls Session variable, or creates a new Customer object.
                If customer Is Nothing Then customer = New Customer()   ' In case we stored null in Session("newCustomer")

                If rbSonyAccount.SelectedValue = "1" Then     ' Yes, I have a Sony Account
                    customer.UserType = "P"
                ElseIf rbSonyAccount.SelectedValue = "0" Then ' No Sony Account
                    If globalData.IsCanada Then Return False ' Canadian users must have an Account
                    customer.UserType = "C"
                Else  ' User hasn't answered the first question yet.
                    ErrorLabel.Text = "Please select an answer."
                    Return False
                End If
                customer.StatusCode = 1

                ' Handle SAP Buyer Accounts
                'If (isVerified) Then  ' This is always true!
                If Request.Form("rbTaxExempt") = "1" Then
                    Session.Add("isTaxExempt", True)
                    customer.Tax_Exempt = True
                Else
                    Session.Remove("isTaxExempt")
                    customer.Tax_Exempt = False
                End If

                If Not AreAccountsValidInSAP(badAccountNumbers) Then ' Invalid account number(s) were entered.
                    If globalData.IsCanada Then
                        ErrorLabel.Text = Resources.Resource.el_InvalidAcct.Replace("†", badAccountNumbers)
                    Else
                        ErrorLabel.Text = $"Invalid Account number ({badAccountNumbers}) for USA"
                    End If
                    Return False
                End If

                If rbSonyAccount.SelectedValue = "1" Then   ' "Yes, I have a Sony Account"
                    customer.ClearSAPBillToAccounts()       ' 2018-01-12 ASleight - Clear any previous attempts first.
                    Dim tbArray() As String = {(Request.Form("tb1")), (Request.Form("tb2")), (Request.Form("tb3")), (Request.Form("tb4")), (Request.Form("tb5")), (Request.Form("tb6")), (Request.Form("tb7")), (Request.Form("tb8")), (Request.Form("tb9")), (Request.Form("tb10")), (Request.Form("tb11")), (Request.Form("tb12"))}
                    For Each acc As String In tbArray
                        If Not String.IsNullOrWhiteSpace(acc) Then
                            customer.AddSAPBillToAccount(acc)
                            wasAccountAdded = True
                        End If
                    Next
                    ' 2018-01-09 Adding a check for no accounts entered. The "wasAccountAdded" variable was declared, given value, but never checked.
                    If Not wasAccountAdded And globalData.IsCanada Then
                        ErrorLabel.Text = "You must provide at least one valid Sony Account to continue."
                        Return False
                    End If
                End If

                If Session("newCustomer") IsNot Nothing Then
                    custMan.UpdateCustomerSubscription(customer)
                    custMan.AddCustomerAccount(customer)
                    HttpContextManager.Customer = customer
                    Session.Add("siamID", customer.SIAMIdentity)
                Else
                    Session.Add("newCustomer", customer)
                End If

                Return True
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                Return False
            End Try
        End Function

        ''' <summary>
        ''' Checks SAP for the validity of the entered account numbers. Does not check if the list is blank.
        ''' Updates the provided string parameter with a comma-delimited list of invalid account numbers.
        ''' </summary>
        Private Function AreAccountsValidInSAP(ByRef badAccountNumbers As String) As Boolean
            Dim areAccountsValid As Boolean = True
            Dim thisArrayList As New ArrayList
            Dim thisAccount As Account
            Dim thisCustMng As New CustomerManager
            Dim inputAccount As String
            'Dim txtTemp As SPSTextBox
            badAccountNumbers = ""

            Try
                Dim tbArray() As String = {(Request.Form("tb1")), (Request.Form("tb2")), (Request.Form("tb3")), (Request.Form("tb4")), (Request.Form("tb5")), (Request.Form("tb6")), (Request.Form("tb7")), (Request.Form("tb8")), (Request.Form("tb9")), (Request.Form("tb10")), (Request.Form("tb11")), (Request.Form("tb12"))}

                For i As Int32 = 0 To 11
                    If Not String.IsNullOrWhiteSpace(tbArray(i)) Then
                        inputAccount = tbArray(i).Trim()
                        thisAccount = New Account With {
                            .AccountNumber = inputAccount
                        }
                        thisCustMng.PopulateSAPAccount(thisAccount, HttpContextManager.GlobalData)  ' Check with SAP if it is a valid account number. Returns the first match.

                        ' Check if no account was found, and also ensure they don't randomly match a partial search.
                        If String.IsNullOrEmpty(thisAccount.SoldToAccount) OrElse String.IsNullOrEmpty(thisAccount.AccountNumber) OrElse thisAccount.Validated = False _
                        OrElse (thisAccount.AccountNumber.TrimStart("0") <> inputAccount.TrimStart("0")) Then
                            ' 2018-01-12 ASleight - Compare by removing leading zeros, since not all users will add them.
                            If badAccountNumbers.Length > 0 Then badAccountNumbers &= ","
                            badAccountNumbers &= inputAccount
                            areAccountsValid = False
                        End If
                    End If
                Next
                ' Debug statement in Test environments only. Useful when testing Registrations so we can check account numbers.
                If Not String.IsNullOrWhiteSpace(badAccountNumbers) Then Utilities.LogDebug("Invalid account(s): " + badAccountNumbers)

                Return areAccountsValid
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                Return False
            End Try
        End Function

        Public Sub rbSonyAccount_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As EventArgs) Handles rbSonyAccount.SelectedIndexChanged
            ErrorLabel.Text = ""
            'LabelSonyAct.CssClass = "bodyCopy"
            ' Show the "Account Required" message for Canadian users who have answer "No" to having an account.
            divWarningMsg.Visible = (rbSonyAccount.SelectedValue = "0" And HttpContextManager.GlobalData.IsCanada)
            PopulateAccount_TaxTable()
            If rbSonyAccount.SelectedValue = "1" Then AddMoreAccountTextBoxs()
            AddMore2.CommandArgument = ""
        End Sub

        Private Function CreateAccountNumberBoxes() As TableRow
            Dim TR As New TableRow
            Dim TD As New TableCell

            Dim tb As New SPSTextBox With {
                .ID = "tb1",
                .Width = Unit.Pixel(90),
                .CssClass = "bodyCopy",
                .MaxLength = 15
            }

            Dim tb2 As New SPSTextBox With {
                .ID = "tb2",
                .Width = Unit.Pixel(90),
                .CssClass = "bodyCopy",
                .MaxLength = 15
            }

            Dim tb3 As New SPSTextBox With {
                .ID = "tb3",
                .Width = Unit.Pixel(90),
                .CssClass = "bodyCopy",
                .MaxLength = 15
            }

            Dim tb4 As New SPSTextBox With {
                .ID = "tb4",
                .Width = Unit.Pixel(90),
                .CssClass = "bodyCopy",
                .MaxLength = 15
            }


            Dim tb5 As New SPSTextBox With {
                .ID = "tb5",
                .Width = Unit.Pixel(90),
                .CssClass = "bodyCopy",
                .MaxLength = 15
            }

            Dim tb6 As New SPSTextBox With {
                .ID = "tb6",
                .Width = Unit.Pixel(90),
                .CssClass = "bodyCopy",
                .MaxLength = 15
            }

            Dim tb7 As New SPSTextBox With {
                .ID = "tb7",
                .Width = Unit.Pixel(90),
                .CssClass = "bodyCopy",
                .Visible = False,
                .MaxLength = 15
            }

            Dim tb8 As New SPSTextBox With {
                .ID = "tb8",
                .Width = Unit.Pixel(90),
                .CssClass = "bodyCopy",
                .Visible = False,
                .MaxLength = 15
            }

            Dim tb9 As New SPSTextBox With {
                .ID = "tb9",
                .Width = Unit.Pixel(90),
                .CssClass = "bodyCopy",
                .Visible = False,
                .MaxLength = 15
            }

            Dim tb10 As New SPSTextBox With {
                .ID = "tb10",
                .Width = Unit.Pixel(90),
                .CssClass = "bodyCopy",
                .Visible = False,
                .MaxLength = 15
            }


            Dim tb11 As New SPSTextBox With {
                .ID = "tb11",
                .Width = Unit.Pixel(90),
                .CssClass = "bodyCopy",
                .Visible = False,
                .MaxLength = 15
            }

            Dim tb12 As New SPSTextBox With {
                .ID = "tb12",
                .Width = Unit.Pixel(90),
                .CssClass = "bodyCopy",
                .Visible = False,
                .MaxLength = 15
            }

            'If AddMore2.CommandArgument.Length = 0 Then
            '    AddMore2.Visible = True
            'Else
            AddMore2.Visible = False
            'End If

            'TD.Controls.Add(New LiteralControl("<tr><td><img src=""images/spacer.gif"" width=""20"" alt=""""></td><td class=""bodyCopy"" align=""left"" colspan=""2"">If you have a Sony Account Number(s), please enter it in a text box below. Each text box is designed to hold a unique 7 digit account number. You can add up to 12 Sony Account numbers on this page. Sony Account numbers are a 7 digit number, if you don't know your number you can leave it blank.</td></tr>"))
            'TD.Controls.Add(New LiteralControl("<tr><td><img src=""images/spacer.gif"" width=""20"" alt=""""></td><td class=""bodyCopy"" align=""left"" colspan=""2"">Your Sony Account(s): &nbsp;"))
            TD.Controls.Add(tb)
            TD.Controls.Add(tb2)
            TD.Controls.Add(tb3)
            TD.Controls.Add(tb4)
            TD.Controls.Add(tb5)
            TD.Controls.Add(tb6)
            TD.Controls.Add(New LiteralControl("<br/>"))
            TD.Controls.Add(tb7)
            TD.Controls.Add(tb8)
            TD.Controls.Add(tb9)
            TD.Controls.Add(tb10)
            TD.Controls.Add(tb11)
            TD.Controls.Add(tb12)
            TD.Controls.Add(New LiteralControl("&nbsp;&nbsp;"))
            TD.Controls.Add(New LiteralControl("</td></tr>"))

            focusFormField = tb.ID.ToString
            TR.Cells.Add(TD)

            Return TR
        End Function

        Private Function CreateTaxExemptQuestion() As TableRow
            Dim TR As New TableRow
            Dim TD As New TableCell
            Dim rb As New RadioButtonList With {
                .ID = "rbTaxExempt",
                .AutoPostBack = True,
                .CssClass = "bodyCopy",
                .SelectedValue = "Yes",
                .RepeatDirection = RepeatDirection.Horizontal
            }
            rb.Items.Add(New ListItem("Yes", "1"))
            rb.Items.Add(New ListItem("No", "0"))
            rb.SelectedIndex = 1

            AddMore2.Visible = False
            Dim strMsg As String = Resources.Resource.el_rgn_TaxExempt
            TD.Controls.Add(New LiteralControl("<tr><td><img src=""images/spacer.gif"" width=""20"" alt=""""></td><td class=""bodyCopy"" align=""left"" colspan=""2"">" + strMsg))
            TD.Controls.Add(rb)
            TD.Controls.Add(New LiteralControl("</td></tr>"))
            focusFormField = rbSonyAccount.ID.ToString

            TR.Cells.Add(TD)

            Return TR
        End Function

        Private Sub PopulateAccount_TaxTable()
            tblAccounts.Rows.Clear()

            If rbSonyAccount.SelectedValue = "1" Then
                tblAccounts.Rows.Add(CreateAccountNumberBoxes())
                pnlCCTaxExemptDetails.Visible = False
            Else
                If HttpContextManager.GlobalData.IsAmerica Then
                    tblAccounts.Rows.Add(CreateTaxExemptQuestion())
                End If
            End If
        End Sub

        Private Function PopulateTaxData(ByRef thisTR As TableRow) As TableRow
            tblAccounts.Rows.Clear()

            If Request.Form("rbTaxExempt") IsNot Nothing Then
                If Request.Form("rbTaxExempt") = "0" Then
                    CType(thisTR.Cells(0).Controls.Item(1), RadioButtonList).SelectedIndex = 1
                    'CType(thisTR.Cells(0).Controls.Item(1), RadioButtonList).Items(0).Selected = True
                ElseIf Request.Form("rbTaxExempt") = "1" Then
                    CType(thisTR.Cells(0).Controls.Item(1), RadioButtonList).SelectedIndex = 0
                    'CType(thisTR.Cells(0).Controls.Item(1), RadioButtonList).Items(1).Selected = True
                End If
            End If

            Return thisTR
        End Function

        Private Function PopulateAccountNumberData(ByRef thisTR As TableRow) As TableRow
            Dim thisControl As Control
            Dim showAll As Boolean = (AddMore2.CommandArgument = "ShowAll") ' False

            'If AddMore2.CommandArgument = "ShowAll" Then showAll = True
            For Each thisControl In thisTR.Cells(0).Controls
                If thisControl.GetType.Name = "SPSTextBox" Then
                    If Request.Form(thisControl.ID) IsNot Nothing Then
                        CType(thisControl, SPSTextBox).Text = Request.Form(thisControl.ID)
                    End If
                    If showAll Then CType(thisControl, SPSTextBox).Visible = True
                End If
            Next

            Return thisTR
        End Function

        Private Sub AddMore2_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles AddMore2.Click
            AddMoreAccountTextBoxs()
            AddMore2.CommandArgument = "ShowAll"
        End Sub

        Private Sub AddMoreAccountTextBoxs()
            tblAccounts.Rows.Clear()
            Dim TR As New TableRow
            Dim TD As New TableCell

            Dim tb As New SPSTextBox With {
                .ID = "tb1",
                .Width = Unit.Pixel(90),
                .CssClass = "bodyCopy",
                .MaxLength = 10,
                .Text = (Request.Form("tb1"))
            }

            Dim tb2 As New SPSTextBox With {
                .ID = "tb2",
                .Width = Unit.Pixel(90),
                .CssClass = "bodyCopy",
                .MaxLength = 10,
                .Text = (Request.Form("tb2"))
            }

            Dim tb3 As New SPSTextBox With {
                .ID = "tb3",
                .Width = Unit.Pixel(90),
                .CssClass = "bodyCopy",
                .MaxLength = 10,
                .Text = (Request.Form("tb3"))
            }

            Dim tb4 As New SPSTextBox With {
                .ID = "tb4",
                .Width = Unit.Pixel(90),
                .CssClass = "bodyCopy",
                .MaxLength = 10,
                .Text = (Request.Form("tb4"))
            }

            Dim tb5 As New SPSTextBox With {
                .ID = "tb5",
                .Width = Unit.Pixel(90),
                .CssClass = "bodyCopy",
                .MaxLength = 10,
                .Text = (Request.Form("tb5"))
            }

            Dim tb6 As New SPSTextBox With {
                .ID = "tb6",
                .Width = Unit.Pixel(90),
                .CssClass = "bodyCopy",
                .MaxLength = 10,
                .Text = (Request.Form("tb6"))
            }

            Dim tb7 As New SPSTextBox With {
                .ID = "tb7",
                .Width = Unit.Pixel(90),
                .CssClass = "bodyCopy",
                .MaxLength = 10,
                .Text = (Request.Form("tb7"))
            }

            Dim tb8 As New SPSTextBox With {
                .ID = "tb8",
                .Width = Unit.Pixel(90),
                .CssClass = "bodyCopy",
                .MaxLength = 10,
                .Text = (Request.Form("tb8"))
            }

            Dim tb9 As New SPSTextBox With {
                .ID = "tb9",
                .Width = Unit.Pixel(90),
                .CssClass = "bodyCopy",
                .MaxLength = 10,
                .Text = (Request.Form("tb9"))
            }

            Dim tb10 As New SPSTextBox With {
                .ID = "tb10",
                .Width = Unit.Pixel(90),
                .CssClass = "bodyCopy",
                .MaxLength = 10,
                .Text = (Request.Form("tb10"))
            }

            Dim tb11 As New SPSTextBox With {
                .ID = "tb11",
                .Width = Unit.Pixel(90),
                .CssClass = "bodyCopy",
                .MaxLength = 10,
                .Text = (Request.Form("tb11"))
            }

            Dim tb12 As New SPSTextBox With {
                .ID = "tb12",
                .Width = Unit.Pixel(90),
                .CssClass = "bodyCopy",
                .MaxLength = 10,
                .Text = (Request.Form("tb12"))
            }

            AddMore2.Visible = False
            tb7.Visible = True
            tb8.Visible = True
            tb9.Visible = True
            tb10.Visible = True
            tb11.Visible = True
            tb12.Visible = True

            Dim acctInstructions As String = IIf(HttpContextManager.GlobalData.IsAmerica, Resources.Resource.el_rgn_AcctHdl, Resources.Resource.el_rgn_AcctHdl_CA)
            TD.Controls.Add(New LiteralControl("<tr><td><img src=""images/spacer.gif"" width=""20"" alt=""""></td><td class=""bodyCopy"" align=""left"" colspan=""2"">" + acctInstructions + "</td></tr>"))
            TD.Controls.Add(New LiteralControl("<tr><td width=""10""><img src=""images/spacer.gif"" width=""10"" alt=""""></td></tr>"))
            TD.Controls.Add(New LiteralControl("<tr><td width=""10""><img src=""images/spacer.gif"" width=""10"" alt=""""></td><td colspan=""2"">"))
            TD.Controls.Add(tb)
            TD.Controls.Add(tb2)
            TD.Controls.Add(tb3)
            TD.Controls.Add(tb4)
            TD.Controls.Add(tb5)
            TD.Controls.Add(tb6)
            TD.Controls.Add(New LiteralControl("</td></tr>"))

            TD.Controls.Add(New LiteralControl("<tr><td width=""20""><img src=""images/spacer.gif"" width=""20"" alt=""""></td><td colspan=""2"">"))
            TD.Controls.Add(tb7)
            TD.Controls.Add(tb8)
            TD.Controls.Add(tb9)
            TD.Controls.Add(tb10)
            TD.Controls.Add(tb11)
            TD.Controls.Add(tb12)
            TD.Controls.Add(New LiteralControl("</td></tr>"))

            TR.Cells.Add(TD)
            tblAccounts.Rows.Add(TR)
            focusFormField = tb.ID.ToString
        End Sub

        Private Sub ShowDynamicData()
            If rbSonyAccount.SelectedValue = "1" Then
                tblAccounts.Rows.Add(PopulateAccountNumberData(CreateAccountNumberBoxes()))
                AddMoreAccountTextBoxs()
            ElseIf rbSonyAccount.SelectedValue = "0" Then
                tblAccounts.Rows.Add(PopulateTaxData(CreateTaxExemptQuestion()))
            End If
        End Sub

        Private Function GenerateSiamIDImageString(ByVal strSiamIdentity As String) As String
            Dim strImg As String = "<img src='images/ImgName.JPG' style='border: none;' alt='' />"
            Dim strValue As String = ""

            Try
                For Each strChar In strSiamIdentity.Replace(" ", "").ToCharArray()
                    strValue += strImg.Replace("ImgName", strChar.ToString())
                Next
            Catch ex As Exception
                Return strValue
            End Try

            Return strValue
        End Function

        Public Sub fnGetSession()
            'Session.Add("CultureData", ObjCst)
        End Sub

        Public Sub fnSetSession()
            If Session("CultureData") IsNot Nothing Then
                Session.Remove("CultureData")
                Session("Language_Changed") = "0"
            End If
        End Sub
    End Class
End Namespace
