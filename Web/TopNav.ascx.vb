Imports ServicesPlusException
Imports Sony.US.SIAMUtilities

Namespace ServicePLUSWebApp

    Partial Class TopNav
        Inherits UserControl

#Region "   PAGE VARIABLES   "
        Dim _activePageIcon As enumPageIconToDisplay = enumPageIconToDisplay.NoPage
        Dim callingPage As String
        Dim wasPageSetByProperty As Boolean = False
        Dim _showdefaultIcon As Boolean = True
        Public selectedLanguage As String
        Dim domianurl As String = Nothing
        Dim isCanadaSite As Boolean = False
        Dim culinfodtls As String = ""

        Enum enumPageIconToDisplay
            MyCatalog = 0
            ViewCart = 1
            MyOrder = 2
            MyProfile = 3
            Logout = 4
            NoPage = 5
            Help = 6
            ContactUs = 7
            Promos = 8
        End Enum
#End Region

#Region "   PAGE EVENTS   "
        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

        Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
            'Dim ci As New CultureInfo(HttpContextManager.SelectedLang)
            'Thread.CurrentThread.CurrentCulture = ci
            'Thread.CurrentThread.CurrentUICulture = ci
        End Sub

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                Dim signinStr As String = ""    ' Store the URL for the "Login" links, which change by language
                Dim uri As String = Request.Url.AbsolutePath
                Dim split As String() = uri.Split("/"c)
                Dim pagePart As String = ""     ' Store the part of the URL that contains the page name

                ' Use the Global Data to set up some URLs and set Languages
                ' NOTE: Updating HttpContextManager.SelectedLang automatically updates GlobalData.Language to the same value.
                If HttpContextManager.GlobalData.IsCanada Then
                    isCanadaSite = True
                    If HttpContextManager.GlobalData.IsFrench Then
                        signinStr = ConfigurationData.Environment.IdentityURLs.LoginFRCA
                        selectedLanguage = "Canada (Fran�ais)"
                    Else
                        signinStr = ConfigurationData.Environment.IdentityURLs.LoginENCA
                        selectedLanguage = "Canada (English)"
                    End If
                Else
                    signinStr = ConfigurationData.Environment.IdentityURLs.LoginUS
                    HttpContextManager.SelectedLang = "en-US"
                    selectedLanguage = "America (English)"
                End If

                'If ConfigurationData.GeneralSettings.Environment = "Development" Then
                'For Canada: Uncomment the below line  and comment the next. For US just viseversa. Note: Its only for local devbox
                'Dim uri As String = "https://servicesplus-qa.sony.ca/default.aspx"
                'End If

                If split.Length >= 3 Then
                    If Not String.IsNullOrEmpty(split(2)) Then pagePart = split(2)
                ElseIf split.Length >= 2 Then
                    If Not String.IsNullOrEmpty(split(1)) Then pagePart = split(1)
                End If

                If pagePart.Contains("sony-knowledge-base-search.aspx") Then
                    signinStr &= "%26post%3Dskbs"
                ElseIf pagePart.Contains("sony-operation-manual.aspx") Then
                    signinStr &= "%26post%3Dom"
                ElseIf pagePart.Contains("sony-parts.aspx") Then
                    signinStr &= "%26post%3Dqo"
                ElseIf pagePart.Contains("sony-repair.aspx") Then
                    signinStr &= "%26post%3Dsonyrepair"
                ElseIf pagePart.Contains("sony-software.aspx") Then
                    signinStr &= "%26post%3Dssm"
                ElseIf pagePart.Contains("sony-training-catalog.aspx") Then
                    signinStr &= "%26post%3Dssp"
                ElseIf pagePart.Contains("sony-service-agreements.aspx") Or uri.Contains("sony-service-agreements.aspx#identifier") Then
                    signinStr &= "%26post%3Dprc"
                ElseIf pagePart.Contains("sony-service-repair-maintenance.aspx") Then
                    signinStr &= "%26post%3Dssrandm"
                ElseIf pagePart.Contains("sony-part-catalog.aspx") Then
                    signinStr &= "%26post%3Dspcd"
                ElseIf pagePart.Contains("parts-promotions.aspx") Then
                    signinStr &= "%26post%3DPP1"
                ElseIf pagePart.Contains("PartsPlusResults.aspx") Then
                    signinStr &= "%26post%3DPPR"
                End If

                If HttpContextManager.Customer IsNot Nothing Then
                    If sender.ID IsNot Nothing Then callingPage = sender.ID.ToString()
                    If callingPage IsNot Nothing And wasPageSetByProperty Then SetActivePageIcon()

                    ' ASleight - This spacing does not work in the signed-in French version of the site. Causes images to be pushed out to the right of the page.
                    '              I've also removed all of the forced image widths in the rest of the row to prevent horizontal compression of the wider French images.

                    'null space to match the top header's width
                    'If isCanadaSite = False Then
                    '    MyCatalogPH.Controls.Add(New LiteralControl("<td><img height=""27"" src=""images/sp_int_topRight_fade_black.gif"" width=""230px""></td>" + vbCrLf))
                    'ElseIf isCanadaSite = True Then
                    '    If HttpContextManager.SelectedLang = "en-US" Then
                    '        MyCatalogPH.Controls.Add(New LiteralControl("<td><img height=""27"" src=""images/sp_int_topRight_fade_black.gif"" width=""125px""></td>" + vbCrLf))
                    '    ElseIf HttpContextManager.SelectedLang = "fr-CA" Then
                    '        MyCatalogPH.Controls.Add(New LiteralControl("<td><img height=""27"" src=""images/sp_int_topRight_fade_black.gif"" width=""125px""></td>" + vbCrLf))
                    '    End If
                    'End If

                    ' -- My Catalog
                    If _activePageIcon = enumPageIconToDisplay.MyCatalog Then
                        ' ASleight - The <td> elements below are problematic. I've switched to a parent <div> in the .ascx markup with "text-align: right" style.
                        'MyCatalogPH.Controls.Add(New LiteralControl("<td>" + vbCrLf))
                        MyCatalogPH.Controls.Add(New LiteralControl($"<img height=""27"" src=""{Resources.Resource.home_hdr_cat_img}"" border=""0"" name=""myCatalog_btn"" alt=""My Catalog"">"))
                        'MyCatalogPH.Controls.Add(New LiteralControl("</td>" + vbCrLf))
                    Else
                        'MyCatalogPH.Controls.Add(New LiteralControl("<td>" + vbCrLf))
                        MyCatalogPH.Controls.Add(New LiteralControl($"<a onmouseover=""imageSwap('myCatalog_btn','{Resources.Resource.home_hdr_cat_img}')""
                            onmouseout=""imageSwap('myCatalog_btn','{Resources.Resource.home_hdr_catoff_img}')"" href=""ViewMyCatalog.aspx"" title=""Go to the My Catalog page"">"))
                        MyCatalogPH.Controls.Add(New LiteralControl($"<img height=""27"" src=""{Resources.Resource.home_hdr_catoff_img}"" border=""0"" name=""myCatalog_btn"" alt=""My Catalog"">"))
                        MyCatalogPH.Controls.Add(New LiteralControl("</a>"))
                        'MyCatalogPH.Controls.Add(New LiteralControl("</td>" + vbCrLf))
                    End If

                    ' -- My Orders
                    If _activePageIcon = enumPageIconToDisplay.MyOrder Then
                        'MyOrderPH.Controls.Add(New LiteralControl("<td>" + vbCrLf))
                        MyOrderPH.Controls.Add(New LiteralControl($"<img height=""27"" src=""{Resources.Resource.home_hdr_oron_img}"" border=""0"" alt=""My Orders"" name=""myOrders_btn"">"))
                        'MyOrderPH.Controls.Add(New LiteralControl("</td>" + vbCrLf))
                    Else
                        'MyOrderPH.Controls.Add(New LiteralControl("<td>" + vbCrLf))
                        MyOrderPH.Controls.Add(New LiteralControl($"<a onmouseover=""imageSwap('myOrders_btn','{Resources.Resource.home_hdr_oron_img}')""
                            onmouseout=""imageSwap('myOrders_btn','{Resources.Resource.home_hdr_oroff_img}')"" href=""CustomerOrders.aspx"" title=""Go to the My Orders page"">"))
                        MyOrderPH.Controls.Add(New LiteralControl($"<img height=""27"" src=""{Resources.Resource.home_hdr_oroff_img}"" border=""0"" name=""myOrders_btn"" alt=""My Orders"">"))
                        MyOrderPH.Controls.Add(New LiteralControl("</a>"))
                        'MyOrderPH.Controls.Add(New LiteralControl("</td>" + vbCrLf))
                    End If

                    ' -- My Profile
                    If _activePageIcon = enumPageIconToDisplay.MyProfile Then
                        'MyProfilePH.Controls.Add(New LiteralControl("<td>" + vbCrLf))
                        MyProfilePH.Controls.Add(New LiteralControl($"<img height=""27"" src=""{Resources.Resource.home_hdr_proon_img}"" border=""0"" name=""myProfile_btn"" alt=""My Profile"">"))
                        'MyProfilePH.Controls.Add(New LiteralControl("</td>" + vbCrLf))
                    Else
                        'MyProfilePH.Controls.Add(New LiteralControl("<td>" + vbCrLf))
                        MyProfilePH.Controls.Add(New LiteralControl($"<a onmouseover=""imageSwap('myProfile_btn','{Resources.Resource.home_hdr_proon_img}')""
                            onmouseout=""imageSwap('myProfile_btn','{Resources.Resource.home_hdr_prooff_img}')"" href=""MyProfile.aspx"" title=""Go to the My Profile page"">"))
                        MyProfilePH.Controls.Add(New LiteralControl($"<img height=""27"" src=""{Resources.Resource.home_hdr_prooff_img}"" border=""0"" name=""myProfile_btn"" alt=""My Profile"">"))
                        MyProfilePH.Controls.Add(New LiteralControl("</a>"))
                        'MyProfilePH.Controls.Add(New LiteralControl("</td>" + vbCrLf))
                    End If

                    '' -- logout
                    If _activePageIcon = enumPageIconToDisplay.Logout Then
                        'LogoutPH.Controls.Add(New LiteralControl("<td>" + vbCrLf))
                        LogoutPH.Controls.Add(New LiteralControl($"<img height=""27"" src=""{Resources.Resource.home_hdr_lgouton_img}"" border=""0"" name=""logout_btn"" alt=""Log out"">"))
                        'LogoutPH.Controls.Add(New LiteralControl("</td>" + vbCrLf))
                    Else
                        'LogoutPH.Controls.Add(New LiteralControl("<td>" + vbCrLf))
                        LogoutPH.Controls.Add(New LiteralControl($"<a onmouseover=""imageSwap('logout_btn','{Resources.Resource.home_hdr_lgouton_img}')""
                            onmouseout=""imageSwap('logout_btn','{Resources.Resource.home_hdr_lgoutoff_img}')"" href=""Logout.aspx"" title=""Log out of your account."">"))
                        'LogoutPH.Controls.Add(New LiteralControl("<a onmouseover=""imageSwap('logout_btn','images/sp_int_logout_btn_on.gif')"" onmouseout=""imageSwap('logout_btn','images/sp_int_logout_btn_off.gif')"" href=""#"" onclick=""window.open('" + config_settings.Item("ServicesPLUSLogoutURL").ToString() + "','','" + CON_POPUP_FEATURES + "'); document.getElementById('hdnlogout').value='logout=1'; window.document.forms(0).submit();"">"))
                        LogoutPH.Controls.Add(New LiteralControl($"<img height=""27"" src=""{Resources.Resource.home_hdr_lgoutoff_img}"" border=""0"" name=""logout_btn"" alt=""Log out"">"))
                        LogoutPH.Controls.Add(New LiteralControl("</a>"))
                        'LogoutPH.Controls.Add(New LiteralControl("</td>" + vbCrLf))
                    End If
                    'MenuPH.Controls.Add(New LiteralControl("<td><dl id='sample' class='dropdown'><dt><a href='#'><span>" + changeLang + "</span></a></dt><dd><ul><li><a href='#'>English<span class='value'>en-US</span></a></li><li><a href='#'>Fran�ais<span class='value'>fr-CA</span></a></li></ul></dd></dl></td>"))
                    'LogoutPH.Controls.Add(New LiteralControl("<td>"))
                    'LogoutPH.Controls.Add(New LiteralControl("<img height=""27"" src=""images/sp_int_topRight_fade_black.gif"" width=""23"">"))
                    'LogoutPH.Controls.Add(New LiteralControl("</td>"))

                    ' ASleight - As with my prior comment above, this section creates formatting issues in French.
                    '              Forced-width spacers are generally not a good idea when content isn't fixed-width.
                    'Else
                    '    If isCanadaSite = False Then
                    '        MyCatalogPH.Controls.Add(New LiteralControl("<td><img height=""27"" src=""images/sp_int_topRight_fade_black.gif"" width=""250px""></td>" + vbCrLf))
                    '    ElseIf isCanadaSite = True Then
                    '        If HttpContextManager.SelectedLang = "en-US" Then
                    '            MyCatalogPH.Controls.Add(New LiteralControl("<td><img height=""27"" src=""images/sp_int_topRight_fade_black.gif"" width=""250px""></td>" + vbCrLf))
                    '        ElseIf HttpContextManager.SelectedLang = "fr-CA" Then
                    '            MyCatalogPH.Controls.Add(New LiteralControl("<td><img height=""27"" src=""images/sp_int_topRight_fade_black.gif"" width=""210px""></td>" + vbCrLf))
                    '        End If
                    '    End If
                End If

                ''promotions
                If _showdefaultIcon Then
                    If (Session("PunchOutSesison") IsNot Nothing And Me.Parent.Parent.ClientID <> ("SignIn") And Me.Parent.Parent.ClientID <> "PunchOutSignIn") Or (Session("PunchOutSesison") Is Nothing) Then
                        If _activePageIcon = enumPageIconToDisplay.Promos Then
                            'PromosPH.Controls.Add(New LiteralControl("<td>" + vbCrLf))
                            PromosPH.Controls.Add(New LiteralControl($"<img height=""27"" src=""{Resources.Resource.home_hdr_promoson_img}"" border=""0"" name=""Promos_btn"" alt=""Promotions"">"))
                            'PromosPH.Controls.Add(New LiteralControl("</td>" + vbCrLf))
                        Else
                            'PromosPH.Controls.Add(New LiteralControl("<td>" + vbCrLf))
                            PromosPH.Controls.Add(New LiteralControl($"<a onmouseover=""imageSwap('Promos_btn','{Resources.Resource.home_hdr_promoson_img}')""
                                onmouseout=""imageSwap('Promos_btn','{Resources.Resource.home_hdr_promsoff_img}')"" href=""http://www.sony.com/professionalservices/promotions""
                                target=""_blank"" title=""Visit the Sony parts promotions page"">"))
                            PromosPH.Controls.Add(New LiteralControl("<img height=""27"" src=" + Resources.Resource.home_hdr_promsoff_img + " border=""0"" name=""Promos_btn"" alt=""Promotions"">"))
                            PromosPH.Controls.Add(New LiteralControl("</a>"))
                            'PromosPH.Controls.Add(New LiteralControl("</td>" + vbCrLf))
                        End If
                        ''Help
                        If _activePageIcon = enumPageIconToDisplay.Help Then
                            'HelpPH.Controls.Add(New LiteralControl("<td>" + vbCrLf))
                            HelpPH.Controls.Add(New LiteralControl($"<img height=""27"" src=""{Resources.Resource.home_hdr_helpon_img}"" border=""0"" name=""Help_btn"" alt=""Help"">"))
                            'HelpPH.Controls.Add(New LiteralControl("</td>" + vbCrLf))
                        Else
                            'HelpPH.Controls.Add(New LiteralControl("<td>" + vbCrLf))
                            HelpPH.Controls.Add(New LiteralControl($"<a onmouseover=""imageSwap('Help_btn','{Resources.Resource.home_hdr_helpon_img}')""
                                onmouseout=""imageSwap('Help_btn','{Resources.Resource.home_hdr_helpoff_img}')"" href=""Help.aspx"" title=""Go to the Help page to learn how to use this site."">"))
                            HelpPH.Controls.Add(New LiteralControl($"<img height=""27"" src=""{Resources.Resource.home_hdr_helpoff_img}"" border=""0"" name=""Help_btn"" alt=""Help"">"))
                            HelpPH.Controls.Add(New LiteralControl("</a>"))
                            'HelpPH.Controls.Add(New LiteralControl("</td>" + vbCrLf))
                        End If
                        'ContactUs
                        If _activePageIcon = enumPageIconToDisplay.ContactUs Then
                            'ContactUsPH.Controls.Add(New LiteralControl("<td>" + vbCrLf))
                            ContactUsPH.Controls.Add(New LiteralControl($"<img height=""27"" src=""{Resources.Resource.home_hdr_conton_img}"" border=""0"" name=""ContactUs_btn"" alt=""Contact Us"">"))
                            'ContactUsPH.Controls.Add(New LiteralControl("</td>" + vbCrLf))
                        Else
                            'ContactUsPH.Controls.Add(New LiteralControl("<td>" + vbCrLf))
                            ContactUsPH.Controls.Add(New LiteralControl($"<a onmouseover=""imageSwap('ContactUs_btn','{Resources.Resource.home_hdr_conton_img}')""
                                onmouseout=""imageSwap('ContactUs_btn','{Resources.Resource.home_hdr_contoff_img}')"" href=""sony-service-contacts.aspx""
                                title=""Go to the Contacts page to find phone numbers and emails for Sony support."">"))
                            ContactUsPH.Controls.Add(New LiteralControl($"<img height=""27"" src=""{Resources.Resource.home_hdr_contoff_img}"" border=""0"" name=""ContactUs_btn"" alt=""Contact Us"">"))
                            ContactUsPH.Controls.Add(New LiteralControl("</a>"))
                            'ContactUsPH.Controls.Add(New LiteralControl("</td>" + vbCrLf))
                        End If
                    Else
                        'PromosPH.Controls.Add(New LiteralControl("<td>"))
                        PromosPH.Controls.Add(New LiteralControl("<img height=""27"" src=""images/sp_int_topRight_fade_black.gif"" width=""59"">"))
                        'PromosPH.Controls.Add(New LiteralControl("</td>"))
                        'HelpPH.Controls.Add(New LiteralControl("<td>"))
                        HelpPH.Controls.Add(New LiteralControl("<img height=""27"" src=""images/sp_int_topRight_fade_black.gif"" width=""52"">"))
                        'HelpPH.Controls.Add(New LiteralControl("</td>"))
                        'ContactUsPH.Controls.Add(New LiteralControl("<td>"))
                        ContactUsPH.Controls.Add(New LiteralControl("<img height=""27"" src=""images/sp_int_topRight_fade_black.gif"" width=""68"">"))
                        'ContactUsPH.Controls.Add(New LiteralControl("</td>"))
                    End If

                End If

                'null space to match the top header's width
                If HttpContextManager.Customer Is Nothing Then
                    'LogoutPH.Controls.Add(New LiteralControl("<td>" + vbCrLf))
                    LogoutPH.Controls.Add(New LiteralControl($"<a onmouseover=""imageSwap('login_btn','{Resources.Resource.home_hdr_lgnon_img}')""
                        onmouseout=""imageSwap('login_btn','{Resources.Resource.home_hdr_lngoff_img}')"" href=""{signinStr}"" title=""Go to the Login page to log into your ServicesPlus account."">"))

                    LogoutPH.Controls.Add(New LiteralControl($"<img height=""27"" src=""{Resources.Resource.home_hdr_lngoff_img}"" border=""0"" name=""login_btn"" alt=""Log in"">"))
                    LogoutPH.Controls.Add(New LiteralControl("</a>"))
                    'LogoutPH.Controls.Add(New LiteralControl("</td>" + vbCrLf))
                End If

                If isCanadaSite = True Then
                    'MenuPH.Controls.Add(New LiteralControl("<td valign=bottom height=50>" + vbCrLf))
                    'MenuPH.Controls.Add(New LiteralControl("<dl id='sample' class='dropdown' style='display: inline-block; vertical-align: top; margin: 5px;'><dt><a href='#'><span>" + changeLang + "</span></a></dt><dd><ul><li><a href='#'>English<span class='value'>en-US</span></a></li><li><a href='#'>Fran�ais<span class='value'>fr-CA</span></a></li></ul></dd></dl>"))
                    'MenuPH.Controls.Add(New LiteralControl("</td>" + vbCrLf))
                End If

            Catch ex As Exception
                'Response.Write(ex.Message())
                'Dim util = New ServicesPlusException.Utilities()
                Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub
#End Region


#Region "   HELPER FUNCTIONS   "
        Private Sub SetActivePageIcon()
            Select Case callingPage
                Case enumPageIconToDisplay.MyCatalog.ToString()
                    _activePageIcon = enumPageIconToDisplay.MyCatalog
                Case enumPageIconToDisplay.ViewCart.ToString()
                    _activePageIcon = enumPageIconToDisplay.ViewCart
                Case enumPageIconToDisplay.MyOrder.ToString()
                    _activePageIcon = enumPageIconToDisplay.MyOrder
                Case enumPageIconToDisplay.MyProfile.ToString()
                    _activePageIcon = enumPageIconToDisplay.MyProfile
                'Case enumPageIconToDisplay.Logout.ToString()
                '    _activePageIcon = enumPageIconToDisplay.Logout
                Case enumPageIconToDisplay.ContactUs.ToString()
                    _activePageIcon = enumPageIconToDisplay.ContactUs
                Case enumPageIconToDisplay.Help.ToString()
                    _activePageIcon = enumPageIconToDisplay.Help
                Case enumPageIconToDisplay.Promos.ToString()
                    _activePageIcon = enumPageIconToDisplay.Promos
                Case Else
                    _activePageIcon = enumPageIconToDisplay.NoPage
            End Select
        End Sub

#End Region


#Region "   PROPERTIES   "
        Public Property ActivePageIcon() As enumPageIconToDisplay
            Get
                Return _activePageIcon
            End Get
            Set(ByVal Value As enumPageIconToDisplay)
                _activePageIcon = Value
            End Set
        End Property

        Public Property ShowDefaultIcon() As Boolean
            Get
                Return _showdefaultIcon
            End Get
            Set(ByVal Value As Boolean)
                _showdefaultIcon = Value
            End Set
        End Property
#End Region

    End Class
End Namespace
