Imports Sony.US.ServicesPLUS.Data
Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Process


Namespace SIAMAdmin

Public Interface IAccountPayment
    Property Order() As Order
        Function Save(Optional ByRef customer As Customer = Nothing) As Boolean
End Interface

End Namespace
