﻿Imports Microsoft.VisualBasic
Imports System

Public Class DIResponse

    Public Enum DIStatus
        Tokenized = 1
        Failed = 2
    End Enum

    Public Enum CreditCardTypes
        DinersClub = 1000
        BankCard = 2000
        AmericanExpress = 3000
        Visa = 4000
        Mastercard = 5000
        Discover = 6000
        JCB = 7000
        Laser = 8000
        Maestro = 9000
        Solo = 10000
        Switch = 11000
    End Enum

    Private _Status As DIStatus
    Private _XiPayNetTransactionID As String
    Private _MerchantReferenceCode As String
    Private _AuthCode As String
    Private _Token As String
    Private _ExpirationMonth As String
    Private _ExpirationYear As String
    Private _CVV As String
    Private _CreditCardType As String
    Private _BinRangeType As String
    Private _Amount As String

    Public Sub New(ByVal ReturnPayload As String)
        Dim xr As System.Xml.XmlReader
        Dim ms As New System.IO.MemoryStream(System.Text.Encoding.Default.GetBytes(ReturnPayload))
        xr = System.Xml.XmlReader.Create(ms)
        xr.ReadStartElement()
        '' read the 
        If xr.ReadElementContentAsString() = "Tokenized" Then
            _Status = DIStatus.Tokenized
        Else
            _Status = DIStatus.Failed
        End If
        '' read the xipay net transaction id value
        _XiPayNetTransactionID = xr.ReadElementContentAsString()
        '' read the merchant reference code value. This will be the value that you specified that will reference your transaction in your system.
        _MerchantReferenceCode = xr.ReadElementContentAsString()
        '' the Authocde. This currently will be 0, however this has been put here for future growth of DI.
        _AuthCode = xr.ReadElementContentAsString()
        '' read the CardInfo Node.
        xr.Read()
        '' read the token value.
        _Token = xr.ReadElementContentAsString()
        '' read the expiration date tag
        xr.Read()
        '' read the exp month
        _ExpirationMonth = xr.ReadElementContentAsString()
        '' read the exp year
        _ExpirationYear = xr.ReadElementContentAsString()
        '' read the end tag for the expiration date
        xr.ReadEndElement()
        '' read the cvv.
        _CVV = xr.ReadElementContentAsString()
        '' read the credit card type that was selected in the drop down. If the drop down was hidden it will default to the first selected or whatever was passed in through the XML
        _CreditCardType = GetCreditCardString(xr.ReadElementContentAsString())
        '' this is the credit card type according to the bin range.
        _BinRangeType = GetCreditCardString(xr.ReadElementContentAsString())
        '' read the cardinfo end tag
        xr.ReadEndElement()
        '' read the amount
        _Amount = xr.ReadElementContentAsString()
        xr.Close()
    End Sub

    Private Function GetCreditCardString(ByVal CCTyp As CreditCardTypes) As String
        Select Case CCTyp
            Case CreditCardTypes.AmericanExpress
                Return "AMEX"
            Case CreditCardTypes.Mastercard
                Return "MasterCard"
            Case CreditCardTypes.Visa
                Return "Visa"
            Case CreditCardTypes.Discover
                Return "Discover"
            Case Else
                Return Nothing
        End Select
    End Function

    Public ReadOnly Property Status() As DIStatus
        Get
            Return _Status
        End Get
    End Property

    Public ReadOnly Property XiPayNetTransactionID() As String
        Get
            Return _XiPayNetTransactionID
        End Get
    End Property

    Public ReadOnly Property MerchantReferenceCode() As String
        Get
            Return _MerchantReferenceCode
        End Get
    End Property

    Public ReadOnly Property AuthCode() As String
        Get
            Return _AuthCode
        End Get
    End Property

    Public ReadOnly Property Token() As String
        Get
            Return _Token
        End Get
    End Property

    Public ReadOnly Property ExpirationMonth() As String
        Get
            Return _ExpirationMonth
        End Get
    End Property

    Public ReadOnly Property ExpirationYear() As String
        Get
            Return _ExpirationYear
        End Get
    End Property

    Public ReadOnly Property CVV() As String
        Get
            Return _CVV
        End Get
    End Property

    Public ReadOnly Property CreditCardType() As String
        Get
            Return _CreditCardType
        End Get
    End Property
End Class
