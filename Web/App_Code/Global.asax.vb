Imports Microsoft.Win32
Imports System.Data
Imports System.Threading

Imports Sony.US.AuditLog
Imports Sony.US.ServicesPLUS.Process
Imports Sony.US.SIAMUtilities
Imports ServicesPlusException
Imports System
Imports System.Configuration
Imports System.Collections.Specialized
Imports System.Web
Imports System.Web.SessionState

Namespace ServicePLUSWebApp

    Public Class [Global]
        Inherits System.Web.HttpApplication

#Region " Component Designer Generated Code "

        Public Sub New()
            MyBase.New()

            'This call is required by the Component Designer.
            InitializeComponent()

            'Add any initialization after the InitializeComponent() call

        End Sub

        'Required by the Component Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Component Designer
        'It can be modified using the Component Designer.
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            components = New System.ComponentModel.Container()
        End Sub

#End Region

        Sub Application_Start(ByVal sender As Object, ByVal e As EventArgs)
            'Load Configuration data from ServicesPLUS.XML file
            ConfigurationData.Initialize()

            'config_settings = handler.GetConfigSettings("//itemdname='SecurityManagement']")
            Application("MinPwdLength") = ConfigurationData.GeneralSettings.SecurityManagement.MinPwdLength
            Application("MaxPwdLength") = ConfigurationData.GeneralSettings.SecurityManagement.MaxPwdLengthNonAccount
            Application("SIAMLogoutURL") = ConfigurationData.Environment.SIAMLogout.SIAMLogoutURL
            'If (Not Registry.LocalMachine.OpenSubKey("SOFTWARE\ServicesPLUS").GetValue("AppServer") Is Nothing) Then
            '    Application("AppServer") = Registry.LocalMachine.OpenSubKey("SOFTWARE\ServicesPLUS").GetValue("AppServer").ToString()
            'End If ' Fires when the application is started
            Dim regKey = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry64)
            If (regKey IsNot Nothing) Then
                Dim subKey = regKey.OpenSubKey("SOFTWARE\\ServicesPLUS")
                If (subKey IsNot Nothing) Then
                    Application("AppServer") = subKey.GetValue("AppServer").ToString()
                End If
            End If
            Dim appVersion = Reflection.Assembly.GetExecutingAssembly().GetName().Version
            Application.Add("Major", appVersion.Major.ToString())
            Application.Add("Minor", appVersion.Minor.ToString())
            Application.Add("Build", appVersion.Build.ToString())
            'Application.Add("Build", System.Reflection.Assembly.GetExecutingAssembly().GetName().Version().Build.ToString())
            Application.Add("VersionFull", appVersion.ToString())
            Application.Add("Version", appVersion.Major.ToString() & "." & appVersion.Minor.ToString() & "." & appVersion.Build.ToString())
            Application.Add("ApplicationName", System.Reflection.Assembly.GetExecutingAssembly().GetName().Name.ToString())

            ' ConfigurationManager.AppSettings.Item("Verify") = ConfigurationData.Environment.SiamLDAPAuthentication.VerifyCertificate

            'To process credit card revocation process store if any instance is running or not.
            Application.Add("RevocationInProcess", False)
            'Store Lock key, First and Second key in the Application so that it could access from any user login as Administrator in ServicesPLUS Admin
            Application.Add("RevocationKeyStructure", Nothing)

            Dim dsErrorSet As New DataSet
            Try
                Dim objProcessUtility As ProcessUtilities = New ProcessUtilities()
                dsErrorSet = objProcessUtility.GetSPSErrorList()
                Application.Add("ErrorList", dsErrorSet)
            Catch exGetErrorList As Exception
                ' Throw exGetErrorList
                'Dim objUtilties As Utilities = New Utilities
                Utilities.WrapExceptionforUI(exGetErrorList)
            End Try

            Dim objPCILogWriter As PCILogWriter = New PCILogWriter()
            Dim newThread As Thread
            newThread = New Thread(New ParameterizedThreadStart(AddressOf objPCILogWriter.StartLogging))
            newThread.Start()

        End Sub

        Sub Session_Start(ByVal sender As Object, ByVal e As EventArgs)
            ' Fires when the session is started   
            Try
                If TypeOf Context.Handler Is IRequiresSessionState OrElse TypeOf Context.Handler Is IReadOnlySessionState Then
                    If Request.IsSecureConnection Then
                        Request.Cookies("ASP.NET_SessionId").Secure = True
                    End If
                End If
            Catch ex As Exception
                ' do nothing
                ' VS 2005 had issues accessing the cookies when there is no session state
                ' so had to include the condition to test if the context.handler is of type that requires session state
                ' in other cases where no session is required, this may throw an exception which can be ignored
            End Try
        End Sub

        Function ReplaceSpecialCharacters(ByVal strPath As String) As String

            Dim strValue As String = ""
            Dim intIndex As Integer = 0
            Dim intIndexNew As Integer = 0

            If (strPath.Contains("+")) Then
                intIndex = strPath.IndexOf("+")
                intIndexNew = strPath.IndexOf("+", intIndex + 1)

                If intIndexNew = (intIndex + 3) Then
                    strValue = strPath.Substring(intIndex, 4)
                Else
                    Return strPath
                End If
            Else
                Return strPath
            End If

            Return strPath.Replace(strValue, ReturnCharacter(strValue))
        End Function

        Public Function ReturnCharacterSub(ByVal strSelect As String) As String
            Dim strCharacter As String = ""
            For Each strChar In strSelect.ToCharArray()
                Select Case strChar.ToString()
                    Case "~"
                        strCharacter = strSelect.Replace(strChar, "+01+")
                        Exit For
                    Case "/"
                        strCharacter = strSelect.Replace(strChar, "+02+")
                        Exit For
                    Case "{"
                        strCharacter = strSelect.Replace(strChar, "+03+")
                        Exit For
                    Case "}"
                        strCharacter = strSelect.Replace(strChar, "+04+")
                        Exit For
                    Case "|"
                        strCharacter = strSelect.Replace(strChar, "+05+")
                        Exit For
                    Case "\"
                        strCharacter = strSelect.Replace(strChar, "+06+")
                        Exit For
                    Case "^"
                        strCharacter = strSelect.Replace(strChar, "+07+")
                        Exit For
                    Case "["
                        strCharacter = strSelect.Replace(strChar, "+08+")
                        Exit For
                    Case "]"
                        strCharacter = strSelect.Replace(strChar, "+09+")
                        Exit For
                    Case "`"
                        strCharacter = strSelect.Replace(strChar, "+10+")
                        Exit For
                    Case "%"
                        strCharacter = strSelect.Replace(strChar, "+11+")
                        Exit For
                    Case "#"
                        strCharacter = strSelect.Replace(strChar, "+12+")
                        Exit For
                    Case "@"
                        strCharacter = strSelect.Replace(strChar, "+13+")
                        Exit For
                    Case "<"
                        strCharacter = strSelect.Replace(strChar, "+14+")
                        Exit For
                    Case ">"
                        strCharacter = strSelect.Replace(strChar, "+14+")
                        Exit For
                    Case "+"
                        strCharacter = strSelect.Replace(strChar, "+16+")
                        Exit For
                    Case Else
                        strCharacter = strSelect
                        Exit Select
                End Select
            Next

            Return strCharacter
        End Function

        Function ReturnCharacter(ByVal strSelect As String) As String
            Dim strCharacter As String = ""
            Select Case strSelect
                Case "+01+"
                    strCharacter = "~"
                Case "+02+"
                    strCharacter = "/"
                Case "+03+"
                    strCharacter = "{"
                Case "+04+"
                    strCharacter = "}"
                Case "+05+"
                    strCharacter = "|"
                Case "+06+"
                    strCharacter = "\"
                Case "+07+"
                    strCharacter = "^"
                Case "+08+"
                    strCharacter = "["
                Case "+09+"
                    strCharacter = "]"
                Case "+10+"
                    strCharacter = "`"
                Case "+11+"
                    strCharacter = "%"
                Case "+12+"
                    strCharacter = "#"
                Case "+13+"
                    strCharacter = "@"
                Case "+14+"
                    strCharacter = "<"
                Case "+15+"
                    strCharacter = ">"
                Case "+16+"
                    strCharacter = "+"
            End Select
            Return strCharacter
        End Function

        ' Fires at the beginning of each request
        Sub Application_BeginRequest(ByVal sender As Object, ByVal e As EventArgs)
            ' Check if the site is in maintenance mode and redirect them
            If ConfigurationData.GeneralSettings.UnderMaintenance = "Y" Then
                'If Me.GetType().Name.ToUpper <> "UNDERMAINTENANCE_ASPX" Then
                If Not HttpContext.Current.Request.Path.ToLower.Contains("undermaintenance") Then
                    Response.Redirect("UnderMaintenance.aspx")
                End If
            Else
                ' If the site is off maintenance mode and the user is at the maintenance page, send them to Home
                If Request.CurrentExecutionFilePath.Contains("UnderMaintenance.aspx") Then
                    Response.Redirect("default.aspx")
                End If
            End If

            ' Detect and redirect to the Error Handler page if required
            Dim oContext As HttpContext = HttpContext.Current
            Dim oldpath As String = oContext.Request.Path.ToLower()
            Dim strExtension As String = String.Empty
            Try
                strExtension = oldpath.Substring(oldpath.LastIndexOf("."), oldpath.Length - oldpath.LastIndexOf("."))
                If strExtension = ".xml" Or strExtension = ".xslt" Then
                    Response.Redirect("ErrorHandler.aspx", True)
                End If
            Catch ex As Exception

            End Try

            '---- to invoke URL re-writing for sony-parts-catalog.aspx ----'
            Dim token As String = "/page"
            Dim i As Integer = oldpath.IndexOf(token)
            Dim l As Integer = token.Length

            If (i <> -1) Then
                Dim j As Integer = oldpath.IndexOf(".aspx")

                If (j <> -1) Then
                    Dim id As String = oldpath.Substring(i + l, j - (i + l))
                    Dim newpath As String = oldpath.Replace(token + id + ".aspx", "/Sony_Parts_CatalogGroup.aspx?pageno=" + id)
                    oContext.RewritePath(newpath)
                    Exit Sub
                End If
            End If

            '---- to invoke URL re-writing for sony-parts-cataloggroup.aspx --------'
            token = "/groupid"
            i = oldpath.IndexOf(token)
            l = token.Length

            If (i <> -1) Then
                Dim j As Integer = oldpath.IndexOf(".aspx")

                If (j <> -1) Then
                    Dim id As String = oldpath.Substring(i + l, j - (i + l))
                    Dim newpath As String = oldpath.Replace(token + id + ".aspx", "/Sony-Parts-Catalog-Details.aspx?groupid=" + id)
                    oContext.RewritePath(newpath)
                    Exit Sub
                End If
            End If

            '----- to invoke the URL re-writing for Sony-Service-Repair-maintenance.aspx --------'
            '
            token = "/model-group-"
            i = oldpath.IndexOf(token)
            l = token.Length

            If (i <> -1) Then
                Dim j As Integer = oldpath.IndexOf(".aspx")

                If (j <> -1) Then
                    Dim id As String = oldpath.Substring(i + l, j - (i + l))
                    Dim newpath As String = oldpath.Replace(token + id + ".aspx", "/sony-repair-maintenance-service.aspx?groupid=" + id)
                    oContext.RewritePath(newpath)
                    Exit Sub
                End If
            End If

            '----- to invoke the URL re-writing for Sony-Service-Repair-maintenance.aspx --------'
            '
            token = "/sony-operation-manual-"
            i = oldpath.IndexOf(token)
            l = token.Length

            If (i <> -1) Then
                Dim j As Integer = oldpath.IndexOf(".aspx")

                If (j <> -1) Then
                    Dim id As String = oldpath.Substring(i + l, j - (i + l))
                    Dim newpath As String = oldpath.Replace(token + id + ".aspx", "/sony-operation-manual.aspx?model=" + If(id.IndexOf("/") >= 0, "", id))
                    oContext.RewritePath(newpath)
                    Exit Sub
                End If
            End If

            '---- to invoke URL re-writing for PartsPLUSResults.aspx --------'
            token = "-kit"
            i = oldpath.IndexOf(token)
            l = token.Length
            If (i <> -1) Then
                Dim strSplit() As String = oldpath.Split("/")
                Dim strKit As String = strSplit(Microsoft.VisualBasic.UBound(strSplit))
                Dim j As Integer = oldpath.IndexOf(".aspx")
                If (j <> -1) Then
                    strSplit = strKit.Split("-")
                    Dim id As String = strSplit(Microsoft.VisualBasic.LBound(strSplit))
                    Dim newpath As String = oldpath.Replace(strKit, "PartsPLUSResults.aspx?stype=kits&kit=" + id)
                    oContext.RewritePath(newpath)
                    Exit Sub
                End If
            End If

            oldpath = ReplaceSpecialCharacters(oldpath)
            token = "/sony-pg-service-maintenance-repair-"
            i = oldpath.IndexOf(token)
            l = token.Length

            If (i <> -1) Then
                Dim j As Integer = oldpath.IndexOf(".aspx")
                If (j <> -1) Then
                    Dim id As String = oldpath.Substring(i + l, j - (i + l))
                    Dim newpath As String = oldpath.Replace(token + id + ".aspx", "/sony-pg-service-maintenance-repair.aspx?thruSearchModel=True&model=" + id.ToUpper())
                    oContext.RewritePath(newpath)
                    Exit Sub
                End If
            End If

            token = "/sony-part-number-"
            i = oldpath.IndexOf(token)
            l = token.Length

            If (i <> -1) Then
                Dim j As Integer = oldpath.IndexOf(".aspx")
                If (j <> -1) Then
                    Dim catm As New Sony.US.ServicesPLUS.Process.CatalogManager
                    Dim id As String = oldpath.Substring(i + l, j - (i + l))
                    Dim newpath As String = oldpath.Replace(token + id + ".aspx", "/PartsPlusResults.aspx?stype=parts&partNumber=" + id)
                    oContext.RewritePath(newpath)
                    Exit Sub
                End If
            End If

            '----- to invoke the URL re-writing for sony-software-model.aspx --------'
            token = "/sony-software-model-"
            i = oldpath.IndexOf(token)
            l = token.Length

            If (i <> -1) Then
                Dim j As Integer = oldpath.IndexOf(".aspx")

                If (j <> -1) Then
                    Dim id As String = oldpath.Substring(i + l, j - (i + l))
                    Dim newpath As String = oldpath.Replace(token + id + ".aspx", "/sony-software-model.aspx?model=" + id.ToUpper())
                    oContext.RewritePath(newpath)
                    Exit Sub
                End If
            End If

            token = "/sony-software-part-"
            i = oldpath.IndexOf(token)
            l = token.Length

            If (i <> -1) Then
                Dim j As Integer = oldpath.IndexOf(".aspx")
                If (j <> -1) Then
                    Dim id As String = oldpath.Substring(i + l, j - (i + l))
                    Dim newpath As String = oldpath.Replace(token + id + ".aspx", "/sony-software-model.aspx?Partnumber=" + id.ToUpper())
                    oContext.RewritePath(newpath)
                    Exit Sub
                End If
            End If

            '----- to invoke the URL re-writing for sony-software.aspx --------'
            token = "/sony-software-"
            i = oldpath.IndexOf(token)
            If oldpath.IndexOf("sony-software-model.aspx") <> -1 Then
                i = -1
            End If
            l = token.Length

            If (i <> -1) And oldpath.IndexOf("/sony-software-model") = -1 Then
                Dim j As Integer = oldpath.IndexOf(".aspx")

                If (j <> -1) Then
                    Dim id As String = oldpath.Substring(i + l, j - (i + l))
                    If Not id.Contains("model-") Then
                        Dim newpath As String = oldpath.Replace(token + id + ".aspx", "/sony-software.aspx?Model=" + id.ToUpper())
                        oContext.RewritePath(newpath)
                        Exit Sub
                    End If
                End If
            End If

            oldpath = oContext.Request.Path
            token = "/Bulletin_"
            l = token.Length
            If (i <> -1) Then
                Dim newpath As String = oldpath.Replace(token, "/Downloads/techbulletin/")
                newpath = newpath.Replace("/ServicesPLUSWebApp", "")
                Dim strEncSessionID As String = newpath.Substring(1, newpath.LastIndexOf("/Downloads/techbulletin/") - 1) ' + 24, newpath.Length - (newpath.LastIndexOf("/Downloads/techbulletin/") + 24))
                Dim strFileSesionID As String = Encryption.DeCrypt(strEncSessionID, Encryption.PWDKey)
                Dim strHTTPCookie As String() = HttpContextManager.GetServerVariableValue("HTTP_COOKIE").Split(";")
                For Each strCookieValue As String In strHTTPCookie
                    If strCookieValue.IndexOf("ASP.NET_SessionId") >= 0 Then
                        strCookieValue = strCookieValue.Replace("ASP.NET_SessionId=", "").Trim()
                        If strFileSesionID <> strCookieValue Then
                            Server.Transfer("/SignIn.aspx", False)
                        Else
                            newpath = newpath.Replace(strEncSessionID + "/", "")
                            If oldpath.IndexOf("/ServicesPLUSWebApp") >= 0 Then
                                newpath = "/ServicesPLUSWebApp" + newpath
                            End If
                            oContext.RewritePath(newpath)
                            Exit Sub
                        End If
                    End If
                Next
                oContext.RewritePath("/NotAuthorized.aspx")
                Exit Sub

            End If
            'End If  'For software sublink
        End Sub

        Public Function GetServerVariableValue(ByVal Key As String) As String
            Dim loop1, loop2 As Integer
            Dim arr1(), arr2() As String
            Dim coll As NameValueCollection
            Dim value As String = String.Empty
            ' Load ServerVariable collection into NameValueCollection object.
            coll = Request.ServerVariables
            ' Get names of all keys into a string array.
            arr1 = coll.AllKeys
            For loop1 = 0 To arr1.GetUpperBound(0)
                If Key = arr1(loop1) Then
                    arr2 = coll.GetValues(loop1) ' Get all values under this key.
                    For loop2 = 0 To arr2.GetUpperBound(0)
                        value = Server.HtmlEncode(arr2(loop2))
                        Return value
                        Exit Function
                    Next loop2
                End If
            Next loop1
            Return value
        End Function

        Sub Application_AuthenticateRequest(ByVal sender As Object, ByVal e As EventArgs)
            ' Fires upon attempting to authenticate the use
        End Sub

        Sub Application_Error(ByVal sender As Object, ByVal e As EventArgs)
            ' Fires when an error occurs
            If ConfigurationManager.AppSettings("ShowError").ToString = "0" Then
                'Session.Add("lastAppError", Context.Error.InnerException)
                'Dim objUtilties As Utilities = New Utilities
                Dim sUnique As String = Utilities.GetUniqueKey()
                Dim oException As ServicesPLUSBaseException = New ServicesPLUSBaseException()
                oException.ErrorDescription = Context.Error.Message
                oException.MInnerException = Context.Error.InnerException
                Utilities.LogException(oException, "Application error", sUnique)
                Response.Redirect("ErrorHandler.aspx?ApplicationError=true&errorcode=" + sUnique, True)
            End If

        End Sub

        Sub Session_End(ByVal sender As Object, ByVal e As EventArgs)
            ''Fires when the session ends             
            ''Remove PunchOut Id from ApplicationObject and Update Active value in Database for this PunchOutSessionID
            'Dim HashObject As Hashtable = Application("punchout")
            'CXMLSesisonManager.RemovePunchOutSession(Session.SessionID, HashObject)
            'Application.Add("punchout", HashObject)

            'Session.Clear()
            'Session.RemoveAll()
        End Sub

        Sub Application_End(ByVal sender As Object, ByVal e As EventArgs)
            ' Fires when the application ends
        End Sub

        Private Sub Global_EndRequest(ByVal sender As Object, ByVal e As EventArgs) Handles Me.EndRequest

        End Sub
    End Class

End Namespace
