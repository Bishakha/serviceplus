﻿'Extra name space
Imports System.Drawing
Imports System.Drawing.Drawing2D
Imports System.Drawing.Imaging
Imports System.Drawing.Text

Namespace ServicePLUSWebApp
    Public Class RandomImage
        'Default Constructor 
        Public Sub New()
        End Sub
        'property
        Public ReadOnly Property Text() As String
            Get
                Return m_text
            End Get
        End Property
        Public ReadOnly Property Image() As Bitmap
            Get
                Return m_image
            End Get
        End Property
        Public ReadOnly Property Width() As Integer
            Get
                Return m_width
            End Get
        End Property
        Public ReadOnly Property Height() As Integer
            Get
                Return m_height
            End Get
        End Property
        'Private variable
        Private m_text As String
        Private m_width As Integer
        Private m_height As Integer
        Private m_image As Bitmap
        Private random As New Random()
        'Methods declaration
        Public Sub New(ByVal s As String, ByVal width As Integer, ByVal height As Integer)
            m_text = s
            SetDimensions(width, height)
            GenerateImage()
        End Sub
        Public Sub Dispose()
            GC.SuppressFinalize(Me)
            Dispose(True)
        End Sub
        Protected Overridable Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                m_image.Dispose()
            End If
        End Sub
        Private Sub SetDimensions(ByVal width As Integer, ByVal height As Integer)
            If width <= 0 Then
                Throw New ArgumentOutOfRangeException("width", width, "Argument out of range, must be greater than zero.")
            End If
            If height <= 0 Then
                Throw New ArgumentOutOfRangeException("height", height, "Argument out of range, must be greater than zero.")
            End If
            m_width = width
            m_height = height
        End Sub
        Private Sub GenerateImage()
            Dim bitmap As New Bitmap(m_width, m_height, PixelFormat.Format32bppArgb)
            Dim g As Graphics = Graphics.FromImage(bitmap)
            g.SmoothingMode = SmoothingMode.AntiAlias
            Dim rect As New Rectangle(0, 0, m_width, m_height)
            Dim hatchBrush As New HatchBrush(HatchStyle.SmallConfetti, Color.LightGray, Color.White)
            g.FillRectangle(hatchBrush, rect)
            Dim size As SizeF
            Dim fontSize As Single = rect.Height + 1
            Dim font As Font

            Do
                fontSize -= 1
                font = New Font(FontFamily.GenericSansSerif, fontSize, FontStyle.Bold)
                size = g.MeasureString(m_text, font)
            Loop While size.Width > rect.Width
            Dim format As New StringFormat()
            format.Alignment = StringAlignment.Center
            format.LineAlignment = StringAlignment.Center
            Dim path As New GraphicsPath()
            'path.AddString(this.text, font.FontFamily, (int) font.Style, 
            '    font.Size, rect, format);
            path.AddString(m_text, font.FontFamily, CInt(font.Style), 75, rect, format)
            Dim v As Single = 4.0F
            Dim points As PointF() = {New PointF(random.[Next](rect.Width) / v, random.[Next](rect.Height) / v), New PointF(rect.Width - random.[Next](rect.Width) / v, random.[Next](rect.Height) / v), New PointF(random.[Next](rect.Width) / v, rect.Height - random.[Next](rect.Height) / v), New PointF(rect.Width - random.[Next](rect.Width) / v, rect.Height - random.[Next](rect.Height) / v)}
            Dim matrix As New Matrix()
            matrix.Translate(0.0F, 0.0F)
            path.Warp(points, rect, matrix, WarpMode.Perspective, 0.0F)
            hatchBrush = New HatchBrush(HatchStyle.Percent10, Color.Black, Color.SkyBlue)
            g.FillPath(hatchBrush, path)
            Dim m As Integer = Math.Max(rect.Width, rect.Height)
            For i As Integer = 0 To CInt(Math.Truncate(rect.Width * rect.Height / 30.0F)) - 1
                Dim x As Integer = random.[Next](rect.Width)
                Dim y As Integer = random.[Next](rect.Height)
                Dim w As Integer = random.[Next](m \ 50)
                Dim h As Integer = random.[Next](m \ 50)
                g.FillEllipse(hatchBrush, x, y, w, h)
            Next
            font.Dispose()
            hatchBrush.Dispose()
            g.Dispose()
            m_image = bitmap
        End Sub
    End Class
End Namespace