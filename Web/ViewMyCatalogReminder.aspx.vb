Imports System
Imports System.IO
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Drawing
Imports System.Web
Imports System.Web.SessionState
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Process
Imports Sony.US.ServicesPLUS.Data
Imports Sony.US.siam
Imports System.Text
Imports ServicesPlusException


Namespace ServicePLUSWebApp


    Partial Class ViewMyCatalogReminder
        Inherits SSL



        '-- ASP.NET Geterated Code ------------------------------------------------------------------------
#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Protected WithEvents CloseWindow As System.Web.UI.WebControls.ImageButton


        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.        
            isProtectedPage(True)
            InitializeComponent()
            Page.ID = Request.Url.AbsolutePath
        End Sub

#End Region




        '-- page members ----------------------------------------------------------------------------------
#Region "Page Members"


        '-- page member arrays ------------------------------------------------------------------------------    
        'Private RecurrancePatternDDLItems() As String = {"One Time", "Weekly", "Monthly"}
        Private RecurrancePatternDDLItems(,) As String = {{CustomerCatalogItem.RecurrencePattern.OneTime.ToString(), CustomerCatalogItem.RecurrencePattern.OneTime}, {CustomerCatalogItem.RecurrencePattern.Weekly.ToString(), CustomerCatalogItem.RecurrencePattern.Weekly}, {CustomerCatalogItem.RecurrencePattern.Monthly.ToString(), CustomerCatalogItem.RecurrencePattern.Monthly}}
        Private SecondaryRecurrancePatternDDLItems0(,) As String = {{"", ""}}
        Private SecondaryRecurrancePatternDDLItems1(,) As String = {{CustomerCatalogItem.RecurrenceDay.Monday.ToString(), CustomerCatalogItem.RecurrenceDay.Monday}, {CustomerCatalogItem.RecurrenceDay.Tuesday.ToString(), CustomerCatalogItem.RecurrenceDay.Tuesday}, {CustomerCatalogItem.RecurrenceDay.Wednesday.ToString(), CustomerCatalogItem.RecurrenceDay.Wednesday}, {CustomerCatalogItem.RecurrenceDay.Thursday.ToString(), CustomerCatalogItem.RecurrenceDay.Thursday}, {CustomerCatalogItem.RecurrenceDay.Friday.ToString(), CustomerCatalogItem.RecurrenceDay.Friday}, {CustomerCatalogItem.RecurrenceDay.Saturday.ToString(), CustomerCatalogItem.RecurrenceDay.Saturday}, {CustomerCatalogItem.RecurrenceDay.Sunday.ToString(), CustomerCatalogItem.RecurrenceDay.Sunday}}
        Private SecondaryRecurrancePatternDDLItems2(,) As String = {{CustomerCatalogItem.RecurrenceDay.FirstOfMonth.ToString(), CustomerCatalogItem.RecurrenceDay.FirstOfMonth}, {CustomerCatalogItem.RecurrenceDay.MiddleOfMonth.ToString(), CustomerCatalogItem.RecurrenceDay.MiddleOfMonth}, {CustomerCatalogItem.RecurrenceDay.LastOfMonth.ToString(), CustomerCatalogItem.RecurrenceDay.LastOfMonth}}

        '-- page member constants --------------------------------------------------------------------
        Private Const conRedirectPage As String = "default.aspx"
        ''Dim objUtilties As Utilities = New Utilities


#End Region '-- end members




        '-- events ----------------------------------------------------------------------------------------
#Region "events"


        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load, RecurrancePatternDDL.SelectedIndexChanged
            'Put user code to initialize the page here
            processPage(sender, e)
        End Sub

        Private Sub AddReminder_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles AddReminder.Click
            processPage(sender, e)
        End Sub

#End Region '-- end events




        '-- methods ---------------------------------------------------------------------------------------
#Region "Methods"



        '-- page control method -----------------------------------------------------------------------
#Region "page control method"
        Private Sub processPage(ByVal sender As System.Object, ByVal e As System.EventArgs)

            If Session.Item("customer") Is Nothing Or Request.QueryString("seq") Is Nothing Or Session.Item("CatalogItems") Is Nothing Then
                refreshParentPage()
            Else

                Try
                    Dim thisCustomer As Customer = Session.Item("customer")
                    Dim callingControl As String = ""
                    Dim categorySeqNumber As Integer = 0

                    If IsNumeric(Request.QueryString("seq").ToString()) Then categorySeqNumber = Integer.Parse(Request.QueryString("seq"))
                    If Not sender.id Is Nothing Then callingControl = sender.id.ToString

                    messageLabel.Text = ""

                    Select Case callingControl
                        Case Page.ID.ToString()
                            If Not IsPostBack Then displayPage(thisCustomer, getCatalogItem(thisCustomer, categorySeqNumber, Session.Item("CatalogItems")))
                        Case AddReminder.ID.ToString()
                            If addNewReminder(thisCustomer, getCatalogItem(thisCustomer, categorySeqNumber, Session.Item("CatalogItems"))) Then
                                refreshParentPage()
                            End If
                            'Case CloseWindow.ID.ToString()
                            'If removeReminder(thisCustomer, getCatalogItem(thisCustomer, categorySeqNumber, Session.Item("CatalogItems"))) Then refreshParentPage()
                        Case RecurrancePatternDDL.ID.ToString, SecondaryRecurrancePatternDDL.ID.ToString
                            displayPage(thisCustomer, getCatalogItem(thisCustomer, categorySeqNumber, Session.Item("CatalogItems")))
                    End Select

                Catch ex As Exception
                    messageLabel.Text = Utilities.WrapExceptionforUI(ex)
                End Try
            End If
        End Sub

        Private Sub displayPage(ByRef thisCustomer As Customer, ByRef thisCCI As CustomerCatalogItem)
            populateRecurrancePattern(thisCCI.TheRecurrencePattern)
            populateSecondaryRecurrancePattern(thisCCI.TheRecurrenceDay)
            populateDateBox(thisCCI.ReminderStartDate)
        End Sub



#End Region



        '-- helper methods ----------------------------------------------------------------------------
#Region "Helpers"



        Private Sub refreshParentPage()
            Response.Write("<script language=""javascript"">top.opener.window.location.reload(); window.close();</script>")
        End Sub


        Private Function getCatalogItem(ByRef thisCustomer As Customer, ByVal categorySeqNumber As Integer, ByRef catalogItems As ArrayList) As CustomerCatalogItem
            Dim thisCatalogItem As New CustomerCatalogItem

            Try
                If Not thisCustomer Is Nothing And catalogItems.Count > categorySeqNumber Then
                    Dim thisItem As CustomerCatalogItem = CType(catalogItems.Item(categorySeqNumber), CustomerCatalogItem)
                    If Not thisItem Is Nothing Then thisCatalogItem = thisItem
                End If




            Catch ex As Exception
                messageLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try

            Return thisCatalogItem
        End Function
#End Region



        '-- add new reminder --------------------------------------------------------------------------
#Region "Add new reminder"

        Private Function addNewReminder(ByRef thisCustomer As Customer, ByRef thisCCI As CustomerCatalogItem) As Boolean
            Dim returnValue As Boolean = False
            Try
                If Not thisCustomer Is Nothing Then
                    Dim needToUpdateStartDate As Boolean = False
                    Dim needToUpdateRecurrenceDay As Boolean = False
                    Dim needToUpdateRecurrencePattern As Boolean = False

                    If Format(thisCCI.ReminderStartDate, "MM/dd/yyyy").ToString() <> ReminderDate.Text.ToString() Then
                        If ReminderDate.Text.Trim.Length = 0 Then
                            ReminderDate.Text = Format(Date.Now, "MM/dd/yyyy").ToString()
                        End If
                        needToUpdateStartDate = True
                    End If

                    If SecondaryRecurrancePatternDDL.SelectedValue.Trim.Length > 0 Then
                        If thisCCI.TheRecurrenceDay.ToString() <> SecondaryRecurrancePatternDDL.SelectedItem.Text.ToString() Then needToUpdateRecurrenceDay = True
                    End If

                    If thisCCI.TheRecurrencePattern.ToString() <> RecurrancePatternDDL.SelectedItem.Text.ToString() Then needToUpdateRecurrencePattern = True


                    If needToUpdateStartDate Or needToUpdateRecurrenceDay Or needToUpdateRecurrencePattern Then
                        If IsDate(ReminderDate.Text.ToString()) Then
                            '-- update one time reminder
                            If RecurrancePatternDDL.SelectedItem.Text.ToString() = CustomerCatalogItem.RecurrencePattern.OneTime.ToString() Then
                                If CType(ReminderDate.Text.ToString(), Date) > Date.Now Then
                                    thisCCI.SetOneTimeReminder(CType(ReminderDate.Text.ToString(), Date))
                                Else
                                    '-- reminder will never happen let user know
                                    messageLabel.Text = "Date must be in the future."
                                    ReminderDate.Text = ""
                                End If
                            End If

                            '-- update weekly reminder
                            If RecurrancePatternDDL.SelectedItem.Text.ToString() = CustomerCatalogItem.RecurrencePattern.Weekly.ToString() Then
                                thisCCI.SetWeeklyReminder(ReminderDate.Text.ToString(), SecondaryRecurrancePatternDDL.SelectedValue.ToString())
                            End If

                            '-- update Monthly reminder 
                            If RecurrancePatternDDL.SelectedItem.Text.ToString() = CustomerCatalogItem.RecurrencePattern.Monthly.ToString() Then
                                thisCCI.SetMonthlyReminder(ReminderDate.Text.ToString(), SecondaryRecurrancePatternDDL.SelectedValue.ToString())
                            End If
                        Else
                            'Reminder date is not a valid date
                            messageLabel.Text = "Reminder date must be a valid date."
                            ReminderDate.Text = ""
                        End If

                        If messageLabel.Text.Length = 0 Then
                            Dim thisCM As New CatalogManager
                            thisCM.UpdateMyCatalogItem(thisCCI)
                            returnValue = True
                        Else
                            returnValue = False
                        End If

                    End If
                End If




            Catch ex As Exception
                messageLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try


            Return returnValue
        End Function

#End Region



        '-- remove reminder ---------------------------------------------------------------------------
#Region "Remove Reminder"


        Private Function removeReminder(ByRef thisCustomer As Customer, ByRef thisCCI As CustomerCatalogItem) As Boolean
            Dim returnValue As Boolean = False
            Try
                thisCCI.RemoveReminder()

                Dim thisCM As New CatalogManager
                thisCM.UpdateMyCatalogItem(thisCCI)
                returnValue = True




            Catch ex As Exception
                messageLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try


            Return returnValue
        End Function

#End Region



        '-- populate form fields ----------------------------------------------------------------------
#Region "Populate Form Fields"

        '-- populate dropdown list ----------------------------------------------------------------
#Region "populate dropdown list"
        Private Sub populateSecondaryRecurrancePattern(ByVal thisDay As CustomerCatalogItem.RecurrenceDay)

            Try
                If ViewState.Item("lastDailyArray") Is Nothing Then ViewState.Item("lastDailyArray") = "-1"

                Select Case RecurrancePatternDDL.SelectedIndex
                    Case 0, -1
                        If ViewState.Item("lastDailyArray") <> "0" Then
                            SecondaryRecurrancePatternDDL.Items.Clear()
                            populateDDL(SecondaryRecurrancePatternDDL, SecondaryRecurrancePatternDDLItems0)
                            SecondaryRecurrancePatternDDL.Enabled = False
                            SecondaryRecurrancePatternDDL.Visible = False
                            ViewState.Item("lastDailyArray") = "0"
                        End If
                    Case 1
                        If ViewState.Item("lastDailyArray") <> "1" Then
                            SecondaryRecurrancePatternDDL.Items.Clear()
                            populateDDL(SecondaryRecurrancePatternDDL, SecondaryRecurrancePatternDDLItems1)

                            If Not SecondaryRecurrancePatternDDL.Items.FindByText(thisDay.ToString()) Is Nothing Then
                                SecondaryRecurrancePatternDDL.Items.FindByText(thisDay.ToString()).Selected = True
                            End If

                            SecondaryRecurrancePatternDDL.Enabled = True
                            SecondaryRecurrancePatternDDL.Visible = True

                            ViewState.Item("lastDailyArray") = "1"
                        End If
                    Case 2
                        If ViewState.Item("lastDailyArray") <> "2" Then
                            SecondaryRecurrancePatternDDL.Items.Clear()
                            populateDDL(SecondaryRecurrancePatternDDL, SecondaryRecurrancePatternDDLItems2)

                            If Not SecondaryRecurrancePatternDDL.Items.FindByText(thisDay.ToString()) Is Nothing Then
                                SecondaryRecurrancePatternDDL.Items.FindByText(thisDay.ToString()).Selected = True
                            End If
                            SecondaryRecurrancePatternDDL.Enabled = True
                            SecondaryRecurrancePatternDDL.Visible = True
                            ViewState.Item("lastDailyArray") = "2"
                        End If
                End Select





            Catch ex As Exception
                messageLabel.Text = Utilities.WrapExceptionforUI(ex)

            End Try

        End Sub

        Private Sub populateRecurrancePattern(ByVal thisPattern As CustomerCatalogItem.RecurrencePattern)
            Try
                If RecurrancePatternDDL.SelectedIndex = -1 Then
                    populateDDL(RecurrancePatternDDL, RecurrancePatternDDLItems)
                    If Not RecurrancePatternDDL.Items.FindByText(thisPattern.ToString()) Is Nothing Then
                        RecurrancePatternDDL.Items.FindByText(thisPattern.ToString()).Selected = True
                    End If
                End If




            Catch ex As Exception
                messageLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try

        End Sub

        Private Sub populateDDL(ByRef thisDropdownList As DropDownList, ByVal thisArray(,) As String)
            Dim x As Integer = 0
            Dim thisItem As ListItem

            While x < thisArray.GetLength(0)
                thisItem = New ListItem
                thisItem.Text = thisArray(x, 0).ToString()
                thisItem.Value = thisArray(x, 1).ToString
                thisDropdownList.Items.Add(thisItem)
                x = x + 1
            End While

        End Sub


#End Region


        '-- populate text boxes --------------------------------------------------------------------
#Region "populate text boxes"
        Private Sub populateDateBox(ByVal thisDate As Date)
            If ReminderDate.Text.Length = 0 And thisDate <> "01/01/1900" Then
                ReminderDate.Text = Format(CType(thisDate.ToString(), Date), "MM/dd/yyyy")
            End If
        End Sub
#End Region


#End Region



#End Region ' -- end methods


    End Class

End Namespace
