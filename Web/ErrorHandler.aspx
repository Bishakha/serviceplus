<%@ Reference Page="~/SSL.aspx" %>

<%@ Page Language="vb" AutoEventWireup="false" Inherits="ServicePLUSWebApp.ErrorHandler" EnableViewStateMac="true" CodeFile="ErrorHandler.aspx.vb" CodeFileBaseClass="ServicePLUSWebApp.SSL" %>

<%@ Register TagPrefix="ServicePLUSWebApp" TagName="ServicePlusNav" Src="ServicePlusNav.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="PersonalMessage" Src="PersonalMessage.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="TopSonyHeader" Src="TopSonyHeader.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="SonyFooter" Src="SonyFooter.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>ServicesPLUS - Customer Service</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
    <meta name="vs_defaultClientScript" content="JavaScript">
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link rel="stylesheet" href="includes/ServicesPLUS_style.css" type="text/css">
    <script language="javascript" src="includes/ServicesPLUS.js" type="text/javascript"></script>
    <script type="text/javascript" src="includes/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="includes/svcplus_globalization.js"></script>

</head>
<body bgcolor="#5d7180" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
    <form id="Form1" method="post" runat="server">
        <center>
            <table width="760" border="0" role="presentation">
                <tr>
                    <td width="25" background="images/sp_left_bkgd.gif">
                        <img src="images/spacer.gif" width="25" height="25" alt=""></td>
                    <td width="710" bgcolor="#ffffff">
                        <table width="710" border="0" role="presentation">
                            <tr>
                                <td>
                                    <ServicePLUSWebApp:TopSonyHeader ID="TopSonyHeader" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td height="23">
                                    <nav><ServicePLUSWebApp:ServicePlusNav ID="SonyNav" runat="server" /></nav>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="710" border="0" role="presentation">
                                        <tr>
                                            <td width="464" bgcolor="#363d45" height="57" background="images/sp_int_header_top_ServicesPLUS_onepix.gif"
                                                align="right" valign="top">
                                                <br />
                                                <h1 class="headerText">ServicesPLUS &nbsp;&nbsp;</h1>
                                            </td>
                                            <td bgcolor="#363d45" valign="top">
                                                <table width="246" border="0" role="presentation">
                                                    <tr>
                                                        <td colspan="3">
                                                            <img src="images/spacer.gif" width="246" height="24" alt=""></td>
                                                    </tr>
                                                    <tr>
                                                        <td width="5">&nbsp;</td>

                                                        <td width="236">
                                                            <span class="memberName">
                                                                <ServicePLUSWebApp:PersonalMessage ID="PersonalMessageDisplay" runat="server"></ServicePLUSWebApp:PersonalMessage>
                                                            </span>
                                                        </td>
                                                        <td width="5">&nbsp;</td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td bgcolor="#f2f5f8">
                                                <table width="464" border="0" role="presentation">
                                                    <tr height="29">
                                                        <td width="20">
                                                            <img src="images/spacer.gif" width="20" height="29" alt=""></td>
                                                        <td>
                                                            <img src="images/spacer.gif" width="20" height="29" alt=""></td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td bgcolor="#99a8b5">&nbsp;</td>
                                        </tr>
                                        <tr height="9">
                                            <td bgcolor="#f2f5f8">
                                                <img src="images/sp_int_header_btm_left_onepix.gif" width="464" height="9" alt=""></td>
                                            <td bgcolor="#99a8b5">
                                                <img src="images/sp_int_header_btm_right.gif" width="246" height="9" alt=""></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table border="0" role="presentation">
                                        <tr>
                                            <td colspan="3">
                                                <img src="images/spacer.gif" width="20" height="30" alt=""></td>
                                        </tr>
                                        <tr>
                                            <td width="20">
                                                <img src="images/spacer.gif" width="20" height="29" alt=""></td>
                                            <td>
                                                <span class="bodyCopyBold">
                                                    <p>
                                                        <asp:Label ID="MessageLabel" runat="server" CssClass="bodyCopyBold"></asp:Label>
                                                    </p>
                                                </span>
                                            </td>
                                            <td width="20">
                                                <img src="images/spacer.gif" width="20" height="29" alt=""></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>

                                <td>
                                    <footer><ServicePLUSWebApp:SonyFooter ID="SonyFooter1" runat="server" /></footer>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="25" background="images/sp_right_bkgd.gif">
                        <img src="images/spacer.gif" width="25" height="20" alt=""></td>
                </tr>
            </table>
        </center>
    </form>
</body>
</html>
