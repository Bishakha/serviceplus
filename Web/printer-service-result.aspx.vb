Imports System
Imports System.IO
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Drawing
Imports System.Web
Imports System.Web.SessionState
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Process
Imports Sony.US.ServicesPLUS.Data
Imports Sony.US.siam
Imports System.Text
Imports System.Text.RegularExpressions
Imports ServicesPlusException
Imports Sony.US.SIAMUtilities

Namespace ServicePLUSWebApp
    Partial Class PrinterServiceResult
        Inherits System.Web.UI.Page

        Dim oServiceInfo As New ServiceItemInfo
        Public bTaxFlag As Boolean
        Public bWarrantyFlag As Boolean
        Public sBillToAddr As String = ""
        Public sShipToAddr As String = ""
        Public sNotify As String
        Public sAccessories As String
        Public sProblemDescr As String
        Public bApprFlag As Boolean
        Public bShow As Boolean = True
        'Added on 04-June-07 [For managing the color]
        Public bColor As Boolean = False
        Public sModelName As String
        'Start Modification after Issue E793 reopen 
        Dim current_bill_to As New Account
        Dim current_ship_to As New SISAccount
        Public sPayerAccount As String = ""
        Public sShipToAccnt As String = ""
        Public sContactEmailAddr As String = ""
        Public isAccntHolder As Boolean = False
        'Start Modification after Issue E793 reopen 
        Public bContractFlag As Boolean = False 'Added for fixing Bug# 177
        Public sInclude As String = ""
        Public sServiceAddr As String = ""
        Public blnShipOnHold As Boolean = False
        Public strServicesRequestID As String = String.Empty
        Public strContactPersonName As String = "NEW"

        Public mDepotServiceCharge As String = ConfigurationData.GeneralSettings.RepairCosting.DepotServiceCharge   ' Default to US cost.
        Public sNavBackToService As String = ""  ' Code added for bug 5644
        'Dim objUtilties As Utilities = New Utilities


        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                Dim objAddress As New CustomerAddr()
                Dim bFax As String = ""
                Dim tempString As String = ""

                oServiceInfo = Session("snServiceInfo")
                If oServiceInfo Is Nothing Then
                    tblSessionOut.Visible = True
                    'Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "printDepotRequest", "<script language=""javascript"">javascript:document.getElementById('tblMain')).style.visibility = ""hidden"";</script>")
                    Exit Sub
                End If
                objAddress = oServiceInfo.BillToAddr
                bTaxFlag = oServiceInfo.isTaxExempt
                blnShipOnHold = oServiceInfo.IsProductShipOnHold
                If (oServiceInfo.ServicesRequestID <> 0) Then
                    strServicesRequestID = oServiceInfo.ServicesRequestID.ToString()
                End If
                strContactPersonName = oServiceInfo.ContactPersonName

                If Not Request.QueryString("model") Is Nothing Then
                    sModelName = Request.QueryString("model").Trim()
                    sInclude = "Please send your " + sModelName + " unit along with the following items to the Sony service center address below:"
                End If

                ' ASleight - 2018-03-23 - Adding per-country Depot Service Charge logic.
                If HttpContextManager.GlobalData.IsCanada Then
                    mDepotServiceCharge = ConfigurationData.GeneralSettings.RepairCosting.DepotServiceChargeForCanada
                Else
                    mDepotServiceCharge = ConfigurationData.GeneralSettings.RepairCosting.DepotServiceCharge
                End If


                If oServiceInfo.WarrantyType = "w" Or oServiceInfo.WarrantyType = "s" Then 'Modified for fixing Bug# 177
                    bWarrantyFlag = True
                Else
                    bWarrantyFlag = False
                End If

                'Start modification of fixing Bug# 177
                If oServiceInfo.WarrantyType = "c" Then
                    bContractFlag = True
                Else
                    bContractFlag = False
                End If
                'End modification of fixing Bug# 177

                'Start Modification after Issue E793 reopen 
                If bShow = True And Not Session("current_bill_to") Is Nothing Then
                    isAccntHolder = True
                    current_bill_to = Session("current_bill_to")
                    If Len(Trim(current_bill_to.AccountNumber)) > 0 Then
                        sPayerAccount = current_bill_to.AccountNumber
                    End If

                End If
                If Not Session("current_ship_to") Is Nothing Then
                    isAccntHolder = True
                    current_ship_to = Session("current_ship_to")
                    If Len(Trim(current_ship_to.AccountNumber)) > 0 Then
                        sShipToAccnt = current_ship_to.AccountNumber
                    End If

                End If
                'End Modification after Issue E793 reopen 

                If Len(Trim(objAddress.Company)) > 0 Then
                    sBillToAddr = objAddress.Company
                End If
                If Len(Trim(objAddress.AttnName)) > 0 Then
                    sBillToAddr = sBillToAddr + "<br/>" + objAddress.AttnName
                End If
                If Len(Trim(objAddress.Address1)) > 0 Then
                    sBillToAddr = sBillToAddr + "<br/>" + objAddress.Address1
                End If
                If Len(Trim(objAddress.Address2)) > 0 Then
                    sBillToAddr = sBillToAddr + "<br/>" + objAddress.Address2
                End If
                If Len(Trim(objAddress.Address3)) > 0 Then
                    sBillToAddr = sBillToAddr + "<br/>" + objAddress.Address3
                End If
                If Len(Trim(objAddress.Address4)) > 0 Then
                    sBillToAddr = sBillToAddr + "<br/>" + objAddress.Address4
                End If
                If Len(Trim(objAddress.City)) > 0 Then
                    sBillToAddr = sBillToAddr + "<br/>" + objAddress.City
                End If
                If Len(Trim(objAddress.State)) > 0 Then
                    sBillToAddr = sBillToAddr + ", " + objAddress.State
                End If

                If Len(Trim(objAddress.State)) > 0 Then
                    sBillToAddr = sBillToAddr + " " + objAddress.ZipCode
                End If
                If objAddress.Phone.Trim().Length > 3 Then
                    sBillToAddr = sBillToAddr + "<br/><br/>Phone: ("
                    tempString = objAddress.Phone
                    tempString = tempString.Insert(3, ")").Replace(")-", ") ")
                    sBillToAddr = sBillToAddr + tempString
                End If


                If objAddress.PhoneExtn.Trim().Length > 0 Then
                    sBillToAddr = sBillToAddr + "<br/> Extn: "
                    tempString = objAddress.PhoneExtn
                    'tempString = tempString.Insert(3, ")")
                    sBillToAddr = sBillToAddr + tempString
                End If
                If objAddress.Fax.Trim().Length > 3 Then
                    sBillToAddr = sBillToAddr + "<br/> FAX: ("
                    tempString = objAddress.Fax
                    tempString = tempString.Insert(3, ")").Replace(")-", ") ")
                    sBillToAddr = sBillToAddr + tempString
                End If 'If Len(Trim(objAddress.Fax)) > 0 Then
                '    bFax = objAddress.Fax
                'End If

                'sBillToAddr = sBillToAddr + "<br/>Ph:" + objAddress.Phone + "<br/> Extn:" + objAddress.PhoneExtn + "<br/> FAX:" + bFax

                objAddress = oServiceInfo.ShipToAddr

                If Len(Trim(objAddress.Company)) > 0 Then
                    sShipToAddr = objAddress.Company
                End If
                If Len(Trim(objAddress.AttnName)) > 0 Then
                    sShipToAddr = sShipToAddr + "<br/>" + objAddress.AttnName
                End If
                If Len(Trim(objAddress.Address1)) > 0 Then
                    sShipToAddr = sShipToAddr + "<br/>" + objAddress.Address1
                End If
                If Len(Trim(objAddress.Address2)) > 0 Then
                    sShipToAddr = sShipToAddr + "<br/>" + objAddress.Address2
                End If
                If Len(Trim(objAddress.Address3)) > 0 Then
                    sShipToAddr = sShipToAddr + "<br/>" + objAddress.Address3
                End If
                If Len(Trim(objAddress.Address4)) > 0 Then
                    sShipToAddr = sShipToAddr + "<br/>" + objAddress.Address4
                End If
                If Len(Trim(objAddress.City)) > 0 Then
                    sShipToAddr = sShipToAddr + "<br/>" + objAddress.City
                End If
                If Len(Trim(objAddress.State)) > 0 Then
                    sShipToAddr = sShipToAddr + ", " + objAddress.State
                End If

                If Len(Trim(objAddress.State)) > 0 Then
                    sShipToAddr = sShipToAddr + " " + objAddress.ZipCode
                End If
                If objAddress.Phone.Trim().Length > 3 Then
                    sShipToAddr = sShipToAddr + "<br/><br/>Phone: ("
                    tempString = objAddress.Phone
                    tempString = tempString.Insert(3, ")").Replace(")-", ") ")
                    sShipToAddr = sShipToAddr + tempString
                End If
                If objAddress.PhoneExtn.Trim().Length > 0 Then
                    sShipToAddr = sShipToAddr + "<br/> Extn: "
                    tempString = objAddress.PhoneExtn
                    'tempString = tempString.Insert(3, ")")
                    sShipToAddr = sShipToAddr + tempString
                End If
                If objAddress.Fax.Trim().Length > 3 Then
                    sShipToAddr = sShipToAddr + "<br/> FAX: ("
                    tempString = objAddress.Fax
                    tempString = tempString.Insert(3, ")").Replace(")-", ") ")
                    sShipToAddr = sShipToAddr + tempString
                End If


                'If Len(Trim(objAddress.Fax)) > 0 Then
                '    sFax = objAddress.Fax
                'End If

                'sShipToAddr = sShipToAddr + "<br/>Ph:" + objAddress.Phone + "<br/> Extn:" + objAddress.PhoneExtn + "<br/> FAX:" + sFax

                lblPONumber.Text = oServiceInfo.PONumber
                'lblPreapprAmt.Text = "$" + oServiceInfo.PreApproveAmount.ToString() 'Commented for fixing Bug# 610
                sProblemDescr = oServiceInfo.ProblemDescription
                lblSerialNo.Text = oServiceInfo.SerialNumber
                sAccessories = oServiceInfo.Accessories
                bApprFlag = oServiceInfo.isApproval

                'sNotify = "<br/><li>After we receive and evaluate your unit, we will contact you with an estimate. If you refuse the estimate, you will be &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;charged $135 for labor plus shipping and handling.</li>"
                'sNotify = sNotify + "<br/><li>If you have a problem associated with specific media, please include a media sample.</li>"

                ' check for Service Contract/Warranty.            

                If UCase(oServiceInfo.WarrantyType) = "C" Or UCase(oServiceInfo.WarrantyType) = "W" Then
                    bShow = False
                End If


                sNotify = "<tr><td><img src='https://www.servicesplus.sel.sony.com/images/checkbox.jpg' border=0 /></td><td><span class='bodyCopy'>This form.&nbsp;</span></td></tr>"
                If oServiceInfo.Accessories.ToString().Length > 0 Then
                    sNotify = sNotify + "<tr><td><img src='https://www.servicesplus.sel.sony.com/images/checkbox.jpg' border=0 /></td><td><span class='bodyCopy'>Accessories: " + oServiceInfo.Accessories.ToString() + "</span></td></tr>"
                End If
                If oServiceInfo.isTaxExempt = True And UCase(oServiceInfo.WarrantyType) = "O" Then
                    sNotify = sNotify + "<tr><td><img src='https://www.servicesplus.sel.sony.com/images/checkbox.jpg' border=0  /></td><td><span class='bodyCopy'>A copy of your tax-exempt certificate as proof of tax-exempt status.&nbsp;</span></td></tr>"
                End If
                'Commented by Chandra on 04-June-07
                'If oServiceInfo.isTaxExempt = True And UCase(oServiceInfo.WarrantyType) = "O" Then
                '    sNotify = "<tr><td><input type=image src='https://servicesplusstage.us.sony.biz/https://www.servicesplus.sel.sony.com/images/checkbox.jpg' border=0  style='position: absolute;z-index: 2;' /><span class='bodyCopy'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;A copy of your tax-exempt certificate as proof of tax-exempt status.&nbsp;</span></td></tr></table>"
                'End If




                If UCase(oServiceInfo.WarrantyType) = "W" Then
                    sNotify = sNotify + "<tr valign='top'><td><img src='https://www.servicesplus.sel.sony.com/images/checkbox.jpg' border=0 /></td><td><span class='bodyCopy'>A copy of your dated sales invoice if the unit was not purchased directly from Sony Electronics Inc. If, upon examination, it is confirmed that your unit qualifies for in-warranty repair, then no parts or labor charges will apply. If your unit does not qualify for in-warranty repair according to the terms and conditions of the applicable warranty, then a detailed repair quote will be given to you. All repairs are subject to the Terms & Conditions of the Limited Warranty.&nbsp;</span></td></tr>"
                    'Changed on June 04 2007 : Chandra
                    'sNotify = sNotify + "<tr></td><input type=image src='https://www.servicesplus.sel.sony.com/images/checkbox.jpg' border=0  style='position: absolute;' /><span class='bodyCopy'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;A copy of your dated sales invoice if the unit was not purchased directly from Sony Electronics Inc. If, upon examination, it &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;is confirmed that your unit qualifies for in-warranty repair, then no parts or labor charges will apply. If your unit does not &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;qualify for in-warranty repair according to the terms and conditions of the applicable warranty, then a detailed repair quote will be &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;given to you. All repairs are subject to the Terms & Conditions of the Limited Warranty.&nbsp;</span></td></tr></table>"
                End If

                'Added on 19-July-07 Chandra...
                sNotify = sNotify + "<tr valign='top'><td><img src='https://www.servicesplus.sel.sony.com/images/checkbox.jpg' border=0 /></td><td><span class='bodyCopy'>Any promotional coupons.&nbsp;</span></td></tr>"

                'If oServiceInfo.IsProductShipOnHold Then
                '    sNotify = sNotify + "<tr valign=""top""><td><img src=""https://www.servicesplus.sel.sony.com/images/checkbox.jpg"" border=""0"" /></td><td>" + vbCrLf + "Do not ship unit: Hold unit for pickup at service center." + vbCrLf + "&nbsp;</td></tr>"
                'End If

                'Added on 04-June-07 Chandra...
                If oServiceInfo.DepotCenterAddress.Length > 0 Then
                    sServiceAddr = "<b>Sony Service Center</b><br/>Sony Electronics Inc.<br/>" + oServiceInfo.DepotCenterAddress.ToString()
                End If

                'Start modification of fixing Bug# 177
                If oServiceInfo.ContractNumber.ToString().Length > 0 Then
                    lblContractNo.Text = oServiceInfo.ContractNumber.ToString()
                Else
                    lblContractNo.Text = "To be determined."
                End If
                'End modification of fixing Bug# 177

                If oServiceInfo.PaymentType = "1" Then
                    lblPaymentType.Text = "Sony Account"
                ElseIf oServiceInfo.PaymentType = "2" Then
                    lblPaymentType.Text = "Credit Card"
                Else
                    lblPaymentType.Text = "Check"
                End If

                'Start Modification after Issue E793 reopen 
                sContactEmailAddr = oServiceInfo.ContactEmailAddress
                'End Modification after Issue E793 reopen 

                If oServiceInfo.isPreventiveMaintenance = True Then
                    lblPreventive.Text = "Yes"
                Else
                    lblPreventive.Text = "No"
                End If
                If oServiceInfo.isIntermittentProblem = True Then
                    lblIntermittent.Text = "Yes"
                Else
                    lblIntermittent.Text = "No"
                End If
                ' Code added for bug 5644
                Dim strCurrentEnvironment As String = ConfigurationData.GeneralSettings.Environment.Trim
                If strCurrentEnvironment = "Development" Then
                    sNavBackToService = "https://spswebdev.am.sony.com/sony-repair.aspx"
                ElseIf strCurrentEnvironment = "QA" Then
                    sNavBackToService = "https://www.servicesplus-qa.sel.sony.com/sony-repair.aspx"
                ElseIf strCurrentEnvironment = "Staging" Then
                    sNavBackToService = "https://www.servicesplus-stage.sel.sony.com/sony-repair.aspx"
                ElseIf strCurrentEnvironment = "Production" Then
                    sNavBackToService = "https://www.servicesplus.sel.sony.com/sony-repair.aspx"
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub
        Protected Overrides Sub Render(ByVal writer As System.Web.UI.HtmlTextWriter)
            Dim sb As StringBuilder = New StringBuilder()
            Dim sw As StringWriter = New StringWriter(sb)
            Dim strRender As String
            Dim hWriter As New HtmlTextWriter(sw)
            MyBase.Render(hWriter)
            strRender = sb.ToString()
            writer.Write(strRender)
            strRender = strRender.Replace("<script language=javascript>PrintPage();</script>", "")
            'Try
            '    'Start of Modification for fixing Bug# 188
            '    If Not Request.QueryString("Email") Is Nothing Then
            '        'File.WriteAllText("C:\user.html", strRender)
            '        Dim cm As New CustomerManager
            '        'Start Modification for E792
            '        Dim customer As New Customer
            '        Dim toMailID As String = ""

            '        customer = Session.Item("customer")
            '        toMailID = customer.EmailAddress.ToString()

            '        cm.SendServiceConfirmationMail(strRender, toMailID, sContactEmailAddr, sServiceAddr)
            '        'End Modification for E792

            '        cm = Nothing

            '    End If
            '    'End of Modification for fixing Bug# 188
            'Catch ex As Exception
            '    Throw ex
            'End Try
        End Sub

        Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
            If tblSessionOut.Visible = True Then
                Page.ClientScript.RegisterStartupScript(Page.GetType(), "PrintPage", "<script language=javascript>function HideTable(){document.getElementsByName('tblMain')(0).style.visibility = 'hidden';}</script>")
            Else
                Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), "PrintPage", "<script language=javascript>PrintPage();function HideTable(){}</script>")
            End If

        End Sub

        Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
            ' check if Service Item info object in session, if yes remove
            'Session.Remove("snServiceInfo")
        End Sub
    End Class
End Namespace

