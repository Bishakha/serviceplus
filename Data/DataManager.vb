
Imports System.Xml
Imports Oracle.ManagedDataAccess.Client
Imports Oracle.ManagedDataAccess.Types
Imports Sony.US.SIAMUtilities
Imports ServicesPlusException
Imports System.Text

Public Class DataManager

    Protected dbConnection As OracleConnection
    Protected connString As String

    Public ReadOnly Property Context() As TransactionContext

    Public Sub New()
        'ConfigurationData.Initialize()
        connString = ConfigurationData.Environment.DataBase.ConnectionString
        dbConnection = New OracleConnection
        dbConnection.ConnectionString = connString
    End Sub

    Public Sub OpenConnection()
        If dbConnection.State = ConnectionState.Closed Then
            dbConnection.Open()
        End If
    End Sub

    Public Sub CloseConnection()
        If dbConnection.State = ConnectionState.Open Then
            dbConnection.Close()
        End If
    End Sub

    Public Function ExecuteCMD(ByRef command As OracleCommand, ByRef context As TransactionContext, Optional ByVal auto_close_connection As Boolean = True) As Integer
        Dim x As Integer

        command.CommandType = CommandType.StoredProcedure
        command.BindByName = True   ' This is required in the Oracle.ManagedDataAccess namespace. Keep this in mind if parameter issues pop up in places where this function isn't used.

        ' ASleight - Below loop does a bunch of parsing, then does nothing with the results? Commented out.
        'Dim objString As New StringBuilder()
        'Dim i As Integer = 0
        'While i < command.Parameters.Count
        '    Dim obj As OracleParameter = command.Parameters.Item(i)
        '    objString.Append(obj.ParameterName & ":" & obj.Value & vbNewLine)
        '    i += 1
        'End While

        'if no transactional context then manage the connection here with no transaction.
        If context Is Nothing Then
            OpenConnection()
            command.Connection = dbConnection
        Else
            command.Connection = context.NativeConnection
            command.Transaction = context.NativeTransaction
        End If


        'Try
        x = command.ExecuteNonQuery()
        'Catch ex As Exception     ' ASleight - I never understood this. It hides the stack trace leading into this method.
        '    Throw New ServicesPLUSOracleException(ERRORCODE.ora06550, ex)
        'End Try

        If context Is Nothing And auto_close_connection Then
            CloseConnection()
        End If

        Return x
    End Function

    Public Function ExecuteLTCMD(ByRef command As OracleCommand, ByRef context As TransactionContext, Optional ByVal auto_close_connection As Boolean = True) As Integer
        Dim objString As New StringBuilder()
        Dim i As Integer = 0

        command.CommandType = CommandType.StoredProcedure
        command.BindByName = True

        While i < command.Parameters.Count
            Dim obj As OracleParameter = command.Parameters.Item(i)
            objString.Append(obj.ParameterName & ":" & obj.Value & vbNewLine)
            i += 1
        End While


        'if no transactional context then manage the connection here with no transaction.
        If context Is Nothing Then
            OpenConnection()
            command.Connection = dbConnection
        Else
            command.Connection = context.NativeConnection
            command.Transaction = context.NativeTransaction
        End If

        Dim x As Integer

        Try
            x = command.ExecuteNonQuery()
        Catch ex As Exception
            Throw ex
        End Try

        If context Is Nothing And auto_close_connection Then
            CloseConnection()
        End If

        Return x
    End Function

    Public Function GetResults(ByRef cmd As OracleCommand) As XmlDocument
        Dim return_value As OracleClob
        Dim doc As New XmlDocument
        Dim error_node As XmlNode
        Dim outparam As New OracleParameter("retVal", OracleDbType.Clob, ParameterDirection.Output)

        cmd.Parameters.Add(outparam)
        cmd.BindByName = True   ' ASleight - This is REQUIRED in Oracle.ManagedDataAccess, or else parameters must be bound in the proper order.

        ExecuteCMD(cmd, Nothing, False)

        If CType(cmd.Parameters("retVal")?.Value, INullable).IsNull Then
            If Not CType(cmd.Parameters("errorCondition")?.Value, INullable).IsNull Then
                'error condition was set in procedure.
                ' ASleight - Sadly, this is another bit of code that does a great job of hiding exception details. Nothing is done with this string.
                Dim error_message = cmd.Parameters("errorCondition").Value.ToString()
            End If
            CloseConnection()
            Throw New ServicesPLUSOracleException(ERRORCODE.ora06550)
        Else
            return_value = cmd.Parameters("retVal").Value
            return_value.Seek(0, IO.SeekOrigin.Begin)

            'Try
            doc.Load(return_value)
            'Catch ex As Exception
            '    'Dim excp As String
            '    'excp = ex.Message()
            '    'excp = ex.StackTrace()  ' ...why?
            '    Throw New ServicesPLUSOracleException(ERRORCODE.ora06550, ex)
            'End Try

            CloseConnection()

            error_node = doc.SelectSingleNode("//ERROR")

            If error_node IsNot Nothing Then
                Throw New ServicesPLUSOracleException(ERRORCODE.ora06550)
            End If
        End If

        Return doc
    End Function

    Public Function GetResults_SearchServiceModels(ByRef cmd As OracleCommand) As XmlDocument
        Dim doc As New XmlDocument
        Dim error_node As XmlNode

        '2016-04-20 ASleight Below changed from .Output to fix recurring errors
        Dim outparam2 As New OracleParameter("rc_confmodels", OracleDbType.RefCursor, ParameterDirection.Output)
        cmd.Parameters.Add(outparam2)

        '2016-04-20 ASleight Below changed from .Output to fix recurring errors
        Dim outparam3 As New OracleParameter("rc_newmodels", OracleDbType.RefCursor, ParameterDirection.Output)
        cmd.Parameters.Add(outparam3)

        cmd.BindByName = True

        ExecuteCMD(cmd, Nothing, False)

        If CType(cmd.Parameters("rc_newmodels")?.Value, INullable).IsNull Then
            If Not CType(cmd.Parameters("errorCondition")?.Value, INullable).IsNull Then
                'error condition was set in procedure
                Dim error_message As String
                error_message = cmd.Parameters("errorCondition").Value  ' ASleight - Get the error message, and do nothing with it. Okay...
            End If
            CloseConnection()
            Throw New ServicesPLUSOracleException(ERRORCODE.ora06550)
        Else
            Dim myAdaptor As New OracleDataAdapter(cmd)
            Dim dataset As New DataSet
            Dim xml_data As String

            Try
                myAdaptor.Fill(dataset)
                xml_data = dataset.GetXml()
                doc.LoadXml(xml_data)
            Catch ex As Exception
                Throw New ServicesPLUSOracleException(ERRORCODE.ora06550, ex)
            End Try

            CloseConnection()

            error_node = doc.SelectSingleNode("//ERROR")

            If error_node Is Nothing Then
            Else
                Throw New ServicesPLUSOracleException(ERRORCODE.ora06550)
            End If

        End If

        Return doc
    End Function
    '''Modified By Sneha by adding optional parameter and looping against it
    Public Function GetResults(ByRef command As OracleCommand, ByVal auto_close_connection As Boolean, Optional ByRef lstConfModels As List(Of String) = Nothing) As DataSet
        command.CommandType = CommandType.StoredProcedure
        command.BindByName = True
        Dim da As New OracleDataAdapter(command)
        Dim ds As New DataSet

        Try
            OpenConnection()
            command.Connection = dbConnection
            da.Fill(ds)
            Dim doc As New XmlDocument
            Dim xml_data As String = ds.GetXml()
            doc.LoadXml(xml_data)
            Dim root As XmlElement = doc.DocumentElement
            Dim nodes As XmlNodeList = root.ChildNodes
            'loop for new models
            If lstConfModels IsNot Nothing Then
                For i As Integer = 0 To doc.GetElementsByTagName("OLD_MATERIAL").Count - 1
                    If nodes(i).Item("OLD_MATERIAL") IsNot Nothing Then
                        lstConfModels.Add(nodes(i).Item("OLD_MATERIAL").InnerText + ":" + nodes(i).Item("NEW_MATERIAL").InnerText)
                    End If
                Next
            End If

            Return ds
        Catch ex As Exception
            Throw New ServicesPLUSOracleException(ERRORCODE.ora06550, ex)
        Finally
            If auto_close_connection Then
                CloseConnection()
            End If
        End Try
    End Function

    Public Function GetResultsAsXML(ByRef command As OracleCommand, ByVal auto_close_connection As Boolean) As XmlDocument
        command.CommandType = CommandType.StoredProcedure
        command.BindByName = True
        Dim da As New OracleDataAdapter(command)
        Dim ds As New DataSet

        Try
            OpenConnection()
            command.Connection = dbConnection
            da.Fill(ds)
        Catch ex As Exception
            'Throw New Exception(ex.Message)
            Throw New ServicesPLUSOracleException(ERRORCODE.ora06550, ex)
        End Try

        If auto_close_connection Then
            CloseConnection()
        End If
        Dim xml_data As String = ds.GetXml()
        Dim doc As New XmlDocument
        doc.LoadXml(xml_data)
        Return doc
    End Function

    Public Function GetResults(ByVal sql As String) As String
        Dim doc As XmlDocument
        Dim output As String
        Dim cmd As OracleCommand = New OracleCommand With {
            .BindByName = True,
            .CommandType = CommandType.StoredProcedure,
            .CommandText = "executeQuery"
        }
        cmd.Parameters.Add("xSql", OracleDbType.Varchar2, 255).Value = sql

        Try
            doc = GetResults(cmd)
            output = doc.InnerXml
        Catch ex As Exception
            ' output = ex.Message
            Throw New ServicesPLUSOracleException(ERRORCODE.ora06550, ex)
        End Try

        Return output

    End Function

    Public Function ExecuteLiteral(ByVal sql As String) As String
        Dim cmd As OracleCommand = New OracleCommand
        Dim output As String
        cmd.CommandType = CommandType.Text
        cmd.CommandText = sql
        cmd.BindByName = True

        If dbConnection.State = ConnectionState.Closed Then
            dbConnection.Open()
            cmd.Connection = dbConnection
        End If

        Try
            cmd.ExecuteNonQuery()
            output = "Success"
        Catch ex As Exception
            output = ex.Message

            If Not ex.InnerException Is Nothing Then
                output = ex.InnerException.Message
            End If
            Throw ex
        End Try

        Return output
    End Function

    Public Function BeginTransaction() As TransactionContext
        Dim oracle_transaction As OracleTransaction

        If dbConnection.State = ConnectionState.Closed Then
            dbConnection.Open()
        End If

        oracle_transaction = dbConnection.BeginTransaction
        Dim context As New TransactionContext(dbConnection, oracle_transaction)
        Return context
    End Function

    Public Sub CommitTransaction(ByRef context As TransactionContext)
        context.CommitTransaction()
    End Sub

    Public Sub RollbackTransaction(ByRef context As TransactionContext)
        If context IsNot Nothing Then context.RollbackTransaction()
    End Sub

    Public Overridable Sub Store(ByRef data As Object, ByRef context As TransactionContext, Optional ByVal update As Boolean = False)

    End Sub

    Public Overridable Function Restore(ByVal identity As String) As Object
        Throw New NotImplementedException("DataManager.Restore() was called, but the function has no body.")
    End Function

    'Commented by Prasad because this method is not called any where in the application
    'DeleteInvalidSIAMUsers
    'Public Function DeleteInvalidSIAMUsers(ByVal UserID As String) As Boolean
    '    Dim command As New OracleCommand
    '    command.Connection = cn
    '    command.CommandType = CommandType.StoredProcedure
    '    command.CommandText = "DeleteInvalidSIAMUsers"
    '    command.Parameters.Add("xUserID", OracleDbType.Varchar2)
    '    command.Parameters("xUserID").Value = UserID
    '    Try
    '        oContext = BeginTransaction()
    '        command.Transaction = oContext.NativeTransaction
    '        ExecuteCMD(command, Nothing, False)
    '        'CommitTransaction(oContext)
    '        'cn.Close()
    '        Return True
    '    Catch ex As Exception
    '        'RollbackTransaction(oContext)
    '        'cn.Close()
    '        Return False
    '    End Try
    'End Function
End Class
