Imports System.Xml
Imports Sony.US.ServicesPLUS.Core
Imports Oracle.ManagedDataAccess.Client
Imports ServicesPlusException
Imports System.Globalization
Imports Oracle.ManagedDataAccess.Types

Public Class OrderDataManager
    Inherits DataManager

    ' Set up the US Culture to force proper number formatting from String to Double and Date values.
    Dim usCulture As New CultureInfo("en-US")

    Public Function Restore(ByVal identity As String, ByRef objGlobalData As GlobalData, Optional ByVal DoGetCustomer As Boolean = True, Optional ByVal IsInvoiceProcessorCall As Boolean = False) As Object
        Dim order As Order
        Dim cm As New CustomerDataManager
        Dim doc As XmlDocument
        Dim nodes As XmlNodeList
        Dim elem As XmlElement
        Dim cmd As New OracleCommand
        Dim potential_delimiter As String = "|"
        Dim find_params() As String
        Dim order_number As String
        Dim customer_siam_id As String = ""
        Dim credit_card_sequenceNumber As String = ""
        Dim customer_id As Long
        Dim isUserActive As Boolean
        Dim debugMessage As String = $"OrderDataManager.Restore - START at {Date.Now.ToShortTimeString}.{Environment.NewLine}"

        If IsInvoiceProcessorCall Then
            find_params = identity.Split(potential_delimiter.ToCharArray())
            cmd.CommandText = "FINDORDERFORINVOICEPROCESSOR"
            cmd.Parameters.Add("xOrderNumber", OracleDbType.Char)
            cmd.Parameters("xOrderNumber").Value = find_params.GetValue(0)
            cmd.Parameters.Add("xAuthCode", OracleDbType.Varchar2, 10)
            cmd.Parameters("xAuthCode").Value = find_params.GetValue(1)
            order_number = find_params.GetValue(0)
        ElseIf identity.IndexOf(potential_delimiter) > 0 Then  'hacker
            find_params = identity.Split(potential_delimiter.ToCharArray())
            cmd.CommandText = "findOrder"
            cmd.Parameters.Add("xOrderNumber", OracleDbType.Char)
            cmd.Parameters("xOrderNumber").Value = find_params.GetValue(0)
            cmd.Parameters.Add("xAuthCode", OracleDbType.Varchar2, 10)
            cmd.Parameters("xAuthCode").Value = find_params.GetValue(1)
            order_number = find_params.GetValue(0)
        Else
            cmd.CommandText = "getOrder"
            cmd.Parameters.Add("xOrderNumber", OracleDbType.Char)
            cmd.Parameters("xOrderNumber").Value = identity
            order_number = identity
        End If

        debugMessage &= $"- Calling GetResults with CommandText: {cmd.CommandText}, OrderID: {order_number}.{Environment.NewLine}"
        doc = GetResults(cmd)
        nodes = doc.DocumentElement.ChildNodes
        If nodes.Count = 0 Then Return Nothing
        order = New Order(order_number)

        For Each node As XmlNode In nodes
            order.SequenceNumber = node("SEQUENCENUMBER").InnerText

            elem = node("ORDERDATE")
            If elem IsNot Nothing Then order.OrderDate = Convert.ToDateTime(elem.InnerText, usCulture)
            'If (Not DateTime.TryParse(node("ORDERDATE").InnerText, order.OrderDate)) Then
            '    Utilities.LogMessages(String.Format("OrderMgr.Restore - Failed to parse Order Date. String = {0}, Culture = {1}", node("ORDERDATE").InnerText, CultureInfo.CurrentCulture.Name))
            '    Try
            '        order.OrderDate = Convert.ToDateTime(node("ORDERDATE").InnerText, usCulture)
            '    Catch ex As Exception
            '        Utilities.WrapException(ex)
            '    End Try
            'End If

            elem = node("ACTIVE")
            If elem IsNot Nothing Then
                isUserActive = IIf(elem.InnerText = "1", True, False)
            End If

            'for training orders, the cs admin can place orders
            elem = node("SIAMIDENTITY")
            If elem IsNot Nothing Then customer_siam_id = elem.InnerText

            elem = node("CUSTOMERID")
            If elem IsNot Nothing Then customer_id = Convert.ToInt64(elem.InnerText)

            elem = node("BILLINGONLY")
            If elem IsNot Nothing Then order.BillingOnly = Convert.ToInt32(elem.InnerText)

            elem = node("ALLOCATEDGRANDTOTAL")
            If elem IsNot Nothing Then order.AllocatedGrandTotal = Convert.ToDouble(elem.InnerText, usCulture)
            'order.AllocatedGrandTotal = node("ALLOCATEDGRANDTOTAL").InnerText
            'If (Not Double.TryParse(elem.InnerText, order.AllocatedGrandTotal)) Then
            '    Utilities.LogMessages(String.Format("OrderMgr.Restore - Failed to parse Grand Total. String = {0}, Culture = {1}", elem.InnerText, CultureInfo.CurrentCulture.Name))
            '    Try
            '        order.AllocatedGrandTotal = Convert.ToDouble(elem.InnerText, usNumberFormat)
            '    Catch ex As Exception
            '        Utilities.WrapException(ex)
            '    End Try
            'End If

            elem = node("ALLOCATEDTAXAMOUNT")
            If elem IsNot Nothing Then order.AllocatedTax = Convert.ToDouble(elem.InnerText, usCulture)
            'If (Not Double.TryParse(elem.InnerText, order.AllocatedTax)) Then
            '    Utilities.LogMessages(String.Format("OrderMgr.Restore - Failed to parse Tax Total. String = {0}, Culture = {1}", elem.InnerText, CultureInfo.CurrentCulture.Name))
            '    Try
            '        order.AllocatedTax = Convert.ToDouble(elem.InnerText, usNumberFormat)
            '    Catch ex As Exception
            '        Utilities.WrapException(ex)
            '    End Try
            'End If

            elem = node("ALLOCATEDSPECIALTAXAMOUNT")
            If elem IsNot Nothing Then order.AllocatedSpecialTax = Convert.ToDouble(elem.InnerText, usCulture)
            'If (Not Double.TryParse(elem.InnerText, order.AllocatedSpecialTax)) Then
            '    Utilities.LogMessages(String.Format("OrderMgr.Restore - Failed to parse Special Tax Total. String = {0}, Culture = {1}", elem.InnerText, CultureInfo.CurrentCulture.Name))
            '    Try
            '        order.AllocatedSpecialTax = Convert.ToDouble(elem.InnerText, usNumberFormat)
            '    Catch ex As Exception
            '        Utilities.WrapException(ex)
            '    End Try
            'End If

            elem = node("ALLOCATEDSHIPPINGAMOUNT")
            If elem IsNot Nothing Then order.AllocatedShipping = Convert.ToDouble(elem.InnerText, usCulture)
            'If (Not Double.TryParse(elem.InnerText, order.AllocatedShipping)) Then
            '    Utilities.LogMessages(String.Format("OrderMgr.Restore - Failed to parse Shipping Total. String = {0}, Culture = {1}", elem.InnerText, CultureInfo.CurrentCulture.Name))
            '    Try
            '        order.AllocatedShipping = Convert.ToDouble(elem.InnerText, usNumberFormat)
            '    Catch ex As Exception
            '        Utilities.WrapException(ex)
            '    End Try
            'End If

            elem = node("ALLOCATEDPARTSUBTOTAL")
            If elem IsNot Nothing Then order.AllocatedPartSubTotal = Convert.ToDouble(elem.InnerText, usCulture)
            'If (Not Double.TryParse(elem.InnerText, order.AllocatedPartSubTotal)) Then
            '    Utilities.LogMessages(String.Format("OrderMgr.Restore - Failed to parse Part Subtotal. String = {0}, Culture = {1}", elem.InnerText, CultureInfo.CurrentCulture.Name))
            '    Try
            '        order.AllocatedPartSubTotal = Convert.ToDouble(elem.InnerText, usNumberFormat)
            '    Catch ex As Exception
            '        Utilities.WrapException(ex)
            '    End Try
            'End If

            elem = node("ALT_TAX_CLASSIFICATION")
            If elem IsNot Nothing Then order.Alt_Tax_Classification = elem.InnerText

            elem = node("ENABLEDOCUMENTIDNUMBER")
            If elem IsNot Nothing Then order.EnableDocumentIDNumber = elem.InnerText

            elem = node("VERISIGNAUTHAUTHCODE")
            If elem IsNot Nothing Then order.VerisgnAuthorizationCode = elem.InnerText

            elem = node("SAPBILLTOACCOUNTNUMBER")
            If elem IsNot Nothing Then order.SAPBillToAccountNumber = elem.InnerText

            elem = node("SISSHIPTOACCOUNTNUMBER")
            If elem IsNot Nothing Then order.LegacyAccountNumber = elem.InnerText

            elem = node("CREDITCARDSEQUENCENUMBER")
            If elem IsNot Nothing Then credit_card_sequenceNumber = elem.InnerText

            elem = node("PONUMBER")
            If elem IsNot Nothing Then order.POReference = elem.InnerText

            elem = node("SHIPPINGMETHODID")
            If elem IsNot Nothing Then order.ShippingMethod = GetShippingMethod(elem.InnerText, objGlobalData)

            elem = node("BILLTONAME")
            If elem IsNot Nothing Then order.BillTo.Name = elem.InnerText

            elem = node("BILLTOSTREET1")
            If elem IsNot Nothing Then order.BillTo.Line1 = elem.InnerText

            elem = node("BILLTOSTREET2")
            If elem IsNot Nothing Then order.BillTo.Line2 = elem.InnerText

            elem = node("BILLTOSTREET3")
            If elem IsNot Nothing Then order.BillTo.Line3 = elem.InnerText

            elem = node("BILLTOSTREET4")
            If elem IsNot Nothing Then order.BillTo.Line4 = elem.InnerText

            elem = node("BILLTOCITY")
            If elem IsNot Nothing Then order.BillTo.City = elem.InnerText

            elem = node("BILLTOSTATE")
            If elem IsNot Nothing Then order.BillTo.State = elem.InnerText

            elem = node("BILLTOPOSTALCODE")
            If elem IsNot Nothing Then order.BillTo.PostalCode = elem.InnerText

            elem = node("BILLTOCOUNTRY")
            If elem IsNot Nothing Then order.BillTo.Country = elem.InnerText

            elem = node("BILLTOATTN")
            If elem IsNot Nothing Then order.BillTo.Attn = elem.InnerText

            elem = node("BILLTOPHONENUMBER")
            If elem IsNot Nothing Then order.BillTo.PhoneNumber = elem.InnerText

            elem = node("BILLTOPHONEEXTENSION")
            If elem IsNot Nothing Then order.BillTo.PhoneExt = elem.InnerText

            elem = node("SHIPTOATTN")
            If elem IsNot Nothing Then order.ShipTo.Attn = elem.InnerText

            elem = node("SHIPTONAME")
            If elem IsNot Nothing Then order.ShipTo.Name = elem.InnerText

            elem = node("SHIPTOSTREET1")
            If elem IsNot Nothing Then order.ShipTo.Line1 = elem.InnerText

            elem = node("SHIPTOSTREET2")
            If elem IsNot Nothing Then order.ShipTo.Line2 = elem.InnerText

            elem = node("SHIPTOSTREET3")
            If elem IsNot Nothing Then order.ShipTo.Line3 = elem.InnerText

            elem = node("SHIPTOSTREET4")
            If elem IsNot Nothing Then order.ShipTo.Line4 = elem.InnerText

            elem = node("SHIPTOCITY")
            If elem IsNot Nothing Then order.ShipTo.City = elem.InnerText

            elem = node("SHIPTOSTATE")
            If elem IsNot Nothing Then order.ShipTo.State = elem.InnerText

            elem = node("SHIPTOPOSTALCODE")
            If elem IsNot Nothing Then order.ShipTo.PostalCode = elem.InnerText

            elem = node("SHIPTOCOUNTRY")
            If elem IsNot Nothing Then order.ShipTo.Country = elem.InnerText

            elem = node("SHIPTOPHONENUMBER")
            If elem IsNot Nothing Then order.ShipTo.PhoneNumber = elem.InnerText

            elem = node("SHIPTOPHONEEXTENSION")
            If elem IsNot Nothing Then order.ShipTo.PhoneExt = elem.InnerText

            elem = node("FIRSTNAME")
            If elem IsNot Nothing Then order.FirstName = elem.InnerText

            elem = node("LASTNAME")
            If elem IsNot Nothing Then order.LastName = elem.InnerText

            order.Action = Order.ActionType.RESTORE
            debugMessage &= $"- Order populated. Grand Total: {order.AllocatedGrandTotal}, Order Sequence: {order.SequenceNumber}.{Environment.NewLine}"
        Next

        'fully restore customer 
        If customer_siam_id <> "" Then
            If DoGetCustomer = True Then order.Customer = cm.Restore(customer_siam_id)
            debugMessage &= $"- Customer SIAM ID was not blank. Provided SIAM ID: {customer_siam_id}, Fetched Customer LdapId: {order.Customer?.LdapID}.{Environment.NewLine}"
        Else
            ' order.Customer = cm.RestoreByID(customer_id)
            '  Throw New Exception("Customer Information not found")
            Throw New ServicesPLUSOracleException(ERRORCODE.ora06550)
        End If

        If order.Customer IsNot Nothing Then order.Customer.IsActive = isUserActive

        'associate original credit card
        If Not String.IsNullOrEmpty(credit_card_sequenceNumber) Then
            debugMessage &= $"- Restoring Credit Card. Sequence #: {credit_card_sequenceNumber}.{Environment.NewLine}"
            If order.Customer IsNot Nothing Then order.CreditCard = order.Customer.GetCreditCard(credit_card_sequenceNumber)
            If order.CreditCard Is Nothing Then
                Dim pmDataManager As New PaymentDataManager()
                order.CreditCard = pmDataManager.GetCustomerCreditCard(credit_card_sequenceNumber)
            End If
        End If

        'Utilities.LogDebug(debugMessage)   ' ASleight - Uncomment this to get debug statements, if having issues with this function.
        Return order
    End Function

    Public Function FindOrder(ByVal order_number As String, ByVal verisign_auth_code As String, ByRef objGlobalData As GlobalData, Optional ByVal IsInvoiceProcessorCall As Boolean = False) As Order
        Return Restore(order_number + "|" + verisign_auth_code, objGlobalData, True, IsInvoiceProcessorCall)
    End Function

    Public Overrides Sub Store(ByRef data As Object, ByRef context As TransactionContext, Optional ByVal update As Boolean = False)
        Dim debugString As String = Date.Now.ToShortTimeString + " - OrderDataManager.Store START" + Environment.NewLine    ' TODO: Remove debug statements when finished
        Try
            Dim order As Order = CType(data, Order)
            'Dim identity As Double
            Dim cmd As New OracleCommand

            'If order.CreditCard IsNot Nothing Then
            'Dim objPayment As New PaymentDataManager()
            'objPayment.Store(order.CreditCard, context)
            'End If

            If (Not update) Or (update And order.Dirty) Then
                If update Then
                    If order.Action = CoreObject.ActionType.DELETE Then
                        cmd.CommandText = "deleteOrder"
                    Else
                        cmd.CommandText = "updateOrder"
                    End If
                    cmd.Parameters.Add("xSequenceNumber", OracleDbType.Int32)
                    cmd.Parameters("xSequenceNumber").Value = IIf(String.IsNullOrEmpty(order.SequenceNumber), 0, Convert.ToInt32(order.SequenceNumber))
                Else
                    cmd.CommandText = "addOrder"
                    cmd.Parameters.Add("xhttp_x_forwarded_for", OracleDbType.Varchar2)
                    cmd.Parameters("xhttp_x_forwarded_for").Value = order.HTTP_X_Forward_For

                    cmd.Parameters.Add("xremote_addr", OracleDbType.Varchar2)
                    cmd.Parameters("xremote_addr").Value = order.RemoteADDR

                    cmd.Parameters.Add("xhttp_referer", OracleDbType.Varchar2)
                    cmd.Parameters("xhttp_referer").Value = order.HTTPReferer

                    cmd.Parameters.Add("xhttp_url", OracleDbType.Varchar2)
                    cmd.Parameters("xhttp_url").Value = order.HTTPURL

                    cmd.Parameters.Add("xhttp_user_agent", OracleDbType.Varchar2)
                    cmd.Parameters("xhttp_user_agent").Value = order.HTTPUserAgent

                    cmd.Parameters.Add("xtransactiontype", OracleDbType.Int16)
                    cmd.Parameters("xtransactiontype").Value = IIf(String.IsNullOrEmpty(order.TransactionType), 0, order.TransactionType)

                    cmd.Parameters.Add("xcustomersequencenumber", OracleDbType.Int32)
                    cmd.Parameters("xcustomersequencenumber").Value = IIf(String.IsNullOrEmpty(order.Customer.SequenceNumber), 0, Convert.ToInt32(order.Customer.SequenceNumber))
                End If

                cmd.Parameters.Add("xOrderNumber", OracleDbType.Varchar2, 50)
                cmd.Parameters("xOrderNumber").Value = order.OrderNumber
                cmd.Parameters.Add("xCustomerID", OracleDbType.Int32)
                cmd.Parameters("xCustomerID").Value = IIf(String.IsNullOrEmpty(order.Customer.CustomerID), "0", Convert.ToInt32(order.Customer.CustomerID, usCulture))
                cmd.Parameters.Add("xOrderDate", OracleDbType.Date)
                cmd.Parameters("xOrderDate").Value = order.OrderDate
                cmd.Parameters.Add("xAllocatedGrandTotal", OracleDbType.Decimal)
                cmd.Parameters("xAllocatedGrandTotal").Value = order.AllocatedGrandTotal
                cmd.Parameters.Add("xAllocatedPartSubTotal", OracleDbType.Decimal)
                cmd.Parameters("xAllocatedPartSubTotal").Value = order.AllocatedPartSubTotal
                cmd.Parameters.Add("xAllocatedTaxAmount", OracleDbType.Decimal)
                cmd.Parameters("xAllocatedTaxAmount").Value = order.AllocatedTax
                cmd.Parameters.Add("xAllocatedSpecialTaxAmount", OracleDbType.Decimal)
                cmd.Parameters("xAllocatedSpecialTaxAmount").Value = order.AllocatedSpecialTax
                cmd.Parameters.Add("xAllocatedShippingAmount", OracleDbType.Decimal)
                cmd.Parameters("xAllocatedShippingAmount").Value = order.AllocatedShipping
                cmd.Parameters.Add("xVerisignAuthAuthCode", OracleDbType.Varchar2, 50)
                cmd.Parameters("xVerisignAuthAuthCode").Value = order.VerisgnAuthorizationCode
                cmd.Parameters.Add("xSAPBillToAccountNumber", OracleDbType.Varchar2, 50)
                cmd.Parameters("xSAPBillToAccountNumber").Value = order.SAPBillToAccountNumber.TrimStart("0")
                cmd.Parameters.Add("xAlt_tax_classification", OracleDbType.Int32)
                If String.IsNullOrEmpty(order.Alt_Tax_Classification) Then
                    cmd.Parameters("xAlt_tax_classification").Value = Nothing
                Else
                    cmd.Parameters("xAlt_tax_classification").Value = Convert.ToInt32(order.Alt_Tax_Classification)
                End If
                'cmd.Parameters("xAlt_tax_classification").Value = IIf(String.IsNullOrEmpty(order.Alt_Tax_Classification), Nothing, Convert.ToInt32(order.Alt_Tax_Classification, usCulture))

                cmd.Parameters.Add("xCreditCardSequenceNumber", OracleDbType.Varchar2, 50)
                If order.CreditCard Is Nothing Then
                    cmd.Parameters("xCreditCardSequenceNumber").Value = ""
                Else
                    cmd.Parameters("xCreditCardSequenceNumber").Value = order.CreditCard.SequenceNumber
                End If
                'cmd.Parameters("xCreditCardSequenceNumber").Value = IIf(order.CreditCard Is Nothing, "", order.CreditCard.SequenceNumber)

                cmd.Parameters.Add("xPONumber", OracleDbType.Varchar2, 50)
                cmd.Parameters("xPONumber").Value = order.POReference
                cmd.Parameters.Add("xShippingMethodID", OracleDbType.Int32)
                cmd.Parameters("xShippingMethodID").Value = IIf(String.IsNullOrEmpty(order.ShippingMethod?.ShippingMethodCode), -1, Convert.ToInt32(order.ShippingMethod.ShippingMethodCode, usCulture))
                cmd.Parameters.Add("xBillToName", OracleDbType.Varchar2, 50)
                cmd.Parameters("xBillToName").Value = order.BillTo.Name
                cmd.Parameters.Add("xBillToSTreet1", OracleDbType.Varchar2, 50)
                cmd.Parameters("xBillToSTreet1").Value = order.BillTo.Line1
                cmd.Parameters.Add("xBillToSTreet2", OracleDbType.Varchar2, 50)
                cmd.Parameters("xBillToSTreet2").Value = order.BillTo.Line2
                cmd.Parameters.Add("xBillToSTreet3", OracleDbType.Varchar2, 50)
                cmd.Parameters("xBillToSTreet3").Value = order.BillTo.Line3
                cmd.Parameters.Add("xBillToCity", OracleDbType.Varchar2, 50)
                cmd.Parameters("xBillToCity").Value = order.BillTo.City
                cmd.Parameters.Add("xBillToState", OracleDbType.Varchar2, 50)
                cmd.Parameters("xBillToState").Value = order.BillTo.State
                cmd.Parameters.Add("xBillToPostalCode", OracleDbType.Varchar2, 50)
                cmd.Parameters("xBillToPostalCode").Value = order.BillTo.PostalCode
                cmd.Parameters.Add("xShipToName", OracleDbType.Varchar2, 50)
                cmd.Parameters("xShipToName").Value = order.ShipTo.Name
                cmd.Parameters.Add("xShipToStreet1", OracleDbType.Varchar2, 50)
                cmd.Parameters("xShipToStreet1").Value = order.ShipTo.Line1
                cmd.Parameters.Add("xShipToStreet2", OracleDbType.Varchar2, 50)
                cmd.Parameters("xShipToStreet2").Value = order.ShipTo.Line2
                cmd.Parameters.Add("xShipToStreet3", OracleDbType.Varchar2, 50)
                cmd.Parameters("xShipToStreet3").Value = order.ShipTo.Line3
                cmd.Parameters.Add("xShipToCity", OracleDbType.Varchar2, 50)
                cmd.Parameters("xShipToCity").Value = order.ShipTo.City
                cmd.Parameters.Add("xShipToState", OracleDbType.Varchar2, 50)
                cmd.Parameters("xShipToState").Value = order.ShipTo.State
                cmd.Parameters.Add("xShipToPostalCode", OracleDbType.Varchar2, 50)
                cmd.Parameters("xShipToPostalCode").Value = order.ShipTo.PostalCode

                cmd.Parameters.Add("xBILLTOATTN", OracleDbType.Varchar2, 50)
                cmd.Parameters("xBILLTOATTN").Value = IIf(String.IsNullOrEmpty(order.BillTo.Attn), "", order.BillTo.Attn)
                cmd.Parameters.Add("xSHIPTOATTN", OracleDbType.Varchar2, 50)
                cmd.Parameters("xSHIPTOATTN").Value = IIf(String.IsNullOrEmpty(order.ShipTo.Attn), "", order.ShipTo.Attn)

                cmd.Parameters.Add("xBILLTOPHONENUMBER", OracleDbType.Varchar2, 50)
                cmd.Parameters("xBILLTOPHONENUMBER").Value = order.BillTo.PhoneNumber
                cmd.Parameters.Add("xSHIPTOPHONENUMBER", OracleDbType.Varchar2, 50)
                cmd.Parameters("xSHIPTOPHONENUMBER").Value = order.ShipTo.PhoneNumber

                cmd.Parameters.Add("xBILLTOPHONEEXTENSION", OracleDbType.Varchar2, 50)
                cmd.Parameters("xBILLTOPHONEEXTENSION").Value = order.BillTo.PhoneExt
                cmd.Parameters.Add("xSHIPTOPHONEEXTENSION", OracleDbType.Varchar2, 50)
                cmd.Parameters("xSHIPTOPHONEEXTENSION").Value = order.ShipTo.PhoneExt

                cmd.Parameters.Add("xSAPSHIPTOACCOUNT", OracleDbType.Varchar2, 50)
                cmd.Parameters("xSAPSHIPTOACCOUNT").Value = order.ShipToAccount.SAPAccount.TrimStart("0"c)
                cmd.Parameters.Add("xMESSAGETYPE", OracleDbType.Varchar2, 50)
                cmd.Parameters("xMESSAGETYPE").Value = order.MessageType
                cmd.Parameters.Add("xMESSAGETEXT", OracleDbType.Varchar2, 50)
                cmd.Parameters("xMESSAGETEXT").Value = order.MessageText
                cmd.Parameters.Add("xORDERSTATUS", OracleDbType.Varchar2, 50)
                cmd.Parameters("xORDERSTATUS").Value = order.OrderStatus
                cmd.Parameters.Add("xPAYMENTCARDRESULTTEXT", OracleDbType.Varchar2, 50)
                cmd.Parameters("xPAYMENTCARDRESULTTEXT").Value = order.Paymentcardresulttext
                cmd.Parameters.Add("xREFERENCENUMBER", OracleDbType.Varchar2, 50)
                cmd.Parameters("xREFERENCENUMBER").Value = order.YourReference

                cmd.Parameters.Add("xENABLEDOCUMENTIDNUMBER", OracleDbType.Varchar2, 50)
                cmd.Parameters("xENABLEDOCUMENTIDNUMBER").Value = order.EnableDocumentIDNumber

                'Include country code to order 
                cmd.Parameters.Add("xBILLTOCOUNTRY", OracleDbType.Varchar2, 50)
                cmd.Parameters("xBILLTOCOUNTRY").Value = order.Customer.CountryCode 'order.BillTo.Country
                cmd.Parameters.Add("xSHIPTOCOUNTRY", OracleDbType.Varchar2, 50)
                cmd.Parameters("xSHIPTOCOUNTRY").Value = order.Customer.CountryCode 'order.ShipTo.Country
                debugString += " - All parameters added." + Environment.NewLine

                cmd.Parameters.Add(New OracleParameter("retVal", OracleDbType.Int64, ParameterDirection.Output))
                debugString += " - Calling ExecuteCMD." + Environment.NewLine
                ExecuteCMD(cmd, context)

                debugString += " ----- RetVal = " + If(cmd.Parameters("retVal")?.Value?.ToString(), "null") + Environment.NewLine
                If Not CType(cmd.Parameters("retVal")?.Value, INullable).IsNull Then
                    order.SequenceNumber = cmd.Parameters("retVal").Value.ToString()
                End If
            End If

            ' Store the Order's line items
            If Not update Then
                debugString += " - Calling StoreOrderDetail."
                For Each detail As OrderLineItem In order.LineItems1
                    StoreOrderDetail(detail, context)
                Next
            End If

            order.Action = CoreObject.ActionType.RESTORE
        Catch ex As Exception
            Utilities.LogMessages(debugString)
            Utilities.WrapExceptionforUI(ex)    ' TODO: Remove debug statements when finished
            Throw ex
        End Try
    End Sub

    Private Sub StoreOrderDetail(ByRef detail As OrderLineItem, ByVal context As TransactionContext)
        Dim cmd As New OracleCommand
        Dim debugString As String = $"OrderDataManager.StoreOrderDetail START at {Date.Now.ToShortTimeString}{Environment.NewLine}"     ' TODO: Remove debug statements when finished
        Try
            cmd.CommandText = "addOrderDetail"

            cmd.Parameters.Add("xOrderNumber", OracleDbType.Varchar2, 15)
            cmd.Parameters("xOrderNumber").Value = detail.Order.OrderNumber
            cmd.Parameters.Add("xPartNumber", OracleDbType.Varchar2, 20)
            'cmd.Parameters("xPartNumber").Value = detail.Product.PartNumber   ' ASleight - .PartNumber now holds the "originally searched" part in case of replacement.
            cmd.Parameters("xPartNumber").Value = detail.Product.ReplacementPartNumber
            'debugString += " - First batch of parameters added." + Environment.NewLine

            cmd.Parameters.Add("xSoftwareDownloadFileName", OracleDbType.Varchar2, 50)
            Select Case detail.Product.Type
                Case Product.ProductType.Certificate, Product.ProductType.OnlineTraining, Product.ProductType.ClassRoomTraining
                    cmd.Parameters("xSoftwareDownloadFileName").Value = "NA"
                Case Product.ProductType.Software, Product.ProductType.ComputerBasedTraining
                    cmd.Parameters("xSoftwareDownloadFileName").Value = detail.SoftwareDownloadFileName
                Case Else
                    cmd.Parameters("xSoftwareDownloadFileName").Value = "NA"
            End Select

            cmd.Parameters.Add("xDownloadOnly", OracleDbType.Int32)
            cmd.Parameters("xDownloadOnly").Value = detail.Downloadable

            cmd.Parameters.Add("xProductTypeID", OracleDbType.Int32)
            cmd.Parameters("xProductTypeID").Value = detail.Product.Type

            cmd.Parameters.Add("xMODEL_ID", OracleDbType.Varchar2, 15)
            cmd.Parameters.Add("xMODEL_DESCRIPTION", OracleDbType.Varchar2, 75)
            cmd.Parameters.Add("xVERSION", OracleDbType.Varchar2, 10)
            cmd.Parameters.Add("xFREE_OF_CHARGE", OracleDbType.Int32)
            cmd.Parameters.Add("xKIT_DESCRIPTION", OracleDbType.Varchar2, 30)
            'debugString += " - Second batch of parameters added." + Environment.NewLine

            If detail.Product.SoftwareItem IsNot Nothing Then
                cmd.Parameters("xMODEL_ID").Value = IIf(detail.Product.SoftwareItem.ModelNumber Is Nothing, "", detail.Product.SoftwareItem.ModelNumber)
                cmd.Parameters("xMODEL_DESCRIPTION").Value = IIf(detail.Product.SoftwareItem.ModelDescription Is Nothing, "", detail.Product.SoftwareItem.ModelDescription)
                cmd.Parameters("xVERSION").Value = IIf(detail.Product.SoftwareItem.Version Is Nothing, "", detail.Product.SoftwareItem.Version)
                cmd.Parameters("xFREE_OF_CHARGE").Value = detail.Product.SoftwareItem.FreeOfCharge
                cmd.Parameters("xKIT_DESCRIPTION").Value = IIf(detail.Product.SoftwareItem.KitDescription Is Nothing, "", detail.Product.SoftwareItem.KitDescription)
            Else
                cmd.Parameters("xMODEL_ID").Value = ""
                cmd.Parameters("xMODEL_DESCRIPTION").Value = ""
                cmd.Parameters("xVERSION").Value = ""
                cmd.Parameters("xFREE_OF_CHARGE").Value = 0
                cmd.Parameters("xKIT_DESCRIPTION").Value = ""
            End If
            'debugString += " - Third batch of parameters added." + Environment.NewLine

            If detail.Product.Type = Product.ProductType.ExtendedWarranty Then
                If CType(detail.Product, ExtendedWarrantyModel).EWModelNumber <> String.Empty Then
                    cmd.Parameters.Add("xEWModelName", OracleDbType.Varchar2, 30)
                    cmd.Parameters("xEWModelName").Value() = CType(detail.Product, ExtendedWarrantyModel).EWModelNumber

                    cmd.Parameters.Add("xEWSerialNumber", OracleDbType.Varchar2, 30)
                    cmd.Parameters("xEWSerialNumber").Value() = CType(detail.Product, ExtendedWarrantyModel).EWSerialNumber

                    cmd.Parameters.Add("xEWPurchaseDate", OracleDbType.Date)
                    cmd.Parameters("xEWPurchaseDate").Value() = CType(detail.Product, ExtendedWarrantyModel).EWPurchaseDate

                    cmd.Parameters("xMODEL_DESCRIPTION").Value() = CType(detail.Product, ExtendedWarrantyModel).Description
                Else
                    If Not detail.Product.SoftwareItem Is Nothing Then
                        cmd.Parameters.Add("xEWModelName", OracleDbType.Varchar2, 30)
                        cmd.Parameters("xEWModelName").Value() = CType(detail.Product.SoftwareItem, ExtendedWarrantyModel).EWModelNumber

                        cmd.Parameters.Add("xEWSerialNumber", OracleDbType.Varchar2, 30)
                        cmd.Parameters("xEWSerialNumber").Value() = CType(detail.Product.SoftwareItem, ExtendedWarrantyModel).EWSerialNumber

                        cmd.Parameters.Add("xEWPurchaseDate", OracleDbType.Date)
                        cmd.Parameters("xEWPurchaseDate").Value() = CType(detail.Product.SoftwareItem, ExtendedWarrantyModel).EWPurchaseDate

                        cmd.Parameters("xMODEL_DESCRIPTION").Value() = CType(detail.Product.SoftwareItem, ExtendedWarrantyModel).Description
                    End If
                End If
                cmd.Parameters.Add("xEWGUID", OracleDbType.Varchar2, 36)
                CType(detail.Product, ExtendedWarrantyModel).CertificateGUID = System.Guid.NewGuid().ToString()
                cmd.Parameters("xEWGUID").Value() = CType(detail.Product, ExtendedWarrantyModel).CertificateGUID
                debugString += " - Extended Warranty parameters added." + Environment.NewLine
            End If

            cmd.Parameters.Add("xQuantity", OracleDbType.Int32)
            cmd.Parameters("xQuantity").Value = detail.Quantity
            cmd.Parameters.Add("xitem_price", OracleDbType.Decimal)
            'cmd.Parameters("xitem_price").Value = IIf(String.IsNullOrEmpty(detail.Product.ListPrice), 0, (detail.Product.ListPrice / detail.Quantity))
            cmd.Parameters("xitem_price").Value = (detail.Product.ListPrice / detail.Quantity)
            cmd.Parameters.Add("xtotal_line_item_price", OracleDbType.Decimal)
            'cmd.Parameters("xtotal_line_item_price").Value = IIf(String.IsNullOrEmpty(detail.Product.ListPrice), 0, detail.Product.ListPrice)
            cmd.Parameters("xtotal_line_item_price").Value = detail.Product.ListPrice
            'debugString += " - Fourth batch of parameters added." + Environment.NewLine

            If detail.Product.Type = Product.ProductType.Software Or detail.Product.Type = Product.ProductType.ExtendedWarranty Then
                cmd.Parameters.Add("xSoftwareDeliveryMethod", OracleDbType.Varchar2, 10)
                If detail.Product.Type = Product.ProductType.ExtendedWarranty Then
                    cmd.Parameters("xSoftwareDeliveryMethod").Value = ""
                Else
                    If detail.Downloadable Then
                        cmd.Parameters("xSoftwareDeliveryMethod").Value = "Download"
                    Else
                        cmd.Parameters("xSoftwareDeliveryMethod").Value = "Ship"
                    End If
                End If

                cmd.Parameters.Add("xTrainingTypeCode", OracleDbType.Int32)

                If {Product.ProductType.Certificate, Product.ProductType.ClassRoomTraining, Product.ProductType.ComputerBasedTraining, Product.ProductType.OnlineTraining}.Contains(detail.Product.SoftwareItem.Type) Then
                    'End If
                    'If detail.Product.SoftwareItem.Type = Product.ProductType.Certificate Or detail.Product.SoftwareItem.Type = Product.ProductType.ClassRoomTraining Or detail.Product.SoftwareItem.Type = Product.ProductType.ComputerBasedTraining Or detail.Product.SoftwareItem.Type = Product.ProductType.OnlineTraining Then
                    Select Case detail.Product.SoftwareItem.Type
                        Case Product.ProductType.ExtendedWarranty
                            cmd.Parameters("xTrainingTypeCode").Value = 8
                        Case Product.ProductType.Certificate
                            cmd.Parameters("xTrainingTypeCode").Value = 3
                        Case Product.ProductType.ClassRoomTraining
                            cmd.Parameters("xTrainingTypeCode").Value = 0
                        Case Product.ProductType.ComputerBasedTraining
                            cmd.Parameters("xTrainingTypeCode").Value = 1
                        Case Product.ProductType.OnlineTraining
                            cmd.Parameters("xTrainingTypeCode").Value = 4
                        Case Else
                            cmd.Parameters("xTrainingTypeCode").Value = -1
                    End Select
                End If

                cmd.Parameters.Add("xTrainingCourceNumber", OracleDbType.Varchar2, 50)
                cmd.Parameters("xTrainingCourceNumber").Value = detail.Product.SoftwareItem.PartNumber
                debugString += " - Software parameters added." + Environment.NewLine
            End If

            cmd.Parameters.Add("xRetVal", OracleDbType.Int64, ParameterDirection.Output)
            cmd.Parameters.Add("xCertificationCode", OracleDbType.Varchar2, 200)
            cmd.Parameters.Item("xCertificationCode").Direction = ParameterDirection.Output

            debugString += " - Calling ExecuteCMD." + Environment.NewLine
            ExecuteCMD(cmd, context)

            debugString += " - Checking 'xRetVal' parameter." + Environment.NewLine
            If Not CType(cmd.Parameters("xRetVal")?.Value, INullable).IsNull Then
                debugString += " --- xRetVal = " + cmd.Parameters("xRetVal").Value.ToString() + Environment.NewLine
                detail.SequenceNumber = cmd.Parameters("xRetVal").Value.ToString()
            End If

            debugString += " - Checking 'xCertificationCode' parameter." + Environment.NewLine
            If Not CType(cmd.Parameters("xCertificationCode")?.Value, INullable).IsNull Then
                debugString += " --- xCertificationCode = " + cmd.Parameters("xCertificationCode").Value.ToString() + Environment.NewLine
                If detail.Product.SoftwareItem.Type = Product.ProductType.ExtendedWarranty Then
                    CType(detail.Product, ExtendedWarrantyModel).CertificateNumber = cmd.Parameters("xCertificationCode").Value.ToString()
                Else
                    detail.Product.SoftwareItem.TrainingCertificationCode = cmd.Parameters("xCertificationCode").Value.ToString()
                End If
            End If
        Catch ex As Exception
            Utilities.LogMessages(debugString)
            'Utilities.WrapExceptionforUI(ex)    ' TODO: Remove debug statements when finished
            Throw ex
        End Try
    End Sub

    Public Function ISFraud(ByRef objOrder As Order) As Short
        Dim custMan As CustomerDataManager = New CustomerDataManager()
        'Try
        Dim oContxt As TransactionContext = custMan.Context
        Dim cmd = New OracleCommand("GETFRAUDCHECKRESULT")

        cmd.Parameters.Add("xUserName", OracleDbType.Varchar2, 50)
        cmd.Parameters.Item("xUserName").Value = objOrder.Customer.UserName
        cmd.Parameters.Add("xPassword", OracleDbType.Varchar2, 50)
        cmd.Parameters.Item("xPassword").Value = SIAMUtilities.Encryption.Encrypt(objOrder.Customer.Password, SIAMUtilities.Encryption.PWDKey)
        cmd.Parameters.Add("xFirstName", OracleDbType.Varchar2, 50)
        cmd.Parameters.Item("xFirstName").Value = objOrder.Customer.FirstName
        cmd.Parameters.Add("xLastName", OracleDbType.Varchar2, 50)
        cmd.Parameters.Item("xLastName").Value = objOrder.Customer.LastName
        cmd.Parameters.Add("xBilltoAttn", OracleDbType.Varchar2, 50)
        cmd.Parameters.Item("xBilltoAttn").Value = objOrder.BillTo.Attn
        cmd.Parameters.Add("xShiptoAttn", OracleDbType.Varchar2, 50)
        cmd.Parameters.Item("xShiptoAttn").Value = objOrder.ShipTo.Attn
        cmd.Parameters.Add("retVal", OracleDbType.Int16)
        cmd.Parameters.Item("retVal").Direction = ParameterDirection.Output

        custMan.ExecuteCMD(cmd, oContxt)
        If Not CType(cmd.Parameters("retVal")?.Value, INullable).IsNull Then
            Return Convert.ToInt16(cmd.Parameters("retVal").Value.ToString())
        Else
            Throw New ServicesPLUSOracleException(ERRORCODE.ora06550)
        End If
        'Catch ex As Exception
        ' Throw New ApplicationException("There is a technical problem processing your order. Please call 1-800-538-7550 for assistance.")
        'Throw New ServicesPLUSOracleException(ServicesPlusException.ERRORCODE.ora06550, ex)
        'End Try
    End Function

    Public Function GetCertificateOrderLinesForOrder(ByVal CourseNumber As String, ByVal Location As Short) As DataSet
        Dim doc As DataSet
        Dim cmd As New OracleCommand("TRAININGPLUS.GetTempReservationAndClass")

        cmd.Parameters.Add("xCourseNumber", OracleDbType.Varchar2, 50)
        cmd.Parameters("xCourseNumber").Value = CourseNumber

        cmd.Parameters.Add("xLocation", OracleDbType.Int16)
        cmd.Parameters("xLocation").Value = Location

        cmd.Parameters.Add(New OracleParameter("xClassCoursor", OracleDbType.RefCursor, ParameterDirection.Output))
        cmd.Parameters.Add(New OracleParameter("xTempReservationCoursor", OracleDbType.RefCursor, ParameterDirection.Output))

        doc = GetResults(cmd, True)
        Return doc
    End Function

    Public Function GetOrderDetails(ByVal order_number As String) As OrderLineItem()
        Dim doc As XmlDocument
        Dim cmd As New OracleCommand("getOrderDetails")
        Dim debugString = $"OrderDataManager.GetOrderDetails START at {Date.Now.ToShortTimeString}{Environment.NewLine}"

        Try     ' TODO: Remove try/catch after debugging
            cmd.Parameters.Add("xOrderNumber", OracleDbType.Varchar2)
            cmd.Parameters("xOrderNumber").Value = order_number

            debugString &= "- Calling GetResults." & Environment.NewLine
            doc = GetResults(cmd)

            'internalize
            Dim nodes As XmlNodeList = doc.DocumentElement.ChildNodes
            Dim item As OrderLineItem
            Dim temp_order As Order
            Dim item_list As New ArrayList
            Dim product As Product
            Dim j As Integer
            Dim product_type As String

            temp_order = New Order(order_number)
            debugString &= "- Begin line item loop." & Environment.NewLine
            For j = 0 To nodes.Count - 1
                debugString &= "- Loop #" & j & Environment.NewLine
                item = New OrderLineItem(temp_order)

                product_type = nodes(j)("PRODUCTTYPEID").InnerText
                product = New Part      ' ASleight - 2018-03-02 - Moved from Case "0". Prevents possibility of null if no Cases match, and removes compiler warning.
                Select Case product_type
                    Case "1"
                        debugString &= "--- Product is a Software" & Environment.NewLine
                        product = New Software With {
                            .Type = Product.ProductType.Software,
                            .DownloadFileName = If(nodes(j)("SOFTWAREDOWNLOADFILENAME")?.InnerText, "")
                        }
                    Case "2"
                        debugString &= "--- Product is a Kit" & Environment.NewLine
                        product = New Kit
                    Case "3"
                        debugString &= "--- Product is a ClassRoomTraining" & Environment.NewLine
                        product = New Training(Product.ProductType.ClassRoomTraining)
                    Case "4"
                        debugString &= "--- Product is a ComputerBasedTraining" & Environment.NewLine
                        product = New Training(Product.ProductType.ComputerBasedTraining) With {
                            .DownloadFileName = If(nodes(j)("SOFTWAREDOWNLOADFILENAME")?.InnerText, "")
                        }
                    Case "5"
                        debugString &= "--- Product is a Certificate" & Environment.NewLine
                        product = New Training(Product.ProductType.Certificate)
                    Case "6"
                        debugString &= "--- Product is an OnlineTraining" & Environment.NewLine
                        product = New Training(Product.ProductType.OnlineTraining)
                    Case "8"
                        debugString &= "--- Product is an ExtendedWarranty" & Environment.NewLine
                        product = New ExtendedWarrantyModel(Product.ProductType.ExtendedWarranty) With {
                            .EWModelNumber = If(nodes(j)("EWMODELNAME")?.InnerText, ""),
                            .EWSerialNumber = If(nodes(j)("EWSERIALNUMBER")?.InnerText, ""),
                            .EWPurchaseDate = Convert.ToDateTime(If(nodes(j)("EWPURCHASEDATE")?.InnerText, "2000-01-01")),
                            .CertificateGUID = If(nodes(j)("CERTIFICATEGUID")?.InnerText, ""),
                            .CertificateNumber = If(nodes(j)("CERTIFICATENUMBER")?.InnerText, "")
                        }
                End Select
                debugString &= "--- Select/Case finished." & Environment.NewLine
                item.Product = product
                item.Product.TrainingTitle = If(nodes(j)("TITLE")?.InnerText, "")
                item.Product.TrainingURL = If(nodes(j)("URL")?.InnerText, "")
                item.Product.TrainingActivationCode = If(nodes(j)("TRAININGCERTIFICATIONCODE")?.InnerText, "")
                item.Product.TrainingTypeCode = Convert.ToInt16(If(nodes(j)("TRAININGTYPECODE")?.InnerText, "0"))
                item.Product.PartNumber = If(nodes(j)("PARTNUMBER")?.InnerText, "")
                item.Product.ReplacementPartNumber = If(nodes(j)("PARTNUMBER")?.InnerText, "")
                debugString &= "--- Product is populated." & Environment.NewLine

                item.partnumber = If(nodes(j)("PARTNUMBER")?.InnerText, "")
                debugString &= $"--- PartNumber is {item.partnumber}{Environment.NewLine}"
                item.Downloadable = IIf(nodes(j)("BILLINGONLY")?.InnerText = "1", True, False)
                item.Deliverymethod = If(nodes(j)("SOFTWAREDELIVERYMETHOD")?.InnerText, "")
                item.Quantity = Convert.ToInt16(If(nodes(j)("QUANTITY")?.InnerText, "1"))
                item.DESCRIPTION = If(nodes(j)("DESCRIPTION")?.InnerText, "")
                item.LISTPRICE = Convert.ToDouble(If(nodes(j)("LISTPRICE")?.InnerText, "0.0"), usCulture)
                item.Item_Price = Convert.ToDouble(If(nodes(j)("ITEM_PRICE")?.InnerText, "0.0"), usCulture)
                item.SequenceNumber = Convert.ToInt32(If(nodes(j)("SEQUENCENUMBER")?.InnerText, "1"))
                item.CERTIFICATIONACTIVATIONCODE = If(nodes(j)("CERTIFICATIONACTIVATIONCODE")?.InnerText, "")

                item.Action = CoreObject.ActionType.RESTORE
                item_list.Add(item)
            Next
            debugString &= "- End line item loop. Item count is: " & item_list.Count & Environment.NewLine

            Dim items(item_list.Count - 1) As OrderLineItem
            item_list.CopyTo(items)

            Return items
        Catch ex As Exception
            Utilities.LogMessages(debugString)
            Throw ex
        End Try
    End Function

    Public Sub DeleteOldOrder(ByVal ordernumber As String, ByRef context As TransactionContext, ByRef CCNumber As String, ByRef CCSeq As Integer)
        Dim cmd As New OracleCommand("OrderPurge.Deleteoldorder")

        cmd.Parameters.Add("xordernumber", OracleDbType.Varchar2, 20)
        cmd.Parameters("xordernumber").Value = ordernumber

        cmd.Parameters.Add("xCCRetVal", OracleDbType.Varchar2, 50)
        cmd.Parameters.Item("xCCRetVal").Direction = ParameterDirection.Output    ' Was .InputOutput

        cmd.Parameters.Add("xCCSEQRetVal", OracleDbType.Int32)
        cmd.Parameters.Item("xCCSEQRetVal").Direction = ParameterDirection.Output    ' Was .InputOutput

        Try
            ExecuteCMD(cmd, context)
            If Not CType(cmd.Parameters("xCCRetVal")?.Value, INullable).IsNull Then
                CCNumber = Convert.ToInt16(cmd.Parameters("xCCRetVal").Value)
            Else
                CCNumber = ""
            End If

            If Not CType(cmd.Parameters("xCCSEQRetVal")?.Value, INullable).IsNull Then
                CCSeq = Convert.ToInt16(cmd.Parameters("xCCSEQRetVal").Value)
            Else
                CCSeq = -1
            End If
        Catch ex As Exception
            ' Throw ex
            Throw New ServicesPLUSOracleException(ERRORCODE.ora06550, ex)
        End Try
    End Sub

    Public Function GetShippingMethods(ByRef objGlobalData As GlobalData) As ShippingMethod()
        Dim doc As XmlDocument
        Dim cmd As New OracleCommand("getShippingMethods")

        If objGlobalData.SalesOrganization IsNot Nothing Then
            cmd.Parameters.Add("Sales_Org", OracleDbType.Varchar2)
            cmd.Parameters("Sales_Org").Value = objGlobalData.SalesOrganization
        End If

        If objGlobalData.Language IsNot Nothing Then
            cmd.Parameters.Add("LanguageId", OracleDbType.Varchar2)
            cmd.Parameters("LanguageId").Value = objGlobalData.Language
        End If

        doc = GetResults(cmd)

        'internalize
        Dim nodes As XmlNodeList = doc.DocumentElement.ChildNodes
        Dim ship_method As ShippingMethod
        Dim ship_method_list As New ArrayList
        Dim j As Integer

        For j = 0 To nodes.Count - 1
            ship_method = New ShippingMethod With {
                .Description = If(nodes(j)("DESCRIPTION")?.InnerText, ""),
                .ShippingMethodCode = If(nodes(j)("SHIPPINGMETHODID")?.InnerText, ""),
                .TrackingURL = If(nodes(j)("TRACKINGURL")?.InnerText, ""),
                .SISBackOrderOverride = If(nodes(j)("BACKORDEROVERRIDE")?.InnerText, ""),
                .SISCarrierCode = If(nodes(j)("SISCARRIERCODE")?.InnerText, ""),
                .SISBackOrderCarrierCode = If(nodes(j)("SISBOCARRIERCODE")?.InnerText, ""),
                .SISBackOrderPickGroupCode = If(nodes(j)("SISBOPICKGROUPCODE")?.InnerText, ""),
                .SISPickGroupCode = If(nodes(j)("SISPICKGROUPCODE")?.InnerText, "")
            }
            'ship_method.SISPickGroupCode = nodes(j)("SISPICKGROUPCODE").InnerText
            ship_method_list.Add(ship_method)
        Next

        Dim methods(ship_method_list.Count - 1) As ShippingMethod
        ship_method_list.CopyTo(methods)

        Return methods
    End Function

    Public Function GetShippingMethod(ByVal shipping_method_id As String, ByRef objGlobalData As GlobalData) As ShippingMethod
        Dim doc As XmlDocument
        Dim nodes As XmlNodeList
        Dim j As Integer
        Dim ship_method As ShippingMethod = Nothing
        Dim cmd As New OracleCommand("GETSHIPPINGMETHOD")

        cmd.Parameters.Add("xShippingMethodID", OracleDbType.Int32)
        cmd.Parameters("xShippingMethodID").Value = shipping_method_id

        cmd.Parameters.Add("xSalesOrg", OracleDbType.Varchar2)
        cmd.Parameters("xSalesOrg").Value = objGlobalData.SalesOrganization

        doc = GetResults(cmd)
        nodes = doc.DocumentElement.ChildNodes
        For j = 0 To nodes.Count - 1 'should only get one back
            ship_method = New ShippingMethod With {
                .Description = nodes(j)("DESCRIPTION").InnerText,
                .ShippingMethodCode = nodes(j)("SHIPPINGMETHODID").InnerText,
                .TrackingURL = nodes(j)("TRACKINGURL").InnerText,
                .SISBackOrderOverride = nodes(j)("BACKORDEROVERRIDE").InnerText,
                .SISCarrierCode = nodes(j)("SISCARRIERCODE").InnerText,
                .SISBackOrderCarrierCode = nodes(j)("SISBOCARRIERCODE")?.InnerText,
                .SISBackOrderPickGroupCode = nodes(j)("SISBOPICKGROUPCODE")?.InnerText,
                .SISPickGroupCode = nodes(j)("SISPICKGROUPCODE")?.InnerText
            }
        Next

        Return ship_method
    End Function

    Public Function GetShippingMethodByCarrierCode(ByVal carriercode As String) As ShippingMethod
        Dim doc As XmlDocument
        Dim nodes As XmlNodeList
        Dim ship_method As ShippingMethod = Nothing
        Dim j As Integer
        Dim cmd As New OracleCommand("getShippingMethodByCarrierCode")

        cmd.Parameters.Add("xCarrierCode", OracleDbType.Char, 2).Value = carriercode

        doc = GetResults(cmd)

        'internalize
        nodes = doc.DocumentElement.ChildNodes

        For j = 0 To nodes.Count - 1 'should only get one back
            ship_method = New ShippingMethod With {
                .Description = nodes(j)("DESCRIPTION").InnerText,
                .ShippingMethodCode = nodes(j)("SHIPPINGMETHODID").InnerText,
                .SISBackOrderCarrierCode = nodes(j)("SISBOCARRIERCODE")?.InnerText,
                .TrackingURL = nodes(j)("TRACKINGURL").InnerText,
                .SISBackOrderPickGroupCode = nodes(j)("SISBOPICKGROUPCODE")?.InnerText,
                .SISBackOrderOverride = nodes(j)("BACKORDEROVERRIDE")?.InnerText,
                .SISCarrierCode = nodes(j)("SISCARRIERCODE")?.InnerText,
                .SISPickGroupCode = nodes(j)("SISPICKGROUPCODE")?.InnerText
            }
        Next

        Return ship_method
    End Function

    Public Function GetCustomerOrders(ByVal customer As Customer, ByVal objGlobalData As GlobalData) As Order()
        Dim doc As XmlDocument
        Dim nodes As XmlNodeList
        Dim order As Order
        Dim order_list As New ArrayList
        Dim cmd As New OracleCommand("getCustomerOrders")
        Dim debugMessage As String = $"OrderDataManager.GetCustomerOrders - START at {Date.Now.ToShortTimeString()}{Environment.NewLine}"

        Try
            cmd.Parameters.Add("xUserId", OracleDbType.Varchar2, 50).Value = customer.CustomerID
            debugMessage &= $"- Fetching Orders for customer email: {customer.EmailAddress}.{Environment.NewLine}"

            doc = GetResults(cmd)
            nodes = doc.DocumentElement.ChildNodes

            For Each node As XmlNode In nodes
                ' 2018-07-23 ASleight - Removing a bunch of extra parameters that were not in the Stored Proc to begin with. Caused NREs.
                debugMessage &= $"- Building Order Number {node("ORDERNUMBER").InnerText} from XmlNodes.{Environment.NewLine}"
                debugMessage &= "--- Properties: ("

                'order = New Order With {
                '    .OrderNumber = If(node("ORDERNUMBER")?.InnerText, "1"),
                '    .SequenceNumber = If(node("SEQUENCENUMBER")?.InnerText, "1"),
                '    .OrderDate = IIf(String.IsNullOrWhiteSpace(node("ORDERDATE")?.InnerText), DateTime.Now, Convert.ToDateTime(node("ORDERDATE").InnerText, usCulture)),
                '    .AllocatedGrandTotal = IIf(String.IsNullOrWhiteSpace(node("ALLOCATEDGRANDTOTAL")?.InnerText), 0.00, Convert.ToDouble(node("ALLOCATEDGRANDTOTAL").InnerText, usCulture)),
                '    .AllocatedPartSubTotal = IIf(String.IsNullOrWhiteSpace(node("ALLOCATEDPARTSUBTOTAL")?.InnerText), 0.00, Convert.ToDouble(node("ALLOCATEDPARTSUBTOTAL").InnerText, usCulture)),
                '    .AllocatedTax = IIf(String.IsNullOrWhiteSpace(node("ALLOCATEDTAXAMOUNT")?.InnerText), 0.00, Convert.ToDouble(node("ALLOCATEDTAXAMOUNT").InnerText, usCulture)),
                '    .AllocatedShipping = IIf(String.IsNullOrWhiteSpace(node("ALLOCATEDSHIPPINGAMOUNT")?.InnerText), 0.00, Convert.ToDouble(node("ALLOCATEDSHIPPINGAMOUNT").InnerText, usCulture)),
                '    .LegacyAccountNumber = If(node("SAPBILLTOACCOUNTNUMBER")?.InnerText, ""),
                '    .SAPBillToAccountNumber = If(node("SAPBILLTOACCOUNTNUMBER")?.InnerText, ""),
                '    .POReference = If(node("PONUMBER")?.InnerText, "NO-PO-REQ"),
                '    .ShippingMethod = IIf(String.IsNullOrWhiteSpace(node("SHIPPINGMETHODID")?.InnerText), Nothing, GetShippingMethod(node("SHIPPINGMETHODID").InnerText, objGlobalData)),
                '    .UpdateDate = IIf(String.IsNullOrWhiteSpace(node("UPDATEDATE")?.InnerText), DateTime.Now, Convert.ToDateTime(node("UPDATEDATE").InnerText, usCulture)),
                '    .BillingOnly = IIf(String.IsNullOrWhiteSpace(node("BILLINGONLY")?.InnerText), 0, Convert.ToInt32(node("BILLINGONLY").InnerText, usCulture)),
                '    .Customer = customer,
                '    .Action = CoreObject.ActionType.RESTORE
                '}
                order = New Order()
                debugMessage &= $"OrderNumber = {If(node("ORDERNUMBER")?.InnerText, "null")}, "
                order.OrderNumber = If(node("ORDERNUMBER")?.InnerText, "1")
                debugMessage &= $"SequenceNumber = {If(node("SEQUENCENUMBER")?.InnerText, "null")}, "
                order.SequenceNumber = If(node("SEQUENCENUMBER")?.InnerText, "1")
                debugMessage &= $"OrderDate = {If(node("ORDERDATE")?.InnerText, "null")}, "
                order.OrderDate = IIf(String.IsNullOrWhiteSpace(node("ORDERDATE")?.InnerText), Date.Now, Convert.ToDateTime(node("ORDERDATE").InnerText, usCulture))
                debugMessage &= $"AllocatedGrandTotal = {If(node("ALLOCATEDGRANDTOTAL")?.InnerText, "null")}, "
                order.AllocatedGrandTotal = IIf(String.IsNullOrWhiteSpace(node("ALLOCATEDGRANDTOTAL")?.InnerText), 0.00, Convert.ToDouble(node("ALLOCATEDGRANDTOTAL").InnerText, usCulture))
                debugMessage &= $"AllocatedPartSubtotal = {If(node("ALLOCATEDPARTSUBTOTAL")?.InnerText, "null")}, "
                order.AllocatedPartSubTotal = IIf(String.IsNullOrWhiteSpace(node("ALLOCATEDPARTSUBTOTAL")?.InnerText), 0.00, Convert.ToDouble(node("ALLOCATEDPARTSUBTOTAL").InnerText, usCulture))
                debugMessage &= $"AllocatedTaxAmount = {If(node("ALLOCATEDTAXAMOUNT")?.InnerText, "null")}, "
                order.AllocatedTax = IIf(String.IsNullOrWhiteSpace(node("ALLOCATEDTAXAMOUNT")?.InnerText), 0.00, Convert.ToDouble(node("ALLOCATEDTAXAMOUNT").InnerText, usCulture))
                debugMessage &= $"AllocatedShippingAmount = {If(node("ALLOCATEDSHIPPINGAMOUNT")?.InnerText, "null")}, "
                order.AllocatedShipping = IIf(String.IsNullOrWhiteSpace(node("ALLOCATEDSHIPPINGAMOUNT")?.InnerText), 0.00, Convert.ToDouble(node("ALLOCATEDSHIPPINGAMOUNT").InnerText, usCulture))
                debugMessage &= $"SAPBillToAccountNumber = {If(node("SAPBILLTOACCOUNTNUMBER")?.InnerText, "null")}, "
                order.LegacyAccountNumber = If(node("SAPBILLTOACCOUNTNUMBER")?.InnerText, "")
                order.SAPBillToAccountNumber = If(node("SAPBILLTOACCOUNTNUMBER")?.InnerText, "")
                debugMessage &= $"PONumber = {If(node("PONUMBER")?.InnerText, "null")}, "
                order.POReference = If(node("PONUMBER")?.InnerText, "NO-PO-REQ")
                debugMessage &= $"ShippingMethodID = {If(node("SHIPPINGMETHODID")?.InnerText, "null")}, "
                order.ShippingMethod = GetShippingMethod(If(node("SHIPPINGMETHODID")?.InnerText, "-1"), objGlobalData)
                debugMessage &= $"UpdateDate = {If(node("UPDATEDATE")?.InnerText, "null")}, "
                order.UpdateDate = IIf(String.IsNullOrWhiteSpace(node("UPDATEDATE")?.InnerText), Date.Now, Convert.ToDateTime(node("UPDATEDATE").InnerText, usCulture))
                debugMessage &= $"BillingOnly = {If(node("BILLINGONLY")?.InnerText, "null")}){Environment.NewLine}"
                order.BillingOnly = IIf(String.IsNullOrWhiteSpace(node("BILLINGONLY")?.InnerText), 0, Convert.ToInt32(node("BILLINGONLY").InnerText, usCulture))
                order.Customer = customer
                order.Action = CoreObject.ActionType.RESTORE

                debugMessage &= $"--- Order {order.OrderNumber} complete.{Environment.NewLine}"

                order_list.Add(order)
            Next

            Dim orders(order_list.Count - 1) As Order
            order_list.CopyTo(orders)

            Return orders
        Catch ex As Exception
            Utilities.LogMessages($"Exception during OrderDataManager.GetCustomerOrders:{Environment.NewLine}" & debugMessage)
            Throw ex
        End Try
    End Function

    Public Function GetCustomerOrders(ByVal customerid As Long) As DataSet
        Dim doc As DataSet
        Dim cmd As New OracleCommand("ORDERMANAGER.GETCUSTOMERORDER")

        cmd.Parameters.Add("xCUSTOMERID", OracleDbType.Int32)
        cmd.Parameters("xCUSTOMERID").Value = customerid

        cmd.Parameters.Add("xCUSTOMERORDER", OracleDbType.RefCursor, ParameterDirection.Output)

        doc = GetResults(cmd, True)

        Return doc
    End Function

    Public Function GetCertificateOrderLinesForOrder(ByVal OrderNumber As String) As DataSet
        Dim doc As DataSet
        Dim cmd As New OracleCommand("ORDERMANAGER.GETCERTIFICATEORDERLINE")

        cmd.Parameters.Add("xORDERNUMBER", OracleDbType.Varchar2, 50)
        cmd.Parameters("xORDERNUMBER").Value = OrderNumber

        cmd.Parameters.Add(New OracleParameter("xORDERLINE", OracleDbType.RefCursor, ParameterDirection.Output))

        doc = GetResults(cmd, True)
        Return doc
    End Function

    Public Sub GetOrderInvoices(ByRef order As Order)
        Dim invoice As Invoice
        Dim doc As XmlDocument
        Dim nodes As XmlNodeList
        Dim cmd As New OracleCommand("getOrderInvoices")
        Try

            cmd.Parameters.Add("xOrderNumber", OracleDbType.Varchar2, 50)
            cmd.Parameters("xOrderNumber").Value = order.OrderNumber
            cmd.Parameters.Add("xSequenceNumber", OracleDbType.Int32)
            cmd.Parameters("xSequenceNumber").Value = order.SequenceNumber

            doc = GetResults(cmd)
            nodes = doc.DocumentElement.ChildNodes

            For Each node As XmlNode In nodes
                invoice = New Invoice(order) With {
                    .InvoiceNumber = If(node("INVOICENUMBER")?.InnerText, ""),
                    .Total = Convert.ToDouble(If(node("INVOICETOTAL")?.InnerText, "0.0"))
                }
                order.AddInvoice(invoice)
            Next
        Catch ex As Exception
            Utilities.LogDebug("Exception during OrderDataManager.GetOrderInvoices")
            Throw ex
        End Try
    End Sub

    Public Sub GetSpecOrderInvoices(ByRef order As Order, ByVal OriginalInvoicenumber As String)
        Dim invoice As Invoice
        Dim doc As XmlDocument
        Dim nodes As XmlNodeList
        Dim cmd As New OracleCommand("getSpecOrderInvoices")

        cmd.Parameters.Add("xOrderNumber", OracleDbType.Varchar2, 50)
        cmd.Parameters("xOrderNumber").Value = order.OrderNumber
        cmd.Parameters.Add("xInvoiceNumber", OracleDbType.Varchar2, 50)
        cmd.Parameters("xInvoiceNumber").Value = OriginalInvoicenumber

        doc = GetResults(cmd)
        nodes = doc.DocumentElement.ChildNodes
        For Each node As XmlNode In nodes
            invoice = New Invoice(order) With {
                .InvoiceNumber = If(node("INVOICENUMBER")?.InnerText, ""),
                .Total = Convert.ToDouble(If(node("INVOICETOTAL")?.InnerText, "0.0"))
            }
            order.AddInvoice(invoice)
        Next
    End Sub

    Public Function GetSpecialTaxURL(ByRef country As String, ByRef state As String, ByRef city As String, ByRef type As String) As String
        Dim doc As XmlDocument
        Dim nodes As XmlNodeList
        Dim cmd As New OracleCommand("getSpecialTaxURL")

        cmd.Parameters.Add("xCountry", OracleDbType.Varchar2, 50)
        cmd.Parameters("xCountry").Value = country
        cmd.Parameters.Add("xState", OracleDbType.Varchar2, 50)
        cmd.Parameters("xState").Value = state
        cmd.Parameters.Add("xCity", OracleDbType.Varchar2, 50)
        cmd.Parameters("xCity").Value = city
        cmd.Parameters.Add("xType", OracleDbType.Varchar2, 25)
        cmd.Parameters("xType").Value = type

        doc = GetResults(cmd)
        nodes = doc.DocumentElement.ChildNodes

        Return If(nodes(0)?("URL")?.InnerText, Nothing)
    End Function

    Public Sub StoreDownloadRecord(ByRef downloadRecord As DownloadRecord, ByVal context As TransactionContext)
        Dim cmd As New OracleCommand("addDownloadRecord")

        cmd.Parameters.Add("xsiamid", OracleDbType.Varchar2, 16)
        cmd.Parameters("xsiamid").Value = downloadRecord.SIAMID
        cmd.Parameters.Add("xkitpartnumber", OracleDbType.Varchar2, 9)
        cmd.Parameters("xkitpartnumber").Value = downloadRecord.KitPartNumber
        cmd.Parameters.Add("xdeliverymethod", OracleDbType.Varchar2, 25)
        cmd.Parameters("xdeliverymethod").Value = downloadRecord.DeliveryMethod

        cmd.Parameters.Add("xcustomerid", OracleDbType.Int32)
        cmd.Parameters("xcustomerid").Value = downloadRecord.CustomerID

        cmd.Parameters.Add("xcustomersequencenumber", OracleDbType.Int32)
        cmd.Parameters("xcustomersequencenumber").Value = downloadRecord.CustomerSequenceNumber

        cmd.Parameters.Add("xmodelid", OracleDbType.Varchar2, 15)
        cmd.Parameters("xmodelid").Value = downloadRecord.ModelID

        cmd.Parameters.Add("xmodeldesc", OracleDbType.Varchar2, 75)
        cmd.Parameters("xmodeldesc").Value = downloadRecord.ModelDescription

        cmd.Parameters.Add("xversion", OracleDbType.Varchar2, 10)
        cmd.Parameters("xversion").Value = downloadRecord.Version

        cmd.Parameters.Add("xfreeofcharge", OracleDbType.Int32)
        cmd.Parameters("xfreeofcharge").Value = downloadRecord.FreeOfCharge

        cmd.Parameters.Add("xkitdesc", OracleDbType.Varchar2, 30)
        cmd.Parameters("xkitdesc").Value = downloadRecord.KitDescription

        cmd.Parameters.Add("xordernumber", OracleDbType.Varchar2, 15)
        cmd.Parameters("xordernumber").Value = downloadRecord.OrderNumber

        cmd.Parameters.Add("xorderlineitemsequence", OracleDbType.Int32)
        cmd.Parameters("xorderlineitemsequence").Value = downloadRecord.OrderLineItemSequence

        cmd.Parameters.Add("xhttp_x_forwarded_for", OracleDbType.Varchar2)
        cmd.Parameters("xhttp_x_forwarded_for").Value = downloadRecord.HTTP_X_Forward_For

        cmd.Parameters.Add("xremote_addr", OracleDbType.Varchar2)
        cmd.Parameters("xremote_addr").Value = downloadRecord.RemoteADDR

        cmd.Parameters.Add("xhttp_referer", OracleDbType.Varchar2)
        cmd.Parameters("xhttp_referer").Value = downloadRecord.HTTPReferer

        cmd.Parameters.Add("xhttp_url", OracleDbType.Varchar2)
        cmd.Parameters("xhttp_url").Value = downloadRecord.HTTPURL

        cmd.Parameters.Add("xhttp_user_agent", OracleDbType.Varchar2)
        cmd.Parameters("xhttp_user_agent").Value = downloadRecord.HTTPUserAgent

        ExecuteCMD(cmd, context)
    End Sub

    'Added by chandra to get user location from database by order number
    Public Function GetUserLocation(ByVal ordernumber As String, ByVal context As TransactionContext) As Short
        Dim cmd As New OracleCommand("GetUserLocationByOrderNumber")

        cmd.Parameters.Add("xOrderNumber", OracleDbType.Varchar2, 25)
        cmd.Parameters("xOrderNumber").Value = ordernumber

        cmd.Parameters.Add("xRetVal", OracleDbType.Int16, ParameterDirection.Output)

        ExecuteCMD(cmd, context)
        If Not CType(cmd.Parameters("xRetVal")?.Value, INullable).IsNull Then
            Return Convert.ToInt16(cmd.Parameters("xRetVal").Value.ToString())
        Else
            Return -1
        End If
    End Function

    Public Function GetConflictedOrder(ByVal OrderNumber As String) As String
        Dim cmd As New OracleCommand("getneworder")

        cmd.Parameters.Add("p_oldorderno", OracleDbType.Varchar2, 20)
        cmd.Parameters("p_oldorderno").Value = OrderNumber

        cmd.Parameters.Add("p_neworderno", OracleDbType.Varchar2, 20)
        cmd.Parameters.Item("p_neworderno").Direction = ParameterDirection.Output

        ExecuteCMD(cmd, Nothing)

        If Not CType(cmd.Parameters("p_neworderno")?.Value, INullable).IsNull Then
            Utilities.LogDebug($"OrderDataManager.GetConflictedOrder - New Order #: {cmd.Parameters("p_neworderno").Value.ToString()}")
            Return cmd.Parameters("p_neworderno").Value.ToString()
        Else
            Return String.Empty     ' Nothing
        End If
    End Function

End Class

