
Imports System.Xml
Imports Sony.US.ServicesPLUS.Core
Imports Oracle.ManagedDataAccess.Client
Imports ServicesPlusException
Imports Oracle.ManagedDataAccess.Types

Public Class PromotionDataManager
    Inherits DataManager

    Public Overrides Function Restore(ByVal identity As String) As Object
        Throw New NotImplementedException()
    End Function

    Public Overrides Sub Store(ByRef data As Object, ByRef context As TransactionContext, Optional ByVal update As Boolean = False)
    End Sub

    Public Function GetCurrentPromotions(ByVal type As Product.ProductType) As Promotion()
        Dim doc As XmlDocument
        Dim promo As Promotion
        Dim promo_list As New ArrayList
        Dim cmd As New OracleCommand("getCurrentPromotions")

        cmd.Parameters.Add("xProductTypeID", OracleDbType.Int32, type, ParameterDirection.Input)

        doc = GetResults(cmd)

        Dim nodes As XmlNodeList = doc.DocumentElement.ChildNodes
        For Each node As XmlNode In nodes
            promo = New Promotion With {
                .PromotionText = node("PROMOTIONTEXT")?.InnerText,
                .ImageFileName = node("IMAGEFILENAME")?.InnerText,
                .EffectiveDate = node("EFFECTIVEDATE")?.InnerText,
                .ExpirationDate = node("EXPIRATIONDATE")?.InnerText,
                .DisplayPriority = node("DISPLAYPRIORITY")?.InnerText
            }
            promo_list.Add(promo)
        Next

        Dim promotions(promo_list.Count - 1) As Promotion
        promo_list.CopyTo(promotions)

        Return promotions
    End Function

    Public Function GetPromotionKits() As PromotionKit()
        Dim doc As XmlDocument
        Dim nodes As XmlNodeList
        Dim kit_list As New ArrayList
        'Dim j As Integer
        Dim cmd As New OracleCommand("GetPromotionKits")

        doc = GetResults(cmd)

        nodes = doc.DocumentElement.ChildNodes

        For Each node As XmlNode In nodes
            Dim kit As New PromotionKit With {
                .PartNumber = node("MODELNAME")?.InnerText,
                .DiscountPercentage = node("DISCOUNTPERCENT")?.InnerText
            }
            kit_list.Add(kit)
        Next
        Dim kits(kit_list.Count - 1) As PromotionKit
        kit_list.CopyTo(kits)

        Return kits
    End Function

    Public Function GetKitInPromotion(ByVal kitName As String) As Kit
        Dim doc As XmlDocument
        Dim node As XmlNode
        Dim kit As Kit = Nothing
        Dim kit_list As New ArrayList
        Dim kit_item As KitItem
        Dim cmd As New OracleCommand("getkitinpromotion")

        cmd.Parameters.Add("xKitName", OracleDbType.Varchar2, 50, kitName, ParameterDirection.Input)
        doc = GetResults(cmd)

        Dim nodes As XmlNodeList = doc.DocumentElement.ChildNodes

        If nodes.Count > 0 Then
            node = nodes(0)

            kit = New Kit With {
                .Model = node("MODELNAME").InnerText,
                .PartNumber = node("MODELNAME").InnerText,
                .DiscountPercentage = If(node("DISCOUNTPERCENT")?.InnerText, ""),
                .Description = If(node("DESCRIPTION")?.InnerText, ""),
                .TheKitType = IIf((node("SISKITTYPEID")?.InnerText = "MJ"), Kit.KitType.Major, Kit.KitType.Minor)
            }

            kit_item = New KitItem With {
                .Quantity = If(node("QUANTITY")?.InnerText, "1"),
                .Product = New Part With {
                    .PartNumber = If(node("PARTNUMBER")?.InnerText, ""),
                    .Model = If(node("MODELNUMBER")?.InnerText, ""),
                    .Description = If(node("PARTDESCRIPTION")?.InnerText, ""),
                    .Category = IIf(node("PARTCATEGORY") Is Nothing, Nothing, New ProductCategory(node("PARTCATEGORY").InnerText)),
                    .ProgramCode = If(node("PROGRAMCODE")?.InnerText, ""),
                    .CoreCharge = If(node("FRBCORECHARGE")?.InnerText, ""),
                    .ReplacementPartNumber = If(node("REPLACEMENTPARTNUMBER")?.InnerText, ""),
                    .ListPrice = If(node("LISTPRICE")?.InnerText, ""),
                    .BillingOnly = If(node("BILLINGONLY")?.InnerText, ""),
                    .SalesText = If(node("SALESTEXT")?.InnerText, "")
                }
            }

            If kit_item.Quantity > 1 Then
                kit_item.Product.ListPrice = kit_item.Product.ListPrice * kit_item.Quantity
            End If

            kit.AddKitItem(kit_item)
        End If

        Return kit
    End Function


    Public Function GetKitsInPromotion() As Kit()
        Dim doc As XmlDocument
        Dim elem As XmlElement
        Dim nodes As XmlNodeList
        Dim kit As Kit
        Dim kit_list As New ArrayList
        Dim kit_type As String
        Dim kit_item As KitItem
        Dim part As Part
        Dim category As ProductCategory
        Dim j As Integer
        Dim cmd As New OracleCommand("getkitsinpromotion")

        doc = GetResults(cmd)
        nodes = doc.DocumentElement.ChildNodes

        For j = 0 To nodes.Count - 1
            kit = New Kit
            kit.Model = nodes(j)("KITMODELNAME").InnerText
            kit.PartNumber = nodes(j)("KITMODELNAME").InnerText

            elem = nodes(j)("DISCOUNTPERCENT")
            If Not elem Is Nothing Then
                kit.DiscountPercentage = elem.InnerText
            End If

            elem = nodes(j)("DESCRIPTION")
            If Not elem Is Nothing Then
                kit.Description = elem.InnerText
            End If

            elem = nodes(j)("SISKITTYPEID")
            If Not elem Is Nothing Then
                kit_type = elem.InnerText

                If kit_type = "MJ" Then
                    kit.TheKitType = Kit.KitType.Major
                Else
                    kit.TheKitType = Kit.KitType.Minor
                End If
            End If

            kit_item = New KitItem
            part = New Part

            elem = nodes(j)("PARTNUMBER")
            If Not elem Is Nothing Then
                part.PartNumber = elem.InnerText
            End If

            elem = nodes(j)("MODELNUMBER")
            If Not elem Is Nothing Then
                part.Model = elem.InnerText
            End If

            elem = nodes(j)("PARTDESCRIPTION")
            If Not elem Is Nothing Then
                part.Description = elem.InnerText
            End If

            category = New ProductCategory(nodes(j)("PARTCATEGORY").InnerText)
            part.Category = category

            elem = nodes(j)("PROGRAMCODE")

            If elem Is Nothing Then
            Else
                part.ProgramCode = elem.InnerText
            End If

            elem = nodes(j)("FRBCORECHARGE")

            If elem Is Nothing Then
            Else
                part.CoreCharge = elem.InnerText
            End If

            elem = nodes(j)("REPLACEMENTPARTNUMBER")

            If elem Is Nothing Then
            Else
                part.ReplacementPartNumber = elem.InnerText
            End If


            elem = nodes(j)("LISTPRICE")
            If elem Is Nothing Then
            Else
                part.ListPrice = elem.InnerText
            End If

            'elem = nodes(j)("CANADIANPRICE")
            'If elem Is Nothing Then
            'Else
            '    part.CanadianPrice = elem.InnerText
            'End If

            'elem = nodes(j)("DEALERPRICE")
            'If elem Is Nothing Then
            'Else
            '    part.DealerPrice = elem.InnerText
            'End If

            'elem = nodes(j)("DISTRIBUTORPRICE")
            'If elem Is Nothing Then
            'Else
            '    part.DistributorPrice = elem.InnerText
            'End If

            elem = nodes(j)("BILLINGONLY")

            If elem Is Nothing Then
            Else
                part.BillingOnly = elem.InnerText
            End If

            elem = nodes(j)("SALESTEXT")

            If elem Is Nothing Then
            Else
                part.SalesText = elem.InnerText
            End If


            kit_item.Product = part

            elem = nodes(j)("QUANTITY")
            If Not elem Is Nothing Then
                kit_item.Quantity = elem.InnerText
                If kit_item.Quantity > 1 Then
                    part.ListPrice = part.ListPrice * kit_item.Quantity
                End If
            End If
            kit.AddKitItem(kit_item)

            kit_list.Add(kit)
        Next

        Dim kits(kit_list.Count - 1) As Kit
        kit_list.CopyTo(kits)

        Return kits

    End Function

    Public Function GetPartInPromotion(ByVal partName As String) As Part
        Dim doc As XmlDocument
        Dim elem As XmlElement
        Dim part_list As New ArrayList
        Dim part As Part = Nothing
        Dim category As ProductCategory
        Dim j As Integer
        Dim cmd As New OracleCommand("GetPartInPromotion")

        cmd.Parameters.Add("xPartNumber", OracleDbType.Varchar2, 50, partName, ParameterDirection.Input)

        doc = GetResults(cmd)

        Dim nodes As XmlNodeList = doc.DocumentElement.ChildNodes

        If nodes.Count > 0 Then
            part = New Part

            elem = nodes(j)("PARTNUMBER")
            If elem Is Nothing Then
            Else
                part.PartNumber = elem.InnerText
            End If

            elem = nodes(j)("MODELNUMBER")
            If elem Is Nothing Then
            Else
                part.Model = elem.InnerText
            End If

            elem = nodes(j)("DESCRIPTION")

            If elem Is Nothing Then
            Else
                part.Description = elem.InnerText
            End If

            elem = nodes(j)("PARTCATEGORY")

            If elem Is Nothing Then
            Else
                category = New ProductCategory(elem.InnerText)
                part.Category = category
            End If

            elem = nodes(j)("PROGRAMCODE")

            If elem Is Nothing Then
            Else
                part.ProgramCode = elem.InnerText
            End If

            elem = nodes(j)("FRBCORECHARGE")

            If elem Is Nothing Then
            Else
                part.CoreCharge = elem.InnerText
            End If

            elem = nodes(j)("REPLACEMENTPARTNUMBER")

            If elem Is Nothing Then
            Else
                part.ReplacementPartNumber = elem.InnerText
            End If

            elem = nodes(j)("LISTPRICE")

            If elem Is Nothing Then
            Else
                part.ListPrice = elem.InnerText
            End If

            'elem = nodes(j)("CANADIANPRICE")

            'If elem Is Nothing Then
            'Else
            '    part.CanadianPrice = elem.InnerText
            'End If

            'elem = nodes(j)("DEALERPRICE")

            'If elem Is Nothing Then
            'Else
            '    part.DealerPrice = elem.InnerText
            'End If

            'elem = nodes(j)("DISTRIBUTORPRICE")

            'If elem Is Nothing Then
            'Else
            '    part.DistributorPrice = elem.InnerText
            'End If

            elem = nodes(j)("BILLINGONLY")

            If elem Is Nothing Then
            Else
                part.BillingOnly = elem.InnerText
            End If

            elem = nodes(j)("SALESTEXT")

            If elem Is Nothing Then
            Else
                part.SalesText = elem.InnerText
            End If

            part_list.Add(part)
        End If

        Return part
    End Function


    Public Function GetPartsInPromotion() As Part()
        Dim doc As XmlDocument
        Dim elem As XmlElement
        Dim nodes As XmlNodeList
        Dim part_list As New ArrayList
        Dim part As Part
        Dim category As ProductCategory
        Dim j As Integer
        Dim cmd As New OracleCommand("GetPartsInPromotion")

        doc = GetResults(cmd)

        nodes = doc.DocumentElement.ChildNodes

        For j = 0 To nodes.Count - 1
            part = New Part

            elem = nodes(j)("PARTNUMBER")
            If elem Is Nothing Then
            Else
                part.PartNumber = elem.InnerText
            End If

            elem = nodes(j)("MODELNUMBER")
            If elem Is Nothing Then
            Else
                part.Model = elem.InnerText
            End If

            elem = nodes(j)("DESCRIPTION")
            If elem Is Nothing Then
            Else
                part.Description = elem.InnerText
            End If

            elem = nodes(j)("PARTCATEGORY")
            If elem Is Nothing Then
            Else
                category = New ProductCategory(elem.InnerText)
                part.Category = category
            End If

            elem = nodes(j)("PROGRAMCODE")
            If elem Is Nothing Then
            Else
                part.ProgramCode = elem.InnerText
            End If

            elem = nodes(j)("FRBCORECHARGE")
            If elem Is Nothing Then
            Else
                part.CoreCharge = elem.InnerText
            End If

            elem = nodes(j)("REPLACEMENTPARTNUMBER")
            If elem Is Nothing Then
            Else
                part.ReplacementPartNumber = elem.InnerText
            End If

            elem = nodes(j)("LISTPRICE")
            If elem Is Nothing Then
            Else
                part.ListPrice = elem.InnerText
            End If

            'elem = nodes(j)("CANADIANPRICE")

            'If elem Is Nothing Then
            'Else
            '    part.CanadianPrice = elem.InnerText
            'End If

            'elem = nodes(j)("DEALERPRICE")

            'If elem Is Nothing Then
            'Else
            '    part.DealerPrice = elem.InnerText
            'End If

            'elem = nodes(j)("DISTRIBUTORPRICE")

            'If elem Is Nothing Then
            'Else
            '    part.DistributorPrice = elem.InnerText
            'End If

            elem = nodes(j)("BILLINGONLY")
            If elem Is Nothing Then
            Else
                part.BillingOnly = elem.InnerText
            End If

            elem = nodes(j)("SALESTEXT")
            If elem Is Nothing Then
            Else
                part.SalesText = elem.InnerText
            End If

            part_list.Add(part)
        Next

        Dim parts(part_list.Count - 1) As Part
        part_list.CopyTo(parts)

        Return parts
    End Function

    Public Function GetInternetPromotionParts(ByRef objGlobalData As GlobalData) As PromotionPart()
        Dim doc As XmlDocument
        Dim cmd As New OracleCommand("GETSISINTERNETPROMOTIONPARTS")

        cmd.Parameters.Add("p_Sales_Org", OracleDbType.Char)
        cmd.Parameters("p_Sales_Org").Value = objGlobalData.SalesOrganization

        cmd.Parameters.Add("p_Language_Id", OracleDbType.Char)
        cmd.Parameters("p_Language_Id").Value = objGlobalData.Language

        doc = GetResults(cmd)

        'internalize
        Dim root As XmlElement = doc.DocumentElement
        Dim nodes As XmlNodeList = root.ChildNodes
        Dim promoPart As PromotionPart
        Dim promoPart_list As New ArrayList
        Dim j As Integer
        Dim elem As XmlElement

        For j = 0 To nodes.Count - 1
            promoPart = New PromotionPart
            promoPart.PartNumber = nodes(j)("PARTNUMBER").InnerText
            elem = nodes(j)("MODELNUMBER")
            If Not elem Is Nothing Then
                promoPart.ModelNumber = nodes(j)("MODELNUMBER").InnerText
            Else
                promoPart.ModelNumber = String.Empty
            End If
            elem = nodes(j)("LISTPRICE")
            If Not elem Is Nothing Then
                promoPart.ListPrice = nodes(j)("LISTPRICE").InnerText
            End If
            elem = nodes(j)("INTERNETPRICE")
            If Not elem Is Nothing Then
                promoPart.InternetPrice = nodes(j)("INTERNETPRICE").InnerText
            End If
            elem = nodes(j)("DISCOUNTPERCENTAGE")
            If Not elem Is Nothing Then
                promoPart.DiscountPercentage = nodes(j)("DISCOUNTPERCENTAGE").InnerText
            End If

            elem = nodes(j)("DESCRIPTION")
            If Not elem Is Nothing Then
                promoPart.Description = elem.InnerText
            End If
            promoPart_list.Add(promoPart)
        Next

        Dim promotionParts(promoPart_list.Count - 1) As PromotionPart
        promoPart_list.CopyTo(promotionParts)

        Return promotionParts

    End Function

    Public Function IsPromotionPart(ByRef context As TransactionContext, ByVal partNumber As String) As Boolean
        Dim boolPromoPart As Boolean = False
        Dim cmd As New OracleCommand("IsPromotionPart")

        cmd.Parameters.Add("xPartNumber", OracleDbType.Varchar2, 20).Value = partNumber

        Dim outparam As New OracleParameter("IsPromo", OracleDbType.Varchar2, 10)
        outparam.Direction = ParameterDirection.Output
        cmd.Parameters.Add(outparam)

        OpenConnection()
        cmd.Connection = dbConnection

        Try
            ExecuteCMD(cmd, context)
            If Not CType(cmd.Parameters("IsPromo")?.Value, INullable).IsNull Then
                boolPromoPart = CType(cmd.Parameters("IsPromo").Value.ToString(), Boolean)
            End If
        Catch ex As Exception
            'Throw New ServicesPLUSOracleException(ServicesPlusException.ERRORCODE.ora06550, ex)
            Throw ex    ' ASleight - Many database calls get re-thrown as the above ora06550 code. Not helpful at all.
        End Try

        Return boolPromoPart
    End Function
End Class
