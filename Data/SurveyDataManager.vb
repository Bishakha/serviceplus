
Imports System.Xml
Imports Oracle.ManagedDataAccess.Client
Imports Oracle.ManagedDataAccess.Types
Imports Sony.US.ServicesPLUS.Core
Imports ServicesPlusException

Public Class SurveyDataManager
    Inherits DataManager

    'restores baseline survey definition
    Public Overrides Function Restore(ByVal identity As String) As Object
        Dim survey As Survey
        Dim doc As XmlDocument
        Dim cmd As New OracleCommand

        cmd.CommandText = "getSurvey"
        cmd.Parameters.Add("xSurveyId", OracleDbType.Int32)
        cmd.Parameters("xSurveyId").Value = identity

        doc = GetResults(cmd)

        'internalize
        Dim ques As Question
        Dim root As XmlElement = doc.DocumentElement
        Dim nodes As XmlNodeList = root.ChildNodes
        Dim j As Integer
        Dim str_display_type As String

        survey = New Survey
        survey.SurveyID = identity

        For j = 0 To nodes.Count - 1 'just hard code to 0 and throw exception if count > 1
            ques = New Question With {
                .QuestionID = nodes(j)("QUESTIONID").InnerText,
                .Text = nodes(j)("QUESTIONTEXT").InnerText
            }
            survey.EffectiveDate = nodes(j)("EFFECTIVEDATE").InnerText
            survey.ExpirationDate = nodes(j)("EXPIRATIONDATE").InnerText
            survey.Description = nodes(j)("DESCRIPTION").InnerText
            str_display_type = nodes(j)("ANSWERDISPLAYTYPE").InnerText

            Select Case str_display_type
                Case "1"
                    ques.AnswerType = Question.AnswerDisplayType.RadioButtons
                Case "2"
                    ques.AnswerType = Question.AnswerDisplayType.Textbox
                Case Else
                    ' Throw New Exception("Unsupported Answer Display Type")
                    Throw New ServicesPLUSOracleException(ERRORCODE.ora06550)
            End Select

            survey.AddQuestion(ques)
        Next

        Return survey
    End Function

    'stores responses only - surveys themselves are currently not programmatically administered.
    Public Overrides Sub Store(ByRef data As Object, ByRef context As TransactionContext, Optional ByVal update As Boolean = False)
        Dim resp As Response = CType(data, Response)
        Dim cmd As New OracleCommand

        If (Not update) Or (update And resp.Dirty) Then
            If update Then
                If resp.Action = CoreObject.ActionType.DELETE Then
                    cmd.CommandText = "deleteSurveyResponse"
                Else
                    cmd.CommandText = "updateSurveyResponse"
                End If

                cmd.Parameters.Add("xSequenceNumber", OracleDbType.Int32)
                cmd.Parameters("xSequenceNumber").Value = resp.SequenceNumber
            Else
                cmd.CommandText = "addSurveyResponse"
            End If

            cmd.Parameters.Add("xCustomerID", OracleDbType.Int32)
            cmd.Parameters("xCustomerID").Value = resp.Respondent.CustomerID
            cmd.Parameters.Add("xSurveyID", OracleDbType.Int32)
            cmd.Parameters("xSurveyID").Value = resp.Survey.SurveyID
            cmd.Parameters.Add("xQuestionID", OracleDbType.Int32)
            cmd.Parameters("xQuestionID").Value = resp.Question.QuestionID
            cmd.Parameters.Add("xTextResponse", OracleDbType.Varchar2, 200)
            cmd.Parameters("xTextResponse").Value = resp.TextResponse

            If resp.RadioButtonResponse <> Response.FixedRadioButtonResponseType.NotSpecified Then
                cmd.Parameters.Add("xRadioButtonResponse", OracleDbType.Int32)
                cmd.Parameters("xRadioButtonResponse").Value = resp.RadioButtonResponse
            End If

            Dim outparam As New OracleParameter("retVal", OracleDbType.Int32)
            outparam.Direction = ParameterDirection.Output
            cmd.Parameters.Add(outparam)
            ExecuteCMD(cmd, context)

            If Not CType(cmd.Parameters("retVal")?.Value, INullable).IsNull Then
                'If update Then
                resp.SequenceNumber = cmd.Parameters("retVal").Value.ToString()
                'Else
                'cust.CustomerID() = cmd.Parameters("retVal").Value().ToString()
                'End If
            End If
        End If

        resp.Action = CoreObject.ActionType.RESTORE
    End Sub

    Public Function GetCustomerResponses(ByVal survey_id As String, ByVal customer As Customer) As Response()
        Dim response As Response
        Dim doc As XmlDocument
        Dim str_display_type As String
        Dim cmd As New OracleCommand

        cmd.CommandText = "getSurveyResponses"
        cmd.Parameters.Add("xSurveyId", OracleDbType.Int32)
        cmd.Parameters("xSurveyId").Value = survey_id
        cmd.Parameters.Add("xCustomerID", OracleDbType.Int32)
        cmd.Parameters("xCustomerID").Value = customer.CustomerID

        doc = GetResults(cmd)
        Dim root As XmlElement = doc.DocumentElement
        Dim nodes As XmlNodeList = root.ChildNodes
        Dim j As Integer
        Dim elem As XmlElement
        Dim response_list As New ArrayList

        For j = 0 To nodes.Count - 1 'just hard code to 0 and throw exception if count > 1
            response = New Response
            response.Survey = New Survey
            response.Respondent = New Customer
            response.Question = New Question
            response.Survey.SurveyID = nodes(j)("SURVEYID").InnerText 'fully hydrate?
            response.Respondent = customer
            response.Question.QuestionID = nodes(j)("QUESTIONID").InnerText
            response.Question.Text = nodes(j)("QUESTIONTEXT").InnerText
            str_display_type = nodes(j)("ANSWERDISPLAYTYPE").InnerText

            Select Case str_display_type
                Case "1"
                    response.Question.AnswerType = Question.AnswerDisplayType.RadioButtons
                Case "2"
                    response.Question.AnswerType = Question.AnswerDisplayType.Textbox
                Case Else
                    'Throw New Exception("Unsupported Answer Display Type")
                    Throw New ServicesPLUSOracleException(ERRORCODE.ora06550)
            End Select

            response.SequenceNumber = nodes(j)("SEQUENCENUMBER").InnerText

            elem = nodes(j)("RADIOBUTTONRESPONSE")
            If elem Is Nothing Then
            Else
                response.RadioButtonResponse = nodes(j)("RADIOBUTTONRESPONSE").InnerText
            End If

            elem = nodes(j)("TEXTRESPONSE")
            If elem Is Nothing Then
            Else
                response.TextResponse = nodes(j)("TEXTRESPONSE").InnerText
            End If

            response.Action = CoreObject.ActionType.RESTORE
            response_list.Add(response)
        Next

        Dim responses(response_list.Count - 1) As Response
        response_list.CopyTo(responses)

        Return responses
    End Function
End Class
