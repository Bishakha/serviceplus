Imports System.Xml
Imports Oracle.ManagedDataAccess.Client
Imports Oracle.ManagedDataAccess.Types
Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.SIAMUtilities
Imports ServicesPlusException

Public Class UserRoleSecurityManager
    Protected cn As OracleConnection
    Protected connString As String
    Protected connection_string As String

    Public Sub New()
        cn = New OracleConnection
        'Dim config_settings As Hashtable
        'Dim handler As New ConfigHandler("SOFTWARE\SIAM")
        'config_settings = handler.GetConfigSettings("//item[@name='CurrentDAL']")
        'connString = config_settings.Item("ConnectionString").ToString()
        connString = ConfigurationData.Environment.DataBase.ConnectionString
        cn.ConnectionString = connString
    End Sub

    Public Sub CloseConnection()
        If cn.State = ConnectionState.Open Then
            cn.Close()
        End If
    End Sub

    Public Function GetLoggedUserFunctionality(ByVal pUserID As String) As Functionality()
        Dim docUserFunct As XmlDocument

        docUserFunct = GetLogUsrFunDocs(pUserID)
        Dim root As XmlElement = docUserFunct.DocumentElement
        Dim nodes As XmlNodeList = root.ChildNodes
        Dim user_func As Functionality
        Dim user_role_list As New ArrayList
        Dim j As Integer

        For j = 0 To nodes.Count - 1
            user_func = New Functionality
            user_func.FunctionalityName = nodes(j)("FUNCTIONALITYNAME").InnerText
            user_func.URLMapped = nodes(j)("MAPPEDURL").InnerText
            user_role_list.Add(user_func)
        Next

        Dim card_types(user_role_list.Count - 1) As Functionality
        user_role_list.CopyTo(card_types)

        Return card_types

    End Function

    Private Function GetLogUsrFunDocs(ByVal pUserID As String) As XmlDocument
        Dim return_value As OracleClob
        Dim doc As New XmlDocument
        Dim error_node As XmlNode
        Dim outparam As New OracleParameter("retVal", OracleDbType.Clob)

        Dim command As New OracleCommand With {
            .Connection = cn,
            .CommandType = CommandType.StoredProcedure,
            .CommandText = "GETLOGGEDUSERFUNCTIONALITY",
            .BindByName = True
        }

        command.Parameters.Add("xUserID", OracleDbType.Varchar2)
        command.Parameters("xUserID").Value = pUserID

        outparam.Direction = ParameterDirection.Output
        command.Parameters.Add(outparam)
        ExecuteCMD(command, Nothing, False)

        If CType(command.Parameters("retVal")?.Value, INullable).IsNull Then
            Dim error_message = "Requested data not found."
            If Not CType(command.Parameters("retVal")?.Value, INullable).IsNull Then
                'error condition was set in procedure
                error_message = command.Parameters("errorCondition").Value.ToString()
            End If
            Throw New Exception(error_message)
        Else
            return_value = command.Parameters("RetVal").Value
            return_value.Seek(0, IO.SeekOrigin.Begin)

            Try
                doc.Load(return_value)
            Catch ex As Exception
                Dim excp As String
                excp = ex.Message()
                excp = ex.StackTrace()
                Throw New ServicesPLUSOracleException(ERRORCODE.ora06550, ex)
            End Try

            cn.Close()

            error_node = doc.SelectSingleNode("//ERROR")

            If error_node Is Nothing Then
            Else
                'Throw New Exception(error_node.InnerText)
                Throw New ServicesPLUSOracleException(ERRORCODE.ora06550)
            End If

        End If
        Return doc
    End Function

    Public Function UpdateUserRoles(ByVal pUserID As String, ByVal pRoleString As String) As Integer
        Dim arRole As String
        Dim sRole As String
        Dim iRet As Integer = 0
        Try
            DeleteUserRoleMap(pUserID)
            For Each arRole In pRoleString.Split("-")
                sRole = arRole
                If sRole.Trim().Length > 0 Then
                    AddUserRoleMap(pUserID, Convert.ToInt32(sRole))
                End If
            Next
            iRet = 1
        Catch ex As Exception
            ' iRet = 0
            Throw New ServicesPLUSOracleException(ERRORCODE.ora06550)
        End Try
        Return iRet
    End Function

    Private Function DeleteUserRoleMap(ByVal pUserId As String) As Integer
        Dim return_value As Decimal
        Dim doc As New XmlDocument
        Dim error_node As XmlNode

        Dim cmd As New OracleCommand With {
            .Connection = cn,
            .CommandType = CommandType.StoredProcedure,
            .CommandText = "DELETEUSERROLES",
            .BindByName = True
        }

        cmd.Parameters.Add("xUserId", OracleDbType.Varchar2)
        cmd.Parameters("xUserId").Value = pUserId

        Dim outparam As New OracleParameter("retVal", OracleDbType.Int32)
        outparam.Direction = ParameterDirection.Output
        cmd.Parameters.Add(outparam)
        ExecuteCMD(cmd, Nothing, False)

        If CType(cmd.Parameters("retVal")?.Value, INullable).IsNull Then
            Dim error_message = "Requested data not found."
            If Not CType(cmd.Parameters("errorCondition")?.Value, INullable).IsNull Then
                'error condition was set in procedure
                error_message = cmd.Parameters("errorCondition").Value
            End If
            Throw New Exception(error_message)
        Else
            return_value = Convert.ToDecimal(cmd.Parameters("retVal").Value)
            error_node = doc.SelectSingleNode("//ERROR")
            CloseConnection()

            If error_node IsNot Nothing Then
                'Throw New Exception(error_node.InnerText)
                Throw New ServicesPLUSOracleException(ERRORCODE.ora06550)
            End If
        End If

        Return return_value
    End Function

    Private Function DeleteUserRole(ByVal pUserId As String, ByVal pRoleId As Integer) As Integer
        Dim return_value As Decimal
        Dim doc As New XmlDocument
        Dim error_node As XmlNode
        Dim outparam As New OracleParameter("retVal", OracleDbType.Int32)

        Dim cmd As New OracleCommand With {
            .Connection = cn,
            .CommandType = CommandType.StoredProcedure,
            .CommandText = "DELETEUSERROLE",
            .BindByName = True
        }

        cmd.Parameters.Add("xUserId", OracleDbType.Varchar2)
        cmd.Parameters("xUserId").Value = pUserId

        cmd.Parameters.Add("xRoleId", OracleDbType.Int32)
        cmd.Parameters("xRoleId").Value = pRoleId

        outparam.Direction = ParameterDirection.Output
        cmd.Parameters.Add(outparam)
        ExecuteCMD(cmd, Nothing, False)

        If CType(cmd.Parameters("retVal")?.Value, INullable).IsNull Then
            If Not CType(cmd.Parameters("errorCondition")?.Value, INullable).IsNull Then
                'error condition was set in procedure
                Throw New Exception(cmd.Parameters("errorCondition").Value.ToString())
            End If
            Throw New ServicesPLUSOracleException(ERRORCODE.ora06550)
        Else
            return_value = Convert.ToDecimal(cmd.Parameters("retVal").Value)
            error_node = doc.SelectSingleNode("//ERROR")
            CloseConnection()

            If error_node IsNot Nothing Then
                Throw New ServicesPLUSOracleException(ERRORCODE.ora06550)
            End If

        End If

        Return return_value
    End Function

    Public Function GetUserRolesDocs(ByVal pSIAMID As String) As XmlDocument
        Dim return_value As OracleClob
        Dim doc As New XmlDocument
        Dim error_node As XmlNode
        Dim outparam As New OracleParameter("retVal", OracleDbType.Clob)

        Dim cmd As New OracleCommand With {
            .Connection = cn,
            .CommandType = CommandType.StoredProcedure,
            .CommandText = "GETUSERROLES",
            .BindByName = True
        }

        cmd.Parameters.Add("xUserId", OracleDbType.Varchar2)
        cmd.Parameters("xUserId").Value = pSIAMID

        outparam.Direction = ParameterDirection.Output
        cmd.Parameters.Add(outparam)
        ExecuteCMD(cmd, Nothing, False)

        If CType(cmd.Parameters("retVal")?.Value, INullable).IsNull Then
            If Not CType(cmd.Parameters("errorCondition")?.Value, INullable).IsNull Then
                'error condition was set in procedure
                Throw New Exception(cmd.Parameters("errorCondition").Value)
            End If
            Throw New Exception("Requested data not found.")
        Else
            return_value = cmd.Parameters("RetVal").Value
            return_value.Seek(0, IO.SeekOrigin.Begin)

            Try
                doc.Load(return_value)
            Catch ex As Exception
                Dim excp As String
                excp = ex.Message()     ' ASleight - Why do we set this twice, then do nothing with it, and hide the error?
                excp = ex.StackTrace()
                Throw New ServicesPLUSOracleException(ERRORCODE.ora06550)
            End Try

            error_node = doc.SelectSingleNode("//ERROR")
            CloseConnection()

            If error_node IsNot Nothing Then
                Throw New Exception(error_node.InnerText)
            End If
        End If

        Return doc
    End Function

    Public Function GetUserDetailDocs(ByVal pSIAMID As String) As DataSet
        Dim command As New OracleCommand With {
            .CommandText = "USERS.GETUSERSDETAILS",
            .BindByName = True
        }

        command.Parameters.Add("xSiamIdentity", OracleDbType.Varchar2, 50)
        command.Parameters("xSiamIdentity").Value = pSIAMID

        Dim xUserfo As New OracleParameter("xUserfo", OracleDbType.RefCursor)
        xUserfo.Direction = ParameterDirection.Output
        command.Parameters.Add(xUserfo)

        Dim ds As New DataSet()
        ds = GetResults(command, True)

        Return ds
    End Function

    Public Function UpdateUserRole(ByVal pRoleName As String, ByVal pStatus As Integer, ByVal pRoleId As Integer) As Boolean
        Dim return_value As Decimal
        Dim doc As New XmlDocument
        Dim error_node As XmlNode
        Dim outparam As New OracleParameter("retVal", OracleDbType.Int32)

        Dim cmd As New OracleCommand With {
            .Connection = cn,
            .CommandType = CommandType.StoredProcedure,
            .CommandText = "UPDATEUSERROLE",
            .BindByName = True
        }

        cmd.Parameters.Add("xRoleName", OracleDbType.Varchar2)
        cmd.Parameters("xRoleName").Value = pRoleName

        cmd.Parameters.Add("xRoleId", OracleDbType.Int32)
        cmd.Parameters("xRoleId").Value = pRoleId

        cmd.Parameters.Add("xStatus", OracleDbType.Int32)
        cmd.Parameters("xStatus").Value = pStatus

        outparam.Direction = ParameterDirection.Output
        cmd.Parameters.Add(outparam)
        ExecuteCMD(cmd, Nothing, False)

        If CType(cmd.Parameters("retVal")?.Value, INullable).IsNull Then
            If Not CType(cmd.Parameters("errorCondition")?.Value, INullable).IsNull Then
                'error condition was set in procedure
                Dim error_message As String
                error_message = cmd.Parameters("errorCondition").Value
                Throw New Exception(error_message)
            End If

            ' Throw New Exception("Requested data not found.")
            Throw New ServicesPLUSOracleException(ERRORCODE.ora06550)
        Else
            return_value = Convert.ToDecimal(cmd.Parameters("retVal").Value)
            cn.Close()
            error_node = doc.SelectSingleNode("//ERROR")

            If error_node Is Nothing Then
            Else
                'Throw New Exception(error_node.InnerText)
                Throw New ServicesPLUSOracleException(ERRORCODE.ora06550)
            End If

        End If

        Return return_value
    End Function

    Public Function AddUserRoleMap(ByVal pUserID As String, ByVal pRoleID As Integer) As Integer
        Dim return_value As Decimal
        Dim doc As New XmlDocument
        Dim error_node As XmlNode

        Dim cmd As New OracleCommand With {
            .Connection = cn,
            .CommandType = CommandType.StoredProcedure,
            .CommandText = "ADDUSERROLEMAP",
            .BindByName = True
        }

        cmd.Parameters.Add("xUSERID", OracleDbType.Varchar2)
        cmd.Parameters("xUSERID").Value = pUserID

        cmd.Parameters.Add("xROLEID", OracleDbType.Int32)
        cmd.Parameters("xROLEID").Value = pRoleID

        Dim outparam As New OracleParameter("retVal", OracleDbType.Int32)
        outparam.Direction = ParameterDirection.Output
        cmd.Parameters.Add(outparam)

        ExecuteCMD(cmd, Nothing, False)

        If CType(cmd.Parameters("retVal")?.Value, INullable).IsNull Then
            If Not CType(cmd.Parameters("errorCondition")?.Value, INullable).IsNull Then
                'error condition was set in procedure
                Dim error_message As String
                error_message = cmd.Parameters("errorCondition").Value
                'Throw New Exception(error_message)
                Throw New ServicesPLUSOracleException(ERRORCODE.ora06550)
            End If

            Throw New Exception("Requested data not found.")
        Else
            return_value = Convert.ToDecimal(cmd.Parameters("retVal").Value)
            CloseConnection()
            error_node = doc.SelectSingleNode("//ERROR")

            If error_node IsNot Nothing Then
                Throw New ServicesPLUSOracleException(ERRORCODE.ora06550)
            End If
        End If

        Return return_value
    End Function

    Public Function AddUserRole(ByVal pRoleName As String, ByVal pActive As Integer) As Integer
        Dim return_value As Decimal
        Dim doc As New XmlDocument
        Dim error_node As XmlNode
        Dim outparam As New OracleParameter("retVal", OracleDbType.Int32)

        Dim cmd As New OracleCommand With {
            .Connection = cn,
            .CommandType = CommandType.StoredProcedure,
            .CommandText = "ADDUSERROLE",
            .BindByName = True
        }

        cmd.Parameters.Add("xRoleName", OracleDbType.Varchar2)
        cmd.Parameters("xRoleName").Value = pRoleName

        cmd.Parameters.Add("xActive", OracleDbType.Int32)
        cmd.Parameters("xActive").Value = pActive

        outparam.Direction = ParameterDirection.Output
        cmd.Parameters.Add(outparam)

        ExecuteCMD(cmd, Nothing, False)

        If CType(cmd.Parameters("retVal")?.Value, INullable).IsNull Then
            If Not CType(cmd.Parameters("errorCondition")?.Value, INullable).IsNull Then
                'error condition was set in procedure
                Dim error_message As String
                error_message = cmd.Parameters("errorCondition").Value.ToString()
                Throw New Exception(error_message)
            End If

            Throw New Exception("Requested data not found.")
        Else
            return_value = Convert.ToDecimal(cmd.Parameters("retVal").Value)
            cn.Close()
            error_node = doc.SelectSingleNode("//ERROR")

            If error_node IsNot Nothing Then
                'Throw New Exception(error_node.InnerText)
                Throw New ServicesPLUSOracleException(ERRORCODE.ora06550)
            End If
        End If
        Return return_value
    End Function

    Public Function GetUserRoles(ByVal pRoleName As String, ByVal pDeleted As Integer) As UserRole()
        Dim docUserRoles As XmlDocument

        docUserRoles = GetUserRoleDocs(pRoleName, pDeleted)
        Dim root As XmlElement = docUserRoles.DocumentElement
        Dim nodes As XmlNodeList = root.ChildNodes
        Dim user_role As UserRole
        Dim user_role_list As New ArrayList
        Dim j As Integer

        For j = 0 To nodes.Count - 1
            user_role = New UserRole With {
                .RoleID = nodes(j)("ROLEID").InnerText,
                .RoleName = nodes(j)("ROLENAME").InnerText,
                .IsDeleted = nodes(j)("ISACTIVE").InnerText
            }
            user_role_list.Add(user_role)
        Next

        Dim card_types(user_role_list.Count - 1) As UserRole
        user_role_list.CopyTo(card_types)

        Return card_types

    End Function

    Private Function GetUserRoleDocs(ByVal pRole As String, ByVal pDeleted As Integer) As XmlDocument
        Dim return_value As OracleClob
        Dim doc As New XmlDocument
        Dim error_node As XmlNode
        Dim outparam As New OracleParameter("retVal", OracleDbType.Clob)

        Dim cmd As New OracleCommand With {
            .Connection = cn,
            .CommandType = CommandType.StoredProcedure,
            .CommandText = "GETROLEBYNAME",
            .BindByName = True
        }

        cmd.Parameters.Add("xRoleName", OracleDbType.Varchar2)
        cmd.Parameters("xRoleName").Value = pRole

        cmd.Parameters.Add("xActive", OracleDbType.Int32)
        cmd.Parameters("xActive").Value = pDeleted

        outparam.Direction = ParameterDirection.Output
        cmd.Parameters.Add(outparam)
        ExecuteCMD(cmd, Nothing, False)

        If CType(cmd.Parameters("retVal")?.Value, INullable).IsNull Then
            If Not CType(cmd.Parameters("errorCondition")?.Value, INullable).IsNull Then
                'error condition was set in procedure
                Dim error_message As String
                error_message = cmd.Parameters("errorCondition").Value
                Throw New Exception(error_message)
            End If

            Throw New Exception("Requested data not found.")
        Else
            return_value = cmd.Parameters("RetVal").Value
            return_value.Seek(0, IO.SeekOrigin.Begin)

            Try
                doc.Load(return_value)
            Catch ex As Exception
                Dim excp As String
                excp = ex.Message()
                excp = ex.StackTrace()
                Throw New ServicesPLUSOracleException(ERRORCODE.ora06550, ex)
            End Try

            cn.Close()

            error_node = doc.SelectSingleNode("//ERROR")

            If error_node IsNot Nothing Then Throw New Exception(error_node.InnerText)
        End If
        Return doc
    End Function

    'Added for Functionality master search page
    Public Function GetFunctionalitiesObj(ByVal functionalityName As String, ByVal mappedUrl As String, ByVal siteStatus As String) As Functionality()
        Dim splitstatus As String() = siteStatus.Split("*")
        Dim m_functionality As Functionality
        Dim m_functionality_list As New ArrayList
        Dim ds As DataSet
        Dim command As New OracleCommand With {
            .Connection = cn,
            .CommandType = CommandType.StoredProcedure,
            .CommandText = "FunctionalityDetails.GETFUNCTIONALITIES",
            .BindByName = True
        }

        command.Parameters.Add("xFunctionalityName", OracleDbType.Varchar2, 50)
        command.Parameters("xFunctionalityName").Value = functionalityName

        command.Parameters.Add("xMappedUrl", OracleDbType.Varchar2, 100)
        command.Parameters("xMappedUrl").Value = mappedUrl

        command.Parameters.Add("xSiteStatus", OracleDbType.Varchar2, 50)
        If splitstatus.Count > 1 Then
            If splitstatus(1) = "Active" Or splitstatus(1) = "InActive" Then
                command.Parameters("xSiteStatus").Value = Convert.ToInt32(splitstatus(0))
            Else
                command.Parameters("xSiteStatus").Value = Convert.ToInt32(splitstatus(0))
            End If
        Else
            command.Parameters("xSiteStatus").Value = splitstatus(0)
        End If

        Dim xFunctionalityInfo As New OracleParameter("xFunctionalityInfo", OracleDbType.RefCursor)
        xFunctionalityInfo.Direction = ParameterDirection.Output
        command.Parameters.Add(xFunctionalityInfo)

        ds = GetResults(command, True)
        Dim j As Integer
        If ds.Tables(0).Rows.Count > 0 Then
            For j = 0 To ds.Tables(0).Rows.Count - 1
                If splitstatus.Count > 1 Then
                    If ds.Tables(0).Rows(j).Item("SITESTATUS").ToString() = "1" And splitstatus(1) = "Active" Then
                        m_functionality = New Functionality With {
                            .FunctionalityId = ds.Tables(0).Rows(j).Item("FUNCTIONALITYID").ToString(),
                            .FunctionalityName = ds.Tables(0).Rows(j).Item("FUNCTIONALITYNAME").ToString(),
                            .URLMapped = ds.Tables(0).Rows(j).Item("MAPPEDURL").ToString(),
                            .GroupID = ds.Tables(0).Rows(j).Item("GroupID").ToString(),
                            .SITESTATUS = ds.Tables(0).Rows(j).Item("SITESTATUS").ToString()
                        }
                        If ds.Tables(0).Rows(j).Item("SITESTATUS").ToString() = "1" Then
                            m_functionality.IsDeleted = "Yes"
                        Else
                            m_functionality.IsDeleted = "No"
                        End If

                        m_functionality_list.Add(m_functionality)
                    ElseIf ds.Tables(0).Rows(j).Item("SITESTATUS").ToString() = "3" And splitstatus(1) = "InActive" Then
                        m_functionality = New Functionality With {
                            .FunctionalityId = ds.Tables(0).Rows(j).Item("FUNCTIONALITYID").ToString(),
                            .FunctionalityName = ds.Tables(0).Rows(j).Item("FUNCTIONALITYNAME").ToString(),
                            .URLMapped = ds.Tables(0).Rows(j).Item("MAPPEDURL").ToString(),
                            .GroupID = ds.Tables(0).Rows(j).Item("GroupID").ToString(),
                            .SITESTATUS = ds.Tables(0).Rows(j).Item("SITESTATUS").ToString()
                        }
                        If ds.Tables(0).Rows(j).Item("SITESTATUS").ToString() = "1" Then
                            m_functionality.IsDeleted = "Yes"
                        Else
                            m_functionality.IsDeleted = "No"
                        End If

                        m_functionality_list.Add(m_functionality)
                    End If
                Else
                    m_functionality = New Functionality With {
                        .FunctionalityId = ds.Tables(0).Rows(j).Item("FUNCTIONALITYID").ToString(),
                        .FunctionalityName = ds.Tables(0).Rows(j).Item("FUNCTIONALITYNAME").ToString(),
                        .URLMapped = ds.Tables(0).Rows(j).Item("MAPPEDURL").ToString(),
                        .GroupID = ds.Tables(0).Rows(j).Item("GroupID").ToString(),
                        .SITESTATUS = ds.Tables(0).Rows(j).Item("SITESTATUS").ToString()
                    }
                    If ds.Tables(0).Rows(j).Item("SITESTATUS").ToString() = "1" Then
                        m_functionality.IsDeleted = "Yes"
                    Else
                        m_functionality.IsDeleted = "No"
                    End If

                    m_functionality_list.Add(m_functionality)
                End If
            Next
        End If
        Dim card_types(m_functionality_list.Count - 1) As Functionality
        m_functionality_list.CopyTo(card_types)

        Return card_types
    End Function

    'Added for Functionality master Add page
    Public Function AddFunctionality(ByVal FunctionalityName As String, ByVal MappedURL As String, ByVal IsActive As Integer, ByVal GroupID As Short, ByVal SITESTATUS As Short) As Integer
        Dim return_value As Decimal
        Dim doc As New XmlDocument
        Dim error_node As XmlNode
        Dim outparam As New OracleParameter("retVal", OracleDbType.Int32)

        Dim cmd As New OracleCommand With {
            .Connection = cn,
            .CommandType = CommandType.StoredProcedure,
            .CommandText = "ADDFUNCTIONALITY",
            .BindByName = True
        }

        cmd.Parameters.Add("xFunctionalityName", OracleDbType.Varchar2)
        cmd.Parameters("xFunctionalityName").Value = FunctionalityName

        cmd.Parameters.Add("xMappedURL", OracleDbType.Varchar2)
        cmd.Parameters("xMappedURL").Value = MappedURL

        cmd.Parameters.Add("xIsActive", OracleDbType.Int32)
        cmd.Parameters("xIsActive").Value = IsActive

        cmd.Parameters.Add("xGroupID", OracleDbType.Int32)
        cmd.Parameters("xGroupID").Value = GroupID

        cmd.Parameters.Add("xSITESTATUS", OracleDbType.Int16)
        cmd.Parameters("xSITESTATUS").Value = SITESTATUS

        outparam.Direction = ParameterDirection.Output
        cmd.Parameters.Add(outparam)
        ExecuteCMD(cmd, Nothing, False)

        If CType(cmd.Parameters("retVal")?.Value, INullable).IsNull Then
            If Not CType(cmd.Parameters("errorCondition")?.Value, INullable).IsNull Then
                'error condition was set in procedure
                Dim error_message As String
                error_message = cmd.Parameters("errorCondition").Value.ToString()
                Throw New Exception(error_message)
            End If

            Throw New Exception("Requested data not found.")
        Else
            return_value = Convert.ToDecimal(cmd.Parameters("retVal").Value)
            CloseConnection()
            error_node = doc.SelectSingleNode("//ERROR")

            If error_node IsNot Nothing Then
                '  Throw New Exception(error_node.InnerText)
                Throw New ServicesPLUSOracleException(ERRORCODE.ora06550)
            End If
        End If

        Return return_value
    End Function

    'Added for Functionality Master Update page
    Public Function UpdateFunctionality(ByVal FunctionalityId As Integer, ByVal FunctionalityName As String, ByVal MappedURL As String, ByVal IsActive As Integer, ByVal GroupId As Short, ByVal SITESTATUS As Short) As Boolean
        Dim return_value As Decimal
        Dim doc As New XmlDocument
        Dim error_node As XmlNode
        Dim outparam As New OracleParameter("retVal", OracleDbType.Int32)

        Dim cmd As New OracleCommand With {
            .Connection = cn,
            .CommandType = CommandType.StoredProcedure,
            .CommandText = "UPDATEFUNCTIONALITY",
            .BindByName = True
        }

        cmd.Parameters.Add("xFuntionalityId", OracleDbType.Int32)
        cmd.Parameters("xFuntionalityId").Value = FunctionalityId

        cmd.Parameters.Add("xFunctionalityName", OracleDbType.Varchar2)
        cmd.Parameters("xFunctionalityName").Value = FunctionalityName

        cmd.Parameters.Add("xMappedURL", OracleDbType.Varchar2)
        cmd.Parameters("xMappedURL").Value = MappedURL

        cmd.Parameters.Add("xIsActive", OracleDbType.Int32)
        cmd.Parameters("xIsActive").Value = IsActive

        cmd.Parameters.Add("xGroupID", OracleDbType.Int32)
        cmd.Parameters("xGroupID").Value = GroupId

        cmd.Parameters.Add("xSITESTATUS", OracleDbType.Int16)
        cmd.Parameters("xSITESTATUS").Value = SITESTATUS

        outparam.Direction = ParameterDirection.Output
        cmd.Parameters.Add(outparam)

        ExecuteCMD(cmd, Nothing, False)

        If CType(cmd.Parameters("retVal")?.Value, INullable).IsNull Then
            If Not CType(cmd.Parameters("errorCondition")?.Value, INullable).IsNull Then
                'error condition was set in procedure
                Dim error_message As String
                error_message = cmd.Parameters("errorCondition").Value.ToString()
                Throw New Exception(error_message)
            End If

            Throw New Exception("Requested data not found.")
        Else
            return_value = Convert.ToDecimal(cmd.Parameters("retVal").Value)
            cn.Close()
            error_node = doc.SelectSingleNode("//ERROR")

            If error_node IsNot Nothing Then
                ' Throw New Exception(error_node.InnerText)
                Throw New ServicesPLUSOracleException(ERRORCODE.ora06550)
            End If
        End If

        Return return_value
    End Function

    'Added for User Search
    Public Function SearchUsers(ByVal searchPattern As String) As SiamUser()
        Dim m_user As SiamUser
        Dim m_user_list As New ArrayList
        Dim j As Integer
        Dim ds As DataSet

        ds = GetSearchUsersDocs(searchPattern)

        If (ds.Tables?.Count > 0) Then
            For j = 0 To ds.Tables(0).Rows.Count - 1
                m_user = New SiamUser
                With ds.Tables(0)
                    m_user.UserID = .Rows(j)("SiamIdentity").ToString()
                    m_user.FirstName = .Rows(j)("FIRSTNAME").ToString()
                    m_user.LastName = .Rows(j)("LASTNAME").ToString()
                    m_user.UserName = .Rows(j)("USERNAME").ToString()
                    m_user.RoleName = .Rows(j)("NAME").ToString()
                    If m_user.RoleName Is String.Empty Then
                        m_user.RoleName = "No Role Mapped"
                    End If
                End With
                m_user_list.Add(m_user)
            Next
        End If
        Dim card_types(m_user_list.Count - 1) As SiamUser
        m_user_list.CopyTo(card_types)

        Return card_types
    End Function

    'Added for User Search
    Public Function GetSearchUsersDocs(ByVal searchPattern As String) As DataSet

        Dim splitSearchPattern As String()
        Dim cmd As New OracleCommand With {
            .CommandText = "USERS.GETUSERSEARCHRESULTS",
            .CommandType = CommandType.StoredProcedure,
            .BindByName = True
        }

        If Not String.IsNullOrEmpty(searchPattern) Then
            splitSearchPattern = searchPattern.Split("*")

            cmd.Parameters.Add("xFirsName", OracleDbType.Varchar2, 50)
            cmd.Parameters("xFirsName").Value = splitSearchPattern(0)

            cmd.Parameters.Add("xLastName", OracleDbType.Varchar2, 50)
            cmd.Parameters("xLastName").Value = splitSearchPattern(1)

            cmd.Parameters.Add("xEmail", OracleDbType.Varchar2, 50)
            cmd.Parameters("xEmail").Value = splitSearchPattern(2)

            cmd.Parameters.Add("xCompany", OracleDbType.Varchar2, 50)
            cmd.Parameters("xCompany").Value = splitSearchPattern(3)

            cmd.Parameters.Add("xUserName", OracleDbType.Varchar2, 50)
            cmd.Parameters("xUserName").Value = splitSearchPattern(4)

            cmd.Parameters.Add("xSiamIdentity", OracleDbType.Varchar2, 50)
            cmd.Parameters("xSiamIdentity").Value = splitSearchPattern(5)

            cmd.Parameters.Add("xRole", OracleDbType.Varchar2, 50)
            cmd.Parameters("xRole").Value = splitSearchPattern(6)

            cmd.Parameters.Add("xStatus", OracleDbType.Int32)
            cmd.Parameters("xStatus").Value = Convert.ToInt32(splitSearchPattern(7))

            cmd.Parameters.Add("xChecktheSearch", OracleDbType.Int32, 1)
            cmd.Parameters("xChecktheSearch").Value = 1
        Else
            cmd.Parameters.Add("xFirsName", OracleDbType.Varchar2, 50)
            cmd.Parameters("xFirsName").Value = ""

            cmd.Parameters.Add("xLastName", OracleDbType.Varchar2, 50)
            cmd.Parameters("xLastName").Value = ""

            cmd.Parameters.Add("xEmail", OracleDbType.Varchar2, 50)
            cmd.Parameters("xEmail").Value = ""

            cmd.Parameters.Add("xCompany", OracleDbType.Varchar2, 50)
            cmd.Parameters("xCompany").Value = ""

            cmd.Parameters.Add("xUserName", OracleDbType.Varchar2, 50)
            cmd.Parameters("xUserName").Value = ""

            cmd.Parameters.Add("xRole", OracleDbType.Varchar2, 50)
            cmd.Parameters("xRole").Value = ""

            cmd.Parameters.Add("xStatus", OracleDbType.Int32)
            cmd.Parameters("xStatus").Value = ""

            cmd.Parameters.Add("xChecktheSearch", OracleDbType.Int32, 1)
            cmd.Parameters("xChecktheSearch").Value = 0
        End If

        Dim xUserfo As New OracleParameter("xUserfo", OracleDbType.RefCursor)
        xUserfo.Direction = ParameterDirection.Output

        cmd.Parameters.Add(xUserfo)
        If cn.State <> ConnectionState.Open Then
            cn.Open()
        End If
        cmd.Connection = cn

        Dim ds As New DataSet()
        Dim oDadapter As New OracleDataAdapter(cmd)
        oDadapter.Fill(ds)

        Return ds
    End Function

    Public Sub ExecuteCMD(ByRef command As OracleCommand, ByRef context As TransactionContext, Optional ByVal auto_close_connection As Boolean = True)
        command.CommandType = CommandType.StoredProcedure

        'if no transactional context then manage the connection here with no transaction.
        If context Is Nothing Then
            If cn.State = ConnectionState.Closed Then
                cn.Open()
            End If
            command.Connection = cn
        Else
            command.Connection = context.NativeConnection
            command.Transaction = context.NativeTransaction
        End If

        'Dim x As Double

        Try
            command.ExecuteNonQuery()
        Catch ex As Exception
            'log
            ' Throw ex
            Throw New ServicesPLUSOracleException(ERRORCODE.ora06550, ex)
        End Try

        If context Is Nothing And auto_close_connection Then
            cn.Close()
        End If
    End Sub

    Public Function GetResults(ByRef command As OracleCommand, ByVal auto_close_connection As Boolean) As DataSet
        command.CommandType = CommandType.StoredProcedure
        Dim da As New OracleDataAdapter(command)
        Dim ds As New DataSet

        Try
            If command.Connection Is Nothing Then command.Connection = cn
            If cn.State <> ConnectionState.Open Then
                cn.Open()
                command.Connection = cn
            End If
            da.Fill(ds)
        Catch ex As Exception
            Throw New ServicesPLUSOracleException(ERRORCODE.ora06550, ex)
        End Try

        If auto_close_connection Then
            cn.Close()
        End If
        Return ds
    End Function
End Class
