Imports System.Xml
Imports Sony.US.ServicesPLUS.Core
Imports Oracle.ManagedDataAccess.Client
Imports System.Collections.Generic
Imports System.Globalization
Imports Oracle.ManagedDataAccess.Types

Public Class CatalogDataManager
    Inherits DataManager

    Private usCulture As New CultureInfo("en-US")

    'Public Overrides Function Restore(ByVal identity As String) As Object

    'End Function

    Public Overrides Sub Store(ByRef data As Object, ByRef context As TransactionContext, Optional ByVal update As Boolean = False)
        Dim item As CustomerCatalogItem = CType(data, CustomerCatalogItem)
        Dim cmd As OracleCommand = New OracleCommand

        If (Not update) Or (update And item.Dirty) Then
            If update Then
                If item.Action = CoreObject.ActionType.DELETE Then
                    cmd.CommandText = "deleteCustomerCatalogItem"
                Else
                    cmd.CommandText = "updateCustomerCatalogItem"
                End If
                cmd.Parameters.Add("xSequenceNumber", OracleDbType.Int32)
                cmd.Parameters("xSequenceNumber").Value = item.SequenceNumber
            Else
                cmd.CommandText = "addCustomerCatalogItem"
            End If

            cmd.Parameters.Add("xCustomerID", OracleDbType.Int32)
            cmd.Parameters("xCustomerID").Value = item.Customer.CustomerID
            cmd.Parameters.Add("xProductTypeID", OracleDbType.Int32)
            cmd.Parameters("xProductTypeID").Value = item.Product.Type
            cmd.Parameters.Add("xModelNumber", OracleDbType.Varchar2, 50)
            cmd.Parameters("xModelNumber").Value = item.ModelNumber
            cmd.Parameters.Add("xPartNumber", OracleDbType.Varchar2, 50)
            cmd.Parameters("xPartNumber").Value = item.Product.PartNumber
            cmd.Parameters.Add("xPartDescription", OracleDbType.Varchar2, 200)
            cmd.Parameters("xPartDescription").Value = item.Product.Description
            cmd.Parameters.Add("xListPrice", OracleDbType.Double)
            cmd.Parameters("xListPrice").Value = item.Product.ListPrice
            cmd.Parameters.Add("xReminderStartDate", OracleDbType.Date)
            cmd.Parameters("xReminderStartDate").Value = item.ReminderStartDate
            cmd.Parameters.Add("xRecurrencePattern", OracleDbType.Int32)
            cmd.Parameters("xRecurrencePattern").Value = item.TheRecurrencePattern
            cmd.Parameters.Add("xRecurrenceDay", OracleDbType.Int32)
            cmd.Parameters("xRecurrenceDay").Value = item.TheRecurrenceDay

            cmd.Parameters.Add(New OracleParameter("retVal", OracleDbType.Decimal, ParameterDirection.Output))
            ExecuteCMD(cmd, context)

            item.Action = CoreObject.ActionType.RESTORE

            If Not CType(cmd.Parameters("retVal").Value, INullable).IsNull Then
                item.SequenceNumber = cmd.Parameters("retVal").Value.ToString()
            End If
        End If

        item.ReturnDeletedObjects = False
    End Sub

    Public Function GetMyCatalog(ByVal customer As Customer, ByRef objGlobalData As GlobalData) As CustomerCatalogItem()
        Dim doc As XmlDocument
        Dim cmd As New OracleCommand("getCustomerCatalogItems")

        cmd.Parameters.Add("xCustomerID", OracleDbType.Int32)
        cmd.Parameters("xCustomerID").Value = customer.CustomerID

        cmd.Parameters.Add("p_Sales_Org", OracleDbType.Char)
        cmd.Parameters("p_Sales_Org").Value = objGlobalData.SalesOrganization

        cmd.Parameters.Add("p_Language_Id", OracleDbType.Char)
        cmd.Parameters("p_Language_Id").Value = objGlobalData.Language

        doc = GetResults(cmd)

        'internalize
        Dim nodes As XmlNodeList = doc.DocumentElement.ChildNodes
        Dim item As CustomerCatalogItem
        Dim item_list As New ArrayList

        For Each node As XmlNode In nodes
            item = New CustomerCatalogItem With {
                .Customer = customer,
                .SequenceNumber = If(node("SEQUENCENUMBER")?.InnerText, ""),
                .ModelNumber = node("MODELNUMBER")?.InnerText,
                .ReminderStartDate = Convert.ToDateTime(If(node("REMINDERSTARTDATE")?.InnerText, "2001-01-01"), usCulture),
                .TheRecurrencePattern = node("RECURRENCEPATTERN")?.InnerText,
                .TheRecurrenceDay = node("RECURRENCEDAY")?.InnerText,
                .DateAdded = Convert.ToDateTime(If(node("UPDATEDATE")?.InnerText, "2001-01-01"), usCulture)
            }

            Select Case node("PRODUCTTYPEID").InnerText
                Case "0"
                    item.Product = New Part
                Case "1"
                    item.Product = New Software
                Case "2"
                    item.Product = New Kit
            End Select
            item.Product.PartNumber = node("PARTNUMBER")?.InnerText
            ' ASleight - Should we add this in? We would need to update ViewMyCatalog as well, to add the extra column
            'item.Product.ReplacementPartNumber = If(node("REPLACEMENTPARTNUMBER")?.InnerText, "")
            item.Product.Description = node("PARTDESCRIPTION")?.InnerText
            item.Product.ListPrice = Convert.ToDouble(If(node("LISTPRICE")?.InnerText, "0.0"), usCulture)

            item.Action = CoreObject.ActionType.RESTORE

            item_list.Add(item)
        Next

        Dim items(item_list.Count - 1) As CustomerCatalogItem
        item_list.CopyTo(items)

        Return items
    End Function

    Public Function GetMyReminder(ByVal customer As Customer) As CustomerCatalogItem()
        Dim doc As XmlDocument
        Dim nodes As XmlNodeList
        Dim item As CustomerCatalogItem
        Dim item_list As New ArrayList
        Dim cmd As New OracleCommand("getCustomerCatalogReminder")

        cmd.Parameters.Add("xCustomerID", OracleDbType.Int32)
        cmd.Parameters("xCustomerID").Value = customer.CustomerID

        doc = GetResults(cmd)
        nodes = doc.DocumentElement.ChildNodes
        For Each node As XmlNode In nodes
            item = New CustomerCatalogItem With {
                .ReminderStartDate = Convert.ToDateTime(If(node("REMINDERSTARTDATE")?.InnerText, "2001-01-01"), usCulture),
                .TheRecurrencePattern = If(node("RECURRENCEPATTERN")?.InnerText, ""),
                .TheRecurrenceDay = If(node("RECURRENCEDAY")?.InnerText, "")
            }
            item_list.Add(item)
        Next

        Dim items(item_list.Count - 1) As CustomerCatalogItem
        item_list.CopyTo(items)

        Return items
    End Function

    Public Function SearchRepairStatus(ByVal xModelNumber As String, ByVal xSerialNumber As String, ByVal xNotificationNumber As String, ByVal xCustomerPhoneNumber As String, ByVal xCUSTOMERID As Integer _
            , ByVal xCUSTOMERSEQUENCENUMBER As Integer, ByVal xHTTP_X_FORWARDED_FOR As String, ByVal xREMOTE_ADDR As String, ByVal xHTTP_REFERER As String _
            , ByVal xHTTP_URL As String, ByVal xHTTP_USER_AGENT As String, ByRef lstConfModels As List(Of String), ByRef objGlobalData As GlobalData) As DataSet

        Dim cmd As New OracleCommand("REPAIRSTATUS.GETREPAIRSTATUS")

        cmd.Parameters.Add("xModelNumber", OracleDbType.Varchar2)
        cmd.Parameters("xModelNumber").Value = xModelNumber

        cmd.Parameters.Add("xSerialNumber", OracleDbType.Varchar2)
        cmd.Parameters("xSerialNumber").Value = xSerialNumber

        cmd.Parameters.Add("xNotificationNumber", OracleDbType.Varchar2)
        cmd.Parameters("xNotificationNumber").Value = xNotificationNumber

        cmd.Parameters.Add("xCustomerPhoneNumber", OracleDbType.Varchar2)
        cmd.Parameters("xCustomerPhoneNumber").Value = xCustomerPhoneNumber

        cmd.Parameters.Add("xCUSTOMERID", OracleDbType.Int32)
        cmd.Parameters("xCUSTOMERID").Value = xCUSTOMERID

        cmd.Parameters.Add("xCUSTOMERSEQUENCENUMBER", OracleDbType.Int32)
        cmd.Parameters("xCUSTOMERSEQUENCENUMBER").Value = xCUSTOMERSEQUENCENUMBER

        cmd.Parameters.Add("xHTTP_X_FORWARDED_FOR", OracleDbType.Varchar2)
        cmd.Parameters("xHTTP_X_FORWARDED_FOR").Value = xHTTP_X_FORWARDED_FOR

        cmd.Parameters.Add("xREMOTE_ADDR", OracleDbType.Varchar2)
        cmd.Parameters("xREMOTE_ADDR").Value = xREMOTE_ADDR

        cmd.Parameters.Add("xHTTP_REFERER", OracleDbType.Varchar2)
        cmd.Parameters("xHTTP_REFERER").Value = xHTTP_REFERER

        cmd.Parameters.Add("xHTTP_URL", OracleDbType.Varchar2)
        cmd.Parameters("xHTTP_URL").Value = xHTTP_URL

        cmd.Parameters.Add("xHTTP_USER_AGENT", OracleDbType.Varchar2)
        cmd.Parameters("xHTTP_USER_AGENT").Value = xHTTP_USER_AGENT

        cmd.Parameters.Add("xSalesOrg", OracleDbType.Varchar2)
        cmd.Parameters("xSalesOrg").Value = objGlobalData.SalesOrganization

        cmd.Parameters.Add("xConfModels", OracleDbType.RefCursor, ParameterDirection.Output)
        cmd.Parameters.Add("xRepairStatusResult", OracleDbType.RefCursor, ParameterDirection.Output)

        lstConfModels = New List(Of String)
        Return GetResults(cmd, True, lstConfModels)
    End Function

    Public Function SearchOperationManual(ByVal xModelNumber As String, ByVal xCUSTOMERID As Integer _
         , ByVal xCUSTOMERSEQUENCENUMBER As Integer, ByVal xHTTP_X_FORWARDED_FOR As String, ByVal xREMOTE_ADDR As String, ByVal xHTTP_REFERER As String _
         , ByVal xHTTP_URL As String, ByVal xHTTP_USER_AGENT As String) As DataSet

        Dim cmd As New OracleCommand("OPERATIONMANUAL.GETOPERATIONMANULABYMODEL")

        cmd.Parameters.Add("xModelNumber", OracleDbType.Varchar2)
        cmd.Parameters("xModelNumber").Value = "%" + xModelNumber.Trim.ToUpper() + "%"

        cmd.Parameters.Add("xCUSTOMERID", OracleDbType.Int32)
        cmd.Parameters("xCUSTOMERID").Value = xCUSTOMERID

        cmd.Parameters.Add("xCUSTOMERSEQUENCENUMBER", OracleDbType.Int32)
        cmd.Parameters("xCUSTOMERSEQUENCENUMBER").Value = xCUSTOMERSEQUENCENUMBER

        cmd.Parameters.Add("xHTTP_X_FORWARDED_FOR", OracleDbType.Varchar2)
        cmd.Parameters("xHTTP_X_FORWARDED_FOR").Value = xHTTP_X_FORWARDED_FOR

        cmd.Parameters.Add("xREMOTE_ADDR", OracleDbType.Varchar2)
        cmd.Parameters("xREMOTE_ADDR").Value = xREMOTE_ADDR

        cmd.Parameters.Add("xHTTP_REFERER", OracleDbType.Varchar2)
        cmd.Parameters("xHTTP_REFERER").Value = xHTTP_REFERER

        cmd.Parameters.Add("xHTTP_URL", OracleDbType.Varchar2)
        cmd.Parameters("xHTTP_URL").Value = xHTTP_URL

        cmd.Parameters.Add("xHTTP_USER_AGENT", OracleDbType.Varchar2)
        cmd.Parameters("xHTTP_USER_AGENT").Value = xHTTP_USER_AGENT

        cmd.Parameters.Add("xManual", OracleDbType.RefCursor, ParameterDirection.Output)

        Return GetResults(cmd, True)
    End Function

    Public Function getConflictedModels(ByVal xModelNumber As String) As DataSet
        Dim cmd As New OracleCommand("getnewmodels")

        cmd.Parameters.Add("p_modelid", OracleDbType.Varchar2, 200)
        cmd.Parameters("p_modelid").Value = xModelNumber

        cmd.Parameters.Add("rc_confmodels", OracleDbType.RefCursor, ParameterDirection.Output)

        Return GetResults(cmd, True)

    End Function

End Class
