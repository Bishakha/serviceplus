
Imports Oracle.ManagedDataAccess.Client

Public Class TransactionContext

    Private m_con As OracleConnection
    Private m_txn As OracleTransaction

    Public Sub New(ByRef con As OracleConnection, ByRef txn As OracleTransaction)
        m_con = con
        m_txn = txn
    End Sub

    Public Sub CommitTransaction()
        m_txn.Commit()
        m_con.Close()
    End Sub

    Public Sub RollbackTransaction()
        m_txn.Rollback()
        m_con.Close()
    End Sub

    Public ReadOnly Property NativeTransaction() As OracleTransaction
        Get
            Return m_txn
        End Get
    End Property

    Public ReadOnly Property NativeConnection() As OracleConnection
        Get
            Return m_con
        End Get
    End Property

End Class
