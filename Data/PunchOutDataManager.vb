Imports System.Xml
Imports Sony.US.ServicesPLUS.Core
Imports Oracle.ManagedDataAccess.Client
Imports Sony.US.SIAMUtilities
Imports ServicesPlusException
Imports Oracle.ManagedDataAccess.Types

Public Class PunchOutDataManager
    Inherits DataManager
    Public Sub New()

    End Sub

    Public Function GetPunchOutRequestData(ByVal QueryStringSesisonID As String) As DataSet
        Dim cmd As New OracleCommand
        cmd.CommandText = "PUNCHOUTMANAGER.GETPUNCHOUTREQUESTDATA"
        cmd.Parameters.Add("xPUNCHOUTBROWSERID", OracleDbType.Varchar2, 40)
        cmd.Parameters("xPUNCHOUTBROWSERID").Value = QueryStringSesisonID

        Dim outparam As New OracleParameter("xPUNCHOUTREQUESTDATA", OracleDbType.RefCursor)
        outparam.Direction = ParameterDirection.Output

        cmd.Parameters.Add(outparam)

        Try
            Return GetResults(cmd, True)
        Catch ex As Exception
            'Throw ex
            Throw New ServicesPLUSOracleException(ERRORCODE.ora06550, ex)
        End Try
    End Function

    Public Function IsPunchOutSession(ByVal BrowserSesisonID As String) As Boolean
        Dim cmd As New OracleCommand
        cmd.CommandText = "PUNCHOUTMANAGER.ISPUNCHOUTSESSION"
        cmd.Parameters.Add("xPUNCHOUTBROWSERID", OracleDbType.Varchar2, 40)
        cmd.Parameters("xPUNCHOUTBROWSERID").Value = BrowserSesisonID

        Dim outparam As New OracleParameter("xPUNCHOUTSESSION", OracleDbType.Int16)
        outparam.Direction = ParameterDirection.Output

        cmd.Parameters.Add(outparam)

        Try
            ExecuteCMD(cmd, Nothing)
            If Not CType(cmd.Parameters("xPUNCHOUTSESSION")?.Value, INullable).IsNull Then
                If Convert.ToInt16(cmd.Parameters("xPUNCHOUTSESSION").Value.ToString()) > 0 Then
                    Return True
                Else
                    Return False
                End If
            Else
                Return False
            End If
        Catch ex As Exception
            ' Throw ex
            Throw New ServicesPLUSOracleException(ERRORCODE.ora06550, ex)
        End Try
    End Function

    '''Returns QueryStringSessionID created by Oracle, a GUID
    Public Function SavePunchOutRequestData(ByVal CXMLIDENTITY As String, ByVal CXMLSHAREDSECRET As String, ByVal BROWSERSESSIONID As String, _
                ByVal CXMLREQUESTTEXT As String, ByVal REQUESTINGMACHINEIP As String, ByVal REQUESTINGURL As String, _
               ByVal POSTTOURL As String, ByRef context As TransactionContext) As String

        Dim cmd As New OracleCommand
        cmd.CommandText = "PUNCHOUTMANAGER.SAVEPUNCHOUTREQUESTDATA"

        cmd.Parameters.Add("xCXMLIDENTITY", OracleDbType.Varchar2, 50)
        cmd.Parameters("xCXMLIDENTITY").Value = CXMLIDENTITY
        cmd.Parameters.Add("xCXMLSHAREDSECRET", OracleDbType.Varchar2, 50)
        cmd.Parameters("xCXMLSHAREDSECRET").Value = String.Empty
        cmd.Parameters.Add("xBROWSERSESSIONID", OracleDbType.Varchar2, 50)
        cmd.Parameters("xBROWSERSESSIONID").Value = BROWSERSESSIONID
        cmd.Parameters.Add("xCXMLREQUESTTEXT", OracleDbType.Varchar2, 4000)
        cmd.Parameters("xCXMLREQUESTTEXT").Value = String.Empty
        cmd.Parameters.Add("xREQUESTINGMACHINEIP", OracleDbType.Varchar2, 15)
        cmd.Parameters("xREQUESTINGMACHINEIP").Value = REQUESTINGMACHINEIP
        cmd.Parameters.Add("xREQUESTINGURL", OracleDbType.Varchar2, 256)
        cmd.Parameters("xREQUESTINGURL").Value = REQUESTINGURL
        cmd.Parameters.Add("xPOSTTOURL", OracleDbType.Varchar2, 256)
        cmd.Parameters("xPOSTTOURL").Value = POSTTOURL


        Dim outparam As New OracleParameter("xPUNCHOUTQUERYSTRINGID", OracleDbType.Varchar2, 40)
        outparam.Direction = ParameterDirection.Output

        cmd.Parameters.Add(outparam)

        Try
            ExecuteCMD(cmd, context)
            If Not CType(cmd.Parameters("xPUNCHOUTQUERYSTRINGID")?.Value, INullable).IsNull Then
                Return cmd.Parameters("xPUNCHOUTQUERYSTRINGID").Value.ToString()
            Else
                'Throw New ApplicationException("New Session ID is blank.")
                Throw New ServicesPLUSOracleException(ERRORCODE.ora06550)
            End If
        Catch ex As Exception
            'Throw ex
            Throw New ServicesPLUSOracleException(ERRORCODE.ora06550, ex)
        End Try
    End Function

    Public Sub UpdatePunchOutRequestData(ByVal BrowserID As String, ByVal AciveStatus As String, ByVal PunchoutSessionID As String)
        Dim cmd As New OracleCommand
        cmd.CommandText = "PUNCHOUTMANAGER.UPDATEPUNCHOUTREQUESTDATA"

        cmd.Parameters.Add("xBROWSERID", OracleDbType.Varchar2, 40)
        cmd.Parameters("xBROWSERID").Value = BrowserID
        cmd.Parameters.Add("xACTIVESTATUS", OracleDbType.Varchar2, 40)
        cmd.Parameters("xACTIVESTATUS").Value = AciveStatus
        cmd.Parameters.Add("xPUNCHOUTSESSIONID", OracleDbType.Varchar2, 40)
        cmd.Parameters("xPUNCHOUTSESSIONID").Value = PunchoutSessionID

        Dim outparam As New OracleParameter("xSUCCESS", OracleDbType.Int16)
        outparam.Direction = ParameterDirection.Output
        cmd.Parameters.Add(outparam)

        Try
            ExecuteCMD(cmd, Nothing)
        Catch ex As Exception
            'Throw ex
            Throw New ServicesPLUSOracleException(ERRORCODE.ora06550, ex)
        End Try
    End Sub
End Class
