Imports System.Xml
Imports Sony.US.ServicesPLUS.Core
Imports Oracle.ManagedDataAccess.Client
Imports Oracle.ManagedDataAccess.Types

Public Class CourseDataManager
    Inherits DataManager

    ' Functions for maintaining CourseParticipants
    Public Function GetCourseParticipants() As CourseParticipant()
        Dim doc As XmlDocument
        Dim cmd As New OracleCommand("GetTrainingParticipants")

        doc = GetResults(cmd)

        'internalize
        Dim root As XmlElement = doc.DocumentElement
        Dim nodes As XmlNodeList = root.ChildNodes
        Dim elem As XmlElement
        Dim item As CourseParticipant
        Dim item_list As New ArrayList
        Dim j As Integer

        For j = 0 To nodes.Count - 1
            item = New CourseParticipant

            elem = nodes(j)("RESERVATIONNUMBER")
            If elem IsNot Nothing Then item.ReservationNumber = elem.InnerText()

            elem = nodes(j)("FIRSTNAME")
            If elem IsNot Nothing Then item.FirstName = elem.InnerText()

            elem = nodes(j)("LASTNAME")
            If elem IsNot Nothing Then item.LastName = elem.InnerText()

            elem = nodes(j)("EMAIL")
            If elem IsNot Nothing Then item.EMail = elem.InnerText()

            elem = nodes(j)("PHONE")
            If elem IsNot Nothing Then item.Phone = elem.InnerText()

            elem = nodes(j)("STATUS")
            If elem IsNot Nothing Then item.Status = elem.InnerText()

            elem = nodes(j)("DATERESERVED")
            If elem IsNot Nothing Then item.DateReserved = elem.InnerText()

            elem = nodes(j)("DATECANCELED")
            If elem IsNot Nothing Then item.DateCanceled = elem.InnerText()

            elem = nodes(j)("DATEWAITED")
            If elem IsNot Nothing Then item.DateWaited = elem.InnerText()

            elem = nodes(j)("EXTENSION")
            If elem IsNot Nothing Then item.Extension = elem.InnerText()

            elem = nodes(j)("FAX")
            If elem IsNot Nothing Then item.Fax = elem.InnerText()

            elem = nodes(j)("NOTES")
            If elem IsNot Nothing Then item.Notes = elem.InnerText()

            elem = nodes(j)("ADDRESS1")
            If elem IsNot Nothing Then item.Address1 = elem.InnerText()

            elem = nodes(j)("ADDRESS2")
            If elem IsNot Nothing Then item.Address2 = elem.InnerText()

            elem = nodes(j)("ADDRESS3")
            If elem IsNot Nothing Then item.Address3 = elem.InnerText()

            elem = nodes(j)("CITY")
            If elem IsNot Nothing Then item.City = elem.InnerText()

            elem = nodes(j)("STATE")
            If elem IsNot Nothing Then item.State = elem.InnerText()

            elem = nodes(j)("ZIP")
            If elem IsNot Nothing Then item.Zip = elem.InnerText()

            elem = nodes(j)("ISOCOUNTRYCODE")
            If elem IsNot Nothing Then item.ISOCountryCode = elem.InnerText()

            elem = nodes(j)("SEQUENCENUMBER")
            If elem IsNot Nothing Then item.SequenceNumber = Convert.ToInt16(elem.InnerText())

            elem = nodes(j)("UPDATEDSIAMID")
            If elem IsNot Nothing Then item.UpdatedSIAMID = elem.InnerText()

            elem = nodes(j)("UPDATEDATE")
            If elem IsNot Nothing Then item.UpdateDate = elem.InnerText()

            elem = nodes(j)("COMPANY")
            If elem IsNot Nothing Then item.Company = elem.InnerText()

            item_list.Add(item)
        Next

        Dim items(item_list.Count - 1) As CourseParticipant
        item_list.CopyTo(items)

        Return items
    End Function

    Public Function SearchNonClassRoomTrainingForATC(ByVal CourseNumber As String) As Training
        Dim myAdaptor As OracleDataAdapter
        Dim dataset As New DataSet
        Dim objTraining As Training = Nothing

        Dim cmd As New OracleCommand("TRAININGPLUS.ATCNonClassRoomTraining") With {
            .CommandType = CommandType.StoredProcedure,
            .BindByName = True
        }

        cmd.Parameters.Add("xCourseNumber", OracleDbType.Varchar2, 50)
        cmd.Parameters("xCourseNumber").Value = CourseNumber.ToUpper()

        cmd.Parameters.Add(New OracleParameter("xCourseCursor", OracleDbType.RefCursor, ParameterDirection.Output))

        OpenConnection()
        cmd.Connection = dbConnection

        myAdaptor = New OracleDataAdapter(cmd)
        Try
            myAdaptor.Fill(dataset)
        Catch ex As Exception
            Console.WriteLine("troubles calling partsfinder: " + ex.Message)
            Throw 'ex
        End Try
        CloseConnection()

        Try
            If dataset.Tables.Count > 0 Then
                If dataset.Tables(0).Rows.Count > 0 Then
                    With dataset.Tables(0)
                        For Each objDataRow As DataRow In .Rows
                            Select Case objDataRow("TRAININGTYPECODE").ToString()
                                Case "0" ' only for Classroom training
                                Case "1" ' only for computer based training
                                    objTraining = New Training(Product.ProductType.ComputerBasedTraining)
                                Case "2" ' deleted from tainingtype table
                                Case "3" ' only for Certification
                                    objTraining = New Training(Product.ProductType.Certificate)
                                Case "4" ' only Online Training
                                    objTraining = New Training(Product.ProductType.OnlineTraining)
                                Case Else
                            End Select

                            objTraining.ModelNumber = objDataRow("Model_ID").ToString()
                            objTraining.TrainingURL = objDataRow("URL").ToString()
                            objTraining.PartNumber = objDataRow("Part_Number").ToString()
                            objTraining.Description = objDataRow("MODEL_DESCRIPTION").ToString()
                            objTraining.FreeOfCharge = Convert.ToInt16(objDataRow("FREE_OF_CHARGE").ToString())
                            'objTraining.ListPrice = Convert.ToDouble((objDataRow("LISTPRICE").ToString())) '6994 V1
                            If Not String.IsNullOrEmpty(objDataRow("LISTPRICE").ToString()) Then
                                objTraining.ListPrice = Convert.ToDouble((objDataRow("LISTPRICE").ToString())) '6994 V1
                            Else
                                objTraining.ListPrice = 0.0
                            End If
                            Exit For
                        Next
                    End With
                End If
            End If
        Catch ex As Exception
            Throw 'ex
        End Try
        Return objTraining
    End Function

    Public Function GetCourseParticipant(ByRef ReservationNumber As String, ByRef ReservationStatus As Char) As CourseParticipant()
        Dim item As CourseParticipant
        Dim item_list As New ArrayList
        Dim doc As XmlDocument
        Dim nodes As XmlNodeList
        Dim elem As XmlElement
        Dim j As Integer

        Dim cmd As New OracleCommand("GetTrainingParticipant")

        cmd.Parameters.Add("xReservationNumber", OracleDbType.Varchar2)
        cmd.Parameters("xReservationNumber").Value = ReservationNumber
        cmd.Parameters.Add("xReservationStatus", OracleDbType.Char)
        cmd.Parameters("xReservationStatus").Value = ReservationStatus

        doc = GetResults(cmd)

        'internalize
        nodes = doc.DocumentElement.ChildNodes

        For j = 0 To nodes.Count - 1
            item = New CourseParticipant

            elem = nodes(j)("RESERVATIONNUMBER")
            If elem IsNot Nothing Then item.ReservationNumber = elem.InnerText()

            elem = nodes(j)("FIRSTNAME")
            If elem IsNot Nothing Then item.FirstName = elem.InnerText()

            elem = nodes(j)("LASTNAME")
            If elem IsNot Nothing Then item.LastName = elem.InnerText()

            elem = nodes(j)("EMAIL")
            If elem IsNot Nothing Then item.EMail = elem.InnerText()

            elem = nodes(j)("PHONE")
            If elem IsNot Nothing Then item.Phone = elem.InnerText()

            elem = nodes(j)("STATUS")
            If elem IsNot Nothing Then item.Status = elem.InnerText()

            elem = nodes(j)("DATERESERVED")
            If elem IsNot Nothing Then item.DateReserved = elem.InnerText()

            elem = nodes(j)("DATECANCELED")
            If elem IsNot Nothing Then item.DateCanceled = elem.InnerText()

            elem = nodes(j)("DATEWAITED")
            If elem IsNot Nothing Then item.DateWaited = elem.InnerText()

            elem = nodes(j)("EXTENSION")
            If elem IsNot Nothing Then item.Extension = elem.InnerText()

            elem = nodes(j)("FAX")
            If elem IsNot Nothing Then item.Fax = elem.InnerText()

            elem = nodes(j)("NOTES")
            If elem IsNot Nothing Then item.Notes = elem.InnerText()

            elem = nodes(j)("ADDRESS1")
            If elem IsNot Nothing Then item.Address1 = elem.InnerText()

            elem = nodes(j)("ADDRESS2")
            If elem IsNot Nothing Then item.Address2 = elem.InnerText()

            elem = nodes(j)("ADDRESS3")
            If elem IsNot Nothing Then item.Address3 = elem.InnerText()

            elem = nodes(j)("CITY")
            If elem IsNot Nothing Then item.City = elem.InnerText()

            elem = nodes(j)("STATE")
            If elem IsNot Nothing Then item.State = elem.InnerText()

            elem = nodes(j)("ZIP")
            If elem IsNot Nothing Then item.Zip = elem.InnerText()

            elem = nodes(j)("ISOCOUNTRYCODE")
            If elem IsNot Nothing Then item.ISOCountryCode = elem.InnerText()

            elem = nodes(j)("COUNTRY")
            If elem IsNot Nothing Then item.Country = elem.InnerText()

            elem = nodes(j)("SEQUENCENUMBER")
            If elem IsNot Nothing Then item.SequenceNumber = Convert.ToInt16(elem.InnerText())

            elem = nodes(j)("UPDATEDSIAMID")
            If elem IsNot Nothing Then item.UpdatedSIAMID = elem.InnerText()

            elem = nodes(j)("UPDATEDATE")
            If elem IsNot Nothing Then item.UpdateDate = elem.InnerText()

            elem = nodes(j)("COMPANY")
            If elem IsNot Nothing Then item.Company = elem.InnerText()

            item_list.Add(item)
        Next

        Dim items(item_list.Count - 1) As CourseParticipant
        item_list.CopyTo(items)

        Return items
    End Function

    Public Sub StoreParticipant(ByRef Participant As CourseParticipant, ByRef context As TransactionContext, Optional ByVal Action As CoreObject.ActionType = CoreObject.ActionType.ADD)
        Dim cmd As New OracleCommand

        Select Case Action
            Case CoreObject.ActionType.ADD
                cmd.CommandText = "AddTrainingParticipant"
                cmd.Parameters.Add("xReservationNumber", OracleDbType.Varchar2, 15)
                cmd.Parameters("xReservationNumber").Value = Participant.ReservationNumber
                cmd.Parameters.Add("xFirstName", OracleDbType.Varchar2, 50)
                cmd.Parameters("xFirstName").Value = Participant.FirstName
                cmd.Parameters.Add("xLastName", OracleDbType.Varchar2, 50)
                cmd.Parameters("xLastName").Value = Participant.LastName
                cmd.Parameters.Add("xEMail", OracleDbType.Varchar2, 100)
                cmd.Parameters("xEMail").Value = Participant.EMail
                cmd.Parameters.Add("xPhone", OracleDbType.Varchar2, 20)
                cmd.Parameters("xPhone").Value = Participant.Phone
                cmd.Parameters.Add("xStatus", OracleDbType.Char)
                cmd.Parameters("xStatus").Value = Participant.Status
                If Participant.Extension IsNot Nothing Then
                    cmd.Parameters.Add("xExtension", OracleDbType.Varchar2, 6)
                    cmd.Parameters("xExtension").Value = Participant.Extension
                End If
                cmd.Parameters.Add("xFax", OracleDbType.Varchar2, 20)
                cmd.Parameters("xFax").Value = Participant.Fax
                If Participant.Notes IsNot Nothing Then
                    cmd.Parameters.Add("xNotes", OracleDbType.Varchar2, 255)
                    cmd.Parameters("xNotes").Value = Participant.Notes
                End If
                cmd.Parameters.Add("xAddress1", OracleDbType.Varchar2, 50)
                cmd.Parameters("xAddress1").Value = Participant.Address1
                If Participant.Address2 IsNot Nothing Then
                    cmd.Parameters.Add("xAddress2", OracleDbType.Varchar2, 50)
                    cmd.Parameters("xAddress2").Value = Participant.Address2
                End If
                If Participant.Address3 IsNot Nothing Then
                    cmd.Parameters.Add("xAddress3", OracleDbType.Varchar2, 50)
                    cmd.Parameters("xAddress3").Value = Participant.Address3
                End If
                cmd.Parameters.Add("xCity", OracleDbType.Varchar2, 50)
                cmd.Parameters("xCity").Value = Participant.City
                cmd.Parameters.Add("xState", OracleDbType.Varchar2, 2)
                cmd.Parameters("xState").Value = Participant.State
                cmd.Parameters.Add("xZip", OracleDbType.Varchar2, 10)
                cmd.Parameters("xZip").Value = Participant.Zip
                cmd.Parameters.Add("xISOCountryCode", OracleDbType.Varchar2, 3)
                cmd.Parameters("xISOCountryCode").Value = Participant.ISOCountryCode
                cmd.Parameters.Add("xUpdatedSIAMID", OracleDbType.Varchar2, 16)
                cmd.Parameters("xUpdatedSIAMID").Value = Participant.UpdatedSIAMID
                cmd.Parameters.Add("xCompany", OracleDbType.Varchar2, 100)
                cmd.Parameters("xCompany").Value = Participant.Company

                cmd.Parameters.Add(New OracleParameter("retVal", OracleDbType.Int32, ParameterDirection.Output))
            Case CoreObject.ActionType.UPDATE
                cmd.CommandText = "UpdateTrainingParticipant"
                cmd.Parameters.Add("xReservationNumber", OracleDbType.Varchar2, 15)
                cmd.Parameters("xReservationNumber").Value = Participant.ReservationNumber
                cmd.Parameters.Add("xSequenceNumber", OracleDbType.Int32)
                cmd.Parameters("xSequenceNumber").Value = Participant.SequenceNumber
                cmd.Parameters.Add("xFirstName", OracleDbType.Varchar2, 50)
                cmd.Parameters("xFirstName").Value = Participant.FirstName
                cmd.Parameters.Add("xLastName", OracleDbType.Varchar2, 50)
                cmd.Parameters("xLastName").Value = Participant.LastName
                cmd.Parameters.Add("xEMail", OracleDbType.Varchar2, 100)
                cmd.Parameters("xEMail").Value = Participant.EMail
                cmd.Parameters.Add("xPhone", OracleDbType.Varchar2, 20)
                cmd.Parameters("xPhone").Value = Participant.Phone
                cmd.Parameters.Add("xStatus", OracleDbType.Char)
                cmd.Parameters("xStatus").Value = Participant.Status
                If Participant.Extension IsNot Nothing Then
                    cmd.Parameters.Add("xExtension", OracleDbType.Varchar2, 6)
                    cmd.Parameters("xExtension").Value = Participant.Extension
                End If
                If Participant.Fax IsNot Nothing Then
                    cmd.Parameters.Add("xFax", OracleDbType.Varchar2, 20)
                    cmd.Parameters("xFax").Value = Participant.Fax
                End If
                If Participant.Notes IsNot Nothing Then
                    cmd.Parameters.Add("xNotes", OracleDbType.Varchar2, 255)
                    cmd.Parameters("xNotes").Value = Participant.Notes
                End If
                cmd.Parameters.Add("xAddress1", OracleDbType.Varchar2, 50)
                cmd.Parameters("xAddress1").Value = Participant.Address1
                If Participant.Address2 IsNot Nothing Then
                    cmd.Parameters.Add("xAddress2", OracleDbType.Varchar2, 50)
                    cmd.Parameters("xAddress2").Value = Participant.Address2
                End If
                If Participant.Address3 IsNot Nothing Then
                    cmd.Parameters.Add("xAddress3", OracleDbType.Varchar2, 50)
                    cmd.Parameters("xAddress3").Value = Participant.Address3
                End If
                cmd.Parameters.Add("xCity", OracleDbType.Varchar2, 50)
                cmd.Parameters("xCity").Value = Participant.City
                cmd.Parameters.Add("xState", OracleDbType.Varchar2, 2)
                cmd.Parameters("xState").Value = Participant.State
                cmd.Parameters.Add("xZip", OracleDbType.Varchar2, 10)
                cmd.Parameters("xZip").Value = Participant.Zip
                cmd.Parameters.Add("xCountry", OracleDbType.Varchar2, 50)
                cmd.Parameters("xCountry").Value = Participant.Country
                cmd.Parameters.Add("xUpdatedSIAMID", OracleDbType.Varchar2, 16)
                cmd.Parameters("xUpdatedSIAMID").Value = Participant.UpdatedSIAMID
                cmd.Parameters.Add("xCompany", OracleDbType.Varchar2, 100)
                cmd.Parameters("xCompany").Value = Participant.Company
            Case CoreObject.ActionType.DELETE
                cmd.CommandText = "DeleteTrainingParticipant"
                'cmd.Parameters.Add("xCode", OracleDbType.Int32)
                'cmd.Parameters("xCode").Value = Participant.Code
        End Select

        ExecuteCMD(cmd, context)

        If (Action = CoreObject.ActionType.ADD) Then
            If Not CType(cmd.Parameters("retVal").Value, INullable).IsNull Then
                'ServicesPlusException.Utilities.LogDebug($"CourseDataManager.StoreParticipant - Add retVal = {cmd.Parameters("retVal").Value}")
                Participant.SequenceNumber = CType(cmd.Parameters("retVal").Value.ToString(), Integer)
            End If
        End If
    End Sub

    ' Functions for maintaining Course
    Public Function GetCourseTitles(ByRef defaultValue As String) As String()
        Dim doc As XmlDocument
        Dim cmd As New OracleCommand("getTrainingCourseTitle")

        doc = GetResults(cmd)

        'internalize
        Dim root As XmlElement = doc.DocumentElement
        Dim nodes As XmlNodeList = root.ChildNodes
        Dim item_list As New ArrayList
        Dim j As Integer

        item_list.Add(defaultValue)
        For j = 0 To nodes.Count - 1
            item_list.Add(nodes(j)("TITLE").InnerText)
        Next

        Dim items(item_list.Count - 1) As String
        item_list.CopyTo(items)

        Return items
    End Function

    Public Function GetActiveCourse(ByVal Status As String, ByVal Hide As String) As Course()
        Dim doc As XmlDocument
        Dim cmd As New OracleCommand("GETACTIVETRAININGCOURSE")

        cmd.Parameters.Add("xStatus", OracleDbType.Varchar2, 1)
        cmd.Parameters("xStatus").Value = Status

        cmd.Parameters.Add("xHide", OracleDbType.Varchar2, 1)
        cmd.Parameters("xHide").Value = Hide

        doc = GetResults(cmd)

        'internalize
        Dim root As XmlElement = doc.DocumentElement
        Dim nodes As XmlNodeList = root.ChildNodes
        Dim item As Course
        Dim item_list As New ArrayList
        Dim j As Integer

        For j = 0 To nodes.Count - 1
            item = New Course With {
                .Number = If(nodes(j)("COURSENUMBER")?.InnerText, ""),
                .Title = If(nodes(j)("TITLE")?.InnerText, ""),
                .Description = If(nodes(j)("DESCRIPTION")?.InnerText, ""),
                .Notes = If(nodes(j)("NOTES")?.InnerText, ""),
                .SequenceNumber = If(nodes(j)("SEQUENCENUMBER")?.InnerText, ""),
                .StatusCode = If(nodes(j)("STATUSCODE")?.InnerText, ""),
                .ListPrice = If(nodes(j)("PRICE")?.InnerText, ""),
                .UpdateDate = If(nodes(j)("UPDATEDATE")?.InnerText, ""),
                .PreRequisite = If(nodes(j)("PREREQUISITES")?.InnerText, ""),
                .Model = If(nodes(j)("MODEL")?.InnerText, ""),
                .URL = If(nodes(j)("URL")?.InnerText, "")
            }
            item.MinorCategory.Code = If(nodes(j)("MINORCATEGORYCODE")?.InnerText, "")
            item.Type.Code = If(nodes(j)("TYPECODE")?.InnerText, "")

            item.StatusCode = Status
            item.Hide = Hide
            item_list.Add(item)
        Next

        Dim items(item_list.Count - 1) As Course
        item_list.CopyTo(items)

        Return items
    End Function

    Public Function GetCourse() As Course()
        Dim doc As XmlDocument
        Dim cmd As New OracleCommand("getTrainingCourse")

        doc = GetResults(cmd)

        'internalize
        Dim nodes As XmlNodeList = doc.DocumentElement.ChildNodes
        Dim item As Course
        Dim item_list As New ArrayList
        Dim j As Integer

        For j = 0 To nodes.Count - 1
            item = New Course With {
                .Number = If(nodes(j)("COURSENUMBER")?.InnerText, ""),
                .Title = If(nodes(j)("TITLE")?.InnerText, ""),
                .Description = If(nodes(j)("DESCRIPTION")?.InnerText, ""),
                .Notes = If(nodes(j)("NOTES")?.InnerText, ""),
                .SequenceNumber = If(nodes(j)("SEQUENCENUMBER")?.InnerText, ""),
                .StatusCode = If(nodes(j)("STATUSCODE")?.InnerText, ""),
                .ListPrice = If(nodes(j)("PRICE")?.InnerText, ""),
                .UpdateDate = If(nodes(j)("UPDATEDATE")?.InnerText, ""),
                .PreRequisite = If(nodes(j)("PREREQUISITES")?.InnerText, ""),
                .Model = If(nodes(j)("MODEL")?.InnerText, "")
            }
            item.MinorCategory.Code = If(nodes(j)("MINORCATEGORYCODE")?.InnerText, "")
            item.Type.Code = If(nodes(j)("TYPECODE")?.InnerText, "")
            item_list.Add(item)
        Next

        Dim items(item_list.Count - 1) As Course
        item_list.CopyTo(items)

        Return items
    End Function

    'TODO: This is a pending method to be completed
    Public Function GetClassRoomTrainingCourse() As DataSet
        Dim doc As DataSet
        Dim cmd As New OracleCommand("TRAININGPLUS.GetClassCourseList")

        Dim outparam As New OracleParameter("xCourseCursor", OracleDbType.RefCursor, ParameterDirection.Output)
        cmd.Parameters.Add(outparam)

        doc = GetResults(cmd, True)

        Return doc
    End Function

    Public Function GetCountry() As CourseCountry()
        Dim doc As XmlDocument
        Dim cmd As New OracleCommand("getCountry")

        doc = GetResults(cmd)

        'internalize
        Dim root As XmlElement = doc.DocumentElement
        Dim nodes As XmlNodeList = root.ChildNodes
        Dim item As CourseCountry
        Dim item_list As New ArrayList
        Dim j As Integer

        For j = 0 To nodes.Count - 1
            item = New CourseCountry With {
                .Code = If(nodes(j)("CODE")?.InnerText, ""),
                .Country = If(nodes(j)("COUNTRY")?.InnerText, ""),
                .Description = If(nodes(j)("DESCRIPTION")?.InnerText, ""),
                .TerritoryName = If(nodes(j)("TERRITORYNAME")?.InnerText, "")
            }
            item_list.Add(item)
        Next

        Dim items(item_list.Count - 1) As CourseCountry
        item_list.CopyTo(items)

        Return items
    End Function

    Public Function GetCourse(ByVal Model As String) As Course()
        Dim doc As XmlDocument
        Dim cmd As New OracleCommand("getTrainingCourseByModel")

        doc = GetResults(cmd)

        cmd.Parameters.Add("xModel", OracleDbType.Varchar2)
        cmd.Parameters("xModel").Value = Model

        'internalize
        Dim root As XmlElement = doc.DocumentElement
        Dim nodes As XmlNodeList = root.ChildNodes
        Dim item As Course
        Dim item_list As New ArrayList
        Dim j As Integer

        For j = 0 To nodes.Count - 1
            item = New Course With {
                .Number = If(nodes(j)("CourseNumber").InnerText, ""),
                .Title = If(nodes(j)("Title").InnerText, ""),
                .Description = If(nodes(j)("Description").InnerText, ""),
                .Notes = If(nodes(j)("Notes").InnerText, ""),
                .SequenceNumber = If(nodes(j)("SequenceNumber").InnerText, ""),
                .StatusCode = If(nodes(j)("StatusCode").InnerText, ""),
                .ListPrice = If(nodes(j)("Price").InnerText, ""),
                .UpdateDate = If(nodes(j)("UpdateDate").InnerText, ""),
                .PreRequisite = If(nodes(j)("PreRequisite").InnerText, ""),
                .Model = If(nodes(j)("Model").InnerText, "")
            }
            item.MinorCategory.Description = If(nodes(j)("MinorCategory").InnerText, "")
            item.Type.Code = If(nodes(j)("TypeCode").InnerText, "")

            item_list.Add(item)
        Next

        Dim items(item_list.Count - 1) As Course
        item_list.CopyTo(items)

        Return items
    End Function

    Public Sub StoreCourse(ByRef TrainingCourse As Course, ByRef context As TransactionContext, Optional ByVal Action As CoreObject.ActionType = CoreObject.ActionType.ADD)
        Dim cmd As New OracleCommand
        Dim bCloseConn As Boolean = True

        Select Case Action
            Case CoreObject.ActionType.ADD
                cmd.CommandText = "AddTrainingCourse"
                cmd.Parameters.Add("xModel", OracleDbType.Varchar2, 4000)
                cmd.Parameters("xModel").Value = TrainingCourse.Model
                cmd.Parameters.Add("xDefaultLocationList", OracleDbType.Varchar2, 4000)
                cmd.Parameters("xDefaultLocationList").Value = TrainingCourse.DefaultLocationList

            Case CoreObject.ActionType.UPDATE
                cmd.CommandText = "UpdateTrainingCourse"
                cmd.Parameters.Add("xModel", OracleDbType.Varchar2, 4000)
                cmd.Parameters("xModel").Value = TrainingCourse.Model

                cmd.Parameters.Add("xDefaultLocationList", OracleDbType.Varchar2, 4000)
                cmd.Parameters("xDefaultLocationList").Value = TrainingCourse.DefaultLocationList

            Case CoreObject.ActionType.DELETE
                cmd.CommandText = "DeleteTrainingCourse"
                bCloseConn = False 'will be useed by subsequent operation
                'cmd.Parameters.Add("xCode", OracleDbType.Int32)
                'cmd.Parameters("xCode").Value = Participant.Code
            Case Else
                cmd.CommandText = "ActivateTrainingCourse"
        End Select
        cmd.Parameters.Add("xCourseNumber", OracleDbType.Varchar2)
        cmd.Parameters("xCourseNumber").Value = TrainingCourse.Number
        cmd.Parameters.Add("xTitle", OracleDbType.Varchar2)
        cmd.Parameters("xTitle").Value = TrainingCourse.Title
        cmd.Parameters.Add("xDescription", OracleDbType.Varchar2)
        cmd.Parameters("xDescription").Value = TrainingCourse.Description
        cmd.Parameters.Add("xTypeCode", OracleDbType.Varchar2)
        cmd.Parameters("xTypeCode").Value = TrainingCourse.Type.Code
        If Not String.IsNullOrWhiteSpace(TrainingCourse.Notes) Then
            cmd.Parameters.Add("xNotes", OracleDbType.Varchar2)
            cmd.Parameters("xNotes").Value = TrainingCourse.Notes
        End If
        cmd.Parameters.Add("xMinorCategoryCode", OracleDbType.Int32)
        cmd.Parameters("xMinorCategoryCode").Value = TrainingCourse.MinorCategory.Code
        cmd.Parameters.Add("xPrice", OracleDbType.Decimal)
        cmd.Parameters("xPrice").Value = TrainingCourse.ListPrice
        If Not String.IsNullOrWhiteSpace(TrainingCourse.PreRequisite) Then
            cmd.Parameters.Add("xPreRequisites", OracleDbType.Varchar2)
            cmd.Parameters("xPreRequisites").Value = TrainingCourse.PreRequisite
        End If
        cmd.Parameters.Add("xURL", OracleDbType.Varchar2)
        cmd.Parameters("xURL").Value = TrainingCourse.TrainingURL

        cmd.Parameters.Add(New OracleParameter("retVal", OracleDbType.Decimal, ParameterDirection.Output))

        ExecuteCMD(cmd, context, bCloseConn)

        If Not CType(cmd.Parameters("retVal").Value, INullable).IsNull Then
            If Action = CoreObject.ActionType.ADD Then
                If Convert.ToInt16(cmd.Parameters("retVal").Value.ToString()) = -1 Then
                    Throw New ApplicationException("Duplicate Course Number")
                Else
                    TrainingCourse.SequenceNumber = cmd.Parameters("retVal").Value.ToString()
                End If
            ElseIf Action = CoreObject.ActionType.UPDATE Then
                TrainingCourse.SequenceNumber = cmd.Parameters("retVal").Value.ToString()
            End If
        End If
    End Sub

    ' Functions for maintaining CourseReservation
    Public Function GetCourseReservation() As CourseReservation()
        Dim doc As XmlDocument
        Dim cmd As New OracleCommand("GetTrainingReservation")

        doc = GetResults(cmd)

        'internalize
        Dim root As XmlElement = doc.DocumentElement
        Dim nodes As XmlNodeList = root.ChildNodes
        Dim elem As XmlElement
        Dim item As CourseReservation
        Dim item_list As New ArrayList
        Dim j As Integer

        For j = 0 To nodes.Count - 1
            item = New CourseReservation

            elem = nodes(j)("PARTNUMBER")
            If elem IsNot Nothing Then item.CourseSchedule.PartNumber = elem.InnerText()

            elem = nodes(j)("RESERVATIONNUMBER")
            If elem IsNot Nothing Then item.ReservationNumber = elem.InnerText()

            elem = nodes(j)("SEQUENCENUMBER")
            If elem IsNot Nothing Then item.SequenceNumber = Convert.ToInt16(elem.InnerText())

            elem = nodes(j)("STATUSCODE")
            If elem IsNot Nothing Then item.StatusCode = Convert.ToInt16(elem.InnerText())

            elem = nodes(j)("UPDATEDATE")
            If elem IsNot Nothing Then item.UpdateDate = elem.InnerText()

            elem = nodes(j)("CUSTOMERSIAMID")
            If elem IsNot Nothing Then item.CustomerSIAMID = elem.InnerText()

            elem = nodes(j)("NOTES")
            If elem IsNot Nothing Then item.Notes = elem.InnerText()

            elem = nodes(j)("SENTINVOICEORQUOTE")
            If elem IsNot Nothing Then item.Sentinvoiceorquote = Convert.ToInt16(elem.InnerText())

            elem = nodes(j)("ADMINSIAMID")
            If elem IsNot Nothing Then item.AdminSIAMID = elem.InnerText()

            elem = nodes(j)("CUSTOMERSIAMID")
            If elem IsNot Nothing Then item.CustomerSIAMID = elem.InnerText()

            item_list.Add(item)
        Next

        Dim items(item_list.Count - 1) As CourseReservation
        item_list.CopyTo(items)

        Return items
    End Function

    Public Function GetCourseReservationByNumber(ByRef number As String) As CourseReservation
        Dim doc As XmlDocument
        Dim cmd As New OracleCommand("GetTrainingReservationByNumber")

        cmd.Parameters.Add("xReservationNumber", OracleDbType.Varchar2)
        cmd.Parameters("xReservationNumber").Value = number

        doc = GetResults(cmd)

        'internalize
        Dim root As XmlElement = doc.DocumentElement
        Dim nodes As XmlNodeList = root.ChildNodes
        Dim elem As XmlElement
        Dim item As New CourseReservation
        Dim j As Integer

        For j = 0 To nodes.Count - 1
            item = New CourseReservation

            elem = nodes(j)("COURSENUMBER")
            If elem IsNot Nothing Then item.CourseSchedule.Course.Number = elem.InnerText()

            elem = nodes(j)("LOCATION")
            If elem IsNot Nothing Then item.CourseSchedule.Location.Code = elem.InnerText()

            elem = nodes(j)("PARTNUMBER")
            If elem IsNot Nothing Then item.CourseSchedule.PartNumber = elem.InnerText()

            elem = nodes(j)("RESERVATIONNUMBER")
            If elem IsNot Nothing Then item.ReservationNumber = elem.InnerText()

            elem = nodes(j)("SEQUENCENUMBER")
            If elem IsNot Nothing Then item.SequenceNumber = Convert.ToInt16(elem.InnerText())

            elem = nodes(j)("STATUSCODE")
            If elem IsNot Nothing Then item.StatusCode = Convert.ToInt16(elem.InnerText())

            elem = nodes(j)("UPDATEDATE")
            If elem IsNot Nothing Then item.UpdateDate = elem.InnerText()

            elem = nodes(j)("CUSTOMERSIAMID")
            If elem IsNot Nothing Then item.CustomerSIAMID = elem.InnerText()

            elem = nodes(j)("NOTES")
            If elem IsNot Nothing Then item.Notes = elem.InnerText()

            elem = nodes(j)("SENTINVOICEORQUOTE")
            If elem IsNot Nothing Then item.Sentinvoiceorquote = Convert.ToInt16(elem.InnerText())

            elem = nodes(j)("ADMINSIAMID")
            If elem IsNot Nothing Then item.AdminSIAMID = elem.InnerText()

            elem = nodes(j)("CUSTOMERSIAMID")
            If elem IsNot Nothing Then item.CustomerSIAMID = elem.InnerText()
        Next

        Return item
    End Function

    Public Function CheckClassValidity(ByVal PartNumber As String, ByRef context As TransactionContext) As String
        Dim cmd As New OracleCommand("CheckClassValidity")

        cmd.Parameters.Add("xPartNumber", OracleDbType.Varchar2, 50)
        cmd.Parameters("xPartNumber").Value = PartNumber

        Dim outparam As New OracleParameter("retVal", OracleDbType.Varchar2, 50)
        outparam.Direction = ParameterDirection.Output
        cmd.Parameters.Add(outparam)

        ExecuteCMD(cmd, context)

        If Not CType(cmd.Parameters("retVal").Value, INullable).IsNull Then
            PartNumber = cmd.Parameters("retVal").Value.ToString()
        Else
            Throw New ApplicationException("Class Validation Failed")
        End If

        Return PartNumber
    End Function

    Public Sub StoreReservation(ByRef Reservation As CourseReservation, ByRef context As TransactionContext, Optional ByVal Action As CoreObject.ActionType = CoreObject.ActionType.ADD)
        Dim cmd As New OracleCommand

        Select Case Action
            Case CoreObject.ActionType.ADD
                cmd.CommandText = "AddTrainingReservation"
                cmd.Parameters.Add("xReservationNumber", OracleDbType.Varchar2, 15)
                cmd.Parameters("xReservationNumber").Value = Reservation.ReservationNumber
                If Not String.IsNullOrWhiteSpace(Reservation.CustomerSIAMID) Then
                    cmd.Parameters.Add("xCustomerSIAMID", OracleDbType.Varchar2, 16)
                    cmd.Parameters("xCustomerSIAMID").Value = Reservation.CustomerSIAMID
                End If
                If Not Not String.IsNullOrWhiteSpace(Reservation.AdminSIAMID) Then
                    cmd.Parameters.Add("xAdminSIAMID", OracleDbType.Varchar2, 16)
                    cmd.Parameters("xAdminSIAMID").Value = Reservation.AdminSIAMID
                End If
                cmd.Parameters.Add("xPartNumber", OracleDbType.Varchar2)
                cmd.Parameters("xPartNumber").Value = Reservation.CourseSchedule.PartNumber
                If Not Not String.IsNullOrWhiteSpace(Reservation.Notes) Then
                    cmd.Parameters.Add("xNotes", OracleDbType.Varchar2, 255)
                    cmd.Parameters("xNotes").Value = Reservation.Notes
                End If
                'Modified by Prasas for 45
                cmd.Parameters.Add("xsentinvoiceorquote", OracleDbType.Int32)
                cmd.Parameters("xsentinvoiceorquote").Value = Reservation.Sentinvoiceorquote

                cmd.Parameters.Add("xTemporaryClass", OracleDbType.Int16)
                If Reservation.CourseSchedule.PartNumber = "TRAINTEMP" Then
                    cmd.Parameters("xTemporaryClass").Value = 1
                Else
                    cmd.Parameters("xTemporaryClass").Value = 0
                End If
                cmd.Parameters.Add("xCourseNumber", OracleDbType.Varchar2, 50)
                cmd.Parameters("xCourseNumber").Value = Reservation.CourseSchedule.Course.Number

                cmd.Parameters.Add("xLOCATION", OracleDbType.Int16)
                cmd.Parameters("xLOCATION").Value = Convert.ToInt16(Reservation.CourseSchedule.Location.Code)

                Dim outparam As New OracleParameter("retVal", OracleDbType.Int64, ParameterDirection.Output)
                cmd.Parameters.Add(outparam)
            Case CoreObject.ActionType.UPDATE
                cmd.CommandText = "UpdateTrainingReservation"
                cmd.Parameters.Add("xReservationNumber", OracleDbType.Varchar2, 15)
                cmd.Parameters("xReservationNumber").Value = Reservation.ReservationNumber
                If Not String.IsNullOrWhiteSpace(Reservation.CustomerSIAMID) Then
                    cmd.Parameters.Add("xCustomerSIAMID", OracleDbType.Varchar2, 16)
                    cmd.Parameters("xCustomerSIAMID").Value = Reservation.CustomerSIAMID
                End If
                If Not String.IsNullOrWhiteSpace(Reservation.AdminSIAMID) Then
                    cmd.Parameters.Add("xAdminSIAMID", OracleDbType.Varchar2, 16)
                    cmd.Parameters("xAdminSIAMID").Value = Reservation.AdminSIAMID
                End If
                cmd.Parameters.Add("xPartNumber", OracleDbType.Varchar2)
                cmd.Parameters("xPartNumber").Value = Reservation.CourseSchedule.PartNumber
                If Not String.IsNullOrWhiteSpace(Reservation.Notes) Then
                    cmd.Parameters.Add("xNotes", OracleDbType.Varchar2, 255)
                    cmd.Parameters("xNotes").Value = Reservation.Notes
                End If
                'Modified by Prasad for 45
                cmd.Parameters.Add("xsentinvoiceorquote", OracleDbType.Int32)
                cmd.Parameters("xsentinvoiceorquote").Value = Reservation.Sentinvoiceorquote

                cmd.Parameters.Add("xTemporaryClass", OracleDbType.Int16)
                If Reservation.CourseSchedule.PartNumber = "TRAINTEMP" Then
                    cmd.Parameters("xTemporaryClass").Value = 1
                Else
                    cmd.Parameters("xTemporaryClass").Value = 0
                End If
                cmd.Parameters.Add("xCourseNumber", OracleDbType.Varchar2, 50)
                cmd.Parameters("xCourseNumber").Value = Reservation.CourseSchedule.Course.Number

                cmd.Parameters.Add("xLOCATION", OracleDbType.Int16)
                cmd.Parameters("xLOCATION").Value = Convert.ToInt16(Reservation.CourseSchedule.Location.Code)

                Dim outparam As New OracleParameter("retVal", OracleDbType.Int64, ParameterDirection.Output)
                cmd.Parameters.Add(outparam)
            Case CoreObject.ActionType.DELETE
                cmd.CommandText = "DeleteTrainingReservation"
                'cmd.Parameters.Add("xCode", OracleDbType.Int32)
                'cmd.Parameters("xCode").Value = Participant.Code
        End Select

        ExecuteCMD(cmd, context)
        If (Action = CoreObject.ActionType.ADD Or Action = CoreObject.ActionType.UPDATE) And
                Not CType(cmd.Parameters("retVal").Value, INullable).IsNull Then
            'ServicesPlusException.Utilities.LogDebug($"CourseDataManager.StoreReservation - Add retOrderNumber = {cmd.Parameters("retVal").Value}")
            Reservation.SequenceNumber = Convert.ToInt32(cmd.Parameters("retVal").Value.ToString())
        End If
    End Sub

    ' Functions for maintaining CourseClass
    Public Function GetActiveCourseClass(ByVal Status As String, ByVal Hide As String) As CourseSchedule()
        Dim item As CourseSchedule
        Dim item_list As New ArrayList
        Dim doc As DataSet
        Dim cmd As New OracleCommand("TRAININGPLUS.GETTRAININGCLASSES")

        cmd.Parameters.Add("xStatus", OracleDbType.Int16)
        cmd.Parameters("xStatus").Value = Convert.ToInt16(Status)

        cmd.Parameters.Add("xHide", OracleDbType.Int16)
        cmd.Parameters("xHide").Value = Convert.ToInt16(Hide)

        cmd.Parameters.Add("xTRAININGCLASSCursor", OracleDbType.RefCursor, ParameterDirection.Output)

        doc = GetResults(cmd, True)

        For Each row As DataRow In doc.Tables(0).Rows
            item = New CourseSchedule With {
                .PartNumber = If(row("PARTNUMBER")?.ToString(), ""),
                .StartDate = Convert.ToDateTime(If(row("STARTDATE"), "2001-01-01")),
                .EndDate = Convert.ToDateTime(If(row("ENDDATE"), "2001-01-01")),
                .Capacity = Convert.ToInt32(If(row("CAPACITY")?.ToString(), "0")),
                .ReminderDaysBefore = Convert.ToInt32(If(row("REMINDERDAYSBEFORE")?.ToString(), "1")),
                .Notes = If(row("NOTES")?.ToString(), ""),
                .UpdateDate = Convert.ToDateTime(If(row("UPDATEDATE"), "2001-01-01")),
                .IsDefaulLocation = IIf(row("ISCDEFAULTLOCATION")?.ToString() = "Y", True, False),
                .StatusCode = Status,
                .Hide = Hide,
                .Course = New Course() With {
                    .Number = If(row("COURSENUMBER")?.ToString(), "")
                },
                .Location = New CourseLocation() With {
                    .Code = If(row("LOCATIONCODE")?.ToString(), ""),
                    .City = If(row("CITY")?.ToString(), ""),
                    .State = If(row("STATE")?.ToString(), "")
                },
                .Instructor = New CourseInstructor() With {
                    .Code = Convert.ToInt32(If(row("INSTRUCTORCODE")?.ToString(), "0"))
                }
            }
            item_list.Add(item)
        Next

        Dim items(item_list.Count - 1) As CourseSchedule
        item_list.CopyTo(items)
        Return items
    End Function

    ' Functions for maintaining CourseClass
    Public Function GetCourseClass() As CourseSchedule()
        Dim doc As XmlDocument
        Dim cmd As New OracleCommand("GetTrainingClass")

        doc = GetResults(cmd)

        'internalize
        Dim root As XmlElement = doc.DocumentElement
        Dim nodes As XmlNodeList = root.ChildNodes
        Dim item As CourseSchedule
        Dim item_list As New ArrayList
        Dim j As Integer

        For j = 0 To nodes.Count - 1
            item = New CourseSchedule

            If Not nodes(j)("COURSENUMBER") Is Nothing Then
                item.Course.Number = nodes(j)("COURSENUMBER").InnerText
            End If
            If Not nodes(j)("STARTDATE") Is Nothing Then
                item.StartDate = nodes(j)("STARTDATE").InnerText
            End If
            If Not nodes(j)("ENDDATE") Is Nothing Then
                item.EndDate = nodes(j)("ENDDATE").InnerText
            End If
            If Not nodes(j)("LOCATIONCODE") Is Nothing Then
                item.Location.Code = nodes(j)("LOCATIONCODE").InnerText
            End If
            If Not nodes(j)("INSTRUCTORCODE") Is Nothing Then
                item.Instructor.Code = nodes(j)("INSTRUCTORCODE").InnerText
            End If
            If Not nodes(j)("CAPACITY") Is Nothing Then
                item.Capacity = nodes(j)("CAPACITY").InnerText
            End If
            If Not nodes(j)("REMINDERDAYSBEFORE") Is Nothing Then
                item.ReminderDaysBefore = nodes(j)("REMINDERDAYSBEFORE").InnerText
            End If
            If Not nodes(j)("NOTES") Is Nothing Then
                item.Notes = nodes(j)("NOTES").InnerText
            End If
            If Not nodes(j)("PARTNUMBER") Is Nothing Then
                item.PartNumber = nodes(j)("PARTNUMBER").InnerText
            End If

            item_list.Add(item)
        Next

        Dim items(item_list.Count - 1) As CourseSchedule
        item_list.CopyTo(items)

        Return items
    End Function
    ' Functions for retrieving course and its children class in XML 
    Public Function GetClassCatalogDetail(ByVal minorCatCode As Integer, ByRef currentDate As String, ByRef salesOrg As String) As XmlDataDocument
        Dim cmd As New OracleCommand With {
            .CommandText = "TRAININGPLUS.getCourseDetail",
            .CommandType = CommandType.StoredProcedure
        }

        cmd.Parameters.Add(New OracleParameter("xMNCode", OracleDbType.Int32, ParameterDirection.Input)).Value = minorCatCode
        cmd.Parameters.Add(New OracleParameter("xCurrentDate", OracleDbType.Varchar2, ParameterDirection.Input)).Value = currentDate
        cmd.Parameters.Add(New OracleParameter("xSalesOrg", OracleDbType.Varchar2, ParameterDirection.Input)).Value = salesOrg

        cmd.Parameters.Add(New OracleParameter("xCourseCursor", OracleDbType.RefCursor, ParameterDirection.Output))
        cmd.Parameters.Add(New OracleParameter("xLocationCursor", OracleDbType.RefCursor, ParameterDirection.Output))
        cmd.Parameters.Add(New OracleParameter("xClassCursor", OracleDbType.RefCursor, ParameterDirection.Output))
        cmd.Parameters.Add(New OracleParameter("xRCLocationCursor", OracleDbType.RefCursor, ParameterDirection.Output))
        cmd.Parameters.Add(New OracleParameter("xRCClassCursor", OracleDbType.RefCursor, ParameterDirection.Output))

        Dim ds As DataSet = GetResults(cmd, True)

        ds.Tables(0).TableName = "TRAININGCOURSE"
        ds.Tables(1).TableName = "TRAININGLOCATION"
        ds.Tables(2).TableName = "TRAININGCLASS"
        ds.Tables(3).TableName = "TRAININGRCLOCATION"
        ds.Tables(4).TableName = "TRAININGRCCLASS"

        ds.Relations.Add("COURSELOCATION", ds.Tables(0).Columns("CNUMBER"), ds.Tables(1).Columns("CNUMBER"), False).Nested = True
        Dim columns1() As DataColumn = {ds.Tables(1).Columns("LCODE"), ds.Tables(1).Columns("CNUMBER")}
        Dim columns2() As DataColumn = {ds.Tables(2).Columns("LOCATIONCODE"), ds.Tables(2).Columns("CNUMBER")}
        ds.Relations.Add("COURSECLASS", columns1, columns2, False).Nested = True

        ds.Relations.Add("COURSERCLOCATION", ds.Tables(0).Columns("CNUMBER"), ds.Tables(3).Columns("CNUMBER"), False).Nested = True
        Dim columns3() As DataColumn = {ds.Tables(3).Columns("LCODE"), ds.Tables(3).Columns("CNUMBER")}
        Dim columns4() As DataColumn = {ds.Tables(4).Columns("LOCATIONCODE"), ds.Tables(4).Columns("CNUMBER")}
        'Dim columns3() As DataColumn = {ds.Tables(3).Columns("LCODE"), ds.Tables(3).Columns("CPARTNUMBER")}
        'Dim columns4() As DataColumn = {ds.Tables(4).Columns("LOCATIONCODE"), ds.Tables(4).Columns("CPARTNUMBER")}

        ds.Relations.Add("COURSERCCLASS", columns3, columns4, False).Nested = True

        Dim xmlDoc = New XmlDataDocument(ds)
        Return xmlDoc
    End Function

    Public Sub GetCourseClassByNumber(ByRef partNumber As String, ByRef cs As CourseSchedule, Optional ByVal CourseNumber As String = "")
        Dim doc As XmlDocument
        Dim cmd As New OracleCommand("TrainingPlus.GetClassByNumber")

        cmd.Parameters.Add("xPartNumber", OracleDbType.Varchar2)
        cmd.Parameters("xPartNumber").Value = partNumber

        cmd.Parameters.Add("xCourseNumber", OracleDbType.Varchar2)
        cmd.Parameters("xCourseNumber").Value = CourseNumber

        cmd.Parameters.Add(New OracleParameter("xCourseCursor", OracleDbType.RefCursor, ParameterDirection.Output))

        doc = GetResultsAsXML(cmd, True)

        'internalize
        Dim root As XmlElement = doc.DocumentElement
        Dim nodes As XmlNodeList = root.ChildNodes
        Dim elem As XmlElement
        Dim j As Integer

        For j = 0 To nodes.Count - 1
            cs = New CourseSchedule

            elem = nodes(j)("TYPECODE")
            If elem IsNot Nothing Then cs.Course.Type.TypeCode = Convert.ToInt32(elem.InnerText)

            elem = nodes(j)("COURSENUMBER")
            If elem IsNot Nothing Then cs.Course.Number = elem.InnerText

            elem = nodes(j)("MODEL")
            If elem IsNot Nothing Then cs.Course.Model = elem.InnerText

            elem = nodes(j)("PREREQUISITES")
            If elem IsNot Nothing Then cs.Course.PreRequisite = elem.InnerText

            elem = nodes(j)("MINORCATEGORY")
            If elem IsNot Nothing Then cs.Course.MinorCategory.Description = elem.InnerText

            elem = nodes(j)("MAJORCATEGORY")
            If elem IsNot Nothing Then cs.Course.MajorCategory.Description = elem.InnerText

            elem = nodes(j)("COURSETYPE")
            If elem IsNot Nothing Then cs.Course.Type.Code = elem.InnerText

            elem = nodes(j)("DESCRIPTION")
            If elem IsNot Nothing Then cs.Course.Description = elem.InnerText

            elem = nodes(j)("INSTRUCTORCODE")
            If elem IsNot Nothing Then cs.Instructor.Code = elem.InnerText

            elem = nodes(j)("LOCATIONNAME")
            If elem IsNot Nothing Then cs.Location.Name = elem.InnerText

            elem = nodes(j)("ADDRESS1")
            If elem IsNot Nothing Then cs.Location.Address1 = elem.InnerText

            elem = nodes(j)("ADDRESS2")
            If elem IsNot Nothing Then cs.Location.Address2 = elem.InnerText

            elem = nodes(j)("ADDRESS3")
            If elem IsNot Nothing Then cs.Location.Address3 = elem.InnerText

            elem = nodes(j)("CITY")
            If elem IsNot Nothing Then cs.Location.City = elem.InnerText

            elem = nodes(j)("STATE")
            If elem IsNot Nothing Then cs.Location.State = elem.InnerText

            elem = nodes(j)("ZIP")
            If elem IsNot Nothing Then cs.Location.Zip = elem.InnerText

            elem = nodes(j)("TERRITORYNAME")
            If elem IsNot Nothing Then cs.Location.Country.TerritoryName = elem.InnerText

            elem = nodes(j)("LOCATIONCODE")
            If elem IsNot Nothing Then cs.Location.Code = elem.InnerText

            elem = nodes(j)("TITLE")
            If elem IsNot Nothing Then cs.Course.Title = elem.InnerText

            elem = nodes(j)("PRICE")
            If elem IsNot Nothing Then cs.Course.ListPrice = Convert.ToDouble(elem.InnerText)

            elem = nodes(j)("STARTDATE")
            If elem IsNot Nothing Then cs.StartDate = elem.InnerText

            elem = nodes(j)("ENDDATE")
            If elem IsNot Nothing Then cs.EndDate = elem.InnerText

            elem = nodes(j)("TIME")
            If elem IsNot Nothing Then cs.Time = elem.InnerText

            elem = nodes(j)("PARTNUMBER")
            If elem IsNot Nothing Then cs.PartNumber = elem.InnerText
        Next
    End Sub

    Public Sub GetCourseByCourseNumber(ByVal CourseNumber As String, ByVal CourseLocation As Integer, ByRef cs As CourseSchedule)
        Dim doc As XmlDocument
        Dim cmd As New OracleCommand("TrainingPlus.GetCourseByCourseNumber")

        cmd.Parameters.Add("xCourseNumber", OracleDbType.Varchar2)
        cmd.Parameters("xCourseNumber").Value = CourseNumber

        cmd.Parameters.Add("xCourseLocation", OracleDbType.Int32)
        cmd.Parameters("xCourseLocation").Value = CourseLocation

        cmd.Parameters.Add(New OracleParameter("xCourseCursor", OracleDbType.RefCursor, ParameterDirection.Output))

        doc = GetResultsAsXML(cmd, True)

        'internalize
        Dim root As XmlElement = doc.DocumentElement
        Dim nodes As XmlNodeList = root.ChildNodes
        Dim elem As XmlElement
        Dim j As Integer

        For j = 0 To nodes.Count - 1
            cs = New CourseSchedule

            elem = nodes(j)("TYPECODE")
            If elem Is Nothing Then
            Else
                cs.Course.Type.TypeCode = Convert.ToInt32(elem.InnerText)
            End If

            elem = nodes(j)("COURSENUMBER")
            If elem Is Nothing Then
            Else
                cs.Course.Number = elem.InnerText
            End If

            elem = nodes(j)("MODEL")
            If elem Is Nothing Then
            Else
                cs.Course.Model = elem.InnerText
            End If

            elem = nodes(j)("PREREQUISITES")
            If elem Is Nothing Then
            Else
                cs.Course.PreRequisite = elem.InnerText
            End If

            elem = nodes(j)("MINORCATEGORY")
            If elem Is Nothing Then
            Else
                cs.Course.MinorCategory.Description = elem.InnerText
            End If

            elem = nodes(j)("MAJORCATEGORY")
            If elem Is Nothing Then
            Else
                cs.Course.MajorCategory.Description = elem.InnerText
            End If

            elem = nodes(j)("COURSETYPE")
            If elem Is Nothing Then
            Else
                cs.Course.Type.Code = elem.InnerText
            End If

            elem = nodes(j)("DESCRIPTION")
            If elem Is Nothing Then
            Else
                cs.Course.Description = elem.InnerText
            End If

            elem = nodes(j)("INSTRUCTORCODE")
            If elem Is Nothing Then
            Else
                cs.Instructor.Code = elem.InnerText
            End If

            elem = nodes(j)("LOCATIONNAME")
            If elem Is Nothing Then
            Else
                cs.Location.Name = elem.InnerText
            End If

            elem = nodes(j)("ADDRESS1")
            If elem Is Nothing Then
            Else
                cs.Location.Address1 = elem.InnerText
            End If

            elem = nodes(j)("ADDRESS2")
            If elem Is Nothing Then
            Else
                cs.Location.Address2 = elem.InnerText
            End If
            elem = nodes(j)("ADDRESS3")
            If elem Is Nothing Then
            Else
                cs.Location.Address3 = elem.InnerText
            End If
            elem = nodes(j)("CITY")
            If elem Is Nothing Then
            Else
                cs.Location.City = elem.InnerText
            End If

            elem = nodes(j)("STATE")
            If elem Is Nothing Then
            Else
                cs.Location.State = elem.InnerText
            End If

            elem = nodes(j)("ZIP")
            If elem Is Nothing Then
            Else
                cs.Location.Zip = elem.InnerText
            End If

            elem = nodes(j)("TERRITORYNAME")
            If elem Is Nothing Then
            Else
                cs.Location.Country.TerritoryName = elem.InnerText
            End If

            elem = nodes(j)("LOCATIONCODE")
            If elem Is Nothing Then
            Else
                cs.Location.Code = elem.InnerText
            End If

            elem = nodes(j)("TITLE")
            If elem Is Nothing Then
            Else
                cs.Course.Title = elem.InnerText
            End If

            elem = nodes(j)("PRICE")
            If elem Is Nothing Then
            Else
                cs.Course.ListPrice = Convert.ToDouble(elem.InnerText)
            End If

            elem = nodes(j)("STARTDATE")
            If elem Is Nothing Then
            Else
                cs.StartDate = elem.InnerText
            End If

            elem = nodes(j)("ENDDATE")
            If elem Is Nothing Then
            Else
                cs.EndDate = elem.InnerText
            End If

            elem = nodes(j)("TIME")
            If elem Is Nothing Then
            Else
                cs.Time = elem.InnerText
            End If

            elem = nodes(j)("PARTNUMBER")
            If elem Is Nothing Then
            Else
                cs.PartNumber = elem.InnerText
            End If
        Next
    End Sub

    ' Functions for maintaining CourseClass
    Public Function GetCourseClassesByCNumber(ByRef courseNumber As String) As CourseSchedule()
        Dim doc As XmlDocument
        Dim cmd As New OracleCommand("GETTRAININGCLASSBYCN")

        cmd.Parameters.Add("xCourseNumber", OracleDbType.Varchar2)
        cmd.Parameters("xCourseNumber").Value = courseNumber

        doc = GetResults(cmd)

        'internalize
        Dim nodes As XmlNodeList = doc.DocumentElement.ChildNodes
        Dim elem As XmlElement
        Dim j As Integer
        Dim item_list As New ArrayList
        Dim cs As CourseSchedule

        For j = 0 To nodes.Count - 1
            cs = New CourseSchedule

            elem = nodes(j)("COURSENUMBER")
            If elem Is Nothing Then
            Else
                cs.Course.Number = elem.InnerText
            End If

            elem = nodes(j)("MODEL")
            If elem Is Nothing Then
            Else
                cs.Course.Model = elem.InnerText
            End If

            elem = nodes(j)("PREREQUISITES")
            If elem Is Nothing Then
            Else
                cs.Course.PreRequisite = elem.InnerText
            End If

            elem = nodes(j)("MINORCATEGORY")
            If elem Is Nothing Then
            Else
                cs.Course.MinorCategory.Description = elem.InnerText
            End If

            elem = nodes(j)("MAJORCATEGORY")
            If elem Is Nothing Then
            Else
                cs.Course.MajorCategory.Description = elem.InnerText
            End If

            elem = nodes(j)("COURSETYPE")
            If elem Is Nothing Then
            Else
                cs.Course.Type.Code = elem.InnerText
            End If

            elem = nodes(j)("DESCRIPTION")
            If elem Is Nothing Then
            Else
                cs.Course.Description = elem.InnerText
            End If

            elem = nodes(j)("INSTRUCTORCODE")
            If elem Is Nothing Then
            Else
                cs.Instructor.Code = elem.InnerText
            End If

            elem = nodes(j)("LOCATIONNAME")
            If elem Is Nothing Then
            Else
                cs.Location.Name = elem.InnerText
            End If

            elem = nodes(j)("ADDRESS1")
            If elem Is Nothing Then
            Else
                cs.Location.Address1 = elem.InnerText
            End If

            elem = nodes(j)("ADDRESS2")
            If elem Is Nothing Then
            Else
                cs.Location.Address2 = elem.InnerText
            End If

            elem = nodes(j)("ADDRESS3")
            If elem Is Nothing Then
            Else
                cs.Location.Address3 = elem.InnerText
            End If

            elem = nodes(j)("CITY")
            If elem Is Nothing Then
            Else
                cs.Location.City = elem.InnerText
            End If

            elem = nodes(j)("STATE")
            If elem Is Nothing Then
            Else
                cs.Location.State = elem.InnerText
            End If

            elem = nodes(j)("ZIP")
            If elem Is Nothing Then
            Else
                cs.Location.Zip = elem.InnerText
            End If

            elem = nodes(j)("TERRITORYNAME")
            If elem Is Nothing Then
            Else
                cs.Location.Country.TerritoryName = elem.InnerText
            End If

            elem = nodes(j)("LOCATIONCODE")
            If elem Is Nothing Then
            Else
                cs.Location.Code = elem.InnerText
            End If

            elem = nodes(j)("TITLE")
            If elem Is Nothing Then
            Else
                cs.Course.Title = elem.InnerText
            End If

            elem = nodes(j)("PRICE")
            If elem Is Nothing Then
            Else
                cs.Course.ListPrice = Convert.ToDouble(elem.InnerText)
            End If

            elem = nodes(j)("STARTDATE")
            If elem Is Nothing Then
            Else
                cs.StartDate = elem.InnerText
            End If

            elem = nodes(j)("ENDDATE")
            If elem Is Nothing Then
            Else
                cs.EndDate = elem.InnerText
            End If

            elem = nodes(j)("TIME")
            If elem Is Nothing Then
            Else
                cs.Time = elem.InnerText
            End If

            elem = nodes(j)("PARTNUMBER")
            If elem Is Nothing Then
            Else
                cs.PartNumber = elem.InnerText
            End If
            item_list.Add(cs)
        Next

        Dim items(item_list.Count - 1) As CourseSchedule
        item_list.CopyTo(items)

        Return items
    End Function

    Public Function IsTrainingCourse(ByVal CourseNumber As String) As Boolean
        Dim cmd As New OracleCommand("ISTRAININGCOURSE")

        cmd.Parameters.Add("xCourseNumber", OracleDbType.Varchar2, 20)
        cmd.Parameters("xCourseNumber").Value = CourseNumber

        cmd.Parameters.Add(New OracleParameter("RetVal", OracleDbType.Int16, ParameterDirection.Output))

        ExecuteCMD(cmd, Context)
        If Not CType(cmd.Parameters("retVal").Value, INullable).IsNull Then
            If Convert.ToInt32(cmd.Parameters("retVal").Value.ToString()) >= 1 Then
                Return True
            End If
        End If
        Return False
    End Function

    Public Sub StoreClass(ByRef TrainingClass As CourseSchedule, ByRef context As TransactionContext, Optional ByVal Action As CoreObject.ActionType = CoreObject.ActionType.ADD)
        Dim cmd As New OracleCommand
        Dim bCloseConn As Boolean = True

        Select Case Action
            Case CoreObject.ActionType.ADD
                cmd.CommandText = "AddTrainingClass"
            Case CoreObject.ActionType.UPDATE
                cmd.CommandText = "UpdateTrainingClass"
            Case CoreObject.ActionType.DELETE
                cmd.CommandText = "DELETETRAININGCLASS"
                bCloseConn = False 'keep connection for subsequent operation
                'cmd.Parameters.Add("xCode", OracleDbType.Int32)
                'cmd.Parameters("xCode").Value = Participant.Code
            Case Else
                cmd.CommandText = "ActivateTrainingClass"
        End Select

        cmd.Parameters.Add("xCourseNumber", OracleDbType.Varchar2)
        cmd.Parameters("xCourseNumber").Value = TrainingClass.Course.Number
        cmd.Parameters.Add("xStartDate", OracleDbType.Date)
        cmd.Parameters("xStartDate").Value = TrainingClass.StartDate
        cmd.Parameters.Add("xEndDate", OracleDbType.Date)
        cmd.Parameters("xEndDate").Value = TrainingClass.EndDate
        cmd.Parameters.Add("xLocationCode", OracleDbType.Int32)
        cmd.Parameters("xLocationCode").Value = TrainingClass.Location.Code
        cmd.Parameters.Add("xInstructorCode", OracleDbType.Int32)
        cmd.Parameters("xInstructorCode").Value = TrainingClass.Instructor.Code
        cmd.Parameters.Add("xCapacity", OracleDbType.Int32)
        cmd.Parameters("xCapacity").Value = TrainingClass.Capacity
        cmd.Parameters.Add("xReminderDaysBefore", OracleDbType.Int32)
        cmd.Parameters("xReminderDaysBefore").Value = TrainingClass.ReminderDaysBefore
        If TrainingClass.Notes <> "" Then
            cmd.Parameters.Add("xNotes", OracleDbType.Varchar2)
            cmd.Parameters("xNotes").Value = TrainingClass.Notes
        End If
        If TrainingClass.Time <> "" Then
            cmd.Parameters.Add("xTime", OracleDbType.Varchar2)
            cmd.Parameters("xTime").Value = TrainingClass.Time
        End If
        cmd.Parameters.Add("xPartNumber", OracleDbType.Varchar2)
        cmd.Parameters("xPartNumber").Value = TrainingClass.PartNumber

        Dim outparam As New OracleParameter("RetVal", OracleDbType.Varchar2, 50)
        outparam.Direction = ParameterDirection.Output
        cmd.Parameters.Add(outparam)

        ExecuteCMD(cmd, context, bCloseConn)

        If (Action = CoreObject.ActionType.ADD Or Action = CoreObject.ActionType.UPDATE) And
                Not CType(cmd.Parameters("retVal").Value, INullable).IsNull Then
            If Action = CoreObject.ActionType.ADD Then
                TrainingClass.PartNumber = cmd.Parameters("retVal").Value.ToString()
                TrainingClass.SequenceNumber = 1
            Else
                TrainingClass.SequenceNumber = Convert.ToInt32(cmd.Parameters("retVal").Value.ToString())
            End If
        End If
    End Sub

    ' Functions for retrieving major and its children minor category in XML 
    Public Function GetMajorMinorCategories() As XmlDataDocument
        Dim cmd As New OracleCommand("TRAININGPLUS.GETMAJORMINORCAT")

        cmd.Parameters.Add(New OracleParameter("xMJCategoryCursor", OracleDbType.RefCursor)).Direction = ParameterDirection.Output
        cmd.Parameters.Add(New OracleParameter("xMNCategoryCursor", OracleDbType.RefCursor)).Direction = ParameterDirection.Output

        Dim ds As DataSet = GetResults(cmd, True)

        ds.Tables(0).TableName = "TRAININGMAJORCATEGORY"
        ds.Tables(1).TableName = "TRAININGMINORCATEGORY"
        ds.Relations.Add("MAJORMINORCAT", ds.Tables(0).Columns("MJCODE"), ds.Tables(1).Columns("MAJORCATEGORYCODE"), False).Nested = True
        Dim xmlDoc As New XmlDataDocument(ds)

        Return xmlDoc
    End Function

    ' Functions for retrieving CourseMajorCategory
    Public Function GetMajorCategories() As CourseMajorCategory()
        Dim item As CourseMajorCategory
        Dim item_list As New ArrayList
        Dim j As Integer
        Dim doc As XmlDocument
        Dim cmd As New OracleCommand("getTrainingMajorCategory")

        doc = GetResults(cmd)

        'internalize
        Dim nodes As XmlNodeList = doc.DocumentElement.ChildNodes

        For j = 0 To nodes.Count - 1
            item = New CourseMajorCategory With {
                .Code = nodes(j)("CODE").InnerText,
                .Description = nodes(j)("DESCRIPTION").InnerText
            }

            item_list.Add(item)
        Next

        Dim items(item_list.Count - 1) As CourseMajorCategory
        item_list.CopyTo(items)

        Return items
    End Function

    Public Function GetMajorCategoryDescritions(ByRef defaultValue As String) As String()
        Dim doc As XmlDocument
        Dim cmd As New OracleCommand("getTrainingMajorCategoryD")

        doc = GetResults(cmd)

        'internalize
        Dim nodes As XmlNodeList = doc.DocumentElement.ChildNodes
        Dim item_list As New ArrayList
        Dim j As Integer

        If Not String.IsNullOrWhiteSpace(defaultValue) Then item_list.Add(defaultValue)

        For j = 0 To nodes.Count - 1
            item_list.Add(nodes(j)("DESCRIPTION").InnerText)
        Next

        Dim items(item_list.Count - 1) As String
        item_list.CopyTo(items)

        Return items
    End Function

    Public Sub StoreMajorCategory(ByRef Category As CourseMajorCategory, ByRef context As TransactionContext, Optional ByVal Action As CoreObject.ActionType = CoreObject.ActionType.ADD)
        Dim cmd As New OracleCommand

        Select Case Action
            Case CoreObject.ActionType.ADD
                cmd.CommandText = "AddTrainingMajorCategory"
                cmd.Parameters.Add("xCode", OracleDbType.Int32)
                cmd.Parameters("xCode").Value = Category.Code
                cmd.Parameters.Add("xDescription", OracleDbType.Varchar2)
                cmd.Parameters("xDescription").Value = Category.Description
            Case CoreObject.ActionType.UPDATE
                cmd.CommandText = "UpdateTrainingMajorCategory"
                cmd.Parameters.Add("xCode", OracleDbType.Int32)
                cmd.Parameters("xCode").Value = Category.Code
                cmd.Parameters.Add("xDescription", OracleDbType.Varchar2)
                cmd.Parameters("xDescription").Value = Category.Description
            Case CoreObject.ActionType.DELETE
                cmd.CommandText = "DeleteTrainingMajorCategory"
                cmd.Parameters.Add("xCode", OracleDbType.Int32)
                cmd.Parameters("xCode").Value = Category.Code
        End Select
        ExecuteCMD(cmd, context)
    End Sub

    Public Sub CancelClassReservation(ByRef classNumber As String, ByRef context As TransactionContext)
        Dim cmd As New OracleCommand("CANCELCSRESERVATION")

        cmd.Parameters.Add("xPartNumber", OracleDbType.Varchar2)
        cmd.Parameters("xPartNumber").Value = classNumber

        ExecuteCMD(cmd, context)
    End Sub

    ' Functions for maintaining CourseMinorCategory
    Public Function GetMinorCategories() As CourseMinorCategory()
        Dim doc As XmlDocument
        Dim nodes As XmlNodeList
        Dim item_list As New ArrayList
        Dim cmd As New OracleCommand("getTrainingMinorCategory")

        doc = GetResults(cmd)
        nodes = doc.DocumentElement.ChildNodes

        For Each node As XmlNode In nodes
            item_list.Add(New CourseMinorCategory With {
                .Code = node("CODE").InnerText,
                .MajorCategoryCode = node("MAJORCATEGORYCODE").InnerText,
                .Description = node("DESCRIPTION").InnerText,
                .Mejor_Minor_Description = node("MEJORMINDESC").InnerText
            })
        Next

        Dim items(item_list.Count - 1) As CourseMinorCategory
        item_list.CopyTo(items)

        Return items
    End Function

    Public Function GetModelList() As ArrayList
        Dim objCourseModels As ArrayList = New ArrayList()
        Dim defaultItem As New CourseModel("Select Model", "Select Model", 0)
        Dim dsCourseModel As DataSet
        Dim cmd As New OracleCommand("TRAININGPLUS.GETTRAININGMODELLIST")

        cmd.Parameters.Add(New OracleParameter("xCourseModelCursor", OracleDbType.RefCursor, ParameterDirection.Output))
        dsCourseModel = GetResults(cmd, True)

        objCourseModels.Add(defaultItem)
        For Each row As DataRow In dsCourseModel.Tables(0).Rows
            objCourseModels.Add(New CourseModel(row("ModelName").ToString(), row("ModelName").ToString(), 0))
        Next

        Return objCourseModels
    End Function

    Public Function GetCourseModelList(ByVal CourseNumber As String) As TrainingCourseModel
        Dim objTrainingCourseModel As New TrainingCourseModel
        Dim dsCourseModel As DataSet
        Dim cmd As New OracleCommand("TRAININGPLUS.GETCOURSEMODELLIST")

        cmd.Parameters.Add("xCourseNumber", OracleDbType.Varchar2, 50)
        cmd.Parameters("xCourseNumber").Value = CourseNumber

        cmd.Parameters.Add(New OracleParameter("xCourseModelCursor", OracleDbType.RefCursor, ParameterDirection.Output))
        dsCourseModel = GetResults(cmd, True)

        For Each drModel As DataRow In dsCourseModel.Tables(0).Rows
            objTrainingCourseModel.AddCourseModel(drModel("ModelName").ToString(), drModel("CourseNumber").ToString(), False)
        Next
        Return objTrainingCourseModel
    End Function

    Public Sub StoreMinorCategory(ByRef Category As CourseMinorCategory, ByRef context As TransactionContext, Optional ByVal Action As CoreObject.ActionType = CoreObject.ActionType.ADD)
        Dim cmd As New OracleCommand

        Select Case Action
            Case CoreObject.ActionType.ADD
                cmd.CommandText = "AddTrainingMinorCategory"
                cmd.Parameters.Add("xCode", OracleDbType.Int32)
                cmd.Parameters("xCode").Value = Category.Code
                cmd.Parameters.Add("xMajorCategoryCode", OracleDbType.Int32)
                cmd.Parameters("xMajorCategoryCode").Value = Category.MajorCategoryCode
                cmd.Parameters.Add("xDescription", OracleDbType.Varchar2)
                cmd.Parameters("xDescription").Value = Category.Description
                cmd.Parameters.Add(New OracleParameter("RetVal", OracleDbType.Int32, ParameterDirection.Output))
            Case CoreObject.ActionType.UPDATE
                cmd.CommandText = "UpdateTrainingMinorCategory"
                cmd.Parameters.Add("xCode", OracleDbType.Int32)
                cmd.Parameters("xCode").Value = Category.Code
                cmd.Parameters.Add("xMajorCategoryCode", OracleDbType.Int32)
                cmd.Parameters("xMajorCategoryCode").Value = Category.MajorCategoryCode
                cmd.Parameters.Add("xDescription", OracleDbType.Varchar2)
                cmd.Parameters("xDescription").Value = Category.Description
            Case CoreObject.ActionType.DELETE
                cmd.CommandText = "DeleteTrainingMinorCategory"
                cmd.Parameters.Add("xCode", OracleDbType.Int32)
                cmd.Parameters("xCode").Value = Category.Code
        End Select
        ExecuteCMD(cmd, context)
    End Sub

    Public Function GetInstructors() As CourseInstructor()
        Dim item_list As New ArrayList
        Dim j As Integer
        Dim doc As XmlDocument
        Dim cmd As New OracleCommand("GetCourseInstructor")

        doc = GetResults(cmd)

        'internalize
        Dim nodes As XmlNodeList = doc.DocumentElement.ChildNodes

        For j = 0 To nodes.Count - 1
            item_list.Add(New CourseInstructor With {
                .Code = nodes(j)("Code").InnerText,
                .FirstName = nodes(j)("FirstName").InnerText,
                .LastName = nodes(j)("LastName").InnerText,
                .Address1 = nodes(j)("Address1").InnerText,
                .Address2 = nodes(j)("Address2").InnerText,
                .Address3 = nodes(j)("Address3").InnerText,
                .City = nodes(j)("City").InnerText,
                .State = nodes(j)("State").InnerText,
                .Zip = nodes(j)("Zip").InnerText,
                .ISOCountryCode = nodes(j)("ISOCountryCode").InnerText,
                .EMail = nodes(j)("EMail").InnerText,
                .Phone = nodes(j)("Phone").InnerText,
                .Extension = nodes(j)("Extension").InnerText,
                .Fax = nodes(j)("Fax").InnerText,
                .LaborRate = nodes(j)("LaborRate").InnerText,
                .Notes = nodes(j)("Notes").InnerText,
                .UpdateDate = nodes(j)("UpdateDate").InnerText,
                .SequenceNumber = nodes(j)("SequenceNumber").InnerText,
                .StatusCode = nodes(j)("StatusCode").InnerText
            })
        Next

        Dim items(item_list.Count - 1) As CourseInstructor
        item_list.CopyTo(items)

        Return items
    End Function

    Public Sub StoreInstructor(ByRef Instructor As CourseInstructor, ByRef context As TransactionContext, Optional ByVal Action As CoreObject.ActionType = CoreObject.ActionType.ADD)
        Dim cmd As New OracleCommand

        Select Case Action
            Case CoreObject.ActionType.ADD
                cmd.CommandText = "AddTrainingInstructor"
                cmd.Parameters.Add(New OracleParameter("RetCode", OracleDbType.Int32, ParameterDirection.Output))
                cmd.Parameters.Add(New OracleParameter("RetVal", OracleDbType.Int32, ParameterDirection.Output))
            Case CoreObject.ActionType.UPDATE
                cmd.CommandText = "UpdateTrainingInstructor"
                cmd.Parameters.Add(New OracleParameter("RetVal", OracleDbType.Int32, ParameterDirection.Output))
                cmd.Parameters.Add("xCode", OracleDbType.Int32)
                cmd.Parameters("xCode").Value = Instructor.Code
            Case CoreObject.ActionType.DELETE
                cmd.CommandText = "DeleteTrainingInstructor"
                cmd.Parameters.Add("xCode", OracleDbType.Int32)
                cmd.Parameters("xCode").Value = Instructor.Code
        End Select
        cmd.Parameters.Add("xFirstName", OracleDbType.Varchar2)
        cmd.Parameters("xFirstName").Value = Instructor.FirstName
        cmd.Parameters.Add("xLastName", OracleDbType.Varchar2)
        cmd.Parameters("xLastName").Value = Instructor.LastName
        cmd.Parameters.Add("xAddress1", OracleDbType.Varchar2)
        cmd.Parameters("xAddress1").Value = Instructor.Address1
        cmd.Parameters.Add("xAddress2", OracleDbType.Varchar2)
        cmd.Parameters("xAddress2").Value = Instructor.Address2
        cmd.Parameters.Add("xAddress3", OracleDbType.Varchar2)
        cmd.Parameters("xAddress3").Value = Instructor.Address3
        cmd.Parameters.Add("xCity", OracleDbType.Varchar2)
        cmd.Parameters("xCity").Value = Instructor.City
        cmd.Parameters.Add("xState", OracleDbType.Varchar2)
        cmd.Parameters("xState").Value = Instructor.State
        cmd.Parameters.Add("xZip", OracleDbType.Varchar2)
        cmd.Parameters("xZip").Value = Instructor.Zip
        cmd.Parameters.Add("xISOCountryCode", OracleDbType.Varchar2)
        cmd.Parameters("xISOCountryCode").Value = Instructor.ISOCountryCode
        cmd.Parameters.Add("xEMail", OracleDbType.Varchar2)
        cmd.Parameters("xEMail").Value = Instructor.EMail
        cmd.Parameters.Add("xPhone", OracleDbType.Varchar2)
        cmd.Parameters("xPhone").Value = Instructor.Phone
        cmd.Parameters.Add("xExtension", OracleDbType.Varchar2)
        cmd.Parameters("xExtension").Value = Instructor.Extension
        cmd.Parameters.Add("xFax", OracleDbType.Varchar2)
        cmd.Parameters("xFax").Value = Instructor.Fax
        cmd.Parameters.Add("xLaborRate", OracleDbType.Varchar2)
        cmd.Parameters("xLaborRate").Value = Instructor.LaborRate
        cmd.Parameters.Add("xNotes", OracleDbType.Varchar2)
        cmd.Parameters("xNotes").Value = Instructor.Notes
        ExecuteCMD(cmd, context)

        If Action = CoreObject.ActionType.ADD Then
            If Not CType(cmd.Parameters("RetCode")?.Value, INullable).IsNull Then
                Instructor.Code = cmd.Parameters("RetCode").Value.ToString()
            End If
        End If
        If (Action = CoreObject.ActionType.ADD Or Action = CoreObject.ActionType.UPDATE) Then
            If Not CType(cmd.Parameters("retVal").Value, INullable).IsNull Then
                Instructor.SequenceNumber = cmd.Parameters("retVal").Value.ToString()
            End If
        End If
    End Sub

    ' Functions for retrieving minor category and its children course in XML 
    Public Function GetCategoryCourse(ByVal minorCatCode As Integer) As XmlDataDocument
        Dim cmd As New OracleCommand("TRAININGPLUS.getCategoryCourse")

        cmd.Parameters.Add(New OracleParameter("xMNCode", OracleDbType.Int32, ParameterDirection.Input))
        cmd.Parameters("xMNCode").Value = minorCatCode
        cmd.Parameters.Add(New OracleParameter("xCategoryCursor", OracleDbType.RefCursor, ParameterDirection.Output))
        cmd.Parameters.Add(New OracleParameter("xCourseCursor", OracleDbType.RefCursor, ParameterDirection.Output))

        Dim ds As DataSet = GetResults(cmd, True)

        ds.Tables(0).TableName = "TRAININGCATEGORY"
        ds.Tables(1).TableName = "TRAININGCATCOURSE"
        ds.Relations.Add("CATCOURSE", ds.Tables(0).Columns("MNCODE"), ds.Tables(1).Columns("MNCODE"), False).Nested = True
        Dim xmlDoc As New XmlDataDocument(ds)

        Return xmlDoc
    End Function

    ' Functions for maintaining CourseLocation
    Public Function GetLocations() As CourseLocation()
        Dim doc As XmlDocument
        Dim cmd As New OracleCommand("GetTrainingLocation")

        doc = GetResults(cmd)

        Dim nodes As XmlNodeList = doc.DocumentElement.ChildNodes
        Dim elem As XmlElement
        Dim item As CourseLocation
        Dim item_list As New ArrayList
        Dim j As Integer

        For j = 0 To nodes.Count - 1
            item = New CourseLocation

            elem = nodes(j)("CODE")
            If elem Is Nothing Then
            Else
                item.Code = elem.InnerText
            End If

            elem = nodes(j)("NAME")
            If elem Is Nothing Then
            Else
                item.Name = elem.InnerText
            End If

            elem = nodes(j)("ADDRESS1")
            If elem Is Nothing Then
            Else
                item.Address1 = elem.InnerText
            End If

            elem = nodes(j)("ADDRESS2")
            If elem Is Nothing Then
            Else
                item.Address2 = elem.InnerText
            End If

            elem = nodes(j)("ADDRESS3")
            If elem Is Nothing Then
            Else
                item.Address3 = elem.InnerText
            End If

            elem = nodes(j)("CITY")
            If elem Is Nothing Then
            Else
                item.City = elem.InnerText
            End If

            elem = nodes(j)("STATE")
            If elem Is Nothing Then
            Else
                item.State = elem.InnerText
            End If

            elem = nodes(j)("ZIP")
            If elem Is Nothing Then
            Else
                item.Zip = elem.InnerText
            End If

            elem = nodes(j)("PHONE")
            If elem Is Nothing Then
            Else
                item.Phone = elem.InnerText
            End If

            elem = nodes(j)("FAX")
            If elem Is Nothing Then
            Else
                item.Fax = elem.InnerText
            End If

            elem = nodes(j)("EXTENSION")
            If elem Is Nothing Then
            Else
                item.Extension = elem.InnerText
            End If

            elem = nodes(j)("SEQUENCENUMBER")
            If elem Is Nothing Then
            Else
                item.SequenceNumber = elem.InnerText
            End If

            elem = nodes(j)("STATUSCODE")
            If elem Is Nothing Then
            Else
                item.StatusCode = elem.InnerText
            End If

            elem = nodes(j)("UPDATEDATE")
            If elem Is Nothing Then
            Else
                item.UpdateDate = elem.InnerText
            End If

            elem = nodes(j)("NOTES")
            If elem Is Nothing Then
            Else
                item.Notes = elem.InnerText
            End If

            elem = nodes(j)("DIRECTION")
            If elem Is Nothing Then
            Else
                item.Direction = nodes(j)("DIRECTION").InnerText
            End If

            elem = nodes(j)("ACCOMODATIONS")
            If elem Is Nothing Then
            Else
                item.Accomodations = elem.InnerText
            End If

            elem = nodes(j)("TERRITORYNAME")
            If elem Is Nothing Then
            Else
                item.Country.TerritoryName = elem.InnerText
            End If

            elem = nodes(j)("ISOCOUNTRYCODE")
            If elem Is Nothing Then
            Else
                item.Country.Country = elem.InnerText
            End If

            item_list.Add(item)
        Next

        Dim items(item_list.Count - 1) As CourseLocation
        item_list.CopyTo(items)

        Return items
    End Function

    ' get default locations to be added on course create
    Public Function GetDefaultLocations(ByVal coursenumber As String) As CourseLocation()
        Dim doc As XmlDocument
        Dim cmd As New OracleCommand("GETDEFAULTLOCATIONS")
        cmd.CommandType = CommandType.StoredProcedure   ' Required for this single-parameter GetResults(OracleCommand cmd)

        cmd.Parameters.Add(New OracleParameter("xCourseNumber", OracleDbType.Varchar2, 20)).Direction = ParameterDirection.Input
        cmd.Parameters("xCourseNumber").Value = coursenumber

        doc = GetResults(cmd)

        'internalize
        Dim nodes As XmlNodeList = doc.DocumentElement.ChildNodes
        Dim elem As XmlElement
        Dim item As CourseLocation
        Dim item_list As New ArrayList
        Dim j As Integer

        For j = 0 To nodes.Count - 1
            item = New CourseLocation

            elem = nodes(j)("CODE")
            If elem Is Nothing Then
            Else
                item.Code = elem.InnerText
            End If
            elem = nodes(j)("NAME")
            If elem Is Nothing Then
            Else
                item.Name = elem.InnerText
            End If
            elem = nodes(j)("ADDRESS1")
            If elem Is Nothing Then
            Else
                item.Address1 = elem.InnerText
            End If
            elem = nodes(j)("ADDRESS2")
            If elem Is Nothing Then
            Else
                item.Address2 = elem.InnerText
            End If
            elem = nodes(j)("ADDRESS3")
            If elem Is Nothing Then
            Else
                item.Address3 = elem.InnerText
            End If
            elem = nodes(j)("CITY")
            If elem Is Nothing Then
            Else
                item.City = elem.InnerText
            End If
            elem = nodes(j)("STATE")
            If elem Is Nothing Then
            Else
                item.State = elem.InnerText
            End If
            elem = nodes(j)("ZIP")
            If elem Is Nothing Then
            Else
                item.Zip = elem.InnerText
            End If
            elem = nodes(j)("PHONE")
            If elem Is Nothing Then
            Else
                item.Phone = elem.InnerText
            End If
            elem = nodes(j)("FAX")
            If elem Is Nothing Then
            Else
                item.Fax = elem.InnerText
            End If
            elem = nodes(j)("EXTENSION")
            If elem Is Nothing Then
            Else
                item.Extension = elem.InnerText
            End If
            elem = nodes(j)("SEQUENCENUMBER")
            If elem Is Nothing Then
            Else
                item.SequenceNumber = elem.InnerText
            End If
            elem = nodes(j)("STATUSCODE")
            If elem Is Nothing Then
            Else
                item.StatusCode = elem.InnerText
            End If
            elem = nodes(j)("UPDATEDATE")
            If elem Is Nothing Then
            Else
                item.UpdateDate = elem.InnerText
            End If
            elem = nodes(j)("NOTES")
            If elem Is Nothing Then
            Else
                item.Notes = elem.InnerText
            End If
            elem = nodes(j)("DIRECTION")
            If elem Is Nothing Then
            Else
                item.Direction = nodes(j)("DIRECTION").InnerText
            End If
            elem = nodes(j)("ACCOMODATIONS")
            If elem Is Nothing Then
            Else
                item.Accomodations = elem.InnerText
            End If
            elem = nodes(j)("TERRITORYNAME")
            If elem Is Nothing Then
            Else
                item.Country.TerritoryName = elem.InnerText
            End If
            elem = nodes(j)("ISOCOUNTRYCODE")
            If elem Is Nothing Then
            Else
                item.Country.Country = elem.InnerText
            End If
            item_list.Add(item)
        Next

        Dim items(item_list.Count - 1) As CourseLocation
        item_list.CopyTo(items)

        Return items
    End Function

    Public Function GetCourseLocationNames(ByRef defaultValue As String) As String()
        Dim doc As XmlDocument
        Dim cmd As New OracleCommand("GetTrainingLocationName")

        doc = GetResults(cmd)

        Dim nodes As XmlNodeList = doc.DocumentElement.ChildNodes
        Dim item As String
        Dim item_list As New ArrayList
        Dim j As Integer

        item_list.Add(defaultValue)
        For j = 0 To nodes.Count - 1
            item = String.Empty
            item = nodes(j)("NAME").InnerText
            item_list.Add(item)
        Next

        Dim items(item_list.Count - 1) As String
        item_list.CopyTo(items)

        Return items
    End Function

    Public Sub StoreLocation(ByRef Location As CourseLocation, ByRef context As TransactionContext, Optional ByVal Action As CoreObject.ActionType = CoreObject.ActionType.ADD)
        Dim cmd As New OracleCommand

        Select Case Action
            Case CoreObject.ActionType.ADD
                cmd.CommandText = "AddTrainingLocation"
                cmd.Parameters.Add("RetCode", OracleDbType.Int32, ParameterDirection.Output)
                cmd.Parameters.Add("RetVal", OracleDbType.Int32, ParameterDirection.Output)
            Case CoreObject.ActionType.UPDATE
                cmd.CommandText = "UpdateTrainingLocation"
                cmd.Parameters.Add("RetVal", OracleDbType.Int32, ParameterDirection.Output)
                cmd.Parameters.Add("xCode", OracleDbType.Int32)
                cmd.Parameters("xCode").Value = Integer.Parse(Location.Code)
            Case CoreObject.ActionType.DELETE
                cmd.CommandText = "DeleteTrainingLocation"
                cmd.Parameters.Add("xCode", OracleDbType.Int32)
                cmd.Parameters("xCode").Value = Integer.Parse(Location.Code)
        End Select
        cmd.Parameters.Add("xName", OracleDbType.Varchar2)
        cmd.Parameters("xName").Value = Location.Name
        cmd.Parameters.Add("xAddress1", OracleDbType.Varchar2)
        cmd.Parameters("xAddress1").Value = Location.Address1
        If Not String.IsNullOrWhiteSpace(Location.Address2) Then
            cmd.Parameters.Add("xAddress2", OracleDbType.Varchar2)
            cmd.Parameters("xAddress2").Value = Location.Address2
        End If
        If Not String.IsNullOrWhiteSpace(Location.Address3) Then
            cmd.Parameters.Add("xAddress3", OracleDbType.Varchar2)
            cmd.Parameters("xAddress3").Value = Location.Address3
        End If
        cmd.Parameters.Add("xCity", OracleDbType.Varchar2)
        cmd.Parameters("xCity").Value = Location.City
        cmd.Parameters.Add("xState", OracleDbType.Varchar2)
        cmd.Parameters("xState").Value = Location.State
        cmd.Parameters.Add("xZip", OracleDbType.Varchar2)
        cmd.Parameters("xZip").Value = Location.Zip
        cmd.Parameters.Add("xPhone", OracleDbType.Varchar2)
        cmd.Parameters("xPhone").Value = Location.Phone
        If Not String.IsNullOrWhiteSpace(Location.Notes) Then
            cmd.Parameters.Add("xNotes", OracleDbType.Varchar2)
            cmd.Parameters("xNotes").Value = Location.Notes
        End If
        If Not String.IsNullOrWhiteSpace(Location.Fax) Then
            cmd.Parameters.Add("xFax", OracleDbType.Varchar2)
            cmd.Parameters("xFax").Value = Location.Fax
        End If
        If Not String.IsNullOrWhiteSpace(Location.Extension) Then
            cmd.Parameters.Add("xExtension", OracleDbType.Varchar2)
            cmd.Parameters("xExtension").Value = Location.Extension
        End If
        If Not String.IsNullOrWhiteSpace(Location.Direction) Then
            cmd.Parameters.Add("xDirection", OracleDbType.Varchar2)
            cmd.Parameters("xDirection").Value = Location.Direction
        End If
        cmd.Parameters.Add("xISOCountryCode", OracleDbType.Varchar2)
        cmd.Parameters("xISOCountryCode").Value = Location.Country.Country
        If Not String.IsNullOrWhiteSpace(Location.Accomodations) Then
            cmd.Parameters.Add("xAccomodations", OracleDbType.Varchar2)
            cmd.Parameters("xAccomodations").Value = Location.Accomodations
        End If

        ExecuteCMD(cmd, context)

        If Action = CoreObject.ActionType.ADD Then
            If Not CType(cmd.Parameters("RetCode")?.Value, INullable).IsNull Then
                Location.Code = cmd.Parameters("RetCode").Value.ToString()
            End If
        End If
        If (Action = CoreObject.ActionType.ADD Or Action = CoreObject.ActionType.UPDATE) Then
            If Not CType(cmd.Parameters("RetVal")?.Value, INullable).IsNull Then
                Location.SequenceNumber = Integer.Parse(cmd.Parameters("RetVal").Value.ToString())
            End If
        End If
    End Sub

    ' Functions for maintaining CourseType
    Public Function GetTypes() As CourseType()
        Dim doc As XmlDocument
        Dim cmd As New OracleCommand("GETTRAININGTYPE")

        doc = GetResults(cmd)

        'internalize
        Dim nodes As XmlNodeList = doc.DocumentElement.ChildNodes
        Dim item As CourseType
        Dim item_list As New ArrayList
        Dim j As Integer

        For j = 0 To nodes.Count - 1
            item = New CourseType With {
                .Code = If(nodes(j)("CODE")?.InnerText, ""),
                .Description = If(nodes(j)("DESCRIPTION")?.InnerText, "")
            }

            item_list.Add(item)
        Next

        Dim items(item_list.Count - 1) As CourseType
        item_list.CopyTo(items)

        Return items
    End Function

    Public Sub StoreType(ByRef Type As CourseType, ByRef context As TransactionContext, Optional ByVal Action As CoreObject.ActionType = CoreObject.ActionType.ADD)
        Dim cmd As New OracleCommand

        Select Case Action
            Case CoreObject.ActionType.ADD
                cmd.CommandText = "AddTrainingType"
                cmd.Parameters.Add("xCode", OracleDbType.Varchar2)
                cmd.Parameters("xCode").Value = Type.Code
                cmd.Parameters.Add("xDescription", OracleDbType.Varchar2)
                cmd.Parameters("xDescription").Value = Type.Description
            Case CoreObject.ActionType.UPDATE
                cmd.CommandText = "UpdateTrainingType"
                cmd.Parameters.Add("xCode", OracleDbType.Varchar2)
                cmd.Parameters("xCode").Value = Type.Code
                cmd.Parameters.Add("xDescription", OracleDbType.Varchar2)
                cmd.Parameters("xDescription").Value = Type.Description
            Case CoreObject.ActionType.DELETE
                cmd.CommandText = "DeleteTrainingType"
                cmd.Parameters.Add("xCode", OracleDbType.Varchar2)
                cmd.Parameters("xCode").Value = Type.Code
        End Select

        ExecuteCMD(cmd, context)
    End Sub

    Public Function GetModels(ByVal Model As String) As CourseModel()
        Dim doc As XmlDocument
        Dim nodes As XmlNodeList
        Dim item_list As New ArrayList
        Dim j As Integer
        Dim cmd As New OracleCommand("GetTrainingModel")
        cmd.Parameters.Add("xModel", OracleDbType.Varchar2)
        cmd.Parameters("xModel").Value = Model.Trim()

        doc = GetResults(cmd)

        nodes = doc.DocumentElement.ChildNodes

        For j = 0 To nodes.Count - 1
            item_list.Add(New CourseModel With {
                .Model = nodes(j)("MODEL").InnerText,
                .CourseNumber = nodes(j)("COURSENUMBER").InnerText
            })
        Next

        Dim items(item_list.Count - 1) As CourseModel
        item_list.CopyTo(items)

        Return items
    End Function

    Public Function GetModelNumber(ByRef defaultValue As String) As String()
        Dim doc As XmlDocument
        Dim nodes As XmlNodeList
        Dim item_list As New ArrayList
        Dim j As Integer
        Dim cmd As New OracleCommand("GetTrainingModelNumber")

        doc = GetResults(cmd)
        nodes = doc.DocumentElement.ChildNodes

        If Not String.IsNullOrWhiteSpace(defaultValue) Then item_list.Add(defaultValue)
        For j = 0 To nodes.Count - 1
            item_list.Add(nodes(j)("MODEL").InnerText)
        Next

        Dim items(item_list.Count - 1) As String
        item_list.CopyTo(items)

        Return items
    End Function

    Public Function GetModelNumberByCourseNumber(ByRef courseNumber As String) As String
        Dim doc As XmlDocument
        Dim nodes As XmlNodeList
        Dim item As String = ""
        Dim j As Integer
        Dim cmd As New OracleCommand("GETTRAININGMODELNUMBERBYCN")

        cmd.Parameters.Add("xCourseNumber", OracleDbType.Varchar2, 25)
        cmd.Parameters("xCourseNumber").Value = courseNumber

        doc = GetResults(cmd)

        nodes = doc.DocumentElement.ChildNodes

        For j = 0 To nodes.Count - 1
            item = nodes(j)("MODEL").InnerText
        Next

        Return item
    End Function

    Public Sub StoreModel(ByRef OldModel As String, ByRef Model As CourseModel, ByRef context As TransactionContext, Optional ByVal Action As CoreObject.ActionType = CoreObject.ActionType.ADD)
        Dim cmd As New OracleCommand

        Select Case Action
            Case CoreObject.ActionType.ADD
                cmd.CommandText = "AddTrainingModel"
                cmd.Parameters.Add("xModel", OracleDbType.Varchar2)
                cmd.Parameters("xModel").Value = Model.Model
                cmd.Parameters.Add("xCourseNumber", OracleDbType.Varchar2)
                cmd.Parameters("xCourseNumber").Value = Model.CourseNumber
            Case CoreObject.ActionType.UPDATE
                cmd.CommandText = "UpdateTrainingModel"
                cmd.Parameters.Add("xModel", OracleDbType.Varchar2)
                cmd.Parameters("xModel").Value = OldModel
                cmd.Parameters.Add("xNewModel", OracleDbType.Varchar2)
                cmd.Parameters("xNewModel").Value = Model.Model
                cmd.Parameters.Add("xCourseNumber", OracleDbType.Varchar2)
                cmd.Parameters("xCourseNumber").Value = Model.CourseNumber
            Case CoreObject.ActionType.DELETE
                cmd.CommandText = "DeleteTrainingModel"
                cmd.Parameters.Add("xModel", OracleDbType.Varchar2)
                cmd.Parameters("xModel").Value = Model.Model
                cmd.Parameters.Add("xCourseNumber", OracleDbType.Varchar2)
                cmd.Parameters("xCourseNumber").Value = Model.CourseNumber
        End Select

        ExecuteCMD(cmd, context)
    End Sub

    Public Sub StoreTrainingOrder(ByRef TrainingOrder As Order, ByRef context As TransactionContext, Optional ByVal Action As CoreObject.ActionType = CoreObject.ActionType.ADD)
        Dim cmd As New OracleCommand
        Dim debugMessage As String = $"CourseDataManager.StoreTrainingOrder - START at {Date.Now.ToShortTimeString}{Environment.NewLine}"

        Try    ' ASleight - This was temporarily put in for debugging purposes. The Data project typically lets the other layers handle errors.
            Select Case Action
                Case CoreObject.ActionType.ADD
                    cmd.CommandText = "AddTrainingOrder"

                    cmd.Parameters.Add("xCustomerID", OracleDbType.Int32)
                    cmd.Parameters("xCustomerID").Value = TrainingOrder.Customer.CustomerID
                    cmd.Parameters.Add("xAllocatedGrandTotal", OracleDbType.Decimal)
                    cmd.Parameters("xAllocatedGrandTotal").Value = TrainingOrder.AllocatedGrandTotal
                    cmd.Parameters.Add("xAllocatedShippingAmount", OracleDbType.Decimal)
                    cmd.Parameters("xAllocatedShippingAmount").Value = TrainingOrder.AllocatedShipping
                    'If Not String.IsNullOrWhiteSpace(TrainingOrder.VerisgnAuthorizationCode) Then
                    cmd.Parameters.Add("xVerisignAuthAuthCode", OracleDbType.Varchar2)
                    cmd.Parameters("xVerisignAuthAuthCode").Value = TrainingOrder.VerisgnAuthorizationCode
                    'End If
                    cmd.Parameters.Add("xSAPBillToAccountNumber", OracleDbType.Varchar2, 10)
                    cmd.Parameters("xSAPBillToAccountNumber").Value = If(TrainingOrder.SAPBillToAccountNumber, DBNull.Value)
                    cmd.Parameters.Add("xCreditCardSequenceNumber", OracleDbType.Int32)
                    cmd.Parameters("xCreditCardSequenceNumber").Value = If(TrainingOrder.CreditCard?.SequenceNumber, DBNull.Value)
                    cmd.Parameters.Add("xPONumber", OracleDbType.Varchar2)
                    cmd.Parameters("xPONumber").Value = If(TrainingOrder.POReference, "")
                    cmd.Parameters.Add("xShippingMethodID", OracleDbType.Int32)
                    cmd.Parameters("xShippingMethodID").Value = If(TrainingOrder.ShippingMethod?.ShippingMethodCode, DBNull.Value)

                    ' Add Bill To details - Changed it like this to prevent Oracle error "Wrong type or number of parameters"
                    cmd.Parameters.Add("xBillToName", OracleDbType.Varchar2)
                    cmd.Parameters.Add("xBillToSTreet1", OracleDbType.Varchar2)
                    cmd.Parameters.Add("xBillToStreet2", OracleDbType.Varchar2, 50)
                    cmd.Parameters.Add("xBillToStreet3", OracleDbType.Varchar2, 50)
                    cmd.Parameters.Add("xBillToCity", OracleDbType.Varchar2, 50)
                    cmd.Parameters.Add("xBillToState", OracleDbType.Varchar2, 2)
                    cmd.Parameters.Add("xBillToPostalCode", OracleDbType.Varchar2, 10)
                    If TrainingOrder.BillTo IsNot Nothing Then
                        cmd.Parameters("xBillToName").Value = TrainingOrder.BillTo.Name
                        cmd.Parameters("xBillToSTreet1").Value = TrainingOrder.BillTo.Line1
                        cmd.Parameters("xBillToStreet2").Value = TrainingOrder.BillTo.Line2
                        cmd.Parameters("xBillToStreet3").Value = TrainingOrder.BillTo.Line3
                        cmd.Parameters("xBillToCity").Value = TrainingOrder.BillTo.City
                        cmd.Parameters("xBillToState").Value = TrainingOrder.BillTo.State
                        cmd.Parameters("xBillToPostalCode").Value = TrainingOrder.BillTo.PostalCode
                    End If

                    ' Add Ship To details - Changed it like this to prevent Oracle error "Wrong type or number of parameters"
                    cmd.Parameters.Add("xShipToName", OracleDbType.Varchar2, 50)
                    cmd.Parameters.Add("xShipToStreet1", OracleDbType.Varchar2, 50)
                    cmd.Parameters.Add("xShipToStreet2", OracleDbType.Varchar2, 50)
                    cmd.Parameters.Add("xShipToStreet3", OracleDbType.Varchar2, 50)
                    cmd.Parameters.Add("xShipToCity", OracleDbType.Varchar2, 50)
                    cmd.Parameters.Add("xShipToState", OracleDbType.Varchar2, 2)
                    cmd.Parameters.Add("xShipToPostalCode", OracleDbType.Varchar2, 10)
                    If TrainingOrder.ShipTo IsNot Nothing Then
                        cmd.Parameters("xShipToName").Value = TrainingOrder.ShipTo.Name
                        cmd.Parameters("xShipToStreet1").Value = TrainingOrder.ShipTo.Line1
                        cmd.Parameters("xShipToStreet2").Value = TrainingOrder.ShipTo.Line2
                        cmd.Parameters("xShipToStreet3").Value = TrainingOrder.ShipTo.Line3
                        cmd.Parameters("xShipToCity").Value = TrainingOrder.ShipTo.City
                        cmd.Parameters("xShipToState").Value = TrainingOrder.ShipTo.State
                        cmd.Parameters("xShipToPostalCode").Value = TrainingOrder.ShipTo.PostalCode
                    End If
                    Dim outparam As New OracleParameter("retOrderNumber", OracleDbType.Varchar2, 15)
                    outparam.Direction = ParameterDirection.Output
                    cmd.Parameters.Add(outparam)

                    cmd.Parameters.Add("xhttp_x_forwarded_for", OracleDbType.Varchar2)
                    cmd.Parameters("xhttp_x_forwarded_for").Value = TrainingOrder.HTTP_X_Forward_For
                    cmd.Parameters.Add("xremote_addr", OracleDbType.Varchar2)
                    cmd.Parameters("xremote_addr").Value = TrainingOrder.RemoteADDR
                    cmd.Parameters.Add("xhttp_referer", OracleDbType.Varchar2)
                    cmd.Parameters("xhttp_referer").Value = TrainingOrder.HTTPReferer
                    cmd.Parameters.Add("xhttp_url", OracleDbType.Varchar2)
                    cmd.Parameters("xhttp_url").Value = TrainingOrder.HTTPURL
                    cmd.Parameters.Add("xhttp_user_agent", OracleDbType.Varchar2)
                    cmd.Parameters("xhttp_user_agent").Value = TrainingOrder.HTTPUserAgent

                    cmd.Parameters.Add("xtransactiontype", OracleDbType.Int32)
                    cmd.Parameters("xtransactiontype").Value = TrainingOrder.TransactionType
                    cmd.Parameters.Add("xcustomersequencenumber", OracleDbType.Int32)
                    cmd.Parameters("xcustomersequencenumber").Value = TrainingOrder.Customer.SequenceNumber

                    cmd.Parameters.Add("retVal", OracleDbType.Int32, ParameterDirection.Output)
                Case CoreObject.ActionType.UPDATE
                    cmd.CommandText = "UpdateTrainingOrder"
                    cmd.Parameters.Add("xOrderNumber", OracleDbType.Varchar2)
                    cmd.Parameters("xOrderNumber").Value = TrainingOrder.OrderNumber
                    cmd.Parameters.Add("xSequenceNumber", OracleDbType.Int32)
                    cmd.Parameters("xSequenceNumber").Value = TrainingOrder.SequenceNumber
                    cmd.Parameters.Add("xCustomerID", OracleDbType.Int32)
                    cmd.Parameters("xCustomerID").Value = TrainingOrder.Customer.CustomerID
                    cmd.Parameters.Add("xAllocatedGrandTotal", OracleDbType.Decimal)
                    cmd.Parameters("xAllocatedGrandTotal").Value = TrainingOrder.AllocatedGrandTotal
                    cmd.Parameters.Add("xAllocatedShippingAmount", OracleDbType.Decimal)
                    cmd.Parameters("xAllocatedShippingAmount").Value = TrainingOrder.AllocatedShipping
                    If Not (TrainingOrder.VerisgnAuthorizationCode Is Nothing) Then
                        cmd.Parameters.Add("xVerisignAuthAuthCode", OracleDbType.Varchar2)
                        cmd.Parameters("xVerisignAuthAuthCode").Value = TrainingOrder.VerisgnAuthorizationCode
                    End If
                    'Commented By Navdeep kaur forresolving 6397
                    'If Not (TrainingOrder.VerisignTransactionCode Is Nothing) Then
                    '    cmd.Parameters.Add("xVerisignAuthTxnCode", OracleDbType.Varchar2)
                    '    cmd.Parameters("xVerisignAuthTxnCode").Value = TrainingOrder.VerisignTransactionCode
                    'End If
                    If Not (TrainingOrder.SAPBillToAccountNumber Is Nothing) Then
                        cmd.Parameters.Add("xSAPBillToAccountNumber", OracleDbType.Varchar2, 10)
                        cmd.Parameters("xSAPBillToAccountNumber").Value = TrainingOrder.SAPBillToAccountNumber
                    End If
                    If Not (TrainingOrder.CreditCard Is Nothing) Then
                        cmd.Parameters.Add("xCreditCardSequenceNumber", OracleDbType.Int32)
                        cmd.Parameters("xCreditCardSequenceNumber").Value = TrainingOrder.CreditCard.SequenceNumber
                    End If
                    If Not (TrainingOrder.POReference Is Nothing) Then
                        cmd.Parameters.Add("xPONumber", OracleDbType.Varchar2)
                        cmd.Parameters("xPONumber").Value = TrainingOrder.POReference
                    End If
                    If Not (TrainingOrder.ShippingMethod Is Nothing) Then
                        cmd.Parameters.Add("xShippingMethodID", OracleDbType.Int32)
                        cmd.Parameters("xShippingMethodID").Value = TrainingOrder.ShippingMethod.ShippingMethodCode
                    End If
                    If Not (TrainingOrder.BillTo.Name Is Nothing) Then
                        cmd.Parameters.Add("xBillToName", OracleDbType.Varchar2)
                        cmd.Parameters("xBillToName").Value = TrainingOrder.BillTo.Name
                    End If
                    If Not (TrainingOrder.BillTo.Line1 Is Nothing) Then
                        cmd.Parameters.Add("xBillToSTreet1", OracleDbType.Varchar2)
                        cmd.Parameters("xBillToSTreet1").Value = TrainingOrder.BillTo.Line1
                    End If
                    If Not (TrainingOrder.BillTo.Line2 Is Nothing) Then
                        cmd.Parameters.Add("xBillToStreet2", OracleDbType.Varchar2, 50)
                        cmd.Parameters("xBillToStreet2").Value = TrainingOrder.BillTo.Line2
                    End If
                    If Not (TrainingOrder.BillTo.Line3 Is Nothing) Then
                        cmd.Parameters.Add("xBillToStreet3", OracleDbType.Varchar2, 50)
                        cmd.Parameters("xBillToStreet3").Value = TrainingOrder.BillTo.Line3
                    End If
                    If Not (TrainingOrder.BillTo.City Is Nothing) Then
                        cmd.Parameters.Add("xBillToCity", OracleDbType.Varchar2, 50)
                        cmd.Parameters("xBillToCity").Value = TrainingOrder.BillTo.City
                    End If
                    If Not (TrainingOrder.BillTo.State Is Nothing) Then
                        cmd.Parameters.Add("xBillToState", OracleDbType.Varchar2, 2)
                        cmd.Parameters("xBillToState").Value = TrainingOrder.BillTo.State
                    End If
                    If Not (TrainingOrder.BillTo.PostalCode Is Nothing) Then
                        cmd.Parameters.Add("xBillToPostalCode", OracleDbType.Varchar2, 10)
                        cmd.Parameters("xBillToPostalCode").Value = TrainingOrder.BillTo.PostalCode
                    End If
                    If Not (TrainingOrder.ShipTo Is Nothing) Then
                        cmd.Parameters.Add("xShipToName", OracleDbType.Varchar2, 50)
                        cmd.Parameters("xShipToName").Value = TrainingOrder.ShipTo.Name

                        cmd.Parameters.Add("xShipToStreet1", OracleDbType.Varchar2, 50)
                        cmd.Parameters("xShipToStreet1").Value = TrainingOrder.ShipTo.Line1

                        cmd.Parameters.Add("xShipToStreet2", OracleDbType.Varchar2, 50)
                        cmd.Parameters("xShipToStreet2").Value = TrainingOrder.ShipTo.Line2

                        cmd.Parameters.Add("xShipToStreet3", OracleDbType.Varchar2, 50)
                        cmd.Parameters("xShipToStreet3").Value = TrainingOrder.ShipTo.Line3

                        cmd.Parameters.Add("xShipToCity", OracleDbType.Varchar2, 50)
                        cmd.Parameters("xShipToCity").Value = TrainingOrder.ShipTo.City

                        cmd.Parameters.Add("xShipToState", OracleDbType.Varchar2, 2)
                        cmd.Parameters("xShipToState").Value = TrainingOrder.ShipTo.State

                        cmd.Parameters.Add("xShipToPostalCode", OracleDbType.Varchar2, 10)
                        cmd.Parameters("xShipToPostalCode").Value = TrainingOrder.ShipTo.PostalCode
                    End If
                    cmd.Parameters.Add(New OracleParameter("retVal", OracleDbType.Int32, ParameterDirection.Output))
                Case CoreObject.ActionType.DELETE
                    cmd.CommandText = "DeleteTrainingOrder"
            End Select

            cmd.Parameters.Add("xBILLTOATTN", OracleDbType.Varchar2, 50)
            cmd.Parameters("xBILLTOATTN").Value = If(TrainingOrder.BillTo?.Attn, DBNull.Value)

            cmd.Parameters.Add("xBILLTOPHONENUMBER", OracleDbType.Varchar2, 50)
            cmd.Parameters("xBILLTOPHONENUMBER").Value = If(TrainingOrder.BillTo?.PhoneNumber, DBNull.Value)

            cmd.Parameters.Add("xBILLTOPHONEEXTENSION", OracleDbType.Varchar2, 50)
            cmd.Parameters("xBILLTOPHONEEXTENSION").Value = If(TrainingOrder.BillTo?.PhoneExt, DBNull.Value)

            debugMessage &= $" - PARAMETERS: "
            For Each param As OracleParameter In cmd.Parameters
                debugMessage &= $"{param.ParameterName} = {param.Value}, "
            Next

            debugMessage &= $"{Environment.NewLine} - Executing Command {cmd.CommandText}{Environment.NewLine}"
            ExecuteCMD(cmd, context)
            Select Case Action
                Case CoreObject.ActionType.ADD
                    If Not CType(cmd.Parameters("retOrderNumber")?.Value, INullable).IsNull Then
                        debugMessage &= $" - Add retOrderNumber = {cmd.Parameters("retOrderNumber").Value}"
                        TrainingOrder.OrderNumber = cmd.Parameters("retOrderNumber").Value.ToString()
                    End If
                    If Not CType(cmd.Parameters("retVal")?.Value, INullable).IsNull Then
                        debugMessage &= $" - Add retVal = {cmd.Parameters("retVal").Value}"
                        TrainingOrder.SequenceNumber = cmd.Parameters("retVal").Value.ToString()
                    End If
                Case CoreObject.ActionType.UPDATE
                    If Not CType(cmd.Parameters("retVal")?.Value, INullable).IsNull Then
                        debugMessage &= $" - Update retVal = {cmd.Parameters("retVal").Value}"
                        TrainingOrder.SequenceNumber = cmd.Parameters("retVal").Value.ToString()
                    End If
                    'store invoices
                    For Each inv As Invoice In TrainingOrder.Invoices
                        StoreInvoice(inv, context)
                    Next
            End Select
        Catch ex As Exception
            ServicesPlusException.Utilities.LogMessages(debugMessage)
            ServicesPlusException.Utilities.WrapExceptionforUI(ex)
            Throw 'ex
        End Try
    End Sub

    Private Sub StoreInvoice(ByVal inv As Invoice, ByVal context As TransactionContext)
        Dim cmd As New OracleCommand("addInvoice")

        cmd.Parameters.Add("xInvoiceNumber", OracleDbType.Varchar2, 20)
        cmd.Parameters("xInvoiceNumber").Value = inv.InvoiceNumber
        cmd.Parameters.Add("xOrderNumber", OracleDbType.Varchar2, 20)
        cmd.Parameters("xOrderNumber").Value = inv.Order.OrderNumber
        cmd.Parameters.Add("xSequenceNumber", OracleDbType.Int32)
        cmd.Parameters("xSequenceNumber").Value = inv.Order.SequenceNumber
        cmd.Parameters.Add("xInvoiceTotal", OracleDbType.Decimal)
        cmd.Parameters("xInvoiceTotal").Value = inv.Total

        ExecuteCMD(cmd, context)
    End Sub

    Public Function SearchTrainingClass(ByRef model As String, ByRef number As String, ByRef title As String, ByRef startDate As String, ByRef endDate As String, ByRef city As String, ByRef state As String, ByRef type As String, ByRef majorCategory As String, ByVal blnSIAM As Boolean, Optional ByVal ShowFutureClass As Boolean = False, Optional ByVal StatusCode As Short = -1, Optional ByVal Hide As Short = -1, Optional ByVal ShowReservationOnly As Boolean = False) As ArrayList
        Dim cmd As New OracleCommand With {
            .CommandType = CommandType.StoredProcedure,
            .BindByName = True
        }
        Dim myAdaptor As OracleDataAdapter
        Dim dataset As New DataSet
        Dim xml_data As String
        Dim debugMessage As String = $"CourseDataManager.SearchTrainingClass START at {Date.Now.ToShortTimeString()}.{Environment.NewLine}"

        Try
            If blnSIAM = True And Hide <> -1 And StatusCode <> -1 Then
                cmd.CommandText = "trainingplus.SIAMTRAININCLASSSEARCH"
                cmd.Parameters.Add("xShowFutureClass", OracleDbType.Int16)
                cmd.Parameters("xShowFutureClass").Value = IIf(ShowFutureClass, 1, 0)
                cmd.Parameters.Add("xStatusCode", OracleDbType.Int16)
                cmd.Parameters("xStatusCode").Value = StatusCode
                cmd.Parameters.Add("xHide", OracleDbType.Int16)
                cmd.Parameters("xHide").Value = Hide
                cmd.Parameters.Add("xShowReservationOnly", OracleDbType.Int16)
                cmd.Parameters("xShowReservationOnly").Value = IIf(ShowReservationOnly, 1, 0)
            Else
                cmd.CommandText = "trainingplus.searchTrainingPLUS"
            End If
            debugMessage &= $"- Command Text: {cmd.CommandText}.{Environment.NewLine}"

            Dim modelParameter = New OracleParameter("xCourseModel", OracleDbType.Varchar2, 50)
            modelParameter.Value = $"{If(model?.ToUpper(), "")}%"
            cmd.Parameters.Add(modelParameter)

            cmd.Parameters.Add("xCourseNumber", OracleDbType.Varchar2, 25)
            cmd.Parameters("xCourseNumber").Value = $"{If(number?.ToUpper(), "")}%"
            cmd.Parameters.Add("xCourseTitle", OracleDbType.Varchar2, 100)
            cmd.Parameters("xCourseTitle").Value = $"%{If(title?.ToUpper(), "")}%"
            If Not String.IsNullOrWhiteSpace(startDate) Then
                cmd.Parameters.Add("xStartDate", OracleDbType.Varchar2, 40)
                cmd.Parameters("xStartDate").Value = startDate
                debugMessage &= $"- Start Date: {startDate}{Environment.NewLine}"
            End If
            If Not String.IsNullOrWhiteSpace(endDate) Then
                cmd.Parameters.Add("xEndDate", OracleDbType.Varchar2, 40)
                cmd.Parameters("xEndDate").Value = endDate
                debugMessage &= $"- End Date: {endDate}{Environment.NewLine}"
            End If
            cmd.Parameters.Add("xCourseCity", OracleDbType.Varchar2, 25)
            cmd.Parameters("xCourseCity").Value = $"%{If(city?.ToUpper(), "")}%"
            cmd.Parameters.Add("xCourseState", OracleDbType.Varchar2, 20)
            cmd.Parameters("xCourseState").Value = $"%{If(state?.ToUpper(), "")}%"
            cmd.Parameters.Add("xCourseType", OracleDbType.Varchar2, 50)
            cmd.Parameters("xCourseType").Value = $"%{If(type?.ToUpper(), "")}%"
            cmd.Parameters.Add("xMajorCourseCategory", OracleDbType.Varchar2, 25)
            cmd.Parameters("xMajorCourseCategory").Value = $"%{If(majorCategory?.ToUpper(), "")}%"

            cmd.Parameters.Add(New OracleParameter("xCourseCursor", OracleDbType.RefCursor, ParameterDirection.Output))

            OpenConnection()
            cmd.Connection = dbConnection
            debugMessage &= $"- Parameters set, connection opened.{Environment.NewLine}"
            myAdaptor = New OracleDataAdapter(cmd)

            myAdaptor.Fill(dataset)
            debugMessage &= $"- Dataset filled from adapter.{Environment.NewLine}"
            xml_data = dataset.GetXml()

            debugMessage &= $"- Calling Dehydrate {Environment.NewLine}"
            Return DehydrateTrainingClasses(xml_data)
        Catch ex As Exception
            ServicesPlusException.Utilities.LogMessages(debugMessage)
            'ServicesPlusException.Utilities.WrapExceptionforUI(ex)
            Throw 'ex
        Finally
            CloseConnection()
        End Try
    End Function

    Public Function SearchTrainingClass(ByRef partNumber As String) As ArrayList
        Dim cmd As New OracleCommand("trainingplus.searchTrainingPLUSByPartNumber") With {
            .CommandType = CommandType.StoredProcedure,
            .BindByName = True
        }

        Dim partNumberParameter = New OracleParameter("xPartNumber", OracleDbType.Varchar2, 50)
        partNumberParameter.Value = partNumber.ToUpper()
        cmd.Parameters.Add(partNumberParameter)

        cmd.Parameters.Add(New OracleParameter("xCourseCursor", OracleDbType.RefCursor, ParameterDirection.Output))

        OpenConnection()
        cmd.Connection = dbConnection

        Dim myAdaptor As New OracleDataAdapter(cmd)
        Dim dataset As New DataSet
        Dim xml_data As String

        Try
            myAdaptor.Fill(dataset)
            xml_data = dataset.GetXml()
            'Console.WriteLine(xml_data)
        Catch ex As Exception
            'log
            Console.WriteLine("troubles calling searchtraingplus by part number: " + ex.Message)
            Throw 'ex
        End Try

        CloseConnection()

        Return DehydrateTrainingClasses(xml_data)
    End Function

    Protected Function DehydrateTrainingClasses(ByRef trainingClassXmlData As String) As ArrayList
        Dim trainingclass_list As New ArrayList
        'internalize model information
        Dim doc As New XmlDocument

        Try
            doc.LoadXml(trainingClassXmlData)

            Dim nodes As XmlNodeList = doc.DocumentElement.ChildNodes
            Dim cs As CourseSchedule

            For Each node As XmlNode In nodes
                cs = New CourseSchedule With {
                    .OldClass = Convert.ToInt16(If(node("OLDCLASS")?.InnerText, "0")),
                    .StartDate = If(node("STARTDATE")?.InnerText, "2001-01-01"),
                    .EndDate = If(node("ENDDATE")?.InnerText, "2001-01-01"),
                    .Time = If(node("TIME")?.InnerText, ""),
                    .PartNumber = If(node("PARTNUMBER")?.InnerText, ""),
                    .StatusCode = Convert.ToInt16(If(node("STATUSCODE")?.InnerText, "0")),
                    .Hide = Convert.ToInt16(If(node("HIDE")?.InnerText, "0")),
                    .Course = New Course() With {
                        .Number = If(node("COURSENUMBER")?.InnerText, ""),
                        .Title = If(node("TITLE")?.InnerText, ""),
                        .ListPrice = Convert.ToDouble(If(node("PRICE")?.InnerText, "0.0")),
                        .Model = If(node("MODEL")?.InnerText, ""),
                        .PreRequisite = If(node("PREREQUISITES")?.InnerText, ""),
                        .Description = If(node("DESCRIPTION")?.InnerText, ""),
                        .URL = If(node("URL")?.InnerText, ""),
                        .MinorCategory = New CourseMinorCategory(If(node("MINORCATEGORY")?.InnerText, "")),
                        .MajorCategory = New CourseMajorCategory(If(node("MAJORCATEGORY")?.InnerText, "")),
                        .Type = New CourseType() With {
                            .Code = If(node("COURSETYPE")?.InnerText, ""),
                            .TypeCode = Convert.ToInt16(If(node("TYPECODE")?.InnerText, "0"))
                        }
                    },
                    .Instructor = New CourseInstructor(If(node("INSTRUCTORCODE")?.InnerText, "")),
                    .Location = New CourseLocation() With {
                        .Name = If(node("LOCATIONNAME")?.InnerText, ""),
                        .Address1 = If(node("ADDRESS1")?.InnerText, ""),
                        .Address2 = If(node("ADDRESS2")?.InnerText, ""),
                        .Address3 = If(node("ADDRESS3")?.InnerText, ""),
                        .City = If(node("CITY")?.InnerText, ""),
                        .State = If(node("STATE")?.InnerText, ""),
                        .Zip = If(node("ZIP")?.InnerText, ""),
                        .Code = If(node("LOCATIONCODE")?.InnerText, ""),
                        .Country = New CourseCountry(If(node("TERRITORYNAME")?.InnerText, ""))
                    }
                }

                trainingclass_list.Add(cs)
            Next
        Catch ex As Exception
            'log error
            Throw 'ex
        End Try

        Return trainingclass_list
    End Function

    Public Function SearchTrainingReservation(ByVal customerID As Integer, ByRef model As String, ByRef number As String, ByRef title As String, ByRef startDate As String, ByRef endDate As String, ByRef location As String, ByRef type As String, ByRef majorCategory As String) As CourseReservation()
        Dim trainingresv_list As New ArrayList
        Dim doc As New XmlDocument
        Dim myAdaptor As OracleDataAdapter
        Dim dataset As New DataSet
        Dim xml_data As String

        Dim cmd As New OracleCommand("trainingplus.searchCustomerReservation") With {
            .CommandType = CommandType.StoredProcedure,
            .BindByName = True
        }

        cmd.Parameters.Add("xCustomerSIAMID", OracleDbType.Int16)
        cmd.Parameters("xCustomerSIAMID").Value = customerID
        cmd.Parameters.Add("xCourseModel", OracleDbType.Varchar2, 50)
        cmd.Parameters("XModelNumber").Value = model.ToUpper() + "%"
        cmd.Parameters.Add("xCourseNumber", OracleDbType.Varchar2, 25)
        cmd.Parameters("xCourseNumber").Value = number.ToUpper() + "%"
        cmd.Parameters.Add("xCourseTitle", OracleDbType.Varchar2, 60)
        cmd.Parameters("xCourseTitle").Value = "%" + title + "%"
        If (startDate Is Nothing Or startDate = "") Then
            cmd.Parameters.Add("xStartDate", OracleDbType.Varchar2, 40)
            cmd.Parameters("xStartDate").Value = startDate
        End If
        If (endDate Is Nothing Or endDate = "") Then
            cmd.Parameters.Add("xEndDate", OracleDbType.Varchar2, 40)
            cmd.Parameters("xEndDate").Value = endDate
        End If
        cmd.Parameters.Add("xCourseLocation", OracleDbType.Varchar2, 30)
        cmd.Parameters("xCourseLocation").Value = "%" + location + "%"
        cmd.Parameters.Add("xCourseType", OracleDbType.Varchar2, 50)
        cmd.Parameters("xCourseType").Value = "%" + type + "%"
        cmd.Parameters.Add("xMajorCourseCategory", OracleDbType.Varchar2, 25)
        cmd.Parameters("xMajorCourseCategory").Value = "%" + majorCategory + "%"

        cmd.Parameters.Add(New OracleParameter("xCourseCursor", OracleDbType.RefCursor, ParameterDirection.Output))

        OpenConnection()
        cmd.Connection = dbConnection
        myAdaptor = New OracleDataAdapter(cmd)

        Try
            myAdaptor.Fill(dataset)
            xml_data = dataset.GetXml()
            'Console.WriteLine(xml_data)
        Catch ex As Exception
            'log
            Console.WriteLine("troubles calling searchCustomerReservation: " + ex.Message)
            Throw 'ex
        End Try

        CloseConnection()

        Try
            doc.LoadXml(xml_data)

            Dim nodes As XmlNodeList = doc.DocumentElement.ChildNodes
            Dim j As Integer
            Dim cs As CourseReservation

            For j = 0 To nodes.Count - 1
                cs = New CourseReservation With {
                    .CustomerSIAMID = customerID,
                    .ReservationNumber = If(nodes(j)("RESERVATIONNUMBER")?.InnerText, ""),
                    .Sentinvoiceorquote = Convert.ToInt16(If(nodes(j)("SENTINVOICEORQUOTE")?.InnerText, "0")),
                    .CourseSchedule = New CourseSchedule With {
                        .PartNumber = If(nodes(j)("PARTNUMBER")?.InnerText, "")
                    }
                }

                trainingresv_list.Add(cs)
            Next

        Catch ex As Exception
            'log error
            Throw 'ex
        End Try

        Dim css(trainingresv_list.Count - 1) As CourseReservation
        trainingresv_list.CopyTo(css)
        Return css
    End Function

    Public Function GetOpenTrainingReservation(ByVal getActiveRecord As Boolean, ByVal blnDoSort As Boolean, ByVal sortKey As String, ByVal sortOrder As Short, ByRef dsReturn As DataSet) As CourseReservation()
        Try
            Dim cmd As New OracleCommand With {
                .CommandType = CommandType.StoredProcedure,
                .BindByName = True
            }

            If getActiveRecord Then
                cmd.CommandText = "trainingplus.getActiveTrainingOrder"
            Else
                cmd.CommandText = "trainingplus.getOpenTrainingOrder"
            End If

            cmd.Parameters.Add(New OracleParameter("xCourseCSCursor", OracleDbType.RefCursor, ParameterDirection.Output))

            OpenConnection()
            cmd.Connection = dbConnection

            Dim myAdaptor As New OracleDataAdapter(cmd)
            myAdaptor.Fill(dsReturn)
            CloseConnection()
        Catch ex As Exception

        End Try

        Return SortCourseReservation(dsReturn, blnDoSort, sortKey, sortOrder)
    End Function

    Public Function SortCourseReservation(ByRef dsDataSet As DataSet, ByVal blnDoSort As Boolean, ByVal sortKey As String, ByVal sortOrder As Short) As CourseReservation()
        Dim trainingresv_list As New ArrayList
        Dim dvDataView As DataView

        Try
            dvDataView = New DataView(dsDataSet.Tables(0))
            If blnDoSort Then
                If sortOrder = 1 Then
                    dvDataView.Sort = sortKey + " ASC"
                Else
                    dvDataView.Sort = sortKey + " DESC"
                End If
            End If
        Catch ex As Exception
            Console.WriteLine("troubles calling getOpenTrainingOrder: " + ex.Message)
            Throw 'ex
        End Try

        Try
            Dim cr As CourseReservation
            Dim rowCounter As Integer = 0
            While (rowCounter < dvDataView.Count)
                cr = New CourseReservation With {
                    .ReservationNumber = dvDataView.Item(rowCounter)("RESERVATIONNUMBER"),
                    .CustomerFirstName = dvDataView.Item(rowCounter)("FIRSTNAME"),
                    .CustomerLastName = dvDataView.Item(rowCounter)("LASTNAME"),
                    .UpdateDate = dvDataView.Item(rowCounter)("UPDATEDATE")
                }
                cr.CourseSchedule.Course.Title = dvDataView.Item(rowCounter)("TITLE")
                If Not dvDataView.Item(rowCounter)("STARTDATE") Is DBNull.Value Then cr.CourseSchedule.StartDate = dvDataView.Item(rowCounter)("STARTDATE")
                trainingresv_list.Add(cr)
                rowCounter += 1
            End While
        Catch ex As Exception
            Throw 'ex
        End Try
        Dim css(trainingresv_list.Count - 1) As CourseReservation
        trainingresv_list.CopyTo(css)
        Return css
    End Function

    Public Function GetUnconfirmedTrainingOrder() As CourseReservation()
        Dim trainingresv_list As New ArrayList
        Dim myAdaptor As OracleDataAdapter
        Dim dataset As New DataSet
        Dim xml_data As String
        Dim cmd As New OracleCommand("trainingplus.getUnconfirmedTrainingOrder") With {
            .CommandType = CommandType.StoredProcedure,
            .BindByName = True
        }

        cmd.Parameters.Add(New OracleParameter("xCourseOrderCursor", OracleDbType.RefCursor, ParameterDirection.Output))

        OpenConnection()
        cmd.Connection = dbConnection

        myAdaptor = New OracleDataAdapter(cmd)

        Try
            myAdaptor.Fill(dataset)
            xml_data = dataset.GetXml()
            'Console.WriteLine(xml_data)
        Catch ex As Exception
            'log
            Console.WriteLine("troubles calling getUnconfirmedTrainingOrder: " + ex.Message)
            Throw 'ex
        End Try

        CloseConnection()

        'internalize model information
        Dim doc As New XmlDocument

        Try
            doc.LoadXml(xml_data)
            Dim nodes As XmlNodeList = doc.DocumentElement.ChildNodes

            For Each node As XmlNode In nodes
                trainingresv_list.Add(New CourseReservation With {
                    .CustomerSIAMID = Convert.ToInt64(If(node("CUSTOMERID")?.InnerText, "0")),
                    .ReservationNumber = If(node("RESERVATIONNUMBER")?.InnerText, ""),
                    .CourseSchedule = New CourseSchedule() With {
                        .PartNumber = If(node("PARTNUMBER")?.InnerText, "")
                    }
                })
            Next

        Catch ex As Exception
            'log error
            Throw 'ex
        End Try

        Dim css(trainingresv_list.Count - 1) As CourseReservation
        trainingresv_list.CopyTo(css)

        Return css
    End Function

    Public Sub GetCourseVacancy(ByRef courseSchedule As CourseSchedule, ByVal bIncludingWait As Boolean, ByRef context As TransactionContext)
        Dim cmd As New OracleCommand("GETCOURSEVACANCY")
        cmd.CommandType = CommandType.StoredProcedure

        cmd.Parameters.Add("xPartNumber", OracleDbType.Varchar2)
        cmd.Parameters("xPartNumber").Value = courseSchedule.PartNumber
        If Not bIncludingWait Then
            cmd.Parameters.Add("xIncludingWait", OracleDbType.Int16)
            cmd.Parameters("xIncludingWait").Value = 0
        End If

        cmd.Parameters.Add(New OracleParameter("retVal", OracleDbType.Int32, ParameterDirection.Output))

        OpenConnection()
        cmd.Connection = dbConnection

        ExecuteCMD(cmd, context)

        If Not CType(cmd.Parameters("retVal")?.Value, INullable).IsNull Then
            courseSchedule.Vacancy = Convert.ToInt16(cmd.Parameters("retVal").Value.ToString())
        End If
    End Sub

    Public Function GetCertificateCourses() As DataSet
        Dim doc As DataSet
        Dim cmd As New OracleCommand("TRAININGPLUS.GetCertificateCourse")

        cmd.Parameters.Add(New OracleParameter("xCourseCursor", OracleDbType.RefCursor, ParameterDirection.Output))
        doc = GetResults(cmd, True)

        Return doc
    End Function

    Public Function GetClassCoursesAndLocation() As DataSet
        Dim doc As DataSet
        Dim cmd As New OracleCommand("TRAININGPLUS.GetClassCoursesAndLocation")

        cmd.Parameters.Add(New OracleParameter("xCourseCursor", OracleDbType.RefCursor, ParameterDirection.Output))
        cmd.Parameters.Add(New OracleParameter("xLocationCursor", OracleDbType.RefCursor, ParameterDirection.Output))

        doc = GetResults(cmd, True)

        Return doc
    End Function

    Public Function GetCertificateCodeForCourse(ByVal CourseNumber As String) As DataSet
        Dim doc As DataSet
        Dim cmd As New OracleCommand("TRAININGPLUS.GetCertificateCodeForCourse")

        cmd.Parameters.Add("xCourseNumber", OracleDbType.Varchar2)
        cmd.Parameters("xCourseNumber").Value = CourseNumber

        cmd.Parameters.Add(New OracleParameter("xCertificateCursor", OracleDbType.RefCursor, ParameterDirection.Output))
        doc = GetResults(cmd, True)

        Return doc
    End Function

    Public Function GetTempReservationAndClass(ByVal CourseNumber As String, ByVal Location As Short) As DataSet
        Dim doc As DataSet
        Dim cmd As New OracleCommand("TRAININGPLUS.GetTempReservationAndClass")

        cmd.Parameters.Add("xCourseNumber", OracleDbType.Varchar2, 50)
        cmd.Parameters("xCourseNumber").Value = CourseNumber

        cmd.Parameters.Add("xLocation", OracleDbType.Int16)
        cmd.Parameters("xLocation").Value = Location

        cmd.Parameters.Add(New OracleParameter("xClassCoursor", OracleDbType.RefCursor, ParameterDirection.Output))
        cmd.Parameters.Add(New OracleParameter("xTempReservationCoursor", OracleDbType.RefCursor, ParameterDirection.Output))

        doc = GetResults(cmd, True)

        Return doc
    End Function

    Public Function GetCertificateOrderLinesForOrder(ByVal CourseNumber As String, ByVal Location As Short) As DataSet
        Dim doc As DataSet
        Dim cmd As New OracleCommand("TRAININGPLUS.GetTempReservationAndClass")

        cmd.Parameters.Add("xCourseNumber", OracleDbType.Varchar2, 50)
        cmd.Parameters("xCourseNumber").Value = CourseNumber

        cmd.Parameters.Add("xLocation", OracleDbType.Int16)
        cmd.Parameters("xLocation").Value = Location

        cmd.Parameters.Add(New OracleParameter("xClassCoursor", OracleDbType.RefCursor, ParameterDirection.Output))
        cmd.Parameters.Add(New OracleParameter("xTempReservationCoursor", OracleDbType.RefCursor, ParameterDirection.Output))

        doc = GetResults(cmd, True)

        Return doc
    End Function

    Public Function SaveCertificateForCourse(ByVal CourseNumber As String, ByVal Certificate As String, ByRef context As TransactionContext) As String
        Dim cmd As New OracleCommand("ADDTRAININGCERTIFICATE")

        cmd.Parameters.Add("xCourseNumber", OracleDbType.Varchar2)
        cmd.Parameters("xCourseNumber").Value = CourseNumber
        cmd.Parameters.Add("xCertificate", OracleDbType.Varchar2)
        cmd.Parameters("xCertificate").Value = Certificate

        Dim outparam As New OracleParameter("retVal", OracleDbType.Varchar2, 4000)
        outparam.Direction = ParameterDirection.Output

        cmd.Parameters.Add(outparam)
        ExecuteCMD(cmd, context, True)
        If Not CType(cmd.Parameters("retVal")?.Value, INullable).IsNull Then
            Return cmd.Parameters("retVal").Value.ToString()
        End If

        Return Nothing
    End Function
    Public Function UpdateActivationCodeForOrderLine(ByVal OrderNumber As String, ByVal OrderLineNumner As Short, ByVal ActivationCode As String, ByRef context As TransactionContext) As String
        Dim cmd As New OracleCommand("UpdateOrderLineActivationCode")

        cmd.Parameters.Add("xORDERNUMBER", OracleDbType.Varchar2)
        cmd.Parameters("xORDERNUMBER").Value = OrderNumber
        cmd.Parameters.Add("xORDERLINENUMBER", OracleDbType.Int16)
        cmd.Parameters("xORDERLINENUMBER").Value = OrderLineNumner
        cmd.Parameters.Add("xACTIVATIONCODE", OracleDbType.Varchar2)
        cmd.Parameters("xACTIVATIONCODE").Value = ActivationCode

        Dim outparam As New OracleParameter("xRETURNVAL", OracleDbType.Varchar2, 500)
        outparam.Direction = ParameterDirection.Output

        cmd.Parameters.Add(outparam)
        ExecuteCMD(cmd, context, True)
        If Not CType(cmd.Parameters("xRETURNVAL")?.Value, INullable).IsNull Then
            Return cmd.Parameters("xRETURNVAL").Value.ToString()
        End If

        Return Nothing
    End Function

    Public Function UpdateTempTrainingReservation(ByVal PartNumber As String, ByVal CourseNumber As String, ByVal Location As Short, ByVal ReservationNumber As String, ByRef context As TransactionContext) As Integer
        Dim cmd As New OracleCommand("UpdateTempTrainingReservation")

        cmd.Parameters.Add("xCourseNumber", OracleDbType.Varchar2, 50)
        cmd.Parameters("xCourseNumber").Value = CourseNumber

        cmd.Parameters.Add("xPartNumber", OracleDbType.Varchar2, 50)
        cmd.Parameters("xPartNumber").Value = PartNumber

        cmd.Parameters.Add("xLocation", OracleDbType.Int16)
        cmd.Parameters("xLocation").Value = Location

        cmd.Parameters.Add("xReservationNumber", OracleDbType.Varchar2, 2000)
        cmd.Parameters("xReservationNumber").Value = ReservationNumber

        cmd.Parameters.Add(New OracleParameter("retVal", OracleDbType.Int16, ParameterDirection.Output))
        ExecuteCMD(cmd, context, True)
        If Not CType(cmd.Parameters("retVal")?.Value, INullable).IsNull Then
            Return Convert.ToInt32(cmd.Parameters("retVal").Value.ToString())
        End If

        Return Nothing
    End Function

    Public Function DeleteTempTrainingReservation(ByVal CourseNumber As String, ByVal Location As Short, ByVal ReservationNumber As String, ByRef context As TransactionContext) As Integer
        Dim cmd As New OracleCommand("DeleteTempTrainingReservation")

        cmd.Parameters.Add("xCourseNumber", OracleDbType.Varchar2, 50)
        cmd.Parameters("xCourseNumber").Value = CourseNumber

        cmd.Parameters.Add("xLocation", OracleDbType.Int16)
        cmd.Parameters("xLocation").Value = Location

        cmd.Parameters.Add("xReservationNumber", OracleDbType.Varchar2, 2000)
        cmd.Parameters("xReservationNumber").Value = ReservationNumber

        cmd.Parameters.Add(New OracleParameter("retVal", OracleDbType.Int16, ParameterDirection.Output))
        ExecuteCMD(cmd, context, True)
        If Not CType(cmd.Parameters("retVal")?.Value, INullable).IsNull Then
            Return Convert.ToInt32(cmd.Parameters("retVal").Value.ToString())
        End If

        Return Nothing
    End Function

    Public Sub GetCourseType(ByRef courseType As CourseType, ByRef context As TransactionContext)
        Dim cmd As New OracleCommand("GETCOURSETYPE")

        cmd.Parameters.Add("xPartNumber", OracleDbType.Varchar2)
        cmd.Parameters("xPartNumber").Value = courseType.PartNumber

        cmd.Parameters.Add(New OracleParameter("retVal", OracleDbType.Int32, ParameterDirection.Output))

        OpenConnection()
        cmd.Connection = dbConnection

        ExecuteCMD(cmd, context)

        If Not CType(cmd.Parameters("retVal")?.Value, INullable).IsNull Then
            courseType.Code = Convert.ToInt16(cmd.Parameters("retVal").Value.ToString())
        End If
    End Sub

    ' Functions for retrieving course and its children class in XML 
    Public Function GetClassParticipants(ByRef classNumber As String, ByRef status As Char, ByVal classLocation As String, ByVal HideStatus As Short, ByVal DeleteStatus As Short) As DataSet
        Dim cmd As New OracleCommand("TRAININGPLUS.getClassParticipant")

        cmd.Parameters.Add(New OracleParameter("xPartNumber", OracleDbType.Varchar2, ParameterDirection.Input)).Value = classNumber
        cmd.Parameters.Add(New OracleParameter("xLocation", OracleDbType.Int16, ParameterDirection.Input)).Value = Convert.ToInt16(classLocation)
        If Not String.IsNullOrWhiteSpace(status) Then
            cmd.Parameters.Add(New OracleParameter("xParticipantStatus", OracleDbType.Char, ParameterDirection.Input)).Value = status
        End If

        cmd.Parameters.Add(New OracleParameter("xStatus", OracleDbType.Int16, ParameterDirection.Input)).Value = DeleteStatus
        cmd.Parameters.Add(New OracleParameter("xHide", OracleDbType.Int16, ParameterDirection.Input)).Value = HideStatus
        cmd.Parameters.Add(New OracleParameter("xParticipantCursor", OracleDbType.RefCursor, ParameterDirection.Output))

        Dim ds As DataSet = GetResults(cmd, True)

        Return ds
    End Function

    Public Function GetCourseParticipants(ByRef classNumber As String, ByRef status As Char, ByVal classLocation As String, ByVal HideStatus As Short, ByVal DeleteStatus As Short) As DataSet
        Dim cmd As New OracleCommand("TRAININGPLUS.getCourseParticipant")

        cmd.Parameters.Add(New OracleParameter("xCourseNumber", OracleDbType.Varchar2, ParameterDirection.Input)).Value = classNumber
        cmd.Parameters.Add(New OracleParameter("xLocation", OracleDbType.Int16, ParameterDirection.Input)).Value = Convert.ToInt16(classLocation)
        cmd.Parameters.Add(New OracleParameter("xStatus", OracleDbType.Int16, ParameterDirection.Input)).Value = DeleteStatus
        cmd.Parameters.Add(New OracleParameter("xHide", OracleDbType.Int16, ParameterDirection.Input)).Value = HideStatus
        If Not String.IsNullOrWhiteSpace(status) Then
            cmd.Parameters.Add(New OracleParameter("xParticipantStatus", OracleDbType.Char, ParameterDirection.Input)).Value = status
        End If

        cmd.Parameters.Add(New OracleParameter("xParticipantCursor", OracleDbType.RefCursor, ParameterDirection.Output))

        Dim ds As DataSet = GetResults(cmd, True)

        Return ds
    End Function

    ' Functions for retrieving course and its children class in XML 
    Public Function GetClassParticipants(ByRef classNumber As String, ByRef status As Char) As DataSet
        Dim cmd As New OracleCommand("TRAININGPLUS.getClassParticipants")

        cmd.Parameters.Add(New OracleParameter("xPartNumber", OracleDbType.Varchar2)).Direction = ParameterDirection.Input
        cmd.Parameters("xPartNumber").Value = classNumber
        If Not String.IsNullOrWhiteSpace(status) Then
            cmd.Parameters.Add(New OracleParameter("xParticipantStatus", OracleDbType.Char)).Direction = ParameterDirection.Input
            cmd.Parameters("xParticipantStatus").Value = status
        End If
        cmd.Parameters.Add(New OracleParameter("xClassCursor", OracleDbType.RefCursor)).Direction = ParameterDirection.Output
        cmd.Parameters.Add(New OracleParameter("xParticipantCursor", OracleDbType.RefCursor)).Direction = ParameterDirection.Output

        Dim ds As DataSet = GetResults(cmd, True)

        ds.DataSetName = "CourseParticipant"
        ds.Tables(0).TableName = "Class"
        ds.Tables(1).TableName = "Participants"

        Return ds
    End Function

    Public Function GetPendingParticipants(ByRef currentDate As String) As Hashtable
        Dim reservation_list As New Hashtable
        Dim myAdaptor As OracleDataAdapter
        Dim doc As New XmlDocument
        Dim dataset As New DataSet
        Dim xml_data As String

        Dim cmd As New OracleCommand("trainingplus.GETPENDINGCOURSEPARTICIPANT") With {
            .CommandType = CommandType.StoredProcedure,
            .BindByName = True
        }

        cmd.Parameters.Add("xCurrentDate", OracleDbType.Varchar2, 40)
        cmd.Parameters("xCurrentDate").Value = currentDate

        cmd.Parameters.Add(New OracleParameter("xParticipantCursor", OracleDbType.RefCursor, ParameterDirection.Output))

        OpenConnection()
        cmd.Connection = dbConnection

        myAdaptor = New OracleDataAdapter(cmd)

        Try
            myAdaptor.Fill(dataset)
            xml_data = dataset.GetXml()
            'Console.WriteLine(xml_data)
        Catch ex As Exception
            'log
            Console.WriteLine("troubles calling GETPENDINGCOURSEPARTICIPANT: " + ex.Message)
            Throw 'ex
        End Try

        CloseConnection()

        Try
            doc.LoadXml(xml_data)

            Dim nodes As XmlNodeList = doc.DocumentElement.ChildNodes
            Dim elem As XmlElement
            Dim j As Integer
            Dim rs As CourseReservation
            Dim reservationNumber As String

            For j = 0 To nodes.Count - 1
                rs = New CourseReservation

                elem = nodes(j)("RESERVATIONNUMBER")
                If elem IsNot Nothing Then
                    reservationNumber = elem.InnerText
                    If reservation_list(reservationNumber) IsNot Nothing Then
                        rs = CType(reservation_list(reservationNumber), CourseReservation)
                    Else
                        rs = New CourseReservation
                        rs.ReservationNumber = reservationNumber
                        reservation_list(reservationNumber) = rs
                    End If

                    elem = nodes(j)("PARTNUMBER")
                    If elem IsNot Nothing Then rs.CourseSchedule.PartNumber = elem.InnerText

                    elem = nodes(j)("STARTDATE")
                    If elem IsNot Nothing Then rs.CourseSchedule.StartDate = elem.InnerText

                    elem = nodes(j)("ENDDATE")
                    If elem IsNot Nothing Then rs.CourseSchedule.EndDate = elem.InnerText

                    elem = nodes(j)("TIME")
                    If elem IsNot Nothing Then rs.CourseSchedule.Time = elem.InnerText

                    elem = nodes(j)("COURSENUMBER")
                    If elem IsNot Nothing Then rs.CourseSchedule.Course.Number = elem.InnerText

                    elem = nodes(j)("TITLE")
                    If elem IsNot Nothing Then rs.CourseSchedule.Course.Title = elem.InnerText

                    elem = nodes(j)("LOCATIONNAME")
                    If elem IsNot Nothing Then rs.CourseSchedule.Location.Name = elem.InnerText

                    elem = nodes(j)("ADDRESS1")
                    If elem IsNot Nothing Then rs.CourseSchedule.Location.Address1 = elem.InnerText

                    elem = nodes(j)("ADDRESS2")
                    If elem IsNot Nothing Then rs.CourseSchedule.Location.Address2 = elem.InnerText

                    elem = nodes(j)("ADDRESS3")
                    If elem IsNot Nothing Then rs.CourseSchedule.Location.Address3 = elem.InnerText

                    elem = nodes(j)("CITY")
                    If elem IsNot Nothing Then rs.CourseSchedule.Location.City = elem.InnerText

                    elem = nodes(j)("STATE")
                    If elem IsNot Nothing Then rs.CourseSchedule.Location.State = elem.InnerText

                    elem = nodes(j)("ZIP")
                    If elem IsNot Nothing Then rs.CourseSchedule.Location.Zip = elem.InnerText

                    elem = nodes(j)("PHONE")
                    If elem IsNot Nothing Then rs.CourseSchedule.Location.Phone = elem.InnerText

                    elem = nodes(j)("CUSTOMERSIAMID")
                    If elem IsNot Nothing Then rs.CustomerSIAMID = elem.InnerText

                    elem = nodes(j)("ADMINSIAMID")
                    If elem IsNot Nothing Then rs.AdminSIAMID = elem.InnerText
                End If
            Next
        Catch ex As Exception
            'log error
            Throw 'ex
        End Try

        Return reservation_list
    End Function

    Public Function GetReservationParticipants(ByRef ReservationNumber As String, ByVal bConfirmed As Boolean) As ArrayList
        Dim participant_list As New ArrayList
        Dim doc As XmlDocument
        Dim cmd As New OracleCommand

        cmd.CommandType = CommandType.StoredProcedure
        If bConfirmed Then
            cmd.CommandText = "trainingplus.GETCONFIRMEDPARTICIPANT"
        Else
            cmd.CommandText = "trainingplus.GETTRAININGPARTICIPANT"
        End If

        cmd.Parameters.Add("xReservationNumber", OracleDbType.Varchar2, 15)
        cmd.Parameters("xReservationNumber").Value = ReservationNumber

        doc = GetResults(cmd)

        Try
            Dim nodes As XmlNodeList = doc.DocumentElement.ChildNodes
            Dim elem As XmlElement
            Dim j As Integer
            Dim pt As CourseParticipant

            For j = 0 To nodes.Count - 1
                pt = New CourseParticipant

                elem = nodes(j)("RESERVATIONNUMBER")
                If elem IsNot Nothing Then pt.ReservationNumber = elem.InnerText

                elem = nodes(j)("FIRSTNAME")
                If elem IsNot Nothing Then pt.FirstName = elem.InnerText

                elem = nodes(j)("LASTNAME")
                If elem IsNot Nothing Then pt.LastName = elem.InnerText

                elem = nodes(j)("EMAIL")
                If elem IsNot Nothing Then pt.EMail = elem.InnerText

                elem = nodes(j)("PHONE")
                If elem IsNot Nothing Then pt.Phone = elem.InnerText

                elem = nodes(j)("STATUS")
                If elem IsNot Nothing Then pt.Status = elem.InnerText

                elem = nodes(j)("ADDRESS1")
                If elem IsNot Nothing Then pt.Address1 = elem.InnerText

                elem = nodes(j)("ADDRESS2")
                If elem IsNot Nothing Then pt.Address2 = elem.InnerText

                elem = nodes(j)("ADDRESS3")
                If elem IsNot Nothing Then pt.Address3 = elem.InnerText

                elem = nodes(j)("CITY")
                If elem IsNot Nothing Then pt.City = elem.InnerText

                elem = nodes(j)("STATE")
                If elem IsNot Nothing Then pt.State = elem.InnerText

                elem = nodes(j)("ISOCOUNTRYCODE")
                If elem IsNot Nothing Then pt.ISOCountryCode = elem.InnerText

                elem = nodes(j)("ZIP")
                If elem IsNot Nothing Then pt.Zip = elem.InnerText

                elem = nodes(j)("EXTENSION")
                If elem IsNot Nothing Then pt.Extension = elem.InnerText

                elem = nodes(j)("DATERESERVED")
                If elem IsNot Nothing Then pt.DateReserved = elem.InnerText

                elem = nodes(j)("DATEWAITED")
                If elem IsNot Nothing Then pt.DateWaited = elem.InnerText

                elem = nodes(j)("DATECANCELLED")
                If elem IsNot Nothing Then pt.DateCanceled = elem.InnerText
            Next
        Catch ex As Exception
            'log error
            Throw 'ex
        End Try

        Return participant_list
    End Function

    Public Function SearchParticipants(ByRef Name As String, ByRef EMail As String, ByRef Company As String) As CourseParticipant()
        Try
            Dim cmd As New OracleCommand("SEARCHTRAININGPARTICIPANTS")
            Dim doc As XmlDocument
            cmd.Parameters.Add("xName", OracleDbType.Varchar2, 50)
            cmd.Parameters("xName").Value = Name
            cmd.Parameters.Add("xEMail", OracleDbType.Varchar2, 100)
            cmd.Parameters("xEMail").Value = EMail
            cmd.Parameters.Add("xCompany", OracleDbType.Varchar2, 100)
            cmd.Parameters("xCompany").Value = Company

            doc = GetResults(cmd)

            Dim nodes As XmlNodeList = doc.DocumentElement.ChildNodes
            Dim elem As XmlElement
            Dim item As CourseParticipant
            Dim item_list As New ArrayList
            Dim j As Integer

            For j = 0 To nodes.Count - 1
                item = New CourseParticipant

                elem = nodes(j)("RESERVATIONNUMBER")
                If elem IsNot Nothing Then item.ReservationNumber = elem.InnerText()

                elem = nodes(j)("FIRSTNAME")
                If elem IsNot Nothing Then item.FirstName = elem.InnerText()

                elem = nodes(j)("LASTNAME")
                If elem IsNot Nothing Then item.LastName = elem.InnerText()

                elem = nodes(j)("EMAIL")
                If elem IsNot Nothing Then item.EMail = elem.InnerText()

                elem = nodes(j)("PHONE")
                If elem IsNot Nothing Then item.Phone = elem.InnerText()

                elem = nodes(j)("STATUS")
                If elem IsNot Nothing Then item.Status = elem.InnerText()

                elem = nodes(j)("DATERESERVED")
                If elem IsNot Nothing Then item.DateReserved = elem.InnerText()

                elem = nodes(j)("DATECANCELED")
                If elem IsNot Nothing Then item.DateCanceled = elem.InnerText()

                elem = nodes(j)("DATEWAITED")
                If elem IsNot Nothing Then item.DateWaited = elem.InnerText()

                elem = nodes(j)("EXTENSION")
                If elem IsNot Nothing Then item.Extension = elem.InnerText()

                elem = nodes(j)("FAX")
                If elem IsNot Nothing Then item.Fax = elem.InnerText()

                elem = nodes(j)("NOTES")
                If elem IsNot Nothing Then item.Notes = elem.InnerText()

                elem = nodes(j)("ADDRESS1")
                If elem IsNot Nothing Then item.Address1 = elem.InnerText()

                elem = nodes(j)("ADDRESS2")
                If elem IsNot Nothing Then item.Address2 = elem.InnerText()

                elem = nodes(j)("ADDRESS3")
                If elem IsNot Nothing Then item.Address3 = elem.InnerText()

                elem = nodes(j)("CITY")
                If elem IsNot Nothing Then item.City = elem.InnerText()

                elem = nodes(j)("STATE")
                If elem IsNot Nothing Then item.State = elem.InnerText()

                elem = nodes(j)("ZIP")
                If elem IsNot Nothing Then item.Zip = elem.InnerText()

                elem = nodes(j)("ISOCOUNTRYCODE")
                If elem IsNot Nothing Then item.ISOCountryCode = elem.InnerText()

                elem = nodes(j)("COUNTRY")
                If elem IsNot Nothing Then item.Country = elem.InnerText()

                elem = nodes(j)("SEQUENCENUMBER")
                If elem IsNot Nothing Then item.SequenceNumber = Convert.ToInt16(elem.InnerText())

                elem = nodes(j)("UPDATEDSIAMID")
                If elem IsNot Nothing Then item.UpdatedSIAMID = elem.InnerText()

                elem = nodes(j)("UPDATEDATE")
                If elem IsNot Nothing Then item.UpdateDate = elem.InnerText()

                elem = nodes(j)("COMPANY")
                If elem IsNot Nothing Then item.Company = elem.InnerText()

                item_list.Add(item)
            Next

            Dim items(item_list.Count - 1) As CourseParticipant
            item_list.CopyTo(items)

            Return items

        Catch ex As System.Exception
            Throw New Exception("Error searching students:" + ex.Message)
        End Try

    End Function

    Public Function CheckIsPreviousDateNull(ByRef context As TransactionContext, ByVal PartNumber As String) As Boolean
        Dim cmd As New OracleCommand("CHECKISPREVIOUSDATENULL")
        Dim IsPreviousDateNull As Boolean = False
        cmd.Parameters.Add("xpartNumber", OracleDbType.Varchar2, 20)
        cmd.Parameters("xpartNumber").Value = PartNumber
        Dim outparam As New OracleParameter("retVal", OracleDbType.Varchar2, 10)
        outparam.Direction = ParameterDirection.Output
        cmd.Parameters.Add(outparam)

        OpenConnection()
        cmd.Connection = dbConnection

        Try
            ExecuteCMD(cmd, context)
            If Not CType(cmd.Parameters("retVal")?.Value, INullable).IsNull Then
                IsPreviousDateNull = CType(cmd.Parameters("retVal").Value.ToString(), Boolean)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message())
        End Try

        Return IsPreviousDateNull
    End Function
End Class
