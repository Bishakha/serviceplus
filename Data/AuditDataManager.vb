Imports System.Xml
Imports Oracle.ManagedDataAccess.Client
Imports Oracle.ManagedDataAccess.Types
Imports Sony.US.SIAMUtilities
Imports ServicesPlusException

Public Class AuditDataManager
    Protected cn As OracleConnection
    Protected connString As String
    Protected connection_string As String

    Public Sub New()


        cn = New OracleConnection
        'Dim config_settings As Hashtable
        'Dim handler As New ConfigHandler("SOFTWARE\SIAM")
        'config_settings = handler.GetConfigSettings("//item[@name='CurrentDAL']")
        'connString = config_settings.Item("ConnectionStringSPS").ToString()

        'New Class implementation for Configuration
        connString = ConfigurationData.Environment.DataBase.ConnectionString
        cn.ConnectionString = connString

    End Sub
    Public Sub CloseConnection()
        If cn.State = ConnectionState.Open Then
            cn.Close()
        End If
    End Sub

    Public Function GetAuditTrail(ByVal pStartDate As String, ByVal pEndDate As String, ByVal pEventType As String) As XmlDocument

        Dim return_value As OracleClob
        Dim doc As New XmlDocument
        Dim error_node As XmlNode
        Dim outparam As New OracleParameter("retVal", OracleDbType.Clob, ParameterDirection.Output)

        Dim cmd As New OracleCommand With {
            .Connection = cn,
            .CommandType = CommandType.StoredProcedure,
            .CommandText = "GETCCAUDITTRAIL"
        }

        cmd.Parameters.Add("xStartDate", OracleDbType.Varchar2)
        cmd.Parameters("xStartDate").Value = pStartDate

        cmd.Parameters.Add("xEndDate", OracleDbType.Varchar2)
        cmd.Parameters("xEndDate").Value = pEndDate

        cmd.Parameters.Add("xEventType", OracleDbType.Varchar2)
        cmd.Parameters("xEventType").Value = pEventType

        cmd.Parameters.Add(outparam)
        ExecuteCMD(cmd, Nothing, False)

        If CType(cmd.Parameters("retVal")?.Value, INullable).IsNull Then
            If Not CType(cmd.Parameters("errorCondition")?.Value, INullable).IsNull Then
                'error condition was set in procedure
                Dim error_message As String
                error_message = cmd.Parameters("errorCondition").Value.ToString()
                Throw New ServicesPLUSOracleException(error_message)
            End If
            Throw New ServicesPLUSOracleException(ERRORCODE.ora06550)
        Else
            return_value = cmd.Parameters("retVal").Value
            return_value.Seek(0, IO.SeekOrigin.Begin)

            Try
                doc.Load(return_value)
            Catch ex As Exception
                Dim excp As String
                excp = ex.Message()
                excp = ex.StackTrace()
                Throw New ServicesPLUSOracleException(ERRORCODE.ora06550, ex)
            End Try

            cn.Close()

            error_node = doc.SelectSingleNode("//ERROR")

            If error_node Is Nothing Then
            Else
                Throw New ServicesPLUSOracleException(ERRORCODE.ora06550)
            End If
        End If

        Return doc
    End Function

    Public Sub ExecuteCMD(ByRef command As OracleCommand, ByRef context As TransactionContext, Optional ByVal auto_close_connection As Boolean = True)
        command.CommandType = CommandType.StoredProcedure
        command.BindByName = True

        Try
            'if no transactional context then manage the connection here with no transaction.
            If context Is Nothing Then
                If cn.State = ConnectionState.Closed Then
                    cn.Open()
                    command.Connection = cn
                End If
            Else
                command.Connection = context.NativeConnection
                command.Transaction = context.NativeTransaction
            End If

            Try
                command.ExecuteNonQuery()
            Catch ex As Exception
                Throw New ServicesPLUSOracleException(ERRORCODE.ora06550, ex)
            End Try

            If context Is Nothing And auto_close_connection Then
                cn.Close()
            End If
        Catch ex As Exception
            Throw New ServicesPLUSOracleException(ERRORCODE.ora06550, ex)
        End Try
    End Sub

    Public Function SelectCC(ByVal ccNo As Integer) As String
        Dim outccNo = String.Empty
        Dim context As TransactionContext = Nothing
        Dim cmd = New OracleCommand With {
            .CommandType = CommandType.StoredProcedure,
            .CommandText = "GETCCNUMBER"
        }

        cmd.Parameters.Add("xCUSTNO", OracleDbType.Int32)
        cmd.Parameters("xCUSTNO").Value = ccNo
        Dim outparam1 As New OracleParameter("retVal", OracleDbType.Varchar2, 100)
        outparam1.Direction = ParameterDirection.Output
        cmd.Parameters.Add(outparam1)

        If cn.State = ConnectionState.Closed Then cn.Open()
        cmd.Connection = cn

        ExecuteCMD(cmd, context)


        If Not CType(cmd.Parameters("retVal")?.Value, INullable).IsNull Then
            outccNo = cmd.Parameters("retVal").Value.ToString()
        End If
        Return outccNo
    End Function


End Class
