Imports System.Xml
Imports System.Collections
Imports Oracle.ManagedDataAccess.Client
Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.SIAMUtilities
Imports ServicesPlusException
Imports System.Collections.Generic
Imports Oracle.ManagedDataAccess.Types

Public Class TechBulletinDataManager
    Inherits DataManager

    Public Function GetProductLines() As TechBulletin.ProductLines()
        Dim doc As XmlDocument
        Dim cmd As New OracleCommand

        cmd.CommandText = "getTBProductLine"

        doc = GetResults(cmd)

        Dim root As XmlElement = doc.DocumentElement
        Dim nodes As XmlNodeList = root.ChildNodes
        Dim prod_list As New ArrayList

        Dim j As Integer

        For j = 0 To nodes.Count - 1
            Dim productLine As TechBulletin.ProductLines = New TechBulletin.ProductLines
            productLine.productLineNo = nodes(j)("PROD_LINE_NUMBER").InnerText
            productLine.productLineDesc = nodes(j)("PROD_LINE_DESC").InnerText
            prod_list.Add(productLine)
        Next

        Dim ProdLines(prod_list.Count - 1) As TechBulletin.ProductLines
        prod_list.CopyTo(ProdLines)

        Return ProdLines
    End Function

    Public Function GetKnowledgeBaseSolution(ByVal SolutionID As String) As DataSet
        Dim cmd As OracleCommand = New OracleCommand

        cmd.CommandText = "KNOWLEDGEBASEMANAGER.GetKnowledgeBaseSolution"
        cmd.Parameters.Add("xSolutionID", OracleDbType.Varchar2, 500)
        cmd.Parameters("xSolutionID").Value = SolutionID

        cmd.Parameters.Add("xSolution", OracleDbType.RefCursor)
        cmd.Parameters.Item("xSolution").Direction = ParameterDirection.Output

        Return GetResults(cmd, True)
    End Function

    Public Function SaveFeedBack(ByRef TechBulletinObject As TechBulletin, ByVal FeedBack As Short, ByVal FeedBackID As Long, ByVal TransactionType As Short, ByVal Suggestion As String, ByVal xCUSTOMERID As Integer _
            , ByVal xCUSTOMERSEQUENCENUMBER As Integer, ByVal xHTTP_X_FORWARDED_FOR As String, ByVal xREMOTE_ADDR As String, ByVal xHTTP_REFERER As String _
            , ByVal xHTTP_URL As String, ByVal xHTTP_USER_AGENT As String) As Long
        Dim cmd As OracleCommand = New OracleCommand

        cmd.CommandText = "KNOWLEDGEBASEMANAGER.SaveKBFeedBack"
        cmd.Parameters.Add("xTYPE", OracleDbType.Int16)
        cmd.Parameters("xTYPE").Value = TechBulletinObject.Type

        cmd.Parameters.Add("xID", OracleDbType.Varchar2, 10)
        cmd.Parameters.Item("xID").Value = TechBulletinObject.BulletinNo

        cmd.Parameters.Add("xSUBJECTPROBLEM", OracleDbType.Varchar2, 4000)
        cmd.Parameters.Item("xSUBJECTPROBLEM").Value = TechBulletinObject.Subject
        cmd.Parameters.Add("xHELPFUL", OracleDbType.Int16)
        cmd.Parameters.Item("xHELPFUL").Value = FeedBack

        cmd.Parameters.Add("xFeedBackID", OracleDbType.Int32)
        cmd.Parameters.Item("xFeedBackID").Value = FeedBackID

        cmd.Parameters.Add("xTransactionType", OracleDbType.Int16)
        cmd.Parameters.Item("xTransactionType").Value = TransactionType

        cmd.Parameters.Add("xSuggestion", OracleDbType.Varchar2, 2000)
        cmd.Parameters.Item("xSuggestion").Value = Suggestion


        cmd.Parameters.Add("xCUSTOMERID", OracleDbType.Int32)
        cmd.Parameters("xCUSTOMERID").Value = xCUSTOMERID

        cmd.Parameters.Add("xCUSTOMERSEQUENCENUMBER", OracleDbType.Int32)
        cmd.Parameters("xCUSTOMERSEQUENCENUMBER").Value = xCUSTOMERSEQUENCENUMBER

        cmd.Parameters.Add("xHTTP_X_FORWARDED_FOR", OracleDbType.Varchar2)
        cmd.Parameters("xHTTP_X_FORWARDED_FOR").Value = xHTTP_X_FORWARDED_FOR

        cmd.Parameters.Add("xREMOTE_ADDR", OracleDbType.Varchar2)
        cmd.Parameters("xREMOTE_ADDR").Value = xREMOTE_ADDR

        cmd.Parameters.Add("xHTTP_REFERER", OracleDbType.Varchar2)
        cmd.Parameters("xHTTP_REFERER").Value = xHTTP_REFERER

        cmd.Parameters.Add("xHTTP_URL", OracleDbType.Varchar2)
        cmd.Parameters("xHTTP_URL").Value = xHTTP_URL

        cmd.Parameters.Add("xHTTP_USER_AGENT", OracleDbType.Varchar2)
        cmd.Parameters("xHTTP_USER_AGENT").Value = xHTTP_USER_AGENT

        Dim outparam As New OracleParameter("retVal", OracleDbType.Int32)
        outparam.Direction = ParameterDirection.Output
        cmd.Parameters.Add(outparam)

        Try
            ExecuteCMD(cmd, Nothing, True)
            If Not CType(cmd.Parameters("retVal")?.Value, INullable).IsNull Then
                Return Convert.ToInt64(cmd.Parameters("retVal").Value.ToString())
            Else
                Return 0
            End If
        Catch ex As Exception
            'Throw ex
            Throw New ServicesPLUSOracleException(ERRORCODE.ora06550, ex)
        End Try
    End Function

    Public Function LogKBSearchResult(ByVal TBINCLUDED As String, ByVal ID As String, ByVal Model As String, ByVal SerialNumber As Integer, ByVal Keyword As String, ByVal DateFrom As Date,
                                      ByVal DateTo As Date, ByVal Hits As Integer, ByVal xCUSTOMERID As Integer, ByVal xCUSTOMERSEQUENCENUMBER As Integer, ByVal xHTTP_X_FORWARDED_FOR As String,
                                      ByVal xREMOTE_ADDR As String, ByVal xHTTP_REFERER As String, ByVal xHTTP_URL As String, ByVal xHTTP_USER_AGENT As String) As Boolean
        Dim cmd As OracleCommand = New OracleCommand

        cmd.CommandText = "KNOWLEDGEBASEMANAGER.LogKBSearchResult"

        cmd.Parameters.Add("xTBINCLUDED", OracleDbType.Varchar2, 3)
        cmd.Parameters("xTBINCLUDED").Value = TBINCLUDED

        cmd.Parameters.Add("xID", OracleDbType.Varchar2, 10)
        cmd.Parameters.Item("xID").Value = ID

        cmd.Parameters.Add("xModel", OracleDbType.Varchar2, 50)
        cmd.Parameters.Item("xModel").Value = Model

        cmd.Parameters.Add("xSerialNumber", OracleDbType.Int32)
        cmd.Parameters.Item("xSerialNumber").Value = SerialNumber

        cmd.Parameters.Add("xSubject", OracleDbType.Varchar2, 100)
        cmd.Parameters.Item("xSubject").Value = Keyword

        cmd.Parameters.Add("xDateFrom", OracleDbType.Date)
        cmd.Parameters.Item("xDateFrom").Value = DateFrom

        cmd.Parameters.Add("xDateTo", OracleDbType.Date)
        cmd.Parameters.Item("xDateTo").Value = DateTo

        cmd.Parameters.Add("xHits", OracleDbType.Varchar2, 10)
        cmd.Parameters.Item("xHits").Value = Hits

        cmd.Parameters.Add("xCUSTOMERID", OracleDbType.Int32)
        cmd.Parameters("xCUSTOMERID").Value = xCUSTOMERID

        cmd.Parameters.Add("xCUSTOMERSEQUENCENUMBER", OracleDbType.Int32)
        cmd.Parameters("xCUSTOMERSEQUENCENUMBER").Value = xCUSTOMERSEQUENCENUMBER

        cmd.Parameters.Add("xHTTP_X_FORWARDED_FOR", OracleDbType.Varchar2)
        cmd.Parameters("xHTTP_X_FORWARDED_FOR").Value = xHTTP_X_FORWARDED_FOR

        cmd.Parameters.Add("xREMOTE_ADDR", OracleDbType.Varchar2)
        cmd.Parameters("xREMOTE_ADDR").Value = xREMOTE_ADDR

        cmd.Parameters.Add("xHTTP_REFERER", OracleDbType.Varchar2)
        cmd.Parameters("xHTTP_REFERER").Value = xHTTP_REFERER

        cmd.Parameters.Add("xHTTP_URL", OracleDbType.Varchar2)
        cmd.Parameters("xHTTP_URL").Value = xHTTP_URL

        cmd.Parameters.Add("xHTTP_USER_AGENT", OracleDbType.Varchar2)
        cmd.Parameters("xHTTP_USER_AGENT").Value = xHTTP_USER_AGENT

        Try
            ExecuteCMD(cmd, Nothing, True)
            Return True
        Catch ex As Exception
            ' Throw ex
            Throw New ServicesPLUSOracleException(ERRORCODE.ora06550, ex)
        End Try
    End Function

    Public Function GetTechBulletin(ByVal searchCriterion As String) As TechBulletin()
        Try
            Dim doc As XmlDocument
            Dim cmd As New OracleCommand

            cmd.CommandText = "getTechBulletins"
            cmd.Parameters.Add("xSearchCondition", OracleDbType.Varchar2, 500)
            cmd.Parameters("xSearchCondition").Value = searchCriterion

            doc = GetResults(cmd)

            Dim root As XmlElement = doc.DocumentElement
            Dim nodes As XmlNodeList = root.ChildNodes
            Dim elem As XmlElement
            Dim tech_list As New ArrayList

            Dim j As Integer

            For j = 0 To nodes.Count - 1
                Dim TechBulletins As TechBulletin = New TechBulletin

                elem = nodes(j)("BULLETIN_NUMBER")
                If Not elem Is Nothing Then
                    TechBulletins.BulletinNo = nodes(j)("BULLETIN_NUMBER").InnerText
                End If

                elem = nodes(j)("MODEL")
                If Not elem Is Nothing Then
                    TechBulletins.Model = nodes(j)("MODEL").InnerText
                End If

                elem = nodes(j)("SUBJECT")
                If Not elem Is Nothing Then
                    TechBulletins.Subject = nodes(j)("SUBJECT").InnerText
                End If

                elem = nodes(j)("PRINTDATE")
                If Not elem Is Nothing Then
                    TechBulletins.PrintDate = nodes(j)("PRINTDATE").InnerText
                End If

                elem = nodes(j)("STARTOFRANGE")
                If Not elem Is Nothing Then
                    TechBulletins.StartRange = nodes(j)("STARTOFRANGE").InnerText
                End If

                elem = nodes(j)("ENDOFRANGE")
                If Not elem Is Nothing Then
                    TechBulletins.EndRange = nodes(j)("ENDOFRANGE").InnerText
                End If
                tech_list.Add(TechBulletins)
            Next

            Dim TechBulletinlist(tech_list.Count - 1) As TechBulletin
            tech_list.CopyTo(TechBulletinlist)

            Return TechBulletinlist
        Catch ex As Exception
            ' Throw New Exception("Unable to retrieve data")
            Throw New ServicesPLUSOracleException(ERRORCODE.ora06550, ex)
        End Try
    End Function

    Public Function GetTechBulletinCount(ByVal searchCriterion As String, ByVal searchSerialNo As String) As Long
        Dim doc As XmlDocument
        Dim cmd As New OracleCommand
        Dim nodes As XmlNodeList

        Try
            cmd.CommandText = "getTechBulletins"
            cmd.Parameters.Add("xSearchCondition", OracleDbType.Varchar2, 800)
            cmd.Parameters("xSearchCondition").Value = searchCriterion
            cmd.Parameters.Add("xSearchSerialNo", OracleDbType.Varchar2, 10)
            cmd.Parameters("xSearchSerialNo").Value = searchSerialNo.ToLower()

            doc = GetResults(cmd)
            nodes = doc.DocumentElement.ChildNodes

            If nodes(0)("TOTALCOUNT") IsNot Nothing Then
                Return nodes(0)("TOTALCOUNT").InnerText
            End If

            Return 0
        Catch ex As Exception
            Utilities.LogDebug($"ERROR in TechBulletinDataManager.GetTechBulletinCount - Params: searchCriterion = {searchCriterion}, searchSerialNo = {searchSerialNo}.")
            Throw ' New ServicesPLUSOracleException(ERRORCODE.ora06550, ex) -- This was erasing the original error message, making troubleshooting frustrating.
        End Try
    End Function

    Public Function GetTechBulletinByRow(ByVal searchCriterion As String, ByVal minRow As Long, ByVal maxRow As Long) As TechBulletin()
        Try
            Dim doc As XmlDocument
            Dim cmd As New OracleCommand

            cmd.CommandText = "getTechBulletinByRows"
            cmd.Parameters.Add("xSearchCondition", OracleDbType.Varchar2, 500)
            cmd.Parameters("xSearchCondition").Value = searchCriterion
            cmd.Parameters.Add("minRow", OracleDbType.Int32)
            cmd.Parameters("minRow").Value = minRow
            cmd.Parameters.Add("maxRow", OracleDbType.Int32)
            cmd.Parameters("maxRow").Value = maxRow

            doc = GetResults(cmd)

            Dim root As XmlElement = doc.DocumentElement
            Dim nodes As XmlNodeList = root.ChildNodes
            Dim elem As XmlElement
            Dim tech_list As New ArrayList

            Dim j As Integer

            For j = 0 To nodes.Count - 1
                Dim TechBulletins As TechBulletin = New TechBulletin

                elem = nodes(j)("TYPE")
                If Not elem Is Nothing Then
                    TechBulletins.Type = Convert.ToInt16(nodes(j)("TYPE").InnerText.ToString())
                End If

                elem = nodes(j)("BULLETIN_NUMBER")
                If Not elem Is Nothing Then
                    TechBulletins.BulletinNo = nodes(j)("BULLETIN_NUMBER").InnerText
                End If

                elem = nodes(j)("MODEL")
                If Not elem Is Nothing Then
                    TechBulletins.Model = nodes(j)("MODEL").InnerText
                End If

                elem = nodes(j)("SUBJECT")
                If Not elem Is Nothing Then
                    TechBulletins.Subject = nodes(j)("SUBJECT").InnerText
                End If

                elem = nodes(j)("PRINTDATE")
                If Not elem Is Nothing Then
                    TechBulletins.PrintDate = nodes(j)("PRINTDATE").InnerText
                End If

                elem = nodes(j)("STARTOFRANGE")
                If Not elem Is Nothing Then
                    TechBulletins.StartRange = nodes(j)("STARTOFRANGE").InnerText
                End If

                elem = nodes(j)("ENDOFRANGE")
                If Not elem Is Nothing Then
                    TechBulletins.EndRange = nodes(j)("ENDOFRANGE").InnerText
                End If
                'Added By Sneha on 11/11/13
                elem = nodes(j)("OLD_MODEL")
                If Not elem Is Nothing Then
                    TechBulletins.OldModel = nodes(j)("OLD_MODEL").InnerText
                End If
                tech_list.Add(TechBulletins)
            Next

            Dim TechBulletinlist(tech_list.Count - 1) As TechBulletin
            tech_list.CopyTo(TechBulletinlist)

            Return TechBulletinlist
        Catch ex As Exception
            ' Throw New Exception("Unable to retrieve data")
            Throw New ServicesPLUSOracleException(ERRORCODE.ora06550, ex)
        End Try
    End Function

    'Added By Sneha on 31/10/2013
    'Summary/Description: 
    '   -Procedure called: getnewmodels which takes model id as input parameter
    '   -returns the conflicted model ids
    Public Function getConflictedModels(ByVal model_number As String) As List(Of String)
        Dim lstConflictedNodes As List(Of String)
        Dim cmd As New OracleCommand With {
            .CommandType = CommandType.StoredProcedure,
            .CommandText = "getnewmodels",
            .BindByName = True
        }
        cmd.Parameters.Add("p_modelid", OracleDbType.Varchar2, 20)
        cmd.Parameters("p_modelid").Value = model_number

        Dim outparam1 As New OracleParameter("rc_confmodels", OracleDbType.RefCursor)
        outparam1.Direction = ParameterDirection.Output
        cmd.Parameters.Add(outparam1)
        If dbConnection.State = ConnectionState.Closed Then
            dbConnection.Open()
            cmd.Connection = dbConnection
        End If

        Dim myAdaptor As New OracleDataAdapter(cmd)
        Dim dataset As New DataSet
        Dim xml_data As String

        Try
            myAdaptor.Fill(dataset)
            xml_data = dataset.GetXml()
        Catch ex As Exception
            Throw New ServicesPLUSOracleException(ERRORCODE.ora06550, ex)
        End Try
        dbConnection.Close()
        Dim doc As New XmlDocument
        Try
            doc.LoadXml(xml_data)

            Dim root As XmlElement = doc.DocumentElement
            Dim nodes As XmlNodeList = root.ChildNodes
            Dim i, j As Integer
            lstConflictedNodes = New List(Of String)

            For i = 0 To nodes.Count - 1
                Dim table_node As XmlElement = nodes(i)
                For j = 0 To table_node.GetElementsByTagName("OLD_MATERIAL").Count - 1
                    If table_node.Item("OLD_MATERIAL") IsNot Nothing Then
                        lstConflictedNodes.Add(table_node.Item("OLD_MATERIAL").InnerText + ":" + table_node.Item("NEW_MATERIAL").InnerText)
                    End If
                Next
            Next
            Return lstConflictedNodes
        Catch ex As Exception
            Throw New ServicesPLUSOracleException(ERRORCODE.ora06550, ex)
        End Try
    End Function
End Class
