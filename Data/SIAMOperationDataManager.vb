﻿Imports System.Xml
Imports Sony.US.ServicesPLUS.Core
Imports Oracle.ManagedDataAccess.Client
Imports ServicesPlusException
Imports Oracle.ManagedDataAccess.Types

Public Class SIAMOperationDataManager
    Inherits DataManager

    Public Function GetServiceCenterList() As DataSet
        Dim dsServiceCenterList As DataSet
        Dim cmd As OracleCommand = New OracleCommand
        cmd.CommandText = "SIAMOPERATION.GetServiceCenterList"

        Dim outparam As New OracleParameter("xServiceCenterList", OracleDbType.RefCursor)
        outparam.Direction = ParameterDirection.Output
        cmd.Parameters.Add(outparam)
        dsServiceCenterList = GetResults(cmd, True)
        Return dsServiceCenterList
    End Function

    Public Function GET_PR_SEC_ServiceCenterMapping() As DataSet
        Dim dsServiceCenterMAPPING As DataSet
        Dim cmd As OracleCommand = New OracleCommand
        cmd.CommandText = "SIAMOPERATION.GET_PR_SEC_SERVICECENTEMAPPING"

        Dim outparam As New OracleParameter("xServiceCenterList", OracleDbType.RefCursor)
        outparam.Direction = ParameterDirection.Output
        cmd.Parameters.Add(outparam)
        dsServiceCenterMAPPING = GetResults(cmd, True)
        Return dsServiceCenterMAPPING
    End Function
    Public Function GETShippingMethods() As DataSet
        Dim dsShippingMethods As DataSet
        Dim cmd As OracleCommand = New OracleCommand
        cmd.CommandText = "SIAMOPERATION.GETSHIPPINGMETHODS"

        Dim outparam As New OracleParameter("xShippingMethods", OracleDbType.RefCursor)
        outparam.Direction = ParameterDirection.Output
        cmd.Parameters.Add(outparam)
        dsShippingMethods = GetResults(cmd, True)
        Return dsShippingMethods
    End Function
    Public Function GETCertificateNumbers() As DataSet
        Dim dsCertificateNumbers As DataSet
        Dim cmd As OracleCommand = New OracleCommand
        cmd.CommandText = "SIAMOPERATION.GETCertificateNumbers"

        Dim outparam As New OracleParameter("xCertificateNumbers", OracleDbType.RefCursor)
        outparam.Direction = ParameterDirection.Output
        cmd.Parameters.Add(outparam)
        dsCertificateNumbers = GetResults(cmd, True)
        Return dsCertificateNumbers
    End Function
    Public Function GETServiceAgreementModel(ByRef objGlobalData As GlobalData) As DataSet
        Dim dsServiceAgreementModel As DataSet
        Dim cmd As OracleCommand = New OracleCommand
        cmd.CommandText = "SIAMOPERATION.GETServiceAgreementModel"

        cmd.Parameters.Add("xSALESORG", OracleDbType.Varchar2)
        cmd.Parameters("xSALESORG").Value = objGlobalData.SalesOrganization

        cmd.Parameters.Add("xLANGUAGEID", OracleDbType.Varchar2)
        cmd.Parameters("xLANGUAGEID").Value = objGlobalData.Language

        Dim outparam As New OracleParameter("xServiceAgreementModel", OracleDbType.RefCursor)
        outparam.Direction = ParameterDirection.Output
        cmd.Parameters.Add(outparam)
        dsServiceAgreementModel = GetResults(cmd, True)
        Return dsServiceAgreementModel
    End Function
    Public Function GETAllServiceAgreementModel() As DataSet
        Dim dsServiceAgreementModel As DataSet
        Dim cmd As OracleCommand = New OracleCommand
        cmd.CommandText = "SIAMOPERATION.GETServiceAgrmentModelALL"

        Dim outparam As New OracleParameter("xServiceAgrmentModelALL", OracleDbType.RefCursor)
        outparam.Direction = ParameterDirection.Output
        cmd.Parameters.Add(outparam)
        dsServiceAgreementModel = GetResults(cmd, True)
        Return dsServiceAgreementModel
    End Function

    Public Function GETPROCESSNAME() As DataSet
        Dim dsPROCESSNAME As DataSet
        Dim cmd As OracleCommand = New OracleCommand
        cmd.CommandText = "SIAMOPERATION.GETPROCESSNAME"

        Dim outparam As New OracleParameter("xPROCESSNAME", OracleDbType.RefCursor)
        outparam.Direction = ParameterDirection.Output
        cmd.Parameters.Add(outparam)
        dsPROCESSNAME = GetResults(cmd, True)
        Return dsPROCESSNAME
    End Function
    Public Function GETPROCESSLOG(ByVal PROCESSNAME As String, ByVal PROCESSDATE As String) As DataSet
        Dim dsPROCESSLOG As DataSet
        Dim cmd As OracleCommand = New OracleCommand
        cmd.CommandText = "SIAMOPERATION.GETPROCESSLOG"

        cmd.Parameters.Add("xPROCESSNAME", OracleDbType.Varchar2, 100)
        cmd.Parameters("xPROCESSNAME").Value = PROCESSNAME
        cmd.Parameters.Add("xPROCESSDATE", OracleDbType.Varchar2, 10)
        cmd.Parameters("xPROCESSDATE").Value = PROCESSDATE

        Dim outparam As New OracleParameter("xPROCESSLOG", OracleDbType.RefCursor)
        outparam.Direction = ParameterDirection.Output
        cmd.Parameters.Add(outparam)
        dsPROCESSLOG = GetResults(cmd, True)
        Return dsPROCESSLOG
    End Function
    Public Function GETPROCESSORERROR(ByVal PROCESSERRORID As Integer) As DataSet
        Dim dsPROCESSLOG As DataSet
        Dim cmd As OracleCommand = New OracleCommand
        cmd.CommandText = "SIAMOPERATION.GETPROCESSERRORFORID"

        cmd.Parameters.Add("xPROCESSERRORID", OracleDbType.Varchar2, 100)
        cmd.Parameters("xPROCESSERRORID").Value = PROCESSERRORID

        Dim outparam As New OracleParameter("xPROCESSLOG", OracleDbType.RefCursor)
        outparam.Direction = ParameterDirection.Output
        cmd.Parameters.Add(outparam)
        dsPROCESSLOG = GetResults(cmd, True)
        Return dsPROCESSLOG
    End Function
    Public Function StoreServiceCenterMapping(ByVal PrimaryServiceCenterID As Integer, ByVal SecondaryServiceCenterID As Integer, ByVal Action As Short, ByRef Contaxt As TransactionContext) As Integer
        Dim cmd As OracleCommand = New OracleCommand

        cmd.CommandText = "STORESERVICECENTER"

        cmd.CommandType = CommandType.StoredProcedure

        cmd.Parameters.Add("xPrimaryID", OracleDbType.Int32)
        cmd.Parameters("xPrimaryID").Value = PrimaryServiceCenterID

        cmd.Parameters.Add("xSecondaryID", OracleDbType.Int32)
        cmd.Parameters("xSecondaryID").Value = SecondaryServiceCenterID

        cmd.Parameters.Add("xActionID", OracleDbType.Int32)
        cmd.Parameters("xActionID").Value = Action

        Try
            Return ExecuteCMD(cmd, Contaxt)
        Catch ex As Exception
            'Throw ex
            Throw New ServicesPLUSOracleException(ERRORCODE.ora06550, ex)
        End Try
    End Function
    Public Function StoreShippingMethod(ByVal SHIPPINGMETHODID As Integer, ByVal STATUSCODE As Integer, ByVal DISPLAYORDER As Integer, ByVal SISBOCARRIERCODE As String, ByVal TRACKINGURL As String, ByVal SISBOPICKGROUPCODE As String, ByVal BACKORDEROVERRIDE As Integer, ByVal DESCRIPTION As String, ByVal SISCARRIERCODE As String, ByVal SISPICKGROUPCODE As String, ByVal PRSVCARRIERCODE As String, ByVal SALESORG As String, ByVal LANGUAGEID As String, ByVal Action As Short, ByRef Contaxt As TransactionContext) As Integer
        Dim cmd As OracleCommand = New OracleCommand
        cmd.CommandText = "STORESHIPPINGMETHOD"

        cmd.CommandType = CommandType.StoredProcedure

        cmd.Parameters.Add("xSHIPPINGMETHODID", OracleDbType.Int32)
        cmd.Parameters("xSHIPPINGMETHODID").Value = SHIPPINGMETHODID

        cmd.Parameters.Add("xSTATUSCODE", OracleDbType.Int32)
        cmd.Parameters("xSTATUSCODE").Value = STATUSCODE

        cmd.Parameters.Add("xDISPLAYORDER", OracleDbType.Int32)
        cmd.Parameters("xDISPLAYORDER").Value = DISPLAYORDER

        cmd.Parameters.Add("xBACKORDEROVERRIDE", OracleDbType.Int32)
        cmd.Parameters("xBACKORDEROVERRIDE").Value = BACKORDEROVERRIDE

        cmd.Parameters.Add("xSISBOPICKGROUPCODE", OracleDbType.Varchar2)
        cmd.Parameters("xSISBOPICKGROUPCODE").Value = SISBOPICKGROUPCODE

        cmd.Parameters.Add("xSISBOCARRIERCODE", OracleDbType.Varchar2)
        cmd.Parameters("xSISBOCARRIERCODE").Value = SISBOCARRIERCODE

        cmd.Parameters.Add("xTRACKINGURL", OracleDbType.Varchar2)
        cmd.Parameters("xTRACKINGURL").Value = TRACKINGURL

        cmd.Parameters.Add("xDESCRIPTION", OracleDbType.Varchar2)
        cmd.Parameters("xDESCRIPTION").Value = DESCRIPTION

        cmd.Parameters.Add("xSISCARRIERCODE", OracleDbType.Varchar2)
        cmd.Parameters("xSISCARRIERCODE").Value = SISCARRIERCODE

        cmd.Parameters.Add("xSISPICKGROUPCODE", OracleDbType.Varchar2)
        cmd.Parameters("xSISPICKGROUPCODE").Value = SISPICKGROUPCODE

        cmd.Parameters.Add("xPRSVCARRIERCODE", OracleDbType.Varchar2)
        cmd.Parameters("xPRSVCARRIERCODE").Value = PRSVCARRIERCODE

        cmd.Parameters.Add("xActionID", OracleDbType.Int32)
        cmd.Parameters("xActionID").Value = Action

        cmd.Parameters.Add("xSalesOrg", OracleDbType.Varchar2)
        cmd.Parameters("xSalesOrg").Value = SALESORG

        cmd.Parameters.Add("xLanguageId", OracleDbType.Varchar2)
        cmd.Parameters("xLanguageId").Value = LANGUAGEID

        Try
            Return ExecuteCMD(cmd, Contaxt)
        Catch ex As Exception
            'Throw ex
            Throw New ServicesPLUSOracleException(ERRORCODE.ora06550, ex)
        End Try
    End Function
    Public Sub InsertCertificateNumbers(ByVal strProductCode As String, ByVal StarCertNumber As Integer, ByVal EndCertNumber As Integer, ByRef Contaxt As TransactionContext)
        Dim cmd As OracleCommand = New OracleCommand
        cmd.CommandText = "InsertCertificateNumbers"

        cmd.CommandType = CommandType.StoredProcedure

        cmd.Parameters.Add("xProductCode", OracleDbType.Varchar2)
        cmd.Parameters("xProductCode").Value = strProductCode

        cmd.Parameters.Add("xStarCertNumber", OracleDbType.Int32)
        cmd.Parameters("xStarCertNumber").Value = StarCertNumber

        cmd.Parameters.Add("xEndCertNumber", OracleDbType.Int32)
        cmd.Parameters("xEndCertNumber").Value = EndCertNumber
        Try
            ExecuteCMD(cmd, Contaxt)
        Catch ex As Exception
            ' Throw ex
            Throw New ServicesPLUSOracleException(ERRORCODE.ora06550, ex)
        End Try
    End Sub
    Public Sub UpdateCertificateNumbers(ByVal strProductCode As String, ByVal StarCertNumber As Integer, ByVal EndCertNumber As Integer, ByRef Contaxt As TransactionContext)
        Dim cmd As OracleCommand = New OracleCommand
        cmd.CommandText = "UpdateCertificateNumbers"

        cmd.CommandType = CommandType.StoredProcedure

        cmd.Parameters.Add("xProductCode", OracleDbType.Varchar2)
        cmd.Parameters("xProductCode").Value = strProductCode

        cmd.Parameters.Add("xStarCertNumber", OracleDbType.Int32)
        cmd.Parameters("xStarCertNumber").Value = StarCertNumber

        cmd.Parameters.Add("xEndCertNumber", OracleDbType.Int32)
        cmd.Parameters("xEndCertNumber").Value = EndCertNumber
        Try
            ExecuteCMD(cmd, Contaxt)
        Catch ex As Exception
            ' Throw ex
            Throw New ServicesPLUSOracleException(ERRORCODE.ora06550, ex)
        End Try
    End Sub
    Public Sub DeleteCertificateNumbers(ByVal strProductCode As String, ByRef Contaxt As TransactionContext)
        Dim cmd As OracleCommand = New OracleCommand
        cmd.CommandText = "DeleteCertificateNumbers"

        cmd.CommandType = CommandType.StoredProcedure

        cmd.Parameters.Add("xProductCode", OracleDbType.Varchar2)
        cmd.Parameters("xProductCode").Value = strProductCode

        Try
            ExecuteCMD(cmd, Contaxt)
        Catch ex As Exception
            'Throw ex
            Throw New ServicesPLUSOracleException(ERRORCODE.ora06550, ex)
        End Try
    End Sub



    Public Sub InsertServiceAgreementModel(ByVal ProductCatagory As String, ByVal ProductGroup As String, ByVal ProductCode As String, ByVal ModelNumber As String, ByRef Contaxt As TransactionContext, ByRef objGlobalData As GlobalData)
        Dim cmd As OracleCommand = New OracleCommand
        cmd.CommandText = "InsertServiceAgreementModel"

        cmd.CommandType = CommandType.StoredProcedure

        cmd.Parameters.Add("xProductCat", OracleDbType.Varchar2)
        cmd.Parameters("xProductCat").Value = ProductCatagory

        cmd.Parameters.Add("xPRODUCTGROUP", OracleDbType.Varchar2)
        cmd.Parameters("xPRODUCTGROUP").Value = ProductGroup

        cmd.Parameters.Add("xProductCode", OracleDbType.Varchar2)
        cmd.Parameters("xProductCode").Value = ProductCode

        cmd.Parameters.Add("xModel", OracleDbType.Varchar2)
        cmd.Parameters("xModel").Value = ModelNumber

        cmd.Parameters.Add("xSalesOrg", OracleDbType.Varchar2)
        cmd.Parameters("xSalesOrg").Value = objGlobalData.SalesOrganization

        cmd.Parameters.Add("xLanguageId", OracleDbType.Varchar2)
        cmd.Parameters("xLanguageId").Value = objGlobalData.Language
        Try
            ExecuteCMD(cmd, Contaxt)
        Catch ex As Exception
            ' Throw ex
            Throw New ServicesPLUSOracleException(ERRORCODE.ora06550, ex)
        End Try
    End Sub
    Public Sub UpdateServiceAgreementModel(ByVal ID As String, ByVal ProductCatagory As String, ByVal ProductGroup As String, ByVal ProductCode As String, ByVal ModelNumber As String, ByRef Contaxt As TransactionContext, ByRef objGlobalData As GlobalData)
        Dim cmd As OracleCommand = New OracleCommand
        cmd.CommandText = "UpdateServiceAgreementModel"

        cmd.CommandType = CommandType.StoredProcedure

        cmd.Parameters.Add("xID", OracleDbType.Int32)
        cmd.Parameters("xID").Value = Convert.ToInt32(ID)


        cmd.Parameters.Add("xProductCat", OracleDbType.Varchar2)
        cmd.Parameters("xProductCat").Value = ProductCatagory

        cmd.Parameters.Add("xPRODUCTGROUP", OracleDbType.Varchar2)
        cmd.Parameters("xPRODUCTGROUP").Value = ProductGroup

        cmd.Parameters.Add("xProductCode", OracleDbType.Varchar2)
        cmd.Parameters("xProductCode").Value = ProductCode

        cmd.Parameters.Add("xModel", OracleDbType.Varchar2)
        cmd.Parameters("xModel").Value = ModelNumber

        cmd.Parameters.Add("xSalesOrg", OracleDbType.Varchar2)
        cmd.Parameters("xSalesOrg").Value = objGlobalData.SalesOrganization

        cmd.Parameters.Add("xLanguageId", OracleDbType.Varchar2)
        cmd.Parameters("xLanguageId").Value = objGlobalData.Language

        Try
            ExecuteCMD(cmd, Contaxt)
        Catch ex As Exception
            ' Throw ex
            Throw New ServicesPLUSOracleException(ERRORCODE.ora06550, ex)
        End Try
    End Sub
    Public Sub DeleteServiceAgreementModel(ByVal ID As String, ByRef Contaxt As TransactionContext)
        Dim cmd As OracleCommand = New OracleCommand
        cmd.CommandText = "DeleteServiceAgreementModel"

        cmd.CommandType = CommandType.StoredProcedure

        cmd.Parameters.Add("xID", OracleDbType.Varchar2)
        cmd.Parameters("xID").Value = Convert.ToInt32(ID)

        Try
            ExecuteCMD(cmd, Contaxt)
        Catch ex As Exception
            'Throw ex
            Throw New ServicesPLUSOracleException(ERRORCODE.ora06550, ex)
        End Try
    End Sub


    Public Function checkProductCode(ByVal strProductCode As String) As Boolean
        Dim cmd As OracleCommand = New OracleCommand
        cmd.CommandText = "checkProductCode"

        cmd.CommandType = CommandType.StoredProcedure

        cmd.Parameters.Add("xProductCode", OracleDbType.Varchar2)
        cmd.Parameters("xProductCode").Value = strProductCode

        Dim outparam2 As New OracleParameter("xret", OracleDbType.Varchar2, 10)
        outparam2.Direction = ParameterDirection.Output
        cmd.Parameters.Add(outparam2)
        checkProductCode(cmd)
        Dim strTemp As String
        strTemp = cmd.Parameters("xret")?.Value?.ToString()

        If strTemp = "Duplicate" Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Sub checkProductCode(ByRef command As OracleCommand)
        command.CommandType = CommandType.StoredProcedure
        command.BindByName = True
        Try
            OpenConnection()
            command.Connection = dbConnection
            command.ExecuteNonQuery()
        Catch ex As Exception
            Throw New ServicesPLUSOracleException(ERRORCODE.ora06550, ex)
        Finally
            CloseConnection()
        End Try
    End Sub

    Public Function GetConflictedModel(ByVal ModelNumber As String, ByRef objGlobalData As GlobalData) As String
        Try
            Dim cmd As OracleCommand = New OracleCommand
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandText = "getnewmaterial"
            cmd.Parameters.Add("p_oldmaterial", OracleDbType.Varchar2, 20)
            cmd.Parameters("p_oldmaterial").Value = ModelNumber

            cmd.Parameters.Add("P_Sales_Org", OracleDbType.Varchar2, 4)
            cmd.Parameters("P_Sales_Org").Value = objGlobalData.SalesOrganization

            Dim outparam2 As New OracleParameter("p_newmaterial", OracleDbType.Varchar2, 20)
            outparam2.Direction = ParameterDirection.Output
            cmd.Parameters.Add(outparam2)

            ExecuteCMD(cmd, Nothing)
            If Not CType(cmd.Parameters("p_newmaterial")?.Value, INullable).IsNull Then
                Return cmd.Parameters("p_newmaterial").Value
            Else
                Return Nothing
            End If
        Catch ex As Exception
            Throw ex
        End Try

    End Function
End Class
