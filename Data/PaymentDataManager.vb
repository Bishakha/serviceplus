Imports System.Xml
Imports Sony.US.ServicesPLUS.Core
Imports Oracle.ManagedDataAccess.Client
Imports Sony.US.SIAMUtilities
Imports ServicesPlusException
Imports Oracle.ManagedDataAccess.Types

Public Class PaymentDataManager
    Inherits DataManager

    Public Overrides Function Restore(ByVal identity As String) As Object
        Throw New NotImplementedException()     ' ASleight - Must be implemented, but not all classes need it.
    End Function

    Public Overrides Sub Store(ByRef data As Object, ByRef context As TransactionContext, Optional ByVal update As Boolean = False)
        Dim card As CreditCard = CType(data, CreditCard)
        Dim cmd As New OracleCommand

        If (Not update) Or (update And card.Dirty) Then
            If update Then
                If card.Action = CoreObject.ActionType.DELETE Then
                    cmd.CommandText = "deleteCreditCard"
                Else
                    cmd.CommandText = "UPDATEANDKEEPCREDITCARD" 'keep the credit card data instead of add new one for invoiceprocessor or backorder release
                End If
                cmd.Parameters.Add("xSequenceNumber", OracleDbType.Int32)
                cmd.Parameters("xSequenceNumber").Value = card.SequenceNumber
            Else
                cmd.CommandText = "addCreditCard"
            End If

            cmd.Parameters.Add("xCustomerID", OracleDbType.Int32)
            cmd.Parameters("xCustomerID").Value = card.Customer.CustomerID

            ' Paymetric will Tokenize any credit card numbers when they are entered. We never know or store actual numbers.
            cmd.Parameters.Add("xCreditCardNumber", OracleDbType.Varchar2, 50)
            cmd.Parameters("xCreditCardNumber").Value = card.CreditCardNumber

            cmd.Parameters.Add("xCreditCardTypeID", OracleDbType.Int32)
            cmd.Parameters("xCreditCardTypeID").Value = card.Type.CreditCardTypeCode
            cmd.Parameters.Add("xExpirationDate", OracleDbType.Date)
            cmd.Parameters("xExpirationDate").Value = card.ExpirationDate   ' Convert.ToDateTime(card.ExpirationDate, New Globalization.CultureInfo("en-US"))

            cmd.Parameters.Add("xCSC", OracleDbType.Varchar2, 50)
            cmd.Parameters("xCSC").Value = card.CSCCode

            cmd.Parameters.Add("xNickName", OracleDbType.Varchar2, 50)
            cmd.Parameters("xNickName").Value = card.NickName

            'Utilities.LogDebug("PaymentDM.Store - Proc: " & cmd.CommandText & ", Customer: " & cmd.Parameters("xCustomerID").Value & _
            '   ", CC Num: " & card.CreditCardNumber & ", CC Expiry: " & cmd.Parameters("xExpirationDate").Value)

            cmd.Parameters.Add("retVal", OracleDbType.Int32, ParameterDirection.Output)
            ExecuteCMD(cmd, context)

            card.Action = CoreObject.ActionType.RESTORE

            If Not CType(cmd.Parameters("retVal")?.Value, INullable).IsNull Then
                card.SequenceNumber = cmd.Parameters("retVal").Value.ToString()
            End If
        End If
    End Sub

    Public Sub HideCreditCard(ByVal creditcard As CreditCard, ByRef context As TransactionContext)
        Dim cmd As New OracleCommand("updatecreditcardhideflag")
        cmd.Parameters.Add("xSequenceNumber", OracleDbType.Int32, creditcard.SequenceNumber, ParameterDirection.Input)
        Try
            ExecuteCMD(cmd, context)
        Catch ex As Exception
            Throw New ServicesPLUSOracleException(ERRORCODE.ora06550, ex)
        End Try
    End Sub

    Public Function GetCreditCardTypes() As CreditCardType()
        Dim doc As XmlDocument
        Dim credit_card_type As CreditCardType
        Dim card_type_list As New ArrayList
        Dim cmd As New OracleCommand("getCreditCardTypes")

        doc = GetResults(cmd)

        Dim nodes As XmlNodeList = doc.DocumentElement.ChildNodes

        For Each node As XmlNode In nodes
            credit_card_type = New CreditCardType With {
                .CreditCardTypeCode = If(node("CREDITCARDTYPEID")?.InnerText, ""),
                .Description = If(node("DESCRIPTION")?.InnerText, ""),
                .CardType = If(node("CARDTYPE")?.InnerText, "")
            }
            card_type_list.Add(credit_card_type)
        Next

        Dim card_types(card_type_list.Count - 1) As CreditCardType
        card_type_list.CopyTo(card_types)
        Return card_types
    End Function

    Public Function GetCustomerCreditCard(ByVal ccSequenceNumber As Integer) As CreditCard
        Dim doc As XmlDocument
        Dim credit_card As CreditCard = Nothing
        Dim cmd As New OracleCommand("getCustomerCreditCard")
        Dim debugMessage As String = "PaymentDataManager.GetCustomerCreditCard - Card Sequence: " & ccSequenceNumber

        cmd.Parameters.Add("xSequenceNumber", OracleDbType.Char, ccSequenceNumber, ParameterDirection.Input)
        Try
            doc = GetResults(cmd)
        Catch ex As ApplicationException
            Return Nothing
        Catch exSys As Exception
            Throw New ServicesPLUSOracleException(ERRORCODE.ora06550, exSys)
        End Try

        'internalize
        Dim nodes As XmlNodeList = doc.DocumentElement.ChildNodes
        'Dim j As Integer

        For Each node As XmlNode In nodes
            Dim type As New CreditCardType
            type.CreditCardTypeCode = node("CREDITCARDTYPEID").InnerText

            credit_card = New CreditCard(type) With {
                .CreditCardNumber = Encryption.DeCrypt(node("CREDITCARDNUMBER").InnerText, Nothing, Nothing),
                .SequenceNumber = If(node("SEQUENCENUMBER")?.InnerText, ""),
                .CSCCode = If(node("CSC")?.InnerText, ""),
                .NickName = If(node("NICKNAME")?.InnerText, ""),
                .ExpirationDate = If(node("EXPIRATIONDATE")?.InnerText, ""),
                .HideCardFlag = (node("HIDECARDFLAG")?.InnerText = "Y"),
                .Action = CoreObject.ActionType.RESTORE
            }
            debugMessage &= $", Card(Nickname: {credit_card.NickName}, Sequence: {credit_card.SequenceNumber}, Raw Expiry: {node("EXPIRATIONDATE").InnerText})"
            Exit For
        Next
        Utilities.LogDebug(debugMessage)

        Return credit_card
    End Function

    Public Function GetCustomerCreditCards(ByVal customer As Customer) As CreditCard()
        Dim doc As XmlDocument
        Dim nodes As XmlNodeList
        Dim credit_card As CreditCard
        Dim card_list As New ArrayList
        Dim cmd As New OracleCommand("getCustomerCreditCards")
        'Dim debugMessage As String = "PaymentDataManager.GetCustomerCreditCards - User: " & customer.CustomerID

        cmd.Parameters.Add("xUserId", OracleDbType.Char)
        cmd.Parameters("xUserId").Value = customer.CustomerID

        doc = GetResults(cmd)
        nodes = doc.DocumentElement.ChildNodes

        For Each node In nodes
            Dim type As New CreditCardType With {
                .CreditCardTypeCode = node("CREDITCARDTYPEID")?.InnerText,
                .Description = node("DESCRIPTION")?.InnerText,
                .CardType = node("CARDTYPE")?.InnerText
            }

            credit_card = New CreditCard(type) With {
                .CreditCardNumber = node("CREDITCARDNUMBER")?.InnerText,
                .SequenceNumber = node("SEQUENCENUMBER")?.InnerText,
                .CSCCode = node("CSC")?.InnerText,
                .NickName = node("NICKNAME")?.InnerText,
                .ExpirationDate = node("EXPIRATIONDATE")?.InnerText,
                .HideCardFlag = (node("HIDECARDFLAG")?.InnerText = "Y"),
                .Customer = customer,
                .Action = Account.ActionType.RESTORE
            }
            'debugMessage &= $", Card(Nickname: {credit_card.NickName}, Sequence: {credit_card.SequenceNumber}, Raw Expiry: {node("EXPIRATIONDATE").InnerText})"
            card_list.Add(credit_card)
        Next
        'Utilities.LogDebug(debugMessage)

        Dim cards(card_list.Count - 1) As CreditCard
        card_list.CopyTo(cards)
        Return cards
    End Function

    Public Function IsTaxExempt(ByVal SIAMIdentity As String, ByVal State As String) As Boolean
        Dim rc As Boolean = False
        Dim doc As XmlDocument
        Dim nodes As XmlNodeList
        Dim cmd As New OracleCommand("getTaxExemptStatus")

        cmd.Parameters.Add("xCustomerSIAMId", OracleDbType.Varchar2, 20, SIAMIdentity, ParameterDirection.Input)
        cmd.Parameters.Add("xShipToState", OracleDbType.Varchar2, 5, State, ParameterDirection.Input)

        doc = GetResults(cmd)
        nodes = doc.DocumentElement.ChildNodes

        Return (nodes.Count > 0)
    End Function
End Class
