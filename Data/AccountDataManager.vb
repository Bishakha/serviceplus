
Imports System.Xml
Imports Sony.US.ServicesPLUS.Core
Imports Oracle.ManagedDataAccess.Client
Imports Oracle.ManagedDataAccess.Types

Public Class AccountDataManager
    Inherits DataManager

    Public Overrides Function Restore(ByVal identity As String) As Object
        Throw New NotImplementedException()
    End Function

    Public Overrides Sub Store(ByRef data As Object, ByRef context As TransactionContext, Optional ByVal update As Boolean = False)
        Dim debugString As String = $"AccountDataManager.Store - START at {Date.Now.ToLongTimeString}{Environment.NewLine}"
        Try     ' FOR DEBUG ONLY
            Dim acct As Account = CType(data, Account)
            Dim cmd As New OracleCommand
            debugString &= $"- Update: {update}, acct.Dirty: {acct.Dirty}"

            If (Not update) Or (update And acct.Dirty) Then
                If update Then
                    If acct.Action = CoreObject.ActionType.DELETE Then
                        cmd.CommandText = "deleteAccount"
                    Else
                        cmd.CommandText = "updateAccount"
                    End If
                    cmd.Parameters.Add("xSequenceNumber", OracleDbType.Int32)
                    cmd.Parameters("xSequenceNumber").Value = acct.SequenceNumber
                Else
                    cmd.CommandText = "addAccount"
                End If
                debugString &= $", Command Text: {cmd.CommandText}.{Environment.NewLine}"
                debugString &= $"- Customer Detail: ({acct.Customer.ToString()}).{Environment.NewLine}"

                cmd.Parameters.Add("xCustomerID", OracleDbType.Int32)
                cmd.Parameters("xCustomerID").Value = Integer.Parse(If(acct.Customer?.CustomerID, "0"))
                cmd.Parameters.Add("xAccountNumber", OracleDbType.Varchar2, 50)
                cmd.Parameters("xAccountNumber").Value = acct.AccountNumber
                debugString &= $"- Parameters Added. Is Customer Null? {acct.Customer Is Nothing}, CustomerID: {acct.Customer?.CustomerID}, Account: {acct.AccountNumber}.{Environment.NewLine}"
                'cmd.Parameters.Add("Validated", OracleDbType.Int32) 'commented for 6865
                'cmd.Parameters("Validated").Value = acct.Validated 'commented for 6865
                'cmd.Parameters.Add("AccountTypeID", OracleDbType.Int32)
                'cmd.Parameters("AccountTypeID").Value = acct.AccountType

                Dim outparam As New OracleParameter("retVal", OracleDbType.Decimal, ParameterDirection.Output)
                cmd.Parameters.Add(outparam)
                ExecuteCMD(cmd, context)

                acct.Action = CoreObject.ActionType.RESTORE
                If Not CType(cmd.Parameters("retVal").Value, INullable).IsNull Then
                    acct.SequenceNumber = cmd.Parameters("retVal").Value.ToString()
                    debugString &= $"- Return Value (Sequence Number): {acct.SequenceNumber}."
                End If
            End If
            'ServicesPlusException.Utilities.LogDebug(debugString)
        Catch ex As Exception
            ServicesPlusException.Utilities.LogMessages(debugString)
            Throw 'ex
        End Try
    End Sub

    Public Sub DeleteCustomerAccount(ByVal customerID As Integer, ByRef accountNumber As String, ByRef context As TransactionContext)
        Dim cmd As New OracleCommand("deleteAccount")

        cmd.Parameters.Add("xCustomerID", OracleDbType.Int32)
        cmd.Parameters("xCustomerID").Value = customerID
        cmd.Parameters.Add("xAccountNumber", OracleDbType.Varchar2, 100)
        cmd.Parameters("xAccountNumber").Value = accountNumber

        ExecuteCMD(cmd, context)
    End Sub

    Public Function GetCustomerAccounts(ByRef customer As Customer) As Account()
        Dim doc As XmlDocument
        Dim cmd As New OracleCommand("getCustomerAccounts")

        cmd.Parameters.Add("xUserId", OracleDbType.Char)
        cmd.Parameters("xUserId").Value = customer.CustomerID

        doc = GetResults(cmd)

        'internalize
        Dim nodes As XmlNodeList = doc.DocumentElement.ChildNodes
        Dim account As Account
        Dim account_list As New ArrayList
        'Dim account_type As String
        Dim j As Integer

        For j = 0 To nodes.Count - 1
            account = New Account With {
                .AccountNumber = nodes(j)("ACCOUNTNUMBER").InnerText.Trim(),
                .SequenceNumber = nodes(j)("SEQUENCENUMBER").InnerText,
                .Customer = customer,
                .Action = CoreObject.ActionType.RESTORE
            }
            account_list.Add(account)
        Next

        Dim accounts(account_list.Count - 1) As Account
        account_list.CopyTo(accounts)

        Return accounts
    End Function

    Public Function GetCustomerAccountByNumber(ByRef accountNumber As String) As Account
        Dim doc As XmlDocument
        Dim cmd As New OracleCommand("getCustomerAccountByNumber")
        cmd.Parameters.Add("xAccntNumber", OracleDbType.Varchar2)
        cmd.Parameters("xAccntNumber").Value = accountNumber

        doc = GetResults(cmd)

        'internalize
        Dim nodes As XmlNodeList = doc.DocumentElement.ChildNodes
        Dim account As Account = Nothing
        Dim j As Integer

        For j = 0 To nodes.Count - 1
            account = New Account With {
                .AccountNumber = nodes(j)("ACCOUNTNUMBER").InnerText.Trim(),
                .SequenceNumber = nodes(j)("SEQUENCENUMBER").InnerText,
                .Action = CoreObject.ActionType.RESTORE
            }
            Return account
        Next

        Return account

    End Function

End Class
