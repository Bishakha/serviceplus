Imports System.Xml
Imports Sony.US.ServicesPLUS.Core
Imports Oracle.ManagedDataAccess.Client
Imports Sony.US.SIAMUtilities
Imports ServicesPlusException
Imports Oracle.ManagedDataAccess.Types

Public Class LoginDataManager
    Protected cn As OracleConnection
    Protected connString As String
    Protected connection_string As String

    Public Sub New()
        cn = New OracleConnection
        'Dim config_settings As Hashtable
        'Dim handler As New ConfigHandler("SOFTWARE\SIAM")
        'config_settings = handler.GetConfigSettings("//item[@name='CurrentDAL']")
        'connString = config_settings.Item("ConnectionString").ToString()
        connString = ConfigurationData.Environment.DataBase.ConnectionString
        cn.ConnectionString = connString
    End Sub

    Public Sub CloseConnection()
        If cn.State = ConnectionState.Open Then
            cn.Close()
        End If
    End Sub

    Public Sub ExecuteCMD(ByRef command As OracleCommand, ByRef context As TransactionContext, Optional ByVal auto_close_connection As Boolean = True)
        command.CommandType = CommandType.StoredProcedure
        command.BindByName = True

        'if no transactional context then manage the connection here with no transaction.
        If context Is Nothing Then
            If cn.State = ConnectionState.Closed Then
                cn.Open()
                command.Connection = cn
            End If
        Else
            command.Connection = context.NativeConnection
            command.Transaction = context.NativeTransaction
        End If

        'Dim x As Double

        Try
            command.ExecuteNonQuery()
        Catch ex As Exception
            'log
            Throw New ServicesPLUSOracleException(ERRORCODE.ora06550, ex)
        End Try

        If context Is Nothing And auto_close_connection Then
            cn.Close()
        End If

    End Sub

    Public Function UpdateLoginTime(ByVal userID As String) As Boolean
        'ByVal mUserId As String, ByVal mEventOrgin As String, ByVal mEventType As String, ByVal mIdentity As String, ByVal mStatus As String
        Dim cmd As New OracleCommand
        cmd.CommandText = "UpdateLoginTime_SIAM" 'while merging SIAM and ServicesPLUS this procedure has been changed, will make any necessery change as per requirement

        cmd.Parameters.Add("xUserId", OracleDbType.Varchar2, 20)
        cmd.Parameters("xUserId").Value = userID

        Dim outparam As New OracleParameter("xIsFirstLogin", OracleDbType.Int32)
        outparam.Direction = ParameterDirection.Output

        cmd.Parameters.Add(outparam)

        ExecuteCMD(cmd, Nothing)

        If CType(cmd.Parameters("xIsFirstLogin")?.Value, INullable).IsNull OrElse cmd.Parameters("xIsFirstLogin").Value > 0 Then
            'If cmd.Parameters("xIsFirstLogin").Value Is DBNull.Value Or cmd.Parameters("xIsFirstLogin").Value > 0 Then
            Return True
        End If
        Return False
    End Function

    Public Sub SetFirstPasswordChangedFlag(ByVal userID As String)
        Dim cmd As New OracleCommand
        cmd.CommandText = "SetFirstPasswordChangedFlag"

        cmd.Parameters.Add("xUserId", OracleDbType.Varchar2, 20)
        cmd.Parameters("xUserId").Value = userID

        ExecuteCMD(cmd, Nothing)
    End Sub
End Class
