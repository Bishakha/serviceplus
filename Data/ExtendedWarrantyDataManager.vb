﻿Imports System.Xml
Imports System.Collections
Imports Oracle.ManagedDataAccess.Client
Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.SIAMUtilities
Imports ServicesPlusException
Imports Oracle.ManagedDataAccess.Types

Public Class ExtendedWarrantyDataManager
    Inherits DataManager

    Dim ewmodel As DataSet

    Public Function GetEWCertData(ByVal CertGUID As String) As DataSet
        Dim cmd As New OracleCommand("ExtendedWarranty.GetCertificateReportData")

        cmd.Parameters.Add("xGUID", OracleDbType.Varchar2, 36)
        cmd.Parameters("xGUID").Value = CertGUID

        cmd.Parameters.Add(New OracleParameter("xEWCertificateDate", OracleDbType.RefCursor, ParameterDirection.Output))

        Try
            ewmodel = GetEWResults(cmd, True)
        Catch ex As Exception
            Throw New ServicesPLUSOracleException(ERRORCODE.ora06550, ex)
        End Try

        Return ewmodel
    End Function

    Public Function GetEWModels(ByVal model_number As String) As DataSet
        Dim cmd As New OracleCommand("ExtendedWarranty.getewmodels")

        cmd.Parameters.Add("ew_modelid", OracleDbType.Varchar2, 20)
        cmd.Parameters("ew_modelid").Value = model_number

        cmd.Parameters.Add(New OracleParameter("rc_ewmodels", OracleDbType.RefCursor, ParameterDirection.Output))

        Try
            ewmodel = GetEWResults(cmd, True)
        Catch ex As Exception
            Throw New ServicesPLUSOracleException(ERRORCODE.ora06550, ex)
        End Try

        Return ewmodel
    End Function

    Public Function GetewmodelsbyItemNoOrDesc(ByVal ew_ItemNumber As String, ByVal ew_Desc As String) As DataSet
        Dim cmd As New OracleCommand("ExtendedWarranty.getewmodelsbyItemNoOrDesc")

        cmd.Parameters.Add("ew_itemnumber", OracleDbType.Varchar2, 20)
        cmd.Parameters("ew_itemnumber").Value = ew_ItemNumber
        cmd.Parameters.Add("ew_desc", OracleDbType.Varchar2, 200)
        cmd.Parameters("ew_desc").Value = ew_Desc

        cmd.Parameters.Add(New OracleParameter("rc_ewmodels", OracleDbType.RefCursor, ParameterDirection.Output))

        Try
            ewmodel = GetEWResults(cmd, True)
        Catch ex As Exception
            Throw New ServicesPLUSOracleException(ERRORCODE.ora06550, ex)
        End Try

        Return ewmodel
    End Function

    Public Function LoadEWProductDetails(ByVal ProductCode As String, ByVal EWProductCode As String) As DataSet
        Dim cmd As New OracleCommand("ExtendedWarranty.LoadEWProductDetails")

        cmd.Parameters.Add("ew_ProductCode", OracleDbType.Varchar2, 20)
        cmd.Parameters("ew_ProductCode").Value = ProductCode
        cmd.Parameters.Add("ew_EWProductCode", OracleDbType.Varchar2, 200)
        cmd.Parameters("ew_EWProductCode").Value = EWProductCode

        cmd.Parameters.Add(New OracleParameter("rc_ewmodels", OracleDbType.RefCursor, ParameterDirection.Output))

        Try
            ewmodel = GetEWResults(cmd, True)
        Catch ex As Exception
            Throw New ServicesPLUSOracleException(ERRORCODE.ora06550, ex)
        End Try

        Return ewmodel
    End Function

    Public Function GETEWFORCART(ByVal ChildProductCode As String, ByVal ParentProductCode As String) As ExtendedWarrantyModel
        Dim myAdaptor As OracleDataAdapter
        Dim warranty As ExtendedWarrantyModel = Nothing
        Dim dataset As New DataSet
        Dim cmd As New OracleCommand("ExtendedWarranty.GETEWFORCART") With {
            .CommandType = CommandType.StoredProcedure,
            .BindByName = True
        }

        cmd.Parameters.Add("xChildProductCode", OracleDbType.Varchar2, 20)
        cmd.Parameters("xChildProductCode").Value = ChildProductCode
        cmd.Parameters.Add("xParentProductCode", OracleDbType.Varchar2, 200)
        cmd.Parameters("xParentProductCode").Value = ParentProductCode

        cmd.Parameters.Add(New OracleParameter("xEWCursor", OracleDbType.RefCursor, ParameterDirection.Output))

        OpenConnection()
        cmd.Connection = dbConnection

        myAdaptor = New OracleDataAdapter(cmd)
        Try
            myAdaptor.Fill(dataset)
        Catch ex As Exception
            Throw ex
        End Try
        CloseConnection()
        Try
            If dataset.Tables?(0)?.Rows?.Count > 0 Then
                For Each objDataRow As DataRow In dataset.Tables(0).Rows
                    warranty = New ExtendedWarrantyModel(Product.ProductType.ExtendedWarranty) With {
                        .ModelNumber = objDataRow("Model_ID").ToString(),
                        .PartNumber = objDataRow("Part_Number").ToString(),
                        .Description = objDataRow("MODEL_DESCRIPTION").ToString(),
                        .FreeOfCharge = Convert.ToInt16(If(objDataRow("FREE_OF_CHARGE")?.ToString(), "0")),
                        .ListPrice = Convert.ToDouble(If(objDataRow("LISTPRICE")?.ToString(), "0.0"))
                    }
                    Exit For
                Next
            End If
        Catch ex As Exception
            Throw New ServicesPLUSOracleException(ERRORCODE.ora06550, ex)
        End Try
        Return warranty
    End Function

    Public Function GetEWResults(ByRef command As OracleCommand, ByVal auto_close_connection As Boolean) As DataSet
        command.CommandType = CommandType.StoredProcedure
        command.BindByName = True
        Dim da As New OracleDataAdapter(command)
        Dim ds As New DataSet

        Try
            OpenConnection()
            command.Connection = dbConnection
            da.Fill(ds)
            If auto_close_connection Then CloseConnection()
        Catch ex As Exception
            Throw New ServicesPLUSOracleException(ERRORCODE.ora06550, ex)
        End Try

        Return ds
    End Function

    Public Function CheckCertificateNumber(ByVal CertificateNumber As String) As Integer
        Dim return_value As Decimal
        Dim doc As New XmlDocument
        Dim error_node As XmlNode
        Dim cmd As New OracleCommand("EXTENDEDWARRANTY.CHECKCERTIFICATENUMBERS")

        cmd.Parameters.Add("xCertificateNumber", OracleDbType.Varchar2)
        cmd.Parameters("xCertificateNumber").Value = CertificateNumber

        cmd.Parameters.Add(New OracleParameter("retVal", OracleDbType.Int32, ParameterDirection.Output))
        ExecuteCMD(cmd, Nothing, False)

        If CType(cmd.Parameters("retVal")?.Value, INullable).IsNull Then
            If Not CType(cmd.Parameters("errorCondition")?.Value, INullable).IsNull Then
                'error condition was set in procedure
                Throw New Exception(cmd.Parameters("errorCondition").Value)
            End If
            Throw New ServicesPLUSOracleException(ERRORCODE.ora06550)
        Else
            return_value = Convert.ToDouble(cmd.Parameters("retVal").Value.ToString())
            CloseConnection()
            error_node = doc.SelectSingleNode("//ERROR")
            If error_node IsNot Nothing Then Throw New ServicesPLUSOracleException(ERRORCODE.ora06550)
        End If

        Return return_value
    End Function
End Class
