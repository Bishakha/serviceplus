﻿using System;
using System.IO;
using System.Messaging;
using System.Text;
using System.Threading;
using System.Timers;
using System.Web;
using System.Xml.Serialization;
using Sony.US.SIAMUtilities;

namespace Sony.US.AuditLog {

    public class PCILogger : IXmlSerializable {
        public PCILogger() {

        }

        public string GetXML() {
            var sb = new StringBuilder();
            sb.Append($"<Log>{Environment.NewLine}");
            sb.Append($"  <UserIdentification>{Environment.NewLine}");
            sb.Append($"    <Customer_EmailAddress>{EmailAddress}</Customer_EmailAddress>{Environment.NewLine}");
            sb.Append($"    <CustomerID>{CustomerID}</CustomerID>{Environment.NewLine}");
            sb.Append($"    <CustomerID_SequenceNumber>{CustomerID_SequenceNumber}</CustomerID_SequenceNumber>{Environment.NewLine}");
            sb.Append($"    <SIAM_ID>{SIAM_ID}</SIAM_ID>{Environment.NewLine}");
            sb.Append($"    <LDAP_ID>{LDAP_ID}</LDAP_ID>{Environment.NewLine}");
            sb.Append($"    <HTTP_X_Forward_For>{HTTPRequestObjectValues.HTTP_X_Forward_For}</HTTP_X_Forward_For>{Environment.NewLine}");
            sb.Append($"    <RemoteADDR>{HTTPRequestObjectValues.RemoteADDR}</RemoteADDR>{Environment.NewLine}");
            sb.Append($"    <HTTPUserAgent>{HTTPRequestObjectValues.HTTPUserAgent}</HTTPUserAgent>{Environment.NewLine}");
            sb.Append($"  </UserIdentification>{Environment.NewLine}");

            sb.Append($"  <EventType>{EventType.ToString()}</EventType>{Environment.NewLine}");
            sb.Append($"  <EventDateTime>{Convert.ToDateTime(EventDateTime).ToString("MM/dd/yyyy hh:mm:ss tt")}</EventDateTime>{Environment.NewLine}");
            sb.Append($"  <IndicationSuccessFailure>{IndicationSuccessFailure.ToString()}</IndicationSuccessFailure>{Environment.NewLine}");

            sb.Append($"  <OriginationOfEvent>{Environment.NewLine}");
            sb.Append($"    <EventOriginMachineName>{EventOriginServerMachineName}</EventOriginMachineName>{Environment.NewLine}");
            sb.Append($"    <OperationalUser>{OperationalUser}</OperationalUser>{Environment.NewLine}");
            sb.Append($"    <HTTPReferer>{HTTPRequestObjectValues.HTTPReferer}</HTTPReferer>{Environment.NewLine}");
            sb.Append($"    <HTTPURL>{HTTPRequestObjectValues.HTTPURL}</HTTPURL>{Environment.NewLine}");
            sb.Append($"    <EventOriginApplicationName>{EventOriginApplication}</EventOriginApplicationName>{Environment.NewLine}");
            sb.Append($"    <EventOriginApplicationLocation>{EventOriginApplicationLocation}</EventOriginApplicationLocation>{Environment.NewLine}");
            sb.Append($"    <EventOriginMethod>{EventOriginMethod}</EventOriginMethod>{Environment.NewLine}");
            sb.Append($"  </OriginationOfEvent>{Environment.NewLine}");

            sb.Append($"  <Affected>{Environment.NewLine}");
            sb.Append($"    <CreditCardMaskedNumber>{CreditCardMaskedNumber}</CreditCardMaskedNumber>{Environment.NewLine}");
            sb.Append($"    <CreditCardSequenceNumber>{CreditCardSequenceNumber}</CreditCardSequenceNumber>{Environment.NewLine}");
            if (!string.IsNullOrWhiteSpace(DataIdentity))
                sb.Append($"    <DataIdentity>{DataIdentity}</DataIdentity>{Environment.NewLine}");
            sb.Append($"    <EventIdentity>{EventIdentity}</EventIdentity>{Environment.NewLine}");
            sb.Append($"  </Affected>{Environment.NewLine}");

            sb.Append($"  <Message>{Message}</Message>{Environment.NewLine}");
            sb.Append($"</Log>{Environment.NewLine}");

            return sb.ToString();
        }

        public void ReadXML(string Log) {
            //Load Log info from XML string
        }

        public void PushLogToMSMQ() {
            //Write Message to MSMQ in XML format
            //Added by praveen for the Bug ID 2126 on 21 Dec
            try {
                if (ConfigurationData.GeneralSettings.LoggingStatus == "ON") {
                    //Added by Sunil on 07-01-10 for bug 2145 (Logging Exception handling)
                    var ts = new TimeSpan(0, 0, 0, 0, 0);
                    ts = System.DateTime.Now - ConfigurationData.GeneralSettings.PCILoggerLastRunTime;
                    if (ts.TotalMilliseconds > (ConfigurationData.GeneralSettings.PCILogger.Interval + 60000)) {
                        ConfigurationData.GeneralSettings.PCILogWriterRunning = false;

                        var objPCILogWriter = new PCILogWriter();
                        var newThread = new Thread(objPCILogWriter.StartLogging);
                        newThread.Start();
                        ConfigurationData.GeneralSettings.PCILoggerLastRunTime = DateTime.Now;
                    }

                    var objManageMSMQMessageObject = new ManageMSMQMessageObject();
                    MessageQueue theQueue = objManageMSMQMessageObject.GetMessageObject(ConfigurationData.GeneralSettings.PCILogger.MSMQName);
                    theQueue.Send(GetXML(), ConfigurationData.GeneralSettings.PCILogger.MSMQLabel);

                }
            } catch (Exception ex) {
                try {
                    var objMailObject = new ServicesPLUSMailObject {
                        Body = $"{ex.Message}{Environment.NewLine}{ex.StackTrace}{Environment.NewLine}{ex.Source}",
                        To = ConfigurationData.Environment.Email.General.ServicesPLUS_Mail,
                        From = ConfigurationData.Environment.Email.General.ServicesPLUS_Mail,
                        Subject = "MSMQ runtime error pushing message in queue : " + ConfigurationData.GeneralSettings.Environment
                    };
                    ServicesPLUSMail.SendNetSMTPMail(objMailObject);
                } catch (Exception) {
                    //Do not handle it.
                }
            }
            //Log this to database LogAuditToDB()
        }

        private void LogAuditToDB() {
            //Rick ask not to log it into database
        }

        #region "  PROPERTIES  "
        public EventType EventType { get; set; }
        public HTTPRequestObject HTTPRequestObjectValues { get; set; } = new HTTPRequestObject();
        public Int16 TransactionType { get; set; } = -1;
        public Int32 CreditCardSequenceNumber { get; set; } = 0;
        public Int32 CustomerID_SequenceNumber { get; set; } = 0;
        public string CreditCardMaskedNumber { get; set; } = string.Empty;
        public string CustomerID { get; set; } = string.Empty;
        public string DataIdentity { get; set; } = string.Empty;
        public string EmailAddress { get; set; } = string.Empty;
        public string EventDateTime { get; set; } = DateTime.Now.ToLongDateString();
        public string EventIdentity { get; set; } = string.Empty;
        public string EventOriginApplication { get; set; } = string.Empty;
        public string EventOriginApplicationLocation { get; set; } = string.Empty;
        public string EventOriginMethod { get; set; } = string.Empty;
        public string EventOriginServerMachineName { get; set; } = string.Empty;
        public string IndicationSuccessFailure { get; set; } = string.Empty;
        public string LDAP_ID { get; set; } = string.Empty;
        public string Message { get; set; } = string.Empty;
        public string OperationalUser { get; set; } = string.Empty;
        public string SIAM_ID { get; set; } = string.Empty;
        public string TransactionNumber { get; set; } = string.Empty;
        #endregion

        public HTTPRequestObject GetRequestContextValue() {
            var objHTTPRequestObject = new HTTPRequestObject {
                HTTP_X_Forward_For = GetServerVariableValue("HTTP_X_FORWARDED_FOR"),
                RemoteADDR = GetServerVariableValue("REMOTE_ADDR"),
                HTTPReferer = GetServerVariableValue("HTTP_REFERER"),
                HTTPURL = GetServerVariableValue("URL"),
                HTTPUserAgent = GetServerVariableValue("HTTP_USER_AGENT")
            };
            return objHTTPRequestObject;
        }

        private string GetServerVariableValue(string Key) {
            // 2016-07-19 ASleight - There is no need to loop through all the variables to get at a single one.
            /* int loop1 = 0;
			int loop2 = 0;
			string[] arr1 = null;
			string[] arr2 = null;
			NameValueCollection coll = default(NameValueCollection);
			string value = string.Empty;
			// Load ServerVariable collection into NameValueCollection object.
			coll = HttpContext.Current.Request.ServerVariables;
			// Get names of all keys into a string array.
			arr1 = coll.AllKeys;
			for (loop1 = 0; loop1 <= arr1.GetUpperBound(0); loop1++)
			{
				if (Key == arr1[loop1])
				{
					arr2 = coll.GetValues(loop1);
					// Get all values under this key.
					for (loop2 = 0; loop2 <= arr2.GetUpperBound(0); loop2++)
					{
						value = HttpContext.Current.Server.HtmlEncode(arr2[loop2]);
						return value;
					}
				}
			}
			return value; */
            if (HttpContext.Current.Request.ServerVariables[Key] != null)
                return HttpContext.Current.Request.ServerVariables[Key];
            else
                return String.Empty;
        }

        #region IXmlSerializable Members

        public System.Xml.Schema.XmlSchema GetSchema() {
            throw new NotImplementedException();
        }

        public void ReadXml(System.Xml.XmlReader reader) {
            throw new NotImplementedException();
        }

        public void WriteXml(System.Xml.XmlWriter writer) {
            throw new NotImplementedException();
        }

        #endregion


    }

    public class HTTPRequestObject {
        public string HTTP_X_Forward_For { get; set; } = string.Empty;
        public string HTTPReferer { get; set; } = string.Empty;
        public string HTTPURL { get; set; } = string.Empty;
        public string HTTPUserAgent { get; set; } = string.Empty;
        public string RemoteADDR { get; set; } = string.Empty;
        public string TransactionType { get; set; } = string.Empty;
    }

    public enum EventType {
        Add = 1,
        Update = 2,
        Delete = 3,
        View = 4,
        Login = 5,
        Encryption = 6,
        SiteStatusChange = 7,
        Others = 8,
        UserApproval = 9,// Added New Event Type Others
        //Add By praveen kumar N.B
        RevocationProcess = 10,
        LoggerStatusChange = 11
    }

    public class PCILogWriter {
        private System.Timers.Timer objTimer = new System.Timers.Timer(1000);

        public PCILogWriter() {

        }

        public void StartLogging() {
            objTimer.Elapsed += new ElapsedEventHandler(objTimer_Elapsed);
            objTimer.Disposed += new EventHandler(objTimer_Disposed);
            objTimer.Interval = ConfigurationData.GeneralSettings.PCILogger.Interval;
            objTimer.Start();
        }

        void objTimer_Disposed(object sender, EventArgs e) {
            //Log Stop Logging event
            objTimer.Stop();
        }

        void objTimer_Elapsed(object sender, ElapsedEventArgs e) {
            //*********************************************************
            //*********************************************************
            //Deleting temporary EW Certificate PDF files
            try {
                DirectoryInfo di;
                di = new DirectoryInfo(ConfigurationData.GeneralSettings.EWTempPDFFolder);
                FileInfo[] aryFi;
                aryFi = di.GetFiles("*.pdf");
                if (aryFi != null) {
                    foreach (FileInfo fi in aryFi) {
                        if (fi.LastWriteTime.Date.Add(new TimeSpan(0, 5, 0)) < DateTime.Now) {
                            fi.Delete();
                        }
                    }

                }
            } catch {
            }
            //*********************************************************
            //*********************************************************
            //*********************************************************


            //Added by Sunil on 07-01-10 for bug 2145 (Logging Exception handling)
            ConfigurationData.GeneralSettings.PCILoggerLastRunTime = System.DateTime.Now;

            if (ConfigurationData.GeneralSettings.PCILogWriterRunning) return;
            //Read MSMQ and log message into a specific text format
            var objManageMSMQMessageObject = new ManageMSMQMessageObject();
            MessageQueue theQueue = objManageMSMQMessageObject.GetMessageObject(ConfigurationData.GeneralSettings.PCILogger.MSMQName);
            if (theQueue != null) {
                theQueue.MessageReadPropertyFilter.SetAll();
                MessageEnumerator myEnumerator = theQueue.GetMessageEnumerator2();

                Message message;
                SonyLogMessage logMessage;
                DateTime dayNow = DateTime.Now;

                bool blnFileCreated = false;
                bool blnIsFileOpen = false;
                TextWriter tw = null;
                string fileName = string.Empty;
                // Move to the next message
                try {
                    while (myEnumerator.MoveNext()) {
                        ConfigurationData.GeneralSettings.PCILogWriterRunning = true;

                        message = myEnumerator.Current;

                        logMessage = new SonyLogMessage(message.Label, message.SentTime.ToString("G"), message.Priority.ToString(), message.Body.ToString());

                        //Append this message to the xml file
                        if (blnFileCreated == false) {
                            fileName = CreateFile();
                            blnFileCreated = true;
                            tw = new StreamWriter(fileName, true);
                            tw.WriteLine("<SPSPCILog>");
                            tw.Close();

                        }
                        if (!blnIsFileOpen) {
                            tw = new StreamWriter(fileName, true);
                            blnIsFileOpen = true;
                            tw.WriteLine(message.Body.ToString());
                            tw.Close();
                        } else {
                            tw = new StreamWriter(fileName, true);
                            tw.WriteLine(message.Body.ToString());
                            tw.Close();
                        }
                        myEnumerator.RemoveCurrent();
                        myEnumerator.Reset();
                        continue;
                    }
                    if (blnIsFileOpen) {
                        tw = new StreamWriter(fileName, true);
                        tw.WriteLine("</SPSPCILog>");
                        tw.Close();
                    }
                } catch (Exception ex) {
                    try {
                        var objMailObject = new ServicesPLUSMailObject {
                            Body = $"{ex.Message}{Environment.NewLine}{ex.StackTrace}{Environment.NewLine}{ex.Source}",
                            To = ConfigurationData.Environment.Email.General.ServicesPLUS_Mail,
                            From = ConfigurationData.Environment.Email.General.ServicesPLUS_Mail,
                            Subject = "MSMQ runtime error creating .XML file : " + ConfigurationData.GeneralSettings.Environment
                        };
                        ServicesPLUSMail.SendNetSMTPMail(objMailObject);
                    } catch (Exception) {
                        //Do not handle it.
                    }
                } finally {
                    ConfigurationData.GeneralSettings.PCILogWriterRunning = false;
                }

            } else {
                try {
                    var objMailObject = new ServicesPLUSMailObject {
                        Body = $"MSMQ : {ConfigurationData.GeneralSettings.PCILogger.MSMQName} does not exist",
                        To = ConfigurationData.Environment.Email.General.ServicesPLUS_Mail,
                        From = ConfigurationData.Environment.Email.General.ServicesPLUS_Mail,
                        Subject = "MSMQ runtime error creating .XML file : " + ConfigurationData.GeneralSettings.Environment
                    };
                    ServicesPLUSMail.SendNetSMTPMail(objMailObject);
                } catch (Exception) {
                    //Do not handle it.
                }
            }
        }

        private string CreateFile() {
            string FileName = GUID.GetSystemGUID().Replace("-", "").Replace("{", "").Replace("}", "");
            string LogDirectory = ConfigurationData.Environment.PCILogger.ApplicationLogsLocation;
            if (Directory.Exists(LogDirectory) == false) {
                DirectoryInfo dInfo = Directory.CreateDirectory(LogDirectory);
            }
            return LogDirectory + @"\" + FileName + ".xml";
        }

    }

    public class ManageMSMQMessageObject {
        public MessageQueue GetMessageObject(string MSMQName) {
            MessageQueue theQueue;
            try {
                if (!IsQueuePresent(MSMQName)) {
                    theQueue = CreateQueue(MSMQName);
                } else {
                    theQueue = new MessageQueue(MSMQName);
                }
                theQueue.Formatter = new ActiveXMessageFormatter();
            } catch {
                theQueue = null;
            }
            return theQueue;
        }

        private MessageQueue CreateQueue(string QueueName) {
            try {
                return MessageQueue.Create(QueueName, false);
            } catch (Exception ex) {
                throw ex;
            }
        }

        private bool IsQueuePresent(string QueueName) {
            try {
                return MessageQueue.Exists(QueueName);
            } catch (Exception ex) {
                throw ex;
            }
        }
    }
}
