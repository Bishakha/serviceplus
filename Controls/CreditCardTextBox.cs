using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI.WebControls;
using System.Drawing;
using System.Web.UI;
using System.ComponentModel;
namespace Sony.US.ServicesPLUS.Controls
{
    public class CreditCardTextBox : TextBox
    {
       
        public override string Text
        {
            get
            {
                return base.Text;
            }
            set
            {
                if (this.TextMode != TextBoxMode.Password)
                {
                    if (value != null)
                    {
                        //Remove Tab from the string
                        value = value.Replace("\t", " ");

                        //Remove special char
                        Int32 iLength = value.Length;
                        Int64 iASCII = 0;
                        for (Int16 iCount = 0; iCount < iLength; iCount++)
                        {
                            iASCII = Convert.ToInt64(value.Substring(iCount, 1).ToCharArray()[0]);
                            if (32 > iASCII || iASCII > 126)
                                if (this.TextMode == TextBoxMode.MultiLine && (iASCII == 13 || iASCII == 10))
                                {
                                    //Dont replace it with space
                                }
                                else
                                {
                                    value = value.Replace(value.Substring(iCount, 1), " ");
                                }
                        }

                        //Remove leading and trailing space
                        base.Text = value.Trim();
                    }
                }
                else
                    base.Text = value;
            }
        }
        private string controlKey = string.Empty;

        [Bindable(true),
         Category("Data"),
         DefaultValue("")]
        public string Key
        {
            get{return controlKey;}
            set { controlKey = value; 
            }
        }
    }
}
