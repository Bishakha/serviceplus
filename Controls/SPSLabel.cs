using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI.WebControls;
using System.Drawing;
using System.Web.UI;
using System.ComponentModel;

namespace Sony.US.ServicesPLUS.Controls
{
    public class SPSLabel : Label
    {
       
        private string controlKey = string.Empty;
        [Bindable(true),
         Category("Data"),
         DefaultValue("")]
        public string Key
        {
            get{return controlKey;}
            set{controlKey=value;}
        }
    }
}
