using System;
using System.Diagnostics;
using Sony.US.SIAMUtilities;
using System.Web;

namespace Sony.US.SIAMUtilities
{
	public class TraceSwitchConfigurator
	{
		private TraceSwitchConfigurator()
		{
		}

		public static System.Diagnostics.TraceSwitch GetSIAMTraceSwitch()
		{
		
            string CurrentTrace = ConfigurationData.GeneralSettings.TraceLevel.CurrentSetting;
            // TraceSettings["CurrentSetting"].ToString();
			System.Diagnostics.TraceSwitch tswitch = new System.Diagnostics.TraceSwitch("Siam trace switch","Siam trace level control switch");
			
			switch (CurrentTrace)
			{
				case "TraceVerbose":
					tswitch.Level = TraceLevel.Verbose;
					break;
				case "TraceError":
					tswitch.Level = TraceLevel.Error;
					break;
				case "TraceInfo":
					tswitch.Level = TraceLevel.Info;
					break;
				case "TraceWarning":
					tswitch.Level = TraceLevel.Warning;
					break;
				case "TraceOff":
					tswitch.Level = TraceLevel.Off;
					break;
				default:
					tswitch.Level = TraceLevel.Error;
					break;
			}
			
			return tswitch;
		}
	}

}
