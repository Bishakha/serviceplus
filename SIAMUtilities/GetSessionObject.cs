using System;
using System.Collections.Generic;
using System.Text;

namespace Sony.US.SIAMUtilities
{
    public delegate void GetSessionUser(ref object obj);
    
    public static  class GetSessionObject
    {
        //public static object obj = new object();
        //public  static event GetSessionUser PullSessionInfo;
        private static GetSessionUser objtmpHandler;
        public static GetSessionObject2 objEvntClass = new GetSessionObject2();
        public static GetSessionUser HandlerObject 
        {
           get {return objtmpHandler;}

            set 
            {
                objtmpHandler = value;
            }
        }
        public static void RaiseEvent(ref object obj)
        {            
            objEvntClass.raise(objtmpHandler,ref obj);
        } 
    }

    public  class GetSessionObject2
    {
        public  event GetSessionUser PullSessionInfo;
        public void raise(GetSessionUser objtmpHandler, ref object obj)
        {
            if (PullSessionInfo == null) PullSessionInfo += (objtmpHandler);
            PullSessionInfo(ref obj);
        }
    }
    
}
