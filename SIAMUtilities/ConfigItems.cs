using System;
using System.Collections;
using System.Diagnostics;

namespace Sony.US.SIAMUtilities
{
	/// <summary>
	/// A structure used to define a User Type
	/// </summary>
	public struct _userType
	{
		/// <summary>
		/// The Name of the User type.
		/// </summary>
		public string Name;
		/// <summary>
		/// The Login Policy for this User type.
		/// </summary>
		public _loginPolicy LoginPolicy;
		/// <summary>
		/// An array of Administration Delegates.
		/// </summary>
		public _Delegate[] AdministrationDelegates;
		/// <summary>
		/// The Authentication delegate for this user type.
		/// </summary>
		public _Delegate AuthenticationDelegate;
		/// <summary>
		/// The Authorization delegate for this user type.
		/// </summary>
		public _Delegate AuthorizationDelegate;
	}

	/// <summary>
	/// A structure used to define constructor arguments.
	/// </summary>
	public struct _constructorArg
	{
		/// <summary>
		/// The index of the argument. Used to define the position.
		/// </summary>
		public int Index;
		/// <summary>
		/// The value of the argument.
		/// </summary>
		public object Value;
	}

	/// <summary>
	/// A structure used to define a delegate.
	/// </summary>
	public struct _Delegate
	{
		/// <summary>
		/// The index order of the delegate. This value will determine the delegate order in the
		/// call chain.
		/// </summary>
		public int Index;
		/// <summary>
		/// A string value that contains the fully qualified class name.
		/// </summary>
		public string TypeDefinition;
		/// <summary>
		/// An array of constructor argument structures.
		/// </summary>
		public _constructorArg[] ConstructorArgs;
	}

	/// <summary>
	/// A structure used to define Security Questions.
	/// </summary>
	public struct _securityQuestions
	{
		/// <summary>
		/// The index order of the Question
		/// </summary>
		public int Index;
		/// <summary>
		/// A string value containing the question text.
		/// </summary>
		public string Question;
	}

	/// <summary>
	/// A structure used to define the login policy
	/// </summary>
	public struct _loginPolicy
	{
		/// <summary>
		/// A string value containing the password expiration date.
		/// </summary>
		public string PasswordExpirationDate;
		/// <summary>
		/// The string value representing the grace period.
		/// </summary>
		public string GracePeriod;
		/// <summary>
		/// An integer value representing the failed login threshold. This value is used by the
		/// Account locking mechanism in SIAM.
		/// </summary>
		public int FailedLoginThreshold;
		/// <summary>
		/// An integer value containing the TimeThreshold. This value is used by the Account Locking
		/// mechanism in SIAM.
		/// </summary>
		public int TimeThreshold;
		/// <summary>
		/// An array of Security Question Structures for this Login Policy.
		/// </summary>
		public _securityQuestions[] SecurityQuestions;
	}

	/// <summary>
	/// A structure defining the parameters for administrative access to LDAP
	/// </summary>
	public struct _siamLDAPAuthentication
	{
		/// <summary>
		/// The distinguished name of the administration user for SIAM
		/// </summary>
		public string DN;
		/// <summary>
		/// The password.
		/// </summary>
		public string Password;
	}

	/// <summary>
	/// A structure that defines SIAM's trace capabilities and current setting.
	/// </summary>
	public struct _traceLevel
	{
		/// <summary>
		/// a string value representing the current setting.
		/// </summary>
		public string CurrentSetting;
	}

	/// <summary>
	/// A structure used to define the current data access layer in siam.
	/// </summary>
	public struct _currentDAL
	{
		/// <summary>
		/// A string value containing the connection string.
		/// </summary>
		public string ConnectionString;
        /// <summary>
        /// A string value containing the connection string.
        /// </summary>
        public string ConnectionStringSPS;
		/// <summary>
		/// A string value contianing the Administrative delegate used by SIAM.
		/// </summary>
		public string AdministrationDelegate;
	}

	public struct _ISSAWebService
	{
		public string Url;
		public string RequestingApplication;
		public string RequestingUserID;
		public string RequestingPassword;
	}

    //Venkat Bug # 6395
    public struct _LdapWebService
    {
        public string Url;
        public string Adminid;
        public string UserName;
        public string Password;
    }
}
