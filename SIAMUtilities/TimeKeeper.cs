using System;

namespace Sony.US.SIAMUtilities
{
	public class TimeKeeper
	{
		private TimeKeeper()
		{
		}

		public static System.TimeSpan GetCurrentTime()
		{
			System.DateTime current = System.DateTime.Now;
			return new TimeSpan(0,current.TimeOfDay.Hours,current.TimeOfDay.Minutes,current.TimeOfDay.Seconds,current.TimeOfDay.Milliseconds);
		}
	}
}
