using System;
using System.Security.Cryptography;
using System.Text;
using System.Collections;
using Microsoft.Win32;

namespace Sony.US.SIAMUtilities {
    /// <summary>
    /// The encription class handles all of the data encryption work for SIAM.
    /// </summary>
    public class Encryption {
        public const String PWDKey = "test key";

        // No direct instantiation
        private Encryption() {
        }

        /// <summary>
        /// Encrypt: Encrypts the string value passed in using triple DES encryption provider.
        /// </summary>
        /// <param name="DataToEncrypt">The string value to encrypt</param>
        /// <returns>A byte buffer containing the encrypted data</returns>
        public static string Encrypt(string DataToEncrypt) {
            /* Read the KEY from registry to encrypt card data before sending to database */

            RegistryKey rk = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\ServicesPLUS");
            string CurrentKey = string.Empty;
            if (rk == null) {
                CurrentKey = "qb6Zibe4OEmGzfy0sgMHd2bPh7ejAo3TZRFsZ1xncY4=";
            } else {
                CurrentKey = rk.GetValue("CurrentKey") as string;
            }

            String currentKey = Encryption.DeCrypt(CurrentKey, ConfigurationData.GeneralSettings.CreditCardLockKey.CreditCard_LockKey);
            if (currentKey.IndexOf("UNLOCK") < 0) throw new ApplicationException("WRONG KEY");
            var RMCrypto = new TripleDESCryptoServiceProvider();
            //byte[] theKey = GetHash("test key");
            //byte[] IV = GetHash("test key");
            currentKey = currentKey.Replace("UNLOCK", String.Empty);
            byte[] theKey = GetHash(currentKey);
            byte[] IV = GetHash(currentKey);
            byte[] InputBuffer = StringToByteBuffer(DataToEncrypt);
            System.IO.MemoryStream theStream = new System.IO.MemoryStream(InputBuffer, 0, InputBuffer.Length, false, false);
            CryptoStream CryptStream = new CryptoStream(theStream, RMCrypto.CreateEncryptor(theKey, IV), CryptoStreamMode.Read);
            byte[] encryptedBuffer = StreamToBuffer(CryptStream);
            return System.Convert.ToBase64String(encryptedBuffer, 0, encryptedBuffer.Length);
        }

        /// <summary>
        /// Encrypt: Encrypts the string value passed in using triple DES encryption provider.
        /// </summary>
        /// <param name="DataToEncrypt">The string value to encrypt</param>
        /// <param name="Key">Specifies the Key to be used to encrypt the data</param>
        /// <returns>A byte buffer containing the encrypted data</returns>
        public static string Encrypt(string DataToEncrypt, string Key) {
            System.Security.Cryptography.TripleDESCryptoServiceProvider RMCrypto = new TripleDESCryptoServiceProvider();
            byte[] theKey = GetHash(Key);
            byte[] IV = GetHash(Key);
            byte[] InputBuffer = StringToByteBuffer(DataToEncrypt);
            System.IO.MemoryStream theStream = new System.IO.MemoryStream(InputBuffer, 0, InputBuffer.Length, false, false);
            CryptoStream CryptStream = new CryptoStream(theStream, RMCrypto.CreateEncryptor(theKey, IV), CryptoStreamMode.Read);
            byte[] encryptedBuffer = StreamToBuffer(CryptStream);
            return System.Convert.ToBase64String(encryptedBuffer, 0, encryptedBuffer.Length);
        }

        /// <summary>
        /// DeCrypt: Decrypts a byte buffer back into it's string value.
        /// </summary>
        /// <param name="DataToDecrypt">The string value to decrypt</param>
        /// <returns>The unencrypted string value</returns>
        public static string DeCrypt(string DataToDeCrypt, string Key, string iV) {
            /* Read the KEY from registry to decrypt card data retrieved from database */
            String currentKey;
            try {


                RegistryKey rk = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\ServicesPLUS");
                string CurrentKeyFromRegistry;
                if (rk == null) {
                    CurrentKeyFromRegistry = "qb6Zibe4OEmGzfy0sgMHd2bPh7ejAo3TZRFsZ1xncY4=";
                } else {
                    CurrentKeyFromRegistry = rk.GetValue("CurrentKey") as string;
                }


                currentKey = Encryption.DeCrypt(CurrentKeyFromRegistry, ConfigurationData.GeneralSettings.CreditCardLockKey.CreditCard_LockKey);
                if (currentKey.IndexOf("UNLOCK") < 0) {
                    throw new ApplicationException("Wrong Lock Key");
                } else
                    currentKey = currentKey.Replace("UNLOCK", String.Empty);

            } catch (Exception ex) {
                throw ex;
            }
            byte[] theKey;
            byte[] IV;
            if (Key == null) {
                theKey = GetHash(currentKey);
                IV = GetHash(currentKey);
            } else {
                theKey = GetHash(Key);
                IV = GetHash(iV);
            }
            byte[] InputBuffer = System.Convert.FromBase64String(DataToDeCrypt);
            System.Security.Cryptography.TripleDESCryptoServiceProvider RMCrypto = new TripleDESCryptoServiceProvider();
            System.IO.MemoryStream theStream = new System.IO.MemoryStream(InputBuffer, 0, InputBuffer.Length, false, false);
            CryptoStream CryptStream = new CryptoStream(theStream, RMCrypto.CreateDecryptor(theKey, IV), CryptoStreamMode.Read);
            System.Text.StringBuilder sb = new StringBuilder();
            while (true) {
                int x = CryptStream.ReadByte();
                if (x == -1) {
                    break;
                } else {
                    sb.Append((char)x);
                }
            }
            return sb.ToString();
        }


        public static Boolean CheckLockKey(string LockKey, out string ErrorMessage) {
            /* Read the KEY from registry to decrypt card data retrieved from database */
            String currentKey;
            try {
                RegistryKey rk = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\ServicesPLUS");
                string CurrentKeyFromRegistry;
                if (rk == null) {
                    CurrentKeyFromRegistry = "qb6Zibe4OEmGzfy0sgMHd2bPh7ejAo3TZRFsZ1xncY4=";
                } else {
                    CurrentKeyFromRegistry = rk.GetValue("CurrentKey") as string;
                }

                currentKey = Encryption.DeCrypt(CurrentKeyFromRegistry, LockKey);
                if (currentKey.IndexOf("UNLOCK") < 0) {
                    ErrorMessage = "";
                    throw new ApplicationException("Wrong Lock Key");
                } else {
                    currentKey = currentKey.Replace("UNLOCK", String.Empty);
                    ErrorMessage = "";
                    return true;
                }
            } catch (Exception ex) {
                ErrorMessage = ex.Message;
                return false;
            }
        }

        /// <summary>
        ///  DeCrypt: Decrypts a byte buffer back into it's string value using the key specified.
        /// </summary>
        /// <param name="DataToDeCrypt">The string value to decrypt</param>
        /// <param name="Key">Specifies the Key to be used to decrypt the data</param>
        /// <returns></returns>
        public static string DeCrypt(string DataToDeCrypt, string Key) {
            DataToDeCrypt = DataToDeCrypt.Trim();
            byte[] theKey;
            byte[] IV;

            theKey = GetHash(Key);
            IV = GetHash(Key);
            byte[] InputBuffer = System.Convert.FromBase64String(DataToDeCrypt);
            System.Security.Cryptography.TripleDESCryptoServiceProvider RMCrypto = new TripleDESCryptoServiceProvider();
            System.IO.MemoryStream theStream = new System.IO.MemoryStream(InputBuffer, 0, InputBuffer.Length, false, false);
            CryptoStream CryptStream = new CryptoStream(theStream, RMCrypto.CreateDecryptor(theKey, IV), CryptoStreamMode.Read);
            System.Text.StringBuilder sb = new StringBuilder();
            while (true) {
                int x = CryptStream.ReadByte();
                if (x == -1) {
                    break;
                } else {
                    sb.Append((char)x);
                }
            }
            return sb.ToString();
        }

        /// <summary>
        /// GetHash computes an MD5 Hash value for a given string. This function is used
        /// specifically to obtain hash values for the encryption key and the initialization
        /// vector.
        /// </summary>
        /// <param name="key">The string for which the Hash is being computed</param>
        /// <returns>A byte buffer contining the hash values</returns>
        public static byte[] GetHash(string key) {
            byte[] data = StringToByteBuffer(key);
            MD5 md5 = new MD5CryptoServiceProvider();
            return md5.ComputeHash(data);
        }

        /// <summary>
        /// StringToByteBuffer is a utility method that simply takes an string as input
        /// and returns the string as a byte array of values.
        /// </summary>
        /// <param name="data">The string value</param>
        /// <returns>A byte array representing the input string.</returns>
        public static byte[] StringToByteBuffer(string data) {
            char[] charBuffer = data.ToCharArray();
            byte[] buffer = new byte[charBuffer.Length];
            for (int x = 0; x <= charBuffer.GetUpperBound(0); x++) {
                buffer[x] = (byte)charBuffer[x];
            }
            return buffer;
        }


        private static byte[] StreamToBuffer(System.IO.Stream stream) {
            System.Collections.ArrayList list = new System.Collections.ArrayList();
            while (true) {
                int x = stream.ReadByte();
                if (x == -1) {
                    break;
                } else {
                    list.Add((byte)x);
                }
            }
            return (byte[])list.ToArray(typeof(byte));
        }
    }
}
