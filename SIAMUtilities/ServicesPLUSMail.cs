using System;
using Sony.US.SIAMUtilities;
using System.Net.Mail;
namespace Sony.US.SIAMUtilities
{
	/// <summary>
	/// 
	/// </summary>
	public static class ServicesPLUSMail
	{	
		/// <summary>
		/// 
		/// </summary>
		/// <param name="MessageBody"></param>
		/// <param name="To"></param>
		/// <param name="From"></param>
		/// <param name="Subject"></param>
		/// <param name="HTML"></param>
		/// <param name="Priority"></param>
		/// <param name="EmailServer"></param>
		/// <param name="Port"></param>
        public static bool SendNetSMTPMail(string MessageBody, string To, string From, string BCC, string Subject, Boolean HTML, MailPriority Priority, string EmailServer, Int16 Port)
        {
            MailMessage mailObj = new MailMessage();
            SmtpClient objSMTPCLient = new SmtpClient();
            MailAddress objAddressFrom = new MailAddress(From);
            MailAddress objAddressTo;
            foreach (string strRecpt in To.Split(';'))
            {
                objAddressTo = new MailAddress(strRecpt);
                mailObj.To.Add(objAddressTo);
            }
            mailObj.Subject = Subject;

            mailObj.From = objAddressFrom;
            if (BCC.Trim().ToString() != "" )
            {
                MailAddress objAddressBCC;
                foreach (string strRecpt in BCC.Split(';'))
                {
                    objAddressBCC = new MailAddress(strRecpt);
                    mailObj.Bcc.Add(objAddressBCC);
                }
            }

            mailObj.Body = MessageBody;
            mailObj.IsBodyHtml = HTML;
            mailObj.Priority = Priority;

            try
            {
                objSMTPCLient.Host = EmailServer;
                objSMTPCLient.Port = Port;
                objSMTPCLient.Send(mailObj);
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool SendNetSMTPMail(ServicesPLUSMailObject mailObj)
        {
            return SendNetSMTPMail(mailObj.Body, mailObj.To, mailObj.From, mailObj.BCC, mailObj.Subject, mailObj.IsHTML, mailObj.Mail_Priority, mailObj.SMTP, mailObj.Port);
        }

	}

    public class ServicesPLUSMailObject
    {
        string m_from = string.Empty;
        public string From
        {
           get{return m_from;}
            set{m_from = value;}
        }
        string m_To = string.Empty;
        public string To
        {
           get{return m_To;}
            set{m_To = value;}
        }
        string m_BCC = string.Empty;
        public string BCC
        {
            get { return m_BCC; }
            set { m_BCC = value; }
        }

        string m_Subject = string.Empty;
        public string Subject
        {
            get { return m_Subject; }
            set { m_Subject = value; }
        }
        string m_Body = string.Empty;
        public string Body
        {
            get { return m_Body; }
            set { m_Body = value; }
        }
        string m_SMTP = string.Empty;
        public string SMTP
        {
            get { return m_SMTP; }
            set { m_SMTP = value; }
        }
        bool b_IsHTML = true;
        public bool IsHTML
        {
            get { return b_IsHTML; }
            set { b_IsHTML = value; }
        }
        Int16 i_Port = 25;
        public Int16 Port
        {
            get { return i_Port; }
            set { i_Port = value; }
        }
        MailPriority m_MailPriority = MailPriority.Normal;
        public MailPriority Mail_Priority
        {
            get { return m_MailPriority; }
            set { m_MailPriority = value; }
        }
    }
    
}
