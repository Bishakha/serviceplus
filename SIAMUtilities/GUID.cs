using System;
using System.Text;
namespace Sony.US.SIAMUtilities
{
	/// <summary>
	/// The GUID class contains a single static method with a private constructor. Is sole mission
	/// is to create a globally unique identifier. NOT TO BE CONFUSED WITH A WINDOWS GUID. Not the
	/// same thing. This is a sony specific implementation of the GUID concept.
	/// </summary>
	public class GUID
	{
		private GUID()
		{
		}

		public static string CreateGUID(string region, string seed)
		{
			string x1, x2, x3, x4, x5, x6, x7, x8, x9, x10, x11, x12, x13, x14, x15, x16;
			StringBuilder sSeed = new StringBuilder(seed.ToString(), 10);

			//pad seed number with zeros on the left
			int seed_length = sSeed.Length;
			int zeros = 10 - seed_length;

			for (int i=seed_length; i < 10; i++)
				sSeed.Insert(0, "0");

			//get region/country code
			string sA1 = region.Substring(0, 1);
			string sA2 = region.Substring(1, 1);
			System.Int16 a1 = Convert.ToInt16(sA1, 10);
			System.Int16 a2 = Convert.ToInt16(sA2, 10);

			string seed_work = sSeed.ToString();

			//parse sequence number into individual digits
			System.Int16 s1 = Convert.ToInt16(seed_work.Substring(0,1));
			System.Int16 s2 = Convert.ToInt16(seed_work.Substring(1,1));
			System.Int16 s3 = Convert.ToInt16(seed_work.Substring(2,1));
			System.Int16 s4 = Convert.ToInt16(seed_work.Substring(3,1));
			System.Int16 s5 = Convert.ToInt16(seed_work.Substring(4,1));
			System.Int16 s6 = Convert.ToInt16(seed_work.Substring(5,1));
			System.Int16 s7 = Convert.ToInt16(seed_work.Substring(6,1));
			System.Int16 s8 = Convert.ToInt16(seed_work.Substring(7,1));
			System.Int16 s9 = Convert.ToInt16(seed_work.Substring(8,1));
			System.Int16 s10 = Convert.ToInt16(seed_work.Substring(9,1));


			//x1 = s1 + s2 + s3 + s4 + s5 + s6 + s7 + s8 + s9 + s10
			decimal temp = (s1 + s2 + s3 + s4 + s5 + s6 + s7 + s8 + s9 + s10);
			System.Int16 c1 = Convert.ToInt16(temp % 10);
			x1 = c1.ToString();

			x2 = s10.ToString();

			//c2 = random number between 0-9
			DateTime t1 = DateTime.Now;
			int random_seed = t1.Millisecond;
			Random random_number = new Random(random_seed);

			double random = random_number.NextDouble();
			random *= 10.0;
			System.Int16 c2 = Convert.ToInt16(random);

			if ( c2 == 10 )
				c2 = 9;

			x3 = ((a1 + c2) % 10).ToString();

			//x4 = s9
			x4 = s9.ToString();

			//x5 = c2
			x5 = c2.ToString();

			//x6 = s5
			x6 = s5.ToString();

			//x7 = s4
			x7 = s4.ToString();

			x8 = ((a2 + c2) % 10).ToString();

			//x9 = s1
			x9 = s1.ToString();

			//c3 = (c1 * c2) / 10
			int c3 = (c1 * c2) / 10;

			//x10 = c3
			x10 = c3.ToString();

			//x11 = s2
			x11 = s2.ToString();

			//x12 = s3
			x12 = s3.ToString();

			//x13 = s8
			x13 = s8.ToString();

			int c4 = (c1 * c2) % 10;

			//x14 = c4
			x14 = c4.ToString();

			//x15 = s7
			x15 = s7.ToString();

			//x16 = 9 - s6
			x16 = (9 - s6).ToString();

			return x1 + x2 + x3 + x4 + x5 + x6 + x7 + x8 + x9 + x10 + x11 + x12 + x13 + x14 + x15 + x16;
		}
        public static string GetSystemGUID()
        {
            Guid objGUID;
            objGUID = Guid.NewGuid();
            return objGUID.ToString();
        }
	}
}
