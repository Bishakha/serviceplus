﻿using System;
using System.Data;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;

namespace Sony.US.SIAMUtilities {
    public class ServicesPLUSReports
    {
        public void ConvertCrReportToPDF(ref ReportDocument DocReport, string TempFileName, DataView ReportData) 
        {
            try
             {
                ExportOptions exportOpts  = new ExportOptions();
                PdfRtfWordFormatOptions pdfFormatOpts = new PdfRtfWordFormatOptions();
                DiskFileDestinationOptions diskOpts=new DiskFileDestinationOptions();
                DocReport.SetDataSource(ReportData.Table);
                //ReportDocument DocReport;
                //DocReport.FileName = Server.MapPath("Report\EWCertificate.rpt")
                exportOpts =  DocReport.ExportOptions;
                
                exportOpts.ExportFormatType = ExportFormatType.PortableDocFormat;
                exportOpts.FormatOptions = pdfFormatOpts;
                exportOpts.ExportDestinationType = ExportDestinationType.DiskFile;

                exportOpts.ExportFormatType = ExportFormatType.PortableDocFormat;

                exportOpts.ExportDestinationType = ExportDestinationType.DiskFile;
                diskOpts.DiskFileName = TempFileName;
                exportOpts.DestinationOptions = diskOpts;
                DocReport.Export();
             }
            catch (Exception  exReport)
             {
                 Console.WriteLine(exReport.Message);
             }

               
                
         
        }

        public void ConvertCrReportToPDF(string ReportFileAbsolutePath, string TempFileName,DataView ReportData)
        {
            try
            {
                //ExportOptions exportOpts = new ExportOptions();
                //PdfRtfWordFormatOptions pdfFormatOpts = new PdfRtfWordFormatOptions();
                //DiskFileDestinationOptions diskOpts = new DiskFileDestinationOptions();
                ReportDocument DocReport= new ReportDocument();
                DocReport.FileName = ReportFileAbsolutePath;
                ConvertCrReportToPDF(ref DocReport, TempFileName,ReportData);
                //exportOpts = DocReport.ExportOptions;

                //exportOpts.ExportFormatType = ExportFormatType.PortableDocFormat;
                //exportOpts.FormatOptions = pdfFormatOpts;
                //exportOpts.ExportDestinationType = ExportDestinationType.DiskFile;

                //exportOpts.ExportFormatType = ExportFormatType.PortableDocFormat;

                //exportOpts.ExportDestinationType = ExportDestinationType.DiskFile;
                //diskOpts.DiskFileName = TempFileName;
                //exportOpts.DestinationOptions = diskOpts;
                //DocReport.Export();
            }
            catch (Exception exReport)
            {
                Console.WriteLine(exReport.Message);
            }




        }

        //       Public Sub ConvertCrReportToPDF(ByVal TempReportFileName As String)
        //    Try
        //        Dim exportOpts As ExportOptions = New ExportOptions()
        //        Dim pdfFormatOpts As PdfRtfWordFormatOptions = New PdfRtfWordFormatOptions()
        //        Dim diskOpts As DiskFileDestinationOptions = New DiskFileDestinationOptions()
        //        Dim DocReport As ReportDocument = New ReportDocument()
        //        DocReport.FileName = Server.MapPath("Report\EWCertificate.rpt")
        //        exportOpts = DocReport.ExportOptions

        //        '// Set the PDF format options.
        //        exportOpts.ExportFormatType = ExportFormatType.PortableDocFormat
        //        exportOpts.FormatOptions = pdfFormatOpts
        //        '// Set the options and export.
        //        exportOpts.ExportDestinationType = ExportDestinationType.DiskFile

        //        ' Set the export format.
        //        exportOpts.ExportFormatType = ExportFormatType.PortableDocFormat

        //        exportOpts.ExportDestinationType = ExportDestinationType.DiskFile

        //        ' Set the disk file options.
        //        diskOpts.DiskFileName = Server.MapPath(TempReportFileName)
        //        '
        //        exportOpts.DestinationOptions = diskOpts

        //        DocReport.Export()

        //        'Dim conContentType As String = "APPLICATION/OCTET-STREAM"
        //        'Dim conAppendedHeaderType As String = "Content-Disposition"
        //        'Dim conFileNamePlaceHolder As String = "##FILENAME##"
        //        'Dim conHeaderType As String = "Attachment; Filename=""" + conFileNamePlaceHolder + """"
        //        'Dim headerInfo As String = conHeaderType.Replace(conFileNamePlaceHolder, "EW.pdf")

        //        'Response.ContentType = conContentType
        //        'Response.AppendHeader(conAppendedHeaderType, headerInfo)
        //        'Response.WriteFile(UrlDecode(UrlEncode(Server.MapPath("./PDFs/x.pdf"))))

        //    Catch exThreadAbort As System.Threading.ThreadAbortException
        //    Catch ex As Exception
        //        Response.Write(ex.Message + vbNewLine + ex.StackTrace + vbNewLine + ex.Source)
        //    End Try

        //End Sub
  
    }
}
