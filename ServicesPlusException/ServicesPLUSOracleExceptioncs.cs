﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ServicesPlusException
{
    public class ServicesPLUSOracleException : ServicesPLUSBaseException 
    {
        public ServicesPLUSOracleException(string ErrorId, Exception objException)
        {
            if (!String.IsNullOrEmpty(ErrorId))
            {
                this.InitializeException("OracleException",ErrorId, objException);
            }
            else
            {
                throw new ArgumentNullException() ;
            }
        }
        public ServicesPLUSOracleException(string errosMessage)
        {
            if (!String.IsNullOrEmpty(errosMessage))
            {
                this.InitializeException("OracleException",errosMessage ,new Exception ());
            }
            else
            {
                throw new ArgumentNullException();
            }
        }
    }
}
